﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace EmmeApp.Common.Exceptions
{
    [Serializable]
    public class HttpCodeException : Exception
    {
        public HttpStatusCode HttpStatusCode { get; set; }
        public HttpCodeException() { }
        public HttpCodeException(string message) : base(message) { }
        public HttpCodeException(string message, HttpStatusCode httpStatusCode) : base(message)
        {
            HttpStatusCode = httpStatusCode;
        }
        public HttpCodeException(string message, Exception innerException) : base(message, innerException) { }
        protected HttpCodeException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
