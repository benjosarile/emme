﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace EmmeApp.Common.Exceptions
{
	[Serializable]
    public class NoInternetConnectivityException : Exception
	{
		public HttpStatusCode HttpStatusCode { get; set; }
		public NoInternetConnectivityException() { }
		public NoInternetConnectivityException(string message) : base(message) { }
		public NoInternetConnectivityException(string message, HttpStatusCode httpStatusCode) : base(message)
		{
			HttpStatusCode = httpStatusCode;
		}
		public NoInternetConnectivityException(string message, Exception innerException) : base(message, innerException) { }
		protected NoInternetConnectivityException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}
