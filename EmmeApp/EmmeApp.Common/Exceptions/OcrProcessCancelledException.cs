﻿using System;
using System.Runtime.Serialization;

namespace EmmeApp.Common.Exceptions
{
    [Serializable]
    public class OcrProcessCancelledException : Exception
    {
        public OcrProcessCancelledException() {}
        protected OcrProcessCancelledException(SerializationInfo info, StreamingContext context) : base(info, context) {}
    }
}
