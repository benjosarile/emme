﻿namespace EmmeApp.Common.Constants
{
    public static class Endpoints
	{
		public const string GetSortiment = "uploads/tx_biralexchange/1.0/sortiment.json";
		public const string GetExchange = "uploads/tx_biralexchange/1.0/exchange_{0}.json";
		public const string DocumentSearch = "service/json/portal/search";
		public const string DocumentCatalogDetails = "service/json/catalog/details?catalog={0}&portal={1}&filterLang=EN&lang=en";

		public const string AzureCognitiveOcr = "vision/v2.1/ocr?language=en&detectOrientation=false";

	}
}
