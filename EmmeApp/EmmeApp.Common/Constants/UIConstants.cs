﻿namespace EmmeApp.Common.Constants
{
    public static class UIConstants
	{
		public const string LabelCauseMessage = "lblCauseMessage";
		public const string LabelMeasureMessage = "lblMeasureMessage";
	}
}
