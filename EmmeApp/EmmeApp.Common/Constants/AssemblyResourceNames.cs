﻿namespace EmmeApp.Common.Constants
{
	public static class AssemblyResourceNames
	{
		public const string PumpReplacementMasterData = "EmmeApp.LocalData.Files.MasterData.csv";
		public const string PumpReplacementMarketCh = "EmmeApp.LocalData.Files.pumpreplacement_ch.json";
		public const string PumpReplacementMarketDe = "EmmeApp.LocalData.Files.pumpreplacement_de.json";
		public const string PumpReplacementMarketIt = "EmmeApp.LocalData.Files.pumpreplacement_it.json";
		public const string PumpReplacementMarketNl = "EmmeApp.LocalData.Files.pumpreplacement_nl.json";
		public const string PumpReplacementMarketEu = "EmmeApp.LocalData.Files.pumpreplacement_eu.json";
		public const string PumpReplacementMarketAt = "EmmeApp.LocalData.Files.pumpreplacement_at.json";
		public const string PumpReplacementCategories = "EmmeApp.LocalData.Files.categories.json";
		public const string PumpReplacementComments = "EmmeApp.LocalData.Files.comments.json";
		public const string PumpReplacementConnectors = "EmmeApp.LocalData.Files.connectors.json";
		public const string PumpReplacementSortiment = "EmmeApp.LocalData.Files.sortiment.json";
		public const string BleServices = "EmmeApp.LocalData.Files.bleService.json";
		public const string BiralContacts = "EmmeApp.LocalData.Files.biralContacts.json";
		public const string BiralGtcs = "EmmeApp.LocalData.Files.biralGTCs.json";

	}
}
