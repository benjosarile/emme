﻿namespace EmmeApp.Common.Constants
{
    public static class AppConstants
    {
        public const string AppCenterLogTag = "Emme";
        public const string AppCenterAndroidKey = "aef69777-2342-4524-b1b5-ae2b28c9cd18";
        public const string AppCenteriOSKey = "6693307a-1a93-4004-ac84-3d56614f6a37";

        public const string MarketCH = "CH";
        public const string MarketDE = "DE";
        public const string MarketNL = "NL";
        public const string MarketIT = "IT";
        public const string MarketEU = "EU";
        public const string MarketAT = "AT";
        public const string MarketES = "ES";

        public const string LanguageCodeEN = "EN";
        public const string LanguageCodeDE = "DE";
        public const string LanguageCodeNL = "NL";
        public const string LanguageCodeIT = "IT";
        public const string LanguageCodeFR = "FR";
        public const string LanguageCodeES = "ES";

        public const string ConstantPressureIcon = "ic_constant_pressure";
        public const string ProportionalPressureIcon = "ic_proportional_pressure";
        public const string ConstantSpeedIcon = "ic_constant_speed";

        public const string DocumentPortal = "2025044";

        public const string Configuration = "Configuration";
        public const string Cockpit = "Cockpit";
        public const string Log = "Log";

        public const byte BiralManufacturerData1 = 0xC0;
        public const byte BiralManufacturerData2 = 0x07;
    }
}
