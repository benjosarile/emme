﻿namespace EmmeApp.Common.Constants
{
	public static class Server
	{
		public const string BiralServerBaseAddress = " https://www.biral.ch/";
		public const string OxomiServerBaseAddress = "https://oxomi.com/";
		public const string AzureCognitiveApiBaseAddress = "https://biralone-pumpreplacement-computervision.cognitiveservices.azure.com/";

		public const string OcmApimSubscriptionKeyHeader = "Ocp-Apim-Subscription-Key";
		public const string OcmApimSubscriptionKey = "3f4b9d659ee74f85a261610c64184d85";
	}
}
