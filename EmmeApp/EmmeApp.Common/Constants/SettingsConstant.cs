﻿namespace EmmeApp.Common.Constants
{
    public static class SettingsConstant
	{
		public const string PumpReplacementLastModified = "PumpReplacementLastModified";
		public const string HasGtcConfirmed = "HasGtcConfirmed";
		public const string DidOcrWasUsed = "DidOcrWasUsed";
	}
}
