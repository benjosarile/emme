﻿namespace EmmeApp.Common.Constants
{
    public static class ResourceConstants 
	{
		public const string InitialStartup_Form1_CustomerInfoPage_Help = "InitialStartup.Form1.CustomerInfoPage.Help";
		public const string InitialStartup_Form2_SystemDataConfigurationPage_Help = "InitialStartup.Form2.SystemDataConfigurationPage.Help";
		public const string InitialStartup_Form3_LevelProbeConfigurationPage_Help = "InitialStartup.Form3.LevelProbeConfigurationPage.Help";
		public const string InitialStartup_Form4_PumpAndMotorConfigurationPage_Help = "InitialStartup.Form4.PumpAndMotorConfigurationPage.Help";

		public const string InitialStartup_Form5_TimingConfigurationPage_Help = "InitialStartup.Form5.TimingConfigurationPage.Help";
		public const string InitialStartup_Form6_MaintenanceAndRelayConfigurationPage_Help = "InitialStartup.Form6.MaintenanceAndRelayConfigurationPage.Help";
        public const string Connect_To_Gateway_Devices_Found_Help = "ConnectToGateway.DevicesFound.Help";
        public const string Connect_To_Gateway_No_Devices_Found_Help = "ConnectToGateway.NoDevicesFound.Help";
        public const string Pump_Configuration_Form1_Help = "PumpConfiguration.Form1.Help";
        public const string Pump_Configuration_SystemDescription_Help = "PumpConfiguration.SystemDescription.Help";
	}
}
