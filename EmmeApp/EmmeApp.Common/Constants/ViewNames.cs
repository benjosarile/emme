﻿namespace EmmeApp.Common.Constants
{
    public static class ViewNames
	{
		public const string TransitionNavigationPage = "TransitionNavigationPage";
		public const string NavigationPage = "NavigationPage";
		public const string EmmeMasterDetailPage = "EmmeMasterDetailPage";
		public const string HomePage = "HomePage";
		public const string LoadingPage = "LoadingPage";
		public const string SearchPumpPage = "SearchPumpPage";
        public const string PumpReplacementDetailPage = "PumpReplacementDetailPage";
		public const string PumpFittingProductsPage = "PumpFittingProductsPage";
		public const string InitialStartupForm1CustomerInfoPage = "InitialStartupForm1CustomerInfoPage";
        public const string InitialStartupForm2SystemDataConfigurationPage = "InitialStartupForm2SystemDataConfigurationPage";
        public const string InitialStartupForm3LevelProbeConfigurationPage = "InitialStartupForm3LevelProbeConfigurationPage";
		public const string InitialStartupForm4PumpAndMotorConfigurationPage = "InitialStartupForm4PumpAndMotorConfigurationPage";
		public const string InitialStartupForm5TimingConfigurationPage = "InitialStartupForm5TimingConfigurationPage";
		public const string InitialStartupForm6MaintenanceAndRelayConfigurationPage = "InitialStartupForm6MaintenanceAndRelayConfigurationPage";

		public const string CockpitControlUnitTabbedPage = "CockpitControlUnitTabbedPage";
		public const string CockpitControlUnitRootPage = "CockpitControlUnitRootPage";
		public const string CockpitControlUnitLogPage = "CockpitControlUnitLogPage";

		public const string CockpitPumpsModulATabbedPage = "CockpitPumpsModulATabbedPage";
        public const string CockpitPumpsModulARootPage = "CockpitPumpsModulARootPage";

        public const string CockpitPumpsVariAERootPage = "CockpitPumpsVariAERootPage";

        public const string ConnectToGatewayPage = "ConnectToGatewayPage";
        public const string ConnectToGatewayListPage = "ConnectToGatewayListPage";
        public const string GatewaySubPage = "GatewaySubPage";

        public const string PumpConfigurationModulAForm1Page = "PumpConfigurationModulAForm1Page";    
        public const string PumpConfigurationModulAForm2SystemDescriptionPage = "PumpConfigurationModulAForm2SystemDescriptionPage";
        public const string PumpConfigurationVariAForm1Page = "PumpConfigurationVariAForm1Page";
        public const string PumpConfigurationVariAForm2SystemDescriptionPage = "PumpConfigurationVariAForm2SystemDescriptionPage";

        public const string PumpLogRootPage = "PumpLogRootPage";
        public const string ControlUnitLogRootPage = "ControlUnitLogRootPage";

		public const string AppSettingsPage = "AppSettingsPage";
		public const string AboutPage = "AboutPage";
		public const string ContactPage = "ContactPage";

		public const string DocumentSearchPage = "DocumentSearchPage";
		public const string DocumentViewPage = "DocumentViewPage";
		public const string DocumentSearchFilterPage = "DocumentSearchFilterPage";
	}
}
