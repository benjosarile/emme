﻿namespace EmmeApp.Common.Constants
{
    public static class NavigationConstants
	{
		public const string ChainModalStack = nameof(ChainModalStack);

		public const string SelectedReplacementPump = "SelectedReplacementPump";
		public const string SelectedBiralProduct = "SelectedBiralProduct";
		public const string ShouldExitWizard = "ShouldExitWizard";
		public const string WizardAlertPopupButtonLeftText = "WizardAlertPopupButtonLeftText";
		public const string WizardAlertPopupRightButtonText = "WizardAlertPopupRightButtonText";
		public const string WizardAlertPopupLeftButtonCommand = "WizardAlertPopupLeftButtonCommand";
        public const string WizardAlertPopupRightButtonCommand = "WizardAlertPopupRightButtonCommand";
        public const string InitialStartupWizardModel = "InitialStartupWizardModel";
        public const string CockpitControlUnitOverviewModel = "CockpitControlUnitOverviewModel";
        public const string CockpitControlUnitRootModel = "CockpitControlUnitRootModel";
        public const string CockpitPumpsModulARootModel = "CockpitPumpsModulARootModel";
        public const string CockpitPumpsVariAERootModel = "CockpitPumpsVariAERootModel";
		public const string InitialParameters = "InitialParameters";
		public const string ParameterCharacteristics = "ParameterCharacteristics";
		public const string HasUpdate = "HasUpdate";


		public const string GatewayConnectionListModel = "GatewayConnectionListModel";
        public const string SelectedGatewayDevice = "SelectedGatewayDevice";
        public const string SelectedBleDevice = "SelectedDevice";
		public const string ShouldExitConnection = "ShouldExitConnection";

        public const string PumpConfigurationModulAWizardModel = "PumpConfigurationModulAWizardModel";
        public const string PumpConfigurationVariAWizardModel = "PumpConfigurationVariAWizardModel";

        public const string PumpLogRootModel = "PumpLogRootModel";
        public const string ControlUnitLogRootModel = "ControlUnitLogRootModel";

		public const string Logs = "Logs";
		public const string DocumentSearchKey = "DocumentSearchKey";
		public const string DocumentFilter = "DocumentFilter";
		public const string DocumentSelectedFilter = "DocumentSelectedFilter";
		public const string DocumentTitle = "DocumentTitle";
		public const string DocumentPageNumber = "DocumentPageNumber";
		public const string DocumentHighlightedText = "DocumentHighlightedText";
		public const string DocumentFilePath = "DocumentFilePath";

        public const string PageNameParameter = "PageNameParameter";
		public const string ShowNoTextPumpReplacement = "ShowNoTextPumpReplacement";



	}
}
