﻿using EmmeApp.Common.Attributes;
using EmmeApp.Common.Enum;
using Prism.Mvvm;
using System;

namespace EmmeApp.Common.PageModels.InitialStartup
{
    public class InitialStartupWizardModel : BindableBase
    {
		private Guid _selectedDeviceUuid;
		public Guid SelectedDeviceUuid
		{
			get => _selectedDeviceUuid;
			set => SetProperty(ref _selectedDeviceUuid, value);
		}

		private string _customerInformationName;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerName, typeof(string))]
		public string CustomerInformationName
        {
            get => _customerInformationName;
            set => SetProperty(ref _customerInformationName, value);
        }

        private string _customerInformationPhone;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerPhone, typeof(string))]
		public string CustomerInformationPhone
        {
            get => _customerInformationPhone;
            set => SetProperty(ref _customerInformationPhone, value);
        }

        private string _customerInformationStreet;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerStreet, typeof(string))]
		public string CustomerInformationStreet
        {
            get => _customerInformationStreet;
            set => SetProperty(ref _customerInformationStreet, value);
        }

        private string _customerInformationCity;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerLocation, typeof(string))]
		public string CustomerInformationCity
        {
            get => _customerInformationCity;
            set => SetProperty(ref _customerInformationCity, value);
        }

        private string _customerInformationZipCode;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerZip, typeof(string))]
		public string CustomerInformationZipCode
        {
            get => _customerInformationZipCode;
            set => SetProperty(ref _customerInformationZipCode, value);
        }

        private string _customerInformationCountry;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerCountry, typeof(string))]
		public string CustomerInformationCountry
        {
            get => _customerInformationCountry;
            set => SetProperty(ref _customerInformationCountry, value);
        }

        private string _systemDataConfigurationSystemName;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerSystemName, typeof(string))]
		public string SystemDataConfigurationSystemName
        {
            get => _systemDataConfigurationSystemName;
            set => SetProperty(ref _systemDataConfigurationSystemName, value);
        }

        private string _systemDataConfigurationMotor1Name;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerMotorType0, typeof(string))]
		public string SystemDataConfigurationMotor1Name
        {
            get => _systemDataConfigurationMotor1Name;
            set => SetProperty(ref _systemDataConfigurationMotor1Name, value);
        }

        private string _systemDataConfigurationMotor2Name;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerMotorType1, typeof(string))]
		public string SystemDataConfigurationMotor2Name
        {
            get => _systemDataConfigurationMotor2Name;
            set => SetProperty(ref _systemDataConfigurationMotor2Name, value);
        }

        private ControlUnitSystemTypeValue _systemDataConfigurationSystemType = ControlUnitSystemTypeValue.NotConfigured;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemType, typeof(ControlUnitSystemTypeValue))]
		public ControlUnitSystemTypeValue SystemDataConfigurationSystemType
        {
            get => _systemDataConfigurationSystemType;
            set => SetProperty(ref _systemDataConfigurationSystemType, value);
        }

        private LevelProbeConfigurationValue _levelProbeConfigurationValue = LevelProbeConfigurationValue.NotConfigured;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelProbeConfiguration, typeof(LevelProbeConfigurationValue))]
		public LevelProbeConfigurationValue LevelProbeConfigurationValue
        {
            get => _levelProbeConfigurationValue;
            set => SetProperty(ref _levelProbeConfigurationValue, value);
        }

        private double _levelProbeConfigurationLevelAnalogThreshold2 = 10;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelAnalogThreshold2Current, typeof(double))]
		public double LevelProbeConfigurationLevelAnalogThreshold2
        {
            get => _levelProbeConfigurationLevelAnalogThreshold2;
            set => SetProperty(ref _levelProbeConfigurationLevelAnalogThreshold2, value);
        }

        private double _levelProbeConfigurationLevelAnalogThreshold1 = 10;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelAnalogThreshold1Current, typeof(double))]
		public double LevelProbeConfigurationLevelAnalogThreshold1
        {
            get => _levelProbeConfigurationLevelAnalogThreshold1;
            set => SetProperty(ref _levelProbeConfigurationLevelAnalogThreshold1, value);
        }

        private double _levelProbeConfigurationLevelAnalogThreshold0 = 10;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelAnalogThreshold0Current, typeof(double))]
		public double LevelProbeConfigurationLevelAnalogThreshold0
        {
            get => _levelProbeConfigurationLevelAnalogThreshold0;
            set => SetProperty(ref _levelProbeConfigurationLevelAnalogThreshold0, value);
        }

        private ControlUnitSystemDoubleMotorValue _pumpAndMotorConfigurationNumberOfPumpValue = ControlUnitSystemDoubleMotorValue.OnePump;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemDoubleMotor, typeof(ControlUnitSystemDoubleMotorValue))]
		public ControlUnitSystemDoubleMotorValue PumpAndMotorConfigurationNumberOfPumpValue
        {
            get => _pumpAndMotorConfigurationNumberOfPumpValue;
            set => SetProperty(ref _pumpAndMotorConfigurationNumberOfPumpValue, value);
        }

        private ControlUnitOskStateValue _pumpAndMotorConfigurationOSK = ControlUnitOskStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorOskEnable, typeof(ControlUnitOskStateValue))]
		public ControlUnitOskStateValue PumpAndMotorConfigurationOSK
        {
            get => _pumpAndMotorConfigurationOSK;
            set => SetProperty(ref _pumpAndMotorConfigurationOSK, value);
        }

        private ControlUnitMotorModeValue _pumpAndMotorConfigurationSystemMotorModePump1Value = ControlUnitMotorModeValue.NotConfigured;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemMotorMode0, typeof(ControlUnitMotorModeValue))]
		public ControlUnitMotorModeValue PumpAndMotorConfigurationSystemMotorModePump1Value
        {
            get => _pumpAndMotorConfigurationSystemMotorModePump1Value;
            set => SetProperty(ref _pumpAndMotorConfigurationSystemMotorModePump1Value, value);
        }

        private ControlUnitMotorModeValue _pumpAndMotorConfigurationSystemMotorModePump2Value = ControlUnitMotorModeValue.NotConfigured;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemMotorMode1, typeof(ControlUnitMotorModeValue))]
		public ControlUnitMotorModeValue PumpAndMotorConfigurationSystemMotorModePump2Value
        {
            get => _pumpAndMotorConfigurationSystemMotorModePump2Value;
            set => SetProperty(ref _pumpAndMotorConfigurationSystemMotorModePump2Value, value);
        }

        private double _pumpAndMotorConfigurationMotorRateCurrentPump1 = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorRatedCurrentMotor0, typeof(double))]
		public double PumpAndMotorConfigurationMotorRateCurrentPump1
        {
            get => _pumpAndMotorConfigurationMotorRateCurrentPump1;
            set => SetProperty(ref _pumpAndMotorConfigurationMotorRateCurrentPump1, value);
        }

        private double _pumpAndMotorConfigurationMotorRateCurrentPump2 = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorRatedCurrentMotor1, typeof(double))]
		public double PumpAndMotorConfigurationMotorRateCurrentPump2
        {
            get => _pumpAndMotorConfigurationMotorRateCurrentPump2;
            set => SetProperty(ref _pumpAndMotorConfigurationMotorRateCurrentPump2, value);
        }

        private ControlUnitThermalProtectionStateValue _pumpAndMotorConfigurationThermalProtectionPump1 = ControlUnitThermalProtectionStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorWskMotor0Enable, typeof(ControlUnitThermalProtectionStateValue))]
		public ControlUnitThermalProtectionStateValue PumpAndMotorConfigurationThermalProtectionPump1
        {
            get => _pumpAndMotorConfigurationThermalProtectionPump1;
            set => SetProperty(ref _pumpAndMotorConfigurationThermalProtectionPump1, value);
        }

        private ControlUnitThermalProtectionStateValue _pumpAndMotorConfigurationThermalProtectionPump2 = ControlUnitThermalProtectionStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorWskMotor1Enable, typeof(ControlUnitThermalProtectionStateValue))]
		public ControlUnitThermalProtectionStateValue PumpAndMotorConfigurationThermalProtectionPump2
        {
            get => _pumpAndMotorConfigurationThermalProtectionPump2;
            set => SetProperty(ref _pumpAndMotorConfigurationThermalProtectionPump2, value);
        }


        private ControlUnitDelayedOffStateValue _timingConfigurationMotorDelayedOffEnable = ControlUnitDelayedOffStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorDelayedOffEnable, typeof(ControlUnitDelayedOffStateValue))]
		public ControlUnitDelayedOffStateValue TimingConfigurationMotorDelayedOffEnable
        {
            get => _timingConfigurationMotorDelayedOffEnable;
            set => SetProperty(ref _timingConfigurationMotorDelayedOffEnable, value);
        }

        private double _timingConfigurationMotorDelayedOffTime = 5;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorDelayedOffTime, typeof(double))]
		public double TimingConfigurationMotorDelayedOffTime
        {
            get => _timingConfigurationMotorDelayedOffTime;
            set => SetProperty(ref _timingConfigurationMotorDelayedOffTime, value);
        }

        private ControlUnitForceRunningStateValue _timingConfigurationMotorForceRunningEnable = ControlUnitForceRunningStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorForceRunningEnable, typeof(ControlUnitForceRunningStateValue))]
		public ControlUnitForceRunningStateValue TimingConfigurationMotorForceRunningEnable
        {
            get => _timingConfigurationMotorForceRunningEnable;
            set => SetProperty(ref _timingConfigurationMotorForceRunningEnable, value);
        }

        private double _timingConfigurationMotorForceRetentionTime = 30;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorForceRetentionTime, typeof(double))]
		public double TimingConfigurationMotorForceRetentionTime
        {
            get => _timingConfigurationMotorForceRetentionTime;
            set => SetProperty(ref _timingConfigurationMotorForceRetentionTime, value);
        }

        private double _timingConfigurationMotorForceRunningTime = 5;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorForceRunningOnTime, typeof(double))]
		public double TimingConfigurationMotorForceRunningTime
        {
            get => _timingConfigurationMotorForceRunningTime;
            set => SetProperty(ref _timingConfigurationMotorForceRunningTime, value);
        }

        private ControlUnitDelayedOnStateValue _timingConfigurationMotorDelayedOnEnable = ControlUnitDelayedOnStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorDelayedOnEnable, typeof(ControlUnitDelayedOnStateValue))]
		public ControlUnitDelayedOnStateValue TimingConfigurationMotorDelayedOnEnable
        {
            get => _timingConfigurationMotorDelayedOnEnable;
            set => SetProperty(ref _timingConfigurationMotorDelayedOnEnable, value);
        }

        private double _timingConfigurationMotorDelayedOnTime = 5;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorDelayedOnTime, typeof(double))]
		public double TimingConfigurationMotorDelayedOnTime
        {
            get => _timingConfigurationMotorDelayedOnTime;
            set => SetProperty(ref _timingConfigurationMotorDelayedOnTime, value);
        }

        private ControlUnitMotorRuntimeObserverStateValue _timingConfigurationMotorRuntimeObserver = ControlUnitMotorRuntimeObserverStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorRuntimeObserverEnable, typeof(ControlUnitMotorRuntimeObserverStateValue))]
		public ControlUnitMotorRuntimeObserverStateValue TimingConfigurationMotorRuntimeObserver
        {
            get => _timingConfigurationMotorRuntimeObserver;
            set => SetProperty(ref _timingConfigurationMotorRuntimeObserver, value);
        }

        private double _timingConfigurationMaxCycles = 3;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorRuntimeObserverMaxCycle, typeof(double))]
		public double TimingConfigurationMaxCycles
        {
            get => _timingConfigurationMaxCycles;
            set => SetProperty(ref _timingConfigurationMaxCycles, value);
        }

        private double _timingConfigurationObserverTimeSpan = 120;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorRuntimeObserverTimespan, typeof(double))]
		public double TimingConfigurationObserverTimeSpan
        {
            get => _timingConfigurationObserverTimeSpan;
            set => SetProperty(ref _timingConfigurationObserverTimeSpan, value);
        }

        private double _timingConfigurationObserverRetentionTime = 15;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorRuntimeObserverRetention, typeof(double))]
		public double TimingConfigurationObserverRetentionTime
        {
            get => _timingConfigurationObserverRetentionTime;
            set => SetProperty(ref _timingConfigurationObserverRetentionTime, value);
        }

        private ControlUnitAtexStateValue _timingConfigurationMotorAtexEnable = ControlUnitAtexStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorAtexEnable, typeof(ControlUnitAtexStateValue))]
		public ControlUnitAtexStateValue TimingConfigurationMotorAtexEnable
        {
            get => _timingConfigurationMotorAtexEnable;
            set => SetProperty(ref _timingConfigurationMotorAtexEnable, value);
        }

        private double _timingConfigurationAtexTimeout = 60;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorAtexTimeout, typeof(double))]
		public double TimingConfigurationAtexTimeout
        {
            get => _timingConfigurationAtexTimeout;
            set => SetProperty(ref _timingConfigurationAtexTimeout, value);
        }

        private double _timingConfigurationAtexLockTimeout = 30;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorAtexTimeoutLock, typeof(double))]
		public double TimingConfigurationAtexLockTimeout
        {
            get => _timingConfigurationAtexLockTimeout;
            set => SetProperty(ref _timingConfigurationAtexLockTimeout, value);
        }

		private ControlUnitMaintenanceWarningStateValue _maintenanceWarningIsWarningEnable = ControlUnitMaintenanceWarningStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemMaintenanceWarningEnable, typeof(ControlUnitMaintenanceWarningStateValue))]
		public ControlUnitMaintenanceWarningStateValue MaintenanceWarningIsWarningEnable
		{
			get => _maintenanceWarningIsWarningEnable;
			set => SetProperty(ref _maintenanceWarningIsWarningEnable, value);
		}

		private double _maintenanceWarningIntervalBetweenWarnings = 12;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemMaintenanceWarningInterval, typeof(double))]
		public double MaintenanceWarningIntervalBetweenWarnings
		{
			get => _maintenanceWarningIntervalBetweenWarnings;
			set => SetProperty(ref _maintenanceWarningIntervalBetweenWarnings, value);
		}

		private ControlUnitCustomRelayConfigurationValue _relayConfigurationRelay0Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_WARNING;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.M2mCustomRelay0Configuration, typeof(ControlUnitCustomRelayConfigurationValue))]
		public ControlUnitCustomRelayConfigurationValue RelayConfigurationRelay0Value
		{
			get => _relayConfigurationRelay0Value;
			set => SetProperty(ref _relayConfigurationRelay0Value, value);
		}

		private ControlUnitCustomRelayConfigurationValue _relayConfigurationRelay1Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_ALARM_MOTOR_0_1;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.M2mCustomRelay1Configuration, typeof(ControlUnitCustomRelayConfigurationValue))]
		public ControlUnitCustomRelayConfigurationValue RelayConfigurationRelay1Value
		{
			get => _relayConfigurationRelay1Value;
			set => SetProperty(ref _relayConfigurationRelay1Value, value);
		}

		private ControlUnitCustomRelayConfigurationValue _relayConfigurationRelay2Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_SERVICE;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.M2mCustomRelay2Configuration, typeof(ControlUnitCustomRelayConfigurationValue))]
		public ControlUnitCustomRelayConfigurationValue RelayConfigurationRelay2Value
		{
			get => _relayConfigurationRelay2Value;
			set => SetProperty(ref _relayConfigurationRelay2Value, value);
		}
	}
}
