﻿using EmmeApp.Common.Enum;

namespace EmmeApp.Common.PageModels.External
{
	public class ValueTExternalModel
	{
		public string Text { get; set; }
		public long Number { get; set; }
		public ExternalValueOneofCaseValue ValueCase { get; set; }
	}
}
