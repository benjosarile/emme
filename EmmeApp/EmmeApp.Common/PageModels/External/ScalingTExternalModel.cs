﻿namespace EmmeApp.Common.PageModels.External
{
    public class ScalingTExternalModel
	{
		public long Max { get; set; }
		public long Min { get; set; }
		public double Step { get; set; }
	}
}
