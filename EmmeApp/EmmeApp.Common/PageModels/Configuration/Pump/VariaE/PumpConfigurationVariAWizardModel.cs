﻿using EmmeApp.Common.Attributes;
using EmmeApp.Common.Enum;
using Prism.Mvvm;
using System;

namespace EmmeApp.Common.PageModels.Configuration
{
    public class PumpConfigurationVariAWizardModel : BindableBase
    {
        private Guid _selectedDeviceUuid;
        public Guid SelectedDeviceUuid
        {
            get => _selectedDeviceUuid;
            set => SetProperty(ref _selectedDeviceUuid, value);
        }

        private bool _isParameterChanged;
        public bool IsParameterChanged
        {
            get => _isParameterChanged;
            set => SetProperty(ref _isParameterChanged, value);
        }

        private PumpControlTypeValue _controlType = PumpControlTypeValue.ConstantPressure;
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.ControlType, typeof(PumpControlTypeValue))]
        public PumpControlTypeValue ControlType
        {
            get => _controlType;
            set => SetProperty(ref _controlType, value);
        }

        private PumpTypeOfOperationValue _typeOfOperation = PumpTypeOfOperationValue.ON;
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.TypeOfOperation, typeof(PumpTypeOfOperationValue))]
        public PumpTypeOfOperationValue TypeOfOperation
        {
            get => _typeOfOperation;
            set => SetProperty(ref _typeOfOperation, value);
        }

        private PumpKeyLockValue _keyLock = PumpKeyLockValue.KeysNotLocked;
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.KeyLock, typeof(PumpKeyLockValue))]
        public PumpKeyLockValue KeyLock
        {
            get => _keyLock;
            set => SetProperty(ref _keyLock, value);
        }

        private double _pumpNumber = 1;
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.PumpNumber, typeof(double))]
        public double PumpNumber
        {
            get => _pumpNumber;
            set => SetProperty(ref _pumpNumber, value);
        }

        private double _nominalValue = 85;
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.NominalValue, typeof(double))]
        public double NominalValue
        {
            get => _nominalValue;
            set => SetProperty(ref _nominalValue, value);
        }

        private long _time;
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.Time, typeof(long))]
        public long Time
        {
            get => _time;
            set => SetProperty(ref _time, value);
        }


        private PumpSignalOriginValue _signalOrigin = PumpSignalOriginValue.Pump;
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.SignalOrigin, typeof(PumpSignalOriginValue), false)]
        public PumpSignalOriginValue SignalOrigin
        {
            get => _signalOrigin;
            set => SetProperty(ref _signalOrigin, value);
        }

        private string _systemName;
        public string SystemName
        {
            get => _systemName;
            set => SetProperty(ref _systemName, value);
        }

        private string _groupName;
        public string GroupName
        {
            get => _groupName;
            set => SetProperty(ref _groupName, value);
        }

        private string _sytemAddress;
        public string SystemAddress
        {
            get => _sytemAddress;
            set => SetProperty(ref _sytemAddress, value);
        }

        private string _contactName;
        public string ContactName
        {
            get => _contactName;
            set => SetProperty(ref _contactName, value);
        }

        private string _phone;
        public string Phone
        {
            get => _phone;
            set => SetProperty(ref _phone, value);
        }

        private string _emailAddress;
        public string EmailAddress
        {
            get => _emailAddress;
            set => SetProperty(ref _emailAddress, value);
        }

        private string _remarks;
        public string Remarks
        {
            get => _remarks;
            set => SetProperty(ref _remarks, value);
        }
    }
}
