﻿using Prism.Mvvm;

namespace EmmeApp.Common.PageModels.Log
{
    public class ControlUnitLogModel : BindableBase
    {
		private int _logId;
		public int LogId
		{
			get => _logId;
			set => SetProperty(ref _logId, value);
		}

		private long _appearanceDateTime;
        public long AppearanceDateTime
        {
            get => _appearanceDateTime;
            set => SetProperty(ref _appearanceDateTime, value);
        }

        private System.Enum _typeValue;
        public System.Enum TypeValue
        {
            get => _typeValue;
            set => SetProperty(ref _typeValue, value);
        }

    }
}
