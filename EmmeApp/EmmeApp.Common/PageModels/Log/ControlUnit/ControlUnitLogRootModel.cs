﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;

namespace EmmeApp.Common.PageModels.Log
{
    public class ControlUnitLogRootModel : BindableBase
    {
		private Guid _selectedDeviceUuid;
		public Guid SelectedDeviceUuid
		{
			get => _selectedDeviceUuid;
			set => SetProperty(ref _selectedDeviceUuid, value);
		}

		private List<ControlUnitLogModel> _eventControlUnitLogList = new List<ControlUnitLogModel>();
        public List<ControlUnitLogModel> EventControlUnitLogList
        {
            get => _eventControlUnitLogList;
            set => SetProperty(ref _eventControlUnitLogList, value);
        }
    }
}
