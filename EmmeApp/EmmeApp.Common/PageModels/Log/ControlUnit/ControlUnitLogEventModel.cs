﻿using Prism.Mvvm;
using System.Collections.Generic;

namespace EmmeApp.Common.PageModels.Log
{
    public class ControlUnitLogEventModel : BindableBase
    {
        private List<ControlUnitLogModel> _eventControlUnitLogList = new List<ControlUnitLogModel>();
        public List<ControlUnitLogModel> EventControlUnitLogList
        {
            get => _eventControlUnitLogList;
            set => SetProperty(ref _eventControlUnitLogList, value);
        }
    }
}
