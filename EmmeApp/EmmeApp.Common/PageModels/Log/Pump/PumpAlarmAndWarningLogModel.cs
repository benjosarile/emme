﻿using EmmeApp.Common.Enum;
using Prism.Commands;
using Prism.Mvvm;

namespace EmmeApp.Common.PageModels.Log
{
    public class PumpAlarmAndWarningLogModel : BindableBase
	{

		private long _appearanceDateTime;
		public long AppearanceDateTime
		{
			get => _appearanceDateTime;
			set => SetProperty(ref _appearanceDateTime, value);
		}


		private long _disappearanceDateTime;
		public long DisappearanceDateTime
		{
			get => _disappearanceDateTime;
			set => SetProperty(ref _disappearanceDateTime, value);
		}

		private PumpCurrentAlarmValue _alarmLogStatus = PumpCurrentAlarmValue.No_Fault;
		public PumpCurrentAlarmValue AlarmLogStatus
		{
			get => _alarmLogStatus;
			set => SetProperty(ref _alarmLogStatus, value);
		}

		private PumpCurrentWarningValue _warningLogStatus = PumpCurrentWarningValue.No_Fault;
		public PumpCurrentWarningValue WarningLogStatus
		{
			get => _warningLogStatus;
			set => SetProperty(ref _warningLogStatus, value);
		}

		private bool _hasHelpText;
		public bool HasHelpText
		{
			get => _hasHelpText;
			set => SetProperty(ref _hasHelpText, value);
		}

		public string Title { get; set; }
		public string Measure { get; set; }
		public string Cause { get; set; }

		private DelegateCommand _viewHelpTextCommand;
		public DelegateCommand ViewHelpTextCommand
		{
			get => _viewHelpTextCommand;
			set => SetProperty(ref _viewHelpTextCommand, value);
		}
	}
}
