﻿using Prism.Mvvm;
using System.Collections.Generic;

namespace EmmeApp.Common.PageModels.Log
{
    public class PumpLogWarningModel : BindableBase
    {
        private List<PumpAlarmAndWarningLogModel> _warningPumpLogList = new List<PumpAlarmAndWarningLogModel>();
        public List<PumpAlarmAndWarningLogModel> WarningPumpLogList
        {
            get => _warningPumpLogList;
            set => SetProperty(ref _warningPumpLogList, value);
        }
    }
}
