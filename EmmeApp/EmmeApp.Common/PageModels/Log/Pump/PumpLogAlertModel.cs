﻿using Prism.Mvvm;
using System.Collections.Generic;

namespace EmmeApp.Common.PageModels.Log
{
    public class PumpLogAlertModel : BindableBase
    {
        private List<PumpAlarmAndWarningLogModel> _alarmPumpLogList = new List<PumpAlarmAndWarningLogModel>();
        public List<PumpAlarmAndWarningLogModel> AlarmPumpLogList
        {
            get => _alarmPumpLogList;
            set => SetProperty(ref _alarmPumpLogList, value);
        }
    }
}
