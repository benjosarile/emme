﻿using EmmeApp.Common.Enum;
using Prism.Mvvm;
using System;
using System.Collections.Generic;

namespace EmmeApp.Common.PageModels.Log
{
	public class PumpLogRootModel : BindableBase
    {
		private ExternalComProfileValue _profileId;
		public ExternalComProfileValue ProfileId
		{
			get => _profileId;
			set => SetProperty(ref _profileId, value);
		}

		private Guid _selectedDeviceUuid;
		public Guid SelectedDeviceUuid
		{
			get => _selectedDeviceUuid;
			set => SetProperty(ref _selectedDeviceUuid, value);
		}

		private List<PumpAlarmAndWarningLogModel> _alarmPumpLogList = new List<PumpAlarmAndWarningLogModel>();
        public List<PumpAlarmAndWarningLogModel> AlarmPumpLogList
        {
            get => _alarmPumpLogList;
            set => SetProperty(ref _alarmPumpLogList, value);
        }

        private List<PumpAlarmAndWarningLogModel> _warningPumpLogList = new List<PumpAlarmAndWarningLogModel>();
        public List<PumpAlarmAndWarningLogModel> WarningPumpLogList
        {
            get => _warningPumpLogList;
            set => SetProperty(ref _warningPumpLogList, value);
        }
    }
}
