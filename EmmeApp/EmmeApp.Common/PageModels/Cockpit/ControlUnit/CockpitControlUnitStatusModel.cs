﻿using EmmeApp.Common.Enum;
using Prism.Mvvm;

namespace EmmeApp.Common.PageModels.Cockpit
{
    public class CockpitControlUnitStatusModel : BindableBase
    {
        private double _cmnProductID = 1;
        public double CmnProductID
        {
            get => _cmnProductID;
            set => SetProperty(ref _cmnProductID, value);
        }

        private double _cmnSystemParameterProfileID = 1;
        public double CmnSystemParameterProfileID
        {
            get => _cmnSystemParameterProfileID;
            set => SetProperty(ref _cmnSystemParameterProfileID, value);
        }

        private double _cmnSystemEventProfileID = 1;
        public double CmnSystemEventProfileID
        {
            get => _cmnSystemEventProfileID;
            set => SetProperty(ref _cmnSystemEventProfileID, value);
        }

        private string _cmnSystemFirmwareVersion = "00.00.01";
        public string CmnSystemFirmwareVersion
        {
            get => _cmnSystemFirmwareVersion;
            set => SetProperty(ref _cmnSystemFirmwareVersion, value);
        }

        private string _cmnSystemFirmwareSignature = "0xAAAA5555";
        public string CmnSystemFirmwareSignature
        {
            get => _cmnSystemFirmwareSignature;
            set => SetProperty(ref _cmnSystemFirmwareSignature, value);
        }

        private double _logSize = 1500;
        public double LogSize
        {
            get => _logSize;
            set => SetProperty(ref _logSize, value);
        }

        private double _logCount = 0;
        public double LogCount
        {
            get => _logCount;
            set => SetProperty(ref _logCount, value);
        }

        private double _logIndex = 0;
        public double LogIndex
        {
            get => _logIndex;
            set => SetProperty(ref _logIndex, value);
        }

        private string _rtosVersion = "V10.0.0";
        public string RTOSVersion
        {
            get => _rtosVersion;
            set => SetProperty(ref _rtosVersion, value);
        }

        private string _halVersion = "V1.7.2";
        public string HALVersion
        {
            get => _halVersion;
            set => SetProperty(ref _halVersion, value);
        }

        private string _cmsisVersion = "V4.30";
        public string CMSISVersion
        {
            get => _cmsisVersion;
            set => SetProperty(ref _cmsisVersion, value);
        }

        private string _bdlcVersion = "V00.00.01";
        public string BDLCVersion
        {
            get => _bdlcVersion;
            set => SetProperty(ref _bdlcVersion, value);
        }

        private string _factoryProductName = "NomBox-42";
        public string FactoryProductName
        {
            get => _factoryProductName;
            set => SetProperty(ref _factoryProductName, value);
        }

        private string _factoryItemNumber = "123456789";
        public string FactoryItemNumber
        {
            get => _factoryItemNumber;
            set => SetProperty(ref _factoryItemNumber, value);
        }

        private string _factorySerialNumber = "123456789";
        public string FactorySerialNumber
        {
            get => _factorySerialNumber;
            set => SetProperty(ref _factorySerialNumber, value);
        }

        private string _factoryAssemblyTime = "1549271462";
        public string FactoryAssemblyTime
        {
            get => _factoryAssemblyTime;
            set => SetProperty(ref _factoryAssemblyTime, value);
        }

        private double _factoryOrderNumber = 0;
        public double FactoryOrderNumber
        {
            get => _factoryOrderNumber;
            set => SetProperty(ref _factoryOrderNumber, value);
        }

        private string _customerName = "Max Mustermann";
        public string CustomerName
        {
            get => _customerName;
            set => SetProperty(ref _customerName, value);
        }

        private string _customerPhoneNumber = "+41 31 720 90 00";
        public string CustomerPhoneNumber
        {
            get => _customerPhoneNumber;
            set => SetProperty(ref _customerPhoneNumber, value);
        }

        private string _customerStreet = "Suedstrasse 10";
        public string CustomerStreet
        {
            get => _customerStreet;
            set => SetProperty(ref _customerStreet, value);
        }

        private string _customerCity = "Muensingen";
        public string CustomerCity
        {
            get => _customerCity;
            set => SetProperty(ref _customerCity, value);
        }

        private string _customerZip = "3110";
        public string CustomerZip
        {
            get => _customerZip;
            set => SetProperty(ref _customerZip, value);
        }

        private string _customerCountry = "Switzerland";
        public string CustomerCountry
        {
            get => _customerCountry;
            set => SetProperty(ref _customerCountry, value);
        }

        private string _systemName = "Biral Testsystem";
        public string SystemName
        {
            get => _systemName;
            set => SetProperty(ref _systemName, value);
        }

        private string _motorType0 = "Motor 0";
        public string MotorType0
        {
            get => _motorType0;
            set => SetProperty(ref _motorType0, value);
        }

        private string _motorType1 = "Motor 1";
        public string MotorType1
        {
            get => _motorType1;
            set => SetProperty(ref _motorType1, value);
        }

        private ControlUnitSystemTypeValue _systemType = ControlUnitSystemTypeValue.NotConfigured;
        public ControlUnitSystemTypeValue SystemType
        {
            get => _systemType;
            set => SetProperty(ref _systemType, value);
        }

        private ControlUnitSystemDoubleMotorValue _systemDoubleMotor = ControlUnitSystemDoubleMotorValue.TwoPumps;
        public ControlUnitSystemDoubleMotorValue SystemDoubleMotor
        {
            get => _systemDoubleMotor;
            set => SetProperty(ref _systemDoubleMotor, value);
        }

        private ControlUnitMotorModeValue _systemMotorMode0 = ControlUnitMotorModeValue.Auto;
        public ControlUnitMotorModeValue SystemMotorMode0
        {
            get => _systemMotorMode0;
            set => SetProperty(ref _systemMotorMode0, value);
        }

        private ControlUnitMotorModeValue _systemMotorMode1 = ControlUnitMotorModeValue.Auto;
        public ControlUnitMotorModeValue SystemMotorMode1
        {
            get => _systemMotorMode1;
            set => SetProperty(ref _systemMotorMode1, value);
        }

        private bool _maintenanceWarningEnable = false;
        public bool MaintenanceWarningEnable
        {
            get => _maintenanceWarningEnable;
            set => SetProperty(ref _maintenanceWarningEnable, value);
        }

        private double _maintenanceWarningTrigger = 0;
        public double MaintenanceWarningTrigger
        {
            get => _maintenanceWarningTrigger;
            set => SetProperty(ref _maintenanceWarningTrigger, value);
        }

        private double _maintenanceWarningInterval = 12;
        public double MaintenanceWarningInterval
        {
            get => _maintenanceWarningInterval;
            set => SetProperty(ref _maintenanceWarningInterval, value);
        }

        private long _systemUnixTimestamp = 1549271681;
        public long SystemUnixTimestamp
        {
            get => _systemUnixTimestamp;
            set => SetProperty(ref _systemUnixTimestamp, value);
        }

        private bool _systemVirginIsActive = false;
        public bool SystemVirginIsActive
        {
            get => _systemVirginIsActive;
            set => SetProperty(ref _systemVirginIsActive, value);
        }

        private ControlUnitSystemFailureValue _systemFailure = ControlUnitSystemFailureValue.CMN_FAILURE_NO_FAILURE;
        public ControlUnitSystemFailureValue SystemFailure
        {
            get => _systemFailure;
            set => SetProperty(ref _systemFailure, value);
        }

        private ControlUnitSystemAlarmValue _systemAlarm = ControlUnitSystemAlarmValue.CMN_ALARM_NO_ALARM;
        public ControlUnitSystemAlarmValue SystemAlarm
        {
            get => _systemAlarm;
            set => SetProperty(ref _systemAlarm, value);
        }

        private ControlUnitSystemWarningValue _systemWarning = ControlUnitSystemWarningValue.CMN_WARNING_NO_WARNING;
        public ControlUnitSystemWarningValue SystemWarning
        {
            get => _systemWarning;
            set => SetProperty(ref _systemWarning, value);
        }

        private ControlUnitSystemStatusValue _systemStatus = ControlUnitSystemStatusValue.CMN_SYSTEM_STATUS_NOT_CONFIGURED;
        public ControlUnitSystemStatusValue SystemStatus
        {
            get => _systemStatus;
            set => SetProperty(ref _systemStatus, value);
        }

        private ControlUnitMotorModeValue _motorModeMotor0 = ControlUnitMotorModeValue.Auto;
        public ControlUnitMotorModeValue MotorModeMotor0
        {
            get => _motorModeMotor0;
            set => SetProperty(ref _motorModeMotor0, value);
        }

        private double _motorRatedCurrentMotor0 = 50;
        public double MotorRatedCurrentMotor0
        {
            get => _motorRatedCurrentMotor0;
            set => SetProperty(ref _motorRatedCurrentMotor0, value);
        }

        private double _motorPhase1CurrentMotor0 = 122;
        public double MotorPhase1CurrentMotor0
        {
            get => _motorPhase1CurrentMotor0;
            set => SetProperty(ref _motorPhase1CurrentMotor0, value);
        }

        private double _motorPhase3CurrentMotor0 = 0;
        public double MotorPhase3CurrentMotor0
        {
            get => _motorPhase3CurrentMotor0;
            set => SetProperty(ref _motorPhase3CurrentMotor0, value);
        }

        private ControlUnitMotorStatusValue _motorStatusMotor0 = ControlUnitMotorStatusValue.CMN_MOTOR_MODE_RUN;
        public ControlUnitMotorStatusValue MotorStatusMotor0
        {
            get => _motorStatusMotor0;
            set => SetProperty(ref _motorStatusMotor0, value);
        }

        private ControlUnitThermalProtectionStateValue _motorWSKMotor0Enable = ControlUnitThermalProtectionStateValue.Enabled;
        public ControlUnitThermalProtectionStateValue MotorWSKMotor0Enable
        {
            get => _motorWSKMotor0Enable;
            set => SetProperty(ref _motorWSKMotor0Enable, value);
        }

        private CockpitControlUnitMotorProtectionStateValue _motorProtectionMotor0Enable = CockpitControlUnitMotorProtectionStateValue.Disabled;
        public CockpitControlUnitMotorProtectionStateValue MotorProtectionMotor0Enable
        {
            get => _motorProtectionMotor0Enable;
            set => SetProperty(ref _motorProtectionMotor0Enable, value);
        }

        private ControlUnitMotorModeValue _motorModeMotor1 = ControlUnitMotorModeValue.Auto;
        public ControlUnitMotorModeValue MotorModeMotor1
        {
            get => _motorModeMotor1;
            set => SetProperty(ref _motorModeMotor1, value);
        }

        private double _motorRatedCurrentMotor1 = 42;
        public double MotorRatedCurrentMotor1
        {
            get => _motorRatedCurrentMotor1;
            set => SetProperty(ref _motorRatedCurrentMotor1, value);
        }

        private double _motorPhase1CurrentMotor1 = 650;
        public double MotorPhase1CurrentMotor1
        {
            get => _motorPhase1CurrentMotor1;
            set => SetProperty(ref _motorPhase1CurrentMotor1, value);
        }

        private double _motorPhase3CurrentMotor1 = 500;
        public double MotorPhase3CurrentMotor1
        {
            get => _motorPhase3CurrentMotor1;
            set => SetProperty(ref _motorPhase3CurrentMotor1, value);
        }

        private ControlUnitMotorStatusValue _motorStatusMotor1 = ControlUnitMotorStatusValue.CMN_MOTOR_MODE_RUN;
        public ControlUnitMotorStatusValue MotorStatusMotor1
        {
            get => _motorStatusMotor1;
            set => SetProperty(ref _motorStatusMotor1, value);
        }

        private ControlUnitThermalProtectionStateValue _motorWSKMotor1Enable = ControlUnitThermalProtectionStateValue.Enabled;
        public ControlUnitThermalProtectionStateValue MotorWSKMotor1Enable
        {
            get => _motorWSKMotor1Enable;
            set => SetProperty(ref _motorWSKMotor1Enable, value);
        }

        private CockpitControlUnitMotorProtectionStateValue _motorProtectionMotor1Enable = CockpitControlUnitMotorProtectionStateValue.Disabled;
        public CockpitControlUnitMotorProtectionStateValue MotorProtectionMotor1Enable
        {
            get => _motorProtectionMotor1Enable;
            set => SetProperty(ref _motorProtectionMotor1Enable, value);
        }

        private ControlUnitOskStateValue _motorOSKEnable = ControlUnitOskStateValue.Disabled;
        public ControlUnitOskStateValue MotorOSKEnable
        {
            get => _motorOSKEnable;
            set => SetProperty(ref _motorOSKEnable, value);
        }

        private ControlUnitDelayedOffStateValue _motorDelayedOffEnable = ControlUnitDelayedOffStateValue.Disabled;
        public ControlUnitDelayedOffStateValue MotorDelayedOffEnable
        {
            get => _motorDelayedOffEnable;
            set => SetProperty(ref _motorDelayedOffEnable, value);
        }

        private double _motorDelayedOffTime = 5;
        public double MotorDelayedOffTime
        {
            get => _motorDelayedOffTime;
            set => SetProperty(ref _motorDelayedOffTime, value);
        }

        private ControlUnitForceRunningStateValue _motorForceRunningEnable = ControlUnitForceRunningStateValue.Disabled;
        public ControlUnitForceRunningStateValue MotorForceRunningEnable
        {
            get => _motorForceRunningEnable;
            set => SetProperty(ref _motorForceRunningEnable, value);
        }

        private double _motorForceRetentionTime = 30;
        public double MotorForceRetentionTime
        {
            get => _motorForceRetentionTime;
            set => SetProperty(ref _motorForceRetentionTime, value);
        }

        private double _motorForceRunningOnTime = 5;
        public double MotorForceRunningOnTime
        {
            get => _motorForceRunningOnTime;
            set => SetProperty(ref _motorForceRunningOnTime, value);
        }

        private ControlUnitDelayedOnStateValue _motorDelayedOnEnable = ControlUnitDelayedOnStateValue.Disabled;
        public ControlUnitDelayedOnStateValue MotorDelayedOnEnable
        {
            get => _motorDelayedOnEnable;
            set => SetProperty(ref _motorDelayedOnEnable, value);
        }

        private double _motorDelayedOnTime = 5;
        public double MotorDelayedOnTime
        {
            get => _motorDelayedOnTime;
            set => SetProperty(ref _motorDelayedOnTime, value);
        }

        private CockpitControlUnitRuntimeObserverStateValue _motorRuntimeObserverEnable = CockpitControlUnitRuntimeObserverStateValue.Disabled;
        public CockpitControlUnitRuntimeObserverStateValue MotorRuntimeObserverEnable
        {
            get => _motorRuntimeObserverEnable;
            set => SetProperty(ref _motorRuntimeObserverEnable, value);
        }

        private double _motorRuntimeObserverMaxCycle = 3;
        public double MotorRuntimeObserverMaxCycle
        {
            get => _motorRuntimeObserverMaxCycle;
            set => SetProperty(ref _motorRuntimeObserverMaxCycle, value);
        }

        private double _motorRuntimeObserverTimespan = 120;
        public double MotorRuntimeObserverTimespan
        {
            get => _motorRuntimeObserverTimespan;
            set => SetProperty(ref _motorRuntimeObserverTimespan, value);
        }

        private double _motorRuntimeObserverRetention = 15;
        public double MotorRuntimeObserverRetention
        {
            get => _motorRuntimeObserverRetention;
            set => SetProperty(ref _motorRuntimeObserverRetention, value);
        }

        private ControlUnitAtexStateValue _motorAtexEnable = ControlUnitAtexStateValue.Disabled;
        public ControlUnitAtexStateValue MotorAtexEnable
        {
            get => _motorAtexEnable;
            set => SetProperty(ref _motorAtexEnable, value);
        }

        private double _motorAtexTimeout = 60;
        public double MotorAtexTimeout
        {
            get => _motorAtexTimeout;
            set => SetProperty(ref _motorAtexTimeout, value);
        }

        private double _motorAtexTimeoutLock = 30;
        public double MotorAtexTimeoutLock
        {
            get => _motorAtexTimeoutLock;
            set => SetProperty(ref _motorAtexTimeoutLock, value);
        }

        private LevelProbeConfigurationValue _levelProbeConfiguration = LevelProbeConfigurationValue.ThreeLevelswitches;
        public LevelProbeConfigurationValue LevelProbeConfiguration
        {
            get => _levelProbeConfiguration;
            set => SetProperty(ref _levelProbeConfiguration, value);
        }

        private ControlUnitLevelActiveValue _levelActiveLevel = ControlUnitLevelActiveValue.CMN_LEVEL_0;
        public ControlUnitLevelActiveValue LevelActiveLevel
        {
            get => _levelActiveLevel;
            set => SetProperty(ref _levelActiveLevel, value);
        }

        private ControlUnitThresholdStatusValue _levelThreshold0Status = ControlUnitThresholdStatusValue.CMN_THRESHOLD_STATUS_NOT_ACTIVE;
        public ControlUnitThresholdStatusValue LevelThreshold0Status
        {
            get => _levelThreshold0Status;
            set => SetProperty(ref _levelThreshold0Status, value);
        }

        private ControlUnitThresholdStatusValue _levelThreshold1Status = ControlUnitThresholdStatusValue.CMN_THRESHOLD_STATUS_NOT_ACTIVE;
        public ControlUnitThresholdStatusValue LevelThreshold1Status
        {
            get => _levelThreshold1Status;
            set => SetProperty(ref _levelThreshold1Status, value);
        }

        private ControlUnitThresholdStatusValue _levelThreshold2Status = ControlUnitThresholdStatusValue.CMN_THRESHOLD_STATUS_NOT_ACTIVE;
        public ControlUnitThresholdStatusValue LevelThreshold2Status
        {
            get => _levelThreshold2Status;
            set => SetProperty(ref _levelThreshold2Status, value);
        }

        private ControlUnitThresholdStatusValue _levelThreshold3Status = ControlUnitThresholdStatusValue.CMN_THRESHOLD_STATUS_NOT_ACTIVE;
        public ControlUnitThresholdStatusValue LevelThreshold3Status
        {
            get => _levelThreshold3Status;
            set => SetProperty(ref _levelThreshold3Status, value);
        }

        private ControlUnitLevelswitchStatusValue _levelswitch0Status = ControlUnitLevelswitchStatusValue.CMN_LEVEL_LEVELSWITCH_STATE_OK_HANGING;
        public ControlUnitLevelswitchStatusValue Levelswitch0Status
        {
            get => _levelswitch0Status;
            set => SetProperty(ref _levelswitch0Status, value);
        }

        private ControlUnitLevelswitchStatusValue _levelswitch1Status = ControlUnitLevelswitchStatusValue.CMN_LEVEL_LEVELSWITCH_STATE_OK_HANGING;
        public ControlUnitLevelswitchStatusValue Levelswitch1Status
        {
            get => _levelswitch1Status;
            set => SetProperty(ref _levelswitch1Status, value);
        }

        private ControlUnitLevelswitchStatusValue _levelswitch2Status = ControlUnitLevelswitchStatusValue.CMN_LEVEL_LEVELSWITCH_STATE_OK_HANGING;
        public ControlUnitLevelswitchStatusValue Levelswitch2Status
        {
            get => _levelswitch2Status;
            set => SetProperty(ref _levelswitch2Status, value);
        }

        private ControlUnitLevelswitchStatusValue _levelswitch3Status = ControlUnitLevelswitchStatusValue.CMN_LEVEL_LEVELSWITCH_STATE_OK_HANGING;
        public ControlUnitLevelswitchStatusValue Levelswitch3Status
        {
            get => _levelswitch3Status;
            set => SetProperty(ref _levelswitch3Status, value);
        }

        private ControlUnitLevelAnalogStatusValue _levelAnalogStatus = ControlUnitLevelAnalogStatusValue.CMN_LEVEL_ANALOG_STATE_OK;
        public ControlUnitLevelAnalogStatusValue LevelAnalogStatus
        {
            get => _levelAnalogStatus;
            set => SetProperty(ref _levelAnalogStatus, value);
        }

        private double _levelAnalogActiveCurrent = 40;
        public double LevelAnalogActiveCurrent
        {
            get => _levelAnalogActiveCurrent;
            set => SetProperty(ref _levelAnalogActiveCurrent, value);
        }

        private double _levelAnalogThreshold0Current = 40;
        public double LevelAnalogThreshold0Current
        {
            get => _levelAnalogThreshold0Current;
            set => SetProperty(ref _levelAnalogThreshold0Current, value);
        }

        private double _levelAnalogThreshold1Current = 40;
        public double LevelAnalogThreshold1Current
        {
            get => _levelAnalogThreshold1Current;
            set => SetProperty(ref _levelAnalogThreshold1Current, value);
        }

        private double _levelAnalogThreshold2Current = 40;
        public double LevelAnalogThreshold2Current
        {
            get => _levelAnalogThreshold2Current;
            set => SetProperty(ref _levelAnalogThreshold2Current, value);
        }

        private CockpitControlUnitRelayStateValue _m2mOperatingRelay = CockpitControlUnitRelayStateValue.NotActive;
        public CockpitControlUnitRelayStateValue M2mOperatingRelay
        {
            get => _m2mOperatingRelay;
            set => SetProperty(ref _m2mOperatingRelay, value);
        }

        private CockpitControlUnitRelayStateValue _m2mCustomRelay0 = CockpitControlUnitRelayStateValue.NotActive;
        public CockpitControlUnitRelayStateValue M2mCustomRelay0
        {
            get => _m2mCustomRelay0;
            set => SetProperty(ref _m2mCustomRelay0, value);
        }

        private ControlUnitCustomRelayConfigurationValue _m2mCustomRelay0Configuration = ControlUnitCustomRelayConfigurationValue.CMN_M2M_WARNING;
        public ControlUnitCustomRelayConfigurationValue M2mCustomRelay0Configuration
        {
            get => _m2mCustomRelay0Configuration;
            set => SetProperty(ref _m2mCustomRelay0Configuration, value);
        }

        private CockpitControlUnitRelayStateValue _m2mCustomRelay1 = CockpitControlUnitRelayStateValue.NotActive;
        public CockpitControlUnitRelayStateValue M2mCustomRelay1
        {
            get => _m2mCustomRelay1;
            set => SetProperty(ref _m2mCustomRelay1, value);
        }

        private ControlUnitCustomRelayConfigurationValue _m2mCustomRelay1Configuration = ControlUnitCustomRelayConfigurationValue.CMN_M2M_LEVEL_HIGH;
        public ControlUnitCustomRelayConfigurationValue M2mCustomRelay1Configuration
        {
            get => _m2mCustomRelay1Configuration;
            set => SetProperty(ref _m2mCustomRelay1Configuration, value);
        }

        private CockpitControlUnitRelayStateValue _m2mCustomRelay2 = CockpitControlUnitRelayStateValue.NotActive;
        public CockpitControlUnitRelayStateValue M2mCustomRelay2
        {
            get => _m2mCustomRelay2;
            set => SetProperty(ref _m2mCustomRelay2, value);
        }

        private ControlUnitCustomRelayConfigurationValue _m2mCustomRelay2Configuration = ControlUnitCustomRelayConfigurationValue.CMN_M2M_ALARM_MOTOR_0;
        public ControlUnitCustomRelayConfigurationValue M2mCustomRelay2Configuration
        {
            get => _m2mCustomRelay2Configuration;
            set => SetProperty(ref _m2mCustomRelay2Configuration, value);
        }

        private CockpitControlUnitExternalOffStateValue _m2mExternalOffActive = CockpitControlUnitExternalOffStateValue.NotActive;
        public CockpitControlUnitExternalOffStateValue M2mExternalOffActive
        {
            get => _m2mExternalOffActive;
            set => SetProperty(ref _m2mExternalOffActive, value);
        }

        private ControlUnitDatalinkStatusValue _datalinkRemoteStatus = ControlUnitDatalinkStatusValue.CMN_DATALINK_STATUS_OFF;
        public ControlUnitDatalinkStatusValue DatalinkRemoteStatus
        {
            get => _datalinkRemoteStatus;
            set => SetProperty(ref _datalinkRemoteStatus, value);
        }

        private ControlUnitDatalinkStatusValue _datalinkServiceStatus = ControlUnitDatalinkStatusValue.CMN_DATALINK_STATUS_OFF;
        public ControlUnitDatalinkStatusValue DatalinkServiceStatus
        {
            get => _datalinkServiceStatus;
            set => SetProperty(ref _datalinkServiceStatus, value);
        }

        private double _datalinkRemotePendingTimeout = 30;
        public double DatalinkRemotePendingTimeout
        {
            get => _datalinkRemotePendingTimeout;
            set => SetProperty(ref _datalinkRemotePendingTimeout, value);
        }

        private double _datalinkConnectionTimeout = 5;
        public double DatalinkConnectionTimeout
        {
            get => _datalinkConnectionTimeout;
            set => SetProperty(ref _datalinkConnectionTimeout, value);
        }

        private double _datalinkResponseTimeout = 5;
        public double DatalinkResponseTimeout
        {
            get => _datalinkResponseTimeout;
            set => SetProperty(ref _datalinkResponseTimeout, value);
        }

        private CockpitControlUnitButtonPressedStateValue _hmiBtnMotor0ManualIn = CockpitControlUnitButtonPressedStateValue.NotPressed;
        public CockpitControlUnitButtonPressedStateValue HmiBtnMotor0ManualIn
        {
            get => _hmiBtnMotor0ManualIn;
            set => SetProperty(ref _hmiBtnMotor0ManualIn, value);
        }

        private CockpitControlUnitButtonPressedStateValue _hmiBtnMotor0AutoOffIn = CockpitControlUnitButtonPressedStateValue.NotPressed;
        public CockpitControlUnitButtonPressedStateValue HmiBtnMotor0AutoOffIn
        {
            get => _hmiBtnMotor0AutoOffIn;
            set => SetProperty(ref _hmiBtnMotor0AutoOffIn, value);
        }

        private CockpitControlUnitButtonPressedStateValue _hmiBtnMotor1ManualIn = CockpitControlUnitButtonPressedStateValue.NotPressed;
        public CockpitControlUnitButtonPressedStateValue HmiBtnMotor1ManualIn
        {
            get => _hmiBtnMotor1ManualIn;
            set => SetProperty(ref _hmiBtnMotor1ManualIn, value);
        }

        private CockpitControlUnitButtonPressedStateValue _hmiBtnMotor1AutoOffIn = CockpitControlUnitButtonPressedStateValue.NotPressed;
        public CockpitControlUnitButtonPressedStateValue HmiBtnMotor1AutoOffIn
        {
            get => _hmiBtnMotor1AutoOffIn;
            set => SetProperty(ref _hmiBtnMotor1AutoOffIn, value);
        }

        private CockpitControlUnitButtonPressedStateValue _hmiBtnMuteShortIn = CockpitControlUnitButtonPressedStateValue.NotPressed;
        public CockpitControlUnitButtonPressedStateValue HmiBtnMuteShortIn
        {
            get => _hmiBtnMuteShortIn;
            set => SetProperty(ref _hmiBtnMuteShortIn, value);
        }

        private CockpitControlUnitButtonPressedStateValue _hmiBtnMuteLongIn = CockpitControlUnitButtonPressedStateValue.NotPressed;
        public CockpitControlUnitButtonPressedStateValue HmiBtnMuteLongIn
        {
            get => _hmiBtnMuteLongIn;
            set => SetProperty(ref _hmiBtnMuteLongIn, value);
        }

        private CockpitControlUnitButtonPressedStateValue _hmiBtnConnectionIn = CockpitControlUnitButtonPressedStateValue.NotPressed;
        public CockpitControlUnitButtonPressedStateValue HmiBtnConnectionIn
        {
            get => _hmiBtnConnectionIn;
            set => SetProperty(ref _hmiBtnConnectionIn, value);
        }

        private CockpitControlUnitKeylockStateValue _hmiBtnKeylockStatus = CockpitControlUnitKeylockStateValue.NotLocked;
        public CockpitControlUnitKeylockStateValue HmiBtnKeylockStatus
        {
            get => _hmiBtnKeylockStatus;
            set => SetProperty(ref _hmiBtnKeylockStatus, value);
        }

        private CockpitControlUnitAudioFeedbackStateValue _hmiButtonFeedback = CockpitControlUnitAudioFeedbackStateValue.Disabled;
        public CockpitControlUnitAudioFeedbackStateValue HmiButtonFeedback
        {
            get => _hmiButtonFeedback;
            set => SetProperty(ref _hmiButtonFeedback, value);
        }

        private double _statisticSystemOperatingTotal = 0;
        public double StatisticSystemOperatingTotal
        {
            get => _statisticSystemOperatingTotal;
            set => SetProperty(ref _statisticSystemOperatingTotal, value);
        }

        private double _statisticMotorOperatingTotal = 0;
        public double StatisticMotorOperatingTotal
        {
            get => _statisticMotorOperatingTotal;
            set => SetProperty(ref _statisticMotorOperatingTotal, value);
        }

        private double _statisticMotorSwitchesTotal = 0;
        public double StatisticMotorSwitchesTotal
        {
            get => _statisticMotorSwitchesTotal;
            set => SetProperty(ref _statisticMotorSwitchesTotal, value);
        }

        private double _statisticMotorOperatingMotor0Total = 0;
        public double StatisticMotorOperatingMotor0Total
        {
            get => _statisticMotorOperatingMotor0Total;
            set => SetProperty(ref _statisticMotorOperatingMotor0Total, value);
        }

        private double _statisticMotorOperatingMotor024h = 0;
        public double StatisticMotorOperatingMotor024h
        {
            get => _statisticMotorOperatingMotor024h;
            set => SetProperty(ref _statisticMotorOperatingMotor024h, value);
        }

        private double _statisticMotorOperatingMotor07d = 0;
        public double StatisticMotorOperatingMotor07d
        {
            get => _statisticMotorOperatingMotor07d;
            set => SetProperty(ref _statisticMotorOperatingMotor07d, value);
        }

        private double _statisticMotorOperatingMotor01m = 0;
        public double StatisticMotorOperatingMotor01m
        {
            get => _statisticMotorOperatingMotor01m;
            set => SetProperty(ref _statisticMotorOperatingMotor01m, value);
        }

        private double _statisticMotorSwitchesMotor0Total = 0;
        public double StatisticMotorSwitchesMotor0Total
        {
            get => _statisticMotorSwitchesMotor0Total;
            set => SetProperty(ref _statisticMotorSwitchesMotor0Total, value);
        }

        private double _statisticMotorSwitchesMotor024h = 0;
        public double StatisticMotorSwitchesMotor024h
        {
            get => _statisticMotorSwitchesMotor024h;
            set => SetProperty(ref _statisticMotorSwitchesMotor024h, value);
        }

        private double _statisticMotorSwitchesMotor07d = 0;
        public double StatisticMotorSwitchesMotor07d
        {
            get => _statisticMotorSwitchesMotor07d;
            set => SetProperty(ref _statisticMotorSwitchesMotor07d, value);
        }

        private double _statisticMotorSwitchesMotor01m = 0;
        public double StatisticMotorSwitchesMotor01m
        {
            get => _statisticMotorSwitchesMotor01m;
            set => SetProperty(ref _statisticMotorSwitchesMotor01m, value);
        }

        private double _statisticMotorOperatingMotor1Total = 0;
        public double StatisticMotorOperatingMotor1Total
        {
            get => _statisticMotorOperatingMotor1Total;
            set => SetProperty(ref _statisticMotorOperatingMotor1Total, value);
        }

        private double _statisticMotorOperatingMotor124h = 0;
        public double StatisticMotorOperatingMotor124h
        {
            get => _statisticMotorOperatingMotor124h;
            set => SetProperty(ref _statisticMotorOperatingMotor124h, value);
        }

        private double _statisticMotorOperatingMotor17d = 0;
        public double StatisticMotorOperatingMotor17d
        {
            get => _statisticMotorOperatingMotor17d;
            set => SetProperty(ref _statisticMotorOperatingMotor17d, value);
        }

        private double _statisticMotorOperatingMotor11m = 0;
        public double StatisticMotorOperatingMotor11m
        {
            get => _statisticMotorOperatingMotor11m;
            set => SetProperty(ref _statisticMotorOperatingMotor11m, value);
        }

        private double _statisticMotorSwitchesMotor1Total = 0;
        public double StatisticMotorSwitchesMotor1Total
        {
            get => _statisticMotorSwitchesMotor1Total;
            set => SetProperty(ref _statisticMotorSwitchesMotor1Total, value);
        }

        private double _statisticMotorSwitchesMotor124h = 0;
        public double StatisticMotorSwitchesMotor124h
        {
            get => _statisticMotorSwitchesMotor124h;
            set => SetProperty(ref _statisticMotorSwitchesMotor124h, value);
        }

        private double _statisticMotorSwitchesMotor17d = 0;
        public double StatisticMotorSwitchesMotor17d
        {
            get => _statisticMotorSwitchesMotor17d;
            set => SetProperty(ref _statisticMotorSwitchesMotor17d, value);
        }

        private double _statisticMotorSwitchesMotor11m = 0;
        public double StatisticMotorSwitchesMotor11m
        {
            get => _statisticMotorSwitchesMotor11m;
            set => SetProperty(ref _statisticMotorSwitchesMotor11m, value);
        }

    }
}
