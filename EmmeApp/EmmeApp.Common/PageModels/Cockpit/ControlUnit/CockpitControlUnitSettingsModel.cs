﻿using Prism.Mvvm;

namespace EmmeApp.Common.PageModels.Cockpit
{
    public class CockpitControlUnitSettingsModel : BindableBase
    {
        private bool _cmdSystemConfirmAlarm = false;
        public bool CmdSystemConfirmAlarm
        {
            get => _cmdSystemConfirmAlarm;
            set => SetProperty(ref _cmdSystemConfirmAlarm, value);
        }

        private bool _cmdSystemConfirmMaintenanceWarning = false;
        public bool CmdSystemConfirmAlarmMaintenance
        {
            get => _cmdSystemConfirmMaintenanceWarning;
            set => SetProperty(ref _cmdSystemConfirmMaintenanceWarning, value);
        }

        private bool _cmdM2MTestAlarmRelay = false;
        public bool CmdM2MTestAlarmRelay
        {
            get => _cmdM2MTestAlarmRelay;
            set => SetProperty(ref _cmdM2MTestAlarmRelay, value);
        }

        private bool _cmdM2MTestCustomRelay0 = false;
        public bool CmdM2MTestCustomRelay0
        {
            get => _cmdM2MTestCustomRelay0;
            set => SetProperty(ref _cmdM2MTestCustomRelay0, value);
        }

        private bool _cmdM2MTestCustomRelay1 = false;
        public bool CmdM2MTestCustomRelay1
        {
            get => _cmdM2MTestCustomRelay1;
            set => SetProperty(ref _cmdM2MTestCustomRelay1, value);
        }

        private bool _cmdM2MTestCustomRelay2 = false;
        public bool CmdM2MTestCustomRelay2
        {
            get => _cmdM2MTestCustomRelay2;
            set => SetProperty(ref _cmdM2MTestCustomRelay2, value);
        }

        private bool _cmdHmiTestAlarmHorn = false;
        public bool CmdHmiTestAlarmHorn
        { 
            get => _cmdHmiTestAlarmHorn;
            set => SetProperty(ref _cmdHmiTestAlarmHorn, value);
        }

        private bool _cmdHmiTestAlarmBuzzer = false;
        public bool CmdHmiTestAlarmBuzzer
        {
            get => _cmdHmiTestAlarmBuzzer;
            set => SetProperty(ref _cmdHmiTestAlarmBuzzer, value);
        }

        private bool _cmdStatisticReset = false;
        public bool CmdStatisticReset
        {
            get => _cmdStatisticReset;
            set => SetProperty(ref _cmdStatisticReset, value);
        }
    }
}
