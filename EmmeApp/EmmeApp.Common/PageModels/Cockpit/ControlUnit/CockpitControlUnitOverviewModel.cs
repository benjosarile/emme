﻿using EmmeApp.Common.Enum;
using Prism.Mvvm;

namespace EmmeApp.Common.PageModels.Cockpit
{
    public class CockpitControlUnitOverviewModel : BindableBase
	{
		private ControlUnitSystemStatusValue _systemStatus = ControlUnitSystemStatusValue.CMN_SYSTEM_STATUS_NOT_CONFIGURED;
		public ControlUnitSystemStatusValue SystemStatus
		{
			get => _systemStatus;
			set => SetProperty(ref _systemStatus, value);
		}

		private ControlUnitSystemAlarmValue _systemAlarm = ControlUnitSystemAlarmValue.CMN_ALARM_NO_ALARM;
		public ControlUnitSystemAlarmValue SystemAlarm
		{
			get => _systemAlarm;
			set => SetProperty(ref _systemAlarm, value);
		}

		private ControlUnitSystemWarningValue _systemWarning = ControlUnitSystemWarningValue.CMN_WARNING_NO_WARNING;
		public ControlUnitSystemWarningValue SystemWarning
		{
			get => _systemWarning;
			set => SetProperty(ref _systemWarning, value);
		}

		private ControlUnitSystemFailureValue _systemFailure = ControlUnitSystemFailureValue.CMN_FAILURE_NO_FAILURE;
		public ControlUnitSystemFailureValue SystemFailure
		{
			get => _systemFailure;
			set => SetProperty(ref _systemFailure, value);
		}

		private ControlUnitSystemTypeValue _systemType = ControlUnitSystemTypeValue.NotConfigured;
		public ControlUnitSystemTypeValue SystemType
		{
			get => _systemType;
			set => SetProperty(ref _systemType, value);
		}

		private ControlUnitSystemDoubleMotorValue _systemDoubleMotor = ControlUnitSystemDoubleMotorValue.OnePump;
		public ControlUnitSystemDoubleMotorValue SystemDoubleMotor
		{
			get => _systemDoubleMotor;
			set => SetProperty(ref _systemDoubleMotor, value);
		}

		private ControlUnitMotorStatusValue _motorStatusMotor0 = ControlUnitMotorStatusValue.CMN_MOTOR_MODE_RUN;
		public ControlUnitMotorStatusValue MotorStatusMotor0
		{
			get => _motorStatusMotor0;
			set => SetProperty(ref _motorStatusMotor0, value);
		}

		private ControlUnitMotorModeValue _motorModeMotor0 = ControlUnitMotorModeValue.Auto;
		public ControlUnitMotorModeValue MotorModeMotor0
		{
			get => _motorModeMotor0;
			set => SetProperty(ref _motorModeMotor0, value);
		}

		private ControlUnitMotorStatusValue _motorStatusMotor1 = ControlUnitMotorStatusValue.CMN_MOTOR_MODE_RUN;
		public ControlUnitMotorStatusValue MotorStatusMotor1
		{
			get => _motorStatusMotor1;
			set => SetProperty(ref _motorStatusMotor1, value);
		}

		private ControlUnitMotorModeValue _motorModeMotor1 = ControlUnitMotorModeValue.Auto;
		public ControlUnitMotorModeValue MotorModeMotor1
		{
			get => _motorModeMotor1;
			set => SetProperty(ref _motorModeMotor1, value);
		}

		private double _motorPhase1CurrentMotor0 = 22;
		public double MotorPhase1CurrentMotor0
		{
			get => _motorPhase1CurrentMotor0;
			set => SetProperty(ref _motorPhase1CurrentMotor0, value);
		}

		private double _motorPhase1CurrentMotor1 = 22;
		public double MotorPhase1CurrentMotor1
		{
			get => _motorPhase1CurrentMotor1;
			set => SetProperty(ref _motorPhase1CurrentMotor1, value);
		}

		private ControlUnitLevelActiveValue _levelActiveLevel = ControlUnitLevelActiveValue.CMN_LEVEL_0;
		public ControlUnitLevelActiveValue LevelActiveLevel
		{
			get => _levelActiveLevel;
			set => SetProperty(ref _levelActiveLevel, value);
		}
	}
}
