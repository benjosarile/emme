﻿using EmmeApp.Common.Attributes;
using EmmeApp.Common.Enum;
using Prism.Mvvm;
using System;

namespace EmmeApp.Common.PageModels.Cockpit
{
    public class CockpitControlUnitRootModel : BindableBase
    {
        private Guid _selectedDeviceUuid;
        public Guid SelectedDeviceUuid
        {
            get => _selectedDeviceUuid;
            set => SetProperty(ref _selectedDeviceUuid, value);
        }

		private double _cmnProductID = 1;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CmnProductId, typeof(double))]
		public double CmnProductID
		{
			get => _cmnProductID;
			set => SetProperty(ref _cmnProductID, value);
		}

		private double _cmnSystemParameterProfileID = 1;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CmnSystemParameterProfileId, typeof(double))]
		public double CmnSystemParameterProfileID
		{
			get => _cmnSystemParameterProfileID;
			set => SetProperty(ref _cmnSystemParameterProfileID, value);
		}

		private double _cmnSystemEventProfileID = 1;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CmnSystemEventProfileId, typeof(double))]
		public double CmnSystemEventProfileID
		{
			get => _cmnSystemEventProfileID;
			set => SetProperty(ref _cmnSystemEventProfileID, value);
		}

		private string _cmnSystemFirmwareVersion = "00.00.01";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CmnFwVersion, typeof(string))]
		public string CmnSystemFirmwareVersion
		{
			get => _cmnSystemFirmwareVersion;
			set => SetProperty(ref _cmnSystemFirmwareVersion, value);
		}

		private string _cmnSystemFirmwareSignature = "0xAAAA5555";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CmnFwSignature, typeof(string))]
		public string CmnSystemFirmwareSignature
		{
			get => _cmnSystemFirmwareSignature;
			set => SetProperty(ref _cmnSystemFirmwareSignature, value);
		}

		private double _logSize = 1500;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LogSize, typeof(double))]
		public double LogSize
		{
			get => _logSize;
			set => SetProperty(ref _logSize, value);
		}

		private double _logCount = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LogCount, typeof(double))]
		public double LogCount
		{
			get => _logCount;
			set => SetProperty(ref _logCount, value);
		}

		private double _logIndex = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LogIndex, typeof(double))]
		public double LogIndex
		{
			get => _logIndex;
			set => SetProperty(ref _logIndex, value);
		}

		private string _rtosVersion = "V10.0.0";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.VersionRtos, typeof(string))]
		public string RTOSVersion
		{
			get => _rtosVersion;
			set => SetProperty(ref _rtosVersion, value);
		}

		private string _halVersion = "V1.7.2";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.VersionHal, typeof(string))]
		public string HALVersion
		{
			get => _halVersion;
			set => SetProperty(ref _halVersion, value);
		}

		private string _cmsisVersion = "V4.30";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.VersionCmsis, typeof(string))]
		public string CMSISVersion
		{
			get => _cmsisVersion;
			set => SetProperty(ref _cmsisVersion, value);
		}

		private string _bdlcVersion = "V00.00.01";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.VersionBdlc, typeof(string))]
		public string BDLCVersion
		{
			get => _bdlcVersion;
			set => SetProperty(ref _bdlcVersion, value);
		}

		private string _factoryProductName = "NomBox-42";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.FactoryProductName, typeof(string))]
		public string FactoryProductName
		{
			get => _factoryProductName;
			set => SetProperty(ref _factoryProductName, value);
		}

		private string _factoryItemNumber = "123456789";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.FactoryItemNumber, typeof(string))]
		public string FactoryItemNumber
		{
			get => _factoryItemNumber;
			set => SetProperty(ref _factoryItemNumber, value);
		}

		private string _factorySerialNumber = "123456789";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.FactorySerialNumber, typeof(string))]
		public string FactorySerialNumber
		{
			get => _factorySerialNumber;
			set => SetProperty(ref _factorySerialNumber, value);
		}

		private string _factoryAssemblyTime = "1549271462";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.FactoryAssemblyTime, typeof(string))]
		public string FactoryAssemblyTime
		{
			get => _factoryAssemblyTime;
			set => SetProperty(ref _factoryAssemblyTime, value);
		}

		private double _factoryOrderNumber = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.FactoryOrderNumber, typeof(double))]
		public double FactoryOrderNumber
		{
			get => _factoryOrderNumber;
			set => SetProperty(ref _factoryOrderNumber, value);
		}

		private string _customerName = "Max Mustermann";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerName, typeof(string))]
		public string CustomerName
		{
			get => _customerName;
			set => SetProperty(ref _customerName, value);
		}

		private string _customerPhoneNumber = "+41 31 720 90 00";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerPhone, typeof(string))]
		public string CustomerPhoneNumber
		{
			get => _customerPhoneNumber;
			set => SetProperty(ref _customerPhoneNumber, value);
		}

		private string _customerStreet = "Suedstrasse 10";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerStreet, typeof(string))]
		public string CustomerStreet
		{
			get => _customerStreet;
			set => SetProperty(ref _customerStreet, value);
		}

		private string _customerCity = "Muensingen";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerLocation, typeof(string))]
		public string CustomerCity
		{
			get => _customerCity;
			set => SetProperty(ref _customerCity, value);
		}

		private string _customerZip = "3110";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerZip, typeof(string))]
		public string CustomerZip
		{
			get => _customerZip;
			set => SetProperty(ref _customerZip, value);
		}

		private string _customerCountry = "Switzerland";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerCountry, typeof(string))]
		public string CustomerCountry
		{
			get => _customerCountry;
			set => SetProperty(ref _customerCountry, value);
		}

		private string _systemName = "Biral Testsystem";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerSystemName, typeof(string))]
		public string SystemName
		{
			get => _systemName;
			set => SetProperty(ref _systemName, value);
		}

		private string _motorType0 = "Motor 0";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerMotorType0, typeof(string))]
		public string MotorType0
		{
			get => _motorType0;
			set => SetProperty(ref _motorType0, value);
		}

		private string _motorType1 = "Motor 1";
		[ControlUnitParam(ExternalControlUnitParameterIdValue.CustomerMotorType1, typeof(string))]
		public string MotorType1
		{
			get => _motorType1;
			set => SetProperty(ref _motorType1, value);
		}

		private ControlUnitSystemTypeValue _systemType = ControlUnitSystemTypeValue.NotConfigured;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemType, typeof(ControlUnitSystemTypeValue))]
		public ControlUnitSystemTypeValue SystemType
		{
			get => _systemType;
			set => SetProperty(ref _systemType, value);
		}

		private ControlUnitSystemDoubleMotorValue _systemDoubleMotor = ControlUnitSystemDoubleMotorValue.TwoPumps;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemDoubleMotor, typeof(ControlUnitSystemDoubleMotorValue))]
		public ControlUnitSystemDoubleMotorValue SystemDoubleMotor
		{
			get => _systemDoubleMotor;
			set => SetProperty(ref _systemDoubleMotor, value);
		}

		private ControlUnitMotorModeValue _systemMotorMode0 = ControlUnitMotorModeValue.Auto;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemMotorMode0, typeof(ControlUnitMotorModeValue))]
		public ControlUnitMotorModeValue SystemMotorMode0
		{
			get => _systemMotorMode0;
			set => SetProperty(ref _systemMotorMode0, value);
		}

		private ControlUnitMotorModeValue _systemMotorMode1 = ControlUnitMotorModeValue.Auto;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorStatusMotor1, typeof(ControlUnitMotorModeValue))]
		public ControlUnitMotorModeValue SystemMotorMode1
		{
			get => _systemMotorMode1;
			set => SetProperty(ref _systemMotorMode1, value);
		}

		private bool _maintenanceWarningEnable = false;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemMaintenanceWarningEnable, typeof(bool))]
		public bool MaintenanceWarningEnable
		{
			get => _maintenanceWarningEnable;
			set => SetProperty(ref _maintenanceWarningEnable, value);
		}

		private double _maintenanceWarningTrigger = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemMaintenanceWarningTrigger, typeof(double))]
		public double MaintenanceWarningTrigger
		{
			get => _maintenanceWarningTrigger;
			set => SetProperty(ref _maintenanceWarningTrigger, value);
		}

		private double _maintenanceWarningInterval = 12;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemMaintenanceWarningInterval, typeof(double))]
		public double MaintenanceWarningInterval
		{
			get => _maintenanceWarningInterval;
			set => SetProperty(ref _maintenanceWarningInterval, value);
		}

		private long _systemUnixTimestamp = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemUnixTimestamp, typeof(long))]
		public long SystemUnixTimestamp
		{
			get => _systemUnixTimestamp;
			set => SetProperty(ref _systemUnixTimestamp, value);
		}

		private bool _systemVirginIsActive = false;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemVirginIsActive, typeof(bool))]
		public bool SystemVirginIsActive
		{
			get => _systemVirginIsActive;
			set => SetProperty(ref _systemVirginIsActive, value);
		}

		private ControlUnitSystemFailureValue _systemFailure = ControlUnitSystemFailureValue.CMN_FAILURE_NO_FAILURE;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemFailure, typeof(ControlUnitSystemFailureValue))]
		public ControlUnitSystemFailureValue SystemFailure
		{
			get => _systemFailure;
			set => SetProperty(ref _systemFailure, value);
		}

		private ControlUnitSystemAlarmValue _systemAlarm = ControlUnitSystemAlarmValue.CMN_ALARM_NO_ALARM;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemAlarm, typeof(ControlUnitSystemAlarmValue))]
		public ControlUnitSystemAlarmValue SystemAlarm
		{
			get => _systemAlarm;
			set => SetProperty(ref _systemAlarm, value);
		}

		private ControlUnitSystemWarningValue _systemWarning = ControlUnitSystemWarningValue.CMN_WARNING_NO_WARNING;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemWarning, typeof(ControlUnitSystemWarningValue))]
		public ControlUnitSystemWarningValue SystemWarning
		{
			get => _systemWarning;
			set => SetProperty(ref _systemWarning, value);
		}

		private ControlUnitSystemStatusValue _systemStatus = ControlUnitSystemStatusValue.CMN_SYSTEM_STATUS_NOT_CONFIGURED;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.SystemStatus, typeof(ControlUnitSystemStatusValue))]
		public ControlUnitSystemStatusValue SystemStatus
		{
			get => _systemStatus;
			set => SetProperty(ref _systemStatus, value);
		}

		private ControlUnitMotorModeValue _motorModeMotor0 = ControlUnitMotorModeValue.Auto;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorModeMotor0, typeof(ControlUnitMotorModeValue))]
		public ControlUnitMotorModeValue MotorModeMotor0
		{
			get => _motorModeMotor0;
			set => SetProperty(ref _motorModeMotor0, value);
		}

		private double _motorRatedCurrentMotor0 = 50;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorRatedCurrentMotor0, typeof(double))]
		public double MotorRatedCurrentMotor0
		{
			get => _motorRatedCurrentMotor0;
			set => SetProperty(ref _motorRatedCurrentMotor0, value);
		}

		private double _motorPhase1CurrentMotor0 = 122;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorPhase1CurrentMotor0, typeof(double))]
		public double MotorPhase1CurrentMotor0
		{
			get => _motorPhase1CurrentMotor0;
			set => SetProperty(ref _motorPhase1CurrentMotor0, value);
		}

		private double _motorPhase3CurrentMotor0 = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorPhase3CurrentMotor0, typeof(double))]
		public double MotorPhase3CurrentMotor0
		{
			get => _motorPhase3CurrentMotor0;
			set => SetProperty(ref _motorPhase3CurrentMotor0, value);
		}

		private ControlUnitMotorStatusValue _motorStatusMotor0 = ControlUnitMotorStatusValue.CMN_MOTOR_MODE_RUN;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorStatusMotor0, typeof(ControlUnitMotorStatusValue))]
		public ControlUnitMotorStatusValue MotorStatusMotor0
		{
			get => _motorStatusMotor0;
			set => SetProperty(ref _motorStatusMotor0, value);
		}

        private ControlUnitThermalProtectionStateValue _motorWSKMotor0Enable = ControlUnitThermalProtectionStateValue.Enabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorWskMotor0Enable, typeof(ControlUnitThermalProtectionStateValue))]
		public ControlUnitThermalProtectionStateValue MotorWSKMotor0Enable
        {
            get => _motorWSKMotor0Enable;
            set => SetProperty(ref _motorWSKMotor0Enable, value);
        }

        private CockpitControlUnitMotorProtectionStateValue _motorProtectionMotor0Enable = CockpitControlUnitMotorProtectionStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorMotorProtectionMotor0Enable, typeof(CockpitControlUnitMotorProtectionStateValue))]
		public CockpitControlUnitMotorProtectionStateValue MotorProtectionMotor0Enable
        {
            get => _motorProtectionMotor0Enable;
            set => SetProperty(ref _motorProtectionMotor0Enable, value);
        }

        private ControlUnitMotorModeValue _motorModeMotor1 = ControlUnitMotorModeValue.Auto;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorModeMotor1, typeof(ControlUnitMotorModeValue))]
		public ControlUnitMotorModeValue MotorModeMotor1
		{
			get => _motorModeMotor1;
			set => SetProperty(ref _motorModeMotor1, value);
		}

		private double _motorRatedCurrentMotor1 = 42;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorRatedCurrentMotor1, typeof(double))]
		public double MotorRatedCurrentMotor1
		{
			get => _motorRatedCurrentMotor1;
			set => SetProperty(ref _motorRatedCurrentMotor1, value);
		}

		private double _motorPhase1CurrentMotor1 = 650;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorPhase1CurrentMotor1, typeof(double))]
		public double MotorPhase1CurrentMotor1
		{
			get => _motorPhase1CurrentMotor1;
			set => SetProperty(ref _motorPhase1CurrentMotor1, value);
		}

		private double _motorPhase3CurrentMotor1 = 500;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorPhase3CurrentMotor1, typeof(double))]
		public double MotorPhase3CurrentMotor1
		{
			get => _motorPhase3CurrentMotor1;
			set => SetProperty(ref _motorPhase3CurrentMotor1, value);
		}

		private ControlUnitMotorStatusValue _motorStatusMotor1 = ControlUnitMotorStatusValue.CMN_MOTOR_MODE_RUN;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorStatusMotor1, typeof(ControlUnitMotorStatusValue))]
		public ControlUnitMotorStatusValue MotorStatusMotor1
		{
			get => _motorStatusMotor1;
			set => SetProperty(ref _motorStatusMotor1, value);
		}

        private ControlUnitThermalProtectionStateValue _motorWSKMotor1Enable = ControlUnitThermalProtectionStateValue.Enabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorWskMotor1Enable, typeof(ControlUnitThermalProtectionStateValue))]
		public ControlUnitThermalProtectionStateValue MotorWSKMotor1Enable
        {
            get => _motorWSKMotor1Enable;
            set => SetProperty(ref _motorWSKMotor1Enable, value);
        }

        private CockpitControlUnitMotorProtectionStateValue _motorProtectionMotor1Enable = CockpitControlUnitMotorProtectionStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorMotorProtectionMotor1Enable, typeof(CockpitControlUnitMotorProtectionStateValue))]
		public CockpitControlUnitMotorProtectionStateValue MotorProtectionMotor1Enable
        {
            get => _motorProtectionMotor1Enable;
            set => SetProperty(ref _motorProtectionMotor1Enable, value);
        }

        private ControlUnitOskStateValue _motorOSKEnable = ControlUnitOskStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorOskEnable, typeof(ControlUnitOskStateValue))]
		public ControlUnitOskStateValue MotorOSKEnable
        {
            get => _motorOSKEnable;
            set => SetProperty(ref _motorOSKEnable, value);
        }

        private ControlUnitDelayedOffStateValue _motorDelayedOffEnable = ControlUnitDelayedOffStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorDelayedOffEnable, typeof(ControlUnitDelayedOffStateValue))]
		public ControlUnitDelayedOffStateValue MotorDelayedOffEnable
        {
            get => _motorDelayedOffEnable;
            set => SetProperty(ref _motorDelayedOffEnable, value);
        }

        private double _motorDelayedOffTime = 5;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorDelayedOffTime, typeof(double))]
		public double MotorDelayedOffTime
		{
			get => _motorDelayedOffTime;
			set => SetProperty(ref _motorDelayedOffTime, value);
		}

        private ControlUnitForceRunningStateValue _motorForceRunningEnable = ControlUnitForceRunningStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorForceRunningEnable, typeof(ControlUnitForceRunningStateValue))]
		public ControlUnitForceRunningStateValue MotorForceRunningEnable
        {
            get => _motorForceRunningEnable;
            set => SetProperty(ref _motorForceRunningEnable, value);
        }

        private double _motorForceRetentionTime = 30;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorForceRetentionTime, typeof(double))]
		public double MotorForceRetentionTime
		{
			get => _motorForceRetentionTime;
			set => SetProperty(ref _motorForceRetentionTime, value);
		}

		private double _motorForceRunningOnTime = 5;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorForceRunningOnTime, typeof(double))]
		public double MotorForceRunningOnTime
		{
			get => _motorForceRunningOnTime;
			set => SetProperty(ref _motorForceRunningOnTime, value);
		}

        private ControlUnitDelayedOnStateValue _motorDelayedOnEnable = ControlUnitDelayedOnStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorDelayedOnEnable, typeof(ControlUnitDelayedOnStateValue))]
		public ControlUnitDelayedOnStateValue MotorDelayedOnEnable
        {
            get => _motorDelayedOnEnable;
            set => SetProperty(ref _motorDelayedOnEnable, value);
        }

        private double _motorDelayedOnTime = 5;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorDelayedOnTime, typeof(double))]
		public double MotorDelayedOnTime
		{
			get => _motorDelayedOnTime;
			set => SetProperty(ref _motorDelayedOnTime, value);
		}

        private CockpitControlUnitRuntimeObserverStateValue _motorRuntimeObserverEnable = CockpitControlUnitRuntimeObserverStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorRuntimeObserverEnable, typeof(CockpitControlUnitRuntimeObserverStateValue))]
		public CockpitControlUnitRuntimeObserverStateValue MotorRuntimeObserverEnable
        {
            get => _motorRuntimeObserverEnable;
            set => SetProperty(ref _motorRuntimeObserverEnable, value);
        }

        private double _motorRuntimeObserverMaxCycle = 3;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorRuntimeObserverMaxCycle, typeof(double))]
		public double MotorRuntimeObserverMaxCycle
		{
			get => _motorRuntimeObserverMaxCycle;
			set => SetProperty(ref _motorRuntimeObserverMaxCycle, value);
		}

		private double _motorRuntimeObserverTimespan = 120;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorRuntimeObserverTimespan, typeof(double))]
		public double MotorRuntimeObserverTimespan
		{
			get => _motorRuntimeObserverTimespan;
			set => SetProperty(ref _motorRuntimeObserverTimespan, value);
		}

		private double _motorRuntimeObserverRetention = 15;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorRuntimeObserverRetention, typeof(double))]
		public double MotorRuntimeObserverRetention
		{
			get => _motorRuntimeObserverRetention;
			set => SetProperty(ref _motorRuntimeObserverRetention, value);
		}

        private ControlUnitAtexStateValue _motorAtexEnable = ControlUnitAtexStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorAtexEnable, typeof(ControlUnitAtexStateValue))]
		public ControlUnitAtexStateValue MotorAtexEnable
        {
            get => _motorAtexEnable;
            set => SetProperty(ref _motorAtexEnable, value);
        }

        private double _motorAtexTimeout = 60;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorAtexTimeout, typeof(double))]
		public double MotorAtexTimeout
		{
			get => _motorAtexTimeout;
			set => SetProperty(ref _motorAtexTimeout, value);
		}

		private double _motorAtexTimeoutLock = 30;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.MotorAtexTimeoutLock, typeof(double))]
		public double MotorAtexTimeoutLock
		{
			get => _motorAtexTimeoutLock;
			set => SetProperty(ref _motorAtexTimeoutLock, value);
		}

		private LevelProbeConfigurationValue _levelProbeConfiguration = LevelProbeConfigurationValue.ThreeLevelswitches;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelProbeConfiguration, typeof(LevelProbeConfigurationValue))]
		public LevelProbeConfigurationValue LevelProbeConfiguration
		{
			get => _levelProbeConfiguration;
			set => SetProperty(ref _levelProbeConfiguration, value);
		}

		private ControlUnitLevelActiveValue _levelActiveLevel = ControlUnitLevelActiveValue.CMN_LEVEL_0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelActiveLevel, typeof(ControlUnitLevelActiveValue))]
		public ControlUnitLevelActiveValue LevelActiveLevel
		{
			get => _levelActiveLevel;
			set => SetProperty(ref _levelActiveLevel, value);
		}

		private ControlUnitThresholdStatusValue _levelThreshold0Status = ControlUnitThresholdStatusValue.CMN_THRESHOLD_STATUS_NOT_ACTIVE;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelThreshold0Status, typeof(ControlUnitThresholdStatusValue))]
		public ControlUnitThresholdStatusValue LevelThreshold0Status
		{
			get => _levelThreshold0Status;
			set => SetProperty(ref _levelThreshold0Status, value);
		}

		private ControlUnitThresholdStatusValue _levelThreshold1Status = ControlUnitThresholdStatusValue.CMN_THRESHOLD_STATUS_NOT_ACTIVE;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelThreshold1Status, typeof(ControlUnitThresholdStatusValue))]
		public ControlUnitThresholdStatusValue LevelThreshold1Status
		{
			get => _levelThreshold1Status;
			set => SetProperty(ref _levelThreshold1Status, value);
		}

		private ControlUnitThresholdStatusValue _levelThreshold2Status = ControlUnitThresholdStatusValue.CMN_THRESHOLD_STATUS_NOT_ACTIVE;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelThreshold2Status, typeof(ControlUnitThresholdStatusValue))]
		public ControlUnitThresholdStatusValue LevelThreshold2Status
		{
			get => _levelThreshold2Status;
			set => SetProperty(ref _levelThreshold2Status, value);
		}

		private ControlUnitThresholdStatusValue _levelThreshold3Status = ControlUnitThresholdStatusValue.CMN_THRESHOLD_STATUS_NOT_ACTIVE;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelThreshold3Status, typeof(ControlUnitThresholdStatusValue))]
		public ControlUnitThresholdStatusValue LevelThreshold3Status
		{
			get => _levelThreshold3Status;
			set => SetProperty(ref _levelThreshold3Status, value);
		}

		private ControlUnitLevelswitchStatusValue _levelswitch0Status = ControlUnitLevelswitchStatusValue.CMN_LEVEL_LEVELSWITCH_STATE_OK_HANGING;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelLevelswitch0Status, typeof(ControlUnitLevelswitchStatusValue))]
		public ControlUnitLevelswitchStatusValue Levelswitch0Status
		{
			get => _levelswitch0Status;
			set => SetProperty(ref _levelswitch0Status, value);
		}

		private ControlUnitLevelswitchStatusValue _levelswitch1Status = ControlUnitLevelswitchStatusValue.CMN_LEVEL_LEVELSWITCH_STATE_OK_HANGING;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelLevelswitch1Status, typeof(ControlUnitLevelswitchStatusValue))]
		public ControlUnitLevelswitchStatusValue Levelswitch1Status
		{
			get => _levelswitch1Status;
			set => SetProperty(ref _levelswitch1Status, value);
		}

		private ControlUnitLevelswitchStatusValue _levelswitch2Status = ControlUnitLevelswitchStatusValue.CMN_LEVEL_LEVELSWITCH_STATE_OK_HANGING;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelLevelswitch2Status, typeof(ControlUnitLevelswitchStatusValue))]
		public ControlUnitLevelswitchStatusValue Levelswitch2Status
		{
			get => _levelswitch2Status;
			set => SetProperty(ref _levelswitch2Status, value);
		}

		private ControlUnitLevelswitchStatusValue _levelswitch3Status = ControlUnitLevelswitchStatusValue.CMN_LEVEL_LEVELSWITCH_STATE_OK_HANGING;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelLevelswitch3Status, typeof(ControlUnitLevelswitchStatusValue))]
		public ControlUnitLevelswitchStatusValue Levelswitch3Status
		{
			get => _levelswitch3Status;
			set => SetProperty(ref _levelswitch3Status, value);
		}

		private ControlUnitLevelAnalogStatusValue _levelAnalogStatus = ControlUnitLevelAnalogStatusValue.CMN_LEVEL_ANALOG_STATE_OK;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelAnalogStatus, typeof(ControlUnitLevelAnalogStatusValue))]
		public ControlUnitLevelAnalogStatusValue LevelAnalogStatus
		{
			get => _levelAnalogStatus;
			set => SetProperty(ref _levelAnalogStatus, value);
		}

		private double _levelAnalogActiveCurrent = 40;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelAnalogActiveCurrent, typeof(double))]
		public double LevelAnalogActiveCurrent
		{
			get => _levelAnalogActiveCurrent;
			set => SetProperty(ref _levelAnalogActiveCurrent, value);
		}

		private double _levelAnalogThreshold0Current = 40;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelAnalogThreshold0Current, typeof(double))]
		public double LevelAnalogThreshold0Current
		{
			get => _levelAnalogThreshold0Current;
			set => SetProperty(ref _levelAnalogThreshold0Current, value);
		}

		private double _levelAnalogThreshold1Current = 40;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelAnalogThreshold1Current, typeof(double))]
		public double LevelAnalogThreshold1Current
		{
			get => _levelAnalogThreshold1Current;
			set => SetProperty(ref _levelAnalogThreshold1Current, value);
		}

		private double _levelAnalogThreshold2Current = 40;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.LevelAnalogThreshold2Current, typeof(double))]
		public double LevelAnalogThreshold2Current
		{
			get => _levelAnalogThreshold2Current;
			set => SetProperty(ref _levelAnalogThreshold2Current, value);
		}

        private CockpitControlUnitRelayStateValue _m2mOperatingRelay = CockpitControlUnitRelayStateValue.NotActive;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.M2mOperatingRelay, typeof(CockpitControlUnitRelayStateValue))]
		public CockpitControlUnitRelayStateValue M2mOperatingRelay
        {
            get => _m2mOperatingRelay;
            set => SetProperty(ref _m2mOperatingRelay, value);
        }

        private CockpitControlUnitRelayStateValue _m2mCustomRelay0 = CockpitControlUnitRelayStateValue.NotActive;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.M2mCustomRelay0, typeof(CockpitControlUnitRelayStateValue))]
		public CockpitControlUnitRelayStateValue M2mCustomRelay0
        {
            get => _m2mCustomRelay0;
            set => SetProperty(ref _m2mCustomRelay0, value);
        }

        private ControlUnitCustomRelayConfigurationValue _m2mCustomRelay0Configuration = ControlUnitCustomRelayConfigurationValue.CMN_M2M_WARNING;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.M2mCustomRelay0Configuration, typeof(ControlUnitCustomRelayConfigurationValue))]
		public ControlUnitCustomRelayConfigurationValue M2mCustomRelay0Configuration
		{
			get => _m2mCustomRelay0Configuration;
			set => SetProperty(ref _m2mCustomRelay0Configuration, value);
		}

        private CockpitControlUnitRelayStateValue _m2mCustomRelay1 = CockpitControlUnitRelayStateValue.NotActive;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.M2mCustomRelay1, typeof(CockpitControlUnitRelayStateValue))]
		public CockpitControlUnitRelayStateValue M2mCustomRelay1
        {
            get => _m2mCustomRelay1;
            set => SetProperty(ref _m2mCustomRelay1, value);
        }

        private ControlUnitCustomRelayConfigurationValue _m2mCustomRelay1Configuration = ControlUnitCustomRelayConfigurationValue.CMN_M2M_LEVEL_HIGH;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.M2mCustomRelay1Configuration, typeof(ControlUnitCustomRelayConfigurationValue))]
		public ControlUnitCustomRelayConfigurationValue M2mCustomRelay1Configuration
		{
			get => _m2mCustomRelay1Configuration;
			set => SetProperty(ref _m2mCustomRelay1Configuration, value);
		}

        private CockpitControlUnitRelayStateValue _m2mCustomRelay2 = CockpitControlUnitRelayStateValue.NotActive;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.M2mCustomRelay2, typeof(CockpitControlUnitRelayStateValue))]
		public CockpitControlUnitRelayStateValue M2mCustomRelay2
        {
            get => _m2mCustomRelay2;
            set => SetProperty(ref _m2mCustomRelay2, value);
        }

        private ControlUnitCustomRelayConfigurationValue _m2mCustomRelay2Configuration = ControlUnitCustomRelayConfigurationValue.CMN_M2M_ALARM_MOTOR_0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.M2mCustomRelay2Configuration, typeof(ControlUnitCustomRelayConfigurationValue))]
		public ControlUnitCustomRelayConfigurationValue M2mCustomRelay2Configuration
		{
			get => _m2mCustomRelay2Configuration;
			set => SetProperty(ref _m2mCustomRelay2Configuration, value);
		}

        private CockpitControlUnitExternalOffStateValue _m2mExternalOffActive = CockpitControlUnitExternalOffStateValue.NotActive;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.M2mExternalOffActive, typeof(CockpitControlUnitExternalOffStateValue))]
		public CockpitControlUnitExternalOffStateValue M2mExternalOffActive
        {
            get => _m2mExternalOffActive;
            set => SetProperty(ref _m2mExternalOffActive, value);
        }

        private ControlUnitDatalinkStatusValue _datalinkRemoteStatus = ControlUnitDatalinkStatusValue.CMN_DATALINK_STATUS_OFF;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.DatalinkRemoteStatus, typeof(ControlUnitDatalinkStatusValue))]
		public ControlUnitDatalinkStatusValue DatalinkRemoteStatus
		{
			get => _datalinkRemoteStatus;
			set => SetProperty(ref _datalinkRemoteStatus, value);
		}

		private ControlUnitDatalinkStatusValue _datalinkServiceStatus = ControlUnitDatalinkStatusValue.CMN_DATALINK_STATUS_OFF;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.DatalinkServiceStatus, typeof(ControlUnitDatalinkStatusValue))]
		public ControlUnitDatalinkStatusValue DatalinkServiceStatus
		{
			get => _datalinkServiceStatus;
			set => SetProperty(ref _datalinkServiceStatus, value);
		}

		private double _datalinkRemotePendingTimeout = 30;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.DatalinkRemotePendingTimeout, typeof(decimal))]
		public double DatalinkRemotePendingTimeout
		{
			get => _datalinkRemotePendingTimeout;
			set => SetProperty(ref _datalinkRemotePendingTimeout, value);
		}

		private double _datalinkConnectionTimeout = 5;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.DatalinkConnectionTimeout, typeof(decimal))]
		public double DatalinkConnectionTimeout
		{
			get => _datalinkConnectionTimeout;
			set => SetProperty(ref _datalinkConnectionTimeout, value);
		}

		private double _datalinkResponseTimeout = 5;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.DatalinkResponseTimeout, typeof(decimal))]
		public double DatalinkResponseTimeout
		{
			get => _datalinkResponseTimeout;
			set => SetProperty(ref _datalinkResponseTimeout, value);
		}

        private CockpitControlUnitButtonPressedStateValue _hmiBtnMotor0ManualIn = CockpitControlUnitButtonPressedStateValue.NotPressed;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.HmiBtnMotor0ManualIn, typeof(CockpitControlUnitButtonPressedStateValue))]
		public CockpitControlUnitButtonPressedStateValue HmiBtnMotor0ManualIn
        {
            get => _hmiBtnMotor0ManualIn;
            set => SetProperty(ref _hmiBtnMotor0ManualIn, value);
        }

        private CockpitControlUnitButtonPressedStateValue _hmiBtnMotor0AutoOffIn = CockpitControlUnitButtonPressedStateValue.NotPressed;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.HmiBtnMotor0AutoOffIn, typeof(CockpitControlUnitButtonPressedStateValue))]
		public CockpitControlUnitButtonPressedStateValue HmiBtnMotor0AutoOffIn
        {
            get => _hmiBtnMotor0AutoOffIn;
            set => SetProperty(ref _hmiBtnMotor0AutoOffIn, value);
        }

        private CockpitControlUnitButtonPressedStateValue _hmiBtnMotor1ManualIn = CockpitControlUnitButtonPressedStateValue.NotPressed;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.HmiBtnMotor1ManualIn, typeof(CockpitControlUnitButtonPressedStateValue))]
		public CockpitControlUnitButtonPressedStateValue HmiBtnMotor1ManualIn
        {
            get => _hmiBtnMotor1ManualIn;
            set => SetProperty(ref _hmiBtnMotor1ManualIn, value);
        }

        private CockpitControlUnitButtonPressedStateValue _hmiBtnMotor1AutoOffIn = CockpitControlUnitButtonPressedStateValue.NotPressed;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.HmiBtnMotor1AutoOffIn, typeof(CockpitControlUnitButtonPressedStateValue))]
		public CockpitControlUnitButtonPressedStateValue HmiBtnMotor1AutoOffIn
        {
            get => _hmiBtnMotor1AutoOffIn;
            set => SetProperty(ref _hmiBtnMotor1AutoOffIn, value);
        }

        private CockpitControlUnitButtonPressedStateValue _hmiBtnMuteShortIn = CockpitControlUnitButtonPressedStateValue.NotPressed;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.HmiBtnMuteShortIn, typeof(CockpitControlUnitButtonPressedStateValue))]
		public CockpitControlUnitButtonPressedStateValue HmiBtnMuteShortIn
        {
            get => _hmiBtnMuteShortIn;
            set => SetProperty(ref _hmiBtnMuteShortIn, value);
        }

        private CockpitControlUnitButtonPressedStateValue _hmiBtnMuteLongIn = CockpitControlUnitButtonPressedStateValue.NotPressed;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.HmiBtnMuteLongIn, typeof(CockpitControlUnitButtonPressedStateValue))]
		public CockpitControlUnitButtonPressedStateValue HmiBtnMuteLongIn
        {
            get => _hmiBtnMuteLongIn;
            set => SetProperty(ref _hmiBtnMuteLongIn, value);
        }

        private CockpitControlUnitButtonPressedStateValue _hmiBtnConnectionIn = CockpitControlUnitButtonPressedStateValue.NotPressed;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.HmiBtnConnectionIn, typeof(CockpitControlUnitButtonPressedStateValue))]
		public CockpitControlUnitButtonPressedStateValue HmiBtnConnectionIn
        {
            get => _hmiBtnConnectionIn;
            set => SetProperty(ref _hmiBtnConnectionIn, value);
        }

        private CockpitControlUnitKeylockStateValue _hmiBtnKeylockStatus = CockpitControlUnitKeylockStateValue.NotLocked;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.HmiKeylockStatus, typeof(CockpitControlUnitKeylockStateValue))]
		public CockpitControlUnitKeylockStateValue HmiBtnKeylockStatus
        {
            get => _hmiBtnKeylockStatus;
            set => SetProperty(ref _hmiBtnKeylockStatus, value);
        }

        private CockpitControlUnitAudioFeedbackStateValue _hmiButtonFeedback = CockpitControlUnitAudioFeedbackStateValue.Disabled;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.HmiButtonFeedback, typeof(CockpitControlUnitAudioFeedbackStateValue))]
		public CockpitControlUnitAudioFeedbackStateValue HmiButtonFeedback
        {
            get => _hmiButtonFeedback;
            set => SetProperty(ref _hmiButtonFeedback, value);
        }

        private double _statisticSystemOperatingTotal = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticSystemOperatingTotal, typeof(double))]
		public double StatisticSystemOperatingTotal
		{
			get => _statisticSystemOperatingTotal;
			set => SetProperty(ref _statisticSystemOperatingTotal, value);
		}

		private double _statisticMotorOperatingTotal = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorOperatingTotal, typeof(double))]
		public double StatisticMotorOperatingTotal
		{
			get => _statisticMotorOperatingTotal;
			set => SetProperty(ref _statisticMotorOperatingTotal, value);
		}

		private double _statisticMotorSwitchesTotal = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorSwitchesTotal, typeof(double))]
		public double StatisticMotorSwitchesTotal
		{
			get => _statisticMotorSwitchesTotal;
			set => SetProperty(ref _statisticMotorSwitchesTotal, value);
		}

		private double _statisticMotorOperatingMotor0Total = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorOperatingMotor0Total, typeof(double))]
		public double StatisticMotorOperatingMotor0Total
		{
			get => _statisticMotorOperatingMotor0Total;
			set => SetProperty(ref _statisticMotorOperatingMotor0Total, value);
		}

		private double _statisticMotorOperatingMotor024h = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorOperatingMotor024h, typeof(double))]
		public double StatisticMotorOperatingMotor024h
		{
			get => _statisticMotorOperatingMotor024h;
			set => SetProperty(ref _statisticMotorOperatingMotor024h, value);
		}

		private double _statisticMotorOperatingMotor07d = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorOperatingMotor07d, typeof(double))]
		public double StatisticMotorOperatingMotor07d
		{
			get => _statisticMotorOperatingMotor07d;
			set => SetProperty(ref _statisticMotorOperatingMotor07d, value);
		}

		private double _statisticMotorOperatingMotor01m = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorOperatingMotor01m, typeof(double))]
		public double StatisticMotorOperatingMotor01m
		{
			get => _statisticMotorOperatingMotor01m;
			set => SetProperty(ref _statisticMotorOperatingMotor01m, value);
		}

		private double _statisticMotorSwitchesMotor0Total = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorSwitchesMotor0Total, typeof(double))]
		public double StatisticMotorSwitchesMotor0Total
		{
			get => _statisticMotorSwitchesMotor0Total;
			set => SetProperty(ref _statisticMotorSwitchesMotor0Total, value);
		}

		private double _statisticMotorSwitchesMotor024h = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorSwitchesMotor024h, typeof(double))]
		public double StatisticMotorSwitchesMotor024h
		{
			get => _statisticMotorSwitchesMotor024h;
			set => SetProperty(ref _statisticMotorSwitchesMotor024h, value);
		}

		private double _statisticMotorSwitchesMotor07d = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorSwitchesMotor07d, typeof(double))]
		public double StatisticMotorSwitchesMotor07d
		{
			get => _statisticMotorSwitchesMotor07d;
			set => SetProperty(ref _statisticMotorSwitchesMotor07d, value);
		}

		private double _statisticMotorSwitchesMotor01m = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorSwitchesMotor01m, typeof(double))]
		public double StatisticMotorSwitchesMotor01m
		{
			get => _statisticMotorSwitchesMotor01m;
			set => SetProperty(ref _statisticMotorSwitchesMotor01m, value);
		}

		private double _statisticMotorOperatingMotor1Total = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorOperatingMotor1Total, typeof(double))]
		public double StatisticMotorOperatingMotor1Total
		{
			get => _statisticMotorOperatingMotor1Total;
			set => SetProperty(ref _statisticMotorOperatingMotor1Total, value);
		}

		private double _statisticMotorOperatingMotor124h = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorOperatingMotor124h, typeof(double))]
		public double StatisticMotorOperatingMotor124h
		{
			get => _statisticMotorOperatingMotor124h;
			set => SetProperty(ref _statisticMotorOperatingMotor124h, value);
		}

		private double _statisticMotorOperatingMotor17d = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorOperatingMotor17d, typeof(double))]
		public double StatisticMotorOperatingMotor17d
		{
			get => _statisticMotorOperatingMotor17d;
			set => SetProperty(ref _statisticMotorOperatingMotor17d, value);
		}

		private double _statisticMotorOperatingMotor11m = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorOperatingMotor11m, typeof(double))]
		public double StatisticMotorOperatingMotor11m
		{
			get => _statisticMotorOperatingMotor11m;
			set => SetProperty(ref _statisticMotorOperatingMotor11m, value);
		}

		private double _statisticMotorSwitchesMotor1Total = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorSwitchesMotor1Total, typeof(double))]
		public double StatisticMotorSwitchesMotor1Total
		{
			get => _statisticMotorSwitchesMotor1Total;
			set => SetProperty(ref _statisticMotorSwitchesMotor1Total, value);
		}

		private double _statisticMotorSwitchesMotor124h = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorSwitchesMotor124h, typeof(double))]
		public double StatisticMotorSwitchesMotor124h
		{
			get => _statisticMotorSwitchesMotor124h;
			set => SetProperty(ref _statisticMotorSwitchesMotor124h, value);
		}

		private double _statisticMotorSwitchesMotor17d = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorSwitchesMotor17d, typeof(double))]
		public double StatisticMotorSwitchesMotor17d
		{
			get => _statisticMotorSwitchesMotor17d;
			set => SetProperty(ref _statisticMotorSwitchesMotor17d, value);
		}

		private double _statisticMotorSwitchesMotor11m = 0;
		[ControlUnitParam(ExternalControlUnitParameterIdValue.StatisticMotorSwitchesMotor11m, typeof(double))]
		public double StatisticMotorSwitchesMotor11m
		{
			get => _statisticMotorSwitchesMotor11m;
			set => SetProperty(ref _statisticMotorSwitchesMotor11m, value);
		}

		private bool _cmdSystemConfirmAlarm = false;
		public bool CmdSystemConfirmAlarm
		{
			get => _cmdSystemConfirmAlarm;
			set => SetProperty(ref _cmdSystemConfirmAlarm, value);
		}

		private bool _cmdSystemConfirmMaintenanceWarning = false;
		public bool CmdSystemConfirmAlarmMaintenance
		{
			get => _cmdSystemConfirmMaintenanceWarning;
			set => SetProperty(ref _cmdSystemConfirmMaintenanceWarning, value);
		}

		private bool _cmdM2MTestAlarmRelay = false;
		public bool CmdM2MTestAlarmRelay
		{
			get => _cmdM2MTestAlarmRelay;
			set => SetProperty(ref _cmdM2MTestAlarmRelay, value);
		}

		private bool _cmdM2MTestCustomRelay0 = false;
		public bool CmdM2MTestCustomRelay0
		{
			get => _cmdM2MTestCustomRelay0;
			set => SetProperty(ref _cmdM2MTestCustomRelay0, value);
		}

		private bool _cmdM2MTestCustomRelay1 = false;
		public bool CmdM2MTestCustomRelay1
		{
			get => _cmdM2MTestCustomRelay1;
			set => SetProperty(ref _cmdM2MTestCustomRelay1, value);
		}

		private bool _cmdM2MTestCustomRelay2 = false;
		public bool CmdM2MTestCustomRelay2
		{
			get => _cmdM2MTestCustomRelay2;
			set => SetProperty(ref _cmdM2MTestCustomRelay2, value);
		}

		private bool _cmdHmiTestAlarmHorn = false;
		public bool CmdHmiTestAlarmHorn
		{
			get => _cmdHmiTestAlarmHorn;
			set => SetProperty(ref _cmdHmiTestAlarmHorn, value);
		}

		private bool _cmdHmiTestAlarmBuzzer = false;
		public bool CmdHmiTestAlarmBuzzer
		{
			get => _cmdHmiTestAlarmBuzzer;
			set => SetProperty(ref _cmdHmiTestAlarmBuzzer, value);
		}

		private bool _cmdStatisticReset = false;
		public bool CmdStatisticReset
		{
			get => _cmdStatisticReset;
			set => SetProperty(ref _cmdStatisticReset, value);
		}
	}
}
