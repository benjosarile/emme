﻿using EmmeApp.Common.Attributes;
using EmmeApp.Common.Enum;
using Prism.Mvvm;
using System;

namespace EmmeApp.Common.PageModels.Cockpit
{
	public class CockpitPumpRootModel : BindableBase
    {
        private Guid _selectedDeviceUuid;
        public Guid SelectedDeviceUuid
        {
            get => _selectedDeviceUuid;
            set => SetProperty(ref _selectedDeviceUuid, value);
        }

        private double _flowRate = 105;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.FlowRate, typeof(double))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.FlowRate, typeof(double))]
        public double FlowRate
        {
            get => _flowRate;
            set => SetProperty(ref _flowRate, value);
        }

        private double _flowHeight = 83;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.FlowHeight, typeof(double))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.FlowHeight, typeof(double))]
        public double FlowHeight
        {
            get => _flowHeight;
            set => SetProperty(ref _flowHeight, value);
        }


        private double _powerOutput = 751;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.PowerOutput, typeof(double))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.PowerOutput, typeof(double))]
        public double PowerOutput
        {
            get => _powerOutput;
            set => SetProperty(ref _powerOutput, value);
        }

        private PumpControlTypeValue _controlType = PumpControlTypeValue.ConstantPressure;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.ControlType, typeof(PumpControlTypeValue))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.ControlType, typeof(PumpControlTypeValue))]
        public PumpControlTypeValue ControlType
        {
            get => _controlType;
            set => SetProperty(ref _controlType, value);
        }

        private double _nominalValue = 85;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.NominalValue, typeof(double))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.NominalValue, typeof(double))]
        public double NominalValue
        {
            get => _nominalValue;
            set => SetProperty(ref _nominalValue, value);
        }

        private PumpSystemStatusValue _systemStatus = PumpSystemStatusValue.No_Fault;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.SystemStatus, typeof(PumpSystemStatusValue))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.SystemStatus, typeof(PumpSystemStatusValue))]
        public PumpSystemStatusValue SystemStatus
        {
            get => _systemStatus;
            set => SetProperty(ref _systemStatus, value);
        }

        private PumpTypeOfOperationValue _typeOfOperation = PumpTypeOfOperationValue.ON;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.TypeOfOperation, typeof(PumpTypeOfOperationValue))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.TypeOfOperation, typeof(PumpTypeOfOperationValue))]
        public PumpTypeOfOperationValue TypeOfOperation
        {
            get => _typeOfOperation;
            set => SetProperty(ref _typeOfOperation, value);
        }

        private string _pumpType;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.Pumptype, typeof(string))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.PumpType, typeof(string))]
        public string PumpType
        {
            get => _pumpType;
            set => SetProperty(ref _pumpType, value);
        }

        private string _swVersion;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.SwVersion, typeof(string))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.SwVersion, typeof(string))]
        public string SwVersion
        {
            get => _swVersion;
            set => SetProperty(ref _swVersion, value);
        }

        private string _serialNumber;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.SerialNumber, typeof(string))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.SerialNumber, typeof(string))]
        public string SerialNumber
        {
            get => _serialNumber;
            set => SetProperty(ref _serialNumber, value);
        }

        private string _manufactureDate;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.ManufactureDate, typeof(string))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.ManufactureDate, typeof(string))]
        public string ManufactureDate
        {
            get => _manufactureDate;
            set => SetProperty(ref _manufactureDate, value);
        }

        private long _time;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.Time, typeof(long))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.Time, typeof(long))]
        public long Time
        {
            get => _time;
            set => SetProperty(ref _time, value);
        }

        private double _speed;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.Speed, typeof(double))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.Speed, typeof(double))]
        public double Speed
        {
            get => _speed;
            set => SetProperty(ref _speed, value);
        }

        private int _operatingHours;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.OperatingHours, typeof(int))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.OperatingHours, typeof(int))]
        public int OperatingHours
        {
            get => _operatingHours;
            set => SetProperty(ref _operatingHours, value);
        }


        private int _electricalEnergy;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.ElectricalEnergy, typeof(int))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.ElectricalEnergy, typeof(int))]
        public int ElectricalEnergy
        {
            get => _electricalEnergy;
            set => SetProperty(ref _electricalEnergy, value);
        }

        private PumpCurrentAlarmValue _currentAlarm = PumpCurrentAlarmValue.No_Fault;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.CurrentAlarm, typeof(PumpCurrentAlarmValue))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.CurrentAlarm, typeof(PumpCurrentAlarmValue))]
        public PumpCurrentAlarmValue CurrentAlarm
        {
            get => _currentAlarm;
            set => SetProperty(ref _currentAlarm, value);
        }

        private PumpCurrentWarningValue _currentWarning = PumpCurrentWarningValue.No_Fault;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.CurrentWarning, typeof(PumpCurrentWarningValue))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.CurrentWarning, typeof(PumpCurrentWarningValue))]
        public PumpCurrentWarningValue CurrentWarning
        {
            get => _currentWarning;
            set => SetProperty(ref _currentWarning, value);
        }

        private PumpAdditionalModuleValue _additionalModule = PumpAdditionalModuleValue.NoAdditionalModulePluggedIn;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.AdditionalModule, typeof(PumpAdditionalModuleValue))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.AdditionalModule, typeof(PumpAdditionalModuleValue))]
        public PumpAdditionalModuleValue AdditionalModule
        {
            get => _additionalModule;
            set => SetProperty(ref _additionalModule, value);
        }

        private PumpSignalOriginValue _signalOrigin = PumpSignalOriginValue.Pump;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.SignalOrigin, typeof(PumpSignalOriginValue))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.SignalOrigin, typeof(PumpSignalOriginValue))]
        public PumpSignalOriginValue SignalOrigin
        {
            get => _signalOrigin;
            set => SetProperty(ref _signalOrigin, value);
        }

        private PumpKeyLockValue _keyLock = PumpKeyLockValue.KeysNotLocked;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.KeyLock, typeof(PumpKeyLockValue))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.KeyLock, typeof(PumpKeyLockValue))]
        public PumpKeyLockValue KeyLock
        {
            get => _keyLock;
            set => SetProperty(ref _keyLock, value);
        }

        private int _pumpNumber = 1;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.PumpNumber, typeof(int))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.PumpNumber, typeof(int))]
        public int PumpNumber
        {
            get => _pumpNumber;
            set => SetProperty(ref _pumpNumber, value);
        }

        private PumpDigInValue _digIn1011 = PumpDigInValue.Min;
        [PumpModulAParam(ExternalPumpModulAParameterIdValue.DigIn1011, typeof(PumpDigInValue))]
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.DigIn1011, typeof(PumpDigInValue))]
        public PumpDigInValue DigIn1011
        {
            get => _digIn1011;
            set => SetProperty(ref _digIn1011, value);
        }


    }
}
