﻿using Prism.Mvvm;

namespace EmmeApp.Common.PageModels.Cockpit
{
    public class CockpitPumpsModulASettingsModel : BindableBase
    {
        private bool _cmdSystemConfirmAlarm = false;
        public bool CmdSystemConfirmAlarm
        {
            get => _cmdSystemConfirmAlarm;
            set => SetProperty(ref _cmdSystemConfirmAlarm, value);
        }
    }
}
