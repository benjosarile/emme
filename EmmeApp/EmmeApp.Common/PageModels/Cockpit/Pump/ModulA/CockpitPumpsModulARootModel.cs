﻿using EmmeApp.Common.Attributes;
using EmmeApp.Common.Enum;

namespace EmmeApp.Common.PageModels.Cockpit
{
    public class CockpitPumpsModulARootModel : CockpitPumpRootModel
   {
      private double _mediaTemperature = 74;
      [PumpModulAParam(ExternalPumpModulAParameterIdValue.MediaTemperature, typeof(double))]
      public double MediaTemperature
      {
         get => _mediaTemperature;
         set => SetProperty(ref _mediaTemperature, value);
      }

      private PumpRelayFunctionValue _relayFunction = PumpRelayFunctionValue.Operating_Relay;
      [PumpModulAParam(ExternalPumpModulAParameterIdValue.RelayFunction, typeof(PumpRelayFunctionValue))]
      public PumpRelayFunctionValue RelayFunction
      {
         get => _relayFunction;
         set => SetProperty(ref _relayFunction, value);
      }

      private PumpPowerLimitValue _powerLimit = PumpPowerLimitValue.PowerLimitOff;
      [PumpModulAParam(ExternalPumpModulAParameterIdValue.PowerLimitEn, typeof(PumpPowerLimitValue))]
      public PumpPowerLimitValue PowerLimit
      {
         get => _powerLimit;
         set => SetProperty(ref _powerLimit, value);
      }

      private double _powerLimitMax = 0;
      [PumpModulAParam(ExternalPumpModulAParameterIdValue.PowerLimitMax, typeof(double))]
      public double PowerLimitMax
      {
         get => _powerLimitMax;
         set => SetProperty(ref _powerLimitMax, value);
      }

      private PumpModulASwitch1Value _switch1 = PumpModulASwitch1Value.FaultSignal;
      [PumpModulAParam(ExternalPumpModulAParameterIdValue.Switch1, typeof(PumpModulASwitch1Value))]
      public PumpModulASwitch1Value Switch1
      {
         get => _switch1;
         set => SetProperty(ref _switch1, value);
      }

      private PumpModulASwitch2Value _switch2 = PumpModulASwitch2Value.ExternOff;
      [PumpModulAParam(ExternalPumpModulAParameterIdValue.Switch2, typeof(PumpModulASwitch2Value))]
      public PumpModulASwitch2Value Switch2
      {
         get => _switch2;
         set => SetProperty(ref _switch2, value);
      }

      private PumpModulASwitch3Value _switch3 = PumpModulASwitch3Value.PowerLimitOff;
      [PumpModulAParam(ExternalPumpModulAParameterIdValue.Switch3, typeof(PumpModulASwitch3Value))]
      public PumpModulASwitch3Value Switch3
      {
         get => _switch3;
         set => SetProperty(ref _switch3, value);
      }

		private string _pumpVersionType;
		[PumpModulAParam(ExternalPumpModulAParameterIdValue.PumpVersionType, typeof(string))]
		public string PumpVersionType
		{
			get => _pumpVersionType;
			set => SetProperty(ref _pumpVersionType, value);
		}

		private bool _cmdSystemConfirmAlarm = false;
      public bool CmdSystemConfirmAlarm
      {
         get => _cmdSystemConfirmAlarm;
         set => SetProperty(ref _cmdSystemConfirmAlarm, value);
      }


   }
}
