﻿using EmmeApp.Common.Enum;
using Prism.Mvvm;

namespace EmmeApp.Common.PageModels.Cockpit
{
    public class CockpitPumpsModulAOverviewModel : BindableBase
    {
        private double _flowRate = 105;
        public double FlowRate
        {
            get => _flowRate;
            set => SetProperty(ref _flowRate, value);
        }

        private double _flowHeight = 83;
        public double FlowHeight
        {
            get => _flowHeight;
            set => SetProperty(ref _flowHeight, value);
        }

        private double _mediaTemperature = 74;
        public double MediaTemperature
        {
            get => _mediaTemperature;
            set => SetProperty(ref _mediaTemperature, value);
        }

        private double _powerOutput = 751;
        public double PowerOutput
        {
            get => _powerOutput;
            set => SetProperty(ref _powerOutput, value);
        }

        private PumpControlTypeValue _controlType = PumpControlTypeValue.ConstantPressure;
        public PumpControlTypeValue ControlType
        {
            get => _controlType;
            set => SetProperty(ref _controlType, value);
        }

        private double _nominalValue = 85;
        public double NominalValue
        {
            get => _nominalValue;
            set => SetProperty(ref _nominalValue, value);
        }

        private PumpSystemStatusValue _systemStatus = PumpSystemStatusValue.No_Fault;
        public PumpSystemStatusValue SystemStatus
        {
            get => _systemStatus;
            set => SetProperty(ref _systemStatus, value);
        }

        private PumpTypeOfOperationValue _typeOfOperation = PumpTypeOfOperationValue.ON;
        public PumpTypeOfOperationValue TypeOfOperation
        {
            get => _typeOfOperation;
            set => SetProperty(ref _typeOfOperation, value);
        }
    }
}
