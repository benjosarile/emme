﻿using EmmeApp.Common.Enum;
using Prism.Mvvm;

namespace EmmeApp.Common.PageModels.Cockpit
{
    public class CockpitPumpsVariAEStatusModel : BindableBase
    {

        private string _pumpType;
        public string PumpType
        {
            get => _pumpType;
            set => SetProperty(ref _pumpType, value);
        }

        private string _swVersion;
        public string SwVersion
        {
            get => _swVersion;
            set => SetProperty(ref _swVersion, value);
        }

        private string _serialNumber;
        public string SerialNumber
        {
            get => _serialNumber;
            set => SetProperty(ref _serialNumber, value);
        }

        private string _manufactureDate;
        public string ManufactureDate
        {
            get => _manufactureDate;
            set => SetProperty(ref _manufactureDate, value);
        }

        private int _time;
        public int Time
        {
            get => _time;
            set => SetProperty(ref _time, value);
        }

        private double _flowRate = 105;
        public double FlowRate
        {
            get => _flowRate;
            set => SetProperty(ref _flowRate, value);
        }

        private double _flowHeight = 83;
        public double FlowHeight
        {
            get => _flowHeight;
            set => SetProperty(ref _flowHeight, value);
        }


        private double _powerOutput = 751;
        public double PowerOutput
        {
            get => _powerOutput;
            set => SetProperty(ref _powerOutput, value);
        }

        private double _speed = 4433;
        public double Speed
        {
            get => _speed;
            set => SetProperty(ref _speed, value);
        }

        private PumpControlTypeValue _controlType = PumpControlTypeValue.ConstantPressure;
        public PumpControlTypeValue ControlType
        {
            get => _controlType;
            set => SetProperty(ref _controlType, value);
        }

        private double _nominalValue = 85;
        public double NominalValue
        {
            get => _nominalValue;
            set => SetProperty(ref _nominalValue, value);
        }

        private int _operatingHours = 127;
        public int OperatingHours
        {
            get => _operatingHours;
            set => SetProperty(ref _operatingHours, value);
        }

        private int _electricalEnergy = 4437;
        public int ElectricalEnergy
        {
            get => _electricalEnergy;
            set => SetProperty(ref _electricalEnergy, value);
        }

        private PumpCurrentAlarmValue _currentAlarm = PumpCurrentAlarmValue.No_Fault;
        public PumpCurrentAlarmValue CurrentAlarm
        {
            get => _currentAlarm;
            set => SetProperty(ref _currentAlarm, value);
        }

        private PumpCurrentWarningValue _currentWarning = PumpCurrentWarningValue.No_Fault;
        public PumpCurrentWarningValue CurrentWarning
        {
            get => _currentWarning;
            set => SetProperty(ref _currentWarning, value);
        }

        private PumpAdditionalModuleValue _additionalModule = PumpAdditionalModuleValue.NoAdditionalModulePluggedIn;
        public PumpAdditionalModuleValue AdditionalModule
        {
            get => _additionalModule;
            set => SetProperty(ref _additionalModule, value);
        }

        private PumpSignalOriginValue _signalOrigin = PumpSignalOriginValue.Pump;
        public PumpSignalOriginValue SignalOrigin
        {
            get => _signalOrigin;
            set => SetProperty(ref _signalOrigin, value);
        }

        private PumpTypeOfOperationValue _typeOfOperation = PumpTypeOfOperationValue.ON;
        public PumpTypeOfOperationValue TypeOfOperation
        {
            get => _typeOfOperation;
            set => SetProperty(ref _typeOfOperation, value);
        }

        private PumpKeyLockValue _keyLock = PumpKeyLockValue.KeysNotLocked;
        public PumpKeyLockValue KeyLock
        {
            get => _keyLock;
            set => SetProperty(ref _keyLock, value);
        }

        private int _pumpNumber = 1;
        public int PumpNumber
        {
            get => _pumpNumber;
            set => SetProperty(ref _pumpNumber, value);
        }

        private PumpRelayFunctionValue _relay1 = PumpRelayFunctionValue.Operating_Relay;
        public PumpRelayFunctionValue Relay1
        {
            get => _relay1;
            set => SetProperty(ref _relay1, value);
        }

        private PumpRelayFunctionValue _relay2 = PumpRelayFunctionValue.Alarm_Relay;
        public PumpRelayFunctionValue Relay2
        {
            get => _relay2;
            set => SetProperty(ref _relay2, value);
        }

        private PumpDigInValue _digIn1011 = PumpDigInValue.Min;
        public PumpDigInValue DigIn1011
        {
            get => _digIn1011;
            set => SetProperty(ref _digIn1011, value);
        }

        private PumpDigInValue _digIn1015 = PumpDigInValue.Max;
        public PumpDigInValue DigIn1015
        {
            get => _digIn1015;
            set => SetProperty(ref _digIn1015, value);
        }

        private PumpDigInValue _digIn1016 = PumpDigInValue.Stop;
        public PumpDigInValue DigIn1016
        {
            get => _digIn1016;
            set => SetProperty(ref _digIn1016, value);
        }

        private PumpVariAESwitch1Value _switch1 = PumpVariAESwitch1Value.FaultSignal;
        public PumpVariAESwitch1Value Switch1
        {
            get => _switch1;
            set => SetProperty(ref _switch1, value);
        }

        private PumpVariAESwitch2Value _switch2 = PumpVariAESwitch2Value.OperationSignal;
        public PumpVariAESwitch2Value Switch2
        {
            get => _switch2;
            set => SetProperty(ref _switch2, value);
        }

        private PumpVariAESwitch3Value _switch3 = PumpVariAESwitch3Value.ExternOff;
        public PumpVariAESwitch3Value Switch3
        {
            get => _switch3;
            set => SetProperty(ref _switch3, value);
        }

        private PumpVariAESwitch4Value _switch4 = PumpVariAESwitch4Value.Value_010V;
        public PumpVariAESwitch4Value Switch4
        {
            get => _switch4;
            set => SetProperty(ref _switch4, value);
        }
    }
}
