﻿using EmmeApp.Common.Attributes;
using EmmeApp.Common.Enum;

namespace EmmeApp.Common.PageModels.Cockpit
{
    public class CockpitPumpsVariAERootModel : CockpitPumpRootModel
	{
        private PumpRelayFunctionValue _relay1 = PumpRelayFunctionValue.Operating_Relay;
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.Relay1, typeof(PumpRelayFunctionValue))]
        public PumpRelayFunctionValue Relay1
		{
			get => _relay1;
			set => SetProperty(ref _relay1, value);
		}

		private PumpRelayFunctionValue _relay2 = PumpRelayFunctionValue.Alarm_Relay;
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.Relay2, typeof(PumpRelayFunctionValue))]
        public PumpRelayFunctionValue Relay2
		{
			get => _relay2;
			set => SetProperty(ref _relay2, value);
		}

		private PumpDigInValue _digIn1015 = PumpDigInValue.Max;
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.DigIn1015, typeof(PumpDigInValue))]
        public PumpDigInValue DigIn1015
		{
			get => _digIn1015;
			set => SetProperty(ref _digIn1015, value);
		}

		private PumpDigInValue _digIn1016 = PumpDigInValue.Stop;
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.DigIn1016, typeof(PumpDigInValue))]
        public PumpDigInValue DigIn1016
		{
			get => _digIn1016;
			set => SetProperty(ref _digIn1016, value);
		}

		private PumpVariAESwitch1Value _switch1 = PumpVariAESwitch1Value.FaultSignal;
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.Switch1, typeof(PumpVariAESwitch1Value))]
        public PumpVariAESwitch1Value Switch1
		{
			get => _switch1;
			set => SetProperty(ref _switch1, value);
		}

		private PumpVariAESwitch2Value _switch2 = PumpVariAESwitch2Value.OperationSignal;
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.Switch2, typeof(PumpVariAESwitch2Value))]
        public PumpVariAESwitch2Value Switch2
		{
			get => _switch2;
			set => SetProperty(ref _switch2, value);
		}

		private PumpVariAESwitch3Value _switch3 = PumpVariAESwitch3Value.ExternOff;
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.Switch3, typeof(PumpVariAESwitch3Value))]
        public PumpVariAESwitch3Value Switch3
		{
			get => _switch3;
			set => SetProperty(ref _switch3, value);
		}

		private PumpVariAESwitch4Value _switch4 = PumpVariAESwitch4Value.Value_010V;
        [PumpVariAParam(ExternalPumpVariAParameterIdValue.Switch4, typeof(PumpVariAESwitch4Value))]
        public PumpVariAESwitch4Value Switch4
		{
			get => _switch4;
			set => SetProperty(ref _switch4, value);
		}

        private bool _cmdSystemConfirmAlarm = false;
        public bool CmdSystemConfirmAlarm
        {
            get => _cmdSystemConfirmAlarm;
            set => SetProperty(ref _cmdSystemConfirmAlarm, value);
        }
    }
}
