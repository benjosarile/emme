﻿namespace EmmeApp.Common.Enum
{
    public enum HttpRequestType
	{
		Delete,
		Get,
		Post,
		Put
	}
}
