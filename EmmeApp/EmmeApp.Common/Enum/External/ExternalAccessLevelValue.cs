﻿namespace EmmeApp.Common.Enum
{
	public enum ExternalAccessLevelValue
	{
		UnknownLevel = 0,
		Dev = 1,
		Manufact = 2,
		Service = 3,
		User = 4
	}
}
