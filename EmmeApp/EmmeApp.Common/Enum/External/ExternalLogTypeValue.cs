﻿namespace EmmeApp.Common.Enum
{
	public enum ExternalLogTypeValue
	{
		UnknownLogType = 0,
		NoLogType = 1,
		Info = 2,
		Warning = 3,
		Alarm = 4,
		Failure = 5
	}
}
