﻿namespace EmmeApp.Common.Enum
{
	public enum ExternalValueOneofCaseValue
	{
		None = 0,
		Text = 1,
		Number = 2,
	}
}
