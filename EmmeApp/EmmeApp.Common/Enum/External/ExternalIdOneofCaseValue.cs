﻿namespace EmmeApp.Common.Enum
{
    public enum ExternalIdOneofCaseValue
	{
		None = 0,
		ControlUnit = 1,
		ModulA = 2,
		VariA = 3
	}
}
