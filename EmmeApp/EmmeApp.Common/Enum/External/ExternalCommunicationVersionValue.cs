﻿namespace EmmeApp.Common.Enum
{
	public enum ExternalCommunicationVersionValue
	{
		UnknownLevel = 0,
		Version1 = 1
	}
}
