﻿namespace EmmeApp.Common.Enum
{
	public enum ExternalUnitTValue
	{
		UnknownUnit = 0,
		NoUnit = 1,
		Ampere = 2,
		Miliampere = 3,
		Seconds = 4,
		Days = 5,
		CubicMeterPerHour = 6,
		Watt = 7,
		Meter = 8,
		M3 = 9,
		Pmin = 10,
		Kwh = 11,
		Hours = 12,
		Percent = 13,
		Celsius = 14
	}
}
