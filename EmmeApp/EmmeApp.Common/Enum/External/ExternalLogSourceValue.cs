﻿namespace EmmeApp.Common.Enum
{
	public enum ExternalLogSourceValue
	{
		UnknownLogSource = 0,
		NoLogSource = 1,
		External = 2,
		Internal = 3,
	}
}
