﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum PumpVariAESwitch2Value
	{
		[Display(Description = nameof(AppResources.Pump_Switch_OperationSignal), ResourceType = typeof(AppResources))]
		OperationSignal = 0,
		[Display(Description = nameof(AppResources.Pump_Switch_ReadySignal), ResourceType = typeof(AppResources))]
		ReadySignal = 1
	}
}
