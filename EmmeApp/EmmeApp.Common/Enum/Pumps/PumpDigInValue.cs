﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum PumpDigInValue
	{
		[Display(Description = nameof(AppResources.Pump_DigIn_Min), ResourceType = typeof(AppResources))]
		Min = 0,
		[Display(Description = nameof(AppResources.Pump_DigIn_Max), ResourceType = typeof(AppResources))]
		Max = 1,
		[Display(Description = nameof(AppResources.Pump_DigIn_Stop), ResourceType = typeof(AppResources))]
		Stop = 2,
		[Display(Description = nameof(AppResources.Pump_DigIn_AlarmReset), ResourceType = typeof(AppResources))]
		AlarmReset = 4,
		[Display(Description = nameof(AppResources.Pump_DigIn_NotActive), ResourceType = typeof(AppResources))]
		NotActive = 254,
		[Display(Description = nameof(AppResources.Pump_DigIn_DataItemNotAvailable), ResourceType = typeof(AppResources))]
		DataItemNotAvailable = 255
	}
}
