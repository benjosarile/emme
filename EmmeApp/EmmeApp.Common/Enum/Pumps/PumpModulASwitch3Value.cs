﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum PumpModulASwitch3Value
	{
		[Display(Description = nameof(AppResources.Pump_Switch_PowerLimitOff), ResourceType = typeof(AppResources))]
		PowerLimitOff = 0,
		[Display(Description = nameof(AppResources.Pump_Switch_PowerLimitOn), ResourceType = typeof(AppResources))]
		PowerLimitOn = 1
	}
}
