﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
    public enum PumpVariAESwitch1Value
	{
		[Display(Description = nameof(AppResources.Pump_Switch_FaultSignal), ResourceType = typeof(AppResources))]
		FaultSignal = 0,
		[Display(Description = nameof(AppResources.Pump_Switch_OperationSignal) , ResourceType = typeof(AppResources))]
		OperationSignal = 1
	}
}
