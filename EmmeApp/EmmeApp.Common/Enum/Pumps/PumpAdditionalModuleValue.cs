﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
    public enum PumpAdditionalModuleValue
    {
        [Display(Description = nameof(AppResources.Pump_AdditionalModule_NoAdditionalModulePluggedIn), ResourceType = typeof(AppResources))]
        NoAdditionalModulePluggedIn = 0,
        
        [Display(Description = nameof(AppResources.Pump_AdditionalModule_AdditionalModulePluggedIn), ResourceType = typeof(AppResources))]
        AdditionalModulePluggedIn = 1,

        [Display(Description = nameof(AppResources.Pump_AdditionalModule_CIM100), ResourceType = typeof(AppResources))]
        CIM100 = 2,

        [Display(Description = nameof(AppResources.Pump_AdditionalModule_CIM110), ResourceType = typeof(AppResources))]
        CIM110 = 3,

        [Display(Description = nameof(AppResources.Pump_AdditionalModule_CIM150), ResourceType = typeof(AppResources))]
        CIM150 = 4,

        [Display(Description = nameof(AppResources.Pump_AdditionalModule_CIM200), ResourceType = typeof(AppResources))]
        CIM200 = 5,

        [Display(Description = nameof(AppResources.Pump_AdditionalModule_CIM250), ResourceType = typeof(AppResources))]
        CIM250 = 6,

        [Display(Description = nameof(AppResources.Pump_AdditionalModule_CIM270), ResourceType = typeof(AppResources))]
        CIM270 = 7,

        [Display(Description = nameof(AppResources.Pump_AdditionalModule_CIM300), ResourceType = typeof(AppResources))]
        CIM300 = 8,

        [Display(Description = nameof(AppResources.Pump_AdditionalModule_CIM500), ResourceType = typeof(AppResources))]
        CIM500 = 9,

        [Display(Description = nameof(AppResources.Pump_AdditionalModule_BIMA2), ResourceType = typeof(AppResources))]
        BIMA2 = 201,

        [Display(Description = nameof(AppResources.Pump_AdditionalModule_BIMB3), ResourceType = typeof(AppResources))]
        BIMB3 = 202,

        [Display(Description = nameof(AppResources.Pump_AdditionalModule_BIMC2), ResourceType = typeof(AppResources))]
        BIMC2 = 203,

        [Display(Description = nameof(AppResources.Pump_AdditionalModule_BIMD2), ResourceType = typeof(AppResources))]
        BIMD2 = 204,

        [Display(Description = nameof(AppResources.Pump_AdditionalModule_BIME2), ResourceType = typeof(AppResources))]
        BIME2 = 205,

        [Display(Description = nameof(AppResources.Pump_AdditionalModule_BIMBoost), ResourceType = typeof(AppResources))]
        BIMBoost = 206,
    }
}
