﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum PumpTypeOfOperationValue
	{
		[Display(Description = nameof(AppResources.Pump_TypeOf_Operation_On), ResourceType = typeof(AppResources))]
		ON = 0,

		[Display(Description = nameof(AppResources.Pump_TypeOf_Operation_Stop), ResourceType = typeof(AppResources))]
		STOP = 1,

		[Display(Description = nameof(AppResources.Pump_TypeOf_Operation_Min), ResourceType = typeof(AppResources))]
		MIN = 2,

		[Display(Description = nameof(AppResources.Pump_TypeOf_Operation_Max), ResourceType = typeof(AppResources))]
		MAX = 3,
	}
}
