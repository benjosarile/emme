﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum PumpControlTypeValue
	{
		[Display(Description = nameof(AppResources.Pump_ControlType_ConstantPressure), ResourceType = typeof(AppResources))]
		ConstantPressure = 0,
		[Display(Description = nameof(AppResources.Pump_ControlType_ProportionalPressure), ResourceType = typeof(AppResources))]
		ProportionalPressure = 1,
		[Display(Description = nameof(AppResources.Pump_ControlType_ConstantSpeed), ResourceType = typeof(AppResources))]
		ConstantSpeed = 2
	}
}
