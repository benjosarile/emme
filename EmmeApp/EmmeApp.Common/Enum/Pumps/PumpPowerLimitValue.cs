﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum PumpPowerLimitValue
	{
		[Display(Description = nameof(AppResources.Pump_PowerLimit_PowerLimitOff), ResourceType = typeof(AppResources))]
		PowerLimitOff = 0,
		[Display(Description = nameof(AppResources.Pump_PowerLimit_PowerLimitOn), ResourceType = typeof(AppResources))]
		PowerLimitOn = 1
	}
}
