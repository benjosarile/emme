﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum PumpKeyLockValue
	{
		[Display(Description = nameof(AppResources.Pump_KeyLock_KeysNotLocked), ResourceType = typeof(AppResources))]
		KeysNotLocked = 0,
		[Display(Description = nameof(AppResources.Pump_KeyLock_KeysLocked), ResourceType = typeof(AppResources))]
		KeysLocked = 1
	}
}
