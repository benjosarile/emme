﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum PumpSignalOriginValue
	{
		[Display(Description = nameof(AppResources.Pump_SignalOrigin_Pump), ResourceType = typeof(AppResources))]
		Pump = 0,
		[Display(Description = nameof(AppResources.Pump_SignalOrigin_Network), ResourceType = typeof(AppResources))]
		Network = 1,
		[Display(Description = nameof(AppResources.Pump_SignalOrigin_Remote), ResourceType = typeof(AppResources))]
		Remote = 2,
		[Display(Description = nameof(AppResources.Pump_SignalOrigin_DigitalInputs), ResourceType = typeof(AppResources))]
		DigitalInputs = 3
	}
}
