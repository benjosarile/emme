﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum PumpVariAESwitch3Value
	{
		[Display(Description = nameof(AppResources.Pump_Switch_ExternOff), ResourceType = typeof(AppResources))]
		ExternOff = 0,
		[Display(Description = nameof(AppResources.Pump_Switch_ExternOn), ResourceType = typeof(AppResources))]
		ExternOn = 1
	}
}
