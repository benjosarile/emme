﻿using EmmeApp.Common.Attributes;
using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum PumpCurrentWarningValue
	{
		[Display(Description = nameof(AppResources.Pump_SystemStatus_0), ResourceType = typeof(AppResources))]
		No_Fault = 0,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_1), ResourceType = typeof(AppResources))]
		Leakage_Current = 1,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_2), ResourceType = typeof(AppResources))]
		Main_Phase_Failure = 2,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_3), ResourceType = typeof(AppResources))]
		External_Fault_Signal = 3,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_4), ResourceType = typeof(AppResources))]
		Too_Many_Restarts_From_Standby_Mode_Per_24_Hours = 4,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_10), ResourceType = typeof(AppResources))]
		[LogHelpText(TitleResource = nameof(AppResources.Log_InternalCommunicationFault_Title),
			CauseResource = nameof(AppResources.Log_InternalCommunicationFault_Cause),
			MeasureResource = nameof(AppResources.Log_InternalCommunicationFault_Measure))]
		Internal_Communication_Error = 10,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_14), ResourceType = typeof(AppResources))]
		Electronic_DC_Link_Protection_Activated_EDP = 14,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_16) , ResourceType = typeof(AppResources))]
		Other = 16,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_29), ResourceType = typeof(AppResources))]
		[LogHelpText(TitleResource = nameof(AppResources.Log_TurbinesOperation_Title),
			CauseResource = nameof(AppResources.Log_TurbinesOperation_Cause),
			MeasureResource = nameof(AppResources.Log_TurbinesOperation_Measure))]
		Turbine_Operation_Impellers_Forced_Backwards = 29,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_30) , ResourceType = typeof(AppResources))]
		Motor_Bearings_Need_Change = 30,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_31) , ResourceType = typeof(AppResources))]
		Motor_Varistors_Need_Change = 31,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_32) , ResourceType = typeof(AppResources))]
		[LogHelpText(TitleResource = nameof(AppResources.Log_HighVoltage_Title),
			CauseResource = nameof(AppResources.Log_HighVoltage_Cause),
			MeasureResource = nameof(AppResources.Log_HighVoltage_Measure))]
		Overvoltage = 32,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_40) , ResourceType = typeof(AppResources))]
		[LogHelpText(TitleResource = nameof(AppResources.Log_LowVoltage_Title),
			CauseResource = nameof(AppResources.Log_LowVoltage_Cause),
			MeasureResource = nameof(AppResources.Log_LowVoltage_Measure))]
		Undervoltage = 40,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_41), ResourceType = typeof(AppResources))]
		[LogHelpText(TitleResource = nameof(AppResources.Log_LowVoltage_Title),
			CauseResource = nameof(AppResources.Log_LowVoltage_Cause),
			MeasureResource = nameof(AppResources.Log_LowVoltage_Measure))]
		Undervoltage_Transient = 41,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_45), ResourceType = typeof(AppResources))]
		Voltage_Asymmetry = 45,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_48) , ResourceType = typeof(AppResources))]
		Overload = 48,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_49), ResourceType = typeof(AppResources))]
		Overcurrent_i_line_i_dc_i_mo = 49,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_51), ResourceType = typeof(AppResources))]
		[LogHelpText(TitleResource = nameof(AppResources.Log_MotorJammed_Title),
			CauseResource = nameof(AppResources.Log_MotorJammed_Cause),
			MeasureResource = nameof(AppResources.Log_MotorJammed_Measure))]
		Blocked_Motor_Pump = 51,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_55), ResourceType = typeof(AppResources))]
		Motor_Current_Protection_Activated_MCP = 55,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_56), ResourceType = typeof(AppResources))]
		Underload = 56,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_57) , ResourceType = typeof(AppResources))]
		Dry_Running = 57,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_64) , ResourceType = typeof(AppResources))]
		[LogHelpText(TitleResource = nameof(AppResources.Log_TemperatureTooHigh_Title),
			CauseResource = nameof(AppResources.Log_TemperatureTooHigh_Cause),
			MeasureResource = nameof(AppResources.Log_TemperatureTooHigh_Measure))]
		Overtemperature = 64,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_65), ResourceType = typeof(AppResources))]
		Motor_Temperature = 65,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_66), ResourceType = typeof(AppResources))]
		Overtemperature_Electronics = 66,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_68), ResourceType = typeof(AppResources))]
		Fluid_Temperature_High = 68,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_70) , ResourceType = typeof(AppResources))]
		Thermo_Relay_2_in_motor_eg_Termistor = 70,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_72), ResourceType = typeof(AppResources))]
		[LogHelpText(TitleResource = nameof(AppResources.Log_HardwareFault_Title),
			CauseResource = nameof(AppResources.Log_HardwareFault_Cause),
			MeasureResource = nameof(AppResources.Log_HardwareFault_Measure))]
		Hardware_Fault_Type_1 = 72,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_73), ResourceType = typeof(AppResources))]
		Hardware_Shut_Down_HSD = 73,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_74), ResourceType = typeof(AppResources))]
		[LogHelpText(TitleResource = nameof(AppResources.Log_HighVoltage_Title),
			CauseResource = nameof(AppResources.Log_HighVoltage_Cause),
			MeasureResource = nameof(AppResources.Log_HighVoltage_Measure))]
		Grid_Voltage_High = 74,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_75), ResourceType = typeof(AppResources))]
		[LogHelpText(TitleResource = nameof(AppResources.Log_LowVoltage_Title),
			CauseResource = nameof(AppResources.Log_LowVoltage_Cause),
			MeasureResource = nameof(AppResources.Log_LowVoltage_Measure))]
		Grid_Voltage_Low = 75,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_76), ResourceType = typeof(AppResources))]
		Internal_Communication_Fault = 76,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_77), ResourceType = typeof(AppResources))]
		Twin_Pump_Communication_Fault = 77,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_84) , ResourceType = typeof(AppResources))]
		[LogHelpText(TitleResource = nameof(AppResources.Log_MemoryAccessError_Title),
			CauseResource = nameof(AppResources.Log_MemoryAccessError_Cause),
			MeasureResource = nameof(AppResources.Log_MemoryAccessError_Measure))]
		Memory_Aaccess_Error = 84,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_85), ResourceType = typeof(AppResources))]
		[LogHelpText(TitleResource = nameof(AppResources.Log_FUParameterError_Title),
			CauseResource = nameof(AppResources.Log_FUParameterError_Cause),
			MeasureResource = nameof(AppResources.Log_FUParameterError_Measure))]
		Freq_Conv_Parameter_Verification_Error_EEPROM = 85,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_88) , ResourceType = typeof(AppResources))]
		[LogHelpText(TitleResource = nameof(AppResources.Log_SensorCommunicationFault_Title),
			CauseResource = nameof(AppResources.Log_SensorCommunicationFault_Cause),
			MeasureResource = nameof(AppResources.Log_SensorCommunicationFault_Measure))]
		General_Sensor_Signal_Fault = 88,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_89) , ResourceType = typeof(AppResources))]
		Feedback_Sensor_Signal_Fault = 89,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_91) , ResourceType = typeof(AppResources))]
		Temperature_Sensor_1_Signal_Fault = 91,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_93) , ResourceType = typeof(AppResources))]
		Sensor_2_Signal_Fault = 93,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_96) , ResourceType = typeof(AppResources))]
		Reference_Input_Signal_Fault = 96,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_105) , ResourceType = typeof(AppResources))]
		Electronic_Rectifier_Protection_Activated_ERP = 105,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_106) , ResourceType = typeof(AppResources))]
		Electronic_Inverter_Protection_Activated_EIP = 106,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_148) , ResourceType = typeof(AppResources))]
		Motor_Drive_End_DE_Bearing_Temp = 148,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_149) , ResourceType = typeof(AppResources))]
		Motor_Non_Drive_End_NDE_Bearing_Temp = 149,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_155) , ResourceType = typeof(AppResources))]
		[LogHelpText(TitleResource = nameof(AppResources.Log_StartingCurrentFault_Title),
			CauseResource = nameof(AppResources.Log_StartingCurrentFault_Cause),
			MeasureResource = nameof(AppResources.Log_StartingCurrentFault_Measure))]
		Inrush_Fault = 155,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_156), ResourceType = typeof(AppResources))]
		Internal_Communication_Failure_In_Frequency_Converter = 156,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_157) , ResourceType = typeof(AppResources))]
		Real_Time_Clock_Error = 157,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_161) , ResourceType = typeof(AppResources))]
		Sensor_Supply_Fault_5V = 161,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_162) , ResourceType = typeof(AppResources))]
		Sensor_Supply_Fault_24V = 162,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_163), ResourceType = typeof(AppResources))]
		Motor_Drive_Protection_Function_Measurement_Fault = 163,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_164) , ResourceType = typeof(AppResources))]
		Signal_Fault_LiqTec_Sensor = 164,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_165) , ResourceType = typeof(AppResources))]
		Signal_Fault_Analog_Input_A1 = 165,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_166) , ResourceType = typeof(AppResources))]
		Signal_Fault_Analog_Input_A2 = 166,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_167) , ResourceType = typeof(AppResources))]
		Signal_Fault_Analog_Input_A3 = 167,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_175), ResourceType = typeof(AppResources))]
		Temperature_Sensor_2_Signal_Fault = 175,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_176), ResourceType = typeof(AppResources))]
		Temperature_Sensor_3_Signal_Fault = 176,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_190) , ResourceType = typeof(AppResources))]
		Limit_1_Exceeded_Limit_Exc_1_Limit = 190,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_191), ResourceType = typeof(AppResources))]
		Limit_2_Exceeded_Limit_Exc_2_Limit = 191,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_240) , ResourceType = typeof(AppResources))]
		Motor_Bearings_Need_Lubrication = 240,

		[Display(Description = nameof(AppResources.Pump_SystemStatus_241) , ResourceType = typeof(AppResources))]
		Missing_Motor_Phase = 241
	}
}
