﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
    public enum PumpVariAESwitch4Value
    {
        [Display(Description = nameof(AppResources.Pump_Switch_010V), ResourceType = typeof(AppResources))]
        Value_010V = 0,
        [Display(Description = nameof(AppResources.Pump_Switch_420mA), ResourceType = typeof(AppResources))]
        Value_420mA = 1,
        [Display(Description = nameof(AppResources.Pump_Switch_Unused), ResourceType = typeof(AppResources))]
        Unused = 2
    }
}
