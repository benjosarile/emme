﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum PumpRelayFunctionValue
	{
		[Display(Description = nameof(AppResources.Pump_RelayFunction_NoFunction), ResourceType = typeof(AppResources))]
		No_function = 0,
		[Display(Description = nameof(AppResources.Pump_RelayFunction_RunningRelay), ResourceType = typeof(AppResources))]
		Running_Relay = 1,
		[Display(Description = nameof(AppResources.Pump_RelayFunction_LubricationRelay), ResourceType = typeof(AppResources))]
		Lubrication_Relay = 2,
		[Display(Description = nameof(AppResources.Pump_RelayFunction_Limit1Relay), ResourceType = typeof(AppResources))]
		Limit_1_Relay = 3,
		[Display(Description = nameof(AppResources.Pump_RelayFunction_Limit2Relay), ResourceType = typeof(AppResources))]
		Limit_2_Relay = 4,
		[Display(Description = nameof(AppResources.Pump_RelayFunction_WarningRelay), ResourceType = typeof(AppResources))]
		Warning_Relay = 5,
		[Display(Description = nameof(AppResources.Pump_RelayFunction_AlarmRelay), ResourceType = typeof(AppResources))]
		Alarm_Relay = 6,
		[Display(Description = nameof(AppResources.Pump_RelayFunction_FaultRelay), ResourceType = typeof(AppResources))]
		Fault_Relay = 7,
		[Display(Description = nameof(AppResources.Pump_RelayFunction_OperatingRelay), ResourceType = typeof(AppResources))]
		Operating_Relay = 8,
		[Display(Description = nameof(AppResources.Pump_RelayFunction_ReadRelay), ResourceType = typeof(AppResources))]
		Ready_Relay = 9,
		[Display(Description = nameof(AppResources.Pump_RelayFunction_BusControlledRelay), ResourceType = typeof(AppResources))]
		Bus_controlled_Relay = 10,
		[Display(Description = nameof(AppResources.Pump_RelayFunction_NotConfigured), ResourceType = typeof(AppResources))]
		Not_configured = 255
	}
}
