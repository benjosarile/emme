﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum LevelProbeConfigurationValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_LevelProbeConfiguration_NotConfigured), ResourceType = typeof(AppResources))]
		NotConfigured = 0,
		[Display(Description = nameof(AppResources.ControlUnit_LevelProbeConfiguration_3Levelswitches), ResourceType = typeof(AppResources))]
		ThreeLevelswitches = 1,
		[Display(Description = nameof(AppResources.ControlUnit_LevelProbeConfiguration_4Levelswitches), ResourceType = typeof(AppResources))]
		FourLevelswitches = 2,
		[Display(Description = nameof(AppResources.ControlUnit_LevelProbeConfiguration_Analogue), ResourceType = typeof(AppResources))]
		Analogue = 3
	}
}
