﻿namespace EmmeApp.Common.Enum
{
    public enum HamburgerMenuValue
   {
      Home = 1,
      Connect = 2,
      PumpReplacement = 3,
      Configuration = 4,
      Documents = 5,
      Cockpit = 6,
      SolutionCenter = 7,
      Log = 8,
      HydraulicAdjustment = 9,
      Contact = 10,
      About = 11,
      AppSettings = 12,

#if DEBUG
        DevExperiments
#endif
   }
}
