﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitForceRunningStateValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_ForceRunningStateValue_Disabled), ResourceType = typeof(AppResources))]
		Disabled = 0,
		[Display(Description = nameof(AppResources.ControlUnit_ForceRunningStateValue_Enabled), ResourceType = typeof(AppResources))]
		Enabled = 1
	}
}
