﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitAtexStateValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_AtexStateValue_Disabled), ResourceType = typeof(AppResources))]
		Disabled = 0,
		[Display(Description = nameof(AppResources.ControlUnit_AtexStateValue_Enabled), ResourceType = typeof(AppResources))]
		Enabled = 1
	}
}
