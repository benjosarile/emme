﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitLevelActiveValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_LevelActiveLevel_Level_INIT), ResourceType = typeof(AppResources))]
		CMN_LEVEL_INIT = 0,
		[Display(Description = nameof(AppResources.ControlUnit_LevelActiveLevel_Level0), ResourceType = typeof(AppResources))]
		CMN_LEVEL_0 = 1,
		[Display(Description = nameof(AppResources.ControlUnit_LevelActiveLevel_Level1), ResourceType = typeof(AppResources))]
		CMN_LEVEL_1 = 2,
		[Display(Description = nameof(AppResources.ControlUnit_LevelActiveLevel_Level2), ResourceType = typeof(AppResources))]
		CMN_LEVEL_2 = 3,
		[Display(Description = nameof(AppResources.ControlUnit_LevelActiveLevel_Level3), ResourceType = typeof(AppResources))]
		CMN_LEVEL_3 = 4,
		[Display(Description = nameof(AppResources.ControlUnit_LevelActiveLevel_Level4), ResourceType = typeof(AppResources))]
		CMN_LEVEL_4 = 5,
	}
}
