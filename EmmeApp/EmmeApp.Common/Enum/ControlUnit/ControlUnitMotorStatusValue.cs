﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitMotorStatusValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_MotorStatus_Stop), ResourceType = typeof(AppResources))]
		CMN_MOTOR_MODE_STOP = 0,
		[Display(Description = nameof(AppResources.ControlUnit_MotorStatus_Run), ResourceType = typeof(AppResources))]
		CMN_MOTOR_MODE_RUN = 1,
		[Display(Description = nameof(AppResources.ControlUnit_MotorStatus_Manual), ResourceType = typeof(AppResources))]
		CMN_MOTOR_MODE_MANUAL = 2,
		[Display(Description = nameof(AppResources.ControlUnit_MotorStatus_DelayedOn), ResourceType = typeof(AppResources))]
		CMN_MOTOR_MODE_DELAYED_ON = 3,
		[Display(Description = nameof(AppResources.ControlUnit_MotorStatus_DelayedOff), ResourceType = typeof(AppResources))]
		CMN_MOTOR_MODE_DELAYED_OFF = 4,
		[Display(Description = nameof(AppResources.ControlUnit_MotorStatus_ForceRun), ResourceType = typeof(AppResources))]
		CMN_MOTOR_MODE_FORCE_RUN = 5,
	}
}
