﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitSystemWarningValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_SystemWarning_NO_WARNING), ResourceType = typeof(AppResources))]
		CMN_WARNING_NO_WARNING = 0,
		[Display(Description = nameof(AppResources.ControlUnit_SystemWarning_SYSTEM_MAINTENANCE), ResourceType = typeof(AppResources))]
		CMN_WARNING_SYSTEM_MAINTENANCE = 1,
		[Display(Description = nameof(AppResources.ControlUnit_SystemWarning_LEVEL_0_SEQUENCE), ResourceType = typeof(AppResources))]
		CMN_WARNING_LEVEL_0_SEQUENCE = 2,
		[Display(Description = nameof(AppResources.ControlUnit_SystemWarning_LEVEL_1_SEQUENCE), ResourceType = typeof(AppResources))]
		CMN_WARNING_LEVEL_1_SEQUENCE = 3,
		[Display(Description = nameof(AppResources.ControlUnit_SystemWarning_LEVEL_2_SEQUENCE), ResourceType = typeof(AppResources))]
		CMN_WARNING_LEVEL_2_SEQUENCE = 4,
		[Display(Description = nameof(AppResources.ControlUnit_SystemWarning_LEVEL_3_SEQUENCE), ResourceType = typeof(AppResources))]
		CMN_WARNING_LEVEL_3_SEQUENCE = 5,
		[Display(Description = nameof(AppResources.ControlUnit_SystemWarning_MOTOR_OSK), ResourceType = typeof(AppResources))]
		CMN_WARNING_MOTOR_OSK = 6,
		[Display(Description = nameof(AppResources.ControlUnit_SystemWarning_MOTOR_FLOW_RATE_MOTOR_0), ResourceType = typeof(AppResources))]
		CMN_WARNING_MOTOR_FLOW_RATE_MOTOR_0 = 7,
		[Display(Description = nameof(AppResources.ControlUnit_SystemWarning_MOTOR_FLOW_RATE_MOTOR_1), ResourceType = typeof(AppResources))]
		CMN_WARNING_MOTOR_FLOW_RATE_MOTOR_1 = 8
	}
}
