﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitDelayedOffStateValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_DelayedOffStateValue_Disabled), ResourceType = typeof(AppResources))]
		Disabled = 0,
		[Display(Description = nameof(AppResources.ControlUnit_DelayedOffStateValue_Enabled), ResourceType = typeof(AppResources))]
		Enabled = 1
	}
}
