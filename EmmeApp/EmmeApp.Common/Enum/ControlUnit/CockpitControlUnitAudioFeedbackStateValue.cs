﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum CockpitControlUnitAudioFeedbackStateValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_AudioFeedbackStateValue_Disabled), ResourceType = typeof(AppResources))]
		Disabled = 0,
		[Display(Description = nameof(AppResources.ControlUnit_AudioFeedbackStateValue_Enabled), ResourceType = typeof(AppResources))]
		Enabled = 1
	}
}
