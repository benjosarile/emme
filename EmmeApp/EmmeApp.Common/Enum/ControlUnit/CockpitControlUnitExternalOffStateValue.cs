﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum CockpitControlUnitExternalOffStateValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_ExternalOffStateValue_NotActive), ResourceType = typeof(AppResources))]
		NotActive = 0,
		[Display(Description = nameof(AppResources.ControlUnit_ExternalOffStateValue_Active), ResourceType = typeof(AppResources))]
		Active = 1
	}
}
