﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
    public enum ControlUnitMotorRuntimeObserverStateValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_MotorRuntimeObserverState_Disabled), ResourceType = typeof(AppResources))]
		Disabled = 0,
		[Display(Description = nameof(AppResources.ControlUnit_MotorRuntimeObserverState_Enabled), ResourceType = typeof(AppResources))]
		Enabled = 1
	}
}
