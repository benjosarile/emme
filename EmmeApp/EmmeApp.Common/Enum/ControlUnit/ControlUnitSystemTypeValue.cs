﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitSystemTypeValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_SystemType_NotConfigured), ResourceType = typeof(AppResources))]
		NotConfigured = 0,
		[Display(Description = nameof(AppResources.ControlUnit_SystemType_ShaftSystem), ResourceType = typeof(AppResources))]
		ShaftSystem = 1,
		[Display(Description = nameof(AppResources.ControlUnit_SystemType_ReservoirSystem), ResourceType = typeof(AppResources))]
		ReservoirSystem = 2
	}
}
