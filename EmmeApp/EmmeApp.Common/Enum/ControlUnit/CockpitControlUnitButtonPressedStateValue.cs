﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum CockpitControlUnitButtonPressedStateValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_ButtonPressedStateValue_NotPressed), ResourceType = typeof(AppResources))]
		NotPressed = 0,
		[Display(Description = nameof(AppResources.ControlUnit_ButtonPressedStateValue_Pressed), ResourceType = typeof(AppResources))]
		Pressed = 1
	}
}
