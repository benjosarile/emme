﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum CockpitControlUnitRuntimeObserverStateValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_RuntimeObserverStateValue_Disabled), ResourceType = typeof(AppResources))]
		Disabled = 0,
		[Display(Description = nameof(AppResources.ControlUnit_RuntimeObserverStateValue_Enabled), ResourceType = typeof(AppResources))]
		Enabled = 1
	}
}
