﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitLevelAnalogStatusValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_LevelAnalogStatus_INIT), ResourceType = typeof(AppResources))]
		CMN_LEVEL_ANALOG_STATE_INIT = 0,
		[Display(Description = nameof(AppResources.ControlUnit_LevelAnalogStatus_OK), ResourceType = typeof(AppResources))]
		CMN_LEVEL_ANALOG_STATE_OK = 1,
		[Display(Description = nameof(AppResources.ControlUnit_LevelAnalogStatus_ALARMRANGE), ResourceType = typeof(AppResources))]
		CMN_LEVEL_ANALOG_STATE_ALARM_RANGE = 2,
		[Display(Description = nameof(AppResources.ControlUnit_LevelAnalogStatus_ALARMDISCONNECTED), ResourceType = typeof(AppResources))]
		CMN_LEVEL_ANALOG_STATE_ALARM_DISCONNECTED = 3,
	}
}
