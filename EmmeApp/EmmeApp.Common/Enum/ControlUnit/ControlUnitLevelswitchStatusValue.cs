﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitLevelswitchStatusValue
    {
		[Display(Description = nameof(AppResources.ControlUnit_LevelswitchStatus_Init), ResourceType = typeof(AppResources))]
		CMN_LEVEL_LEVELSWITCH_STATE_INIT = 0,
		[Display(Description = nameof(AppResources.ControlUnit_LevelswitchStatus_OkHanging), ResourceType = typeof(AppResources))]
		CMN_LEVEL_LEVELSWITCH_STATE_OK_HANGING = 1,
		[Display(Description = nameof(AppResources.ControlUnit_LevelswitchStatus_Floating), ResourceType = typeof(AppResources))]
		CMN_LEVEL_LEVELSWITCH_STATE_OK_FLOATING = 2,
		[Display(Description = nameof(AppResources.ControlUnit_LevelswitchStatus_AlarmShorted), ResourceType = typeof(AppResources))]
		CMN_LEVEL_LEVELSWITCH_STATE_ALARM_SHORTED = 3,
		[Display(Description = nameof(AppResources.ControlUnit_LevelswitchStatus_AlarmDisconnected) , ResourceType = typeof(AppResources))]
		CMN_LEVEL_LEVELSWITCH_STATE_ALARM_DISCONNECTED = 4,
    }
}
