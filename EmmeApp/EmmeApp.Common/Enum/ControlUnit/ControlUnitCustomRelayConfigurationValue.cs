﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitCustomRelayConfigurationValue
    {
		[Display(Description = nameof(AppResources.ControlUnit_CustomRelayConfiguration_NotConfigured) , ResourceType = typeof(AppResources))]
		CMN_M2M_NOT_CONFIGURED = 0,
		[Display(Description = nameof(AppResources.ControlUnit_CustomRelayConfiguration_Warning), ResourceType = typeof(AppResources))]
		CMN_M2M_WARNING = 1,
		[Display(Description = nameof(AppResources.ControlUnit_CustomRelayConfiguration_LevelHigh), ResourceType = typeof(AppResources))]
		CMN_M2M_LEVEL_HIGH = 2,
		[Display(Description = nameof(AppResources.ControlUnit_CustomRelayConfiguration_AlarmMotor0), ResourceType = typeof(AppResources))]
		CMN_M2M_ALARM_MOTOR_0 = 3,
		[Display(Description = nameof(AppResources.ControlUnit_CustomRelayConfiguration_AlarmMotor1), ResourceType = typeof(AppResources))]
		CMN_M2M_ALARM_MOTOR_1 = 4,
		[Display(Description = nameof(AppResources.ControlUnit_CustomRelayConfiguration_AlarmMotor12), ResourceType = typeof(AppResources))]
		CMN_M2M_ALARM_MOTOR_0_1 = 5,
		[Display(Description = nameof(AppResources.ControlUnit_CustomRelayConfiguration_OperatingMotor0), ResourceType = typeof(AppResources))]
		CMN_M2M_OPERATING_MOTOR_0 = 6,
		[Display(Description = nameof(AppResources.ControlUnit_CustomRelayConfiguration_OperatingMotor1), ResourceType = typeof(AppResources))]
		CMN_M2M_OPERATING_MOTOR_1 = 7,
		[Display(Description = nameof(AppResources.ControlUnit_CustomRelayConfiguration_OperatingMotor12) , ResourceType = typeof(AppResources))]
		CMN_M2M_OPERATING_MOTOR_0_1 = 8,
		[Display(Description = nameof(AppResources.ControlUnit_CustomRelayConfiguration_Maintenance), ResourceType = typeof(AppResources))]
		CMN_M2M_SERVICE = 9,
    }
}
