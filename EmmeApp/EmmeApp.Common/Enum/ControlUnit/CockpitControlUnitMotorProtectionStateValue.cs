﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum CockpitControlUnitMotorProtectionStateValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_MotorProtectionStateValue_Disabled), ResourceType = typeof(AppResources))]
		Disabled = 0,
		[Display(Description = nameof(AppResources.ControlUnit_MotorProtectionStateValue_Enabled), ResourceType = typeof(AppResources))]
		Enabled = 1
	}
}
