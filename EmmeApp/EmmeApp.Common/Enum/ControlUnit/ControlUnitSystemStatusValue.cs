﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitSystemStatusValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_SystemStatus_NotConfigured), ResourceType = typeof(AppResources))]
		CMN_SYSTEM_STATUS_NOT_CONFIGURED = 0,
		[Display(Description = nameof(AppResources.ControlUnit_SystemStatus_Ok), ResourceType = typeof(AppResources))]
		CMN_SYSTEM_STATUS_OK = 1,
		[Display(Description = nameof(AppResources.ControlUnit_SystemStatus_Alarm), ResourceType = typeof(AppResources))]
		CMN_SYSTEM_STATUS_ALARM = 2,
		[Display(Description = nameof(AppResources.ControlUnit_SystemStatus_AlarmConfirmed), ResourceType = typeof(AppResources))]
		CMN_SYSTEM_STATUS_ALARM_CONFIRMED = 3,
		[Display(Description = nameof(AppResources.ControlUnit_SystemStatus_Warning), ResourceType = typeof(AppResources))]
		CMN_SYSTEM_STATUS_WARNING = 4,
		[Display(Description = nameof(AppResources.ControlUnit_SystemStatus_WarningConfirmed), ResourceType = typeof(AppResources))]
		CMN_SYSTEM_STATUS_WARNING_CONFIRMED = 5,
		[Display(Description = nameof(AppResources.ControlUnit_SystemStatus_Failure), ResourceType = typeof(AppResources))]
		CMN_SYSTEM_STATUS_FAILURE = 6,
	}
}
