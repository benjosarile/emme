﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitDatalinkStatusValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_Datalink_Status_NotConfigured), ResourceType = typeof(AppResources))]
		CMN_DATALINK_STATUS_NOT_CONFIGURED = 0,
		[Display(Description = nameof(AppResources.ControlUnit_Datalink_Status_Off), ResourceType = typeof(AppResources))]
		CMN_DATALINK_STATUS_OFF = 1,
		[Display(Description = nameof(AppResources.ControlUnit_Datalink_Status_Disconnected), ResourceType = typeof(AppResources))]
		CMN_DATALINK_STATUS_DISCONNECTED = 2,
		[Display(Description = nameof(AppResources.ControlUnit_Datalink_Status_Pending), ResourceType = typeof(AppResources))]
		CMN_DATALINK_STATUS_PENDING = 3,
		[Display(Description = nameof(AppResources.ControlUnit_Datalink_Status_Connected), ResourceType = typeof(AppResources))]
		CMN_DATALINK_STATUS_CONNECTED = 4,
		[Display(Description = nameof(AppResources.ControlUnit_Datalink_Status_Alarm), ResourceType = typeof(AppResources))]
		CMN_DATALINK_STATUS_ALARM = 5
	}
}
