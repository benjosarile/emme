﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitSystemDoubleMotorValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_SystemDoubleMotor_1Pump), ResourceType = typeof(AppResources))]
		OnePump = 0,
		[Display(Description = nameof(AppResources.ControlUnit_SystemDoubleMotor_2Pumps), ResourceType = typeof(AppResources))]
		TwoPumps = 1
	}
}
