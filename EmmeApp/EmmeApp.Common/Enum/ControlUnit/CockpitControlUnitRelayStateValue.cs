﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum CockpitControlUnitRelayStateValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_RelayStateValue_NotActive), ResourceType = typeof(AppResources))]
		NotActive = 0,
		[Display(Description = nameof(AppResources.ControlUnit_RelayStateValue_Active), ResourceType = typeof(AppResources))]
		Active = 1
	}
}
