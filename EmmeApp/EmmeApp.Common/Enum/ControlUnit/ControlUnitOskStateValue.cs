﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitOskStateValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_OskStateValue_Disabled), ResourceType = typeof(AppResources))]
		Disabled = 0,
		[Display(Description = nameof(AppResources.ControlUnit_OskStateValue_Enabled), ResourceType = typeof(AppResources))]
		Enabled = 1
	}
}
