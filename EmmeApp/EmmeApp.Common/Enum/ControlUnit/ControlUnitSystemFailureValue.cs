﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitSystemFailureValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_NoFailure), ResourceType = typeof(AppResources))]
		CMN_FAILURE_NO_FAILURE = 0,
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_Internal_Motor0), ResourceType = typeof(AppResources))]
		CMN_FAILURE_INTERNAL_MOTOR_0 = 1,
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_Internal_Motor1), ResourceType = typeof(AppResources))]
		CMN_FAILURE_INTERNAL_MOTOR_1 = 2,
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_Internal_HMI), ResourceType = typeof(AppResources))]
		CMN_FAILURE_INTERNAL_HMI = 3,
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_Internal_Level), ResourceType = typeof(AppResources))]
		CMN_FAILURE_INTERNAL_LEVEL = 4,
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_Internal_M2M), ResourceType = typeof(AppResources))]
		CMN_FAILURE_INTERNAL_M2M = 5,
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_Internal_ADC), ResourceType = typeof(AppResources))]
		CMN_FAILURE_INTERNAL_ADC = 6,
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_Internal_Statistic), ResourceType = typeof(AppResources))]
		CMN_FAILURE_INTERNAL_STATISTIC = 7,
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_System_Reset), ResourceType = typeof(AppResources))]
		CMN_FAILURE_SYSTEM_RESET = 8,
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_Supply_Drop), ResourceType = typeof(AppResources))]
		CMN_FAILURE_SUPPLY_DROP = 9,
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_OS_StackOverflow), ResourceType = typeof(AppResources))]
		CMN_FAILURE_OS_STACKOVERFLOW = 10,
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_Flash_Corrupt), ResourceType = typeof(AppResources))]
		CMN_FAILURE_FLASH_CORRUPT = 11,
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_Memory_Failure), ResourceType = typeof(AppResources))]
		CMN_FAILURE_MEMORY_FAILURE = 12,
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_Memory_Data_Corrupt), ResourceType = typeof(AppResources))]
		CMN_FAILURE_MEMORY_DATA_CORRUPT = 13,
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_ServiceControl), ResourceType = typeof(AppResources))]
		CMN_FAILURE_SERVICECONTROL = 14,
		[Display(Description = nameof(AppResources.ControlUnit_SystemFailure_RemoteControl), ResourceType = typeof(AppResources))]
		CMN_FAILURE_REMOTECONTROL = 15
	}
}
