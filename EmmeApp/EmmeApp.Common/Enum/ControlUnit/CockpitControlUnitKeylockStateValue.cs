﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum CockpitControlUnitKeylockStateValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_KeylockStateValue_NotLocked), ResourceType = typeof(AppResources))]
		NotLocked = 0,
		[Display(Description = nameof(AppResources.ControlUnit_KeylockStateValue_Locked), ResourceType = typeof(AppResources))]
		Locked = 1
	}
}
