﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitDelayedOnStateValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_DelayedOnStateValue_Disabled), ResourceType = typeof(AppResources))]
		Disabled = 0,
		[Display(Description = nameof(AppResources.ControlUnit_DelayedOnStateValue_Enabled), ResourceType = typeof(AppResources))]
		Enabled = 1
	}
}
