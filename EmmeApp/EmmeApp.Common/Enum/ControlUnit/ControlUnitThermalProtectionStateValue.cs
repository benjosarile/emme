﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
    public enum ControlUnitThermalProtectionStateValue
    {
        [Display(Description = nameof(AppResources.ControlUnit_ThermalProtectionStateValue_Disabled), ResourceType = typeof(AppResources))]
        Disabled = 0,
		[Display(Description = nameof(AppResources.ControlUnit_ThermalProtectionStateValue_Enabled), ResourceType = typeof(AppResources))]
        Enabled = 1	
    }
}
