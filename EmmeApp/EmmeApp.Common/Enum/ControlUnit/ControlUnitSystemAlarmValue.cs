﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitSystemAlarmValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_NO_ALARM), ResourceType = typeof(AppResources))]
		CMN_ALARM_NO_ALARM = 0,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_NOT_CONFIGURED), ResourceType = typeof(AppResources))]
		CMN_ALARM_SYSTEM_NOT_CONFIGURED = 1,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_SWITCH_FEEDBACK_MOTOR_0), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_SWITCH_FEEDBACK_MOTOR_0 = 2,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_MOTOR_PROTECTION_MOTOR_0), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_MOTOR_PROTECTION_MOTOR_0 = 3,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_OVERCURRENT_MOTOR_0), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_OVERCURRENT_MOTOR_0 = 4,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_SWITCH_MOTOR_0), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_SWITCH_MOTOR_0 = 5,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_WSK_MOTOR_0), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_WSK_MOTOR_0 = 6,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_PHASE_ASYMMETRIC_MOTOR_0), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_PHASE_ASYMMETRIC_MOTOR_0 = 7,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_DISCONNECT_MOTOR_0), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_DISCONNECT_MOTOR_0 = 8,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_SWITCH_FEEDBACK_MOTOR_1), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_SWITCH_FEEDBACK_MOTOR_1 = 9,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_MOTOR_PROTECTION_MOTOR_1), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_MOTOR_PROTECTION_MOTOR_1 = 10,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_OVERCURRENT_MOTOR_1), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_OVERCURRENT_MOTOR_1 = 11,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_SWITCH_MOTOR_1), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_SWITCH_MOTOR_1 = 12,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_WSK_MOTOR_1), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_WSK_MOTOR_1 = 13,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_PHASE_ASYMMETRIC_MOTOR_1), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_PHASE_ASYMMETRIC_MOTOR_1 = 14,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_DISCONNECT_MOTOR_1), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_DISCONNECT_MOTOR_1 = 15,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_FLOW_RATE), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_FLOW_RATE = 16,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_MOTOR_CONFIG), ResourceType = typeof(AppResources))]
		CMN_ALARM_MOTOR_CONFIG = 17,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_LEVEL_LEVELSWITCH_0_DISCONNECT), ResourceType = typeof(AppResources))]
		CMN_ALARM_LEVEL_LEVELSWITCH_0_DISCONNECT = 18,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_LEVEL_LEVELSWITCH_1_DISCONNECT), ResourceType = typeof(AppResources))]
		CMN_ALARM_LEVEL_LEVELSWITCH_1_DISCONNECT = 19,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_LEVEL_LEVELSWITCH_2_DISCONNECT), ResourceType = typeof(AppResources))]
		CMN_ALARM_LEVEL_LEVELSWITCH_2_DISCONNECT = 20,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_LEVEL_LEVELSWITCH_3_DISCONNECT), ResourceType = typeof(AppResources))]
		CMN_ALARM_LEVEL_LEVELSWITCH_3_DISCONNECT = 21,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_LEVEL_LEVELSWITCH_0_SHORTED), ResourceType = typeof(AppResources))]
		CMN_ALARM_LEVEL_LEVELSWITCH_0_SHORTED = 22,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_LEVEL_LEVELSWITCH_1_SHORTED), ResourceType = typeof(AppResources))]
		CMN_ALARM_LEVEL_LEVELSWITCH_1_SHORTED = 23,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_LEVEL_LEVELSWITCH_2_SHORTED), ResourceType = typeof(AppResources))]
		CMN_ALARM_LEVEL_LEVELSWITCH_2_SHORTED = 24,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_LEVEL_LEVELSWITCH_3_SHORTED), ResourceType = typeof(AppResources))]
		CMN_ALARM_LEVEL_LEVELSWITCH_3_SHORTED = 25,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_LEVEL_ANALOG_DISCONNECT), ResourceType = typeof(AppResources))]
		CMN_ALARM_LEVEL_ANALOG_DISCONNECT = 26,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_LEVEL_ANALOG_RANGE), ResourceType = typeof(AppResources))]
		CMN_ALARM_LEVEL_ANALOG_RANGE = 27,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_LEVEL_HIGH), ResourceType = typeof(AppResources))]
		CMN_ALARM_LEVEL_HIGH = 28,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_LEVEL_OUT_OF_ORDER), ResourceType = typeof(AppResources))]
		CMN_ALARM_LEVEL_OUT_OF_ORDER = 29,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_LEVEL_CONFIG), ResourceType = typeof(AppResources))]
		CMN_ALARM_LEVEL_CONFIG = 30,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_M2M_CONFIG), ResourceType = typeof(AppResources))]
		CMN_ALARM_M2M_CONFIG = 31,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_DATALINK_CONFIG), ResourceType = typeof(AppResources))]
		CMN_ALARM_DATALINK_CONFIG = 32,
		[Display(Description = nameof(AppResources.ControlUnit_SystemAlarm_HMI_CONFIG), ResourceType = typeof(AppResources))]
		CMN_ALARM_HMI_CONFIG = 33
	}
}
