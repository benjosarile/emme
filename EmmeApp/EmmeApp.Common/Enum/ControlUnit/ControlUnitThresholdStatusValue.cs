﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitThresholdStatusValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_ThresholdStatus_Init), ResourceType = typeof(AppResources))]
		CMN_THRESHOLD_STATUS_INIT = 0,
		[Display(Description = nameof(AppResources.ControlUnit_ThresholdStatus_NotActive), ResourceType = typeof(AppResources))]
		CMN_THRESHOLD_STATUS_NOT_ACTIVE = 1,
		[Display(Description = nameof(AppResources.ControlUnit_ThresholdStatus_Active), ResourceType = typeof(AppResources))]
		CMN_THRESHOLD_STATUS_ACTIVE = 2,
		[Display(Description = nameof(AppResources.ControlUnit_ThresholdStatus_Warning), ResourceType = typeof(AppResources))]
		CMN_THRESHOLD_STATUS_WARNING = 3,
		[Display(Description = nameof(AppResources.ControlUnit_ThresholdStatus_Alarm), ResourceType = typeof(AppResources))]
		CMN_THRESHOLD_STATUS_ALARM = 4,
	}
}
