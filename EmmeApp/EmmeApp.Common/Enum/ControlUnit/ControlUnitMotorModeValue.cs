﻿using EmmeApp.Localization.Resources;
using System.ComponentModel.DataAnnotations;

namespace EmmeApp.Common.Enum
{
	public enum ControlUnitMotorModeValue
	{
		[Display(Description = nameof(AppResources.ControlUnit_SystemMotorMode_NotConfigured), ResourceType = typeof(AppResources))]
		NotConfigured = 0,
		[Display(Description = nameof(AppResources.ControlUnit_SystemMotorMode_Inactive), ResourceType = typeof(AppResources))]
		Inactive = 1,
		[Display(Description = nameof(AppResources.ControlUnit_SystemMotorMode_Auto), ResourceType = typeof(AppResources))]
		Auto = 2,
		[Display(Description = nameof(AppResources.ControlUnit_SystemMotorMode_Manual), ResourceType = typeof(AppResources))]
		Manual = 3
	}
}
