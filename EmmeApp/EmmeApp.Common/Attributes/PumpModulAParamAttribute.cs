﻿using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.External;
using System;
using System.Diagnostics;

namespace EmmeApp.Common.Attributes
{
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PumpModulAParamAttribute : Attribute
    {
        public PumpModulAParamAttribute(ExternalPumpModulAParameterIdValue modulAParameterId, Type type, bool canWrite = true)
        {
            this.ModulAParameterId = modulAParameterId;
            this.Type = type;
            this.CanWrite = canWrite;

            Debug.WriteLine($"Pump ModulA paramter Id: {modulAParameterId}");
        }

        public ExternalPumpModulAParameterIdValue ModulAParameterId { get; set; }
        public Type Type { get; set; }

        public bool CanWrite { get; set; }

        public object GetValue(ExternalIdOneofCaseValue idCase, ValueTExternalModel parameterValue, ScalingTExternalModel parameterScaling)
        {
			if (parameterValue != null)
			{
                if (this.Type == typeof(string))
                {
                    return parameterValue.Text;
                }
                else if (this.Type == typeof(bool))
                {
                    return Convert.ToBoolean(parameterValue.Number);
                }
                else if (this.Type.IsEnum)
                {
                    return System.Enum.ToObject(this.Type, parameterValue.Number);
                }
                else if (this.Type == typeof(double))
                {
                    if (parameterScaling == null || parameterScaling.Step == 0)
                    {
                        return parameterValue.Number;
                    }

                    double step = Math.Round(parameterScaling.Step, 5);
                    double value = parameterValue.Number * step;
                    return value;
                }
                else if(this.Type == typeof(long))
                {
                    return parameterValue.Number;
                }
				else if (this.Type == typeof(int))
				{
					return Convert.ToInt32(parameterValue.Number);
				}
			}

            return null;
        }
    }
}
