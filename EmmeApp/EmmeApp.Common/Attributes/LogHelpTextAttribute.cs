﻿using EmmeApp.Localization.Helpers;
using System;

namespace EmmeApp.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
	public class LogHelpTextAttribute : Attribute
	{
		public LogHelpTextAttribute()
		{
		}

		public string TitleResource { get; set; }
		public string CauseResource { get; set; }
		public string MeasureResource { get; set; }

		public string GetTranslation(string resourceKey)
		{
			var translateExtension = new TranslateExtension();
			translateExtension.Text = resourceKey;
			var s = translateExtension.ProvideValue(null).ToString();
			return s;
		}
	}
}
