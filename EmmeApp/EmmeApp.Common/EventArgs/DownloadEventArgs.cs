﻿namespace EmmeApp.Common.EventArgs
{
	public class DownloadEventArgs : System.EventArgs
	{
		public bool FileSaved { get; set; }
		public string Filepath { get; set; }

		public DownloadEventArgs(bool fileSaved)
		{
			this.FileSaved = fileSaved;
		}
		public DownloadEventArgs(bool fileSaved, string filePath)
		{
			this.FileSaved = fileSaved;
			this.Filepath = filePath;
		}
	}
}
