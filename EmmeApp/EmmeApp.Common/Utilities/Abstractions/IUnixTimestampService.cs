﻿using System;

namespace EmmeApp.Common.Utilities.Abstractions
{
    public interface IUnixTimestampService
    {
        long ConvertToUnix(DateTime date);

        DateTime ConvertToLocalTime(long unix);
    }
}
