﻿using System;
using System.Collections.Generic;

namespace EmmeApp.Common.Utilities.Abstractions
{
    public interface IAppCenterCustomService
	{
		void Init();
		void TrackError(Exception ex, Dictionary<string, string> properties = null);
		void TrackEvent(string name, Dictionary<string, string> properties = null);
		void GenerateTestCrash();
	}
}
