﻿namespace EmmeApp.Common.Utilities.Abstractions
{
    public interface ISettingsService
	{
		bool SetValue(string key, string value);
		string GetValue(string key);
		void Clear();
	}
}
