﻿using System;
using System.Threading.Tasks;

namespace EmmeApp.Common.Utilities.Abstractions
{
    public interface ITaskRunner
	{
		Task<T> Run<T>(Func<T> action);
	}
}
