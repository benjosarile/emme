﻿namespace EmmeApp.Common.Utilities.Abstractions
{
    public interface ICultureInfoService
	{
		string GetCurrentLanguage();
		string GetCurrentCountry();
		string GetCurrentCountryCode();
	}
}
