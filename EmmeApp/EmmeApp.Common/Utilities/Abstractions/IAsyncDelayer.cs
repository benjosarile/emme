﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace EmmeApp.Common.Utilities.Abstractions
{
	public interface IAsyncDelayer
	{
		Task Delay(int millisecondsDelay);
		Task Delay(int millisecondsDelay, CancellationToken cancellationToken);
		Task Delay(TimeSpan timeSpan);
		Task Delay(TimeSpan timeSpan, CancellationToken cancellationToken);
	}
}