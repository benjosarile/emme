﻿namespace EmmeApp.Common.Utilities.Abstractions
{
	public interface IFileService
	{
		string GetFilePath(string filename);
		string ReadFromAFile(string filePath);
		void WriteToAFile(string filePath, string content, bool isAppend);
	}
}