﻿namespace EmmeApp.Common.Utilities.Abstractions
{
    public interface IExitService
    {
        void CloseApp();
    }
}
