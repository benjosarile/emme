﻿using Prism.Navigation;

namespace EmmeApp.Common.Utilities.Abstractions
{
    public interface INavigationHelperService
	{
		NavigationMode GetNavigationMode(INavigationParameters parameters);
	}
}
