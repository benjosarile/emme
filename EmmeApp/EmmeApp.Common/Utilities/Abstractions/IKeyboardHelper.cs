﻿namespace EmmeApp.Common.Utilities.Abstractions
{
    public interface IKeyboardHelper
    {
        void HideKeyboard();
    }
}
