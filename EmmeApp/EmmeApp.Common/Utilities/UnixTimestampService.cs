﻿using System;

namespace EmmeApp.Common.Utilities.Abstractions
{
    public class UnixTimestampService : IUnixTimestampService
    {
        public DateTime ConvertToLocalTime(long unix)
        {
            long unixTimestampValue;
            if (unix < 0)
            {
                unixTimestampValue = 0;
            }
            else if (unix > 2147483647)
            {
                unixTimestampValue = 2147483647;
            }
            else
            {
                unixTimestampValue = unix;
            }
            return DateTimeOffset.FromUnixTimeSeconds(unixTimestampValue).DateTime.ToLocalTime();
        }

        public long ConvertToUnix(DateTime date)
        {
            var dateTime = DateTime.SpecifyKind(date, DateTimeKind.Local);
            var dateTimeOffset = new DateTimeOffset(dateTime);
            return dateTimeOffset.ToUnixTimeSeconds();
        }
    }
}
