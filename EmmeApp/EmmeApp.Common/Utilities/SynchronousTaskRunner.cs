﻿using System;
using System.Threading.Tasks;

namespace EmmeApp.Common.Utilities.Abstractions
{
    public class SynchronousTaskRunner : ITaskRunner
	{
		public Task<T> Run<T>(Func<T> action)
		{
			var result = action();
			return Task.FromResult(result);
		}
	}
}
