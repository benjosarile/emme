﻿using Prism.Navigation;

namespace EmmeApp.Common.Utilities.Abstractions
{
    public class NavigationHelperService : INavigationHelperService
	{
		public NavigationMode GetNavigationMode(INavigationParameters parameters)
		{
			return parameters.GetNavigationMode();
		}
	}
}
