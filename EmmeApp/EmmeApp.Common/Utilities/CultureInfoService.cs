﻿using EmmeApp.Common.Utilities.Abstractions;
using System.Globalization;

namespace EmmeApp.Common.Utilities
{
    public class CultureInfoService : ICultureInfoService
	{
		public string GetCurrentLanguage()
		{
			return CultureInfo.CurrentUICulture?.TwoLetterISOLanguageName?.ToUpper() ?? string.Empty;
		}

		public string GetCurrentCountry()
		{
			return RegionInfo.CurrentRegion?.Name?.ToUpper() ?? string.Empty;
		}

		public string GetCurrentCountryCode()
		{
			return RegionInfo.CurrentRegion?.TwoLetterISORegionName?.ToUpper() ?? string.Empty;
		}
	}
}
