﻿using System.Globalization;

namespace EmmeApp.Common.Utilities.Abstractions
{
    public static class StringExtensions
    {
        public static string ToTitleCase(this string value)
        {
            value = value ?? string.Empty;
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            var newValue = textInfo.ToTitleCase(value.ToLower());
            return newValue;
        }
    }
}
