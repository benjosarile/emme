﻿using EmmeApp.Common.Utilities.Abstractions;
using System;
using System.IO;
using System.Text;

namespace EmmeApp.Common.Utilities
{
    public class FileService : IFileService
	{
		public string GetFilePath(string filename)
		{
			string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			return Path.Combine(path, string.Format("{0}", filename));
		}
		public string ReadFromAFile(string filePath)
		{
			throw new NotImplementedException();
		}

		public void WriteToAFile(string filePath, string content, bool isAppend)
		{
			using (var streamWriter = new StreamWriter(filePath, isAppend))
			{
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                streamWriter.WriteLine(content, encode);
			}
		}

	}
}
