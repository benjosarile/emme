﻿using EmmeApp.Common.Utilities.Abstractions;
using System;
using System.Threading.Tasks;

namespace EmmeApp.Common.Utilities
{
    public class AsynchronousTaskRunner : ITaskRunner
	{
		public Task<T> Run<T>(Func<T> action)
		{
			return Task.Run(action);
		}
	}
}
