﻿using Prism.Navigation;
using System;
using System.Threading.Tasks;

namespace EmmeApp.Common.Utilities.Abstractions
{
    public static class NavigationUtilities
	{
		public static Func<INavigationService, string> GetNavigationUriPath { get; set; } =
		(navSvc) => navSvc.GetNavigationUriPath();

		public static Func<INavigationService, string, INavigationParameters, bool?, bool, Task<INavigationResult>> NavigateAsync { get; set; } = (navSvc, pageName, navParams, isModal, isAnimated) => navSvc.NavigateAsync(pageName, navParams, isModal, isAnimated);

		public static Func<INavigationService, INavigationParameters, Task<INavigationResult>> GoBackToRootAsync { get; set; } =
		(navSvc, navParams) => navSvc.GoBackToRootAsync(navParams);

		public static Func<INavigationService, INavigationParameters, bool?, bool, Task<INavigationResult>> GoBackAsync { get; set; } = (navSvc, navParams, isModal, isAnimated) => navSvc.GoBackAsync(navParams, isModal, isAnimated);
    }
}
