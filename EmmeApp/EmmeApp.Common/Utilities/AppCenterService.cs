﻿using EmmeApp.Common.Constants;
using EmmeApp.Common.Utilities.Abstractions;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using System;
using System.Collections.Generic;

namespace EmmeApp.Common.Utilities
{
    public class AppCenterCustomService : IAppCenterCustomService
	{
		public void Init()
		{
			AppCenter.Start($"android={AppConstants.AppCenterAndroidKey};ios={AppConstants.AppCenteriOSKey}",
						   typeof(Analytics), typeof(Crashes));

			AppCenter.LogLevel = LogLevel.Verbose;
			Analytics.SetEnabledAsync(true);
		}

		public void TrackError(Exception ex, Dictionary<string, string> properties = null)
		{
			if (!AppCenter.Configured)
			{
				Init();
			}

			Crashes.TrackError(new Exception(ex.Message), properties);
		}

		public void TrackEvent(string name, Dictionary<string, string> properties = null)
		{
			if (!AppCenter.Configured)
			{
				Init();
			}

			Analytics.TrackEvent(name, properties);
		}

		public void GenerateTestCrash()
		{
			Crashes.GenerateTestCrash();
		}

	}
}
