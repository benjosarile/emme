﻿using EmmeApp.Common.Utilities.Abstractions;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace EmmeApp.Common.Utilities
{
    public class AsyncDelayer : IAsyncDelayer
	{
		public Task Delay(int millisecondsDelay)
		{
			return Task.Delay(millisecondsDelay);
		}

		public Task Delay(int millisecondsDelay, CancellationToken cancellationToken)
		{
			return Task.Delay(millisecondsDelay, cancellationToken);
		}

		public Task Delay(TimeSpan timeSpan)
		{
			return Task.Delay(timeSpan);
		}

		public Task Delay(TimeSpan timeSpan, CancellationToken cancellationToken)
		{
			return Task.Delay(timeSpan,cancellationToken);
		}
	}
}
