﻿using System;
using System.Runtime.Serialization;

namespace EmmeApp.Localization.Exceptions
{
    [Serializable]
    public class TranslateException : Exception
    {
        public TranslateException(string message) : base(message)
        {
        }

        protected TranslateException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
