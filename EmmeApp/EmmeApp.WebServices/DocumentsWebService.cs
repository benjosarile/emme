﻿using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.DataContracts.Response;
using EmmeApp.WebServices.Abstractions;
using EmmeApp.WebServices.Base;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace EmmeApp.WebServices
{
    public class DocumentsWebService : WebServiceBase, IDocumentsWebService
	{
		public DocumentsWebService(IHttpService httpService) : base(httpService)
		{
			httpService.ResetServerBaseAddress(Server.OxomiServerBaseAddress, TimeSpan.FromSeconds(120));
		}

		public async Task<object> Search(string searchQuery, string filterLang, string lang, string filterCountry)
		{
			try
			{
				var data = new List<KeyValuePair<string, string>>();
				data.Add(new KeyValuePair<string, string>("portal", "2025044"));
				data.Add(new KeyValuePair<string, string>("filterBox", $"#oxomi-search-filters"));
				data.Add(new KeyValuePair<string, string>("filterBoxGroups", $"brands,types,categories,tags,langs"));
				data.Add(new KeyValuePair<string, string>("contentType", $"catalogs,videos,galleries,items"));
				data.Add(new KeyValuePair<string, string>("filterLang", $"{filterLang}"));
				data.Add(new KeyValuePair<string, string>("lang", $"{lang}"));
				data.Add(new KeyValuePair<string, string>("filterCountry", $"{filterCountry}"));

				if (!string.IsNullOrEmpty(searchQuery))
				{
					string[] filterList = searchQuery.Split('|');

					foreach (var filterItem in filterList)
					{
						data.Add(new KeyValuePair<string, string>("filters[]", $"{filterItem}"));
					}
				}

				var responseMessage = await PostRequestAsync(Endpoints.DocumentSearch, CancellationToken.None, data);
				if (responseMessage != null)
				{
					var content = await responseMessage.ReadAsStringAsync();

					if (content.Contains("CONTENT_BY_TYPE"))
					{
						var s = JsonConvert.DeserializeObject<SearchContentByTypeRootContract>(content);
						return s;
					}
					else if (content.Contains(""))
					{
						var s = JsonConvert.DeserializeObject<SearchPagesByCatalogRootContract>(content);
						return s;
					}
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}

			return null;
		}


		public async Task<string> GetLink(string pdfLink)
		{
			try
			{
				var responseMessage = await RequestAsync(HttpRequestType.Get, pdfLink, null, CancellationToken.None);

				if (responseMessage.StatusCode == System.Net.HttpStatusCode.OK)
				{
					return responseMessage.RequestMessage.RequestUri.OriginalString;
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}

			return null;
		}

		public async Task<CatalogDetailsContract> GetCatalogDetails(string portal, string catalog)
		{
			try
			{
				string endpoint = string.Format(Endpoints.DocumentCatalogDetails, catalog, portal);

				var responseMessage = await GetRequestAsync(endpoint, CancellationToken.None);
				if (responseMessage != null)
				{
					var content = await responseMessage.ReadAsStringAsync();
					var catalogDetails = JsonConvert.DeserializeObject<CatalogDetailsContract>(content);
					return catalogDetails;
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}

			return null;
		}
	}
}
