﻿using EmmeApp.Common.Enum;
using EmmeApp.Common.Exceptions;
using EmmeApp.WebServices.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EmmeApp.WebServices.Base
{
    public class WebServiceBase : IWebServiceBase
    {
        private readonly IHttpService _httpService;

        protected IEmmeHttpClient HttpClient => _httpService.EmmeHttpClient;

        public WebServiceBase(IHttpService httpService)
        {
            _httpService = httpService;
        }

        public async Task<HttpContent> GetRequestAsync(string requestUri, CancellationToken cancellationToken, bool handleHttpException = true)
        {
            return await RequestContentAsync(HttpRequestType.Get, requestUri, null, cancellationToken, handleHttpException);
        }

        public async Task<HttpContent> PutRequestAsync(string requestUri, CancellationToken cancellationToken, object content = null, bool handleHttpException = true)
        {
            return await RequestContentAsync(HttpRequestType.Put, requestUri, content, cancellationToken, handleHttpException);
        }

        public async Task<HttpContent> PostRequestAsync(string requestUri, CancellationToken cancellationToken, object content = null, bool handleHttpException = true)
        {
            return await RequestContentAsync(HttpRequestType.Post, requestUri, content, cancellationToken, handleHttpException);
        }

        public async Task<HttpContent> DeleteRequestAsync(string requestUri, CancellationToken cancellationToken, bool handleHttpException = true)
        {
            return await RequestContentAsync(HttpRequestType.Delete, requestUri, null, cancellationToken, handleHttpException);
        }

        protected async Task<HttpContent> RequestContentAsync(HttpRequestType requestType, string uri, object content, CancellationToken cancellationToken, bool handleHttpException = true)
        {
            var requestResponse = await RequestAsync(requestType, uri, content, cancellationToken);

            if (requestResponse != null)
            {
                if (requestResponse.IsSuccessStatusCode)
                {
                    return requestResponse.Content;
                }
                else
                {
                    string resString = await requestResponse.Content.ReadAsStringAsync();

                    throw GetExceptionForHandling(requestResponse.StatusCode, resString, handleHttpException);
                }
            }
            else
            {
                throw new ServerErrorException("Server did not respond.");
            }
        }

        protected async Task<HttpResponseMessage> RequestAsync(HttpRequestType verb, string requestUri, object content, CancellationToken cancellationToken)
        {
            HttpResponseMessage requestResponse;

            if (content == null)
            {
                var jsonString = JsonConvert.SerializeObject(string.Empty);
                requestResponse = await this.HttpClient.RequestAsync(verb, requestUri, cancellationToken, new StringContent(jsonString, Encoding.UTF8, "application/json"));
            }
            else
            {
                var nameValueColl = content as List<KeyValuePair<string, string>>;
                var byteArrayContent = content as ByteArrayContent;

                if (nameValueColl != null)
                {
                    requestResponse = await this.HttpClient.RequestAsync(verb, requestUri, cancellationToken, new FormUrlEncodedContent(nameValueColl));
                }
                else if (byteArrayContent != null)
                {
                    requestResponse = await this.HttpClient.RequestAsync(verb, requestUri, cancellationToken, byteArrayContent);
                }
                else
                {
                    var httpContent = (HttpContent)content;
                    requestResponse = await this.HttpClient.RequestAsync(verb, requestUri, cancellationToken, httpContent);
                }
            }

            if (requestResponse != null)
            {
                return requestResponse;
            }
            else
            {
                throw new ServerErrorException("Server did not respond.");
            }
        }

        protected Exception HandleHttpException(HttpStatusCode httpStatusCode, string errorMessage = "")
        {
            switch (httpStatusCode)
            {
                case HttpStatusCode.InternalServerError:
                    return new ServerErrorException(errorMessage);
                case HttpStatusCode.NotFound:
                    return new NotFoundException(errorMessage);
                default:
                    return new ServerErrorException(errorMessage);
            }
        }

        private Exception GetExceptionForHandling(HttpStatusCode httpStatusCode, string errorMessage = "", bool handleHttpException = true)
        {
            if (handleHttpException)
            {
                return HandleHttpException(httpStatusCode, errorMessage);
            }
            else
            {
                return new HttpCodeException(errorMessage, httpStatusCode);
            }
        }
    }
}
