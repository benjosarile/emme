﻿using EmmeApp.Common.Enum;
using EmmeApp.WebServices.Abstractions;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace EmmeApp.WebServices.Base
{
	public class EmmeHttpClient : IEmmeHttpClient
	{
		private readonly HttpClient _httpClient;
		public HttpRequestHeaders DefaultRequestHeaders => _httpClient.DefaultRequestHeaders;

		public Uri BaseAddress
		{
			get { return _httpClient.BaseAddress; }
			set { _httpClient.BaseAddress = value; }
		}

		public EmmeHttpClient()
		{
			_httpClient = new HttpClient();
			_httpClient.Timeout = TimeSpan.FromSeconds(300);
		}

		public EmmeHttpClient(System.Net.Http.HttpClientHandler handler, TimeSpan timeout)
		{
			_httpClient = new HttpClient(handler);
			_httpClient.Timeout = timeout;
		}

		public async Task<HttpResponseMessage> DeleteAsync(string requestUri)
		{
			return await _httpClient.DeleteAsync(requestUri);
		}

		public async Task<HttpResponseMessage> GetAsync(string requestUri)
		{
			return await _httpClient.GetAsync(requestUri);
		}

		public async Task<HttpResponseMessage> PostAsync(string requestUri, HttpContent stringJsonContent)
		{
			return await _httpClient.PostAsync(requestUri, stringJsonContent);
		}

		public async Task<HttpResponseMessage> PutAsync(string requestUri, HttpContent stringJsonContent)
		{
			return await _httpClient.PutAsync(requestUri, stringJsonContent);
		}

		public async Task<HttpResponseMessage> RequestAsync(HttpRequestType httpRequestType, string requestUri, CancellationToken cancellationToken, HttpContent content = null)
		{
			switch (httpRequestType)
			{
				case HttpRequestType.Delete:
					return await _httpClient.DeleteAsync(requestUri,cancellationToken);
				case HttpRequestType.Post:
					return await _httpClient.PostAsync(requestUri, content, cancellationToken);
				case HttpRequestType.Put:
					return await _httpClient.PutAsync(requestUri, content, cancellationToken);
				default:
					return await _httpClient.GetAsync(requestUri, cancellationToken);
			}
		}

		public void AddHeader(string key, string value)
		{
			if (_httpClient.DefaultRequestHeaders.Contains(key))
			{
				_httpClient.DefaultRequestHeaders.Remove(key);
			}

			_httpClient.DefaultRequestHeaders.Add(key, value);
		}

	}
}
