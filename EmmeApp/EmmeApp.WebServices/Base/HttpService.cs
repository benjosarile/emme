﻿using EmmeApp.Common.Constants;
using EmmeApp.WebServices.Abstractions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace EmmeApp.WebServices.Base
{
    public class HttpService : IHttpService
	{
		public HttpService()
		{
			if (Cookie == null) Cookie = new CookieContainer();
			if (HttpClientHandler == null) HttpClientHandler = new HttpClientHandler { CookieContainer = Cookie, UseCookies = true };

			EmmeHttpClient = new EmmeHttpClient(HttpClientHandler, TimeSpan.FromSeconds(120)) { BaseAddress = new Uri(Server.BiralServerBaseAddress) };
			EmmeHttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

		}

		public IEmmeHttpClient EmmeHttpClient { get; private set; }

		public CookieContainer Cookie { get; }

		public HttpClientHandler HttpClientHandler { get; }

		public void ResetServerBaseAddress(string baseAddress, TimeSpan timeout)
		{
			EmmeHttpClient = new EmmeHttpClient(HttpClientHandler,  timeout) { BaseAddress = new Uri(baseAddress) };
			EmmeHttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
		}

		public void AddClientRequestHeaders(Dictionary<string, string> headers)
		{
			if (EmmeHttpClient != null)
			{
				foreach (KeyValuePair<string, string> header in headers)
				{
					EmmeHttpClient.DefaultRequestHeaders.Add(header.Key, header.Value);
				}
			}
		}

	}
}
