﻿using EmmeApp.Common.Constants;
using EmmeApp.DataContracts.Response;
using EmmeApp.WebServices.Abstractions;
using EmmeApp.WebServices.Base;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace EmmeApp.WebServices
{
    public class OcrWebService : WebServiceBase, IOcrWebService
	{
		public OcrWebService(IHttpService httpService) : base(httpService)
		{
			httpService.ResetServerBaseAddress(Server.AzureCognitiveApiBaseAddress, TimeSpan.FromSeconds(20));
			httpService.AddClientRequestHeaders(new Dictionary<string, string>()
			{
				{ Server.OcmApimSubscriptionKeyHeader, Server.OcmApimSubscriptionKey }
			});
		}

		public async Task<OcrObjectRootResponseContract> ReadTextOnImage(byte[] byteData, CancellationToken cancellationToken)
		{
			try
			{
				ByteArrayContent content = new ByteArrayContent(byteData);
				content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

				var responseMessage = await PostRequestAsync(Endpoints.AzureCognitiveOcr, cancellationToken, content);

				if(responseMessage != null)
				{
					string contentString = await responseMessage.ReadAsStringAsync();
					return JsonConvert.DeserializeObject<OcrObjectRootResponseContract>(contentString);
				}
			}
			catch(Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}

			return null;
		}
	}
}
