﻿using EmmeApp.Common.Constants;
using EmmeApp.DataContracts.Response;
using EmmeApp.WebServices.Abstractions;
using EmmeApp.WebServices.Base;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace EmmeApp.WebServices
{
    public class SortimentWebService : WebServiceBase, ISortimentWebService
	{
		public SortimentWebService(IHttpService httpService) : base(httpService)
		{
			httpService.ResetServerBaseAddress(Server.BiralServerBaseAddress, TimeSpan.FromSeconds(120));
		}

		public async Task<SortimentContract> GetSortiment()
		{
			try
			{
				var cts = new CancellationTokenSource(TimeSpan.FromSeconds(10));
				
				var responseMessage = await GetRequestAsync(Endpoints.GetSortiment, cts.Token);
				if (responseMessage != null)
				{
					var sortimentContract = JsonConvert.DeserializeObject<SortimentContract>(await responseMessage.ReadAsStringAsync());
					sortimentContract.LastModified = responseMessage.Headers.LastModified;
					return sortimentContract;
				}
			}
			catch(Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}

			return null;
		}

	}
}
