﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;

namespace EmmeApp.WebServices.Abstractions
{ 
	public interface IHttpService
	{
		CookieContainer Cookie { get; }
		HttpClientHandler HttpClientHandler { get; }
		IEmmeHttpClient EmmeHttpClient { get; }

		void ResetServerBaseAddress(string baseAddress, TimeSpan timeout);
		void AddClientRequestHeaders(Dictionary<string, string> headers);
	}
}
