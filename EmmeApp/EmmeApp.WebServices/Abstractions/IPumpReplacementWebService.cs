﻿using EmmeApp.DataContracts.Response;
using System.Threading.Tasks;

namespace EmmeApp.WebServices.Abstractions
{
	public interface IPumpReplacementWebService
	{
		Task<PumpReplacementRootContract> GetExchange(string marketKey);
	}
}