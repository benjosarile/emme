﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace EmmeApp.WebServices.Abstractions
{
	public interface IWebServiceBase
	{
		Task<HttpContent> GetRequestAsync(string requestUri, CancellationToken cancellationToken, bool handleHttpException = true);
		Task<HttpContent> PostRequestAsync(string requestUri, CancellationToken cancellationToken, object content = null, bool handleHttpException = true);
		Task<HttpContent> PutRequestAsync(string requestUri, CancellationToken cancellationToken, object content = null, bool handleHttpException = true);
		Task<HttpContent> DeleteRequestAsync(string requestUri, CancellationToken cancellationToken,  bool handleHttpException = true);
	}
}