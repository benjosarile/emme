﻿using EmmeApp.DataContracts.Response;
using System.Threading.Tasks;

namespace EmmeApp.WebServices.Abstractions
{
    public interface ISortimentWebService
	{
		Task<SortimentContract> GetSortiment();
	}
}