﻿using EmmeApp.DataContracts.Response;
using System.Threading.Tasks;

namespace EmmeApp.WebServices.Abstractions
{
	public interface IDocumentsWebService
	{
		Task<object> Search(string searchQuery, string filterLang, string lang, string filterCountry);
		Task<string> GetLink(string pdfLink);
		Task<CatalogDetailsContract> GetCatalogDetails(string portal, string catalog);
	}
}