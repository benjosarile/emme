﻿using EmmeApp.DataContracts.Response;
using System.Threading;
using System.Threading.Tasks;

namespace EmmeApp.WebServices.Abstractions
{
	public interface IOcrWebService
	{
		Task<OcrObjectRootResponseContract> ReadTextOnImage(byte[] byteData, CancellationToken cancellationToken);
	}
}
