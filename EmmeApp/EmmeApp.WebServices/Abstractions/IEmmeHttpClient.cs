﻿using EmmeApp.Common.Enum;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace EmmeApp.WebServices.Abstractions
{
	public interface IEmmeHttpClient
	{
		Uri BaseAddress { get; set; }

		HttpRequestHeaders DefaultRequestHeaders { get; }

		void AddHeader(string key, string value);

		Task<HttpResponseMessage> RequestAsync(HttpRequestType httpRequestType, string requestUri, CancellationToken cancellationToken, HttpContent content = null);
	
		Task<HttpResponseMessage> GetAsync(string requestUri);

		Task<HttpResponseMessage> PostAsync(string requestUri, HttpContent stringJsonContent);

		Task<HttpResponseMessage> PutAsync(string requestUri, HttpContent stringJsonContent);

		Task<HttpResponseMessage> DeleteAsync(string requestUri);
	}
}