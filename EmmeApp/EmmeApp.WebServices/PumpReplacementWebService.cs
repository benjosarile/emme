﻿using EmmeApp.Common.Constants;
using EmmeApp.DataContracts.Response;
using EmmeApp.WebServices.Abstractions;
using EmmeApp.WebServices.Base;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace EmmeApp.WebServices
{
	public class PumpReplacementWebService : WebServiceBase, IPumpReplacementWebService
	{
		public PumpReplacementWebService(IHttpService httpService) : base(httpService)
		{
			httpService.ResetServerBaseAddress(Server.BiralServerBaseAddress, TimeSpan.FromSeconds(120));
		}

		public async Task<PumpReplacementRootContract> GetExchange(string marketKey)
		{
			try
			{
				string url = string.Format(Endpoints.GetExchange, marketKey.ToLower()).ToLower();

				var responseMessage = await GetRequestAsync(url, CancellationToken.None);
				if (responseMessage != null)
				{
					var pumpReplacementRootContract = JsonConvert.DeserializeObject<PumpReplacementRootContract>(await responseMessage.ReadAsStringAsync());
					return pumpReplacementRootContract;
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}

			return null;
		}
	}
}
