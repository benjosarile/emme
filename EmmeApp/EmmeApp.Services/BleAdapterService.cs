﻿using EmmeApp.ProtocolServices.Abstractions;
using Plugin.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace EmmeApp.ProtocolServices
{
    public class BleAdapterService : IBleAdapterService
    {
        private readonly IAdapter _adapter;

        public BleAdapterService(IAdapter adapter)
        {
            _adapter = adapter;
        }

        public IObservable<IEnumerable<IDevice>> GetConnectedDevices(Guid? serviceUuid = null)
        {
            return _adapter.GetConnectedDevices(serviceUuid);
        }

        public async Task<IEnumerable<IDevice>> GetConnectedDevices(Guid serviceUuid, List<Guid> characteristicUuids)
        {
            var devices = await _adapter.GetConnectedDevices(serviceUuid);

            var eligibleDevice = new List<IDevice>();

            foreach (var device in devices)
            {
                bool isEligible = true;
                foreach (var characteristicUuid in characteristicUuids)
                {
                    var characteristicUuidList = new List<Guid>();
                    characteristicUuidList.Add(characteristicUuid);

                    try
                    {
                        var knownCharacteristic = await device.GetKnownCharacteristics(serviceUuid, characteristicUuidList.ToArray());

                        if (knownCharacteristic == null)
                        {
                            isEligible = false;
                            break;
                        }
                    }
                    catch
                    {
                        isEligible = false;
                        break;
                    }
                }

                if (isEligible)
                {
                    eligibleDevice.Add(device);
                }
            }

            return devices;
        }

        public async Task<IEnumerable<IDevice>> GetConnectedDevicesAsync(Guid? serviceUuid = null)
        {
            return await _adapter.GetConnectedDevices(serviceUuid);
        }

        public IObservable<IDevice> GetKnownDevice(Guid deviceId)
        {
            return _adapter.GetKnownDevice(deviceId);
        }

        public async Task<IDevice> GetKnownDeviceAsync(Guid deviceId)
        {
            return await _adapter.GetKnownDevice(deviceId);
        }
    }
}