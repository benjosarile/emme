﻿using EmmeApp.ProtocolServices.Abstractions;
using ProtoBuf;
using System.Collections.Generic;
using System.IO;

namespace EmmeApp.ProtocolServices
{
    public class ProtocolBufferService : IProtocolBufferService
	{
		public T Deserialize<T>(byte[] data)
		{
			try
			{
				var ms = new MemoryStream(data);
				T tr;

				while ((tr = Serializer.Deserialize<T>(ms)) != null) //fs.Length > fs.Position)
				{
					if (ms.Position == ms.Length)
					{
						break;
					}
				}

				return tr;
			}
			catch
			{

			}

			return default(T);
		}

		public byte[] Serialize<T>(T data)
		{
			if (data != null)
			{
				using (var ms = new MemoryStream())
				{
					Serializer.Serialize(ms, data);
					var buffer = ms.ToArray();
					return buffer;
				}
			}

			return new List<byte>().ToArray();
		}
	}
}
