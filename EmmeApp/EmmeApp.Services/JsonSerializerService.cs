﻿using EmmeApp.ProtocolServices.Abstractions;
using Newtonsoft.Json;
using System;

namespace EmmeApp.ProtocolServices
{
    public class JsonSerializerService : IJsonService
	{
		public T DeserializeObject<T>(string value, JsonSerializerSettings settings)
		{
			return JsonConvert.DeserializeObject<T>(value, settings);
		}

		public object DeserializeObject(string value)
		{
			return JsonConvert.DeserializeObject(value);
		}

		public object DeserializeObject(string value, JsonSerializerSettings settings)
		{
			return JsonConvert.DeserializeObject(value, settings);
		}

		public object DeserializeObject(string value, Type type)
		{
			return JsonConvert.DeserializeObject(value, type);
		}

		public T DeserializeObject<T>(string value)
		{
			return JsonConvert.DeserializeObject<T>(value);
		}

		public object DeserializeObject(string value, Type type, JsonSerializerSettings settings)
		{
			return JsonConvert.DeserializeObject(value, type, settings);
		}

		public object DeserializeObject(string value, Type type, params JsonConverter[] converters)
		{
			return JsonConvert.DeserializeObject(value, type, converters);
		}

		public T DeserializeObject<T>(string value, params JsonConverter[] converters)
		{
			return JsonConvert.DeserializeObject<T>(value, converters);
		}

		public string SerializeObject(object value)
		{
			return JsonConvert.SerializeObject(value);
		}

		public string SerializeObject(object value, Formatting formatting)
		{
			return JsonConvert.SerializeObject(value, formatting);
		}

		public string SerializeObject(object value, Type type, JsonSerializerSettings settings)
		{
			return JsonConvert.SerializeObject(value, type, settings);
		}

		public string SerializeObject(object value, params JsonConverter[] converters)
		{
			return JsonConvert.SerializeObject(value, converters);
		}

		public string SerializeObject(object value, Formatting formatting, params JsonConverter[] converters)
		{
			return JsonConvert.SerializeObject(value, formatting, converters);
		}

		public string SerializeObject(object value, JsonSerializerSettings settings)
		{
			return JsonConvert.SerializeObject(value, settings);
		}

		public string SerializeObject(object value, Formatting formatting, JsonSerializerSettings settings)
		{
			return JsonConvert.SerializeObject(value, formatting, settings);
		}

		public string SerializeObject(object value, Type type, Formatting formatting, JsonSerializerSettings settings)
		{
			return JsonConvert.SerializeObject(value, formatting, settings);
		}
	}
}
