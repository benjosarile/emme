﻿namespace EmmeApp.ProtocolServices.Abstractions
{
	public interface IProtocolBufferService
	{
		T Deserialize<T>(byte[] data);
		byte[] Serialize<T>(T data);
	}
}