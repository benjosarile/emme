﻿using Newtonsoft.Json;
using System;

namespace EmmeApp.ProtocolServices.Abstractions
{
	public interface IJsonService
	{
		T DeserializeObject<T>(string value, JsonSerializerSettings settings);
		object DeserializeObject(string value);
		object DeserializeObject(string value, JsonSerializerSettings settings);
		object DeserializeObject(string value, Type type);
		T DeserializeObject<T>(string value);
		object DeserializeObject(string value, Type type, JsonSerializerSettings settings);
		object DeserializeObject(string value, Type type, params JsonConverter[] converters);
		T DeserializeObject<T>(string value, params JsonConverter[] converters);

		string SerializeObject(object value);
		string SerializeObject(object value, Formatting formatting);
		string SerializeObject(object value, Type type, JsonSerializerSettings settings);
		string SerializeObject(object value, params JsonConverter[] converters);
		string SerializeObject(object value, Formatting formatting, params JsonConverter[] converters);
		string SerializeObject(object value, JsonSerializerSettings settings);
		string SerializeObject(object value, Formatting formatting, JsonSerializerSettings settings);
		string SerializeObject(object value, Type type, Formatting formatting, JsonSerializerSettings settings);
	}
}