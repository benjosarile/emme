﻿using Plugin.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmmeApp.ProtocolServices.Abstractions
{
	public interface IBleAdapterService
	{
		IObservable<IEnumerable<IDevice>> GetConnectedDevices(Guid? serviceUuid = null);
		Task<IEnumerable<IDevice>> GetConnectedDevices(Guid serviceUuid, List<Guid> characteristicUuids);
		Task<IEnumerable<IDevice>> GetConnectedDevicesAsync(Guid? serviceUuid = null);
		IObservable<IDevice> GetKnownDevice(Guid deviceId);
		Task<IDevice> GetKnownDeviceAsync(Guid deviceId);
	}
}