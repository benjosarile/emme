﻿using ProtoBuf;

namespace EmmeApp.DataContracts.Enums
{
    [ProtoContract(Name = @"variA_parameterId")]
	public enum ExternalVariAParameterIdEnumContract
	{
		[ProtoEnum(Name = @"variA_todo")]
		VariAtodo = 0,
	}
}
