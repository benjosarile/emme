﻿using ProtoBuf;

namespace EmmeApp.DataContracts.Enums
{
    [ProtoContract(Name = @"logSource_t")]
	public enum ExternalLogSourceEnumContract
	{
		[ProtoEnum(Name = @"unknownLogSource")]
		UnknownLogSource = 0,
		[ProtoEnum(Name = @"noLogSource")]
		NoLogSource = 1,
		[ProtoEnum(Name = @"external")]
		External = 2,
		[ProtoEnum(Name = @"internal")]
		Internal = 3,
	}
}
