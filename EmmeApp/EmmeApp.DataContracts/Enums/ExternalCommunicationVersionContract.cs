﻿using ProtoBuf;

namespace EmmeApp.DataContracts.Enums
{
    [ProtoContract(Name = @"communicationVersion_t")]
	public enum ExternalCommunicationVersionContract
	{
		[ProtoEnum(Name = @"unknownLevel")]
		UnknownLevel = 0,
		[ProtoEnum(Name = @"Version1")]
		Version1 = 1
	}
}
