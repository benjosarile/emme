﻿namespace EmmeApp.DataContracts.Enums
{
    public enum ExternalValueOneofCaseEnumContract
	{
		None = 0,
		Text = 1,
		Number = 2,
	}
}
