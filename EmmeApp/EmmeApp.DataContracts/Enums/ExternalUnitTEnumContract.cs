﻿using ProtoBuf;

namespace EmmeApp.DataContracts.Enums
{
    [ProtoContract(Name = @"unit_t")]
	public enum ExternalUnitTEnumContract
	{
		[ProtoEnum(Name = @"unknownUnit")]
		UnknownUnit = 0,
		[ProtoEnum(Name = @"noUnit")]
		NoUnit = 1,
		[ProtoEnum(Name = @"A")]
		Ampere = 2,
		[ProtoEnum(Name = @"mA")]
		Miliampere = 3,
		[ProtoEnum(Name = @"s")]
		Seconds = 4,
		[ProtoEnum(Name = @"d")]
		Days = 5,
		[ProtoEnum(Name = @"cubicMperH")]
		CubicMeterPerHour = 6,
		[ProtoEnum(Name = @"W")]
		Watt = 7,
		[ProtoEnum(Name = @"m")]
		Meter = 8,
		[ProtoEnum(Name = @"m")]
		M3 = 9,
		[ProtoEnum(Name = @"m")]
		Pmin = 10,
		[ProtoEnum(Name = @"m")]
		Kwh = 11,
		[ProtoEnum(Name = @"m")]
		Hours = 12,
		[ProtoEnum(Name = @"m")]
		Percent = 13,
		[ProtoEnum(Name = @"m")]
		Celsius = 14
	}
}
