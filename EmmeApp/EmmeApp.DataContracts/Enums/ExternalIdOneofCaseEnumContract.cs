﻿namespace EmmeApp.DataContracts.Enums
{
    public enum ExternalIdOneofCaseEnumContract
	{
		None = 0,
		ControlUnit = 1,
		ModulA = 2,
		VariA = 3
	}
}
