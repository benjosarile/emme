﻿using ProtoBuf;

namespace EmmeApp.DataContracts.Enums
{
    [ProtoContract(Name = @"logType_t")]
	public enum ExternalLogTypeEnumContract
	{
		[ProtoEnum(Name = @"unknownLogType")]
		UnknownLogType = 0,
		[ProtoEnum(Name = @"noLogType")]
		NoLogType = 1,
		[ProtoEnum(Name = @"info")]
		Info = 2,
		[ProtoEnum(Name = @"warning")]
		Warning = 3,
		[ProtoEnum(Name = @"alarm")]
		Alarm = 4,
		[ProtoEnum(Name = @"failure")]
		Failure = 5
	}
}
