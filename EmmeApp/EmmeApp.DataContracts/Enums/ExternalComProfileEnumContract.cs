﻿using ProtoBuf;

namespace EmmeApp.DataContracts.Enums
{
    [ProtoContract(Name = @"comProfile_t")]
	public enum ExternalComProfileEnumContract
	{
		[ProtoEnum(Name = @"unknown")]
		Unknown = 0,
		[ProtoEnum(Name = @"dismessa")]
		Dismessa = 1,
		[ProtoEnum(Name = @"modulA")]
		ModulA = 2,
		[ProtoEnum(Name = @"variA")]
		VariA = 3,
	}
}
