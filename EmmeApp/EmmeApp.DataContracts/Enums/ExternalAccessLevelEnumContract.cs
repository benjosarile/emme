﻿using ProtoBuf;

namespace EmmeApp.DataContracts.Enums
{
    [ProtoContract(Name = @"accessLevel_t")]
	public enum ExternalAccessLevelEnumContract
	{
		[ProtoEnum(Name = @"unknownLevel")]
		UnknownLevel = 0,
		[ProtoEnum(Name = @"dev")]
		Dev = 1,
		[ProtoEnum(Name = @"manufact")]
		Manufact = 2,
		[ProtoEnum(Name = @"service")]
		Service = 3,
		[ProtoEnum(Name = @"user")]
		User = 4
	}
}
