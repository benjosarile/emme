﻿using EmmeApp.DataContracts.Enums;
using ProtoBuf;

namespace EmmeApp.DataContracts.External
{
	[ProtoContract(Name = @"logEntry_t", UseProtoMembersOnly = true)]
	public class LogEntryTExternalContract : BaseExternalContract
	{
		[ProtoMember(1, Name = @"logId", DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public int LogId { get; set; }

		[ProtoMember(4, Name = @"timeAppear", DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public uint TimeAppear { get; set; }

		[ProtoMember(5, Name = @"timeDisappear", DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public uint TimeDisappear { get; set; }

		[ProtoMember(6, Name = @"source", DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public ExternalLogSourceEnumContract Source { get; set; }

		[ProtoMember(7, Name = @"type", DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public ExternalLogTypeEnumContract Type { get; set; }
	}
}
