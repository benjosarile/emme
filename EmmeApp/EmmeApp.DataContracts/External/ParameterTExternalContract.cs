﻿using EmmeApp.DataContracts.Enums;
using ProtoBuf;

namespace EmmeApp.DataContracts.External
{
    [ProtoContract(Name = @"parameter_t")]
	public class ParameterTExternalContract : BaseExternalContract
	{
		[ProtoMember(1, DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public ExternalControlUnitParameterIdEnumContract ControlUnit
		{
			get { return __pbn__id.Is(1) ? ((ExternalControlUnitParameterIdEnumContract)__pbn__id.Int32) : default(int); }
			set { __pbn__id = new DiscriminatedUnion32(1, (int)value); }
		}

		public bool ShouldSerializecontrolUnit() => __pbn__id.Is(1);
		public void ResetcontrolUnit() => DiscriminatedUnion32.Reset(ref __pbn__id, 1);

		private DiscriminatedUnion32 __pbn__id;

		[ProtoMember(2, DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public ExternalPumpModulAParameterIdEnumContract ModulA
		{
			get { return __pbn__id.Is(2) ? ((ExternalPumpModulAParameterIdEnumContract)__pbn__id.Int32) : default(int); }
			set { __pbn__id = new global::ProtoBuf.DiscriminatedUnion32(2, (int)value); }
		}
		public bool ShouldSerializemodulA() => __pbn__id.Is(2);
		public void ResetmodulA() => global::ProtoBuf.DiscriminatedUnion32.Reset(ref __pbn__id, 2);

		[ProtoMember(3, DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public ExternalPumpVariAParameterIdEnumContract VariA
		{
			get { return __pbn__id.Is(3) ? ((ExternalPumpVariAParameterIdEnumContract)__pbn__id.Int32) : default(int); }
			set { __pbn__id = new global::ProtoBuf.DiscriminatedUnion32(3, (int)value); }
		}
		public bool ShouldSerializevariA() => __pbn__id.Is(3);
		public void ResetvariA() => global::ProtoBuf.DiscriminatedUnion32.Reset(ref __pbn__id, 3);


		[ProtoMember(4, Name = @"value", DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public ValueTExternalContract Value { get; set; }

		[ProtoMember(5, Name = @"scaling", DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public ScalingTExternalContract Scaling { get; set; }

		[ProtoMember(6, Name = @"unit", DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public ExternalUnitTEnumContract Unit { get; set; }

		[ProtoMember(7, Name = @"read", DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public ExternalAccessLevelEnumContract Read { get; set; }

		[ProtoMember(8, Name = @"write", DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public ExternalAccessLevelEnumContract Write { get; set; }

		public ExternalIdOneofCaseEnumContract IdCase => (ExternalIdOneofCaseEnumContract)__pbn__id.Discriminator;
		
	}
}
