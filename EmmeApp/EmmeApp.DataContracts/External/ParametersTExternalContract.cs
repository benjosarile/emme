﻿using ProtoBuf;
using System.Collections.Generic;

namespace EmmeApp.DataContracts.External
{
	[ProtoContract(Name = @"parameters_t")]
	public class ParametersTExternalContract : BaseExternalContract
	{
		[ProtoMember(1, Name = @"parameter")]
		public List<ParameterTExternalContract> Parameters { get; set; } = new List<ParameterTExternalContract>();
	}
}
