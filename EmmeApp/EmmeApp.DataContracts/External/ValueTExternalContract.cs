﻿using EmmeApp.DataContracts.Enums;
using ProtoBuf;
using System.ComponentModel;

namespace EmmeApp.DataContracts.External
{
	[ProtoContract(Name = @"value_t", UseProtoMembersOnly = true)]
	public class ValueTExternalContract : BaseExternalContract
	{
		[ProtoMember(1, Name = @"text", IsRequired = true)]
		[DefaultValue("")]
		public string Text
		{
			get { return __pbn__value.Is(1) ? ((string)__pbn__value.Object) : ""; }
			set { __pbn__value = new DiscriminatedUnion64Object(1, value); }
		}
		public bool ShouldSerializeText() => __pbn__value.Is(1);
		public void ResetText() => DiscriminatedUnion64Object.Reset(ref __pbn__value, 1);

		private DiscriminatedUnion64Object __pbn__value;

		[ProtoMember(2, Name = @"number", IsRequired = true)]
		public long Number
		{
			get { return __pbn__value.Is(2) ? __pbn__value.Int64 : default(long); }
			set { __pbn__value = new DiscriminatedUnion64Object(2, value); }
		}
		public bool ShouldSerializeNumber() => __pbn__value.Is(2);
		public void ResetNumber() => DiscriminatedUnion64Object.Reset(ref __pbn__value, 2);

		public ExternalValueOneofCaseEnumContract ValueCase => (ExternalValueOneofCaseEnumContract)__pbn__value.Discriminator;
	}
}
