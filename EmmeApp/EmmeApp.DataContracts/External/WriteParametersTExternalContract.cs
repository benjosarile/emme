﻿using ProtoBuf;
using System.Collections.Generic;

namespace EmmeApp.DataContracts.External
{
	[ProtoContract(Name = @"parameters_t")]
	public class WriteParametersControlUnitExternalContract : BaseExternalContract
	{
		[ProtoMember(1, Name = @"parameter")]
		public List<WriteParameterControlUnitExternalContract> Parameters { get; set; } = new List<WriteParameterControlUnitExternalContract>();
	}

	[ProtoContract(Name = @"parameters_t")]
	public class WriteParametersPumpModulAExternalContract : BaseExternalContract
	{
		[ProtoMember(1, Name = @"parameter")]
		public List<WriteParameterPumpModulAExternalContract> Parameters { get; set; } = new List<WriteParameterPumpModulAExternalContract>();
	}

	[ProtoContract(Name = @"parameters_t")]
	public class WriteParametersPumpVariAExternalContract : BaseExternalContract
	{
		[ProtoMember(1, Name = @"parameter")]
		public List<WriteParameterPumpVariAExternalContract> Parameters { get; set; } = new List<WriteParameterPumpVariAExternalContract>();
	}
}
