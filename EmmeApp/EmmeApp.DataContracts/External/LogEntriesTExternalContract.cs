﻿using ProtoBuf;
using System.Collections.Generic;

namespace EmmeApp.DataContracts.External
{
    [ProtoContract(Name = @"logEntries_t")]
	public class LogEntriesTExternalContract : BaseExternalContract
	{
		[ProtoMember(1, Name = @"logEntry")]
		public List<LogEntryTExternalContract> LogEntries { get; set; } = new List<LogEntryTExternalContract>();
	}
}
