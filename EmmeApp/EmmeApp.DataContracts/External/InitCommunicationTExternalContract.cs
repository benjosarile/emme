﻿using EmmeApp.DataContracts.Enums;
using ProtoBuf;

namespace EmmeApp.DataContracts.External
{
    [ProtoContract(Name = @"initCommunication_t", UseProtoMembersOnly = true)]
	public class InitCommunicationTExternalContract : BaseExternalContract
	{
		[ProtoMember(1, DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public ExternalComProfileEnumContract ProfileId { get; set; }

		[ProtoMember(2, Name = @"FWVersion", DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public string FwVersion { get; set; }

		[ProtoMember(3, Name = @"communicationVersion", DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public ExternalCommunicationVersionContract CommunicationVersion { get; set; }

	}
}
