﻿using EmmeApp.DataContracts.Enums;
using ProtoBuf;

namespace EmmeApp.DataContracts.External
{
    [ProtoContract(Name = @"parameter_t")]
	public class WriteParameterControlUnitExternalContract : BaseExternalContract
	{
		[ProtoMember(1, DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public ExternalControlUnitParameterIdEnumContract ControlUnit { get; set; }

		[ProtoMember(4, Name = @"value", IsRequired = true)]
		public ValueTExternalContract Value { get; set; } = new ValueTExternalContract();
	}

	[ProtoContract(Name = @"parameter_t")]
	public class WriteParameterPumpModulAExternalContract : BaseExternalContract
	{
		[ProtoMember(2, DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public ExternalPumpModulAParameterIdEnumContract ModulA { get; set; }

		[ProtoMember(4, Name = @"value", IsRequired = true)]
		public ValueTExternalContract Value { get; set; } = new ValueTExternalContract();
	}

	[ProtoContract(Name = @"parameter_t")]
	public class WriteParameterPumpVariAExternalContract : BaseExternalContract
	{
		[ProtoMember(3, DataFormat = DataFormat.FixedSize, IsRequired = true)]
		public ExternalPumpVariAParameterIdEnumContract VariA { get; set; }

		[ProtoMember(4, Name = @"value",  IsRequired = true)]
		public ValueTExternalContract Value { get; set; } = new ValueTExternalContract();
	}
}