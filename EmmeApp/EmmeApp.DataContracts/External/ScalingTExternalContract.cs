﻿using ProtoBuf;

namespace EmmeApp.DataContracts.External
{
    [ProtoContract(Name = @"scaling_t")]
	public class ScalingTExternalContract : BaseExternalContract
	{
		[ProtoMember(1, Name = @"max")]
		public long Max { get; set; }

		[ProtoMember(2, Name = @"min")]
		public long Min { get; set; }

		[ProtoMember(3, Name = @"step")]
		public double Step { get; set; }

	}
}
