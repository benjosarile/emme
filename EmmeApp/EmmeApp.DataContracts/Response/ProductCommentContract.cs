﻿using Newtonsoft.Json;

namespace EmmeApp.DataContracts.Response
{
    public class ProductCommentContract
	{
		[JsonProperty("cd")]
		public long CommentId { get; set; }

		[JsonProperty("tx")]
		public string Text { get; set; }

		[JsonProperty("l10n")]
		public LocalizationContract L10N { get; set; }
	}
}
