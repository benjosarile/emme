﻿using Newtonsoft.Json;

namespace EmmeApp.DataContracts.Response
{
    public class ProductCategoryContract
	{
		[JsonProperty("id")]
		public long CategoryId { get; set; }

		[JsonProperty("na")]
		public string Name { get; set; }

		[JsonProperty("l10n")]
		public LocalizationContract L10N { get; set; }
	}

	public class LocalizationContract
	{
		[JsonProperty("fr", NullValueHandling = NullValueHandling.Ignore)]
		public LocalizationDetailContract Fr { get; set; }

		[JsonProperty("it", NullValueHandling = NullValueHandling.Ignore)]
		public LocalizationDetailContract It { get; set; }

		[JsonProperty("nl", NullValueHandling = NullValueHandling.Ignore)]
		public LocalizationDetailContract Nl { get; set; }

		[JsonProperty("de", NullValueHandling = NullValueHandling.Ignore)]
		public LocalizationDetailContract De { get; set; }

		[JsonProperty("en", NullValueHandling = NullValueHandling.Ignore)]
		public LocalizationDetailContract En { get; set; }
	}

	public class LocalizationDetailContract
	{
		[JsonProperty("na")]
		public string Name { get; set; }

		[JsonProperty("tx")]
		public string Text { get; set; }

	}
}
