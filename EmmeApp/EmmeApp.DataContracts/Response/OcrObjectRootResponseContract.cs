﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EmmeApp.DataContracts.Response
{
    public class OcrObjectRootResponseContract
	{
        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("textAngle")]
        public double TextAngle { get; set; }

        [JsonProperty("orientation")]
        public string Orientation { get; set; }

        [JsonProperty("regions")]
        public List<OcrRegionContract> Regions { get; set; }
    }
}
