﻿using Newtonsoft.Json;

namespace EmmeApp.DataContracts.Response
{
    public class BiralProductContract
	{
		[JsonProperty("id")]
		public long ProductId { get; set; }

		[JsonProperty("ty")]
		public string Type { get; set; }

		[JsonProperty("di")]
		public string Diameter { get; set; }

		[JsonProperty("pr")]
		public string Pressure { get; set; }

		[JsonProperty("ln")]
		public string Length { get; set; }

		[JsonProperty("ng")]
		public string Engine { get; set; }

		[JsonProperty("cat")]
		public long Category { get; set; }
	}
}
