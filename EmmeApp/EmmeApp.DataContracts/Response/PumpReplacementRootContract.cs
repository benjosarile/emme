﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EmmeApp.DataContracts.Response
{
    public class PumpReplacementRootContract
	{
		[JsonProperty("exchange")]
		public List<PumpReplacementContract> PumpReplacements { get; set; }

		[JsonProperty("products")]
		public List<BiralProductContract> Products { get; set; }

		[JsonProperty("categories")]
		public List<ProductCategoryContract> Categories { get; set; }

		[JsonProperty("comments")]
		public List<ProductCommentContract> Comments { get; set; }

		[JsonProperty("connectors")]
		public List<ProductConnectorContract> Connectors { get; set; }
	}

	public class PumpReplacementContract
	{
		[JsonProperty("ty")]
		public string Type { get; set; }

		[JsonProperty("di")]
		public string Diameter { get; set; }

		[JsonProperty("ln")]
		public string Length { get; set; }

		[JsonProperty("pr")]
		public string Pressure { get; set; }

		[JsonProperty("ng")]
		public string Engine { get; set; }

		[JsonProperty("p")]
		public List<PumpReplacementProductContract> PumpReplacementProducts { get; set; }
	}

	public class PumpReplacementProductContract
	{
		[JsonProperty("id")]
		public long ProductId { get; set; }

		[JsonProperty("ci")]
		public List<PumpReplacementConnectorInformationContract> ConnectorInformation { get; set; }

		[JsonProperty("cd")]
		public List<string> CommentCodes { get; set; }
	}

	public class PumpReplacementConnectorInformationContract
	{
		[JsonProperty("a")]
		public int NumberOfPieces { get; set; }

		[JsonProperty("cd")]
		public string ConnectorCode { get; set; }
	}

}
