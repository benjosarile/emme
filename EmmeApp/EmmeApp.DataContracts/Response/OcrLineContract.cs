﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EmmeApp.DataContracts.Response
{
    public class OcrLineContract
    {
        [JsonProperty("boundingBox")]
        public string BoundingBox { get; set; }

        [JsonProperty("words")]
        public List<OcrWordContract> Words { get; set; }
    }
}
