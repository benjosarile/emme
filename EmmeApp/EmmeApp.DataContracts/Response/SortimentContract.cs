﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace EmmeApp.DataContracts.Response
{
    public class SortimentContract
	{
		[JsonProperty("version")]
		public int Version { get; set; }

		[JsonProperty("markets")]
		public List<SortimentMarketContract> Markets { get; set; }

		public DateTimeOffset? LastModified { get; set; }
	}

	public class SortimentMarketContract
	{
		[JsonProperty("key")]
		public string Key { get; set; }
		[JsonProperty("name")]
		public string Name { get; set; }
		[JsonProperty("l")]
		public MarketLinkContract MarketLink { get; set; }
	}

	public class MarketLinkContract
	{
		[JsonProperty("de", NullValueHandling = NullValueHandling.Ignore)]
		public Uri De { get; set; }

		[JsonProperty("fr", NullValueHandling = NullValueHandling.Ignore)]
		public Uri Fr { get; set; }

		[JsonProperty("it", NullValueHandling = NullValueHandling.Ignore)]
		public Uri It { get; set; }

		[JsonProperty("nl", NullValueHandling = NullValueHandling.Ignore)]
		public Uri Nl { get; set; }

		[JsonProperty("en", NullValueHandling = NullValueHandling.Ignore)]
		public Uri En { get; set; }
	}
}
