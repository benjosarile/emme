﻿using Newtonsoft.Json;

namespace EmmeApp.DataContracts.Response
{
    public class OcrWordContract
    {
        [JsonProperty("boundingBox")]
        public string BoundingBox { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
    }
}
