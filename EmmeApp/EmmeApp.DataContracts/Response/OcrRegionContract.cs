﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EmmeApp.DataContracts.Response
{
    public class OcrRegionContract
    {
        [JsonProperty("boundingBox")]
        public string BoundingBox { get; set; }

        [JsonProperty("lines")]
        public List<OcrLineContract> Lines { get; set; }
    }
}
