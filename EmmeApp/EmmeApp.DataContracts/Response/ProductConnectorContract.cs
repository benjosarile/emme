﻿using Newtonsoft.Json;

namespace EmmeApp.DataContracts.Response
{
    public class ProductConnectorContract
	{
		[JsonProperty("cd")]
		public string ConnectorId { get; set; }

		[JsonProperty("tx")]
		public string Text { get; set; }

		[JsonProperty("p")]
		public uint ProductNumber { get; set; }

		[JsonProperty("l10n")]
		public LocalizationContract L10N { get; set; }
	}
}
