﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace EmmeApp.DataContracts.Response
{
    public class SearchContentByTypeRootContract
	{
		[JsonProperty("success")]
		public bool Success { get; set; }

		[JsonProperty("error")]
		public bool Error { get; set; }

		[JsonProperty("currentFilters")]
		public List<SearchCurrentFilterContract> CurrentFilters { get; set; }

		[JsonProperty("filters")]
		public List<SearchFilterContract> Filters { get; set; }

		[JsonProperty("filterBox")]
		public List<SearchFilterBoxContract> FilterBox { get; set; }

		[JsonProperty("outputType")]
		public string OutputType { get; set; }

		[JsonProperty("limitHit")]
		public bool LimitHit { get; set; }

		[JsonProperty("groups")]
		public List<SearchGroupContract> Groups { get; set; }
	}

	public class SearchGroupContract
	{
		[JsonProperty("anchorLink")]
		public string AnchorLink { get; set; }

		[JsonProperty("anchorLinkName")]
		public string AnchorLinkName { get; set; }

		[JsonProperty("anchorCount")]
		public int AnchorCount { get; set; }

		[JsonProperty("code")]
		public string Code { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("catalogs")]
		public List<SearchCatalogContract> Catalogs { get; set; }
	}

	public class SearchPagesByCatalogRootContract
	{
		[JsonProperty("success")]
		public bool Success { get; set; }

		[JsonProperty("error")]
		public bool Error { get; set; }

		[JsonProperty("currentFilters")]
		public List<SearchCurrentFilterContract> CurrentFilters { get; set; }

		[JsonProperty("filters")]
		public List<SearchFilterContract> Filters { get; set; }

		[JsonProperty("filterBox")]
		public List<SearchFilterBoxContract> FilterBox { get; set; }

		[JsonProperty("outputType")]
		public string OutputType { get; set; }

		[JsonProperty("limitHit")]
		public bool LimitHit { get; set; }

		[JsonProperty("groups")]
		public List<SearchCatalogContract> Groups { get; set; }
	}

	public class SearchCurrentFilterContract
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }
	}

	public class SearchFilterBoxContract
	{
		[JsonProperty("text")]
		public string Text { get; set; }

		[JsonProperty("children")]
		public List<SearchFilterContract> Children { get; set; }
	}

	public class SearchFilterContract
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("group")]
		public string Group { get; set; }

		[JsonProperty("img", NullValueHandling = NullValueHandling.Ignore)]
		public string Img { get; set; }

		[JsonProperty("icon", NullValueHandling = NullValueHandling.Ignore)]
		public string Icon { get; set; }
	}

	public class SearchCatalogContract
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("code")]
		public object Code { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("brandName")]
		public string BrandName { get; set; }

		[JsonProperty("brandId")]
		public string BrandId { get; set; }

		[JsonProperty("brandIcon")]
		public string BrandIcon { get; set; }

		[JsonProperty("supplierNumber")]
		public string SupplierNumber { get; set; }

		[JsonProperty("supplierNumbers")]
		public string SupplierNumbers { get; set; }

		[JsonProperty("categories")]
		public List<object> Categories { get; set; }

		[JsonProperty("tags")]
		public List<object> Tags { get; set; }

		[JsonProperty("outdated")]
		public bool Outdated { get; set; }

		[JsonProperty("type")]
		public string Type { get; set; }

		[JsonProperty("lang")]
		public string Lang { get; set; }

		[JsonProperty("publishDate")]
		public DateTimeOffset PublishDate { get; set; }

		[JsonProperty("typeName")]
		public string TypeName { get; set; }

		[JsonProperty("query")]
		public string Query { get; set; }

		[JsonProperty("numPages")]
		public long NumPages { get; set; }

		[JsonProperty("printable")]
		public bool Printable { get; set; }

		[JsonProperty("previewUrl")]
		public string PreviewUrl { get; set; }

		[JsonProperty("mediumUrl")]
		public string MediumUrl { get; set; }

		[JsonProperty("archiveLabel")]
		public string ArchiveLabel { get; set; }

		[JsonProperty("pdfAvailable")]
		public bool PdfAvailable { get; set; }

		[JsonProperty("downloadUrl")]
		public string DownloadUrl { get; set; }

		[JsonProperty("pages")]
		public List<SearchPageContract> Pages { get; set; }
	}

	public class SearchPageContract
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("pageName")]
		public string PageName { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("internalName")]
		public string InternalName { get; set; }

		[JsonProperty("page")]
		public long PagePage { get; set; }

		[JsonProperty("usePageNum")]
		public string UsePageNum { get; set; }

		[JsonProperty("brand")]
		public string Brand { get; set; }

		[JsonProperty("query")]
		public string Query { get; set; }

		[JsonProperty("brandIcon")]
		public string BrandIcon { get; set; }

		[JsonProperty("previewUrl")]
		public string PreviewUrl { get; set; }

		[JsonProperty("mediumUrl")]
		public string MediumUrl { get; set; }

		[JsonProperty("outdated")]
		public bool Outdated { get; set; }

		[JsonProperty("pdfAvailable")]
		public bool PdfAvailable { get; set; }
	}

	public class CatalogDetailsContract
	{
		[JsonProperty("success")]
		public bool Success { get; set; }

		[JsonProperty("error")]
		public bool Error { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("catalog")]
		public string Catalog { get; set; }

		[JsonProperty("outdated")]
		public bool Outdated { get; set; }

		[JsonProperty("archiveLabel")]
		public string ArchiveLabel { get; set; }

		[JsonProperty("pdfAvailable")]
		public bool PdfAvailable { get; set; }

		[JsonProperty("infoplayEnabled")]
		public bool InfoplayEnabled { get; set; }

		[JsonProperty("mobileEnabled")]
		public bool MobileEnabled { get; set; }

		[JsonProperty("brandName")]
		public string BrandName { get; set; }

		[JsonProperty("brandId")]
		public string BrandId { get; set; }

		[JsonProperty("supplierNumber")]
		public string SupplierNumber { get; set; }

		[JsonProperty("supplierNumbers")]
		public List<string> SupplierNumbers { get; set; }

		[JsonProperty("numPages")]
		public long NumPages { get; set; }

		[JsonProperty("webviewerXOD")]
		public string WebviewerXod { get; set; }

		[JsonProperty("previewUrl")]
		public string PreviewUrl { get; set; }

		[JsonProperty("baseFileUrl")]
		public string BaseFileUrl { get; set; }

		[JsonProperty("isForwardingEnabled")]
		public bool IsForwardingEnabled { get; set; }

		[JsonProperty("pageOffset")]
		public long PageOffset { get; set; }

		[JsonProperty("toc")]
		public List<object> Toc { get; set; }

		[JsonProperty("pageMapping")]
		public List<object> PageMapping { get; set; }

		[JsonProperty("surcharge")]
		public object Surcharge { get; set; }

		[JsonProperty("showHintForCatalogsWithoutForwarding")]
		public bool ShowHintForCatalogsWithoutForwarding { get; set; }

		[JsonProperty("showHintForCatalogNotInPartnerPortal")]
		public bool ShowHintForCatalogNotInPartnerPortal { get; set; }

		[JsonProperty("isVisibleInPartnerPortal")]
		public bool IsVisibleInPartnerPortal { get; set; }

		[JsonProperty("extendedSecurity")]
		public bool ExtendedSecurity { get; set; }

		[JsonProperty("extendedSecurityWithWarning")]
		public bool ExtendedSecurityWithWarning { get; set; }

		[JsonProperty("paperclipActive")]
		public bool PaperclipActive { get; set; }

		[JsonProperty("deeplinksActive")]
		public bool DeeplinksActive { get; set; }

		[JsonProperty("requestByMailActive")]
		public bool RequestByMailActive { get; set; }

		[JsonProperty("requestToSupplierActive")]
		public bool RequestToSupplierActive { get; set; }

		[JsonProperty("catalogDownloadUrl")]
		public string CatalogDownloadUrl { get; set; }
	}

}
