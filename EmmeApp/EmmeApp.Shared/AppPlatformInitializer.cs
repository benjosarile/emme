﻿using Acr.UserDialogs;
using EmmeApp.Helpers;
using EmmeApp.Managers;
using EmmeApp.Managers.Mappers;
using EmmeApp.Repositories;
using EmmeApp.Repositories.Database;
using EmmeApp.ViewModels;
using EmmeApp.Views;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using Plugin.Multilingual;
using Plugin.Multilingual.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Prism;
using Prism.Ioc;
using Prism.Logging;
using Xamarin.Forms;
using Plugin.DeviceInfo.Abstractions;
using Plugin.DeviceInfo;
using Plugin.Messaging;
using EmmeApp.Common.Utilities;
using Prism.Plugin.Popups;
using EmmeApp.CustomControls;
using EmmeApp.Common.Services;
using Plugin.BluetoothLE;
using EmmeApp.Services;
using EmmeApp.WebServices;
using Plugin.Media.Abstractions;
using Plugin.Media;
using EmmeApp.ProtocolServices.Abstractions;
using EmmeApp.ProtocolServices;
using EmmeApp.WebServices.Abstractions;
using EmmeApp.WebServices.Base;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Repositories.Abstractions;
using EmmeApp.Common.Utilities.Abstractions;
#if __ANDROID__
using EmmeApp.Droid.Database;
using EmmeApp.Droid.Services;
using EmmeApp.Droid.Utility;
#elif __IOS__
using EmmeApp.iOS.Database;
using EmmeApp.iOS.Services;
using EmmeApp.iOS.Utility;
#endif

namespace EmmeApp.Shared
{
    public class AppPlatformInitializer : IPlatformInitializer
	{
		public void RegisterTypes(IContainerRegistry containerRegistry)
		{
			RegisterPlugins(containerRegistry);
			RegisterHelper(containerRegistry);
			RegisterUtilites(containerRegistry);
			RegisterFactories(containerRegistry);
			RegisterRepositories(containerRegistry);
			RegisterServices(containerRegistry);
			RegisterManagers(containerRegistry);
			RegisterUI(containerRegistry);
		}


		private void RegisterUI(IContainerRegistry container)
		{
			container.RegisterForNavigation<NavigationPage>();
			container.RegisterForNavigation<TransitionNavigationPage>();
            container.RegisterForNavigation<LoadingPage, LoadingPageViewModel>();
            container.RegisterForNavigation<EmmeMasterDetailPage, EmmeMasterDetailPageViewModel>();
            container.RegisterForNavigation<HomePage, HomePageViewModel>();
            container.RegisterForNavigation<SearchPumpPage, SearchPumpPageViewModel>();
			container.RegisterForNavigation<PumpFittingProductsPage, PumpFittingProductsPageViewModel>();
            container.RegisterForNavigation<PumpReplacementDetailPage, PumpReplacementDetailPageViewModel>();
            container.RegisterForNavigation<InitialStartupForm1CustomerInfoPage, InitialStartupForm1CustomerInfoPageViewModel>();
            container.RegisterForNavigation<InitialStartupForm2SystemDataConfigurationPage, InitialStartupForm2SystemDataConfigurationPageViewModel>();
            container.RegisterForNavigation<InitialStartupForm3LevelProbeConfigurationPage, InitialStartupForm3LevelProbeConfigurationPageViewModel>();
            container.RegisterForNavigation<InitialStartupForm4PumpAndMotorConfigurationPage, InitialStartupForm4PumpAndMotorConfigurationPageViewModel>();
			container.RegisterForNavigation<InitialStartupForm5TimingConfigurationPage, InitialStartupForm5TimingConfigurationPageViewModel>();
			container.RegisterForNavigation<InitialStartupForm6MaintenanceAndRelayConfigurationPage, InitialStartupForm6MaintenanceAndRelayConfigurationPageViewModel>();

			container.RegisterForNavigation<CockpitControlUnitRootPage, CockpitControlUnitRootPageViewModel>();
			container.RegisterForNavigation<CockpitControlUnitLogPage, CockpitControlUnitLogPageViewModel>();

			container.RegisterForNavigation<CockpitPumpsModulARootPage, CockpitPumpsModulARootPageViewModel>();

            container.RegisterForNavigation<CockpitPumpsVariAERootPage, CockpitPumpsVariAERootPageViewModel>();

            container.RegisterForNavigation<ConnectToGatewayPage, ConnectToGatewayPageViewModel>();
            container.RegisterForNavigation<ConnectToGatewayListPage, ConnectToGatewayListPageViewModel>();
            container.RegisterForNavigation<GatewaySubPage, GatewaySubPageViewModel>();

            container.RegisterPopupNavigationService();
			container.RegisterForNavigation<DialogInformationOverlay>();
			container.RegisterForNavigation<DialogListOverlay>();

            container.RegisterForNavigation<PumpConfigurationModulAForm1Page, PumpConfigurationModulAForm1PageViewModel>();
            container.RegisterForNavigation<PumpConfigurationModulAForm2SystemDescriptionPage, PumpConfigurationModulAForm2SystemDescriptionPageViewModel>();
            container.RegisterForNavigation<PumpConfigurationVariAForm1Page, PumpConfigurationVariAForm1PageViewModel>();
            container.RegisterForNavigation<PumpConfigurationVariAForm2SystemDescriptionPage, PumpConfigurationVariAForm2SystemDescriptionPageViewModel>();

            container.RegisterForNavigation<PumpLogRootPage, PumpLogRootPageViewModel>();
            container.RegisterForNavigation<ControlUnitLogRootPage, ControlUnitLogRootPageViewModel>();

			container.RegisterForNavigation<AppSettingsPage, AppSettingsPageViewModel>();
			container.RegisterForNavigation<AboutPage, AboutPageViewModel>();
			container.RegisterForNavigation<ContactPage, ContactPageViewModel>();

			container.RegisterForNavigation<DocumentSearchPage, DocumentSearchPageViewModel>();
			container.RegisterForNavigation<DocumentSearchFilterPage, DocumentSearchFilterPageViewModel>();
			container.RegisterForNavigation<DocumentViewPage, DocumentViewPageViewModel>();
		}	

		private void RegisterFactories(IContainerRegistry container)
		{
			container.Register<ILoggerFacade, DebugLogger>();
		}

		private void RegisterManagers(IContainerRegistry container)
		{
			container.RegisterSingleton<IAssemblyResourceManager, AssemblyResourceManager>();
			container.RegisterSingleton<ISortimentManager, SortimentManager>();
			container.RegisterSingleton<IPumpReplacementManager, PumpReplacementManager>();
            container.RegisterSingleton<IMasterPumpReplacementManager, MasterPumpReplacementManager>();
			container.RegisterSingleton<IProductCategoryManager, ProductCategoryManager>();
			container.RegisterSingleton<IProductCommentManager, ProductCommentManager>();
			container.RegisterSingleton<IProductConnectorManager, ProductConnectorManager>();

			container.RegisterSingleton<IBiralProductManager, BiralProductManager>();
			container.RegisterSingleton<IPumpReplacementProductManager, PumpReplacementProductManager>();
			container.RegisterSingleton<IPumpReplacementUpdateManager, PumpReplacementUpdateManager>();

			container.RegisterSingleton<IBleManager, BleManager>();
			container.RegisterSingleton<IParameterDataCacheManager, ParameterDataCacheManager>();
			container.RegisterSingleton<ICockpitControlUnitManager, CockpitControlUnitManager>();
			container.RegisterSingleton<IConfigurationControlUnitManager, ConfigurationControlUnitManager>();
			container.RegisterSingleton<IBiralContactManager, BiralContactManager>();
			container.RegisterSingleton<IMarketManager, MarketManager>();
            container.RegisterSingleton<IConfigurationPumpModulAManager, ConfigurationPumpModulAManager>();
            container.RegisterSingleton<IConfigurationPumpVariAManager, ConfigurationPumpVariAManager>();
			container.RegisterSingleton<ILogControlUnitManager, LogControlUnitManager>();
			container.RegisterSingleton<ILogPumpManager, LogPumpManager>();
     
            container.RegisterSingleton<ICockpitPumpsModulAManager, CockpitPumpsModulAManager>();
            container.RegisterSingleton<ICockpitPumpsVariAEManager, CockpitPumpsVariAEManager>();
			container.RegisterSingleton<IDocumentsManager, DocumentsManager>();
			container.RegisterSingleton<IOcrManager, OcrManager>();

			container.RegisterSingleton<IBiralGtcManager, BiralGtcManager>();
        }

        private void RegisterRepositories(IContainerRegistry container)
		{
			container.RegisterSingleton<IMobileDatabase, MobileDatabase>();
			container.RegisterSingleton<ISortimentRepository, SortimentRepository>();
			container.RegisterSingleton<IMasterPumpReplacementRepository, MasterPumpReplacementRepository>();
			container.RegisterSingleton<IPumpReplacementRepository, PumpReplacementRepository>();
			container.RegisterSingleton<IPumpReplacementProductRepository, PumpReplacementProductRepository>();
			container.RegisterSingleton<IBiralProductRepository, BiralProductRepository>();
			container.RegisterSingleton<IProductCategoryRepository, ProductCategoryRepository>();
			container.RegisterSingleton<IProductCommentRepository, ProductCommentRepository>();
			container.RegisterSingleton<IProductConnectorRepository, ProductConnectorRepository>();
		}

		private void RegisterUtilites(IContainerRegistry container)
		{
			container.RegisterSingleton<ICrossMultilingualService, CrossMultilingualService>();
			container.RegisterSingleton<IServiceEntityMapper, ServiceEntityMapper>();
			container.RegisterSingleton<IFileService, FileService>();
			container.RegisterSingleton<ITaskRunner, AsynchronousTaskRunner>();
			container.RegisterSingleton<IAsyncDelayer, AsyncDelayer>();
			container.RegisterSingleton<INavigationHelperService, NavigationHelperService>();
            container.RegisterSingleton<IUnixTimestampService, UnixTimestampService>();
            container.RegisterSingleton<ICultureInfoService, CultureInfoService>();

#if __ANDROID__
			container.RegisterSingleton<ISQLiteConnectionFactory, AndroidSqlite>();
			container.RegisterSingleton<IAnalyticsService, AndroidAnalyticsService>();
            container.RegisterSingleton<IKeyboardHelper, AndroidKeyboardHelper>();
            container.RegisterSingleton<IExitService, AndroidExitService>();
			container.RegisterSingleton<ISettingsService, AndroidSettingsPreference>();
#elif __IOS__
            container.RegisterSingleton<ISQLiteConnectionFactory, IosSqlite>();
			container.RegisterSingleton<IAnalyticsService, IosAnalyticsService>();
            container.RegisterSingleton<IKeyboardHelper, IosKeyboardHelper>();
            container.RegisterSingleton<IExitService, IosExitService>();
			container.RegisterSingleton<ISettingsService, IosSettingsPreference>();
#endif
        }

        private void RegisterHelper(IContainerRegistry container)
		{
			container.RegisterSingleton<IEnumProvider, EnumProvider>();
		}

		private void RegisterServices(IContainerRegistry container)
		{
			container.RegisterSingleton<IAppCenterCustomService, AppCenterCustomService>();
			container.RegisterSingleton<IProtocolBufferService, ProtocolBufferService>();
			container.RegisterSingleton<IJsonService, JsonSerializerService>();
			container.RegisterSingleton<IBleAdapterService, BleAdapterService>();
			container.Register<IPermissionsService, PermissionsService>();

			container.Register<IHttpService, HttpService>();
			container.Register<IEmmeHttpClient, EmmeHttpClient>();
			container.Register<IWebServiceBase, WebServiceBase>();
			container.RegisterSingleton<ISortimentWebService, SortimentWebService>();
			container.RegisterSingleton<IPumpReplacementWebService, PumpReplacementWebService>();
			container.RegisterSingleton<IDocumentsWebService, DocumentsWebService>();
			container.RegisterSingleton<IOcrWebService, OcrWebService>();
		}

		private void RegisterPlugins(IContainerRegistry container)
		{
			container.RegisterInstance<IConnectivity>(CrossConnectivity.Current);
			container.RegisterInstance<IUserDialogs>(UserDialogs.Instance);
			container.RegisterInstance<IPermissions>(CrossPermissions.Current);
			container.RegisterInstance<IMultilingual>(CrossMultilingual.Current);
			container.RegisterInstance<IDeviceInfo>(CrossDeviceInfo.Current);
            container.RegisterInstance<IMessaging>(CrossMessaging.Current);
            container.RegisterInstance<IAdapter>(CrossBleAdapter.Current);
			container.RegisterInstance<IMedia>(CrossMedia.Current);

        }
    }
}
