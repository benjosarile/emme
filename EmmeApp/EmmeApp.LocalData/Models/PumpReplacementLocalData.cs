﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EmmeApp.LocalData.Models
{
    public class PumpReplacementRootLocalData
	{
		[JsonProperty("exchange")]
		public List<PumpReplacementLocalData> PumpReplacements { get; set; }

		[JsonProperty("products")]
		public List<BiralProductLocalData> Products { get; set; }

		[JsonProperty("categories")]
		public List<ProductCategoryLocalData> Categories { get; set; }

		[JsonProperty("comments")]
		public List<ProductCommentLocalData> Comments { get; set; }

		[JsonProperty("connectors")]
		public List<ProductConnectorLocalData> Connectors { get; set; }
	}

	public class PumpReplacementLocalData
	{
		[JsonProperty("ty")]
		public string Type { get; set; }

		[JsonProperty("di")]
		public string Diameter { get; set; }

		[JsonProperty("ln")]
		public string Length { get; set; }

		[JsonProperty("pr")]
		public string Pressure { get; set; }

		[JsonProperty("ng")]
		public string Engine { get; set; }

		[JsonProperty("p")]
		public List<PumpReplacementProductLocalData> PumpReplacementProducts { get; set; }
	}

	public class PumpReplacementProductLocalData
	{
		[JsonProperty("id")]
		public long ProductId { get; set; }

		[JsonProperty("ci")]
		public List<PumpReplacementConnectorInformationLocalData> ConnectorInformation { get; set; }

		[JsonProperty("cd")]
		public List<string> CommentCodes { get; set; }
	}

	public class PumpReplacementConnectorInformationLocalData
	{
		[JsonProperty("a")]
		public int NumberOfPieces { get; set; }

		[JsonProperty("cd")]
		public string ConnectorCode { get; set; }
	}

}
