﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EmmeApp.LocalData.Models
{
    public class ProductCategoryRootLocalData
	{
		[JsonProperty("categories")]
		public List<ProductCategoryLocalData> Categories { get; set; }
	}

	public class ProductCategoryLocalData
	{
		[JsonProperty("id")]
		public long CategoryId { get; set; }

		[JsonProperty("na")]
		public string Name { get; set; }

		[JsonProperty("l10n")]
		public LocalizationLocalData L10N { get; set; }
	}

	public class LocalizationLocalData
	{
		[JsonProperty("fr", NullValueHandling = NullValueHandling.Ignore)]
		public LocalizationDetailLocalData Fr { get; set; }

		[JsonProperty("it", NullValueHandling = NullValueHandling.Ignore)]
		public LocalizationDetailLocalData It { get; set; }

		[JsonProperty("nl", NullValueHandling = NullValueHandling.Ignore)]
		public LocalizationDetailLocalData Nl { get; set; }

		[JsonProperty("de", NullValueHandling = NullValueHandling.Ignore)]
		public LocalizationDetailLocalData De { get; set; }

		[JsonProperty("en", NullValueHandling = NullValueHandling.Ignore)]
		public LocalizationDetailLocalData En { get; set; }
	}

	public class LocalizationDetailLocalData
	{
		[JsonProperty("na")]
		public string Name { get; set; }

		[JsonProperty("tx")]
		public string Text { get; set; }

	}
}
