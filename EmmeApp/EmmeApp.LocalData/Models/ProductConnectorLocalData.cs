﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EmmeApp.LocalData.Models
{
    public class ProductConnectorRootLocalData
	{
		[JsonProperty("connectors")]
		public List<ProductConnectorLocalData> Connectors { get; set; }
	}

	public class ProductConnectorLocalData
	{
		[JsonProperty("cd")]
		public string ConnectorId { get; set; }

		[JsonProperty("tx")]
		public string Text { get; set; }

		[JsonProperty("p")]
		public uint ProductNumber { get; set; }

		[JsonProperty("l10n")]
		public LocalizationLocalData L10N { get; set; }

	}
}
