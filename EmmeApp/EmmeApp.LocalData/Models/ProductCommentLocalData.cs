﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EmmeApp.LocalData.Models
{
    public class ProductCommentRootLocalData
	{
		[JsonProperty("comments")]
		public List<ProductCommentLocalData> Comments { get; set; }
	}

	public class ProductCommentLocalData
	{
		[JsonProperty("cd")]
		public long CommentId { get; set; }

		[JsonProperty("tx")]
		public string Text { get; set; }

		[JsonProperty("l10n")]
		public LocalizationLocalData L10N { get; set; }
	}

}
