﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EmmeApp.LocalData.Models
{
    public class BiralContactRootLocalData
	{
		[JsonProperty("contacts")]
		public List<BiralContactLocalData> Contacts { get; set; }
	}

	public class BiralContactLocalData
	{
		[JsonProperty("country")]
		public string Country { get; set; }

		[JsonProperty("countryCode")]
		public string CountryCode { get; set; }

		[JsonProperty("languageCode")]
		public string LanguageCode { get; set; }

		[JsonProperty("market")]
		public string Market { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("address1")]
		public string Address1 { get; set; }

		[JsonProperty("address2")]
		public string Address2 { get; set; }

		[JsonProperty("contactNumber")]
		public string ContactNumber { get; set; }

		[JsonProperty("emailAddress")]
		public string EmailAddress { get; set; }

		[JsonProperty("website")]
		public string Website { get; set; }

	}
}
