﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EmmeApp.LocalData.Models
{
    public class BleServiceRootLocalData
	{
		[JsonProperty("services")]
		public List<BleServiceLocalData> BleServices { get; set; } 
	}
	public class BleServiceLocalData
	{
		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("uuid")]
		public string Uuid { get; set; }

		public List<BleServiceCharacteristicLocalData> Characteristics { get; set; }
	}

	public class BleServiceCharacteristicLocalData
	{
		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("uuid")]
		public string Uuid { get; set; }
		
		[JsonProperty("role")]
		public string Role { get; set; }
	}
}
