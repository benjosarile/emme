﻿using Newtonsoft.Json;

namespace EmmeApp.LocalData.Models
{
    public class BiralGtcLocalData
	{
		[JsonProperty("country")]
		public string Country { get; set; }

		[JsonProperty("countryCode")]
		public string CountryCode { get; set; }

		[JsonProperty("language")]
		public string Language { get; set; }

		[JsonProperty("link")]
		public string Link { get; set; }
	}
}
