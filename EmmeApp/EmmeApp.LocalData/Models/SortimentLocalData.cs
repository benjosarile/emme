﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace EmmeApp.LocalData.Models
{
    public class SortimentLocalData
	{
		[JsonProperty("version")]
		public int Version { get; set; }

		[JsonProperty("markets")]
		public List<SortimentMarketLocalData> Markets { get; set; }
	}

	public class SortimentMarketLocalData
	{
		[JsonProperty("key")]
		public string Key { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("l")]
		public MarketLinkLocalData MarketLink { get; set; }
	}

	public class MarketLinkLocalData
{
		[JsonProperty("de", NullValueHandling = NullValueHandling.Ignore	)]
		public Uri De { get; set; }

		[JsonProperty("fr", NullValueHandling = NullValueHandling.Ignore)]
		public Uri Fr { get; set; }

		[JsonProperty("it", NullValueHandling = NullValueHandling.Ignore)]
		public Uri It { get; set; }

		[JsonProperty("nl", NullValueHandling = NullValueHandling.Ignore)]
		public Uri Nl { get; set; }

		[JsonProperty("en", NullValueHandling = NullValueHandling.Ignore)]
		public Uri En { get; set; }
	}
}
