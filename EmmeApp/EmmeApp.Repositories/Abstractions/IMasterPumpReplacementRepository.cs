﻿using EmmeApp.DataObjects;

namespace EmmeApp.Repositories.Abstractions
{
    public interface IMasterPumpReplacementRepository : IRepository<MasterPumpReplacementDataObject>
	{
	}
}