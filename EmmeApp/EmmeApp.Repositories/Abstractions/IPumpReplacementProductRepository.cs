﻿using EmmeApp.DataObjects;

namespace EmmeApp.Repositories.Abstractions
{
    public interface IPumpReplacementProductRepository : IRepository<PumpReplacementProductDataObject>
	{
	}
}