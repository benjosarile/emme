﻿using EmmeApp.DataObjects;

namespace EmmeApp.Repositories.Abstractions
{
    public interface IProductCategoryRepository : IRepository<ProductCategoryDataObject>
	{
	}
}