﻿using SQLite;

namespace EmmeApp.Repositories.Abstractions
{
    public interface ISQLiteConnectionFactory
	{
		SQLiteConnection CreateConnection(string dbName);
	}
}
