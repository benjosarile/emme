﻿using EmmeApp.DataObjects;

namespace EmmeApp.Repositories.Abstractions
{
    public interface ISortimentRepository : IRepository<SortimentDataObject>
	{

	}
}