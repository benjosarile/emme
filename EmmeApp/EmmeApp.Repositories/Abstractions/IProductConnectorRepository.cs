﻿using EmmeApp.DataObjects;

namespace EmmeApp.Repositories.Abstractions
{
    public interface IProductConnectorRepository : IRepository<ProductConnectorDataObject>
	{
	}
}
