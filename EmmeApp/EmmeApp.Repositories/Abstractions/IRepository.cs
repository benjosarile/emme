﻿using EmmeApp.DataObjects.Base;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EmmeApp.Repositories.Abstractions
{
    public interface IRepository<T> where T : class, IDataObjectBase, new()
	{
		int Count();
		int Count(Expression<Func<T, bool>> predicate);

		void DeleteAll();
		int DeleteItem(int id);

		T FirstOrDefault();
		T FirstOrDefault(Expression<Func<T, bool>> predicate);

		IEnumerable<T> GetAll();
		IEnumerable<T> Where(Expression<Func<T, bool>> predicate);

		long Save(T item);
		bool SaveList(IEnumerable<T> list);
	}
}
