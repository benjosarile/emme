﻿using EmmeApp.DataObjects;

namespace EmmeApp.Repositories.Abstractions
{
    public interface IProductCommentRepository : IRepository<ProductCommentDataObject>
	{
	}
}