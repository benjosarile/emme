﻿using EmmeApp.DataObjects;

namespace EmmeApp.Repositories.Abstractions
{
    public interface IPumpReplacementRepository : IRepository<PumpReplacementDataObject>
	{

	}
}