﻿using EmmeApp.DataObjects;
using EmmeApp.Repositories.Abstractions;

namespace EmmeApp.Repositories
{
    public class ProductConnectorRepository : Repository<ProductConnectorDataObject>, IProductConnectorRepository
	{
		public ProductConnectorRepository(IMobileDatabase db) : base(db)
		{
		}
	}
}
