﻿using EmmeApp.DataObjects;
using EmmeApp.Repositories.Abstractions;

namespace EmmeApp.Repositories
{
    public class SortimentRepository : Repository<SortimentDataObject>, ISortimentRepository
	{
		public SortimentRepository(IMobileDatabase db) : base(db)
		{

		}
	}
}
