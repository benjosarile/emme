﻿using EmmeApp.DataObjects;
using EmmeApp.Repositories.Abstractions;

namespace EmmeApp.Repositories
{
    public class BiralProductRepository : Repository<BiralProductDataObject>, IBiralProductRepository
	{
		public BiralProductRepository(IMobileDatabase db) : base(db)
		{
		}
	}
}
