﻿using EmmeApp.DataObjects;
using EmmeApp.Repositories.Abstractions;

namespace EmmeApp.Repositories
{
    public class MasterPumpReplacementRepository : Repository<MasterPumpReplacementDataObject>, IMasterPumpReplacementRepository
	{
		public MasterPumpReplacementRepository(IMobileDatabase db) : base(db)
		{
		}
	}
}
