﻿using EmmeApp.DataObjects;
using EmmeApp.Repositories.Abstractions;

namespace EmmeApp.Repositories
{
    public class PumpReplacementProductRepository : Repository<PumpReplacementProductDataObject>, IPumpReplacementProductRepository
	{
		public PumpReplacementProductRepository(IMobileDatabase db) : base(db)
		{
		}
	}
}
