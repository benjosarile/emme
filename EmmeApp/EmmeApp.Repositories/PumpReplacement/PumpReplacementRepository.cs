﻿using EmmeApp.DataObjects;
using EmmeApp.Repositories.Abstractions;

namespace EmmeApp.Repositories
{
    public class PumpReplacementRepository : Repository<PumpReplacementDataObject>, IPumpReplacementRepository
	{
		public PumpReplacementRepository(IMobileDatabase db) : base(db)
		{
		}
	}
}
