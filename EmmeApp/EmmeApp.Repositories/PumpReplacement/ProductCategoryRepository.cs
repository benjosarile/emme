﻿using EmmeApp.DataObjects;
using EmmeApp.Repositories.Abstractions;

namespace EmmeApp.Repositories
{
    public class ProductCategoryRepository : Repository<ProductCategoryDataObject>, IProductCategoryRepository
	{
		public ProductCategoryRepository(IMobileDatabase db) : base(db)
		{
		}
	}
}
