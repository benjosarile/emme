﻿using EmmeApp.DataObjects;
using EmmeApp.Repositories.Abstractions;

namespace EmmeApp.Repositories
{
    public class ProductCommentRepository : Repository<ProductCommentDataObject>, IProductCommentRepository
	{
		public ProductCommentRepository(IMobileDatabase db) : base(db)
		{
		}
	}
}
