﻿using EmmeApp.DataObjects;
using EmmeApp.DataObjects.Base;
using EmmeApp.Repositories.Abstractions;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EmmeApp.Repositories.Database
{
    public class MobileDatabase : IMobileDatabase
	{
		private const string DBName = "EmmeDatabase.db3";
		private static readonly object _locker = new object();
		private readonly SQLiteConnection DB;

		public MobileDatabase(ISQLiteConnectionFactory connectionFactory)
		{
			lock (_locker)
			{
				DB = connectionFactory.CreateConnection(DBName);
				DB.CreateTable<MasterPumpReplacementDataObject>();

				DB.CreateTable<SortimentDataObject>();
				DB.CreateTable<PumpReplacementDataObject>();
				DB.CreateTable<PumpReplacementProductDataObject>();

				DB.CreateTable<BiralProductDataObject>();
				DB.CreateTable<ProductCategoryDataObject>();
				DB.CreateTable<ProductCommentDataObject>();
				DB.CreateTable<ProductConnectorDataObject>();
			}
		}

		public void BulkInsert<T>(IEnumerable<T> list) where T : class, IDataObjectBase, new()
		{
			lock (_locker)
			{
				DB.InsertAll(list, typeof(T));
			}
		}

		public int Count<T>() where T : class, IDataObjectBase, new()
		{
			lock (_locker)
			{
				return DB.Table<T>().Count();
			}
		}

		public int Count<T>(Expression<Func<T, bool>> expression) where T : class, IDataObjectBase, new()
		{
			lock (_locker)
			{
				return DB.Table<T>().Count(expression);
			}
		}

		public int DeleteAll<T>() where T : IDataObjectBase, new()
		{
			lock (_locker)
			{
				return DB.DeleteAll<T>();
			}
		}

		public int DeleteItem<T>(long id) where T : IDataObjectBase, new()
		{
			lock (_locker)
			{
				return DB.Delete<T>(id);
			}
		}

		public T FirstOrDefault<T>() where T : class, IDataObjectBase, new()
		{
			lock (_locker)
			{
				var item = DB.Table<T>().FirstOrDefault();

				return item;
			}
		}

		public T FirstOrDefault<T>(Expression<Func<T, bool>> expression) where T : class, IDataObjectBase, new()
		{
			lock (_locker)
			{
				var item = DB.Table<T>().FirstOrDefault(expression);

				return item;
			}
		}


		public T GetItem<T>(long id) where T : class, IDataObjectBase, new()
		{
			lock (_locker)
			{
				var item = DB.Table<T>().FirstOrDefault(x => x.Id == id);

				return item;
			}
		}

		public IEnumerable<T> GetAll<T>() where T : class, IDataObjectBase, new()
		{
			lock (_locker)
			{
				IEnumerable<T> list = DB.Table<T>().ToList();
				return list;
			}
		}

		public IEnumerable<T> Where<T>(Expression<Func<T, bool>> expression) where T : class, IDataObjectBase, new()
		{
			lock (_locker)
			{
				var item = DB.Table<T>().Where(expression).ToList();

				return item;
			}
		}

		public long InsertItem<T>(T item) where T : class, IDataObjectBase, new()
		{
			lock (_locker)
			{
				DB.Insert(item);
				var newID = DB.ExecuteScalar<long>("select last_insert_rowid();");
				return newID;
			}
		}

		public long SaveItem<T>(T item) where T : class, IDataObjectBase, new()
		{
			lock (_locker)
			{
				if (item.Id != 0)
				{
					DB.Update(item);
					return item.Id;
				}
				else
				{
					DB.Insert(item);
					var newID = DB.ExecuteScalar<long>("select last_insert_rowid();");
					return newID;
				}
			}
		}

		public void UpdateItem<T>(T item) where T : IDataObjectBase
		{
			lock (_locker)
			{
				DB.Update(item);
			}
		}

		public int UpdateItemScalar<T>(T item) where T : IDataObjectBase
		{
			lock (_locker)
			{
				return DB.Update(item);
			}
		}
	}
}
