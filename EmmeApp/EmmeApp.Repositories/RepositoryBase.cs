﻿using EmmeApp.Repositories.Abstractions;

namespace EmmeApp.Repositories
{
    public class RepositoryBase
	{
		protected IMobileDatabase DB { get; }

		public RepositoryBase(IMobileDatabase db)
		{
			this.DB = db;
		}
	}
}
