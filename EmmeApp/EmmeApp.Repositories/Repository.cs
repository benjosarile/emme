﻿using EmmeApp.DataObjects.Base;
using EmmeApp.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EmmeApp.Repositories
{
    public class Repository<T> : RepositoryBase, IRepository<T> where T : class, IDataObjectBase, new()
	{
		public Repository(IMobileDatabase db) : base(db)
		{
		}

		public int Count()
		{
			return DB.Count<T>();
		}

		public int Count(Expression<Func<T, bool>> predicate)
		{
			return DB.Count<T>(predicate);
		}

		public virtual void DeleteAll()
		{
			DB.DeleteAll<T>();
		}

		public virtual T FirstOrDefault()
		{
			var item = DB.FirstOrDefault<T>();
			return item;
		}

		public virtual T FirstOrDefault(Expression<Func<T, bool>> predicate)
		{
			var item = DB.FirstOrDefault<T>(predicate);
			return item;
		}

		public virtual IEnumerable<T> GetAll()
		{
			var list = DB.GetAll<T>();
			return list;
		}

		public virtual IEnumerable<T> Where(Expression<Func<T, bool>> predicate)
		{
			var list = DB.Where<T>(predicate);
			return list;
		}

		public virtual long Save(T item)
		{
			return DB.SaveItem(item);
		}


		public virtual bool SaveList(IEnumerable<T> list)
		{
			if (list == null) return false;

			DB.BulkInsert(list);

			return true;
		}

		public virtual int DeleteItem(int id)
		{
			return DB.DeleteItem<T>(id);
		}

		
	}
}
