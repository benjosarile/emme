﻿using EmmeApp.Repositories.Abstractions;
using SQLite;
using System;
using System.IO;

namespace EmmeApp.iOS.Database
{
    public class IosSqlite : ISQLiteConnectionFactory
	{
		public SQLiteConnection CreateConnection(string dbName)
		{
			string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			string libraryPath = Path.Combine(documentsPath, "..", "Library");
			var path = Path.Combine(libraryPath, dbName);

			return new SQLiteConnection(path);
		}
	}
}