﻿using CarouselView.FormsPlugin.iOS;
using EmmeApp.iOS.Services;
using EmmeApp.Providers;
using EmmeApp.Shared;
using FFImageLoading.Forms.Platform;
using Foundation;
using UIKit;
using Xamarin.Essentials;
using Xamarin.Forms;


namespace EmmeApp.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
		private App _app;

		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
			CachedImageRenderer.Init();

			DependencyService.Register<IAppTrackingTransparencyPermission, AppTrackingTransparencyPermission>();
			
            Rg.Plugins.Popup.Popup.Init();
            AiForms.Effects.iOS.Effects.Init();
            UXDivers.Effects.iOS.Assembly.Register();
			global::Xamarin.Forms.Forms.Init();
			CarouselViewRenderer.Init();

			InitializeContainer();
			LoadApplication(_app);

			Firebase.Core.App.Configure();

#if !DEBUG
            AppTrackingTransparency.ATTrackingManager.RequestTrackingAuthorization((result) =>
            {
                switch (result)
                {
                    case AppTrackingTransparency.ATTrackingManagerAuthorizationStatus.NotDetermined:
                        break;
                    case AppTrackingTransparency.ATTrackingManagerAuthorizationStatus.Restricted:
                        break;
                    case AppTrackingTransparency.ATTrackingManagerAuthorizationStatus.Denied:
                        break;
                    case AppTrackingTransparency.ATTrackingManagerAuthorizationStatus.Authorized:
                        break;
                    default:
                        break;
                }
            });

            var status = AppTrackingTransparency.ATTrackingManager.TrackingAuthorizationStatus;
#endif

            return base.FinishedLaunching(app, options);
        }

		public override async void OnActivated(UIApplication uiApplication)
		{
			base.OnActivated(uiApplication);
			
			if (UIDevice.CurrentDevice.CheckSystemVersion(15,0))
			{
				var appTrackingTransparencyPermission = DependencyService.Get<IAppTrackingTransparencyPermission>();
				var status = await appTrackingTransparencyPermission.CheckPermissionStatusAsync();

				if (status != PermissionStatus.Granted)                
					appTrackingTransparencyPermission.RequestPermissionAsync(HandleRequestedTransparencyPermission);
				else
					HandleRequestedTransparencyPermission(status);                                           
			}
		}
		
		private void HandleRequestedTransparencyPermission(PermissionStatus status)
		{
			if (status == PermissionStatus.Granted)
			{
				// Granted handler
			}
			else
			{
				// Not granted handler
			}
		}
		
		private void InitializeContainer()
		{
			AppPlatformInitializer initializer = new AppPlatformInitializer();
			_app = new App(initializer);
		}
		
		[Export("application:supportedInterfaceOrientationsForWindow:")]
		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations(UIApplication application, UIWindow forWindow)
		{
			return UIInterfaceOrientationMask.Portrait;
		}
	}
}
