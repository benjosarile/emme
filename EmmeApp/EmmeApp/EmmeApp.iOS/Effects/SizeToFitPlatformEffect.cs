﻿using EmmeApp.Common.Constants;
using EmmeApp.Effects;
using EmmeApp.iOS.Effects;
using Foundation;
using System;
using System.Drawing;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ResolutionGroupName(EffectsConstants.BaseNamespace)]
[assembly: ExportEffect(typeof(SizeToFitPlatformEffect), nameof(SizeToFit))]
namespace EmmeApp.iOS.Effects
{
    [Foundation.Preserve(AllMembers = true)]
	public class SizeToFitPlatformEffect : PlatformEffect
	{
		UILabel _view;
		nfloat _orgFontSize;

		protected override void OnAttached()
		{
			_view = Control as UILabel;
			_orgFontSize = _view.Font.PointSize;

			UpdateFitFont();
		}

		protected override void OnDetached()
		{
			try
			{
				_view.Font = UIFont.FromName(_view.Font.Name, _orgFontSize);
				var render = Platform.GetRenderer(Element as Label) as LabelRenderer;
				render?.LayoutSubviews();
				System.Diagnostics.Debug.WriteLine($"{this.GetType().FullName} Detached Disposing");
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine($"{this.GetType().FullName} Detached Disposing {ex.Message}");
			}

			_view = null;

			System.Diagnostics.Debug.WriteLine($"{this.GetType().FullName} Detached completely");
		}

		protected override void OnElementPropertyChanged(System.ComponentModel.PropertyChangedEventArgs args)
		{
			base.OnElementPropertyChanged(args);
			if (args.PropertyName == VisualElement.HeightProperty.PropertyName ||
				args.PropertyName == VisualElement.WidthProperty.PropertyName ||
				args.PropertyName == Label.TextProperty.PropertyName)
			{
				UpdateFitFont();
			}
			else if (args.PropertyName == Label.FontProperty.PropertyName)
			{
				_orgFontSize = _view.Font.PointSize;
				UpdateFitFont();
			}
		}

		void UpdateFitFont()
		{
			var formsView = Element as View;
			if (formsView.Width < 0 || formsView.Height < 0)
			{
				return;
			}

			var height = MeasureTextSize(_view.Text, formsView.Width, _orgFontSize, _view.Font.Name);

			var fontSize = _orgFontSize;

			if (height < formsView.Height)
			{
				while (height < formsView.Height)
				{
					fontSize += 0.5f;
					height = MeasureTextSize(_view.Text, formsView.Width, fontSize, _view.Font.Name);
				}
			}

			while (height > formsView.Height && fontSize > 0)
			{
				fontSize -= 0.5f;
				height = MeasureTextSize(_view.Text, formsView.Width, fontSize, _view.Font.Name);
			}

			_view.Font = UIFont.FromName(_view.Font.Name, fontSize);

			var render = Platform.GetRenderer(formsView) as LabelRenderer;
			render.LayoutSubviews();
		}

		public double MeasureTextSize(string text, double width, double fontSize, string fontName = null)
		{
			var nsText = new NSString(text);
			var boundSize = new SizeF((float)width, float.MaxValue);
			var options = NSStringDrawingOptions.UsesFontLeading | NSStringDrawingOptions.UsesLineFragmentOrigin;

			if (fontName == null)
			{
				fontName = "HelveticaNeue";
			}

			var attributes = new UIStringAttributes
			{
				Font = UIFont.FromName(fontName, (float)fontSize)
			};

			var sizeF = nsText.GetBoundingRect(boundSize, options, attributes, null).Size;

			return (double)sizeF.Height;// + 5;
		}
	}
}