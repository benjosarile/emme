using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AppTrackingTransparency;
using EmmeApp.Providers;
using Xamarin.Essentials;

namespace EmmeApp.iOS.Services
{
    public class AppTrackingTransparencyPermission : Permissions.BasePlatformPermission, IAppTrackingTransparencyPermission
    {
        protected override Func<IEnumerable<string>> RequiredInfoPlistKeys => () =>  new string[] { "NSUserTrackingUsageDescription" };

        /// <summary>
        /// Request tracking permission
        /// </summary>
        /// <param name="completion"></param>
        public void RequestPermissionAsync(Action<PermissionStatus> completion)
        {
            ATTrackingManager.RequestTrackingAuthorization((result) => completion(ConvertPermissionStatus(result)));
        }

        /// <summary>
        /// Check current tracking permission
        /// </summary>
        /// <returns></returns>
        public Task<PermissionStatus> CheckPermissionStatusAsync()
        {
            return Task.FromResult(ConvertPermissionStatus(ATTrackingManager.TrackingAuthorizationStatus));
        }

        /// <summary>
        /// This will convert permission status
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        private PermissionStatus ConvertPermissionStatus(ATTrackingManagerAuthorizationStatus status)
        {
            switch (status)
            {
                case ATTrackingManagerAuthorizationStatus.NotDetermined:
                    return PermissionStatus.Disabled;
                case ATTrackingManagerAuthorizationStatus.Restricted:
                    return PermissionStatus.Restricted;
                case ATTrackingManagerAuthorizationStatus.Denied:
                    return PermissionStatus.Denied;
                case ATTrackingManagerAuthorizationStatus.Authorized:
                    return PermissionStatus.Granted;
                default:
                    return PermissionStatus.Unknown;
            }
        }
    }
}