﻿using EmmeApp.Common.Utilities.Abstractions;
using Foundation;

namespace EmmeApp.iOS.Utility
{
    public class IosSettingsPreference : ISettingsService
	{
		public string GetValue(string key)
		{
			string value = NSUserDefaults.StandardUserDefaults.StringForKey(key);
			return value;
		}

		public bool SetValue(string key, string value)
		{
			bool result = false;
			NSUserDefaults.StandardUserDefaults.SetString(value, key);
			result = NSUserDefaults.StandardUserDefaults.Synchronize();
			return result;
		}

		public void Clear()
		{
			NSUserDefaults.StandardUserDefaults.RemovePersistentDomain(NSBundle.MainBundle.BundleIdentifier);
			NSUserDefaults.StandardUserDefaults.Synchronize();
		}
	}
}
