﻿using EmmeApp.CustomControls;
using EmmeApp.iOS.CustomRenderers;
using Foundation;
using System.ComponentModel;
using System.Net;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomWebView), typeof(CustomWebViewRenderer))]
namespace EmmeApp.iOS.CustomRenderers
{
	public class CustomWebViewRenderer : ViewRenderer<CustomWebView, UIWebView>
	{
		protected override void OnElementChanged(ElementChangedEventArgs<CustomWebView> e)
		{
			base.OnElementChanged(e);

			if (this.Control == null)
			{
				SetNativeControl(new UIWebView());

			}

			if (e.NewElement != null)
			{
				this.Control.Opaque = false;
				this.Control.BackgroundColor = UIColor.White;
				ShowContent(e.NewElement.Uri);
			}
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			if (e.PropertyName == "Uri")
			{
				ShowContent(this.Element.Uri, this.Element.PageNumber, this.Element.HighlightedText);
			}
		}

		public void ShowContent(string path, long pageNumber = 0, string highlightedText = "")
		{
			if (!string.IsNullOrEmpty(path))
			{
				string pageQueryString = pageNumber > 0 ? $"#page={pageNumber}" : string.Empty;
				string searchQueryString = !string.IsNullOrEmpty(highlightedText) ? $"search={highlightedText}" : string.Empty;
				searchQueryString = WebUtility.UrlEncode(searchQueryString);

				path = WebUtility.UrlEncode(path);

				string url = string.Format("https://mozilla.github.io/pdf.js/web/viewer.html?file={0}{1}&{2}", path, pageQueryString, searchQueryString);
				var nsURL = new NSUrl(url);
				var nsUrlRequest = new NSUrlRequest(nsURL);
				this.Control.LoadRequest(nsUrlRequest);
			}
		}
	}
}