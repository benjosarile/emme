﻿using EmmeApp.iOS.CustomRenderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ViewCell), typeof(CustomViewCellRenderer))]
namespace EmmeApp.iOS.CustomRenderers
{
    public class CustomViewCellRenderer : ViewCellRenderer
	{
		public override UITableViewCell GetCell(Xamarin.Forms.Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var cell = base.GetCell(item, reusableCell, tv);
			if (cell != null)
			{
				switch (item.StyleId)
				{
					case "RemoveSelectionStyle": cell.SelectionStyle = UIKit.UITableViewCellSelectionStyle.None; break;
					case "TransparentStyle":
						{
							cell.SelectionStyle = UIKit.UITableViewCellSelectionStyle.None;
							cell.BackgroundColor = UIColor.LightGray; 
						}
						break;

					case "DefaultSelectionStyle":
						{
							cell.SelectedBackgroundView = new UIView()
							{
								BackgroundColor = UIColor.FromRGBA(0x00, 0x00, 0x00, 0x1A)
							};
						}
						break;
					default: break;
				}
			}
			return cell;
		}
	}
}