﻿using System.ComponentModel;
using EmmeApp.CustomControls;
using EmmeApp.iOS.CustomRenderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]

namespace EmmeApp.iOS.CustomRenderers
{
	public class CustomEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var entry = e.NewElement;
                if(entry != null)
                {
                    Control.TintColor = entry.TextColor.ToUIColor();
                }
                Control.BorderStyle = UITextBorderStyle.None;
               
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if(e.PropertyName == nameof(CustomEntry.TextColor))
            {
                Control.TintColor = Element.TextColor.ToUIColor();
            }
        }
    }

}