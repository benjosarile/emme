﻿using EmmeApp.CustomControls;
using EmmeApp.iOS.CustomRenderers;
using System.ComponentModel;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEventButton), typeof(CustomEventButtonRenderer))]
namespace EmmeApp.iOS.CustomRenderers
{
    public class CustomEventButtonRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);

            if (Control != null && e.NewElement != null)
            {
                var customButton = e.NewElement as CustomEventButton;

                var button = Control;
                button.TitleLabel.LineBreakMode = UIKit.UILineBreakMode.WordWrap;
                button.TitleLabel.TextAlignment = UITextAlignment.Center;
                button.VerticalAlignment = UIControlContentVerticalAlignment.Center;
                button.SetTitleColor(Element.IsEnabled ? Element.TextColor.ToUIColor() : UIColor.White, Element.IsEnabled ? UIControlState.Normal : UIControlState.Disabled);
                button.TouchDown += delegate
                {
                    customButton.OnPressed();
                };
                button.TouchUpInside += delegate
                {
                    customButton.OnReleased();
                };
                button.TouchCancel += delegate
                {
                    customButton.OnReleased();
                };
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == nameof(Button.IsEnabled))
                Control.SetTitleColor(Element.IsEnabled ? Element.TextColor.ToUIColor() : UIColor.White, Element.IsEnabled ? UIControlState.Normal : UIControlState.Disabled);
        }
    }
}