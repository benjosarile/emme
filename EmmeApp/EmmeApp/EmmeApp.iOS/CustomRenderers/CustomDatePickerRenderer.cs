﻿using EmmeApp.iOS.CustomRenderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(DatePicker), typeof(CustomDatePickerRenderer))]
namespace EmmeApp.iOS.CustomRenderers
{
    public class CustomDatePickerRenderer : DatePickerRenderer
   {
      protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
      {
         base.OnElementChanged(e);
         if (Control != null)
         {
            Control.Layer.BorderWidth = 0;
            Control.BorderStyle = UITextBorderStyle.None;
         }
      }
   }
}