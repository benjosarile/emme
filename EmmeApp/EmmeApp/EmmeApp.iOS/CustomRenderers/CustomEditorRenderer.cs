﻿using EmmeApp.CustomControls;
using EmmeApp.iOS.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace EmmeApp.iOS.CustomRenderers
{
    public class CustomEditorRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var entry = e.NewElement;
                if (entry != null)
                {
                    Control.TintColor = entry.TextColor.ToUIColor();
                }
            }
        }
    }
}