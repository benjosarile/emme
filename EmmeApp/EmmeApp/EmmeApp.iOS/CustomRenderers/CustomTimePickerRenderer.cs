﻿using EmmeApp.iOS.CustomRenderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(TimePicker), typeof(CustomTimePickerRenderer))]
namespace EmmeApp.iOS.CustomRenderers
{
    public class CustomTimePickerRenderer : TimePickerRenderer
   {
      protected override void OnElementChanged(ElementChangedEventArgs<TimePicker> e)
      {
         base.OnElementChanged(e);
         if (Control != null)
         {
            Control.Layer.BorderWidth = 0;
            Control.BorderStyle = UITextBorderStyle.None;
         }
      }
   }
}