﻿using CoreAnimation;
using CoreGraphics;
using EmmeApp.CustomControls;
using EmmeApp.iOS.CustomRenderers;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(GradientColorStack), typeof(GradientColorStackRenderer))]
namespace EmmeApp.iOS.CustomRenderers
{
    public class GradientColorStackRenderer : VisualElementRenderer<StackLayout>
	{
		private CAGradientLayer previousLayer = null;

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (e.PropertyName == GradientColorStack.StartColorProperty.PropertyName)
			{
				Draw(NativeView.Layer.Frame);
			}
			else if (e.PropertyName == GradientColorStack.EndColorProperty.PropertyName)
			{
				Draw(NativeView.Layer.Frame);
			}
		}

		public override void Draw(CGRect rect)
		{
			base.Draw(rect);
			GradientColorStack stack = (GradientColorStack)this.Element;
			CGColor startColor = stack.StartColor.ToCGColor();
			CGColor endColor = stack.EndColor.ToCGColor();
			bool isHorizontalGradient = stack.IsHorizontalGradient;

			CAGradientLayer gradientLayer;

			if (isHorizontalGradient)
			{
				gradientLayer = new CAGradientLayer()
				{
					StartPoint = new CGPoint(0, 0.5),
					EndPoint = new CGPoint(1, 0.5)
				};
			}
			else
			{
				gradientLayer = new CAGradientLayer();
			}

			#region for Vertical Gradient  
			//var gradientLayer = new CAGradientLayer();     
			#endregion
			#region for Horizontal Gradient  
			//var gradientLayer = new CAGradientLayer()
			//{
			//	StartPoint = new CGPoint(0, 0.5),
			//	EndPoint = new CGPoint(1, 0.5)
			//};
			#endregion
			gradientLayer.Frame = rect;
			gradientLayer.Colors = new CGColor[] {
				startColor,
				endColor
			};

			if (previousLayer == null)
			{
				NativeView.Layer.InsertSublayer(gradientLayer, 0);
			}
			else
			{
				NativeView.Layer.ReplaceSublayer(previousLayer, gradientLayer);
			}

			previousLayer = gradientLayer;
		}
	}
}