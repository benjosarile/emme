﻿using EmmeApp.iOS.CustomRenderers;
using System.ComponentModel;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Button), typeof(CustomButtonRenderer))]
namespace EmmeApp.iOS.CustomRenderers
{
    public class CustomButtonRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            if (this.Control != null)
            {
                var element = e.NewElement;
                if (element != null)
                {
                    this.Control.TitleLabel.LineBreakMode = UIKit.UILineBreakMode.WordWrap;
                    this.Control.TitleLabel.TextAlignment = UITextAlignment.Center;
                    this.Control.VerticalAlignment = UIControlContentVerticalAlignment.Center;
                    this.Control.SetTitleColor(element.IsEnabled ? element?.TextColor.ToUIColor() : UIColor.White, element.IsEnabled ? UIControlState.Normal : UIControlState.Disabled);
                }
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == nameof(Button.IsEnabled))
            {
                this.Control.SetTitleColor(this.Element.IsEnabled ? this.Element.TextColor.ToUIColor() : UIColor.White, this.Element.IsEnabled ? UIControlState.Normal : UIControlState.Disabled);
            }
        }
    }
}