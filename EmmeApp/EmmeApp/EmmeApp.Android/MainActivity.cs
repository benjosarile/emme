﻿using Acr.UserDialogs;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using CarouselView.FormsPlugin.Android;
using EmmeApp.Shared;
using EmmeApp.Views;
using System.Linq;
using Xamarin.Forms;

namespace EmmeApp.Droid
{
    [Activity(Label = "Biral ONE",
        Icon = "@mipmap/ic_launcher",
        RoundIcon = "@mipmap/ic_launcher_round",
        Theme = "@style/MainTheme",
        MainLauncher = false,
        WindowSoftInputMode = Android.Views.SoftInput.StateVisible | Android.Views.SoftInput.AdjustResize,
        ConfigurationChanges = ConfigChanges.ScreenSize, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        private App _app;

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            FFImageLoading.Forms.Platform.CachedImageRenderer.Init(true);
            global::Xamarin.Forms.Forms.Init(this, bundle);
            CarouselViewRenderer.Init();
            AiForms.Effects.Droid.Effects.Init();
            Rg.Plugins.Popup.Popup.Init(this, bundle);
            Plugin.CurrentActivity.CrossCurrentActivity.Current.Init(this, bundle);
            UserDialogs.Init(this);

            InitializeContainer();
            LoadApplication(_app);
        }

        private void InitializeContainer()
        {
            AppPlatformInitializer initializer = new AppPlatformInitializer();
            _app = new App(initializer);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            Plugin.Permissions.PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public override void OnBackPressed()
        {
            var childViewCount = ((ViewGroup)(this).Window.DecorView).ChildCount;

            var modalStack = Xamarin.Forms.Application.Current.MainPage.Navigation.ModalStack;
            var navigationStack = Xamarin.Forms.Application.Current.MainPage.Navigation.NavigationStack;
            if (navigationStack.Count == 1 && navigationStack.FirstOrDefault() is MasterDetailPage)
            {
                var masterDetaiPage = (MasterDetailPage)navigationStack.FirstOrDefault();

                if (masterDetaiPage.Detail is NavigationPage && !((masterDetaiPage.Detail as NavigationPage).CurrentPage is HomePage))
                {
                    base.OnBackPressed();
                    return;
                }
                else if (!modalStack.Any())
                {
                    base.OnBackPressed();
                    Java.Lang.JavaSystem.Exit(0);
                }
            }
            
            if (navigationStack.Count > 1 ||
                modalStack.Count > 0 ||
                childViewCount > 2)
            {
                base.OnBackPressed();
                return;
            }


            if (!Rg.Plugins.Popup.Popup.SendBackPressed(base.OnBackPressed))
            {
                base.OnBackPressed();
                Java.Lang.JavaSystem.Exit(0);
            }
        }
    }
}

