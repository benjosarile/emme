﻿using Android.OS;
using EmmeApp.Common.Services;
using Firebase.Analytics;
using Plugin.CurrentActivity;
using System.Collections.Generic;

namespace EmmeApp.Droid.Services
{
	public class AndroidAnalyticsService : IAnalyticsService
	{

		public void LogEvent(string eventId)
		{
			LogEvent(eventId, null);
		}

		public void LogEvent(string eventId, string paramName, string value)
		{
			LogEvent(eventId, new Dictionary<string, string>
			{
				{paramName, value}
			});
		}

		public void LogEvent(string eventId, IDictionary<string, string> parameters)
		{
			var fireBaseAnalytics = FirebaseAnalytics.GetInstance(CrossCurrentActivity.Current.AppContext);

			if (parameters == null)
			{
				fireBaseAnalytics.LogEvent(eventId, null);
				return;
			}

			var bundle = new Bundle();

			foreach (var item in parameters)
			{
				bundle.PutString(FirebaseAnalytics.Param.ItemId, item.Key);
				bundle.PutString(FirebaseAnalytics.Param.ItemName, item.Value);
			}

			fireBaseAnalytics.LogEvent(eventId, bundle);
		}
	}
}