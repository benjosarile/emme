﻿using Android.App;
using Android.Content;
using EmmeApp.Common.Utilities.Abstractions;

namespace EmmeApp.Droid.Services
{
    public class AndroidSettingsPreference : ISettingsService
	{
		private readonly string _name = "Emme";

		public string GetValue(string key)
		{
			var setting = Application.Context.GetSharedPreferences(_name, FileCreationMode.Private);
			return setting.GetString(key, string.Empty);
		}

		public bool SetValue(string key, string value)
		{
			bool result = false;

			var setting = Application.Context.GetSharedPreferences(_name, FileCreationMode.Private);
			var holder = setting.Edit();

			holder.PutString(key, value);
			result = holder.Commit();

			return result;
		}

		public void Clear()
		{
			var setting = Application.Context.GetSharedPreferences(_name, FileCreationMode.Private);
			var holder = setting.Edit();
			holder.Clear();
			holder.Commit();
		}
	}
}
