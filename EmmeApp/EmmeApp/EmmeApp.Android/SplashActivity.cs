﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;

namespace EmmeApp.Droid
{
    [Activity(
	 Label = "Biral ONE",
	 MainLauncher = true,
	 NoHistory = true,
	 Theme = "@style/MainTheme.Splash",
	 ConfigurationChanges = ConfigChanges.ScreenSize, ScreenOrientation = ScreenOrientation.Portrait
 )]
	public class SplashActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			var intent = new Intent(this, typeof(MainActivity));
			this.StartActivity(intent);
		}
	}
}