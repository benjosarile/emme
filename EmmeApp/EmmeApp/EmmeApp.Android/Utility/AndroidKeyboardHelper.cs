﻿using Android.App;
using Android.Content;
using Android.Views.InputMethods;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Droid.Utility;
using Plugin.CurrentActivity;

[assembly: Xamarin.Forms.Dependency(typeof(AndroidKeyboardHelper))]
namespace EmmeApp.Droid.Utility
{
    public class AndroidKeyboardHelper : IKeyboardHelper
	{
		public void HideKeyboard()
		{
			var context = CrossCurrentActivity.Current.Activity;
			var inputMethodManager = context.GetSystemService(Context.InputMethodService) as InputMethodManager;
			if (inputMethodManager != null && context is Activity)
			{
				var activity = context as Activity;
				var token = activity.CurrentFocus?.WindowToken;
				inputMethodManager.HideSoftInputFromWindow(token, HideSoftInputFlags.None);

				activity.Window.DecorView.ClearFocus();
			}
		}
	}
}