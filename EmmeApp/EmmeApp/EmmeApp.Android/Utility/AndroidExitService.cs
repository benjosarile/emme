﻿using EmmeApp.Common.Utilities.Abstractions;
using Plugin.CurrentActivity;

namespace EmmeApp.Droid.Utility
{
    public class AndroidExitService : IExitService
    {
        public void CloseApp()
        {
            var context = CrossCurrentActivity.Current.Activity;
            context.Finish();
        }
    }
}