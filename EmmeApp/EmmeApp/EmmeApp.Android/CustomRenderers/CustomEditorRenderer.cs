﻿using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using EmmeApp.CustomControls;
using EmmeApp.Droid.CustomRenderers;
using EmmeApp.Droid.Utility;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace EmmeApp.Droid.CustomRenderers
{
	public class CustomEditorRenderer : EditorRenderer
    {
        public CustomEditorRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            Control?.SetBackgroundColor(Android.Graphics.Color.Transparent);

            if (Control != null && Element != null)
            {
                SetPointerColor();
            }

            if (e.OldElement == null)
            {
                SetTouchListener();
            }
        }

        private void SetTouchListener()
        {
            var nativeEditText = (global::Android.Widget.EditText)Control;

            nativeEditText.OverScrollMode = OverScrollMode.Always;
            nativeEditText.ScrollBarStyle = ScrollbarStyles.InsideInset;
            nativeEditText.SetOnTouchListener(new DroidTouchListener());

            Control.VerticalScrollBarEnabled = true;
            Control.ScrollBarStyle = Android.Views.ScrollbarStyles.InsideInset;
            Android.Content.Res.TypedArray a = Control.Context.Theme.ObtainStyledAttributes(new int[0]);
            InitializeScrollbars(a);
            a.Recycle();
        }

        private void SetPointerColor()
        {
            try
            {
                IntPtr IntPtrtextViewClass = JNIEnv.FindClass(typeof(EditText));
                IntPtr mCursorDrawableResProperty = JNIEnv.GetFieldID(IntPtrtextViewClass, "mCursorDrawableRes", "I");
                JNIEnv.SetField(Control.Handle, mCursorDrawableResProperty, 0);
            }
            catch (Exception)
            {
                Control.SetTextCursorDrawable(Resource.Drawable.textView_cursor);
                Control.SetCursorVisible(true);
            }
        }

    }
}