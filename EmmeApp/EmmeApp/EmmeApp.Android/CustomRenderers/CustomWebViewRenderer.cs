﻿using Android.Content;
using EmmeApp.CustomControls;
using EmmeApp.Droid.CustomRenderers;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomWebView), typeof(CustomWebViewRenderer))]
namespace EmmeApp.Droid.CustomRenderers
{
    public class CustomWebViewRenderer : WebViewRenderer
	{
		public CustomWebViewRenderer(Context context) : base(context)
		{
		}

		protected override void OnElementChanged(ElementChangedEventArgs<WebView> e)
		{
			base.OnElementChanged(e);


			if (e.NewElement == null)
			{
				return;
			}

			var webView = (CustomWebView)this.Element;

			if (this.Element == null)
			{
				return;
			}

			this.Control.SetBackgroundColor(Android.Graphics.Color.White);
			this.Control.Settings.AllowFileAccess = true;
			this.Control.Settings.AllowUniversalAccessFromFileURLs = true;

			ShowContent(webView.Uri, webView.PageNumber, webView.HighlightedText);
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			System.Diagnostics.Debug.WriteLine(e.PropertyName);

			if (e.PropertyName != CustomWebView.UriProperty.PropertyName)
			{
				return;
			}

			var webView = this.Element as CustomWebView;

			if (webView == null)
			{
				return;
			}

			ShowContent(webView.Uri, webView.PageNumber, webView.HighlightedText);
		}

		public void ShowContent(string path, long pageNumber = 0, string highlightedText = "")
		{
			if (!string.IsNullOrEmpty(path))
			{
				bool isValidUrl = path.StartsWith("http://") || path.StartsWith("https://");
				bool isPdfFile = path.Contains(".pdf");

				if (!isValidUrl && !isPdfFile)
				{
					this.Control.Settings.AllowUniversalAccessFromFileURLs = true;
					this.Control.LoadUrl(string.Format("file:///android_asset/{0}", path));
				}
				else if (isPdfFile)
				{
					this.Control.Settings.AllowFileAccess = true;
					this.Control.Settings.AllowUniversalAccessFromFileURLs = true;

					string pageQueryString = pageNumber > 0 ? $"#page={pageNumber}" : string.Empty;
					string searchQueryString = !string.IsNullOrEmpty(highlightedText) ? $"search={highlightedText}" : string.Empty;

					string url = string.Format("file:///android_asset/pdfjs/web/viewer.html?file={0}{1}&{2}", path, pageQueryString, searchQueryString);
					this.Control.LoadUrl(url);
				}
				else
				{
					this.Control.LoadUrl(path);
				}
			}
		}
	}
}