﻿using Android.Content;
using Android.Runtime;
using Android.Widget;
using EmmeApp.CustomControls;
using EmmeApp.Droid.CustomRenderers;
using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace EmmeApp.Droid.CustomRenderers
{
	public class CustomEntryRenderer : EntryRenderer
	{
		public CustomEntryRenderer(Context context) : base(context)
		{
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            Control?.SetBackgroundColor(Android.Graphics.Color.Transparent);

			if (Control != null && Element != null)
			{
				SetPointerColor();
			}

			UpdateInputType();
        }

        private void SetPointerColor()
        {
			try
			{
				IntPtr IntPtrtextViewClass = JNIEnv.FindClass(typeof(TextView));
				IntPtr mCursorDrawableResProperty = JNIEnv.GetFieldID(IntPtrtextViewClass, "mCursorDrawableRes", "I");
				JNIEnv.SetField(Control.Handle, mCursorDrawableResProperty, 0);
			}
			catch(Exception)
			{
				Control.SetTextCursorDrawable(Resource.Drawable.textView_cursor);
				Control.SetCursorVisible(true);
			}
		}

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (e.PropertyName == CustomEntry.KeyboardProperty.PropertyName)
				UpdateInputType();
		}

		void UpdateInputType()
		{
			var customEntry = (CustomEntry)this.Element;
			var keyboard = customEntry.Keyboard;
			var nativeEntryEditText = Control;

			if (keyboard == Keyboard.Telephone)
			{
				nativeEntryEditText.InputType = Android.Text.InputTypes.ClassNumber;
			}
		}
	}
}