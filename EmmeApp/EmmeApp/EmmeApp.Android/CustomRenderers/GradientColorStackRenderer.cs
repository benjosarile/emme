﻿using Android.Content;
using EmmeApp.CustomControls;
using EmmeApp.Droid.CustomRenderers;
using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(GradientColorStack), typeof(GradientColorStackRenderer))]
namespace EmmeApp.Droid.CustomRenderers
{
    public class GradientColorStackRenderer : VisualElementRenderer<StackLayout>
	{
		public GradientColorStackRenderer(Context context) : base(context)
		{
		}

		private Color StartColor
		{
			get;
			set;
		}
		private Color EndColor
		{
			get;
			set;
		}
		private bool IsHorizontalGradient
		{
			get;
			set;
		}
		protected override void DispatchDraw(global::Android.Graphics.Canvas canvas)
		{
			Android.Graphics.LinearGradient gradient;

			if (IsHorizontalGradient)
			{
				gradient = new Android.Graphics.LinearGradient(0, 0, Width, 0,
					this.StartColor.ToAndroid(),
					this.EndColor.ToAndroid(),
					Android.Graphics.Shader.TileMode.Mirror);
			}
			else
			{
				gradient = new Android.Graphics.LinearGradient(0, 0, 0, Height,
					this.StartColor.ToAndroid(),
					this.EndColor.ToAndroid(),
					Android.Graphics.Shader.TileMode.Mirror);
			}
			//#region for Vertical Gradient  
			////var gradient = new Android.Graphics.LinearGradient(0, 0, 0, Height,      
			//#endregion
			//#region  for Horizontal Gradient  
			//var gradient = new Android.Graphics.LinearGradient(0, 0, Width, 0,
			//#endregion
			//this.StartColor.ToAndroid(),
			//this.EndColor.ToAndroid(),
			//Android.Graphics.Shader.TileMode.Mirror);

			var paint = new Android.Graphics.Paint()
			{
				Dither = true,
			};
			paint.SetShader(gradient);
			canvas.DrawPaint(paint);
			base.DispatchDraw(canvas);
		}
		protected override void OnElementChanged(ElementChangedEventArgs<StackLayout> e)
		{
			base.OnElementChanged(e);
			if (e.OldElement != null || Element == null)
			{
				return;
			}
			try
			{
				var stack = e.NewElement as GradientColorStack;
				this.StartColor = stack.StartColor;
				this.EndColor = stack.EndColor;
				this.IsHorizontalGradient = stack.IsHorizontalGradient;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(@"ERROR:", ex.Message);
			}
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (e.PropertyName == GradientColorStack.StartColorProperty.PropertyName || e.PropertyName == GradientColorStack.EndColorProperty.PropertyName)
			{
				var stack = Element as GradientColorStack;
				this.StartColor = stack.StartColor;
				this.EndColor = stack.EndColor;
				this.IsHorizontalGradient = stack.IsHorizontalGradient;
			}

		}
	}
}