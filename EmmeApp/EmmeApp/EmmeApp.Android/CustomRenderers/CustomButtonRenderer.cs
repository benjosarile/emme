﻿using System.ComponentModel;
using Android.Content;
using Android.Content.Res;
using Android.Graphics.Drawables;
using EmmeApp.Droid.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Xamarin.Forms.Button), typeof(CustomButtonRenderer))]
namespace EmmeApp.Droid.CustomRenderers
{
    public class CustomButtonRenderer : ButtonRenderer
    {
        public CustomButtonRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);

            this.Control?.SetAllCaps(false);
            if (this.Control != null)
            {
                var button = this.Element as Xamarin.Forms.Button;

                var shape = new PaintDrawable(button.BackgroundColor.ToAndroid());

                shape.SetCornerRadius(button.CornerRadius * 4);

                var ripple = new RippleDrawable(ColorStateList.ValueOf(Android.Graphics.Color.DarkGray), shape, null);

                this.Control.SetBackground(ripple);

                this.Control.StateListAnimator = null;
                Control.SetTextColor(Element.IsEnabled ? Element.TextColor.ToAndroid() : Android.Graphics.Color.White);
            }

        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName == nameof(Button.IsEnabled))
                Control.SetTextColor(Element.IsEnabled ? Element.TextColor.ToAndroid() : Android.Graphics.Color.White);
        }

    }
}