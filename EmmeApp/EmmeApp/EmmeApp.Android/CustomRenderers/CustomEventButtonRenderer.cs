﻿using Android.Content;
using Android.Views;
using EmmeApp.CustomControls;
using EmmeApp.Droid.CustomRenderers;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEventButton), typeof(CustomEventButtonRenderer))]
namespace EmmeApp.Droid.CustomRenderers
{
    public class CustomEventButtonRenderer : ButtonRenderer
    {
        public CustomEventButtonRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control?.SetAllCaps(false);

                var customEventButton = e.NewElement as CustomEventButton;

                var button = Control as Android.Widget.Button;

				var tempBackgroundColor = customEventButton.BackgroundColor;

				button.Touch += (object sender, TouchEventArgs args) =>
                {
                    if (args.Event.Action == MotionEventActions.Down)
                    {
                        customEventButton.OnPressed();
                        customEventButton.BackgroundColor = manipulateColor(tempBackgroundColor.ToAndroid(), 0.80f);
                    }
                    else if (args.Event.Action == MotionEventActions.Up || args.Event.Action == MotionEventActions.Cancel)
                    {
                        customEventButton.OnReleased();
                        customEventButton.BackgroundColor = customEventButton.DefaultBackgroundColor;
                    }
                };
                Control.SetTextColor(Element.IsEnabled ? Element.TextColor.ToAndroid() : Android.Graphics.Color.White);
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName == nameof(Xamarin.Forms.Button.IsEnabled))
                Control.SetTextColor(Element.IsEnabled ? Element.TextColor.ToAndroid() : Android.Graphics.Color.White);
        }

        public System.Drawing.Color manipulateColor(Android.Graphics.Color color, float factor)
        {
            int a = Android.Graphics.Color.GetAlphaComponent(color);
            int r = Java.Lang.Math.Round(Android.Graphics.Color.GetRedComponent(color) * factor);
            int g = Java.Lang.Math.Round(Android.Graphics.Color.GetGreenComponent(color) * factor);
            int b = Java.Lang.Math.Round(Android.Graphics.Color.GetBlueComponent(color) * factor);
            return Android.Graphics.Color.Argb(a,
                    Java.Lang.Math.Min(r, 255),
                    Java.Lang.Math.Min(g, 255),
                    Java.Lang.Math.Min(b, 255)).ToColor();
        }
    }
}