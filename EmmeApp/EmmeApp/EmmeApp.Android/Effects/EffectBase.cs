﻿using System.Linq;
using Xamarin.Forms.Platform.Android;

namespace EmmeApp.Droid.Effects
{
    [Android.Runtime.Preserve(AllMembers = true)]
	public abstract class EffectBase : PlatformEffect
	{
		public static bool IsFastRenderers = global::Xamarin.Forms.Forms.Flags.Any(x => x == "FastRenderers_Experimental");

		protected bool IsFastRenderer
		{
			get
			{
				return IsFastRenderers && (Container == null && !(Element is Xamarin.Forms.Button));
			}
		}
	}
}