﻿using Prism.Events;

namespace EmmeApp.Events
{
    public class ConnectionEstablishedEvent : PubSubEvent
    {
    }
}
