﻿using EmmeApp.ItemModels;
using Prism.Events;

namespace EmmeApp.Events
{
    public class GatewayParametersEvent : PubSubEvent<NavigationItemModel>
    {
    }
}
