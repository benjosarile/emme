﻿using EmmeApp.ItemModels;
using Prism.Events;

namespace EmmeApp.Events
{
    public class CockpitControlUnitButtonSendEvent : PubSubEvent<CockpitControlUnitButtonSendModel>
    {
    }
}
