﻿using Prism.Events;

namespace EmmeApp.Events
{
    public class HamburgerTappedEvent : PubSubEvent
    {
    }

	public class HamburgerHideEvent : PubSubEvent
	{
	}
}
