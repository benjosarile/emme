﻿using Prism.Events;

namespace EmmeApp.Events
{
    public class ConnectionLostEvent : PubSubEvent
    {
    }
}
