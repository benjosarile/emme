﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace EmmeApp.Converters
{
    public class BooleanToUnderlineTextDecorationConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            bool boolVal = System.Convert.ToBoolean(value);

			return boolVal ? TextDecorations.Underline : TextDecorations.None;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}
