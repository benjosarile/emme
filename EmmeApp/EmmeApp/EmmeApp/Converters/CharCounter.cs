﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace EmmeApp.Converters
{
    public class CharCounter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string counter = value?.ToString();

            if (counter != null)
            {
                return counter.Length;
            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
