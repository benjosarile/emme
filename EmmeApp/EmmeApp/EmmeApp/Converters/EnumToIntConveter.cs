﻿using System;
using Xamarin.Forms;

namespace EmmeApp.Converters
{
    public class EnumToIntConveter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int retVal = 0;
            if ((Type)parameter != null)
            {
                retVal = (int)Enum.Parse((Type)parameter, value.ToString());
            }
            return retVal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Enum evalue = default(Enum);
            if ((Type)parameter != null)
            {
                evalue = (Enum)Enum.Parse((Type)parameter, value.ToString());
            }
            return evalue;
        }
    }
}
