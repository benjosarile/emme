﻿using Humanizer;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Crashes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using Xamarin.Forms;

namespace EmmeApp.Converters
{
    public class HumanizeConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				string strValue = value is Enum ? EnumHumanizeExtensions.Humanize((Enum)value) : string.Empty;
				return strValue;
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);

				var properties = new Dictionary<string, string> {
					{ "exception", ex.Message},
					{ "converter", nameof(HumanizeConverter) },
					{ "method", "Convert"},
				};
				Crashes.TrackError(new Exception(ex.Message), properties);
			}

			return string.Empty;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}
