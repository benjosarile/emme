﻿using System;
using Xamarin.Forms;

namespace EmmeApp.Converters
{
	public class EnumToBooleanConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			int retVal = 0;
			if ((Type)parameter != null)
			{
				retVal = (int)Enum.Parse((Type)parameter, value.ToString());
			}

			return retVal == 1;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			Enum evalue = default(Enum);
			if ((Type)parameter != null)
			{
				long retVal = 0;
				if(value is bool)
				{
					retVal = System.Convert.ToInt64(value);
				}

				evalue = (Enum)Enum.Parse((Type)parameter, retVal.ToString());
			}
			return evalue;
		}
	}
}
