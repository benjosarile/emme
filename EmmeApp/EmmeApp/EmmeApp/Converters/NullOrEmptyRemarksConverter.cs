﻿using EmmeApp.Localization.Resources;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace EmmeApp.Converters
{
    public class NullOrEmptyRemarksConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !string.IsNullOrEmpty(value.ToString()) ? value.ToString() : AppResources.Pump_Replacement_Detail_Page_No_Remarks_Here; 
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
