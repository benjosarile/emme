﻿using System;
using System.Diagnostics;
using System.Globalization;
using Xamarin.Forms;

namespace EmmeApp.Converters
{
    public class UnixTimestampToDateTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
				try
				{
					var date = DateTimeOffset.FromUnixTimeSeconds(System.Convert.ToInt64(value)).DateTime.ToLocalTime();
					if (parameter == null)
					{
						return date.ToString("HH:mm:ss dd.MM.yy");
					}

					return date.ToString(parameter.ToString());
				}
				catch(Exception ex)
				{
					Debug.WriteLine(ex.Message);
				}
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
