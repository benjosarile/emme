﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace EmmeApp.Converters
{
    public class EventArgsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var eventArgs = value as EventArgs;
            if (eventArgs == null)
            {
                throw new ArgumentException("Expected value to be of type EventArgs", nameof(value));
            }
            return eventArgs;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
