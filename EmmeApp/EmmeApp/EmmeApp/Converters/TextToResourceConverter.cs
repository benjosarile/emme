﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace EmmeApp.Converters
{
    public class TextToResourceConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string resourceKey = value.ToString();

			var resource = App.Current.Resources[resourceKey];
			return resource;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
