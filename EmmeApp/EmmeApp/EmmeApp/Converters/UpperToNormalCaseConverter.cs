﻿using EmmeApp.Common.Utilities.Abstractions;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace EmmeApp.Converters
{
    public class UpperToNormalCaseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var valStr = value?.ToString();
            return valStr.ToTitleCase();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return string.Empty;
        }
    }
}
