﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace EmmeApp.Converters
{
    public class BindableStackListLastItemBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter is CustomControls.BindableStackList)
            {
                var index = (parameter as CustomControls.BindableStackList).ItemsSource.IndexOf(value);
                var count = (parameter as CustomControls.BindableStackList).ItemsSource.Count;

                return index+1 == count;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
