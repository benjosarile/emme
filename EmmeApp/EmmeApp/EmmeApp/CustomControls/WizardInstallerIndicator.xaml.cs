﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EmmeApp.CustomControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WizardInstallerIndicator : ContentView
    {

        public static readonly BindableProperty SelectedIndexProperty =
        BindableProperty.Create(nameof(SelectedIndex),
        typeof(int),
        typeof(WizardInstallerIndicator),
        defaultValue: 0,
        propertyChanged: (bindable, oldVal, newVal) =>
        {
            ((WizardInstallerIndicator)bindable).UpdateSelectedIndex((int)newVal);
        }, defaultBindingMode: BindingMode.TwoWay);

        public static readonly BindableProperty MaxIndicatorProperty =
            BindableProperty.Create(nameof(MaxIndicator),
            typeof(int),
            typeof(WizardInstallerIndicator),
            defaultValue: 1,
            propertyChanged: (bindable, oldVal, newVal) =>
            {
                ((WizardInstallerIndicator)bindable).UpdateMaxIndicator();
            });


        public int MaxIndicator
        {
            get
            {
                return (int)GetValue(MaxIndicatorProperty);
            }
            set
            {
                SetValue(MaxIndicatorProperty, value);
            }
        }

        public int SelectedIndex
        {
            get
            {
                return (int)GetValue(SelectedIndexProperty);
            }
            set
            {
                SetValue(SelectedIndexProperty, value);
            }
        }

        public WizardInstallerIndicator()
        {
            InitializeComponent();
        }

        private void UpdateSelectedIndex(int newVal)
        {
			this.SelectedIndex = newVal;

            UpdateMaxIndicator();
        }

        private void UpdateMaxIndicator()
        {
			this.stackIndicator.Children.Clear();

            for (int i = 0; i < MaxIndicator; i++)
            {
                BoxView circle = new BoxView();
                circle.VerticalOptions = LayoutOptions.CenterAndExpand;
                circle.HorizontalOptions = LayoutOptions.CenterAndExpand;
                circle.WidthRequest = 5;
                circle.HeightRequest = 5;
                circle.CornerRadius = 2.5;
                if (i >= SelectedIndex)
                {
                    circle.BackgroundColor = Color.FromHex("#a5a5a5");
                }
                else
                {
                    circle.BackgroundColor = Color.FromHex("#0072ba");
                }
				this.stackIndicator.Children.Add(circle);
            }
        }
    }
}