﻿using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace EmmeApp.CustomControls
{
    public partial class WizardNumericUpDown : Grid
    {
        public static readonly BindableProperty MaximumProperty = BindableProperty.Create(nameof(Maximum), typeof(double), typeof(WizardNumericUpDown), 1.0, propertyChanging: OnMaximumPropertyChanging);

        private static void OnMaximumPropertyChanging(BindableObject bindable, object oldValue, object newValue)
        {
            double oldValueD = (double)oldValue;
            double newValueD = (double)newValue;

            var control = (bindable as WizardNumericUpDown);

            double currentValue = control.Value;
            double minValue = control.Minimum;
            double maxValue = control.Maximum;

            Debug.WriteLine($"MIN = {minValue} CUR = {currentValue} MAX = {maxValue} OLD = {oldValue} NEW = {newValue}");
        }

        public static readonly BindableProperty MinimumProperty = BindableProperty.Create(nameof(Minimum), typeof(double), typeof(WizardNumericUpDown), 0.0);
        public static readonly BindableProperty ValueProperty = BindableProperty.Create(nameof(Value), typeof(double), typeof(WizardNumericUpDown), 0.0, BindingMode.TwoWay);
        public static readonly BindableProperty StepValueProperty = BindableProperty.Create(nameof(StepValue), typeof(double), typeof(WizardNumericUpDown), 1.0);
        public static readonly BindableProperty TitleProperty = BindableProperty.Create(nameof(Title), typeof(string), typeof(WizardNumericUpDown), string.Empty);
        public static readonly BindableProperty SubtitleProperty = BindableProperty.Create(nameof(Subtitle), typeof(string), typeof(WizardNumericUpDown), string.Empty);
        public static readonly BindableProperty ThrottleProperty = BindableProperty.Create(nameof(Throttle), typeof(double), typeof(WizardNumericUpDown), 0.1);

        private DialogListOverlay _popupPage;
        public ICommand ShowDialogCommand { get; private set; }

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public string Subtitle
        {
            get { return (string)GetValue(SubtitleProperty); }
            set { SetValue(SubtitleProperty, value); }
        }

        public double Maximum
        {
            get { return (double)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        public double Minimum
        {
            get { return (double)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }

        public double StepValue
        {
            get { return (double)GetValue(StepValueProperty); }
            set { SetValue(StepValueProperty, value); }
        }

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public double Throttle
        {
            get { return (double)GetValue(ThrottleProperty); }
            set { SetValue(ThrottleProperty, value); }
        }

        public List<string> ItemsSource { get; set; }
        public string SelectedValue { get; set; }

        public WizardNumericUpDown()
        {
            InitializeComponent();


            _labelTitle.SetBinding(Label.TextProperty, new Binding(nameof(Title), source: this));
            _labelSubtitle.SetBinding(Label.TextProperty, new Binding(nameof(Subtitle), source: this));
            _labelValue.SetBinding(Label.TextProperty, new Binding(nameof(Value), source: this, stringFormat: "{0:N1}"));

            _btnMinus.Clicked += _btnMinus_Clicked;
            _btnPlus.Clicked += _btnPlus_Clicked;

            this.ShowDialogCommand = new Command(async () => await ExecuteShowDialogCommand());

            Observable.FromEventPattern<PropertyChangedEventArgs>(this, nameof(PropertyChanged))
               .Where(x => x.EventArgs.PropertyName == nameof(IsEnabled))
               .Throttle(TimeSpan.FromMilliseconds(5))
               .Select(_ => Observable.FromAsync(async () =>
               {
                   await Task.Delay(10);
                   SetStyleProperty();
               }))
               .Switch()
               .Subscribe();
        }

        private async Task ExecuteShowDialogCommand()
        {
            PopulateList();

            if (this.ItemsSource?.Count > 1)
            {
                _popupPage = new DialogListOverlay();
                _popupPage.ItemSelectedPropertyChanged += popupPage_ItemSelectedPropertyChanged;
                _popupPage.CloseDialogPropertyChanged += popupPage_CloseDialogPropertyChanged;
                _popupPage.ItemsSource = null;
                _popupPage.ItemsSource = this.ItemsSource;
                _popupPage.SelectedValue = this.SelectedValue;
                _popupPage.Title = this.Title;

                await PopupNavigation.Instance.PushAsync(_popupPage);
            }
        }

        private void popupPage_CloseDialogPropertyChanged()
        {
            if (_popupPage != null)
            {
                _popupPage.ItemSelectedPropertyChanged -= popupPage_ItemSelectedPropertyChanged;
                _popupPage.CloseDialogPropertyChanged -= popupPage_CloseDialogPropertyChanged;
                _popupPage = null;
            }
        }

        private void popupPage_ItemSelectedPropertyChanged(string selectedItem)
        {
            var val = System.Convert.ToDouble(selectedItem);
            if (IsEnabled && val >= Minimum && val <= Maximum)
            {
                Value = val;
            }
        }

        private void PopulateList()
        {
            ItemsSource = new List<string>();

            string strFormat = "N1";
            for (double val = Minimum; val < Maximum; val += StepValue)
            {
                ItemsSource.Add(val.ToString(strFormat));
            }

            SelectedValue = ItemsSource.FirstOrDefault(x => x == Value.ToString(strFormat));
        }
        private void SetStyleProperty()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (!IsEnabled)
                {
                    _labelTitle.Style = (Style)Application.Current.Resources["WizardControlTitleLabelDiabledStyle"];
                    _labelSubtitle.Style = (Style)Application.Current.Resources["WizardControlSubtitleLabelDisabledStyle"];
                    _btnMinus.Style = (Style)Application.Current.Resources["WizardNumericUpDownButtonDisabledStyle"];
                    _btnPlus.Style = (Style)Application.Current.Resources["WizardNumericUpDownButtonDisabledStyle"];
                    _labelValue.Style = (Style)Application.Current.Resources["WizardNumericUpDownLabelDisabledStyle"];

                }
                else
                {
                    _labelTitle.Style = (Style)Application.Current.Resources["WizardControlTitleLabelStyle"];
                    _labelSubtitle.Style = (Style)Application.Current.Resources["WizardControlSubtitleLabelStyle"];
                    _btnMinus.Style = (Style)Application.Current.Resources["WizardNumericUpDownButtonStyle"];
                    _btnPlus.Style = (Style)Application.Current.Resources["WizardNumericUpDownButtonStyle"];
                    _labelValue.Style = (Style)Application.Current.Resources["WizardNumericUpDownLabelStyle"];
                }
            });
        }

        private void _btnMinus_Clicked(object sender, EventArgs e)
        {
            var nextValue = Value - StepValue;
            if (nextValue >= Minimum)
            {
                Value = nextValue;
            }
        }

        private void _btnPlus_Clicked(object sender, EventArgs e)
        {
            var nextValue = Value + StepValue;
            if (nextValue <= Maximum)
            {
                Value = nextValue;
            }
        }

        private void gridNumeric_Tapped(object sender, EventArgs e)
        {
            if (IsEnabled)
            {
                this.ShowDialogCommand.Execute(null);
            }
        }

        private class DoubleToIntFloorConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                var realVal = System.Convert.ToDouble(value);
                return System.Convert.ToInt32(Math.Floor(realVal));
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return System.Convert.ToDouble(value);
            }
        }

        private class IntToDoubleConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return System.Convert.ToInt32(value);
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return System.Convert.ToDouble(value);
            }
        }


    }
}