﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace EmmeApp.CustomControls
{
	public partial class DialogAlertSingleContent : PopupPage
	{
		public static readonly BindableProperty ButtonTextProperty = BindableProperty.Create(nameof(ButtonText), typeof(string), typeof(DialogAlertSingleContent), string.Empty, BindingMode.TwoWay);
		public static readonly BindableProperty ButtonCommandProperty = BindableProperty.Create(nameof(ButtonCommand), typeof(ICommand), typeof(DialogAlertSingleContent), null, BindingMode.TwoWay);
		public static readonly BindableProperty ViewProperty = BindableProperty.Create(nameof(View), typeof(ContentView), typeof(DialogAlertSingleContent), null, BindingMode.TwoWay, propertyChanged: (bindableObject, oldValue, newValue) => { ((DialogAlertSingleContent)bindableObject).DrawView(); });

		private bool _canBackPopup = false;

		public string ButtonText
		{
			get { return (string)GetValue(ButtonTextProperty); }
			set { SetValue(ButtonTextProperty, value); }
		}

		public ICommand ButtonCommand
		{
			get { return (ICommand)GetValue(ButtonCommandProperty); }
			set { SetValue(ButtonCommandProperty, value); }
		}

		public ContentView View
		{
			get { return (ContentView)GetValue(ViewProperty); }
			set { SetValue(ViewProperty, value); }
		}

		public DialogAlertSingleContent()
		{
			InitializeComponent();
			this._title.SetBinding(Label.TextProperty, new Binding(nameof(Title), source: this));
			this._button.SetBinding(Button.TextProperty, new Binding(nameof(ButtonText), source: this));

			_canBackPopup = true;
		}

		protected override bool OnBackButtonPressed()
		{
			return true;
		}

		private async Task ExecuteBackCommand()
		{
			if (_canBackPopup)
			{
				_canBackPopup = false;
				await PopupNavigation.Instance.PopAsync();
			}
		}

		private void DrawView()
		{
			this._contentView.Content = View;
		}

		private async void _button_Clicked(object sender, EventArgs e)
		{
			await ExecuteBackCommand();
			this.ButtonCommand?.Execute(null);
		}

		public async Task NavigationBack()
		{
			await ExecuteBackCommand();
		}
	}
}