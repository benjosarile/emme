﻿using System.Diagnostics;
using Xamarin.Forms;
namespace EmmeApp.CustomControls
{
    public class ProgressRing : ProgressBar
    {

        public static readonly BindableProperty AnimatedProgressProperty = BindableProperty.Create("AnimatedProgress", typeof(double),
                                                                                                   typeof(ProgressRing), 0.0,
                                                                                                   propertyChanged: HandleAnimatedProgressChanged);
        public double AnimatedProgress
        {
            get { return (double)base.GetValue(AnimatedProgressProperty); }
            set
            {
                base.SetValue(AnimatedProgressProperty, value);

                StartProgressToAnimation();
            }
        }

        public static readonly BindableProperty AnimationLengthProperty = BindableProperty.Create("AnimationLength", typeof(uint),
                                                                                                  typeof(ProgressRing), (uint)800,
                                                                                                  propertyChanged: HandleAnimationLengthChanged);
        public uint AnimationLength
        {
            get { return (uint)base.GetValue(AnimationLengthProperty); }
            set { base.SetValue(AnimationLengthProperty, value); }
        }

        public static readonly BindableProperty AnimationEasingProperty = BindableProperty.Create("AnimationEasing", typeof(Easing),
                                                                                                  typeof(ProgressRing), Easing.Linear,
                                                                                                  propertyChanged: HandleAnimationEasingChanged);

        public Easing AnimationEasing
        {
            get { return (Easing)base.GetValue(AnimationEasingProperty); }
            set { base.SetValue(AnimationEasingProperty, value); }
        }

        public static readonly BindableProperty RingProgressColorProperty = BindableProperty.Create("RingProgressColor", typeof(Color),
                                                                                                    typeof(ProgressRing), Color.FromRgb(234, 105, 92));
        public Color RingProgressColor
        {
            get { return (Color)base.GetValue(RingProgressColorProperty); }
            set { base.SetValue(RingProgressColorProperty, value); }
        }

        public static readonly BindableProperty RingBaseColorProperty = BindableProperty.Create("RingBaseColor", typeof(Color),
                                                                                                typeof(ProgressRing), Color.FromRgb(46, 60, 76));
        public Color RingBaseColor
        {
            get { return (Color)base.GetValue(RingBaseColorProperty); }
            set { base.SetValue(RingBaseColorProperty, value); }
        }

        public static readonly BindableProperty RingThicknessProperty = BindableProperty.Create("RingThickness", typeof(double),
                                                                                                typeof(ProgressRing), 5.0);
        public double RingThickness
        {
            get { return (double)base.GetValue(RingThicknessProperty); }
            set { base.SetValue(RingThicknessProperty, value); }
        }

        public void StartProgressToAnimation()
        {
            ViewExtensions.CancelAnimations(this);
            ProgressTo(this.AnimatedProgress, this.AnimationLength, this.AnimationEasing);

        }


        static void HandleAnimatedProgressChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var progress = (ProgressRing)bindable;
            progress.AnimatedProgress = (double)newValue;

            Debug.WriteLine($"Animated Progress: {progress.AnimatedProgress}");
        }

        static void HandleAnimationLengthChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var progressRing = (ProgressRing)bindable;

            var animationIsRunning = progressRing.AnimationIsRunning("Progress");
            
            if (animationIsRunning)
            {
                progressRing.StartProgressToAnimation();
            }
        }

        static void HandleAnimationEasingChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var progressRing = (ProgressRing)bindable;
            var animationIsRunning = progressRing.AnimationIsRunning("Progress");

            if (animationIsRunning)
            {
                progressRing.StartProgressToAnimation();
            }
        }


    }
}
