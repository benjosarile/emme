﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reactive.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace EmmeApp.CustomControls
{
    public partial class WizardSlider : StackLayout
    {
      public static readonly BindableProperty MaximumProperty = BindableProperty.Create(nameof(Maximum), typeof(double), typeof(WizardSlider), 1.0, propertyChanging: OnMaximumPropertyChanging);
      private static void OnMaximumPropertyChanging(BindableObject bindable, object oldValue, object newValue)
      {
         double oldValueD = (double)oldValue;
         double newValueD = (double)newValue;

         var control = (bindable as WizardSlider);

         double currentValue = control.Value;
         double minValue = control.Minimum;
         double maxValue = control.Maximum;

         Debug.WriteLine($"MIN = {minValue} CUR = {currentValue} MAX = {maxValue} OLD = {oldValue} NEW = {newValue}");
      }
        public static readonly BindableProperty MinimumProperty = BindableProperty.Create(nameof(Minimum), typeof(double), typeof(WizardSlider), 0.0, propertyChanged: (bindableObject, oldValue, newValue) => { ((WizardSlider)bindableObject).OnMinChanged(); });
        public static readonly BindableProperty ValueProperty = BindableProperty.Create(nameof(Value), typeof(double), typeof(WizardSlider), 0.0, BindingMode.TwoWay);
        public static readonly BindableProperty StepValueProperty = BindableProperty.Create(nameof(StepValue), typeof(double), typeof(WizardSlider), 1.0);
        public static readonly BindableProperty TitleProperty = BindableProperty.Create(nameof(Title), typeof(string), typeof(WizardSlider), string.Empty);
        public static readonly BindableProperty SubtitleProperty = BindableProperty.Create(nameof(Subtitle), typeof(string), typeof(WizardSlider), string.Empty);
        public static readonly BindableProperty ThrottleProperty = BindableProperty.Create(nameof(Throttle), typeof(double), typeof(WizardSlider), 0.1);

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public string Subtitle
        {
            get { return (string)GetValue(SubtitleProperty); }
            set { SetValue(SubtitleProperty, value); }
        }

        public double Maximum
        {
            get { return (double)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        public double Minimum
        {
            get { return (double)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }

        public double StepValue
        {
            get { return (double)GetValue(StepValueProperty); }
            set { SetValue(StepValueProperty, value); }
        }

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public double Throttle
        {
            get { return (double)GetValue(ThrottleProperty); }
            set { SetValue(ThrottleProperty, value); }
        }

        public WizardSlider()
        {
            InitializeComponent();

            _labelTitle.SetBinding(Label.TextProperty, new Binding(nameof(Title), source: this));
            _labelSubtitle.SetBinding(Label.TextProperty, new Binding(nameof(Subtitle), source: this));
            _slider.SetBinding(Slider.MaximumProperty, new Binding(nameof(Maximum), source: this));
            _labelValue.SetBinding(Label.TextProperty, new Binding(nameof(Value), source: this));
            _labelMinimum.SetBinding(Label.TextProperty, new Binding(nameof(Minimum), source: this));
            _labelMaximum.SetBinding(Label.TextProperty, new Binding(nameof(Maximum), source: this));
            _slider.ValueChanged += _slider_ValueChanged;


            Observable.FromEventPattern<PropertyChangedEventArgs>(this, nameof(PropertyChanged))
               .Where(x => x.EventArgs.PropertyName == nameof(IsEnabled))
               .Throttle(TimeSpan.FromMilliseconds(5))
               .Select(_ => Observable.FromAsync(async () =>
               {
                   await Task.Delay(10);
                   SetStyleProperty();
               }))
               .Switch()
               .Subscribe();

            Observable.FromEventPattern<PropertyChangedEventArgs>(this, nameof(PropertyChanged))
               .Where(x => x.EventArgs.PropertyName == nameof(Value))
               .Throttle(TimeSpan.FromSeconds(Throttle))
               .Select(_ => Observable.FromAsync(async cts =>
               {
                   await TranslateValue();
               }))
               .Switch()
               .Subscribe()
               .Dispose();
        }

        private void SetStyleProperty()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (!IsEnabled)
                {
                    _labelTitle.Style = (Style)Application.Current.Resources["WizardControlTitleLabelDiabledStyle"];
                    _labelSubtitle.Style = (Style)Application.Current.Resources["WizardControlSubtitleLabelDisabledStyle"];
                    _labelMinimum.Style = (Style)Application.Current.Resources["SliderLabelValueDisabledStyle"];
                    _labelMaximum.Style = (Style)Application.Current.Resources["SliderLabelMaximumDisabledStyle"];
                    _labelValue.Style = (Style)Application.Current.Resources["SliderLabelValueDisabledStyle"];
                    _slider.Style = (Style)Application.Current.Resources["WizardSliderDisabledStyle"];

                }
                else
                {
                    _labelTitle.Style = (Style)Application.Current.Resources["WizardControlTitleLabelStyle"];
                    _labelSubtitle.Style = (Style)Application.Current.Resources["WizardControlSubtitleLabelStyle"];
                    _labelMinimum.Style = (Style)Application.Current.Resources["SliderLabelValueStyle"];
                    _labelMaximum.Style = (Style)Application.Current.Resources["SliderLabelMaximumStyle"];
                    _labelValue.Style = (Style)Application.Current.Resources["SliderLabelValueStyle"];
                    _slider.Style = (Style)Application.Current.Resources["WizardSliderStyle"];
                }
            });
        }

        bool _firstInstance = true;
        private void _slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            if(!_firstInstance)
            {
                var tempValue = Math.Round(e.NewValue / StepValue);

                Value = tempValue * StepValue;
            }
            _firstInstance = false;
        }

        private void OnMinChanged()
        {
            if(Minimum < Maximum)
            {
                this._slider.Minimum = Minimum;
            }
        }

        private async Task TranslateValue()
        {
            _slider.Value = Value;

            var maxRange = (Maximum - Minimum);
            var minValue = Value - Minimum;
            var negate = Device.RuntimePlatform == Device.iOS ? 33.33 : 40;
            var xvalue = minValue * ((this._slider.Width - negate) / maxRange);

            SetMinMaxLabelVisibility(maxRange);
            xvalue = RealignCurrentLabelValue(xvalue);

            await _labelValue.TranslateTo(xvalue, 0, 10);
        }
        
        private double RealignCurrentLabelValue(double xvalue)
        {
            var count = Value.ToString().Length;

            // Adjust _labelValue translate value (xvalue) when has more than 3 chars (Keep center)
            xvalue -= count >= 3 ? 7 : 0;

            // Adjust _labelValue translate value (xvalue) when reach the maximum slider value (Set limit)
            xvalue = _labelMaximum.X > xvalue ? xvalue : _labelMaximum.X - 5;

            // Adjust _labelValue translate value (xvalue) when has more than 5 chars (Keep center)
            var fiveDigitNegation = Device.RuntimePlatform == Device.iOS ? 3 : 8;
            xvalue -= count >= 5 ? fiveDigitNegation : 0;
            return xvalue;
        }

        private void SetMinMaxLabelVisibility(double maxRange)
        {
            /// Set visibility of minimum and maximum value (max: 10% of max value; min 5% of max value)
            var visibilityRange = (maxRange * 0.1);
            _labelMinimum.IsVisible = Value > Minimum + (visibilityRange * 0.5);
            _labelMaximum.IsVisible = Value < Maximum - visibilityRange;
        }

        protected override async void LayoutChildren(double x, double y, double width, double height)
        {
            base.LayoutChildren(x, y, width, height);
            await TranslateValue();
        }
    }
}