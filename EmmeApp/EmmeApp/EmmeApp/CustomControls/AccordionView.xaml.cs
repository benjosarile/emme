﻿using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EmmeApp.CustomControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AccordionView : ContentView
    {
        private double _height { get; set; }
        private double _width { get; set; }

        #region Bindable Properties
        public static readonly BindableProperty HeaderBackgroundColorProperty =
            BindableProperty.Create(nameof(HeaderBackgroundColor),
                typeof(Color),
                typeof(AccordionView),
                defaultValue: Color.Transparent,
                propertyChanged: (bindable, oldVal, newVal) =>
                {
                    ((AccordionView)bindable).UpdateHeaderBackgroundColor();
                });

        public static readonly BindableProperty HeaderOpenedBackgroundColorProperty =
            BindableProperty.Create(nameof(HeaderOpenedBackgroundColor),
                typeof(Color),
                typeof(AccordionView),
                defaultValue: Color.Transparent,
                propertyChanged: (bindable, oldVal, newVal) =>
                {
                    ((AccordionView)bindable).UpdateHeaderBackgroundColor();
                });

        public static readonly BindableProperty HeaderTextColorProperty =
            BindableProperty.Create(nameof(HeaderTextColor),
                typeof(Color),
                typeof(AccordionView),
                defaultValue: Color.Black,
                propertyChanged: (bindable, oldVal, newVal) =>
                {
                    ((AccordionView)bindable).UpdateHeaderTextColor((Color)newVal);
                });

        public static readonly BindableProperty HeaderTextProperty =
            BindableProperty.Create(nameof(HeaderTextProperty),
                typeof(string),
                typeof(AccordionView),
                propertyChanged: (bindable, oldVal, newVal) =>
                {
                    ((AccordionView)bindable).UpdateHeaderText((string)newVal);
                });


        public static readonly BindableProperty IsBodyVisibleProperty =
            BindableProperty.Create(nameof(IsBodyVisible),
                typeof(bool),
                typeof(AccordionView),
                defaultValue: true,
                propertyChanged: async (bindable, oldVal, newVal)  =>
                {
                    await ((AccordionView)bindable).UpdateBodyVisibility((bool)newVal);
                });


        #endregion

        #region Public Properties
        public Color HeaderBackgroundColor
        {
            get
            {
                return (Color)GetValue(HeaderBackgroundColorProperty);
            }
            set
            {
                SetValue(HeaderBackgroundColorProperty, value);
            }
        }
        public Color HeaderOpenedBackgroundColor
        {
            get
            {
                return (Color)GetValue(HeaderOpenedBackgroundColorProperty);
            }
            set
            {
                SetValue(HeaderOpenedBackgroundColorProperty, value);
            }
        }
        public Color HeaderTextColor
        {
            get
            {
                return (Color)GetValue(HeaderTextColorProperty);
            }
            set
            {
                SetValue(HeaderTextColorProperty, value);
            }
        }
        public string HeaderText
        {
            get
            {
                return (string)GetValue(HeaderTextProperty);
            }
            set
            {
                SetValue(HeaderTextProperty, value);
            }
        }


        public bool IsBodyVisible
        {
            get
            {
                return (bool)GetValue(IsBodyVisibleProperty);
            }
            set
            {
                SetValue(IsBodyVisibleProperty, value);
            }
        }

        public View AccordionContent
        {
            get { return BodyView.Content; }
            set { BodyView.Content = value; }
        }

        #endregion


        public AccordionView()
        {
            InitializeComponent();
            IsBodyVisible = false;
        }

        public void UpdateHeaderBackgroundColor(Color color)
        {
            HeaderView.BackgroundColor = color;
        }

        public void UpdateHeaderBackgroundColor()
        {
            if (IsBodyVisible)
            {
                HeaderView.BackgroundColor = HeaderOpenedBackgroundColor;
                BodyView.BackgroundColor = HeaderOpenedBackgroundColor;

            }
            else
            {
                HeaderView.BackgroundColor = HeaderBackgroundColor;
            }
        }

        public void UpdateHeaderTextColor(Color color)
        {
            HeaderLabel.TextColor = color;
        }

        public void UpdateHeaderText(string text)
        {
            HeaderLabel.Text = text;
        }


        public async Task UpdateBodyVisibility(bool isVisible)
        {
            BodyView.IsVisible = isVisible;
            IndicatorImage.Source = "ic_expand.png";
            if (isVisible)
            {
                await IndicatorImage.RotateTo(-180, 200, Easing.Linear);
            }
            else
            {
                await IndicatorImage.RotateTo(0, 200, Easing.Linear);
            }
        }

        private void Handle_Tapped(object sender, System.EventArgs e)
        {
            IsBodyVisible = !IsBodyVisible;
            UpdateHeaderBackgroundColor();
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            if (width > 0 && height > 0)
            {
                _width = width;
                _height = height;
            }
        }
    }
}