﻿using Rg.Plugins.Popup.Services;
using System;
using Xamarin.Forms;

namespace EmmeApp.CustomControls
{
    public class WizardHelpPopupButton : ImageButton
	{
		public static readonly BindableProperty ViewResourceProperty = BindableProperty.Create(nameof(View), typeof(string), typeof(WizardHelpPopupButton), string.Empty, BindingMode.TwoWay);
		public static readonly BindableProperty TitleProperty = BindableProperty.Create(nameof(Title), typeof(string), typeof(WizardHelpPopupButton), string.Empty, BindingMode.TwoWay);

		private DialogInformationOverlay _popupPage;

		public string ViewResource
		{
			get { return (string)GetValue(ViewResourceProperty); }
			set { SetValue(ViewResourceProperty, value); }
		}

		public string Title
		{
			get { return (string)GetValue(TitleProperty); }
			set { SetValue(TitleProperty, value); }
		}


		public WizardHelpPopupButton()
		{
			this.Clicked += WizardHelpPopupButton_Clicked;
		}

		private async void WizardHelpPopupButton_Clicked(object sender, EventArgs e)
		{
			if(!string.IsNullOrEmpty(ViewResource))
			{
				this._popupPage = new DialogInformationOverlay();
				this._popupPage.Title = Title;

				var resource = App.Current.Resources[ViewResource];
				this._popupPage.View = (ContentView)resource;

				await PopupNavigation.Instance.PushAsync(_popupPage);
			}
		}
	}
}
