﻿using System;
using Xamarin.Forms;

namespace EmmeApp.CustomControls
{
    public class CustomEventButton : Button
	{
		public static readonly BindableProperty DefaultBackgroundColorProperty = BindableProperty.Create(nameof(DefaultBackgroundColor), typeof(Color), typeof(CustomEventButton), (Color)App.Current.Resources["Cerulean"], defaultBindingMode: BindingMode.TwoWay);

		public Color DefaultBackgroundColor
		{
			get { return (Color)GetValue(DefaultBackgroundColorProperty); }
			set { SetValue(DefaultBackgroundColorProperty, value); }
		}


		public event EventHandler Pressed;
		public event EventHandler Released;

		public CustomEventButton()
		{
			this.CornerRadius = 0;
		}

		public virtual void OnPressed()
		{
			Pressed?.Invoke(this, EventArgs.Empty);
		}

		public virtual void OnReleased()
		{
			Released?.Invoke(this, EventArgs.Empty);
		}
	}
}
