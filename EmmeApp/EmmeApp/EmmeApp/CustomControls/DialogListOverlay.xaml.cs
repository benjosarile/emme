﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace EmmeApp.CustomControls
{
	public partial class DialogListOverlay : PopupPage
    {
        public delegate void ItemSelectedEventHandler(string selectedItem);
        public event ItemSelectedEventHandler ItemSelectedPropertyChanged;

        public delegate void CloseDialogEventHandler();
        public event CloseDialogEventHandler CloseDialogPropertyChanged;

        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(nameof(ItemsSource), typeof(IList), typeof(DialogListOverlay), null, propertyChanged: (bindableObject, oldValue, newValue) => { ((DialogListOverlay)bindableObject).OnItemSourceChanged(); });
        public static readonly BindableProperty SelectedValueProperty = BindableProperty.Create(nameof(SelectedValue), typeof(string), typeof(DialogListOverlay), string.Empty, BindingMode.TwoWay, propertyChanged: (bindableObject, oldValue, newValue) => { ((DialogListOverlay)bindableObject).OnSelectedValueChanged(); });

		private bool _canBackPopup = false;

		public IList ItemsSource
        {
            get { return (IList)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }
        public string SelectedValue
        {
            get { return (string)GetValue(SelectedValueProperty); }
            set { SetValue(SelectedValueProperty, value); }
        }

        public DialogListOverlay ()
		{
			InitializeComponent ();

            this._lvwSelector.SetBinding(ListView.SelectedItemProperty, new Binding(nameof(SelectedValue), BindingMode.TwoWay, source: this));
            this._title.SetBinding(Label.TextProperty, new Binding(nameof(Title), source: this));

            _canBackPopup = true;
		}

        private void OnItemSourceChanged()
        {
            this._lvwSelector.ItemsSource = ItemsSource;
        }

        private void OnSelectedValueChanged()
        {
            this._lvwSelector.ScrollTo(SelectedValue, ScrollToPosition.Center, false);
        }

        public async void button1_Clicked(object sender, EventArgs e)
        {
            await ExecuteBackCommand();
        }

        private async Task ExecuteBackCommand()
        {
            CloseDialogPropertyChanged?.Invoke();

            if (_canBackPopup)
			{
				_canBackPopup = false;
				await PopupNavigation.Instance.PopAsync();
			}
		}

		public async void button2_Clicked(object sender, EventArgs e)
        {
            ItemSelectedPropertyChanged?.Invoke(SelectedValue);

            await ExecuteBackCommand();
        }

        
    }
}