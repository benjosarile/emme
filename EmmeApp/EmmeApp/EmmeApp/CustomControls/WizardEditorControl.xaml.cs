﻿using System;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

namespace EmmeApp.CustomControls
{
    public partial class WizardEditorControl : StackLayout
	{
        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(WizardEditorControl), string.Empty, BindingMode.TwoWay);
        public static readonly BindableProperty TextPlaceholderProperty = BindableProperty.Create(nameof(TextPlaceHolder), typeof(string), typeof(WizardEditorControl), string.Empty);
        public static readonly BindableProperty IsFocusProperty = BindableProperty.Create(nameof(IsFocus), typeof(bool), typeof(WizardEditorControl), false, BindingMode.TwoWay);
        public static readonly BindableProperty IsErrorProperty = BindableProperty.Create(nameof(IsError), typeof(bool), typeof(WizardEditorControl), false, BindingMode.TwoWay);
        public static readonly BindableProperty KeyboardProperty = BindableProperty.Create(nameof(Keyboard), typeof(Keyboard), typeof(WizardEditorControl), Keyboard.Default, BindingMode.TwoWay);

        public static readonly BindableProperty MaximumTextLengthProperty = BindableProperty.Create(nameof(MaximumTextLength), typeof(int), typeof(WizardEditorControl), 32, BindingMode.TwoWay);
        public static readonly BindableProperty ControlHeightProperty = BindableProperty.Create(nameof(ControlHeight), typeof(double), typeof(WizardEditorControl), 90.0);


        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public bool IsFocus
        {
            get { return (bool)GetValue(IsFocusProperty); }
            set { SetValue(IsFocusProperty, value); }
        }

        public string TextPlaceHolder
        {
            get { return (string)GetValue(TextPlaceholderProperty); }
            set { SetValue(TextPlaceholderProperty, value); }
        }

        public bool IsError
        {
            get { return (bool)GetValue(IsErrorProperty); }
            set { SetValue(IsErrorProperty, value); }
        }

        public Keyboard Keyboard
        {
            get { return (Keyboard)GetValue(KeyboardProperty); }
            set { SetValue(KeyboardProperty, value); }
        }

        public int MaximumTextLength
        {
            get { return Convert.ToInt32(GetValue(MaximumTextLengthProperty)); }
            set { SetValue(MaximumTextLengthProperty, value); }
        }

        public double ControlHeight
        {
            get { return (double)GetValue(ControlHeightProperty); }
            set { SetValue(ControlHeightProperty, value); }
        }

        public WizardEditorControl ()
		{
			InitializeComponent ();
            this._textHolder.SetBinding(CustomEditor.TextProperty, new Binding(nameof(Text), source: this));
            this._textHolder.SetBinding(CustomEditor.PlaceholderProperty, new Binding(nameof(TextPlaceHolder), source: this));
            this._textHolder.SetBinding(CustomEditor.IsFocusedProperty, new Binding(nameof(IsFocus), source: this));
            this._textHolder.SetBinding(CustomEditor.KeyboardProperty, new Binding(nameof(Keyboard), source: this));
            this._textHolder.SetBinding(CustomEditor.HeightRequestProperty, new Binding(nameof(ControlHeight), source: this));
            App.Current.On<Android>().UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Resize);
            
        }

        private void _textHolder_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(e.NewTextValue))
            {
                if (Keyboard == Keyboard.Telephone)
                {
                    bool isValid = e.NewTextValue.ToCharArray().All(x => char.IsDigit(x));
                    ((CustomEditor)sender).Text = isValid ? e.NewTextValue : e.NewTextValue.Remove(e.NewTextValue.Length - 1);
                }
            }

            string textValue = ((CustomEditor)sender).Text;
            textValue = textValue ?? "";

            UpdateError(textValue);
        }

        private void UpdateError(string str)
        {
            int strLength = str.Length;

            this.IsError = strLength > MaximumTextLength;
        }
    }
}