﻿
using Xamarin.Forms;

namespace EmmeApp.CustomControls
{
    public partial class CockpitStatusItem : Grid
	{
        public static readonly BindableProperty ParameterNameProperty = BindableProperty.Create(nameof(ParameterName), typeof(string), typeof(CockpitStatusItem), string.Empty);
        public static readonly BindableProperty ParameterValueProperty = BindableProperty.Create(nameof(ParameterValue), typeof(string), typeof(CockpitStatusItem), string.Empty, propertyChanged: (bindableObject, oldValue, newValue) => { ((CockpitStatusItem)bindableObject).SetValue(); });
        public static readonly BindableProperty ParameterUnitProperty = BindableProperty.Create(nameof(ParameterUnit), typeof(string), typeof(CockpitStatusItem), string.Empty, propertyChanged: (bindableObject, oldValue, newValue) => { ((CockpitStatusItem)bindableObject).SetValue(); });


        public string ParameterName
        {
            get { return (string)GetValue(ParameterNameProperty); }
            set { SetValue(ParameterNameProperty, value); }
        }

        public string ParameterValue
        {
            get { return (string)GetValue(ParameterValueProperty); }
            set { SetValue(ParameterValueProperty, value); }
        }

        public string ParameterUnit
        {
            get { return (string)GetValue(ParameterUnitProperty); }
            set { SetValue(ParameterUnitProperty, value); }
        }

        public CockpitStatusItem ()
		{
			InitializeComponent ();
            this._labelName.SetBinding(Label.TextProperty, new Binding(nameof(ParameterName), source: this, stringFormat: "{0}:"));
        }

        private void SetValue()
        {
            string formattedValue = $"{ParameterValue} {ParameterUnit}";
            
            this._labelValue.Text = formattedValue.Trim();
        }

    }
}