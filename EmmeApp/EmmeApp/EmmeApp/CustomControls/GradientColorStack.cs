﻿using Xamarin.Forms;

namespace EmmeApp.CustomControls
{
    public class GradientColorStack : StackLayout
	{
		public static readonly BindableProperty StartColorProperty = BindableProperty.Create(nameof(StartColor), typeof(Color), typeof(GradientColorStack), Color.White, defaultBindingMode: BindingMode.TwoWay);
		public static readonly BindableProperty EndColorProperty = BindableProperty.Create(nameof(EndColor), typeof(Color), typeof(GradientColorStack), Color.White, defaultBindingMode: BindingMode.TwoWay);

		public Color StartColor
		{
			get { return (Color)GetValue(StartColorProperty); }
			set { SetValue(StartColorProperty, value); }
		}

		public Color EndColor
		{
			get { return (Color)GetValue(EndColorProperty); }
			set { SetValue(EndColorProperty, value); }
		}

		public bool IsHorizontalGradient
		{
			get;
			set;
		} = true;
	}
}
