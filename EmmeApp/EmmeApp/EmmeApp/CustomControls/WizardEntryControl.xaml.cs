﻿using System;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

namespace EmmeApp.CustomControls
{
    public partial class WizardEntryControl : StackLayout
	{
		public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(WizardEntryControl), string.Empty, BindingMode.TwoWay);
		public static readonly BindableProperty TextPlaceholderProperty = BindableProperty.Create(nameof(TextPlaceHolder), typeof(string), typeof(WizardEntryControl), string.Empty);
		public static readonly BindableProperty IsFocusProperty = BindableProperty.Create(nameof(IsFocus), typeof(bool), typeof(WizardEntryControl), false, BindingMode.TwoWay);
		public static readonly BindableProperty IsErrorProperty = BindableProperty.Create(nameof(IsError), typeof(bool), typeof(WizardEntryControl), false, BindingMode.TwoWay);
		public static readonly BindableProperty KeyboardProperty = BindableProperty.Create(nameof(Keyboard), typeof(Keyboard), typeof(WizardEntryControl), Keyboard.Default, BindingMode.TwoWay);

		public static readonly BindableProperty MaximumTextLengthProperty = BindableProperty.Create(nameof(MaximumTextLength), typeof(int), typeof(WizardEntryControl), 32, BindingMode.TwoWay);

		public string Text
		{
			get { return (string)GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}

		public bool IsFocus
		{
			get { return (bool)GetValue(IsFocusProperty); }
			set { SetValue(IsFocusProperty, value); }
		}

		public string TextPlaceHolder
		{
			get { return (string)GetValue(TextPlaceholderProperty); }
			set { SetValue(TextPlaceholderProperty, value); }
		}

		public bool IsError
		{
			get { return (bool)GetValue(IsErrorProperty); }
			set { SetValue(IsErrorProperty, value); }
		}

		public Keyboard Keyboard
		{
			get { return (Keyboard)GetValue(KeyboardProperty); }
			set { SetValue(KeyboardProperty, value); }
		}

		public int MaximumTextLength
		{
			get { return Convert.ToInt32(GetValue(MaximumTextLengthProperty)); }
			set { SetValue(MaximumTextLengthProperty, value); }
		}

		public WizardEntryControl()
		{
			InitializeComponent();

			this._textHolder.SetBinding(CustomEntry.TextProperty, new Binding(nameof(Text), source: this));
			this._textHolder.SetBinding(CustomEntry.PlaceholderProperty, new Binding(nameof(TextPlaceHolder), source: this));
			this._textHolder.SetBinding(CustomEntry.IsFocusedProperty, new Binding(nameof(IsFocus), source: this));
			this._textHolder.SetBinding(CustomEntry.KeyboardProperty, new Binding(nameof(Keyboard), source: this));
			App.Current.On<Android>().UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Resize);
		}

		private void _textHolder_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (!string.IsNullOrWhiteSpace(e.NewTextValue) && Keyboard == Keyboard.Telephone)
			{
				bool isValid = e.NewTextValue.ToCharArray().All(x => char.IsDigit(x));
				((CustomEntry)sender).Text = isValid ? e.NewTextValue : e.NewTextValue.Remove(e.NewTextValue.Length - 1);
			}

			string textValue = ((CustomEntry)sender).Text;
			textValue = textValue ?? "";

			UpdateError(textValue);
		}

		private void UpdateError(string str)
		{
			int strLength = str.Length;

			this.IsError = strLength > MaximumTextLength;
		}
	}
}