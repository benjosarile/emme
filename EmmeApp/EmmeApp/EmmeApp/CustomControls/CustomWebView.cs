﻿using Xamarin.Forms;

namespace EmmeApp.CustomControls
{
    public class CustomWebView : WebView
	{
		public static readonly BindableProperty UriProperty = BindableProperty.Create(
			nameof(Uri),
			typeof(string),
			typeof(CustomWebView),
			string.Empty,
			defaultBindingMode: BindingMode.TwoWay
		  );

		public static readonly BindableProperty PageNumberProperty = BindableProperty.Create(
			nameof(PageNumber),
			typeof(long),
			typeof(CustomWebView),
			default(long),
			defaultBindingMode: BindingMode.TwoWay
		  );

		public static readonly BindableProperty HighlightedTextProperty = BindableProperty.Create(
			nameof(HighlightedText),
			typeof(string),
			typeof(CustomWebView),
			string.Empty,
			defaultBindingMode: BindingMode.TwoWay
		  );

		public string Uri
		{
			get { return (string)GetValue(UriProperty); }
			set { SetValue(UriProperty, value); }
		}

		public long PageNumber
		{
			get { return (long)GetValue(PageNumberProperty); }
			set { SetValue(PageNumberProperty, value); }
		}

		public string HighlightedText
		{
			get { return (string)GetValue(HighlightedTextProperty); }
			set { SetValue(HighlightedTextProperty, value); }
		}

	}
}
