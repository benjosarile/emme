﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace EmmeApp.CustomControls
{
	public partial class DialogAlertContent : PopupPage
    {
        public static readonly BindableProperty LeftButtonTextProperty = BindableProperty.Create(nameof(LeftButtonText), typeof(string), typeof(DialogAlertContent), string.Empty, BindingMode.TwoWay);
        public static readonly BindableProperty LeftButtonCommandProperty = BindableProperty.Create(nameof(LeftButtonText), typeof(ICommand), typeof(DialogAlertContent), null, BindingMode.TwoWay);
        public static readonly BindableProperty RightButtonTextProperty = BindableProperty.Create(nameof(RightButtonText), typeof(string), typeof(DialogAlertContent), string.Empty, BindingMode.TwoWay);
        public static readonly BindableProperty RightButtonCommandProperty = BindableProperty.Create(nameof(RightButtonCommand), typeof(ICommand), typeof(DialogAlertContent), null, BindingMode.TwoWay);
        public static readonly BindableProperty ViewProperty = BindableProperty.Create(nameof(View), typeof(ContentView), typeof(DialogAlertSingleContent), null, BindingMode.TwoWay, propertyChanged: (bindableObject, oldValue, newValue) => { ((DialogAlertContent)bindableObject).DrawView(); });

		private bool _canBackPopup = false;

        public string LeftButtonText
        {
            get { return (string)GetValue(LeftButtonTextProperty); }
            set { SetValue(LeftButtonTextProperty, value); }
        }

        public string RightButtonText
        {
            get { return (string)GetValue(RightButtonTextProperty); }
            set { SetValue(RightButtonTextProperty, value); }
        }

        public ICommand LeftButtonCommand
        {
            get { return (ICommand)GetValue(LeftButtonCommandProperty); }
            set { SetValue(LeftButtonCommandProperty, value); }
        }

        public ICommand RightButtonCommand
        {
            get { return (ICommand)GetValue(RightButtonCommandProperty); }
            set { SetValue(RightButtonCommandProperty, value); }
        }

        public ContentView View
        {
            get { return (ContentView)GetValue(ViewProperty); }
            set { SetValue(ViewProperty, value); }
        }

        public DialogAlertContent()
        {
            InitializeComponent();
            this._title.SetBinding(Label.TextProperty, new Binding(nameof(Title), source: this));
            this._leftButton.SetBinding(Button.TextProperty, new Binding(nameof(LeftButtonText), source: this));
            this._rightButton.SetBinding(Button.TextProperty, new Binding(nameof(RightButtonText), source: this));

			_canBackPopup = true;
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        private async Task ExecuteBackCommand()
        {
			if (_canBackPopup)
			{
				_canBackPopup = false;
				await PopupNavigation.Instance.PopAsync();
			}
        }

        private void DrawView()
        {
            this._contentView.Content = View;
        }

        private async void _leftButton_Clicked(object sender, EventArgs e)
        {
            await ExecuteBackCommand();
            this.LeftButtonCommand?.Execute(null);
        }

        private async void _rightButton_Clicked(object sender, EventArgs e)
        {
            await ExecuteBackCommand();
			this.RightButtonCommand?.Execute(null);
        }
    }
}