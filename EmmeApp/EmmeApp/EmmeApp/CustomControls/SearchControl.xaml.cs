﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using Application = Xamarin.Forms.Application;

namespace EmmeApp.CustomControls
{
    public partial class SearchControl : StackLayout
	{
        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(SearchControl), string.Empty, BindingMode.TwoWay);
        public static readonly BindableProperty SearchPlaceHolderProperty = BindableProperty.Create(nameof(SearchPlaceHolder), typeof(string), typeof(SearchControl), string.Empty);
        public static readonly BindableProperty IsFocusProperty = BindableProperty.Create(nameof(IsFocus), typeof(bool), typeof(SearchControl), false, BindingMode.TwoWay, propertyChanged: (bindableObject, oldValue, newValue) => { ((SearchControl)bindableObject).IsFocusChanged(); });

        public static readonly BindableProperty TextColorProperty = BindableProperty.Create(nameof(TextColor), typeof(Color), typeof(SearchControl), Color.Black);
        public static readonly BindableProperty PlaceholderColorProperty = BindableProperty.Create(nameof(PlaceholderColor), typeof(Color), typeof(SearchControl), (Color)Application.Current.Resources["Color.OpaqueWhite"]);
        public static readonly BindableProperty FontSizeProperty = BindableProperty.Create(nameof(FontSize), typeof(double), typeof(SearchControl), 14.0);
        public static readonly BindableProperty FontFamilyProperty = BindableProperty.Create(nameof(FontFamily), typeof(string), typeof(SearchControl), Application.Current.Resources["HelveticaNeue"].ToString());
        public static readonly BindableProperty OnStartFocusProperty = BindableProperty.Create(nameof(OnStartFocus), typeof(bool), typeof(SearchControl), false, propertyChanged: (bindableObject, oldValue, newValue) => { ((SearchControl)bindableObject).OnStartFocusChanged(); });
        public static readonly BindableProperty EnableClearButtonProperty = BindableProperty.Create(nameof(EnableClearButton), typeof(bool), typeof(SearchControl), false);
        public static readonly BindableProperty OnCompleteCommandProperty = BindableProperty.Create(nameof(OnCompleteCommand), typeof(ICommand), typeof(HomePageTile), null);


        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public bool IsFocus
        {
            get { return (bool)GetValue(IsFocusProperty); }
            set { SetValue(IsFocusProperty, value); }
        }

        public bool OnStartFocus
        {
            get { return (bool)GetValue(OnStartFocusProperty); }
            set { SetValue(OnStartFocusProperty, value); }
        }

        public bool EnableClearButton
        {
            get { return (bool)GetValue(EnableClearButtonProperty); }
            set { SetValue(EnableClearButtonProperty, value); }
        }

        public string SearchPlaceHolder
        {
            get { return (string)GetValue(SearchPlaceHolderProperty); }
            set { SetValue(SearchPlaceHolderProperty, value); }
        }

        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        public Color PlaceholderColor
        {
            get { return (Color)GetValue(PlaceholderColorProperty); }
            set { SetValue(PlaceholderColorProperty, value); }
        }

        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        public string FontFamily
        {
            get { return (string)GetValue(FontFamilyProperty); }
            set { SetValue(FontFamilyProperty, value); }
        }

        public ICommand OnCompleteCommand
        {
            get { return (ICommand)GetValue(OnCompleteCommandProperty); }
            set { SetValue(OnCompleteCommandProperty, value); }
        }

        public SearchControl()
        {
            InitializeComponent();

            this._textHolder.Completed += _textHolder_Completed;

            this._textHolder.SetBinding(CustomEntry.TextProperty, new Binding(nameof(Text), source: this));
            this._textHolder.SetBinding(CustomEntry.PlaceholderProperty, new Binding(nameof(SearchPlaceHolder), source: this));
            this._textHolder.SetBinding(CustomEntry.IsFocusedProperty, new Binding(nameof(IsFocus), source: this));
            this._textHolder.SetBinding(CustomEntry.TextColorProperty, new Binding(nameof(TextColor), source: this));
            this._textHolder.SetBinding(CustomEntry.PlaceholderColorProperty, new Binding(nameof(PlaceholderColor), source: this));
            this._textHolder.SetBinding(CustomEntry.FontSizeProperty, new Binding(nameof(FontSize), source: this));
            this._textHolder.SetBinding(CustomEntry.FontFamilyProperty, new Binding(nameof(FontFamily), source: this));
            this._clearButton.SetBinding(Grid.IsVisibleProperty, new Binding(nameof(EnableClearButton), source: this));
        }        

        private void SetFocus()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                this._textHolder.Focus();
            });
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            this._textHolder.Text = string.Empty;
            this.SetFocus();
        }

        private void OnStartFocusChanged()
        {
            if (OnStartFocus)
            {
                this.SetFocus();
            }
        }

        private void IsFocusChanged()
        {
            if (IsFocus)
            {
                this.SetFocus();
            }
        }

        private void _textHolder_Completed(object sender, EventArgs e)
        {
            if (OnCompleteCommand != null && OnCompleteCommand.CanExecute(null))
            {
                OnCompleteCommand.Execute(null);
            }
        }

    }
}