﻿using Rg.Plugins.Popup.Services;
using System;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace EmmeApp.CustomControls
{
    public partial class WizardPopupPicker : Frame
    {
        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(WizardPopupPicker), string.Empty, BindingMode.TwoWay, propertyChanged: (bindableObject, oldValue, newValue) => { ((WizardPopupPicker)bindableObject).SetText(); });
        public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create(nameof(Placeholder), typeof(string), typeof(WizardPopupPicker), string.Empty, propertyChanged: (bindableObject, oldValue, newValue) => { ((WizardPopupPicker)bindableObject).SetText(); });
        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(nameof(ItemsSource), typeof(IList), typeof(WizardPopupPicker), null);
        public static readonly BindableProperty TitleProperty = BindableProperty.Create(nameof(Title), typeof(string), typeof(WizardPopupPicker), string.Empty);
        public static readonly BindableProperty HasErrorProperty = BindableProperty.Create(nameof(HasError), typeof(bool), typeof(WizardPopupPicker), false, BindingMode.TwoWay);

        private DialogListOverlay _popupPage;

        public ICommand ShowDialogCommand { get; private set; }

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public bool HasError
        {
            get { return (bool)GetValue(HasErrorProperty); }
            set { SetValue(HasErrorProperty, value); }
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        public IList ItemsSource
        {
            get { return (IList)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public WizardPopupPicker()
        {
            InitializeComponent();

            this.ShowDialogCommand = new Command(async () => await ExecuteShowDialogCommand());

        }

        private void EnabledPicker()
        {
            if (_grid != null && _textHolder != null)
            {
                if (!this.IsEnabled)
                {
                    _grid.BackgroundColor = Color.FromHex("#fafafa");
                    _textHolder.TextColor = Color.FromHex("#c7c7c7");
                    _grid.IsEnabled = false;
                }
                else
                {
                    _grid.BackgroundColor = Color.White;
                    _textHolder.TextColor = Color.Black;
                    _grid.IsEnabled = true;
                }
            }
        }


        private void SetText()
        {
            var valueNotSet = string.IsNullOrEmpty(this.Text);
            _textHolder.Text = valueNotSet ? this.Placeholder : this.Text;
            this.HasError = false;
        }

        private async Task ExecuteShowDialogCommand()
        {
            if (this.ItemsSource?.Count > 1)
            {
                _popupPage = new DialogListOverlay();
                _popupPage.ItemSelectedPropertyChanged += _popupPage_ItemSelectedPropertyChanged;
                _popupPage.CloseDialogPropertyChanged += _popupPage_CloseDialogPropertyChanged;
                _popupPage.ItemsSource = null;
                _popupPage.ItemsSource = this.ItemsSource;
                _popupPage.SelectedValue = this.Text;
                _popupPage.Title = this.Title;

                await PopupNavigation.Instance.PushAsync(_popupPage);
            }
        }

        private void _popupPage_CloseDialogPropertyChanged()
        {
            if (_popupPage != null)
            {
                _popupPage.ItemSelectedPropertyChanged -= _popupPage_ItemSelectedPropertyChanged;
                _popupPage.CloseDialogPropertyChanged -= _popupPage_CloseDialogPropertyChanged;
                _popupPage = null;
            }
        }

        private void _popupPage_ItemSelectedPropertyChanged(string selectedItem)
        {
            this.Text = selectedItem;
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            this.ShowDialogCommand.Execute(null);
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            EnabledPicker();
        }
    }
}