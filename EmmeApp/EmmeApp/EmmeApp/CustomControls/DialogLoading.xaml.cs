﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace EmmeApp.CustomControls
{
	public partial class DialogLoading : PopupPage
	{
		public static readonly BindableProperty ButtonTextProperty = BindableProperty.Create(nameof(ButtonText), typeof(string), typeof(DialogLoading), string.Empty, BindingMode.TwoWay);
		public static readonly BindableProperty ButtonCommandProperty = BindableProperty.Create(nameof(ButtonCommand), typeof(ICommand), typeof(DialogLoading), null, BindingMode.TwoWay);
		public static readonly BindableProperty ContentTextProperty = BindableProperty.Create(nameof(ContentText), typeof(string), typeof(DialogLoading), string.Empty, BindingMode.TwoWay);

		private bool _canBackPopup = false;

		public string ButtonText
		{
			get { return (string)GetValue(ButtonTextProperty); }
			set { SetValue(ButtonTextProperty, value); }
		}

		public ICommand ButtonCommand
		{
			get { return (ICommand)GetValue(ButtonCommandProperty); }
			set { SetValue(ButtonCommandProperty, value); }
		}

		public string ContentText
		{
			get { return (string)GetValue(ContentTextProperty); }
			set { SetValue(ContentTextProperty, value); }
		}

		public DialogLoading()
		{
			InitializeComponent();
			this._button.SetBinding(Button.TextProperty, new Binding(nameof(ButtonText), source: this));
			this._contentTextLabel.SetBinding(Label.TextProperty, new Binding(nameof(ContentText), source: this));

			_canBackPopup = true;
		}

		protected override bool OnBackButtonPressed()
		{
			return true;
		}

		private async Task ExecuteBackCommand()
		{
			if (_canBackPopup)
			{
				_canBackPopup = false;
				await PopupNavigation.Instance.PopAsync();
			}
		}

		private async void _button_Clicked(object sender, EventArgs e)
		{
			await ExecuteBackCommand();
			this.ButtonCommand?.Execute(null);
		}
	}
}