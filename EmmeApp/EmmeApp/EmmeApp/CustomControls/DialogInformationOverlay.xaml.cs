﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;

using Xamarin.Forms;

namespace EmmeApp.CustomControls
{
	public partial class DialogInformationOverlay : PopupPage
	{
		public static readonly BindableProperty ViewProperty = BindableProperty.Create(nameof(View), typeof(ContentView), typeof(DialogInformationOverlay), null, BindingMode.TwoWay, propertyChanged: (bindableObject, oldValue, newValue) => { ((DialogInformationOverlay)bindableObject).DrawView(); });

		public ContentView View
		{
			get { return (ContentView)GetValue(ViewProperty); }
			set { SetValue(ViewProperty, value); }
		}

		public DialogInformationOverlay()
		{
			InitializeComponent();
			this._title.SetBinding(Label.TextProperty, new Binding(nameof(Title), source: this));
		}

        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        private void DrawView()
		{
			this._contentView.Content = View;
		}

		private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
		{
			await PopupNavigation.Instance.PopAsync();
		}
	}
}