﻿using EmmeApp.Effects;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace EmmeApp.CustomControls
{
    public partial class HomePageTile : Grid
	{
		public static readonly BindableProperty IsSizeToFitEnableProperty = BindableProperty.Create(nameof(IsSizeToFitEnable), typeof(bool), typeof(HomePageTile), false, BindingMode.TwoWay, propertyChanged: (bindableObject, oldValue, newValue) => ((HomePageTile)bindableObject).SetSizeToFit(newValue));
		public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(HomePageTile), string.Empty);
		public static readonly BindableProperty ImageSourceProperty = BindableProperty.Create(nameof(ImageSource), typeof(string), typeof(HomePageTile), string.Empty);
		public static readonly BindableProperty CommandProperty = BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(HomePageTile), null);
		public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create(nameof(CommandParameter), typeof(object), typeof(HomePageTile), null);

		public bool IsSizeToFitEnable
		{
			get { return Convert.ToBoolean(IsSizeToFitEnableProperty); }
			set { SetValue(IsSizeToFitEnableProperty, value); }
		}

		public string Text
		{
			get { return (string)GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}

		public string ImageSource
		{
			get { return (string)GetValue(ImageSourceProperty); }
			set { SetValue(ImageSourceProperty, value); }
		}

		public object CommandParameter
		{
			get { return GetValue(CommandParameterProperty); }
			set { SetValue(CommandParameterProperty, value); }
		}

		public ICommand Command
		{
			get { return (ICommand)GetValue(CommandProperty); }
			set { SetValue(CommandProperty, value); }
		}

		public HomePageTile()
		{
			InitializeComponent();

			this._textHolder.SetBinding(Label.TextProperty, new Binding(nameof(Text), source: this));
			this._imageHolder.SetBinding(Image.SourceProperty, new Binding(nameof(ImageSource), source: this));

		}

		private void SetSizeToFit(object newValue)
		{
			var val = Convert.ToBoolean(newValue);
			var control = this;

			if (val)
			{
				control.RowDefinitions[1].Height = new GridLength(18, GridUnitType.Absolute);
				control._textHolder.LineBreakMode = LineBreakMode.NoWrap;
				control._textHolder.Effects.Add(new SizeToFit());
			}
			else
			{
				control.RowDefinitions[1].Height = new GridLength(1, GridUnitType.Auto);
				control._textHolder.LineBreakMode = LineBreakMode.WordWrap;
				control._textHolder.Effects.Clear();
			}
		}
		private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
		{
			if (Command != null && Command.CanExecute(null))
			{
				Command.Execute(CommandParameter);
			}
		}
	}
}