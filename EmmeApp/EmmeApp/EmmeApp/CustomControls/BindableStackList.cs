﻿using System.Collections;
using System.Windows.Input;
using Xamarin.Forms;

namespace EmmeApp.CustomControls
{
    public class BindableStackList : StackLayout
    {
        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(nameof(ItemsSource), typeof(IList), typeof(BindableStackList), null, propertyChanged: (bindableObject, oldValue, newValue) => { ((BindableStackList)bindableObject).OnItemSourceChanged(newValue); });
        public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create(nameof(ItemTemplate), typeof(DataTemplate), typeof(BindableStackList), null);
        public static readonly BindableProperty CommandProperty = BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(BindableStackList), null);


        public IList ItemsSource
        {
            get { return (IList)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        private void OnItemSourceChanged(object newValue)
        {
            if (newValue is IList newItemsList)
            {
                foreach (var item in newItemsList)
                {
                    TapGestureRecognizer tapGesture = new TapGestureRecognizer();
                    tapGesture.NumberOfTapsRequired = 1;
                    tapGesture.Command = Command;
                    tapGesture.CommandParameter = item;
                    
                    var view = (View)ItemTemplate.CreateContent();
                    view.BindingContext = item;
                    view.GestureRecognizers.Add(tapGesture);


                    Children.Add(view);
                }
            }
        }

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }
    }
}
