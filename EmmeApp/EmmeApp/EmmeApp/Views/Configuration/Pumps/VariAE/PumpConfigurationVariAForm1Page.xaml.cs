﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class PumpConfigurationVariAForm1Page : MobileContentPageBase
    {
        public PumpConfigurationVariAForm1Page()
        {
            InitializeComponent();
        }
        protected override bool OnBackButtonPressed()
        {
            var bindingContext = (PumpConfigurationVariAForm1PageViewModel)BindingContext;
            bindingContext.WizardCloseCommand.Execute();
            return true;
        }

        private void DatePickerTapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            variADatePicker.Focus();
        }

        private void TimePickerTapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            variATimePicker.Focus();
        }
    }
}
