﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class PumpConfigurationVariAForm2SystemDescriptionPage : MobileContentPageBase
    {
        public PumpConfigurationVariAForm2SystemDescriptionPage()
        {
            InitializeComponent();
        }
        protected override bool OnBackButtonPressed()
        {
            var bindingContext = (PumpConfigurationVariAForm2SystemDescriptionPageViewModel)BindingContext;
            bindingContext.WizardCloseCommand.Execute();
            return true;
        }
    }
}
