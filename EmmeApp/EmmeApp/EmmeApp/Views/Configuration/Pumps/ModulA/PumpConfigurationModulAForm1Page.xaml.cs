﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class PumpConfigurationModulAForm1Page : MobileContentPageBase
    {
        public PumpConfigurationModulAForm1Page()
        {
            InitializeComponent();
        }
        protected override bool OnBackButtonPressed()
        {
            var bindingContext = (PumpConfigurationModulAForm1PageViewModel)BindingContext;
            bindingContext.WizardCloseCommand.Execute();
            return true;
        }

        private void DatePickerTapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            modulADatePicker.Focus();
        }

        private void TimePickerTapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            modulATimePicker.Focus();
        }
    }
}
