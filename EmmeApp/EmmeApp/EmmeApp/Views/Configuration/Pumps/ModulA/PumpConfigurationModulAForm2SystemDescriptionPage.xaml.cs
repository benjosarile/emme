﻿using EmmeApp.ViewModels;

namespace EmmeApp.Views
{
    public partial class PumpConfigurationModulAForm2SystemDescriptionPage : MobileContentPageBase
    {
		public PumpConfigurationModulAForm2SystemDescriptionPage()
		{
			InitializeComponent ();
		}

        protected override bool OnBackButtonPressed()
        {
            var bindingContext = (PumpConfigurationModulAForm2SystemDescriptionPageViewModel)BindingContext;
            bindingContext.WizardCloseCommand.Execute();
            return true;
        }
    }
}