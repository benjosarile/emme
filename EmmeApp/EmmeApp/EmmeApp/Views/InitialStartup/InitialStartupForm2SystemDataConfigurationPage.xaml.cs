﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class InitialStartupForm2SystemDataConfigurationPage : MobileContentPageBase
    {
        public InitialStartupForm2SystemDataConfigurationPage()
        {
            InitializeComponent();
        }

		protected override bool OnBackButtonPressed()
		{
			var bindingContext = (InitialStartupForm2SystemDataConfigurationPageViewModel)BindingContext;
			bindingContext.WizardLeftButtonCommand.Execute();
			return true;
		}
	}
}
