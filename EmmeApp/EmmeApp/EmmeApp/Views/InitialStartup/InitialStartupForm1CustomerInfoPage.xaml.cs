﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class InitialStartupForm1CustomerInfoPage : MobileContentPageBase
    {
        public InitialStartupForm1CustomerInfoPage()
        {
            InitializeComponent();
        }

		protected override bool OnBackButtonPressed()
		{
			var bindingContext = (InitialStartupForm1CustomerInfoPageViewModel)BindingContext;
			bindingContext.WizardCloseCommand.Execute();
			return true;
		}
	}
}
