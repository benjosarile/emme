﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class InitialStartupForm4PumpAndMotorConfigurationPage : MobileContentPageBase
    {
        public InitialStartupForm4PumpAndMotorConfigurationPage()
        {
            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            var bindingContext = (InitialStartupForm4PumpAndMotorConfigurationPageViewModel)BindingContext;
            bindingContext.WizardLeftButtonCommand.Execute();
            return true;
        }
    }
}
