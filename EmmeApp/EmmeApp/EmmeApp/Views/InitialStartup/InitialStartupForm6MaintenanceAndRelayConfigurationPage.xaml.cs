﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class InitialStartupForm6MaintenanceAndRelayConfigurationPage : MobileContentPageBase
	{
        public InitialStartupForm6MaintenanceAndRelayConfigurationPage()
        {
            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            var bindingContext = (InitialStartupForm6MaintenanceAndRelayConfigurationPageViewModel)BindingContext;
            bindingContext.WizardLeftButtonCommand.Execute();
            return true;
        }
    }
}
