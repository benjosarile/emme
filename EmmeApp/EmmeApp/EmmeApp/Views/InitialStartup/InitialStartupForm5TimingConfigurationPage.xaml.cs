﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class InitialStartupForm5TimingConfigurationPage : MobileContentPageBase
    {
        public InitialStartupForm5TimingConfigurationPage()
        {
            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            var bindingContext = (InitialStartupForm5TimingConfigurationPageViewModel)BindingContext;
            bindingContext.WizardLeftButtonCommand.Execute();
            return true;
        }
    }
}
