﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class InitialStartupForm3LevelProbeConfigurationPage : MobileContentPageBase
    {
        public InitialStartupForm3LevelProbeConfigurationPage()
        {
            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            var bindingContext = (InitialStartupForm3LevelProbeConfigurationPageViewModel)BindingContext;
            bindingContext.WizardLeftButtonCommand.Execute();
            return true;
        }
    }
}
