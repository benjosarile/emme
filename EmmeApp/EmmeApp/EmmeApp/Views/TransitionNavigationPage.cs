﻿using EmmeApp.Common.Constants;
using EmmeApp.Events;
using EmmeApp.ItemModels;
using Prism.Events;
using Prism.Navigation;
using Xamarin.Forms;

namespace EmmeApp.Views
{
	public class TransitionNavigationPage : NavigationPage
	{
		public TransitionNavigationPage() : base()
		{

		}

		public TransitionNavigationPage(Page root) : base(root)
		{
		}

		protected override bool OnBackButtonPressed()
		{
			var navigationStack = Xamarin.Forms.Application.Current.MainPage.Navigation.NavigationStack;

			if (navigationStack.Count == 1 && navigationStack[0] is MasterDetailPage)
			{
				var masterDetaiPage = (MasterDetailPage)navigationStack[0];

				if (masterDetaiPage.Detail is NavigationPage && !((masterDetaiPage.Detail as NavigationPage).CurrentPage is HomePage))
				{
					var eventAggregator = App.Resolve<IEventAggregator>();

					eventAggregator.GetEvent<MasterNavigationEvent>().Publish(new NavigationItemModel()
					{
						NavigationPath = $"{ViewNames.NavigationPage}/{ViewNames.HomePage}",
						Parameters = new NavigationParameters(),
						IsModalNavigation = false
					});
				}
			}

			return true;
		}
	}
}
