﻿using EmmeApp.CustomControls;
using EmmeApp.Localization.Resources;
using EmmeApp.ViewModels;
using Prism.Navigation;
using System.Linq;

namespace EmmeApp.Views
{
    public partial class CockpitPumpsVariAERootPage : MobileContentPageBase, IInitialize
    {
        public CockpitPumpsVariAERootPage()
        {
            InitializeComponent();
        }

		public void Initialize(INavigationParameters parameters)
		{
			if (!tabViewControl.ItemSource.Any())
			{
				CustomTabItem overviewTabItem = new CustomTabItem(AppResources.Cockpit_Tabbed_Title_Overview, new CockpitPumpsVariAEOverviewPage())
				{
					HeaderTabTextFontAttributes = Xamarin.Forms.FontAttributes.Bold,
					HeaderTabTextFontFamily = App.Current.Resources["HelveticaNeue-Bold"].ToString(),
				};
				CustomTabItem statusTabItem = new CustomTabItem(AppResources.Cockpit_Tabbed_Title_Status, new CockpitPumpsVariAEStatusPage());
				CustomTabItem settingsTabItem = new CustomTabItem(AppResources.Cockpit_Tabbed_Title_Settings, new CockpitPumpsVariAESettingsPage());

				tabViewControl.AddTab(overviewTabItem);
				tabViewControl.AddTab(statusTabItem);
				tabViewControl.AddTab(settingsTabItem);

				var overviewVm = (CockpitPumpsVariAEOverviewPageViewModel)tabViewControl.ItemSource[0].Content.BindingContext;
				var statusVm = (CockpitPumpsVariAEStatusPageViewModel)tabViewControl.ItemSource[1].Content.BindingContext;
				var settingsVm = (CockpitPumpsVariAESettingsPageViewModel)tabViewControl.ItemSource[2].Content.BindingContext;
				var vm = (CockpitPumpsVariAERootPageViewModel)this.BindingContext;
				vm.SetCockpitPumpsVariAEOverviewPageViewModel(overviewVm);
				vm.SetCockpitPumpsVariAEStatusPageViewModel(statusVm);
				vm.SetCockpitPumpsVariAESettingsPageViewModel(settingsVm);
			}

			foreach (var tabItem in tabViewControl.ItemSource)
			{
				(tabItem.Content as IInitialize)?.Initialize(parameters);
				(tabItem.Content as INavigationAware)?.OnNavigatedTo(parameters);
				(tabItem.Content as INavigationAware)?.OnNavigatedFrom(parameters);
				(tabItem.Content?.BindingContext as IInitialize)?.Initialize(parameters);
				(tabItem.Content?.BindingContext as INavigationAware)?.OnNavigatedTo(parameters);
				(tabItem.Content?.BindingContext as INavigationAware)?.OnNavigatedFrom(parameters);
			}
		}

		private void TabViewControl_PositionChanged(object sender, CustomControls.PositionChangedEventArgs e)
		{
			int tabItemCount = tabViewControl.ItemSource.Count;

			for (int i = 0; i < tabItemCount; i++)
			{
				if (e.NewPosition != i)
				{
					tabViewControl.ItemSource[i].HeaderTabTextFontAttributes = Xamarin.Forms.FontAttributes.None;
					tabViewControl.ItemSource[e.NewPosition].HeaderTabTextFontFamily = App.Current.Resources["HelveticaNeue"].ToString();
				}
			}

			tabViewControl.ItemSource[e.NewPosition].HeaderTabTextFontAttributes = Xamarin.Forms.FontAttributes.Bold;
			tabViewControl.ItemSource[e.NewPosition].HeaderTabTextFontFamily = App.Current.Resources["HelveticaNeue-Bold"].ToString();
		}
	}
}
