﻿using EmmeApp.CustomControls;
using EmmeApp.Localization.Resources;
using EmmeApp.ViewModels;
using Prism.Navigation;
using System.Linq;

namespace EmmeApp.Views
{
    public partial class CockpitPumpsModulARootPage : MobileContentPageBase, IInitialize
    {
        public CockpitPumpsModulARootPage()
        {
            InitializeComponent();
        }

        public void Initialize(INavigationParameters parameters)
        {
            if (!tabViewControl.ItemSource.Any())
            {
                CustomTabItem overviewTabItem = new CustomTabItem(AppResources.Cockpit_Tabbed_Title_Overview, new CockpitPumpsModulAOverviewPage())
                {
                    HeaderTabTextFontAttributes = Xamarin.Forms.FontAttributes.Bold,
                    HeaderTabTextFontFamily = App.Current.Resources["HelveticaNeue-Bold"].ToString(),
                };
                CustomTabItem statusTabItem = new CustomTabItem(AppResources.Cockpit_Tabbed_Title_Status, new CockpitPumpsModulAStatusPage());
                CustomTabItem settingsTabItem = new CustomTabItem(AppResources.Cockpit_Tabbed_Title_Settings, new CockpitPumpsModulASettingsPage());

                tabViewControl.AddTab(overviewTabItem);
                tabViewControl.AddTab(statusTabItem);
                tabViewControl.AddTab(settingsTabItem);

                var overviewVm = (CockpitPumpsModulAOverviewPageViewModel)tabViewControl.ItemSource[0].Content.BindingContext;
                var statusVm = (CockpitPumpsModulAStatusPageViewModel)tabViewControl.ItemSource[1].Content.BindingContext;
                var settingsVm = (CockpitPumpsModulASettingsPageViewModel)tabViewControl.ItemSource[2].Content.BindingContext;
                var vm = (CockpitPumpsModulARootPageViewModel)this.BindingContext;
                vm.SetCockpitPumpsModulAOverviewPageViewModel(overviewVm);
                vm.SetCockpitPumpsModulAStatusPageViewModel(statusVm);
                vm.SetCockpitPumpsModulASettingsPageViewModel(settingsVm);
            }

            foreach (var tabItem in tabViewControl.ItemSource)
            {
                (tabItem.Content as IInitialize)?.Initialize(parameters);
                (tabItem.Content as INavigationAware)?.OnNavigatedTo(parameters);
                (tabItem.Content as INavigationAware)?.OnNavigatedFrom(parameters);
                (tabItem.Content?.BindingContext as IInitialize)?.Initialize(parameters);
                (tabItem.Content?.BindingContext as INavigationAware)?.OnNavigatedTo(parameters);
                (tabItem.Content?.BindingContext as INavigationAware)?.OnNavigatedFrom(parameters);
            }
        }

        private void TabViewControl_PositionChanged(object sender, CustomControls.PositionChangedEventArgs e)
        {
            int tabItemCount = tabViewControl.ItemSource.Count;

            for (int i = 0; i < tabItemCount; i++)
            {
                if (e.NewPosition != i)
                {
                    tabViewControl.ItemSource[i].HeaderTabTextFontAttributes = Xamarin.Forms.FontAttributes.None;
                    tabViewControl.ItemSource[e.NewPosition].HeaderTabTextFontFamily = App.Current.Resources["HelveticaNeue"].ToString();
                }
            }

            tabViewControl.ItemSource[e.NewPosition].HeaderTabTextFontAttributes = Xamarin.Forms.FontAttributes.Bold;
            tabViewControl.ItemSource[e.NewPosition].HeaderTabTextFontFamily = App.Current.Resources["HelveticaNeue-Bold"].ToString();
        }
    }
}
