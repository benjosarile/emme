﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class AboutPage : MobileContentPageBase
    {
        public AboutPage()
        {
            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            var bindingContext = (AboutPageViewModel)BindingContext;
            bindingContext.BackCommand.Execute();
            return true;
        }
    }
}
