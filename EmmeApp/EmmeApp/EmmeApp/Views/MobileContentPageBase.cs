﻿using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace EmmeApp.Views
{
    public class MobileContentPageBase : ContentPage
	{
		public MobileContentPageBase()
		{
			Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);

			On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

			switch (Device.RuntimePlatform)
			{
				case Device.iOS:
					Padding = new Thickness(0, 20, 0, 0);
					break;
				default:
					Padding = new Thickness(0);
					break;
			}
		}
	}
}
