﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class PumpReplacementDetailPage : MobileContentPageBase
    {
        public PumpReplacementDetailPage()
        {
            InitializeComponent();
        }

		protected override bool OnBackButtonPressed()
		{
			var bindingContext = (PumpReplacementDetailPageViewModel)BindingContext;
			bindingContext.BackCommand.Execute();
			return true;
		}
	}
}
