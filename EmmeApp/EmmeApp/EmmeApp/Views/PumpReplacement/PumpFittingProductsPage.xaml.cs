﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
	public partial class PumpFittingProductsPage : MobileContentPageBase
	{
		public PumpFittingProductsPage()
		{
			InitializeComponent();
		}

		protected override bool OnBackButtonPressed()
		{
			var bindingContext = (PumpFittingProductsPageViewModel)BindingContext;
			bindingContext.BackCommand.Execute();
			return true;
		}
	}
}
