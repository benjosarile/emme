﻿using EmmeApp.ViewModels;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

namespace EmmeApp.Views
{
    public partial class SearchPumpPage : MobileContentPageBase
    {
		public SearchPumpPage()
        {
            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            var bindingContext = (SearchPumpPageViewModel)BindingContext;
            if (bindingContext.IsOcrRunning)
            {
                try
                {
                    bindingContext.CancellationTokenSource?.Cancel();
                } catch { }
            }
            else
            {
                bindingContext.BackCommand.Execute();
            }
            return true;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.Current.On<Android>().UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Resize);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.Current.On<Android>().UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Pan);
        }

    }

    
}