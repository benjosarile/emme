﻿using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Localization.Resources;
using EmmeApp.ViewModels;
using Prism.Navigation;
using System.Linq;

namespace EmmeApp.Views
{
    public partial class ControlUnitLogRootPage : MobileContentPageBase, IInitialize
    {
        public ControlUnitLogRootPage()
        {
            InitializeComponent();
        }

        public void Initialize(INavigationParameters parameters)
        {
            if (!tabViewControl.ItemSource.Any())
            {
                CustomTabItem eventTabItem = new CustomTabItem(AppResources.ControlUnitLog_Tabbed_Title_Event.ToTitleCase(), new ControlUnitLogEventPage())
                {
                    HeaderTabTextFontAttributes = Xamarin.Forms.FontAttributes.Bold,
                    HeaderTabTextFontFamily = App.Current.Resources["HelveticaNeue-Bold"].ToString(),
                };

                tabViewControl.AddTab(eventTabItem);

                var eventPageVm = (ControlUnitLogEventPageViewModel)tabViewControl.ItemSource[0].Content.BindingContext;
                var vm = (ControlUnitLogRootPageViewModel)this.BindingContext;
                vm.SetControlUnitLogEventPageViewModel(eventPageVm);
            }

            foreach (var tabItem in tabViewControl.ItemSource)
            {
                (tabItem.Content as IInitialize)?.Initialize(parameters);
                (tabItem.Content as INavigationAware)?.OnNavigatedTo(parameters);
                (tabItem.Content as INavigationAware)?.OnNavigatedFrom(parameters);
                (tabItem.Content?.BindingContext as IInitialize)?.Initialize(parameters);
                (tabItem.Content?.BindingContext as INavigationAware)?.OnNavigatedTo(parameters);
                (tabItem.Content?.BindingContext as INavigationAware)?.OnNavigatedFrom(parameters);
            }
        }

        private void TabViewControl_PositionChanged(object sender, PositionChangedEventArgs e)
        {
            int tabItemCount = tabViewControl.ItemSource.Count;

            for (int i = 0; i < tabItemCount; i++)
            {
                if (e.NewPosition != i)
                {
                    tabViewControl.ItemSource[i].HeaderTabTextFontAttributes = Xamarin.Forms.FontAttributes.None;
                    tabViewControl.ItemSource[e.NewPosition].HeaderTabTextFontFamily = App.Current.Resources["HelveticaNeue"].ToString();
                }
            }

            tabViewControl.ItemSource[e.NewPosition].HeaderTabTextFontAttributes = Xamarin.Forms.FontAttributes.Bold;
            tabViewControl.ItemSource[e.NewPosition].HeaderTabTextFontFamily = App.Current.Resources["HelveticaNeue-Bold"].ToString();
        }
    }
}
