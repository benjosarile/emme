﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class ControlUnitLogEventPage : ContentView
    {
        public ControlUnitLogEventPage()
        {
            InitializeComponent();
        }

        private void IncrementalListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null) return;

            Task.Delay(500);

            if (sender is ListView lv) lv.SelectedItem = null;

        }
    }
}
