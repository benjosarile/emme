﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EmmeApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PumpLogAlertPage : ContentView
	{
		public PumpLogAlertPage ()
		{
			InitializeComponent ();
		}
	}
}