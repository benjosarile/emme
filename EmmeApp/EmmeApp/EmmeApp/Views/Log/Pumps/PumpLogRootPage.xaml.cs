﻿using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Localization.Resources;
using EmmeApp.ViewModels;
using Prism.Navigation;
using System.Linq;

namespace EmmeApp.Views
{
    public partial class PumpLogRootPage : MobileContentPageBase, IInitialize
    {
        public PumpLogRootPage()
        {
            InitializeComponent();
        }

        public void Initialize(INavigationParameters parameters)
        {
            if (!tabViewControl.ItemSource.Any())
            {
                CustomTabItem alarmTabItem = new CustomTabItem(AppResources.PumpLog_Tabbed_Title_Alarm.ToTitleCase(), new PumpLogAlertPage())
                {
                    HeaderTabTextFontAttributes = Xamarin.Forms.FontAttributes.Bold,
                    HeaderTabTextFontFamily = App.Current.Resources["HelveticaNeue-Bold"].ToString(),
                };
                CustomTabItem warningTabItem = new CustomTabItem(AppResources.PumpLog_Tabbed_Title_Warning.ToTitleCase(), new PumpLogWarningPage());

                tabViewControl.AddTab(alarmTabItem);
                tabViewControl.AddTab(warningTabItem);

                var alertPageViewModel = (PumpLogAlertPageViewModel)tabViewControl.ItemSource[0].Content.BindingContext;
                var warningPageViewModel = (PumpLogWarningPageViewModel)tabViewControl.ItemSource[1].Content.BindingContext;

                var vm = (PumpLogRootPageViewModel)this.BindingContext;
                vm.SetPumpLogAlertPageViewModel(alertPageViewModel);
                vm.SetPumpLogWarningPageViewModel(warningPageViewModel);

            }

            foreach (var tabItem in tabViewControl.ItemSource)
            {
                (tabItem.Content as IInitialize)?.Initialize(parameters);
                (tabItem.Content as INavigationAware)?.OnNavigatedTo(parameters);
                (tabItem.Content as INavigationAware)?.OnNavigatedFrom(parameters);
                (tabItem.Content?.BindingContext as IInitialize)?.Initialize(parameters);
                (tabItem.Content?.BindingContext as INavigationAware)?.OnNavigatedTo(parameters);
                (tabItem.Content?.BindingContext as INavigationAware)?.OnNavigatedFrom(parameters);
            }
        }

        private void TabViewControl_PositionChanged(object sender, PositionChangedEventArgs e)
        {
            int tabItemCount = tabViewControl.ItemSource.Count;

            for (int i = 0; i < tabItemCount; i++)
            {
                if (e.NewPosition != i)
                {
                    tabViewControl.ItemSource[i].HeaderTabTextFontAttributes = Xamarin.Forms.FontAttributes.None;
                    tabViewControl.ItemSource[e.NewPosition].HeaderTabTextFontFamily = App.Current.Resources["HelveticaNeue"].ToString();
                }
            }

            tabViewControl.ItemSource[e.NewPosition].HeaderTabTextFontAttributes = Xamarin.Forms.FontAttributes.Bold;
            tabViewControl.ItemSource[e.NewPosition].HeaderTabTextFontFamily = App.Current.Resources["HelveticaNeue-Bold"].ToString();
        }
    }
}
