﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class DocumentSearchFilterPage : MobileContentPageBase
    {
        public DocumentSearchFilterPage()
        {
            InitializeComponent();
        }


		protected override bool OnBackButtonPressed()
		{
			var bindingContext = (DocumentSearchFilterPageViewModel)BindingContext;
			bindingContext.BackCommand.Execute();
			return true;
		}
	}
}
