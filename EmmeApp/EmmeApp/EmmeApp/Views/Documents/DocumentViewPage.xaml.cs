﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class DocumentViewPage : MobileContentPageBase
    {
        public DocumentViewPage()
        {
            InitializeComponent();
        }

		protected override bool OnBackButtonPressed()
		{
			var bindingContext = (DocumentViewPageViewModel)BindingContext;
			bindingContext.BackCommand.Execute();
			return true;
		}
	}
}
