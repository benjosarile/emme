﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class DocumentSearchPage : MobileContentPageBase
    {
        public DocumentSearchPage()
        {
            InitializeComponent();
        }

		protected override bool OnBackButtonPressed()
		{
			var bindingContext = (DocumentSearchPageViewModel)BindingContext;
			bindingContext.BackCommand.Execute();
			return true;
		}
	}
}
