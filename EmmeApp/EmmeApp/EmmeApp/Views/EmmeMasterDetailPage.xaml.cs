﻿using EmmeApp.Events;
using Prism.Events;
using Prism.Navigation;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace EmmeApp.Views
{
    public partial class EmmeMasterDetailPage : Xamarin.Forms.MasterDetailPage, IDestructible
    {
        private readonly IEventAggregator _eventAggregator;
        public EmmeMasterDetailPage(IEventAggregator eventAggregator)
        {
            InitializeComponent();

			Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
			On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

			_eventAggregator = eventAggregator;
            _eventAggregator.GetEvent<HamburgerTappedEvent>().Subscribe(HamburgerToggle);
            _eventAggregator.GetEvent<HamburgerHideEvent>().Subscribe(HamburgerHide);

		}

		protected override bool OnBackButtonPressed()
		{
			return true;
		}

		public void Destroy()
        {
            _eventAggregator.GetEvent<HamburgerTappedEvent>().Unsubscribe(HamburgerToggle);
			_eventAggregator.GetEvent<HamburgerHideEvent>().Unsubscribe(HamburgerHide);
		}

		private void HamburgerToggle()
        {
            MasterDetail.IsPresented = !IsPresented;
        }

		private void HamburgerHide()
		{
			MasterDetail.IsPresented = false;
		}
	}
}