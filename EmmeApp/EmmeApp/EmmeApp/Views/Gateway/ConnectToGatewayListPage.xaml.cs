﻿using EmmeApp.ViewModels;
using Xamarin.Forms;

namespace EmmeApp.Views
{
    public partial class ConnectToGatewayListPage : MobileContentPageBase
    {
        public ConnectToGatewayListPage()
        {
            InitializeComponent();
        }

		protected override bool OnBackButtonPressed()
		{
			var vm = (ConnectToGatewayListPageViewModel)BindingContext;
			vm.BackCommand.Execute();
			return true;
		}
	}
}
