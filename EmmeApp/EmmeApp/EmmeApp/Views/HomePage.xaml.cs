﻿namespace EmmeApp.Views
{
    public partial class HomePage : MobileContentPageBase
    {
        public HomePage()
        {
            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}
