﻿namespace EmmeApp.Views
{
    public partial class LoadingPage : MobileContentPageBase
	{
		public LoadingPage()
		{
			InitializeComponent();
		}

		protected override bool OnBackButtonPressed() 
		{
			return true;
		}
	}
}
