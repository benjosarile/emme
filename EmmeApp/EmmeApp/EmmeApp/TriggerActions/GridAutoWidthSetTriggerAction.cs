﻿using Xamarin.Forms;

namespace EmmeApp.TriggerActions
{
    public class GridSetAutoWidthTriggerAction : TriggerAction<Grid>
	{
		protected override void Invoke(Grid sender)
		{
			if (sender != null && sender.Height > 0)
			{
				sender.WidthRequest = sender.Height;
			}
		}
	}
}
