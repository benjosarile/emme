﻿using Xamarin.Forms;

namespace EmmeApp.TriggerActions
{
    public class GridSetAutoHeightTriggerAction : TriggerAction<Grid>
	{
		protected override void Invoke(Grid sender)
		{
			if (sender != null && sender.Width > 0)
			{
				sender.HeightRequest = sender.Width;
			}
		}
	}
}
