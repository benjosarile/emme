﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.Configuration;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Events;
using EmmeApp.ItemModels;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Plugin.BluetoothLE;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EmmeApp.ViewModels
{
    public class GatewaySubPageViewModel : ViewModelBase
    {
        private readonly IAdapter _adapter;
        private readonly IBleManager _bleManager;
        private readonly IParameterDataCacheManager _parameterDataCacheManager = null;
        private readonly ICockpitControlUnitManager _cockpitControlUnitManager;
        private readonly IConfigurationControlUnitManager _configurationControlUnitManager;
        private readonly IConfigurationPumpModulAManager _configurationPumpModulAManager;
        private readonly IConfigurationPumpVariAManager _configurationPumpVariAManager;
        private readonly ILogControlUnitManager _logControlUnitManager;
        private readonly ILogPumpManager _logPumpManager;
        private readonly ICockpitPumpsModulAManager _cockpitPumpsModulAManager;
        private readonly ICockpitPumpsVariAEManager _cockpitPumpsVariAEManager;

        private CompositeDisposable DestroyWith { get; } = new CompositeDisposable();

        public GatewaySubPageViewModel(INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IAdapter adapter,
            IBleManager bleManager,
            IParameterDataCacheManager parameterDataCacheManager,
            ICockpitControlUnitManager cockpitControlUnitManager,
            IConfigurationControlUnitManager configurationControlUnitManager,
            IConfigurationPumpModulAManager configurationPumpModulAManager,
            IConfigurationPumpVariAManager configurationPumpVariAManager,
            ILogControlUnitManager logControlUnitManager,
            ILogPumpManager logPumpManager,
            ICockpitPumpsModulAManager cockpitPumpsModulAManager,
            ICockpitPumpsVariAEManager cockpitPumpsVariAEManager) : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
        {
            _adapter = adapter;
            _bleManager = bleManager;
            _parameterDataCacheManager = parameterDataCacheManager;
            _cockpitControlUnitManager = cockpitControlUnitManager;
            _configurationControlUnitManager = configurationControlUnitManager;
            _configurationPumpModulAManager = configurationPumpModulAManager;
            _configurationPumpVariAManager = configurationPumpVariAManager;
            _cockpitPumpsModulAManager = cockpitPumpsModulAManager;
            _cockpitPumpsVariAEManager = cockpitPumpsVariAEManager;
            _logControlUnitManager = logControlUnitManager;
            _logPumpManager = logPumpManager;

            this.ConnectToDeviceCommand = new DelegateCommand(() => OnConnectToDevice());
            this.ExitCommand = new DelegateCommand(async () => await OnExit());
        }

        private string _pageNameParam = string.Empty;
        private bool _isPopupShow = false;
        private bool _isAcquiringData = false;

        private GatewayDeviceItemModel _gateway;
        public GatewayDeviceItemModel Gateway
        {
            get => _gateway;
            set => SetProperty(ref _gateway, value);
        }

        private bool _isConnected;
        public bool IsConnected
        {
            get => _isConnected;
            set => SetProperty(ref _isConnected, value);
        }

        private double _animatedProgress = 1;
        public double AnimatedProgress
        {
            get => _animatedProgress;
            set => SetProperty(ref _animatedProgress, value);
        }

        private double _progress;
        public double Progress
        {
            get => _progress;
            set
            {
                if (value.Equals(1) && !this.IsConnected)
                {
                    Device.BeginInvokeOnMainThread(async () =>
                         {
                             this.DestroyWith?.Dispose();
                             _adapter.StopScan();
                             await GoBackOnErrorAsync(AppResources.ConnectToGateway_Connection_Failed);
                         });
                }
                Debug.WriteLine($"Progress Animation: {value}");
                SetProperty(ref _progress, value);
            }
        }

        private double _animationLength = 20000;
        public double AnimationLength
        {
            get => _animationLength;
            set => SetProperty(ref _animationLength, value);
        }


        public DelegateCommand ConnectToDeviceCommand { get; private set; }
        public DelegateCommand ExitCommand { get; private set; }

        private void OnConnectToDevice()
        {
            this.AppCenterService.TrackEvent($"{ViewNames.GatewaySubPage} - ExecuteConnectToDeviceCommand");

            if (this.Gateway.Device.Status == ConnectionStatus.Disconnected)
            {
                _bleManager.ConnectToDevice(this.Gateway.Device);
            }
        }

        private async Task OnExit()
        {
            this.AppCenterService.TrackEvent($"{ViewNames.GatewaySubPage} - ExecuteExitCommand");

            INavigationParameters parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.ShouldExitConnection, string.Empty);

            _isAcquiringData = true;

            switch (_pageNameParam)
            {
                case AppConstants.Configuration:
                    await ConfigurationNavigationAsync();
                    break;

                case AppConstants.Cockpit:
                    await CockpitNavigationAsync();
                    break;

                case AppConstants.Log:
                    await LogNavigationAsync();
                    break;

                default:
                    this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
                    //TODO loading taking too long 2252
                    await _parameterDataCacheManager.GetAndCacheParameters(onError: ex => this.UserDialogs.Alert(ex.ToString()));
                    this.UserDialogs.HideLoading();
                    break;
            }

            _isAcquiringData = false;

            var navigationResult = await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);

            if (!navigationResult.Success)
            {
                throw navigationResult.Exception;
            }
        }

        private async Task CheckDeviceIfAllowed()
        {
            this.AppCenterService.TrackEvent($"{ViewNames.GatewaySubPage} - CheckDeviceIfAllowed");

            var availableServices = _bleManager.GetAvailableServices();

            foreach (var availableService in availableServices)
            {
                var serviceGuid = Guid.Parse(availableService.Uuid);

                try
                {
                    var gattService = await this.Gateway.Device.GetKnownService(serviceGuid);

                    if (gattService != null)
                    {
                        var gattCharacteristics = await gattService.DiscoverCharacteristics();

                        var notificationCharactestics = availableService.Characteristics.FirstOrDefault(x => x.Role == "Notification");

                        if (notificationCharactestics != null && gattCharacteristics.Uuid.ToString().ToUpper() == notificationCharactestics.Uuid)
                        {
                            this.IsConnected = true;
                        }
                    }
                }
                catch
                {
                    this.IsConnected = false;
                }

                if (this.IsConnected)
                {
                    this.AppCenterService.TrackEvent($"{ViewNames.GatewaySubPage} - CheckDeviceIfAllowed", new Dictionary<string, string>()
                    {
                        { "IsConnected", "true"}
                    });


                    this.AnimationLength = 300;
                    break;
                }
            }

            if (!this.IsConnected)
            {
                await GoBackOnErrorAsync(AppResources.Alert_Message_BLE_NotCompatible);

                return;
            }

            await Task.Delay(2000);
            //Insert event aggregator

            this.AppCenterService.TrackEvent($"{ViewNames.GatewaySubPage} - CheckDeviceIfAllowed", new Dictionary<string, string>()
            {
                { "IsConnected", "false"}
            });

            this.ExitCommand?.Execute();
        }

        private async Task GoBackOnErrorAsync(string errorMessage)
        {
            this.AppCenterService.TrackEvent($"{ViewNames.GatewaySubPage} - GoBackOnErrorAsync", new Dictionary<string, string>()
            {
                { "errorMessage", errorMessage }
            });

            if (!_isPopupShow)
            {
                _isPopupShow = true;
                this.Gateway.Device.CancelConnection();
                await this.UserDialogs.AlertAsync(errorMessage);
                await NavigationUtilities.GoBackAsync(this.NavigationService, null, null, false);
                _isPopupShow = false;
            }
        }

        private async Task ConfigurationNavigationAsync()
        {
            try
            {
                this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);

                //TODO this is taking too long
                var initialData = await _parameterDataCacheManager.GetAndCacheParameters(onError: ex => this.UserDialogs.Alert(ex.ToString()));

                if (initialData != null && initialData.Item1 != null && initialData.Item2 != null)
                {
                    var initCommunication = initialData.Item1;
                    var initParameters = initialData.Item2;
                    var initLogs = initialData.Item3;

                    if (initCommunication != null)
                    {
                        INavigationParameters parameters = new NavigationParameters();

                        if (initCommunication.ProfileId == ExternalComProfileValue.Dismessa)
                        {
                            var model = new InitialStartupWizardModel();

                            //TODO this is taking too long
                            var modelResult = _configurationControlUnitManager.MapParametersToWizardModel(model, initParameters);
                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            model = modelResult.Item1;
                            model.SelectedDeviceUuid = connectedDevice.Uuid;

                            var logRootModel = new ControlUnitLogRootModel();
                            logRootModel = _logControlUnitManager.MapParametersToWizardModel(logRootModel, initLogs);

                            parameters.Add(NavigationConstants.ControlUnitLogRootModel, logRootModel);
                            parameters.Add(NavigationConstants.InitialStartupWizardModel, model);
                            parameters.Add(NavigationConstants.ParameterCharacteristics, modelResult.Item2);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);

                            string navigationPath = $"{ViewNames.InitialStartupForm1CustomerInfoPage}";
                            NavigationItemModel gatewayParam = new NavigationItemModel()
                            {
                                NavigationPath = navigationPath,
                                Parameters = parameters,
                                IsModalNavigation = true
                            };
                            this.EventAggregator.GetEvent<GatewayParametersEvent>().Publish(gatewayParam);

                            this.UserDialogs.HideLoading();
                            return;
                        }
                        else if (initCommunication.ProfileId == ExternalComProfileValue.ModulA)
                        {
                            var model = new PumpConfigurationModulAWizardModel();

                            //TODO this is taking too long
                            var modelResult = _configurationPumpModulAManager.MapParametersToWizardModel(model, initParameters);
                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            model = modelResult.Item1;
                            model.SelectedDeviceUuid = connectedDevice.Uuid;

                            var cockpitPumpRootModel = new CockpitPumpsModulARootModel();
                            var rootModelResult = _cockpitPumpsModulAManager.MapParametersToModel(cockpitPumpRootModel, initParameters);
                            cockpitPumpRootModel = rootModelResult.Item1;

                            parameters.Add(NavigationConstants.PumpConfigurationModulAWizardModel, model);
                            parameters.Add(NavigationConstants.ParameterCharacteristics, modelResult.Item2);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);
                            parameters.Add(NavigationConstants.Logs, initLogs);
                            parameters.Add(NavigationConstants.CockpitPumpsModulARootModel, cockpitPumpRootModel);

                            string navigationPath = $"{ViewNames.PumpConfigurationModulAForm1Page}";
                            NavigationItemModel gatewayParam = new NavigationItemModel()
                            {
                                NavigationPath = navigationPath,
                                Parameters = parameters,
                                IsModalNavigation = true
                            };
                            this.EventAggregator.GetEvent<GatewayParametersEvent>().Publish(gatewayParam);
                            this.UserDialogs.HideLoading();
                            return;
                        }
                        else if (initCommunication.ProfileId == ExternalComProfileValue.VariA)
                        {
                            var model = new PumpConfigurationVariAWizardModel();

                            //TODO this is taking too long
                            var modelResult = _configurationPumpVariAManager.MapParametersToWizardModel(model, initParameters);
                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            model = modelResult.Item1;
                            model.SelectedDeviceUuid = connectedDevice.Uuid;

                            var cockpitPumpRootModel = new CockpitPumpsVariAERootModel();
                            var rootModelResult = _cockpitPumpsVariAEManager.MapParametersToModel(cockpitPumpRootModel, initParameters);
                            cockpitPumpRootModel = rootModelResult.Item1;

                            parameters.Add(NavigationConstants.PumpConfigurationVariAWizardModel, model);
                            parameters.Add(NavigationConstants.ParameterCharacteristics, modelResult.Item2);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);
                            parameters.Add(NavigationConstants.Logs, initLogs);
                            parameters.Add(NavigationConstants.CockpitPumpsVariAERootModel, cockpitPumpRootModel);

                            string navigationPath = $"{ViewNames.PumpConfigurationVariAForm1Page}";
                            NavigationItemModel gatewayParam = new NavigationItemModel()
                            {
                                NavigationPath = navigationPath,
                                Parameters = parameters,
                                IsModalNavigation = true
                            };
                            this.EventAggregator.GetEvent<GatewayParametersEvent>().Publish(gatewayParam);

                            this.UserDialogs.HideLoading();
                            return;
                        }
                    }
                    else
                    {
                        this.UserDialogs.HideLoading();
                        await this.UserDialogs.AlertAsync(AppResources.Alert_Message_CantGetParameters, okText: AppResources.Alert_Message_BLE_Permission_Ok);
                    }
                }
            }
            catch (Exception ex)
            {
                var properties = new Dictionary<string, string> {
                    { "exception", ex.Message},
                    { "viewModel", nameof(GatewaySubPageViewModel) },
                    { "method", "GatewaySubPageNavigation"},
                };

                this.AppCenterService.TrackError(new Exception(ex.Message), properties);
            }
            finally
            {
                this.UserDialogs.HideLoading();
            }
        }

        private async Task CockpitNavigationAsync()
        {
            try
            {
                this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);

                //TODO this GetAndCacheParameters is taking too long
                var initialData = await _parameterDataCacheManager.GetAndCacheParameters(onError: ex => this.UserDialogs.Alert(ex.ToString()));

                if (initialData != null && initialData.Item1 != null && initialData.Item2 != null)
                {
                    var initCommunication = initialData.Item1;
                    var initParameters = initialData.Item2;
                    var initLogs = initialData.Item3;

                    if (initCommunication != null)
                    {
                        INavigationParameters parameters = new NavigationParameters();

                        if (initCommunication.ProfileId == ExternalComProfileValue.Dismessa)
                        {
                            var rootModel = new CockpitControlUnitRootModel();

                            //TODO this mapping is taking too long
                            var rootModelResult = _cockpitControlUnitManager.MapParamtersToModel(rootModel, initParameters);
                            var logRootModel = new ControlUnitLogRootModel();
                            logRootModel = _logControlUnitManager.MapParametersToWizardModel(logRootModel, initLogs);

                            rootModel = rootModelResult.Item1;
                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            rootModel.SelectedDeviceUuid = connectedDevice.Uuid;

                            parameters.Add(NavigationConstants.CockpitControlUnitRootModel, rootModel);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);
                            parameters.Add(NavigationConstants.ParameterCharacteristics, rootModelResult.Item2);
                            parameters.Add(NavigationConstants.ControlUnitLogRootModel, logRootModel);
                            parameters.Add(NavigationConstants.Logs, initLogs);

                            string navigationPath = $"{ViewNames.NavigationPage}/{ViewNames.CockpitControlUnitRootPage}";
                            NavigationItemModel gatewayParam = new NavigationItemModel()
                            {
                                NavigationPath = navigationPath,
                                Parameters = parameters,
                                IsModalNavigation = false
                            };
                            this.EventAggregator.GetEvent<GatewayParametersEvent>().Publish(gatewayParam);

                            this.UserDialogs.HideLoading();
                            return;
                        }
                        else if (initCommunication.ProfileId == ExternalComProfileValue.ModulA)
                        {
                            var rootModel = new CockpitPumpsModulARootModel();

                            //TODO this mapping is taking too long
                            var rootModelResult = _cockpitPumpsModulAManager.MapParametersToModel(rootModel, initParameters);
                            rootModel = rootModelResult.Item1;
                            var logRootModel = new PumpLogRootModel();
                            logRootModel = _logPumpManager.MapParametersToWizardModel(logRootModel, initLogs);
                            logRootModel.ProfileId = initCommunication.ProfileId;

                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            rootModel.SelectedDeviceUuid = connectedDevice.Uuid;

                            parameters.Add(NavigationConstants.CockpitPumpsModulARootModel, rootModel);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);
                            parameters.Add(NavigationConstants.ParameterCharacteristics, rootModelResult.Item2);
                            parameters.Add(NavigationConstants.PumpLogRootModel, logRootModel);
                            parameters.Add(NavigationConstants.Logs, initLogs);

                            string navigationPath = $"{ViewNames.NavigationPage}/{ViewNames.CockpitPumpsModulARootPage}";
                            NavigationItemModel gatewayParam = new NavigationItemModel()
                            {
                                NavigationPath = navigationPath,
                                Parameters = parameters,
                                IsModalNavigation = false
                            };
                            this.EventAggregator.GetEvent<GatewayParametersEvent>().Publish(gatewayParam);

                            this.UserDialogs.HideLoading();
                            return;
                        }
                        else if (initCommunication.ProfileId == ExternalComProfileValue.VariA)
                        {
                            var rootModel = new CockpitPumpsVariAERootModel();

                            //TODO this mapping is taking too long
                            var rootModelResult = _cockpitPumpsVariAEManager.MapParametersToModel(rootModel, initParameters);
                            rootModel = rootModelResult.Item1;
                            var logRootModel = new PumpLogRootModel();
                            logRootModel = _logPumpManager.MapParametersToWizardModel(logRootModel, initLogs);
                            logRootModel.ProfileId = initCommunication.ProfileId;

                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            rootModel.SelectedDeviceUuid = connectedDevice.Uuid;

                            parameters.Add(NavigationConstants.CockpitPumpsVariAERootModel, rootModel);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);
                            parameters.Add(NavigationConstants.ParameterCharacteristics, rootModelResult.Item2);
                            parameters.Add(NavigationConstants.PumpLogRootModel, logRootModel);
                            parameters.Add(NavigationConstants.Logs, initLogs);

                            string navigationPath = $"{ViewNames.NavigationPage}/{ViewNames.CockpitPumpsVariAERootPage}";
                            NavigationItemModel gatewayParam = new NavigationItemModel()
                            {
                                NavigationPath = navigationPath,
                                Parameters = parameters,
                                IsModalNavigation = false
                            };
                            this.EventAggregator.GetEvent<GatewayParametersEvent>().Publish(gatewayParam);

                            this.UserDialogs.HideLoading();
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var properties = new Dictionary<string, string> {
                    { "exception", ex.Message},
                    { "viewModel", nameof(GatewaySubPageViewModel) },
                    { "method", "CockpitNavigation"},
                };

                this.AppCenterService.TrackError(new Exception(ex.Message), properties);
            }
            finally
            {
                this.UserDialogs.HideLoading();
            }
        }

        private async Task LogNavigationAsync()
        {
            try
            {
                this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
                var initialData = await _parameterDataCacheManager.GetAndCacheParameters(onError: ex => this.UserDialogs.Alert(ex.ToString()));

                if (initialData != null && initialData.Item1 != null && initialData.Item2 != null)
                {
                    var initCommunication = initialData.Item1;
                    var initParameters = initialData.Item2;
                    var initLogs = initialData.Item3;

                    if (initCommunication != null)
                    {
                        INavigationParameters parameters = new NavigationParameters();
                        if (initCommunication.ProfileId == ExternalComProfileValue.Dismessa)
                        {
                            var rootModel = new CockpitControlUnitRootModel();
                            var rootModelResult = _cockpitControlUnitManager.MapParamtersToModel(rootModel, initParameters);
                            var logRootModel = new ControlUnitLogRootModel();
                            logRootModel = _logControlUnitManager.MapParametersToWizardModel(logRootModel, initLogs);

                            rootModel = rootModelResult.Item1;
                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            rootModel.SelectedDeviceUuid = connectedDevice.Uuid;

                            parameters.Add(NavigationConstants.CockpitControlUnitRootModel, rootModel);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);
                            parameters.Add(NavigationConstants.ParameterCharacteristics, rootModelResult.Item2);
                            parameters.Add(NavigationConstants.ControlUnitLogRootModel, logRootModel);
                            parameters.Add(NavigationConstants.Logs, initLogs);

                            string navigationPath = $"{ViewNames.NavigationPage}/{ViewNames.ControlUnitLogRootPage}";
                            NavigationItemModel gatewayParam = new NavigationItemModel()
                            {
                                NavigationPath = navigationPath,
                                Parameters = parameters,
                                IsModalNavigation = false
                            };
                            this.EventAggregator.GetEvent<GatewayParametersEvent>().Publish(gatewayParam);

                            this.UserDialogs.HideLoading();
                            return;
                        }
                        else if (initCommunication.ProfileId == ExternalComProfileValue.ModulA || initCommunication.ProfileId == ExternalComProfileValue.VariA)
                        {
                            var logRootModel = new PumpLogRootModel();
                            logRootModel = _logPumpManager.MapParametersToWizardModel(logRootModel, initLogs);
                            logRootModel.ProfileId = initCommunication.ProfileId;

                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            logRootModel.SelectedDeviceUuid = connectedDevice.Uuid;

                            parameters.Add(NavigationConstants.PumpLogRootModel, logRootModel);
                            parameters.Add(NavigationConstants.Logs, initLogs);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);

                            if (logRootModel.ProfileId == ExternalComProfileValue.ModulA)
                            {
                                var rootModel = new CockpitPumpsModulARootModel();
                                var rootModelResult = _cockpitPumpsModulAManager.MapParametersToModel(rootModel, initParameters);
                                rootModel = rootModelResult.Item1;

                                parameters.Add(NavigationConstants.CockpitPumpsModulARootModel, rootModel);
                                parameters.Add(NavigationConstants.ParameterCharacteristics, rootModelResult.Item2);

                            }
                            else if (logRootModel.ProfileId == ExternalComProfileValue.VariA)
                            {
                                var rootModel = new CockpitPumpsVariAERootModel();
                                var rootModelResult = _cockpitPumpsVariAEManager.MapParametersToModel(rootModel, initParameters);
                                rootModel = rootModelResult.Item1;

                                parameters.Add(NavigationConstants.CockpitPumpsVariAERootModel, rootModel);
                                parameters.Add(NavigationConstants.ParameterCharacteristics, rootModelResult.Item2);
                            }

                            string navigationPath = $"{ViewNames.NavigationPage}/{ViewNames.PumpLogRootPage}";
                            NavigationItemModel gatewayParam = new NavigationItemModel()
                            {
                                NavigationPath = navigationPath,
                                Parameters = parameters,
                                IsModalNavigation = false
                            };
                            this.EventAggregator.GetEvent<GatewayParametersEvent>().Publish(gatewayParam);

                            this.UserDialogs.HideLoading();
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var properties = new Dictionary<string, string> {
                    { "exception", ex.Message},
                    { "viewModel", nameof(GatewaySubPageViewModel) },
                    { "method", "LogNavigation"},
                };

                this.AppCenterService.TrackError(new Exception(ex.Message), properties);
            }
            finally
            {
                this.UserDialogs.HideLoading();
            }
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            this.AppCenterService.TrackEvent($"{ViewNames.GatewaySubPage} - OnNavigatedTo");

            base.OnNavigatedTo(parameters);

            this.Gateway = new GatewayDeviceItemModel();
            if (parameters.ContainsKey(NavigationConstants.SelectedGatewayDevice))
            {
                this.Gateway = parameters.GetValue<GatewayDeviceItemModel>(NavigationConstants.SelectedGatewayDevice);
            }

            if (parameters.ContainsKey(NavigationConstants.PageNameParameter))
            {
                _pageNameParam = parameters.GetValue<string>(NavigationConstants.PageNameParameter);
            }

            this.Gateway.Device
               .WhenStatusChanged()
               .ObserveOn(RxApp.MainThreadScheduler)
               .Subscribe(async status => await DeviceConnectionStatusCallback(status))
               .DisposeWith(this.DestroyWith);

            this.ConnectToDeviceCommand?.Execute();
        }

        public async Task DeviceConnectionStatusCallback(ConnectionStatus status)
        {
            this.AppCenterService.TrackEvent($"{ViewNames.GatewaySubPage} - DeviceConnectionStatusCallback", new Dictionary<string, string>()
            {
                { "status", status.ToString() }
            });

            switch (status)
            {
                case ConnectionStatus.Connecting:
                    System.Diagnostics.Debug.WriteLine($"Connection: Connecting {DateTime.Now}");
                    break;

                case ConnectionStatus.Connected:
                    if (!_isAcquiringData)
                        await CheckDeviceIfAllowed();
                    System.Diagnostics.Debug.WriteLine($"Connection: Connected {DateTime.Now}");
                    break;

                case ConnectionStatus.Disconnected:
                    System.Diagnostics.Debug.WriteLine($"Connection: Disconnected {DateTime.Now}");
                    break;
            }
        }

        public override void Destroy()
        {
            this.AppCenterService.TrackEvent($"{ViewNames.GatewaySubPage} - Destroy");
            base.Destroy();
            this.DestroyWith?.Dispose();
            _adapter.StopScan();
        }
    }
}
