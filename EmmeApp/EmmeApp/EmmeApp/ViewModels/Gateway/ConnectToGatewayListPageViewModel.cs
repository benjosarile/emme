﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Events;
using EmmeApp.ItemModels;
using EmmeApp.Localization.Resources;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmmeApp.ViewModels
{
    public class ConnectToGatewayListPageViewModel : ViewModelBase
	{
		public ConnectToGatewayListPageViewModel(INavigationService navigationService,
			INavigationHelperService navigationHelperService,
			IAppCenterCustomService appCenterService,
			IUserDialogs userDialogs, 
			IEventAggregator eventAggregator) : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
		{
			this.BackCommand = new DelegateCommand(async () => await OnBack());
			this.ToggleMenuCommand = new DelegateCommand(() => OnToggleMenu(), () => this.CanExecuteCommand).ObservesProperty(() => this.CanExecuteCommand);
			this.SelectGatewayCommand = new DelegateCommand<GatewayDeviceItemModel>(async (gateway) => await OnSelectGateway(gateway));
			this.TryAgainCommand = new DelegateCommand(async () => await OnTryAgain());
		}

        private string _pageNameParam = string.Empty;

        private bool _canExecuteCommand = true;
		public bool CanExecuteCommand
		{
			get => _canExecuteCommand;
			set => SetProperty(ref _canExecuteCommand, value);
		}

		private List<GatewayDeviceItemModel> _gatewayList;
		public List<GatewayDeviceItemModel> GatewayList
		{
			get => _gatewayList;
			set => SetProperty(ref _gatewayList, value);
		}

		private string _devicesFound = string.Empty;
		public string DevicesFound
		{
			get => _devicesFound;
			set => SetProperty(ref _devicesFound, value);
		}

		private string _helpResourceKey;
		public string HelpResourceKey
		{
			get => _helpResourceKey;
			set => SetProperty(ref _helpResourceKey, value);
		}

		public DelegateCommand BackCommand { get; private set; }
		public DelegateCommand ToggleMenuCommand { get; private set; }
		public DelegateCommand<GatewayDeviceItemModel> SelectGatewayCommand { get; private set; }
		public DelegateCommand TryAgainCommand { get; private set; }

		private async Task OnBack()
		{
			this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - ExecuteBackCommand");

			var parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.ShouldExitConnection, string.Empty);
            parameters.Add(NavigationConstants.PageNameParameter, this._pageNameParam);

            var navigationResult = await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);

			if (!navigationResult.Success)
			{
				throw navigationResult.Exception;
			}
		}

		private void OnToggleMenu()
		{
			this.CanExecuteCommand = false;
			this.EventAggregator.GetEvent<HamburgerTappedEvent>().Publish();
			this.CanExecuteCommand = true;
		}

		private async Task OnSelectGateway(GatewayDeviceItemModel gateway)
		{
			this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - ExecuteSelectGatewayCommand", new Dictionary<string, string>()
			{
				{ "DeviceUUid", gateway.DeviceUuid}
			});

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.SelectedGatewayDevice, gateway);
            parameters.Add(NavigationConstants.PageNameParameter, this._pageNameParam);
            var s = await NavigationUtilities.NavigateAsync(this.NavigationService, ViewNames.GatewaySubPage, parameters, true, false);

			if (!s.Success)
			{
				throw s.Exception;
			}
		}

		private async Task OnTryAgain()
		{
			this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - ExecuteTryAgainCommand");
			await NavigationUtilities.GoBackAsync(this.NavigationService, null, null, false);
		}

		public override async void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);

			this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - OnNavigatedTo");

			if (parameters.ContainsKey(NavigationConstants.ShouldExitConnection))
			{
				this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - OnNavigatedTo", new Dictionary<string, string>()
				{
					{ AnalyticsConstants.Behavior, "ShouldExitConnection: true" }
				});

				await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
				return;
			}

            if (parameters.ContainsKey(NavigationConstants.PageNameParameter))
            {
                _pageNameParam = parameters.GetValue<string>(NavigationConstants.PageNameParameter);
            }

            var navigationMode = this.NavigationHelperService.GetNavigationMode(parameters);

			if (navigationMode == NavigationMode.New)
			{
				this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - OnNavigatedTo", new Dictionary<string, string>()
				{
					{ AnalyticsConstants.Behavior, "NavigationMode: New" }
				});

				this.GatewayList = new List<GatewayDeviceItemModel>();
				if (parameters.ContainsKey(NavigationConstants.GatewayConnectionListModel))
				{
					this.GatewayList = parameters.GetValue<List<GatewayDeviceItemModel>>(NavigationConstants.GatewayConnectionListModel);
				}

				this.DevicesFound = $"({this.GatewayList.Count}) {AppResources.ConnectToGateway_Devices_Found_Label}";

				if (this.GatewayList.Count != 0)
				{
					this.HelpResourceKey = ResourceConstants.Connect_To_Gateway_Devices_Found_Help;
				}
				else
				{
					this.HelpResourceKey = ResourceConstants.Connect_To_Gateway_No_Devices_Found_Help;
				}
			}
		}
	}
}
