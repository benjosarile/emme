﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.ItemModels;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Plugin.BluetoothLE;
using Plugin.Geolocator;
using Prism.Commands;
using Prism.Navigation;
using ReactiveUI;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EmmeApp.ViewModels
{
    public class ConnectToGatewayPageViewModel : ViewModelBase
    {
        private readonly IAdapter _adapter;
        private readonly IAsyncDelayer _asyncDelayer;
        private readonly IBleManager _bleManager;
        private IDisposable _scan;
        private IDisposable _timer;
        private IDisposable _connectionStatus;

        private readonly IPopupNavigation _popupNavigation;

        public ConnectToGatewayPageViewModel(
            INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IUserDialogs userDialogs,
            IPopupNavigation popupNavigation,
            IAsyncDelayer asyncDelayer,
            IAdapter adapter,
            IBleManager bleManager) : base(navigationService, navigationHelperService, appCenterService, userDialogs)
        {
            _adapter = adapter;
            _asyncDelayer = asyncDelayer;
            _bleManager = bleManager;
            _popupNavigation = popupNavigation;
            this.ScanCommand = new DelegateCommand(async () => await OnScan());
        }

        CompositeDisposable _deactivateWith;
        private CompositeDisposable DeactivateWith
        {
            get
            {
                if (_deactivateWith == null)
                {
                    _deactivateWith = new CompositeDisposable();
                }

                return _deactivateWith;
            }
        }

        private string _pageNameParam = string.Empty;

        private bool _isScanning;
        public bool IsScanning
        {
            get => _isScanning;
            set => SetProperty(ref _isScanning, value);
        }

        private List<GatewayDeviceItemModel> _devices;
        public List<GatewayDeviceItemModel> Devices
        {
            get => _devices;
            set => SetProperty(ref _devices, value);
        }

        private GatewayDeviceItemModel _selectedDevice;
        public GatewayDeviceItemModel SelectedDevice
        {
            get => _selectedDevice;
            set => SetProperty(ref _selectedDevice, value);
        }


        public DelegateCommand ScanCommand { get; private set; }

        private async Task OnScan()
        {
            if (_adapter.Status == AdapterStatus.PoweredOn && !this.IsScanning)
            {
                this.IsScanning = true;

                this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - ExecuteScanCommand");
                DisposeSubscription();
                StopScanning();
                if (!(await SetupBLEPermissions()))
                {
                    await CloseConnectionPage();
                    return;
                }
                StartTimer();
                ScanDevices();
            }
        }

        private async Task<bool> SetupBLEPermissions()
        {
            var crossPermissions = Plugin.Permissions.CrossPermissions.Current;
            var crossGeolocator = CrossGeolocator.Current;

            if (Device.RuntimePlatform == Device.Android &&
                    (!crossGeolocator.IsGeolocationAvailable ||
                    !crossGeolocator.IsGeolocationEnabled ||
                    (await crossPermissions.CheckPermissionStatusAsync(Plugin.Permissions.Abstractions.Permission.Location)) != Plugin.Permissions.Abstractions.PermissionStatus.Granted))
            {
                while (true)
                {
                    if (!crossGeolocator.IsGeolocationAvailable || !crossGeolocator.IsGeolocationEnabled)
                    {
                        var isConfirmed =
                            await UserDialogs.ConfirmAsync(AppResources.ConnectToGateway_GPS_Needed, okText: AppResources.Alert_Message_Proceed, cancelText: AppResources.Alert_Message_Cancel);

                        await crossPermissions.RequestPermissionsAsync(Plugin.Permissions.Abstractions.Permission.Location);

                        if (!isConfirmed)
                            return false;

                        continue;
                    }

                    if ((await crossPermissions.CheckPermissionStatusAsync(Plugin.Permissions.Abstractions.Permission.Location)) != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                    {
                        var permissionRequestResult =
                            await crossPermissions.RequestPermissionsAsync(Plugin.Permissions.Abstractions.Permission.Location);

                        if (permissionRequestResult[Plugin.Permissions.Abstractions.Permission.Location] != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                            continue;
                    }

                    if (crossGeolocator.IsGeolocationAvailable && crossGeolocator.IsGeolocationEnabled
                        && (await crossPermissions.CheckPermissionStatusAsync(Plugin.Permissions.Abstractions.Permission.Location)) == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                    {
                        break;
                    }
                }
            }

            return true;
        }

        private void StartTimer()
        {
            this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - StartTimer");

            _timer = Observable
                .Timer(TimeSpan.FromSeconds(5))
                .Select(_ => Observable.FromAsync(async ct =>
                {
                    await TimerElapsedCallback();
                }))
                .Merge(3)
                .Subscribe();
        }

        public async Task TimerElapsedCallback()
        {
            await _asyncDelayer.Delay(10);
            StopScanning();
            if (!this.IsScanning)
            {
                NavigateToGatewayListPage();
            }
        }

        private void StopScanning()
        {
            this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - StopScanning");

            if (_adapter.IsScanning)
            {
                _adapter.StopScan();
                this.IsScanning = false;
                System.Diagnostics.Debug.WriteLine($"Stop:{DateTime.Now}");
            }
        }

        private void DisposeSubscription()
        {
            this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - DisposeSubscription");

            System.Diagnostics.Debug.WriteLine($"Disposing:{DateTime.Now}");
            _scan?.Dispose();
            _scan = null;
            _timer?.Dispose();
            _timer = null;
            _connectionStatus?.Dispose();
            _connectionStatus = null;
        }

        private void ScanDevices()
        {
            this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - ScanDevices");

            if (!_adapter.IsScanning)
            {
                System.Diagnostics.Debug.WriteLine($"Start:{DateTime.Now}");
                this.Devices = new List<GatewayDeviceItemModel>();
                this.IsScanning = true;

                var bleAvailableServices = _bleManager.GetAvailableServices();

                this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - ScanDevices", new Dictionary<string, string>()
                {
                    { AnalyticsConstants.Behavior, $"BLE Available Services Count: {bleAvailableServices.Count}" }
                });

                _scan = _adapter
                    .Scan()
                    .Buffer(TimeSpan.FromSeconds(1))
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe(
                        results => ScanResultCallback(results),
                        ex => this.UserDialogs.Alert(ex.ToString(), "ERROR")
                    );
            }
        }

        public void ScanResultCallback(IList<IScanResult> results)
        {
            if (!results.Any())
                return;

            var list = new List<GatewayDeviceItemModel>();
            foreach (var result in results)
            {
                try
                {
                    var deviceName = result.Device.Name;
                    if (string.IsNullOrEmpty(deviceName))
                    {
                        var message = $"Empty device name found: {result.Device.Uuid}";
                        System.Diagnostics.Debug.WriteLine(message);

                        var properties = new Dictionary<string, string> {
                            { "exception", message},
                            { "viewModel", nameof(ConnectToGatewayPageViewModel) },
                            { "method", "ScanResultCallback"},
                        };

                        this.AppCenterService.TrackError(new Exception(message), properties);

                        if (!result.AdvertisementData.ManufacturerData.Any())
                        {
                            continue;
                        }

                        if (result.AdvertisementData.ManufacturerData[0] != 0xC0 && result.AdvertisementData.ManufacturerData[1] != 0x07)
                        {
                            continue;
                        }
                    }
                }
                catch
                {
                    //Somehow iOS cannot lookup the Manufaturing details and will crash
                    continue;
                }

                //This is added to remove duplicates
                var dev = this.Devices.FirstOrDefault(x => x.Uuid.Equals(result.Device.Uuid));

                if (dev != null)
                {
                    dev.TrySet(result);
                }
                else
                {
                    dev = new GatewayDeviceItemModel();
                    dev.TrySet(result);

                    list.Add(dev);
                }
            }

            list = list.GroupBy(x => x.Uuid).Select(x => x.FirstOrDefault()).ToList();

            if (list.Any())
            {
                #if !DEBUG
                    list = list.Where(x => x.ManufacturerData[0] == 0xC0 && x.ManufacturerData[1] == 0x07).ToList();
                #endif

                this.Devices = new List<GatewayDeviceItemModel>(list);
            }
        }

        private async Task SubscribeConnectionStatus()
        {
            this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - SubscribeConnectionStatus");

            var connectedDevice = await _bleManager.GetConnectedDevice();

            if (connectedDevice != null)
            {
                await this.UserDialogs.AlertAsync(AppResources.Alert_Message_DeviceAlreadyConnected);
                await CloseConnectionPage();
                return;
            }

            _connectionStatus?.Dispose();
            _connectionStatus =
                _adapter
                    .WhenStatusChanged()
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe(status => ConnectionStatusCallback(status));

        }

        public void ConnectionStatusCallback(AdapterStatus status)
        {
            if (status == AdapterStatus.PoweredOn)
            {
                this.ScanCommand?.Execute();
            }
        }

        private void NavigateToGatewayListPage()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                INavigationParameters parameters = new NavigationParameters();
                parameters.Add(NavigationConstants.GatewayConnectionListModel, this.Devices);
                parameters.Add(NavigationConstants.PageNameParameter, this._pageNameParam);
                await NavigationUtilities.NavigateAsync(this.NavigationService, $"{ViewNames.ConnectToGatewayListPage}", parameters, null, false);
            });
            DisposeSubscription();
        }

        private async Task CloseConnectionPage()
        {
            this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - CloseConnectionPage");
            await NavigationUtilities.GoBackAsync(this.NavigationService, null, null, false);
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - OnNavigatedTo");

            if (parameters.ContainsKey(NavigationConstants.ShouldExitConnection))
            {
                await CloseConnectionPage();
                return;
            }

            if (parameters.ContainsKey(NavigationConstants.PageNameParameter))
            {
                _pageNameParam = parameters.GetValue<string>(NavigationConstants.PageNameParameter);
            }


            bool shouldProceed = true;
            if (_adapter.Status != AdapterStatus.PoweredOn)
            {
                this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - OnNavigatedTo", new Dictionary<string, string>()
                {
                    { AnalyticsConstants.Behavior, "Adapter Status: Off" }
                });

                if (_adapter.CanControlAdapterState())
                {
                    var cResult = await this.UserDialogs.ConfirmAsync(new ConfirmConfig()
                    {
                        OkText = AppResources.Alert_Message_BLE_Permission_Accept,
                        CancelText = AppResources.Alert_Message_BLE_Permission_Deny,
                        Message = AppResources.Alert_Message_BLE_Permission
                    });

                    if (cResult)
                    {
                        this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - OnNavigatedTo", new Dictionary<string, string>()
                        {
                            { AnalyticsConstants.Behavior, "SetAdapterState" }
                        });

                        _adapter.SetAdapterState(true);
                    }

                    shouldProceed = cResult;
                }
                else
                {
                    shouldProceed = false;

                    await this.UserDialogs.AlertAsync(new AlertConfig()
                    {
                        OkText = AppResources.Alert_Message_BLE_Permission_Ok,
                        Message = AppResources.Alert_Message_BLE_Permission_OpenSetting
                    });
                }
            }

            if (shouldProceed)
            {
                this.Devices = new List<GatewayDeviceItemModel>();
                await SubscribeConnectionStatus();
            }
            else
            {
                await CloseConnectionPage();
            }


        }

        public override void Destroy()
        {
            this.AppCenterService.TrackEvent($"{ViewNames.ConnectToGatewayPage} - Destroy");

            DisposeSubscription();
        }
    }
}
