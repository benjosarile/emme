﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.Configuration;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities.External;
using EmmeApp.Events;
using EmmeApp.Helpers;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Humanizer;
using Plugin.BluetoothLE;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace EmmeApp.ViewModels
{
    public class PumpConfigurationVariAForm1PageViewModel : ViewModelBase, IChainModalViewModel
    {
        private readonly IEnumProvider _enumHelper;
        private readonly IPopupNavigation _popupNavigation;
        private readonly IKeyboardHelper _keyboardHelper;
        private readonly IUnixTimestampService _unixTimestampService;
        private readonly IConfigurationPumpVariAManager _configurationPumpVariAManager;
        private readonly IBleManager _bleManager;
        private readonly ICockpitPumpsVariAEManager _cockpitPumpsVariAEManager;
        private readonly ILogPumpManager _logPumpManager;

        private DialogAlertSingle _lostConnectionAlertDialog = null;
        private bool _isIntentionallyReconnecting = false;
        private readonly SemaphoreSlim _connectionManagementThrottle = new SemaphoreSlim(1, 1);

        private ModalChainStack _configModalStack = null;

        public PumpConfigurationVariAForm1PageViewModel(INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IEnumProvider enumHelper,
            IPopupNavigation popupNavigation,
            IKeyboardHelper keyboardHelper,
            IUnixTimestampService unixTimestampService,
            IBleManager bleManager,
            IConfigurationPumpVariAManager configurationPumpVariAManager,
            ICockpitPumpsVariAEManager cockpitPumpsVariAEManager,
            ILogPumpManager logPumpManager)
            : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
        {
            _enumHelper = enumHelper;
            _popupNavigation = popupNavigation;
            _keyboardHelper = keyboardHelper;
            _unixTimestampService = unixTimestampService;
            _configurationPumpVariAManager = configurationPumpVariAManager;
            _bleManager = bleManager;
            _cockpitPumpsVariAEManager = cockpitPumpsVariAEManager;
            _logPumpManager = logPumpManager;

            _configModalStack = new ModalChainStack().ChainPush(this);

            this.WizardCloseCommand = new DelegateCommand(async () => await OnWizardClose());
            this.WizardLeftButtonCommand = new DelegateCommand(async () => await OnWizardClose());
            this.WizardRightButtonCommand = new DelegateCommand(async () => await OnNextButton());
            this.SaveToPumpCommand = new DelegateCommand<object>(async (parameterId) => await OnSaveToPump(parameterId));

            EventAggregator.GetEvent<ConnectionEstablishedEvent>().Subscribe(OnConnectionEstablished);
            EventAggregator.GetEvent<ConnectionLostEvent>().Subscribe(OnConnectionLost);
        }

        private List<byte[]> _bleParameterData;
        private List<byte[]> _bleParameterBufferData;
        private bool _shouldAcquireNewData = false;
        private bool _isInitializing = true;
        private bool _isUpdating = false;
        private IDisposable _notifyDisposable;
        private IDisposable _deviceStatusDisposable;
        private List<LogEntryTExternalEntity> _logs;
        private ParameterTExternalEntity _initialNominalParameterValue;

        public CockpitPumpsVariAERootModel CockpitPumpsRootModel { get; set; }

        private string _wizardTitle = AppResources.PumpConfiguration_ModulA_Title;
        public string WizardTitle
        {
            get => _wizardTitle;
            set => SetProperty(ref _wizardTitle, value);
        }

        private int _wizardStepNumber = 1;
        public int WizardStepNumber
        {
            get => _wizardStepNumber;
            set => SetProperty(ref _wizardStepNumber, value);
        }

        private string _wizardFooterLeftLabel = AppResources.InitialStartup_Wizard_Cancel;
        public string WizardFooterLeftLabel
        {
            get => _wizardFooterLeftLabel;
            set => SetProperty(ref _wizardFooterLeftLabel, value);
        }
        private string _wizardFooterRightLabel = AppResources.InitialStartup_Wizard_Next;
        public string WizardFooterRightLabel
        {
            get => _wizardFooterRightLabel;
            set => SetProperty(ref _wizardFooterRightLabel, value);
        }
        private string _wizardErrorMessage;
        public string WizardErrorMessage
        {
            get => _wizardErrorMessage;
            set => SetProperty(ref _wizardErrorMessage, value);
        }
        private bool _wizardErrorMessageVisibility = false;
        public bool WizardErrorMessageVisibility
        {
            get => _wizardErrorMessageVisibility;
            set => SetProperty(ref _wizardErrorMessageVisibility, value);
        }

        private string _helpResourceKey = ResourceConstants.Pump_Configuration_Form1_Help;
        public string HelpResourceKey
        {
            get => _helpResourceKey;
            set => SetProperty(ref _helpResourceKey, value);
        }

        private List<ParameterPumpVariACharacteristicExternalEntity> _parameterCharacteristicList;
        public List<ParameterPumpVariACharacteristicExternalEntity> ParameterCharacteristicList
        {
            get => _parameterCharacteristicList;
            set => SetProperty(ref _parameterCharacteristicList, value);
        }

        private PumpConfigurationVariAWizardModel _wizardModel;
        public PumpConfigurationVariAWizardModel WizardModel
        {
            get => _wizardModel;
            set => SetProperty(ref _wizardModel, value);
        }

        private List<ParameterTExternalEntity> _initialParameters;
        public List<ParameterTExternalEntity> InitialParameters
        {
            get => _initialParameters;
            set => SetProperty(ref _initialParameters, value);
        }

        private TimeSpan _variATimePicker = DateTimeOffset.FromUnixTimeSeconds(1549276713).LocalDateTime.TimeOfDay;
        public TimeSpan VariATimePicker
        {
            get => _variATimePicker;
            set
            {
                if (!this.VariATimePicker.Hours.Equals(value.Hours) || !this.VariATimePicker.Minutes.Equals(value.Minutes))
                {
                    ValidateParameterValueChanged();
                }
                SetProperty(ref _variATimePicker, value);
                SetTime();
            }
        }

        private DateTime _variADatePicker = DateTimeOffset.FromUnixTimeSeconds(1549276713).LocalDateTime.Date;
        public DateTime VariADatePicker
        {
            get => _variADatePicker;
            set
            {
                if (!this.VariADatePicker.Month.Equals(value.Month) || !this.VariADatePicker.Day.Equals(value.Day) || !this.VariADatePicker.Year.Equals(value.Year))
                {
                    ValidateParameterValueChanged();
                }
                SetProperty(ref _variADatePicker, value);
                SetTime();
            }
        }

        private string _selectedControlType;
        public string SelectedControlType
        {
            get => _selectedControlType;
            set
            {
                ParameterValueChanged(value, this.SelectedControlType);
                SetControlType(value);
                SetProperty(ref _selectedControlType, value);
            }
        }

        private List<string> _controlTypeList;
        public List<string> ControlTypeList
        {
            get => _controlTypeList;
            set => SetProperty(ref _controlTypeList, value);
        }

        private string _selectedTypeofOperation;
        public string SelectedTypeofOperation
        {
            get => _selectedTypeofOperation;
            set
            {
                ParameterValueChanged(value, this.SelectedTypeofOperation);
                SetTypeOfOperation(value);
                SetProperty(ref _selectedTypeofOperation, value);
            }
        }

        private List<string> _typeofOperationList;
        public List<string> TypeofOperationList
        {
            get => _typeofOperationList;
            set => SetProperty(ref _typeofOperationList, value);
        }

        private List<string> _pumpNumberList;
        public List<string> PumpNumberList
        {
            get => _pumpNumberList;
            set => SetProperty(ref _pumpNumberList, value);
        }

        private bool _keyboardLock = false;
        public bool KeyboardLock
        {
            get => _keyboardLock;
            set
            {
                if (this.WizardModel != null && !_isInitializing)
                {
                    ParameterValueChanged(value, this.KeyboardLock);
                    SetKeyLock(value);
                }

                SetProperty(ref _keyboardLock, value);
            }
        }

        private bool _signalOriginIsPump = true;
        public bool SignalOrigiIsPump
        {
            get => _signalOriginIsPump;
            set => SetProperty(ref _signalOriginIsPump, value);
        }

        private bool _isSetPointEnabled = false;
        public bool IsSetPointEnabled
        {
            get => _isSetPointEnabled;
            set => SetProperty(ref _isSetPointEnabled, value);
        }

        private string _nominalValueUnit = AppResources.WizardForm_UnitsInMeters;
        public string NominalValueUnit
        {
            get => _nominalValueUnit;
            set => SetProperty(ref _nominalValueUnit, value);
        }

        private double _nominalValueMax = 100;
        public double NominalValueMax
        {
            get => _nominalValueMax;
            set => SetProperty(ref _nominalValueMax, value);
        }

        private double _nominalValueMin;
        public double NominalValueMin
        {
            get => _nominalValueMin;
            set => SetProperty(ref _nominalValueMin, value);
        }

        private double _nominalValueStep = 0.1;
        public double NominalValueStep
        {
            get => _nominalValueStep;
            set => SetProperty(ref _nominalValueStep, value);
        }

        private double _nominalValue = 85;
        public double NominalValue
        {
            get => _nominalValue;
            set
            {
                if (this.WizardModel != null && !_isInitializing)
                {
                    ParameterValueChanged(value, this.NominalValue);
                    this.WizardModel.NominalValue = value;
                    this.SaveToPumpCommand?.Execute(ExternalPumpVariAParameterIdValue.NominalValue);
                }
                SetProperty(ref _nominalValue, value);
            }
        }

        private string _selectedPumpNumber;
        public string SelectedPumpNumber
        {
            get => _selectedPumpNumber;
            set
            {
                ParameterValueChanged(value, this.SelectedPumpNumber);
                SetPumpNumber(value);
                SetProperty(ref _selectedPumpNumber, value);
            }
        }

        public DelegateCommand WizardCloseCommand { get; private set; }
        public DelegateCommand WizardLeftButtonCommand { get; private set; }
        public DelegateCommand WizardRightButtonCommand { get; private set; }

        public DelegateCommand<object> SaveToPumpCommand { get; private set; }

        private async Task OnWizardClose()
        {
            _keyboardHelper.HideKeyboard();

            DialogAlert dialogAlert = new DialogAlert();
            dialogAlert.LeftButtonText = AppResources.PumpConfiguration_Wizard_Popup_Close_Back;
            dialogAlert.RightButtonText = AppResources.PumpConfiguration_Wizard_Popup_Close_Confirm;
            //dialogAlert.ContentText = AppResources.PumpConfiguration_Wizard_Popup_Close_Content;
            //TODO Change for 2169
            dialogAlert.ContentText = AppResources.InitialStartup_Wizard_Popup_Close_Content;
            dialogAlert.Title = AppResources.PumpConfiguration_Wizard_Popup_Close_Title;
            dialogAlert.RightButtonCommand = new DelegateCommand(async () => await CloseWizardPage());
            await _popupNavigation.PushAsync(dialogAlert, false);
        }

        private async Task OnNextButton()
        {
            _keyboardHelper.HideKeyboard();

            if (this.WizardModel != null)
            {
                _shouldAcquireNewData = false;

                this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
                await Task.Delay(100);
                var logRootModel = new PumpLogRootModel();
                logRootModel = _logPumpManager.MapParametersToWizardModel(logRootModel, _logs);

                this.WizardErrorMessage = string.Empty;
                this.WizardErrorMessageVisibility = false;

                INavigationParameters parameters = new NavigationParameters();
                parameters.Add(NavigationConstants.PumpConfigurationVariAWizardModel, this.WizardModel);
                parameters.Add(NavigationConstants.ParameterCharacteristics, this.ParameterCharacteristicList);
                parameters.Add(NavigationConstants.CockpitPumpsVariAERootModel, CockpitPumpsRootModel);
                parameters.Add(NavigationConstants.PumpLogRootModel, logRootModel);
                parameters.Add(NavigationConstants.ChainModalStack, _configModalStack);

                this.UserDialogs.HideLoading();
                await NavigationUtilities.NavigateAsync(this.NavigationService, ViewNames.PumpConfigurationVariAForm2SystemDescriptionPage, parameters, null, false);

            }
        }

        private async Task OnSaveToPump(object parameterId)
        {
            if (!_shouldAcquireNewData)
                return;

            var paramId = (ExternalPumpVariAParameterIdValue)parameterId;
            if (!this.WizardModel.IsParameterChanged || parameterId == null)
            {
                return;
            }

            this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
            var connectedDevice = await _bleManager.GetConnectedDevice();

            //attempt to reconnect.
            if (connectedDevice == null || connectedDevice.Status == ConnectionStatus.Disconnected)
            {
                connectedDevice = await Reconnect(this.WizardModel.SelectedDeviceUuid);
            }

            if (connectedDevice != null && connectedDevice.Status == ConnectionStatus.Connected)
            {
                var characteristic = await _bleManager.GetDeviceWriteCharacteristic(connectedDevice);

                if (characteristic != null)
                {
                    var parameterCharacteristicList = new List<ParameterPumpVariACharacteristicExternalEntity>(this.ParameterCharacteristicList);

                    if (paramId == ExternalPumpVariAParameterIdValue.NominalValue)
                    {
                        var param = parameterCharacteristicList.FirstOrDefault(x => x.VariA == ExternalPumpVariAParameterIdValue.NominalValue);

                        if (param != null && param.Scaling != null)
                        {
                            if (WizardModel.ControlType == PumpControlTypeValue.ConstantSpeed)
                            {
                                param.Scaling.Step = 1;
                            }
                            else
                            {
                                param.Scaling.Step = 0.1;
                            }
                        }
                    }

                    var parametersList = _configurationPumpVariAManager.MapWizardModelToParameters(this.WizardModel, parameterCharacteristicList, paramId);

                    bool isWriteSuccess = true;
                    foreach (var parameters in parametersList)
                    {
                        var parametersT = new ParametersTExternalEntity() { Parameters = parameters };
                        isWriteSuccess = await _bleManager.WriteCommand(parametersT, characteristic, ExternalIdOneofCaseValue.VariA);

                        if (!isWriteSuccess)
                        {
                            break;
                        }
                    }

                    if (isWriteSuccess)
                    {
                        this.UserDialogs.HideLoading();
                        this.WizardModel.IsParameterChanged = false;
                    }
                    else
                    {
                        this.UserDialogs.HideLoading();
                        await this.UserDialogs.AlertAsync(AppResources.Alert_Message_BLE_CannotWriteData);
                    }
                }
            }

            this.UserDialogs.HideLoading();
        }

        private async Task CloseWizardPage()
        {
            _shouldAcquireNewData = false;

            _keyboardHelper.HideKeyboard();

            SetMaxValuesToDefault();

            INavigationParameters parameters = new NavigationParameters();

            if (this.InitialParameters != null && this.InitialParameters.Any())
            {
                parameters.Add(NavigationConstants.InitialParameters, this.InitialParameters);
            }

            await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
        }

        private async Task<IDevice> Reconnect(Guid selectedDeviceUuid)
        {
            var connectedDevice = await _bleManager.GetKnownDevice(selectedDeviceUuid);

            if (connectedDevice != null)
            {
                _isIntentionallyReconnecting = true;

                var newDevice = connectedDevice;

                if (newDevice.Status == ConnectionStatus.Disconnected)
                {
                    var restartTheDeviceTask = Task.WhenAll(_bleManager.RestartTheDevice(connectedDevice));
                    newDevice = restartTheDeviceTask.Result[0];
                    await Task.Delay(2000);
                }

                if (newDevice != null && newDevice.Status == ConnectionStatus.Connected)
                {
                    var characteristic = await _bleManager.GetDeviceReadCharacteristic(newDevice);

                    if (characteristic != null)
                    {
                        _shouldAcquireNewData = false;

                        _bleParameterData = new List<byte[]>();
                        _bleParameterBufferData = new List<byte[]>();

                        int parameterCtr = 0;

                        _notifyDisposable?.Dispose();
                        _notifyDisposable =
                            characteristic
                                .WhenNotificationReceived()
                                .Subscribe(
                                    async result => { await ReadParameters(result); parameterCtr++; },
                                    ex => { }
                                );

                        _deviceStatusDisposable?.Dispose();
                        _deviceStatusDisposable =
                            newDevice
                                .WhenStatusChanged()
                                .Subscribe(
                                    (connectionStatus) =>
                                    {
                                        switch (connectionStatus)
                                        {
                                            case ConnectionStatus.Connected:
                                                OnConnectionEstablished();
                                                break;

                                            case ConnectionStatus.Disconnected:
                                                OnConnectionLost();
                                                break;
                                        }
                                    });

                        _shouldAcquireNewData = true;

                        if (!characteristic.IsNotifying)
                        {
                            await characteristic.EnableNotifications();
                        }

                        //TODO UserStory #2023
                        //Reduced maxRetryCount from 150 to 50
                        int maxRetryCount = 50;
                        int retryCount = 0;
                        while (parameterCtr < 50)
                        {
                            await Task.Delay(100);
                            retryCount++;

                            if (retryCount >= maxRetryCount)
                            {
                                newDevice = null;
                                break;
                            }
                        }
                    }
                    else
                    {
                        newDevice.CancelConnection();
                    }
                }

                connectedDevice = newDevice;
            }

            return connectedDevice;
        }

        private async void OnConnectionEstablished()
        {
            if (!(await _connectionManagementThrottle.WaitAsync(0)))
                return;

            _isIntentionallyReconnecting = false;

            await RemoveAlertForLostConnection();

            _connectionManagementThrottle.Release();
        }

        private async void OnConnectionLost()
        {
            if (!(await _connectionManagementThrottle.WaitAsync(0)))
                return;

            if (_isIntentionallyReconnecting)
            {
                _connectionManagementThrottle.Release();
                return;
            }

            await AlertUserOfLostConnection();

            _connectionManagementThrottle.Release();
        }

        private async Task AlertUserOfLostConnection()
        {
            if (_lostConnectionAlertDialog != null)
                return;

            UserDialogs.HideLoading();
            _lostConnectionAlertDialog = new DialogAlertSingle();
            _lostConnectionAlertDialog.ButtonText = AppResources.Alert_Message_OK;
            _lostConnectionAlertDialog.ContentText = AppResources.Alert_MessageBody_BLE_LostConnection;
            _lostConnectionAlertDialog.Title = AppResources.Alert_MessageHeader_BLE_LostConnection;
            _lostConnectionAlertDialog.ButtonCommand =
                new DelegateCommand(
                    async () =>
                    {
                        _lostConnectionAlertDialog = null;
                        await _configModalStack.Peek().CloseAsync();
                    });
            await _popupNavigation.PushAsync(_lostConnectionAlertDialog, false);
        }

        private async Task RemoveAlertForLostConnection()
        {
            if (_lostConnectionAlertDialog != null && _popupNavigation.PopupStack.Any(x => x == _lostConnectionAlertDialog))
            {
                await _popupNavigation.RemovePageAsync(_lostConnectionAlertDialog);
                _lostConnectionAlertDialog = null;
            }
        }

        private async Task ReadParameters(CharacteristicGattResult result)
        {
            if (_shouldAcquireNewData)
            {
                if (!_isUpdating && _bleParameterBufferData.Any())
                {
                    _isUpdating = true;

                    var bleParameterBufferData = new List<byte[]>(_bleParameterBufferData);
                    _bleParameterBufferData = new List<byte[]>();

                    var parameters = new List<ParameterTExternalEntity>();

                    foreach (var parameterData in bleParameterBufferData)
                    {
                        parameters.AddRange(_bleManager.GetInitializeParameters(parameterData));
                    }

                    foreach (var newParameter in parameters)
                    {
                        var parameter = this.InitialParameters.FirstOrDefault(x => x.VariA == newParameter.VariA);
                        if (parameter != null)
                        {
                            parameter.Value = newParameter.Value;

                            if (newParameter.Unit != ExternalUnitTValue.UnknownUnit)
                            {
                                parameter.Unit = newParameter.Unit;
                            }

                            if (newParameter.Scaling != null)
                            {
                                parameter.Scaling = newParameter.Scaling;
                            }

                            newParameter.Unit = parameter.Unit;
                            newParameter.Scaling = parameter.Scaling;
                        }
                    }

                    if (parameters.Any(x => x.VariA == ExternalPumpVariAParameterIdValue.SignalOrigin ||
                                            x.VariA == ExternalPumpVariAParameterIdValue.ControlType ||
                                            x.VariA == ExternalPumpVariAParameterIdValue.TypeOfOperation ||
                                            x.VariA == ExternalPumpVariAParameterIdValue.PumpNumber ||
                                            x.VariA == ExternalPumpVariAParameterIdValue.NominalValue ||
                                            x.VariA == ExternalPumpVariAParameterIdValue.KeyLock ||
                                            x.VariA == ExternalPumpVariAParameterIdValue.Time))
                    {

                        var modelResult = _configurationPumpVariAManager.MapParametersToWizardModel(this.WizardModel, parameters);

                        this.WizardModel = modelResult.Item1;
                        this.InitialParameters = parameters;

                        if (parameters.Any(x => x.VariA == ExternalPumpVariAParameterIdValue.NominalValue || x.VariA == ExternalPumpVariAParameterIdValue.ControlType))
                        {
                            SetMinimumMaximumValue();
                        }

                        if (parameters.Any(x => x.VariA == ExternalPumpVariAParameterIdValue.NominalValue) && _initialNominalParameterValue != null && _initialNominalParameterValue.Scaling != null)
                        {
                            var param = parameters.FirstOrDefault(x => x.VariA == ExternalPumpVariAParameterIdValue.NominalValue);

                            if (param.Value != null && WizardModel.ControlType != PumpControlTypeValue.ConstantSpeed)
                            {
                                this.WizardModel.NominalValue = param.Value.Number * 0.1;
                            }
                        }

                        _isInitializing = true;
                        InitializeParameters();
                        await Task.Delay(1000);
                        _isInitializing = false;
                    }

                    _isUpdating = false;
                }
                else
                {
                    if (result != null && result.Data != null)
                    {
                        var resultData = result.Data;
                        _bleParameterBufferData.Add(resultData);
                    }
                }

                if (!_isUpdating && _bleParameterBufferData.Any())
                {
                    await ReadParameters(null);
                }
            }
        }

        private void PopulateList()
        {
            this.ControlTypeList = _enumHelper.PopulateList<PumpControlTypeValue>()
                .Select(x => x.Humanize())
                .ToList();

            this.TypeofOperationList = _enumHelper.PopulateList<PumpTypeOfOperationValue>()
                .Select(x => x.Humanize())
                .ToList();

            this.PumpNumberList = new List<string>();
            Enumerable.Range(1, 64).ForEach(x => this.PumpNumberList.Add(x.ToString()));
        }

        private void SetControlType(string value)
        {
            if (this.WizardModel != null && !string.IsNullOrEmpty(value))
            {
                this.WizardModel.ControlType = EnumDehumanizeExtensions.DehumanizeTo<PumpControlTypeValue>(value);
                this.SaveToPumpCommand?.Execute(ExternalPumpVariAParameterIdValue.ControlType);
            }
        }

        private void SetTypeOfOperation(string value)
        {
            if (this.WizardModel != null && !string.IsNullOrEmpty(value))
            {
                this.WizardModel.TypeOfOperation = EnumDehumanizeExtensions.DehumanizeTo<PumpTypeOfOperationValue>(value);
                this.SaveToPumpCommand?.Execute(ExternalPumpVariAParameterIdValue.TypeOfOperation);
                SetSignalOriginCondition();
            }
        }

        private void SetSignalOriginCondition()
        {
            if (this.WizardModel != null)
            {
                this.SignalOrigiIsPump = this.WizardModel.SignalOrigin == PumpSignalOriginValue.Pump;
                this.IsSetPointEnabled = this.WizardModel.SignalOrigin == PumpSignalOriginValue.Pump && this.WizardModel.TypeOfOperation == PumpTypeOfOperationValue.ON;

                this.WizardModel.IsParameterChanged = this.WizardModel.SignalOrigin == PumpSignalOriginValue.Pump && this.WizardModel.IsParameterChanged;

                this.WizardErrorMessageVisibility = !this.SignalOrigiIsPump || !this.IsSetPointEnabled;

                if (!this.SignalOrigiIsPump)
                {
                    this.WizardErrorMessage = this.SignalOrigiIsPump ? string.Empty : AppResources.PumpConfiguration_Error_Message_SignalOrigin;
                }
                else if (!this.IsSetPointEnabled)
                {
                    this.WizardErrorMessage = this.IsSetPointEnabled ? string.Empty : AppResources.PumpConfiguration_Error_Message_SetPoint;
                }
            }
        }

        private void SetKeyLock(bool value)
        {
            if (this.WizardModel != null)
            {
                this.WizardModel.KeyLock = value ? PumpKeyLockValue.KeysLocked : PumpKeyLockValue.KeysNotLocked;
                this.SaveToPumpCommand?.Execute(ExternalPumpVariAParameterIdValue.KeyLock);
            }
        }

        private void SetTime()
        {
            if (this.WizardModel != null && !_isInitializing)
            {
                var unixTimestamp = _unixTimestampService.ConvertToUnix(this.VariADatePicker.Date + this.VariATimePicker);
                this.WizardModel.Time = unixTimestamp;
                this.SaveToPumpCommand?.Execute(ExternalPumpVariAParameterIdValue.Time);
            }
        }

        private void SetPumpNumber(string value)
        {
            if (this.WizardModel != null)
            {
                this.WizardModel.PumpNumber = Convert.ToInt32(value);
                this.SaveToPumpCommand?.Execute(ExternalPumpVariAParameterIdValue.PumpNumber);
            }
        }

        public void ParameterValueChanged(object parameterValue, object defaultValue)
        {
            if (parameterValue != null && defaultValue != null && !parameterValue.Equals(defaultValue))
            {
                ValidateParameterValueChanged();
            }
        }

        private void ValidateParameterValueChanged()
        {
            if (!_isInitializing)
            {
                this.WizardModel.IsParameterChanged = true;
            }
        }

        private void SetMinimumMaximumValue()
        {
            if (this.WizardModel != null)
            {
                var nominalValueParam = this.InitialParameters.FirstOrDefault(x => x.VariA == ExternalPumpVariAParameterIdValue.NominalValue);
                if (nominalValueParam != null && nominalValueParam.Scaling != null)
                {
                    double step = Math.Round(nominalValueParam.Scaling.Step, 5);

                    if ((Double.IsNaN(step) || step == 0) && _initialNominalParameterValue != null)
                    {
                        step = Math.Round(_initialNominalParameterValue.Scaling.Step, 5);
                    }

                    var valMax = nominalValueParam.Scaling.Max * step;
                    var valMin = nominalValueParam.Scaling.Min * step;

                    if ((Double.IsNaN(valMax) || valMax == 0) && _initialNominalParameterValue != null)
                    {
                        valMax = _initialNominalParameterValue.Scaling.Max * step;
                    }

                    if (nominalValueParam.Unit == ExternalUnitTValue.Percent || this.WizardModel.ControlType == PumpControlTypeValue.ConstantSpeed)
                    {
                        this.NominalValueMin = 0;
                        this.NominalValueMax = 100;
                    }
                    else if (valMax > valMin)
                    {
                        if (valMax > this.NominalValueMin)
                        {
                            this.NominalValueMax = valMax;
                        }
                        else
                        {
                            this.NominalValueMin = valMin;
                            this.NominalValueMax = valMax;
                        }
                    }

                    this.NominalValueMin = valMin;
                    this.NominalValueStep = step;

                    string nominalValueUnit = _enumHelper.GetWizardUnitText(nominalValueParam.Unit);

                    if (string.IsNullOrEmpty(nominalValueUnit))
                    {
                        nominalValueUnit = AppResources.WizardForm_UnitsInMeters;
                    }

                    this.NominalValueUnit = this.WizardModel.ControlType == PumpControlTypeValue.ConstantSpeed ? AppResources.WizardForm_UnitsInPercent : nominalValueUnit;
                }
                else
                {
                    this.NominalValueUnit = this.WizardModel.ControlType == PumpControlTypeValue.ConstantSpeed ? AppResources.WizardForm_UnitsInPercent : this.NominalValueUnit;
                }
            }
        }

        private double AdjustWizardModelValue(double wizardModelValue, double valueMax, double step)
        {
            var percDiff = Math.Abs(wizardModelValue - valueMax) * 100 / valueMax;

            if (percDiff > 0 && wizardModelValue > valueMax)
            {
                wizardModelValue *= Math.Round(step, 5);
            }

            return wizardModelValue;
        }

        private void InitializeParameters()
        {
            SetSignalOriginCondition();

            if (this.WizardModel != null)
            {
                this.SelectedControlType = this.WizardModel.ControlType.Humanize();
                this.SelectedTypeofOperation = this.WizardModel.TypeOfOperation.Humanize();
                this.SelectedPumpNumber = this.WizardModel.PumpNumber.ToString();

                if (_initialNominalParameterValue != null && _initialNominalParameterValue.Scaling != null)
                {
                    this.WizardModel.NominalValue = AdjustWizardModelValue(this.WizardModel.NominalValue, this.NominalValueMax, _initialNominalParameterValue.Scaling.Step);
                }

                this.NominalValue = this.WizardModel.NominalValue > this.NominalValueMax ? this.NominalValueMax : this.WizardModel.NominalValue;

                this.KeyboardLock = this.WizardModel.KeyLock == PumpKeyLockValue.KeysLocked;
                this.VariATimePicker = DateTimeOffset.FromUnixTimeSeconds(this.WizardModel.Time).LocalDateTime.TimeOfDay;
                this.VariADatePicker = DateTimeOffset.FromUnixTimeSeconds(this.WizardModel.Time).LocalDateTime.Date;
            }
        }

        private async Task InitializeModel(PumpConfigurationVariAWizardModel model)
        {
            _isInitializing = true;
            this.WizardModel = model;
            InitializeParameters();
            SetMinimumMaximumValue();

            await Task.Delay(200);
            this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
            await Task.Delay(200);
            await Task.WhenAll(Reconnect(this.WizardModel.SelectedDeviceUuid));
            this.UserDialogs.HideLoading();

            _isInitializing = false;
        }

        private void SetMaxValuesToDefault()
        {
            NominalValue = 85;
            NominalValueMax = 100;
            NominalValueMin = 0;
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.ShouldExitWizard))
            {
                await CloseWizardPage();
            }

            if (parameters.ContainsKey(NavigationConstants.InitialParameters))
            {
                this.InitialParameters = parameters.GetValue<List<ParameterTExternalEntity>>(NavigationConstants.InitialParameters);

                _initialNominalParameterValue = InitialParameters.FirstOrDefault(x => x.VariA == ExternalPumpVariAParameterIdValue.NominalValue);

                SetMinimumMaximumValue();
            }

            if (parameters.ContainsKey(NavigationConstants.ParameterCharacteristics))
            {
                this.ParameterCharacteristicList = parameters.GetValue<List<ParameterPumpVariACharacteristicExternalEntity>>(NavigationConstants.ParameterCharacteristics);
            }

            if (parameters.ContainsKey(NavigationConstants.Logs))
            {
                _logs = parameters.GetValue<List<LogEntryTExternalEntity>>(NavigationConstants.Logs);
            }

            if (parameters.ContainsKey(NavigationConstants.CockpitPumpsVariAERootModel))
            {
                CockpitPumpsRootModel = parameters.GetValue<CockpitPumpsVariAERootModel>(NavigationConstants.CockpitPumpsVariAERootModel);
            }

            if (parameters.ContainsKey(NavigationConstants.PumpConfigurationVariAWizardModel))
            {
                PopulateList();
                var model = parameters.GetValue<PumpConfigurationVariAWizardModel>(NavigationConstants.PumpConfigurationVariAWizardModel);
                await InitializeModel(model);
            }
        }

        public async Task CloseAsync()
        {
            await CloseWizardPage();
        }

        public override void Destroy()
        {
            base.Destroy();
            _lostConnectionAlertDialog = null;
            _notifyDisposable?.Dispose();
            _notifyDisposable = null;
            _deviceStatusDisposable?.Dispose();
            _deviceStatusDisposable = null;
            EventAggregator.GetEvent<ConnectionEstablishedEvent>().Unsubscribe(OnConnectionEstablished);
            EventAggregator.GetEvent<ConnectionLostEvent>().Unsubscribe(OnConnectionLost);
        }
    }
}
