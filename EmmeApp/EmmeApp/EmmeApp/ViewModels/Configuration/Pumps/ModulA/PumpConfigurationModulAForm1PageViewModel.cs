﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.Configuration;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities.External;
using EmmeApp.Events;
using EmmeApp.Helpers;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Humanizer;
using Plugin.BluetoothLE;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace EmmeApp.ViewModels
{
    public class PumpConfigurationModulAForm1PageViewModel : ViewModelBase, IChainModalViewModel
    {
        private readonly IEnumProvider _enumHelper;
        private readonly IPopupNavigation _popupNavigation;
        private readonly IKeyboardHelper _keyboardHelper;
        private readonly IUnixTimestampService _unixTimestampService;
        private readonly IConfigurationPumpModulAManager _configurationPumpModulAManager;
        private readonly IBleManager _bleManager;
        private readonly ICockpitPumpsModulAManager _cockpitPumpsModulAManager;
        private readonly ILogPumpManager _logPumpManager;

        private DialogAlertSingle _lostConnectionAlertDialog = null;
        private bool _isIntentionallyReconnecting = false;
        private readonly SemaphoreSlim _connectionManagementThrottle = new SemaphoreSlim(1, 1);
        private readonly SemaphoreSlim _saveActionThrottle = new SemaphoreSlim(1, 1);

        private readonly SubscriptionToken _connectionEstablishedSubToken;
        private readonly SubscriptionToken _connectionLostSubToken;

        private ModalChainStack _configModalStack = null;

        public PumpConfigurationModulAForm1PageViewModel(INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IEnumProvider enumHelper,
            IPopupNavigation popupNavigation,
            IKeyboardHelper keyboardHelper,
            IUnixTimestampService unixTimestampService,
            IBleManager bleManager,
            IConfigurationPumpModulAManager configurationPumpModulAManager,
            ICockpitPumpsModulAManager cockpitPumpsModulAManager,
            ILogPumpManager logPumpManager)
            : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
        {
            _enumHelper = enumHelper;
            _popupNavigation = popupNavigation;
            _keyboardHelper = keyboardHelper;
            _unixTimestampService = unixTimestampService;
            _configurationPumpModulAManager = configurationPumpModulAManager;
            _bleManager = bleManager;
            _cockpitPumpsModulAManager = cockpitPumpsModulAManager;
            _logPumpManager = logPumpManager;

            _configModalStack = new ModalChainStack().ChainPush(this);

            this.WizardCloseCommand = new DelegateCommand(async () => await OnWizardClose());
            this.WizardLeftButtonCommand = new DelegateCommand(async () => await OnWizardClose());
            this.WizardRightButtonCommand = new DelegateCommand(async () => await OnNextButton());
            this.SaveToPumpCommand = new DelegateCommand<object>(async (parameterId) => await OnSaveToPump(parameterId));

            _connectionEstablishedSubToken = this.EventAggregator.GetEvent<ConnectionEstablishedEvent>().Subscribe(async () => await OnConnectionEstablished());
            _connectionLostSubToken = this.EventAggregator.GetEvent<ConnectionLostEvent>().Subscribe(async () => await OnConnectionLost());
        }

        private List<byte[]> _bleParameterData;
        private List<byte[]> _bleParameterBufferData;
        private bool _shouldAcquireNewData = false;
        private bool _isInitializing = true;
        private bool _isUpdating = false;
        private IDisposable _notifyDisposable;
        private IDisposable _deviceStatusDisposable;
        private List<LogEntryTExternalEntity> _logs;
        private ParameterTExternalEntity _initialNominalParameterValue;
        private ParameterTExternalEntity _initialPowerLimitParameterValue;

        public CockpitPumpsModulARootModel CockpitPumpsRootModel { get; set; }

        private string _wizardTitle = AppResources.PumpConfiguration_ModulA_Title;
        public string WizardTitle
        {
            get => _wizardTitle;
            set => SetProperty(ref _wizardTitle, value);
        }

        private int _wizardStepNumber = 1;
        public int WizardStepNumber
        {
            get => _wizardStepNumber;
            set => SetProperty(ref _wizardStepNumber, value);
        }

        private string _wizardFooterLeftLabel = AppResources.InitialStartup_Wizard_Cancel;
        public string WizardFooterLeftLabel
        {
            get => _wizardFooterLeftLabel;
            set => SetProperty(ref _wizardFooterLeftLabel, value);
        }
        private string _wizardFooterRightLabel = AppResources.InitialStartup_Wizard_Next;
        public string WizardFooterRightLabel
        {
            get => _wizardFooterRightLabel;
            set => SetProperty(ref _wizardFooterRightLabel, value);
        }
        private string _wizardErrorMessage;
        public string WizardErrorMessage
        {
            get => _wizardErrorMessage;
            set => SetProperty(ref _wizardErrorMessage, value);
        }
        private bool _wizardErrorMessageVisibility = false;
        public bool WizardErrorMessageVisibility
        {
            get => _wizardErrorMessageVisibility;
            set => SetProperty(ref _wizardErrorMessageVisibility, value);
        }

        private string _helpResourceKey = ResourceConstants.Pump_Configuration_Form1_Help;
        public string HelpResourceKey
        {
            get => _helpResourceKey;
            set => SetProperty(ref _helpResourceKey, value);
        }

        private PumpConfigurationModulAWizardModel _wizardModel;
        public PumpConfigurationModulAWizardModel WizardModel
        {
            get => _wizardModel;
            set => SetProperty(ref _wizardModel, value);
        }

        private List<ParameterTExternalEntity> _initialParameters;
        public List<ParameterTExternalEntity> InitialParameters
        {
            get => _initialParameters;
            set => SetProperty(ref _initialParameters, value);
        }

        private List<ParameterPumpModulACharacteristicExternalEntity> _parameterCharacteristicList;
        public List<ParameterPumpModulACharacteristicExternalEntity> ParameterCharacteristicList
        {
            get => _parameterCharacteristicList;
            set => SetProperty(ref _parameterCharacteristicList, value);
        }



        private TimeSpan _modulATimePicker = DateTimeOffset.FromUnixTimeSeconds(1549276713).LocalDateTime.TimeOfDay;
        public TimeSpan ModulATimePicker
        {
            get => _modulATimePicker;
            set
            {
                if (!this.ModulATimePicker.Hours.Equals(value.Hours) || !this.ModulATimePicker.Minutes.Equals(value.Minutes))
                {
                    ValidateParameterValueChanged();
                }
                Debug.WriteLine($"ModulA Time Picker - {value.ToString()}");

                SetProperty(ref _modulATimePicker, value, onChanged: () => SetTime());
            }
        }

        private DateTime _modulADatePicker = DateTimeOffset.FromUnixTimeSeconds(1549276713).LocalDateTime.Date;
        public DateTime ModulADatePicker
        {
            get => _modulADatePicker;
            set
            {
                if (!this.ModulADatePicker.Month.Equals(value.Month) || !this.ModulADatePicker.Day.Equals(value.Day) || !this.ModulADatePicker.Year.Equals(value.Year))
                {
                    ValidateParameterValueChanged();
                }
                Debug.WriteLine($"ModulA Date Picker - {value.ToString()}");

                SetProperty(ref _modulADatePicker, value, onChanged: () => SetTime());
            }
        }

        private string _selectedControlType;
        public string SelectedControlType
        {
            get => _selectedControlType;
            set => SetProperty(ref _selectedControlType, value, onChanged: async () => await SetControlTypeChanged());
        }

        private async Task SetControlTypeChanged()
        {
            ValidateParameterValueChanged();
            await SetControlType(SelectedControlType);
        }

        private List<string> _controlTypeList;
        public List<string> ControlTypeList
        {
            get => _controlTypeList;
            set => SetProperty(ref _controlTypeList, value);
        }

        private string _selectedTypeofOperation;
        public string SelectedTypeofOperation
        {
            get => _selectedTypeofOperation;
            set => SetProperty(ref _selectedTypeofOperation, value, onChanged: async () => await SetTypeOfOperationChagned());
        }

        private async Task SetTypeOfOperationChagned()
        {
            ValidateParameterValueChanged();
            await SetTypeOfOperation(SelectedTypeofOperation);
        }

        private List<string> _typeofOperationList;
        public List<string> TypeofOperationList
        {
            get => _typeofOperationList;
            set => SetProperty(ref _typeofOperationList, value);
        }

        private List<string> _pumpNumberList;
        public List<string> PumpNumberList
        {
            get => _pumpNumberList;
            set => SetProperty(ref _pumpNumberList, value);
        }

        private bool _keyboardLock = false;
        public bool KeyboardLock
        {
            get => _keyboardLock;
            set => SetProperty(ref _keyboardLock, value, onChanged: () => SetKeyLockChanged(value));
        }

        private void SetKeyLockChanged(bool value)
        {
            if (this.WizardModel != null && !_isInitializing)
            {
                ValidateParameterValueChanged();
                SetKeyLock(value);
            }
        }

        private bool _signalOriginIsPump = true;
        public bool SignalOrigiIsPump
        {
            get => _signalOriginIsPump;
            set => SetProperty(ref _signalOriginIsPump, value);
        }

        private bool _isSetPointEnabled = false;
        public bool IsSetPointEnabled
        {
            get => _isSetPointEnabled;
            set => SetProperty(ref _isSetPointEnabled, value);
        }

        private string _nominalValueUnit = AppResources.WizardForm_UnitsInMeters;
        public string NominalValueUnit
        {
            get => _nominalValueUnit;
            set => SetProperty(ref _nominalValueUnit, value);
        }

        private double _nominalValueMax = 100;
        public double NominalValueMax
        {
            get => _nominalValueMax;
            set => SetProperty(ref _nominalValueMax, value);
        }

        private double _nominalValueMin;
        public double NominalValueMin
        {
            get => _nominalValueMin;
            set => SetProperty(ref _nominalValueMin, value);
        }

        private double _nominalValueStep = 0.1;
        public double NominalValueStep
        {
            get => _nominalValueStep;
            set => SetProperty(ref _nominalValueStep, value);
        }

        private double _nominalValue = 85;
        public double NominalValue
        {
            get => _nominalValue;
            set => SetProperty(ref _nominalValue, value, onChanged: () => SetNominalValueChanged(value));
        }

        private void SetNominalValueChanged(double value)
        {
            if (this.WizardModel != null && !_isInitializing)
            {
                ValidateParameterValueChanged();
                this.WizardModel.NominalValue = value;
                this.SaveToPumpCommand?.Execute(ExternalPumpModulAParameterIdValue.NominalValue);
            }
        }

        private string _powerLimitUnit = AppResources.WizardForm_UnitsInm3h;
        public string PowerLimitUnit
        {
            get => _powerLimitUnit;
            set => SetProperty(ref _powerLimitUnit, value);
        }

        private double _powerLimitMaxValue = 120;
        public double PowerLimitMaxValue
        {
            get => _powerLimitMaxValue;
            set => SetProperty(ref _powerLimitMaxValue, value);
        }

        private double _powerLimitMinValue;
        public double PowerLimitMinValue
        {
            get => _powerLimitMinValue;
            set => SetProperty(ref _powerLimitMinValue, value);
        }

        private double _powerLimitStep = 0.1;
        public double PowerLimitStep
        {
            get => _powerLimitStep;
            set => SetProperty(ref _powerLimitStep, value);
        }

        private double _powerLimit = 105;
        public double PowerLimit
        {
            get => _powerLimit;
            set => SetProperty(ref _powerLimit, value, onChanged: () => SetPowerLimitChanged(value));
        }

        private void SetPowerLimitChanged(double value)
        {
            if (this.WizardModel != null && !_isInitializing)
            {
                ValidateParameterValueChanged();
                this.WizardModel.PowerLimitMax = value;
                this.SaveToPumpCommand?.Execute(ExternalPumpModulAParameterIdValue.PowerLimitMax);
            }
        }

        private string _selectedPumpNumber;
        public string SelectedPumpNumber
        {
            get => _selectedPumpNumber;
            set => SetProperty(ref _selectedPumpNumber, value, onChanged: () => SetSelectedPumpNumberChanged(value));
        }

        private void SetSelectedPumpNumberChanged(string value)
        {
            ValidateParameterValueChanged();
            SetPumpNumber(value);
        }

        private bool _isPowerLimitEnabled;
        public bool IsPowerLimitEnabled
        {
            get => _isPowerLimitEnabled;
            set => SetProperty(ref _isPowerLimitEnabled, value);
        }

        private PumpTypeOfOperationValue? _isUniqueTypeOfOperation = null;

        public DelegateCommand WizardCloseCommand { get; private set; }
        public DelegateCommand WizardLeftButtonCommand { get; private set; }
        public DelegateCommand WizardRightButtonCommand { get; private set; }

        public DelegateCommand<object> SaveToPumpCommand { get; private set; }

        private async Task OnWizardClose()
        {
            _keyboardHelper.HideKeyboard();

            DialogAlert dialogAlert = new DialogAlert();
            dialogAlert.LeftButtonText = AppResources.PumpConfiguration_Wizard_Popup_Close_Back;
            dialogAlert.RightButtonText = AppResources.PumpConfiguration_Wizard_Popup_Close_Confirm;
            //dialogAlert.ContentText = AppResources.PumpConfiguration_Wizard_Popup_Close_Content;
            //TODO Change for 2169
            dialogAlert.ContentText = AppResources.InitialStartup_Wizard_Popup_Close_Content;
            dialogAlert.Title = AppResources.PumpConfiguration_Wizard_Popup_Close_Title;
            dialogAlert.RightButtonCommand = new DelegateCommand(async () => await CloseWizardPage());
            await _popupNavigation.PushAsync(dialogAlert, false);
        }

        private async Task OnNextButton()
        {
            _keyboardHelper.HideKeyboard();

            if (this.WizardModel != null)
            {
                _shouldAcquireNewData = false;

                this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
                await Task.Delay(100);

                var logRootModel = new PumpLogRootModel();
                logRootModel = _logPumpManager.MapParametersToWizardModel(logRootModel, _logs);

                this.WizardErrorMessage = string.Empty;
                this.WizardErrorMessageVisibility = false;

                INavigationParameters parameters = new NavigationParameters();
                parameters.Add(NavigationConstants.PumpConfigurationModulAWizardModel, this.WizardModel);
                parameters.Add(NavigationConstants.ParameterCharacteristics, this.ParameterCharacteristicList);
                parameters.Add(NavigationConstants.CockpitPumpsModulARootModel, CockpitPumpsRootModel);
                parameters.Add(NavigationConstants.PumpLogRootModel, logRootModel);
                parameters.Add(NavigationConstants.ChainModalStack, _configModalStack);

                this.UserDialogs.HideLoading();
                await NavigationUtilities.NavigateAsync(this.NavigationService, ViewNames.PumpConfigurationModulAForm2SystemDescriptionPage, parameters, null, false);
            }
        }

        private async Task OnSaveToPump(object parameterId, bool showLoading = true)
        {
            if (!_shouldAcquireNewData)
                return;

            if (parameterId == null)
            {
                return;
            }

            try
            {
                await _saveActionThrottle.WaitAsync();

                WizardModel.IsParameterChanged = true;
                var clonedWizardModel = new PumpConfigurationModulAWizardModel()
                {
                    ContactName = WizardModel.ContactName,
                    ControlType = WizardModel.ControlType,
                    EmailAddress = WizardModel.EmailAddress,
                    GroupName = WizardModel.GroupName,
                    IsParameterChanged = WizardModel.IsParameterChanged,
                    KeyLock = WizardModel.KeyLock,
                    NominalValue = WizardModel.NominalValue,
                    Phone = WizardModel.Phone,
                    PowerLimit = WizardModel.PowerLimit,
                    PowerLimitMax = WizardModel.PowerLimitMax,
                    PumpNumber = WizardModel.PumpNumber,
                    Remarks = WizardModel.Remarks,
                    SelectedDeviceUuid = WizardModel.SelectedDeviceUuid,
                    SignalOrigin = WizardModel.SignalOrigin,
                    SystemAddress = WizardModel.SystemAddress,
                    Time = WizardModel.Time,
                    SystemName = WizardModel.SystemName,
                    TypeOfOperation = WizardModel.TypeOfOperation,
                };

                var paramId = (ExternalPumpModulAParameterIdValue)parameterId;
                if (showLoading)
                    this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
                var connectedDevice = await _bleManager.GetConnectedDevice();

                //attempt to reconnect.
                if (connectedDevice == null || connectedDevice.Status == ConnectionStatus.Disconnected)
                {
                    connectedDevice = await Reconnect(clonedWizardModel.SelectedDeviceUuid);
                }

                if (connectedDevice != null && connectedDevice.Status == ConnectionStatus.Connected)
                {
                    var characteristic = await _bleManager.GetDeviceWriteCharacteristic(connectedDevice);

                    if (characteristic != null)
                    {
                        var parameterCharacteristicList = new List<ParameterPumpModulACharacteristicExternalEntity>(this.ParameterCharacteristicList);

                        if (paramId == ExternalPumpModulAParameterIdValue.NominalValue)
                        {
                            var param = parameterCharacteristicList.FirstOrDefault(x => x.ModulA == ExternalPumpModulAParameterIdValue.NominalValue);

                            if (param != null && param.Scaling != null)
                            {
                                if (clonedWizardModel.ControlType == PumpControlTypeValue.ConstantSpeed)
                                {
                                    param.Scaling.Step = 1;
                                }
                                else
                                {
                                    param.Scaling.Step = 0.1;
                                }
                            }
                        }

                        var parametersList = _configurationPumpModulAManager.MapWizardModelToParameters(clonedWizardModel, parameterCharacteristicList, paramId);

                        bool isWriteSuccess = true;
                        foreach (var parameters in parametersList)
                        {
                            var parametersT = new ParametersTExternalEntity() { Parameters = parameters };
                            isWriteSuccess = await _bleManager.WriteCommand(parametersT, characteristic, ExternalIdOneofCaseValue.ModulA);

                            if (!isWriteSuccess)
                            {
                                break;
                            }
                        }

                        if (isWriteSuccess)
                        {
                            this.UserDialogs.HideLoading();
                            //this.WizardModel.IsParameterChanged = false;
                        }
                        else
                        {
                            this.UserDialogs.HideLoading();
                            await this.UserDialogs.AlertAsync(AppResources.Alert_Message_BLE_CannotWriteData);
                        }
                    }
                }

                if (showLoading)
                    this.UserDialogs.HideLoading();
            }
            finally
            {
                _saveActionThrottle.Release();
            }
        }

        private async Task CloseWizardPage()
        {
            _shouldAcquireNewData = false;

            _keyboardHelper.HideKeyboard();

            SetMaxValuesToDefault();

            INavigationParameters parameters = new NavigationParameters();



            if (this.InitialParameters != null && this.InitialParameters.Any())
            {
                var rootModel = new CockpitPumpsModulARootModel();
                var rootModelResult = _cockpitPumpsModulAManager.MapParametersToModel(rootModel, this.InitialParameters);
                rootModel = rootModelResult.Item1;
                parameters.Add(NavigationConstants.CockpitPumpsModulARootModel, this.CockpitPumpsRootModel);
                parameters.Add(NavigationConstants.InitialParameters, this.InitialParameters);
            }

            await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
        }

        private async Task<IDevice> Reconnect(Guid selectedDeviceUuid)
        {
            var connectedDevice = await _bleManager.GetKnownDevice(selectedDeviceUuid);

            if (connectedDevice != null)
            {
                _isIntentionallyReconnecting = true;

                var newDevice = connectedDevice;

                if (newDevice.Status == ConnectionStatus.Disconnected)
                {
                    var restartTheDeviceTask = Task.WhenAll(_bleManager.RestartTheDevice(connectedDevice));
                    newDevice = restartTheDeviceTask.Result[0];
                    await Task.Delay(2000);
                }

                if (newDevice != null && newDevice.Status == ConnectionStatus.Connected)
                {
                    var characteristic = await _bleManager.GetDeviceReadCharacteristic(newDevice);

                    if (characteristic != null)
                    {
                        _shouldAcquireNewData = false;

                        _bleParameterData = new List<byte[]>();
                        _bleParameterBufferData = new List<byte[]>();

                        int parameterCtr = 0;

                        _notifyDisposable?.Dispose();
                        _notifyDisposable =
                            characteristic
                                .WhenNotificationReceived()
                                .Subscribe(
                                    async result => { await ReadParameters(result); parameterCtr++; },
                                    ex => { }
                                );

                        _deviceStatusDisposable?.Dispose();
                        _deviceStatusDisposable =
                            newDevice
                                .WhenStatusChanged()
                                .Subscribe(
                                    async (connectionStatus) =>
                                    {
                                        switch (connectionStatus)
                                        {
                                            case ConnectionStatus.Connected:
                                                await OnConnectionEstablished();
                                                break;

                                            case ConnectionStatus.Disconnected:
                                                await OnConnectionLost();
                                                break;
                                        }
                                    });

                        _shouldAcquireNewData = true;

                        if (!characteristic.IsNotifying)
                        {
                            await characteristic.EnableNotifications();
                        }

                        //TODO UserStory #2023
                        //Reduced maxRetryCount from 150 to 50
                        int maxRetryCount = 50;
                        int retryCount = 0;
                        while (parameterCtr < 50)
                        {
                            await Task.Delay(100);
                            retryCount++;

                            if (retryCount >= maxRetryCount)
                            {
                                newDevice = null;
                                break;
                            }
                        }
                    }
                    else
                    {
                        newDevice.CancelConnection();
                    }
                }

                connectedDevice = newDevice;
            }

            return connectedDevice;
        }

        private async Task OnConnectionEstablished()
        {
            if (!(await _connectionManagementThrottle.WaitAsync(0)))
                return;

            _isIntentionallyReconnecting = false;

            await RemoveAlertForLostConnection();

            _connectionManagementThrottle.Release();
        }

        private async Task OnConnectionLost()
        {
            if (!(await _connectionManagementThrottle.WaitAsync(0)))
                return;

            if (_isIntentionallyReconnecting)
            {
                _connectionManagementThrottle.Release();
                return;
            }

            await AlertUserOfLostConnection();

            _connectionManagementThrottle.Release();
        }

        private async Task AlertUserOfLostConnection()
        {
            if (_lostConnectionAlertDialog != null)
                return;

            UserDialogs.HideLoading();
            _lostConnectionAlertDialog = new DialogAlertSingle();
            _lostConnectionAlertDialog.ButtonText = AppResources.Alert_Message_OK;
            _lostConnectionAlertDialog.ContentText = AppResources.Alert_MessageBody_BLE_LostConnection;
            _lostConnectionAlertDialog.Title = AppResources.Alert_MessageHeader_BLE_LostConnection;
            _lostConnectionAlertDialog.ButtonCommand =
                new DelegateCommand(
                    async () =>
                    {
                        _lostConnectionAlertDialog = null;
                        await _configModalStack.Peek().CloseAsync();
                    });
            //TEST resuming connection repeatedly then disconnect then tap ok to return to homepage then reconnect -> crash!!!
            await _popupNavigation.PushAsync(_lostConnectionAlertDialog, false);
        }

        private async Task RemoveAlertForLostConnection()
        {
            if (_lostConnectionAlertDialog != null && _popupNavigation.PopupStack.Any(x => x == _lostConnectionAlertDialog))
            {
                await _popupNavigation.RemovePageAsync(_lostConnectionAlertDialog);
                _lostConnectionAlertDialog = null;
            }
        }

        private async Task ReadParameters(CharacteristicGattResult result)
        {
            if (_shouldAcquireNewData)
            {
                if (!_isUpdating && _bleParameterBufferData.Any())
                {
                    _isUpdating = true;

                    var bleParameterBufferData = new List<byte[]>(_bleParameterBufferData);
                    _bleParameterBufferData = new List<byte[]>();

                    var parameters = new List<ParameterTExternalEntity>();

                    foreach (var parameterData in bleParameterBufferData)
                    {
                        parameters.AddRange(_bleManager.GetInitializeParameters(parameterData));
                    }

                    foreach (var newParameter in parameters)
                    {
                        var parameter = this.InitialParameters.FirstOrDefault(x => x.ModulA == newParameter.ModulA);
                        if (parameter != null)
                        {
                            parameter.Value = newParameter.Value;

                            if (newParameter.Unit != ExternalUnitTValue.UnknownUnit)
                            {
                                parameter.Unit = newParameter.Unit;
                            }

                            if (newParameter.Scaling != null)
                            {
                                parameter.Scaling = newParameter.Scaling;
                            }

                            newParameter.Unit = parameter.Unit;
                            newParameter.Scaling = parameter.Scaling;
                        }
                    }

                    if (parameters.Any(x => x.ModulA == ExternalPumpModulAParameterIdValue.SignalOrigin ||
                                            x.ModulA == ExternalPumpModulAParameterIdValue.ControlType ||
                                            x.ModulA == ExternalPumpModulAParameterIdValue.TypeOfOperation ||
                                            x.ModulA == ExternalPumpModulAParameterIdValue.PumpNumber ||
                                            x.ModulA == ExternalPumpModulAParameterIdValue.NominalValue ||
                                            x.ModulA == ExternalPumpModulAParameterIdValue.PowerLimitMax ||
                                            x.ModulA == ExternalPumpModulAParameterIdValue.PowerLimitEn ||
                                            x.ModulA == ExternalPumpModulAParameterIdValue.KeyLock ||
                                            x.ModulA == ExternalPumpModulAParameterIdValue.Time))
                    {

                        var modelResult = _configurationPumpModulAManager.MapParametersToWizardModel(this.WizardModel, parameters);

                        this.WizardModel = modelResult.Item1;
                        this.InitialParameters = parameters;

                        if (parameters.Any(x => x.ModulA == ExternalPumpModulAParameterIdValue.PowerLimitMax
                            || x.ModulA == ExternalPumpModulAParameterIdValue.NominalValue
                            || x.ModulA == ExternalPumpModulAParameterIdValue.ControlType))
                        {
                            SetMinimumMaximumValue();
                        }

                        if (parameters.Any(x => x.ModulA == ExternalPumpModulAParameterIdValue.NominalValue) && _initialNominalParameterValue != null && _initialNominalParameterValue.Scaling != null)
                        {
                            var param = parameters.FirstOrDefault(x => x.ModulA == ExternalPumpModulAParameterIdValue.NominalValue);

                            if (param.Value != null && WizardModel.ControlType != PumpControlTypeValue.ConstantSpeed)
                            {
                                this.WizardModel.NominalValue = param.Value.Number * 0.1;
                            }
                        }

                        if (parameters.Any(x => x.ModulA == ExternalPumpModulAParameterIdValue.PowerLimitMax) && _initialPowerLimitParameterValue != null && _initialPowerLimitParameterValue.Scaling != null)
                        {
                            var param = parameters.FirstOrDefault(x => x.ModulA == ExternalPumpModulAParameterIdValue.PowerLimitMax);

                            if (param.Value != null)
                            {
                                var val = param.Value.Number;
                                this.WizardModel.PowerLimitMax = val * Math.Round(_initialPowerLimitParameterValue.Scaling.Step, 5);
                            }
                        }

                        _isInitializing = true;
                        InitializeParameters();
                        await Task.Delay(1000);
                        _isInitializing = false;
                    }

                    _isUpdating = false;
                }
                else
                {
                    if (result != null && result.Data != null)
                    {
                        var resultData = result.Data;
                        _bleParameterBufferData.Add(resultData);
                    }
                }

                if (!_isUpdating && _bleParameterBufferData.Any())
                {
                    await ReadParameters(null);
                }
            }
        }

        private void PopulateList()
        {
            this.ControlTypeList = _enumHelper.PopulateList<PumpControlTypeValue>()
                .Select(x => x.Humanize())
                .ToList();

            this.TypeofOperationList = _enumHelper.PopulateList<PumpTypeOfOperationValue>()
                .Select(x => x.Humanize())
                .ToList();

            this.PumpNumberList = new List<string>();
            Enumerable.Range(1, 64).ForEach(x => this.PumpNumberList.Add(x.ToString()));
        }

        private async Task SetControlType(string value)
        {
            this.WizardModel.ControlType = EnumDehumanizeExtensions.DehumanizeTo<PumpControlTypeValue>(value);
            UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
            await Task.Delay(5000);
            await OnSaveToPump(ExternalPumpModulAParameterIdValue.ControlType, false);
            SetSignalOriginCondition();
            UserDialogs.HideLoading();
        }

        private async Task SetTypeOfOperation(string value)
        {
            this.WizardModel.TypeOfOperation = EnumDehumanizeExtensions.DehumanizeTo<PumpTypeOfOperationValue>(value);
            UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
            await Task.Delay(5000);
            await OnSaveToPump(ExternalPumpModulAParameterIdValue.TypeOfOperation, false);
            SetSignalOriginCondition();
            UserDialogs.HideLoading();
        }

        private void SetSignalOriginCondition()
        {
            if (this.WizardModel != null)
            {
                this.SignalOrigiIsPump = this.WizardModel.SignalOrigin == PumpSignalOriginValue.Pump;
                this.IsSetPointEnabled = this.WizardModel.SignalOrigin == PumpSignalOriginValue.Pump && this.WizardModel.TypeOfOperation == PumpTypeOfOperationValue.ON;

                this.WizardModel.IsParameterChanged = this.WizardModel.SignalOrigin == PumpSignalOriginValue.Pump && this.WizardModel.IsParameterChanged;

                this.WizardErrorMessageVisibility = !this.SignalOrigiIsPump || !this.IsSetPointEnabled;

                if (!this.SignalOrigiIsPump)
                {
                    this.WizardErrorMessage = this.SignalOrigiIsPump ? string.Empty : AppResources.PumpConfiguration_Error_Message_SignalOrigin;
                }
                else if (!this.IsSetPointEnabled)
                {
                    this.WizardErrorMessage = this.IsSetPointEnabled ? string.Empty : AppResources.PumpConfiguration_Error_Message_SetPoint;
                }
            }
        }

        private void SetKeyLock(bool value)
        {
            if (this.WizardModel != null)
            {
                this.WizardModel.KeyLock = value ? PumpKeyLockValue.KeysLocked : PumpKeyLockValue.KeysNotLocked;
                this.SaveToPumpCommand?.Execute(ExternalPumpModulAParameterIdValue.KeyLock);
            }
        }

        private void SetTime()
        {
            if (this.WizardModel != null && !_isInitializing)
            {
                var unixTimestamp = _unixTimestampService.ConvertToUnix(this.ModulADatePicker.Date + this.ModulATimePicker);
                this.WizardModel.Time = unixTimestamp;
                this.SaveToPumpCommand?.Execute(ExternalPumpModulAParameterIdValue.Time);
            }
        }

        private void SetPumpNumber(string value)
        {
            if (this.WizardModel != null)
            {
                this.WizardModel.PumpNumber = Convert.ToInt32(value);
                this.SaveToPumpCommand?.Execute(ExternalPumpModulAParameterIdValue.PumpNumber);
            }
        }

        private void SetPowerLimitEnabledCondition()
        {
            if (this.WizardModel != null)
            {
                IsPowerLimitEnabled = this.WizardModel.PowerLimit == PumpPowerLimitValue.PowerLimitOn;

                this.WizardErrorMessageVisibility = IsPowerLimitEnabled;

                if (!this.IsPowerLimitEnabled)
                {
                    this.WizardErrorMessage = AppResources.PumpConfiguration_Error_Message_PowerLimitEnabled;
                }
            }
        }

        private void ValidateParameterValueChanged()
        {
            if (!_isInitializing)
            {
                this.WizardModel.IsParameterChanged = true;
            }
        }

        private void SetMinimumMaximumValue()
        {
            if (this.WizardModel != null)
            {
                var nominalValueParam = this.InitialParameters.FirstOrDefault(x => x.ModulA == ExternalPumpModulAParameterIdValue.NominalValue);
                if (nominalValueParam != null && nominalValueParam.Scaling != null)
                {
                    double step = Math.Round(nominalValueParam.Scaling.Step, 5);

                    if ((Double.IsNaN(step) || step == 0) && _initialNominalParameterValue != null)
                    {
                        step = Math.Round(_initialNominalParameterValue.Scaling.Step, 5);
                    }

                    var valMax = nominalValueParam.Scaling.Max * step;
                    var valMin = nominalValueParam.Scaling.Min * step;

                    if ((Double.IsNaN(valMax) || valMax == 0) && _initialNominalParameterValue != null)
                    {
                        valMax = _initialNominalParameterValue.Scaling.Max * step;
                    }

                    if (nominalValueParam.Unit == ExternalUnitTValue.Percent
                        || this.WizardModel.ControlType == PumpControlTypeValue.ConstantSpeed)
                    {
                        this.NominalValueMin = 0;
                        this.NominalValueMax = 100;
                    }
                    else if (valMax > valMin)
                    {
                        if (valMax > this.NominalValueMin)
                        {
                            this.NominalValueMax = valMax;
                        }
                        else
                        {
                            this.NominalValueMin = valMin;
                            this.NominalValueMax = valMax;
                        }
                    }

                    this.NominalValueMin = valMin;
                    this.NominalValueStep = step;

                    string nominalValueUnit = _enumHelper.GetWizardUnitText(nominalValueParam.Unit);

                    if (string.IsNullOrEmpty(nominalValueUnit))
                    {
                        nominalValueUnit = AppResources.WizardForm_UnitsInMeters;
                    }

                    this.NominalValueUnit = this.WizardModel.ControlType == PumpControlTypeValue.ConstantSpeed ? AppResources.WizardForm_UnitsInPercent : nominalValueUnit;

                }
                else
                {
                    this.NominalValueUnit = this.WizardModel.ControlType == PumpControlTypeValue.ConstantSpeed ? AppResources.WizardForm_UnitsInPercent : this.NominalValueUnit;
                }
            }

            if (this.WizardModel != null)
            {
                var powerLimitParam = this.InitialParameters.FirstOrDefault(x => x.ModulA == ExternalPumpModulAParameterIdValue.PowerLimitMax);
                if (powerLimitParam != null && powerLimitParam.Scaling != null)
                {
                    double step = Math.Round(powerLimitParam.Scaling.Step, 5);

                    if ((Double.IsNaN(step) || step == 0) && _initialPowerLimitParameterValue != null)
                    {
                        step = Math.Round(_initialPowerLimitParameterValue.Scaling.Step, 5);
                    }

                    var valMax = powerLimitParam.Scaling.Max * step;
                    var valMin = powerLimitParam.Scaling.Min * step;

                    if ((Double.IsNaN(valMax) || valMax == 0) && _initialPowerLimitParameterValue != null)
                    {
                        valMax = _initialPowerLimitParameterValue.Scaling.Max * step;
                    }

                    if (valMax > valMin)
                    {
                        if (valMax > this.PowerLimitMinValue)
                        {
                            this.PowerLimitMaxValue = valMax;
                        }
                        else
                        {
                            this.PowerLimitMinValue = valMin;
                            this.PowerLimitMaxValue = valMax;
                        }
                    }

                    this.PowerLimitMinValue = valMin;
                    this.PowerLimitStep = step;
                    string unit = _enumHelper.GetWizardUnitText(powerLimitParam.Unit);
                    this.PowerLimitUnit = string.IsNullOrEmpty(unit) ? this.PowerLimitUnit : unit;
                }
            }
        }

        private double AdjustWizardModelValue(double wizardModelValue, double valueMax, double step)
        {
            var percDiff = Math.Abs(wizardModelValue - valueMax) * 100 / valueMax;

            if (percDiff > 0 && wizardModelValue > valueMax)
            {
                wizardModelValue *= Math.Round(step, 5);
            }

            return wizardModelValue;
        }

        private void InitializeParameters()
        {
            SetSignalOriginCondition();
            this.WizardModel.IsParameterChanged = this.WizardModel.SignalOrigin == PumpSignalOriginValue.Pump && this.WizardModel.IsParameterChanged;
            SetPowerLimitEnabledCondition();

            if (this.WizardModel != null)
            {
                this.SelectedControlType = this.WizardModel.ControlType.Humanize();
                this.SelectedTypeofOperation = this.WizardModel.TypeOfOperation.Humanize();
                this.SelectedPumpNumber = this.WizardModel.PumpNumber.ToString();

                if (_initialNominalParameterValue != null && _initialNominalParameterValue.Scaling != null)
                {
                    this.WizardModel.NominalValue = AdjustWizardModelValue(this.WizardModel.NominalValue, this.NominalValueMax, _initialNominalParameterValue.Scaling.Step);
                }

                this.NominalValue = this.WizardModel.NominalValue > this.NominalValueMax ? this.NominalValueMax : this.WizardModel.NominalValue;

                if (_initialPowerLimitParameterValue != null && _initialPowerLimitParameterValue.Scaling != null)
                {
                    this.WizardModel.PowerLimitMax = AdjustWizardModelValue(this.WizardModel.PowerLimitMax, this.PowerLimitMaxValue, _initialPowerLimitParameterValue.Scaling.Step);
                }

                this.PowerLimit = this.WizardModel.PowerLimitMax > this.PowerLimitMaxValue ? this.PowerLimitMaxValue : this.WizardModel.PowerLimitMax;

                this.KeyboardLock = this.WizardModel.KeyLock == PumpKeyLockValue.KeysLocked;
                Debug.WriteLine($"===================== WIZARDMODEL TIME {this.WizardModel.Time}");
                this.ModulATimePicker = DateTimeOffset.FromUnixTimeSeconds(this.WizardModel.Time).LocalDateTime.TimeOfDay;
                this.ModulADatePicker = DateTimeOffset.FromUnixTimeSeconds(this.WizardModel.Time).LocalDateTime.Date;
            }
        }

        private async Task InitializeModel(PumpConfigurationModulAWizardModel model)
        {
            _isInitializing = true;
            this.WizardModel = model;
            InitializeParameters();
            SetMinimumMaximumValue();

            await Task.Delay(200);
            this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
            await Task.Delay(200);
            await Task.WhenAll(Reconnect(this.WizardModel.SelectedDeviceUuid));
            this.UserDialogs.HideLoading();

            _isInitializing = false;
        }


        private void SetMaxValuesToDefault()
        {
            NominalValue = 85;
            NominalValueMax = 100;
            NominalValueMin = 0;
            PowerLimit = 105;
            PowerLimitMaxValue = 120;
            PowerLimitMinValue = 0;
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.ShouldExitWizard))
            {
                await CloseWizardPage();
            }

            if (parameters.ContainsKey(NavigationConstants.InitialParameters))
            {
                this.InitialParameters = parameters.GetValue<List<ParameterTExternalEntity>>(NavigationConstants.InitialParameters);

                _initialNominalParameterValue = InitialParameters.FirstOrDefault(x => x.ModulA == ExternalPumpModulAParameterIdValue.NominalValue);

                _initialPowerLimitParameterValue = InitialParameters.FirstOrDefault(x => x.ModulA == ExternalPumpModulAParameterIdValue.PowerLimitMax);

                SetMinimumMaximumValue();
            }

            if (parameters.ContainsKey(NavigationConstants.ParameterCharacteristics))
            {
                this.ParameterCharacteristicList = parameters.GetValue<List<ParameterPumpModulACharacteristicExternalEntity>>(NavigationConstants.ParameterCharacteristics);
            }

            if (parameters.ContainsKey(NavigationConstants.Logs))
            {
                _logs = parameters.GetValue<List<LogEntryTExternalEntity>>(NavigationConstants.Logs);
            }

            if (parameters.ContainsKey(NavigationConstants.CockpitPumpsModulARootModel))
            {
                CockpitPumpsRootModel = parameters.GetValue<CockpitPumpsModulARootModel>(NavigationConstants.CockpitPumpsModulARootModel);
            }

            if (parameters.ContainsKey(NavigationConstants.PumpConfigurationModulAWizardModel))
            {
                PopulateList();

                var model = parameters.GetValue<PumpConfigurationModulAWizardModel>(NavigationConstants.PumpConfigurationModulAWizardModel);
                await InitializeModel(model);
            }
        }

        public async Task CloseAsync()
        {
            await CloseWizardPage();
        }

        public override void Destroy()
        {
            base.Destroy();
            _lostConnectionAlertDialog = null;
            _notifyDisposable?.Dispose();
            _notifyDisposable = null;
            _deviceStatusDisposable?.Dispose();
            _deviceStatusDisposable = null;
            this.EventAggregator.GetEvent<ConnectionEstablishedEvent>().Unsubscribe(_connectionEstablishedSubToken);
            this.EventAggregator.GetEvent<ConnectionLostEvent>().Unsubscribe(_connectionLostSubToken);
        }
    }
}
