﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.Configuration;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Plugin.Messaging;
using Prism.Commands;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EmmeApp.ViewModels
{
    public class PumpConfigurationModulAForm2SystemDescriptionPageViewModel : ViewModelBase, IChainModalViewModel
	{
		private readonly IKeyboardHelper _keyboardHelper;
		private readonly IPopupNavigation _popupNavigation;
		private readonly IMessaging _messagingPlugin;
		private readonly IFileService _fileService;
		private readonly IConfigurationPumpModulAManager _configurationPumpModulAManager;
		private readonly ICockpitPumpsModulAManager _cockpitPumpsModulAManager;
		private readonly ILogPumpManager _logPumpManager;

		public PumpConfigurationModulAForm2SystemDescriptionPageViewModel(
			INavigationService navigationService,
			INavigationHelperService navigationHelperService,
			IAppCenterCustomService appCenterService,
			IPopupNavigation popupNavigation,
			IMessaging messagingPlugin,
			IFileService fileService,
			IUserDialogs userDialogs,
			IKeyboardHelper keyboardHelper,
			IConfigurationPumpModulAManager configurationPumpModulAManager,
			ICockpitPumpsModulAManager cockpitPumpsModulAManager,
			ILogPumpManager logPumpManager) : base(navigationService, navigationHelperService, appCenterService, userDialogs)
		{
			_keyboardHelper = keyboardHelper;
			_popupNavigation = popupNavigation;
			_messagingPlugin = messagingPlugin;
			_fileService = fileService;
			_configurationPumpModulAManager = configurationPumpModulAManager;
			_cockpitPumpsModulAManager = cockpitPumpsModulAManager;
			_logPumpManager = logPumpManager;

			this.WizardCloseCommand = new DelegateCommand(async () => await OnWizardClose());
			this.WizardLeftButtonCommand = new DelegateCommand(async () => await OnBack());
			this.WizardRightButtonCommand = new DelegateCommand(async () => await OnNextButton());
		}

		public CockpitPumpsModulARootModel CockpitPumpsRootModel { get; set; }
		public PumpLogRootModel LogRootModel { get; set; }

		private PumpConfigurationModulAWizardModel _wizardModel;
		public PumpConfigurationModulAWizardModel WizardModel
		{
			get => _wizardModel;
			set => SetProperty(ref _wizardModel, value);
		}

		private bool _systemNameHasError;
		public bool SystemNameHasError
		{
			get => _systemNameHasError;
			set
			{
				SetProperty(ref _systemNameHasError, value);
				PlausibilityCheck();
			}
		}

		private bool _groupNameHasError;
		public bool GroupNameHasError
		{
			get => _groupNameHasError;
			set
			{
				SetProperty(ref _groupNameHasError, value);
				PlausibilityCheck();
			}
		}

		private bool _systemAddressHasError;
		public bool SystemAddressHasError
		{
			get => _systemAddressHasError;
			set
			{
				SetProperty(ref _systemAddressHasError, value);
				PlausibilityCheck();
			}
		}

		private bool _contactNameHasError;
		public bool ContactNameHasError
		{
			get => _contactNameHasError;
			set
			{
				SetProperty(ref _contactNameHasError, value);
				PlausibilityCheck();
			}
		}

		private bool _phoneHasError;
		public bool PhoneHasError
		{
			get => _phoneHasError;
			set
			{
				SetProperty(ref _phoneHasError, value);
				PlausibilityCheck();
			}
		}

		private bool _emailHasError;
		public bool EmailHasError
		{
			get => _emailHasError;
			set
			{
				SetProperty(ref _emailHasError, value);
				PlausibilityCheck();
			}
		}

		private bool _remarksHasError;
		public bool RemarksHasError
		{
			get => _remarksHasError;
			set
			{
				SetProperty(ref _remarksHasError, value);
				PlausibilityCheck();
			}
		}



		private string _wizardTitle = AppResources.PumpConfiguration_ModulA_Title;
		public string WizardTitle
		{
			get => _wizardTitle;
			set => SetProperty(ref _wizardTitle, value);
		}

		private int _wizardStepNumber = 2;
		public int WizardStepNumber
		{
			get => _wizardStepNumber;
			set => SetProperty(ref _wizardStepNumber, value);
		}

		private string _wizardFooterLeftLabel = AppResources.InitialStartup_Wizard_Back;
		public string WizardFooterLeftLabel
		{
			get => _wizardFooterLeftLabel;
			set => SetProperty(ref _wizardFooterLeftLabel, value);
		}
		private string _wizardFooterRightLabel = AppResources.InitialStartup_Wizard_Finish;
		public string WizardFooterRightLabel
		{
			get => _wizardFooterRightLabel;
			set => SetProperty(ref _wizardFooterRightLabel, value);
		}
		private string _wizardErrorMessage;
		public string WizardErrorMessage
		{
			get => _wizardErrorMessage;
			set => SetProperty(ref _wizardErrorMessage, value);
		}
		private bool _wizardErrorMessageVisibility = false;
		public bool WizardErrorMessageVisibility
		{
			get => _wizardErrorMessageVisibility;
			set => SetProperty(ref _wizardErrorMessageVisibility, value);
		}

		private string _helpResourceKey = ResourceConstants.Pump_Configuration_SystemDescription_Help;
		public string HelpResourceKey
		{
			get => _helpResourceKey;
			set => SetProperty(ref _helpResourceKey, value);
		}

		public DelegateCommand WizardCloseCommand { get; private set; }
		public DelegateCommand WizardLeftButtonCommand { get; private set; }
		public DelegateCommand WizardRightButtonCommand { get; private set; }

		private async Task OnBack()
		{
			if (this.WizardModel != null)
			{
				_keyboardHelper.HideKeyboard();

				INavigationParameters parameters = new NavigationParameters();
				parameters.Add(NavigationConstants.PumpConfigurationModulAWizardModel, this.WizardModel);
				await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
			}
		}

		private async Task OnNextButton()
		{
			if (this.WizardModel != null)
			{
				_keyboardHelper.HideKeyboard();

				this.WizardErrorMessage = string.Empty;
				this.WizardErrorMessageVisibility = false;

				if (PlausibilityCheckFailed(out string errorMessage))
				{
					this.WizardErrorMessage = errorMessage;
					this.WizardErrorMessageVisibility = true;
				}
				else
				{
					var dialogAlert = new DialogAlert();
					dialogAlert.LeftButtonText = AppResources.PumpConfiguration_Wizard_Popup_Finish_SendEmail;
					dialogAlert.RightButtonText = AppResources.PumpConfiguration_Wizard_Popup_Finish_Done;
					dialogAlert.ContentText = AppResources.PumpConfiguration_Wizard_Popup_Finish_Content;
					dialogAlert.Title = AppResources.PumpConfiguration_Wizard_Popup_Finish_Title;

					dialogAlert.RightButtonCommand = new DelegateCommand(async () => await CloseWizardPage());
					dialogAlert.LeftButtonCommand = new DelegateCommand(async () => await SendEmail());
					await _popupNavigation.PushAsync(dialogAlert, false);
				}
			}
		}

		private async Task SendEmail()
		{
			if (_messagingPlugin.EmailMessenger.CanSendEmail)
			{
				this.UserDialogs.ShowLoading(AppResources.Loading_Message_EmailRedirect);

				CockpitPumpsRootModel.SerialNumber = CockpitPumpsRootModel.SerialNumber ?? string.Empty;
				var dateNow = DateTime.Now;
				string newLine = "\r\n";

				string subject = $"{this.CockpitPumpsRootModel.SerialNumber}_{dateNow.ToString("yyyyMMddhhmmss")}";
				string filename = $"{this.CockpitPumpsRootModel.SerialNumber}_{dateNow.ToString("yyyyMMddhhmmss")}.txt";

				if (Device.RuntimePlatform == Device.iOS)
				{
					newLine = "\n";
				}

				var configurationSb = _configurationPumpModulAManager.GenerateEmailContent(this.WizardModel,newLine);
				var cockpitSb = _cockpitPumpsModulAManager.GenerateEmailContent(this.CockpitPumpsRootModel,newLine);
				var logSb = _logPumpManager.GenerateEmailContent(this.LogRootModel,newLine);

				configurationSb.Append(newLine);
				configurationSb.Append(cockpitSb);
				configurationSb.Append(newLine);
				configurationSb.Append(logSb);

				string filePath = _fileService.GetFilePath(filename);
				_fileService.WriteToAFile(filePath, configurationSb.ToString(), false);

				var email = new EmailMessageBuilder()
					.Subject(subject)
					.Body(configurationSb.ToString())
					.WithAttachment(filePath, "text/plain")
					.Build();

				_messagingPlugin.EmailMessenger.SendEmail(email);

				this.UserDialogs.HideLoading();
			}
			else
			{
				await this.UserDialogs.AlertAsync(AppResources.Alert_Message_FailedToCreateMail_Message, AppResources.Alert_Message_FailedToCreateMail_Title);
			}
		}

		private async Task OnWizardClose()
		{
			_keyboardHelper.HideKeyboard();

			var dialogAlert = new DialogAlert();
			dialogAlert.LeftButtonText = AppResources.PumpConfiguration_Wizard_Popup_Close_Back;
			dialogAlert.RightButtonText = AppResources.PumpConfiguration_Wizard_Popup_Close_Confirm;
			//dialogAlert.ContentText = AppResources.PumpConfiguration_Wizard_Popup_Close_Content;
			//TODO Change for 2169
			dialogAlert.ContentText = AppResources.InitialStartup_Wizard_Popup_Close_Content;
			dialogAlert.Title = AppResources.PumpConfiguration_Wizard_Popup_Close_Title;
			dialogAlert.RightButtonCommand = new DelegateCommand(async () => await CloseWizardPage());
			await _popupNavigation.PushAsync(dialogAlert, false);
		}

		private async Task CloseWizardPage()
		{
			_keyboardHelper.HideKeyboard();

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.ShouldExitWizard, "");
			parameters.Add(NavigationConstants.CockpitPumpsModulARootModel, CockpitPumpsRootModel);

			await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
		}

		private void PlausibilityCheck()
		{
			this.WizardErrorMessage = string.Empty;
			this.WizardErrorMessageVisibility = false;

			if (PlausibilityCheckFailed(out string errorMessage))
			{
				this.WizardErrorMessage = errorMessage;
				this.WizardErrorMessageVisibility = true;
			}
		}

		public bool PlausibilityCheckFailed(out string errorMessage)
		{
			errorMessage = string.Empty;
			bool textFieldErrors = this.SystemNameHasError || this.GroupNameHasError || this.SystemAddressHasError || this.ContactNameHasError || this.PhoneHasError || this.EmailHasError || this.RemarksHasError;
			if (textFieldErrors)
			{
				errorMessage = AppResources.PumpConfiguration_Error_Message_Entry_Limit_200;
				return textFieldErrors;
			}

			return false;
		}

		private void SetCockpitPumpsDataModel()
		{
			if (WizardModel != null && CockpitPumpsRootModel != null)
			{
				CockpitPumpsRootModel.Time = WizardModel.Time;
				CockpitPumpsRootModel.ControlType = WizardModel.ControlType;
				CockpitPumpsRootModel.TypeOfOperation = WizardModel.TypeOfOperation;
			}
		}

		public override void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);

			if (parameters.ContainsKey(NavigationConstants.PumpConfigurationModulAWizardModel))
			{
				this.WizardModel = parameters.GetValue<PumpConfigurationModulAWizardModel>(NavigationConstants.PumpConfigurationModulAWizardModel);
			}

			if (parameters.ContainsKey(NavigationConstants.CockpitPumpsModulARootModel))
			{
				CockpitPumpsRootModel = parameters.GetValue<CockpitPumpsModulARootModel>(NavigationConstants.CockpitPumpsModulARootModel);
				SetCockpitPumpsDataModel();
			}

			if (parameters.ContainsKey(NavigationConstants.PumpLogRootModel))
			{
				LogRootModel =  parameters.GetValue<PumpLogRootModel>(NavigationConstants.PumpLogRootModel);
			}

			if (parameters.ContainsKey(NavigationConstants.ChainModalStack))
			{
				var configModalStack = parameters.GetValue<ModalChainStack>(NavigationConstants.ChainModalStack);
				configModalStack.Push(this);
			}
		}

		public async Task CloseAsync()
		{
			await CloseWizardPage();
		}
	}
}
