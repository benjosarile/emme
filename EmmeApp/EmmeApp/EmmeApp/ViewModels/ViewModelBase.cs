﻿using Acr.UserDialogs;
using EmmeApp.Common.Utilities.Abstractions;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;

namespace EmmeApp.ViewModels
{
    public class ViewModelBase : BindableBase, IInitialize,  INavigationAware, IDestructible
    {
        protected INavigationService NavigationService { get; private set; }
        protected INavigationHelperService NavigationHelperService { get; private set; }
        protected IUserDialogs UserDialogs { get; private set; }
        protected IEventAggregator EventAggregator { get; private set; }
        protected IAppCenterCustomService AppCenterService { get; set; }

        public ViewModelBase(INavigationService navigationService, INavigationHelperService navigationHelperService, IAppCenterCustomService appCenterService)
        {
            this.NavigationService = navigationService;
            this.NavigationHelperService = navigationHelperService;
            this.AppCenterService = appCenterService;
        }

        public ViewModelBase(INavigationService navigationService, INavigationHelperService navigationHelperService, IAppCenterCustomService appCenterService, IUserDialogs userDialogs)
        {
            this.NavigationService = navigationService;
            this.NavigationHelperService = navigationHelperService;
            this.AppCenterService = appCenterService;
            this.UserDialogs = userDialogs;
        }

        public ViewModelBase(INavigationService navigationService, INavigationHelperService navigationHelperService, IAppCenterCustomService appCenterService, IUserDialogs userDialogs, IEventAggregator eventAggregator)
        {
            this.NavigationService = navigationService;
            this.NavigationHelperService = navigationHelperService;
            this.AppCenterService = appCenterService;
            this.UserDialogs = userDialogs;
            this.EventAggregator = eventAggregator;
        }

        public virtual void OnNavigatedFrom(INavigationParameters parameters)
        {

        }

        public virtual void OnNavigatedTo(INavigationParameters parameters)
        {

        }

        public virtual void Initialize(INavigationParameters parameters)
        {

        }

        public virtual void Destroy()
        {

        }
    }
}
