﻿using Acr.UserDialogs;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Entities;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace EmmeApp.ViewModels
{
    public class AboutPageViewModel : ViewModelBase
    {
        private readonly IBiralGtcManager _biralGtcManager;
        private readonly IBiralContactManager _biralContactManager;

        public AboutPageViewModel(INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IBiralGtcManager biralGtcManager,
            IBiralContactManager biralContactManager)
            : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
        {
            _biralGtcManager = biralGtcManager;
            _biralContactManager = biralContactManager;

            BackCommand = new DelegateCommand(async () => await OnBack());
            CallCommand = new DelegateCommand(() => OnCall(), () => CanNavigateToWeb)
                .ObservesProperty(() => CanNavigateToWeb);
            ShareCommand = new DelegateCommand(async () => await OnShare(), () => CanNavigateToWeb)
                .ObservesProperty(() => CanNavigateToWeb);
            VisitSiteCommand = new DelegateCommand<string>(async (site) => await OnVisitSite(site), (site) => CanNavigateToWeb && !string.IsNullOrEmpty(site))
                .ObservesProperty(() => CanNavigateToWeb);
        }


        private string _appVersionString;
        public string AppVersionString
        {
            get => _appVersionString;
            set => SetProperty(ref _appVersionString, value);
        }

        private string _gtcUrl;
        public string GtcUrl
        {
            get => _gtcUrl;
            set => SetProperty(ref _gtcUrl, value);
        }

        private string _copyrightCompany;
        public string CopyrightCompany
        {
            get => _copyrightCompany;
            set => SetProperty(ref _copyrightCompany, value);
        }

        private BiralContactEntity _biralContact;
        public BiralContactEntity BiralContact
        {
            get => _biralContact;
            set => SetProperty(ref _biralContact, value);
        }

        private bool _canNavigateToWeb = true;
        public bool CanNavigateToWeb
        {
            get => _canNavigateToWeb;
            set => SetProperty(ref _canNavigateToWeb, value);
        }

        public DelegateCommand BackCommand { get; set; }
        public DelegateCommand CallCommand { get; private set; }
        public DelegateCommand ShareCommand { get; private set; }
        public DelegateCommand<string> VisitSiteCommand { get; private set; }

        private async Task OnBack()
        {
            await NavigationUtilities.GoBackAsync(this.NavigationService, null, null, false);
        }
        private void OnCall()
        {
            CanNavigateToWeb = false;

            string phoneNumber = BiralContact.ContactNumber;
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                phoneNumber = phoneNumber.Replace(" ", string.Empty);
                Launcher.TryOpenAsync(string.Format("tel:{0}", phoneNumber));
            }

            CanNavigateToWeb = true;
        }

        private async Task OnShare()
        {
            CanNavigateToWeb = false;

            string emailAddress = BiralContact.EmailAddress;
            if (!string.IsNullOrEmpty(emailAddress))
            {
                try
                {
                    var message = new EmailMessage
                    {
                        Subject = "",
                        Body = "",
                        To = new List<string>() { emailAddress }
                    };

                    await Email.ComposeAsync(message);
                }
                catch (Exception)
                {
                }
            }

            CanNavigateToWeb = true;
        }

        private async Task OnVisitSite(string site)
        {
            CanNavigateToWeb = false;

            if (!string.IsNullOrEmpty(site))
            {
                await Launcher.TryOpenAsync(site);
            }

            CanNavigateToWeb = true;
        }

        private void PopulateValues()
        {
            var gtcEntity = _biralGtcManager.GetCurrentBiralGtc();
            GtcUrl = gtcEntity.Link;

            AppVersionString = $"V{AppInfo.VersionString}";
            CopyrightCompany = string.Format(AppResources.Settings_Copyright_Company, DateTime.Now.Year);
            BiralContact = _biralContactManager.GetDefaultBiralContact();
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            PopulateValues();
        }
    }
}
