﻿using Acr.UserDialogs;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Threading.Tasks;

namespace EmmeApp.ViewModels
{
    public class AppSettingsPageViewModel : ViewModelBase
	{
		private readonly IPopupNavigation _popupNavigation;
		private readonly ISortimentManager _sortimentManager;
		private readonly IPumpReplacementUpdateManager _pumpReplacementUpdateManager;

		public AppSettingsPageViewModel(INavigationService navigationService,
			INavigationHelperService navigationHelperService,
			IAppCenterCustomService appCenterService,
			IUserDialogs userDialogs,
			IEventAggregator eventAggregator,
			IPopupNavigation popupNavigation,
			ISortimentManager sortimentManager,
			IPumpReplacementUpdateManager pumpReplacementUpdateManager)
			: base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
		{
			_popupNavigation = popupNavigation;
			_sortimentManager = sortimentManager;
			_pumpReplacementUpdateManager = pumpReplacementUpdateManager;

			this.BackCommand = new DelegateCommand(async () => await OnBack());
			this.UpdateCommand = new DelegateCommand(async () => await OnUpdate());
		}

		private string _versionLastModifiedDisplay;
		public string VersionLastModifiedDisplay
		{
			get => _versionLastModifiedDisplay;
			set => SetProperty(ref _versionLastModifiedDisplay, value);
		}

		public DelegateCommand BackCommand { get; set; }
		public DelegateCommand UpdateCommand { get; set; }

		private async Task OnBack()
		{
			await NavigationUtilities.GoBackAsync(this.NavigationService, null, null, false);
		}

		private async Task OnUpdate()
		{
			this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
			bool hasConnectivity = true;
			var lastModified = _sortimentManager.GetPumpReplacementLastModified();
			bool hasUpdate = false;
			try
			{
				hasUpdate = await _sortimentManager.HasUpdate(lastModified);
			}
			catch
			{
				hasConnectivity = false;
			}

			this.UserDialogs.HideLoading();

			if (hasUpdate)
			{
				var dialogAlert = new DialogAlert();
				dialogAlert.LeftButtonText = AppResources.PumpReplacement_Update_Popup_Cancel;
				dialogAlert.RightButtonText = AppResources.PumpReplacement_Update_Popup_Update;
				dialogAlert.ContentText = AppResources.PumpReplacement_Update_Popup_NewUpdateAvailable;
				dialogAlert.Title = AppResources.PumpReplacement_Update_Popup_Title;
				dialogAlert.RightButtonCommand = new DelegateCommand(async () => await UpdatePumpReplacement());
				await _popupNavigation.PushAsync(dialogAlert, false);
			}
			else 
			{
				var dialogAlert = new DialogAlertSingle();
				if (hasConnectivity)
				{
					dialogAlert.ContentText = AppResources.PumpReplacement_Update_Popup_NoUpdateAvailable;
				}
				else
				{
					dialogAlert.ContentText = AppResources.PumpReplacement_Update_Popup_NoInternetConnection;
				}

				dialogAlert.Title = AppResources.PumpReplacement_Update_Popup_Title;
				dialogAlert.ButtonText = AppResources.PumpReplacement_Update_Popup_Okay;
				await _popupNavigation.PushAsync(dialogAlert, false);
			}
		}

		private async Task UpdatePumpReplacement()
		{
			this.UserDialogs.ShowLoading(AppResources.PumpReplacement_Update_Popup_Updating);

			bool isSuccess = await _pumpReplacementUpdateManager.Update();

			var dialogAlert = new DialogAlertSingle();

			if (isSuccess)
			{
				dialogAlert.ContentText = AppResources.PumpReplacement_Update_Popup_UpdateSuccess;

				_sortimentManager.SetPumpReplacementLastModified(DateTimeOffset.UtcNow);
				SetVersionLastModifiedDisplay(_sortimentManager.GetPumpReplacementLastModified());
			}
			else
			{
				dialogAlert.ContentText = AppResources.PumpReplacement_Update_Popup_UpdateFailed;
			}

			dialogAlert.Title = AppResources.PumpReplacement_Update_Popup_Title;
			dialogAlert.ButtonText = AppResources.PumpReplacement_Update_Popup_Okay;
			await _popupNavigation.PushAsync(dialogAlert, false);

			this.UserDialogs.HideLoading();
		}

		private void SetVersionLastModifiedDisplay(DateTimeOffset? lastModified)
		{
			if (lastModified.HasValue)
			{
				this.VersionLastModifiedDisplay = lastModified.Value.ToLocalTime().ToString("dd.MM.yyyy - HH:mm");
			}
			else
			{
				this.VersionLastModifiedDisplay = "-";
			}
		}

		public override void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);
			SetVersionLastModifiedDisplay(_sortimentManager.GetPumpReplacementLastModified());
		}
	}
}
