﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Entities;
using EmmeApp.Managers.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace EmmeApp.ViewModels
{
    public class ContactPageViewModel : ViewModelBase
    {
        private readonly IBiralContactManager _biralContactManager;

        public ContactPageViewModel(INavigationService navigationService, 
            INavigationHelperService navigationHelperService, 
            IAppCenterCustomService appCenterService, 
            IUserDialogs userDialogs, 
            IEventAggregator eventAggregator,
            IBiralContactManager biralContactManager) 
            : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
        {
            _biralContactManager = biralContactManager;

            BackCommand = new DelegateCommand(async () => await OnBack());
            CallCommand = new DelegateCommand(() => OnCall(), () => CanNavigateToWeb)
                .ObservesProperty(() => CanNavigateToWeb);
            ShareCommand = new DelegateCommand(async () => await OnShare(), () => CanNavigateToWeb)
                .ObservesProperty(() => CanNavigateToWeb);
            VisitSiteCommand = new DelegateCommand<string>(async (site) => await OnVisitSite(site), (site) => CanNavigateToWeb && !string.IsNullOrEmpty(site))
                .ObservesProperty(() => CanNavigateToWeb);
        }

        private bool showNoTextPumpReplacement;

        private BiralContactEntity _biralContact;
        public BiralContactEntity BiralContact
        {
            get => _biralContact;
            set => SetProperty(ref _biralContact, value);
        }

        private bool _canNavigateToWeb = true;
        public bool CanNavigateToWeb
        {
            get => _canNavigateToWeb;
            set => SetProperty(ref _canNavigateToWeb, value);
        }

        public DelegateCommand BackCommand { get; set; }
        public DelegateCommand CallCommand { get; private set; }
        public DelegateCommand ShareCommand { get; private set; }
        public DelegateCommand<string> VisitSiteCommand { get; private set; }

        private async Task OnBack()
        {
            var navParameters = new NavigationParameters();
            if (showNoTextPumpReplacement)
            {
                navParameters.Add(NavigationConstants.ShowNoTextPumpReplacement, "");
            }

            await NavigationUtilities.GoBackAsync(this.NavigationService, navParameters, null, false);
        }

        private void OnCall()
        {
            CanNavigateToWeb = false;

            string phoneNumber = BiralContact.ContactNumber;
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                phoneNumber = phoneNumber.Replace(" ", string.Empty);
                Launcher.TryOpenAsync(string.Format("tel:{0}", phoneNumber));
            }

            CanNavigateToWeb = true;
        }

        private async Task OnShare()
        {
            CanNavigateToWeb = false;

            string emailAddress = BiralContact.EmailAddress;
            if (!string.IsNullOrEmpty(emailAddress))
            {
                try
                {
                    var message = new EmailMessage
                    {
                        Subject = "",
                        Body = "",
                        To = new List<string>() { emailAddress }
                    };

                    await Email.ComposeAsync(message);
                }
                catch (Exception)
                {
                }
            }

            CanNavigateToWeb = true;
        }

        private async Task OnVisitSite(string site)
        {
            CanNavigateToWeb = false;

            if (!string.IsNullOrEmpty(site))
            {
                await Launcher.TryOpenAsync(site);
            }

            CanNavigateToWeb = true;
        }

        private void PopulateValues()
        {
            BiralContact = _biralContactManager.GetCurrentBiralContact();
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            showNoTextPumpReplacement = parameters.ContainsKey(NavigationConstants.ShowNoTextPumpReplacement);
            PopulateValues();
        }
    }
}
