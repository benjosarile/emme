﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Exceptions;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Entities;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers;
using EmmeApp.Managers.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace EmmeApp.ViewModels
{
    public class DocumentSearchPageViewModel : ViewModelBase
    {
        private readonly IKeyboardHelper _keyboardHelper;
        private readonly IDocumentsManager _documentsManager;
        private readonly IMarketManager _marketManager;
        private readonly IPopupNavigation _popupNavigation;

        public DocumentSearchPageViewModel(INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IKeyboardHelper keyboardHelper,
            IDocumentsManager documentsManager,
            IMarketManager marketManager,
            IPopupNavigation popupNavigation)
            : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
        {
            _documentsManager = documentsManager;
            _keyboardHelper = keyboardHelper;
            _marketManager = marketManager;
            _popupNavigation = popupNavigation;
            this.BackCommand = new DelegateCommand(async () => await OnBack());
            this.SearchDocumentCommand = new DelegateCommand(async () => await OnSearchDocument());
            this.TryAgainCommand = new DelegateCommand(async () => await OnTryAgain());
            this.ItemTappedCommand = new DelegateCommand<SearchResultItemEntity>(async (item) => await OnItemTapped(item));
            this.RemoveSelectedFilterCommand = new DelegateCommand<SearchFilterEntity>(async (item) => await OnRemoveSelectedFilter(item));
        }

        private bool _canSearch = true;
        public bool CanSearch
        {
            get => _canSearch;
            set => SetProperty(ref _canSearch, value);
        }

        private bool _hasSearchResult = true;
        public bool HasSearchResult
        {
            get => _hasSearchResult;
            set => SetProperty(ref _hasSearchResult, value);
        }

        private bool _hasSelectedFilter = false;
        public bool HasSelectedFilter
        {
            get => _hasSelectedFilter;
            set => SetProperty(ref _hasSelectedFilter, value);
        }

        private string _localFilePath;
        public string LocalFilePath
        {
            get => _localFilePath;
            set => SetProperty(ref _localFilePath, value);
        }

        private ObservableCollection<object> _items;
        public ObservableCollection<object> Items
        {
            get => _items;
            set => SetProperty(ref _items, value);
        }

        private ObservableCollection<Grouping<string, SearchFilterEntity>> _originalFilterItems;
        public ObservableCollection<Grouping<string, SearchFilterEntity>> OriginalFilterItems
        {
            get => _originalFilterItems;
            set => SetProperty(ref _originalFilterItems, value);
        }

        private ObservableCollection<SearchFilterEntity> _selectedFilterItems;
        public ObservableCollection<SearchFilterEntity> SelectedFilterItems
        {
            get => _selectedFilterItems;
            set => SetProperty(ref _selectedFilterItems, value);
        }

        public DelegateCommand BackCommand { get; private set; }
        public DelegateCommand SearchDocumentCommand { get; private set; }
        public DelegateCommand TryAgainCommand { get; private set; }
        public DelegateCommand<SearchResultItemEntity> ItemTappedCommand { get; private set; }
        public DelegateCommand<SearchFilterEntity> RemoveSelectedFilterCommand { get; private set; }

        private async Task OnBack()
        {
            _keyboardHelper.HideKeyboard();
            await NavigationUtilities.GoBackAsync(this.NavigationService, null, null, false);
        }

        private async Task SearchAsync(List<SearchFilterEntity> searchFilters = null)
        {
            this.UserDialogs.ShowLoading(AppResources.Document_Search_Searching);

            this.HasSelectedFilter = false;
            this.HasSearchResult = true;

            if (searchFilters == null)
            {
                searchFilters = new List<SearchFilterEntity>();
            }

            try
            {
                var documentLanguage = _marketManager.GetDocumentLanguage();
                var searchRootEntity = await _documentsManager.Search(string.Join("|", searchFilters.Select(x => x.Id)), documentLanguage.Item2, documentLanguage.Item2, documentLanguage.Item1);
                if (searchRootEntity != null)
                {
                    this.Items = new ObservableCollection<object>(searchRootEntity.Item1);
                    this.OriginalFilterItems = new ObservableCollection<Grouping<string, SearchFilterEntity>>(searchRootEntity.Item2);

                    if (!this.Items.Any())
                    {
                        this.HasSearchResult = false;
                    }
                }
                else
                {
                    this.HasSearchResult = false;
                }

                this.CanSearch = true;
            }
            catch (NoInternetConnectivityException)
            {
                this.CanSearch = false;
            }

            this.HasSelectedFilter = this.SelectedFilterItems != null && this.SelectedFilterItems.Any();

            this.UserDialogs.HideLoading();
        }

        private async Task OnTryAgain()
        {
            await SearchAsync(this.SelectedFilterItems?.ToList());
        }

        private async Task OnSearchDocument()
        {
            if (this.SelectedFilterItems == null)
            {
                this.SelectedFilterItems = new ObservableCollection<SearchFilterEntity>();
            }

            var parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.DocumentFilter, this.OriginalFilterItems);
            parameters.Add(NavigationConstants.DocumentSelectedFilter, this.SelectedFilterItems);
            await NavigationUtilities.NavigateAsync(this.NavigationService, ViewNames.DocumentSearchFilterPage, parameters, null, false);
        }

        private async Task OnItemTapped(SearchResultItemEntity item)
        {
            this.UserDialogs.ShowLoading(AppResources.Document_Search_Opening);
            try
            {
                string pdfLink = await _documentsManager.GetLink(item.DocumentLink);

                if (pdfLink != null && pdfLink.Contains("?"))
                {
                    this.UserDialogs.HideLoading();
                    pdfLink = pdfLink.Split('?').FirstOrDefault();
                    string highlightedText = item.PageNumber > 0 ? this.SelectedFilterItems.FirstOrDefault(x => x.Id == x.Name).Id : string.Empty;
                    await ViewDocumentNavigation(item.Name, pdfLink, item.PageNumber, highlightedText);
                }
                else
                {
                    this.UserDialogs.HideLoading();
                    await this.UserDialogs.AlertAsync(AppResources.Document_Search_CantLoadDocument);
                }
            }
            catch (NoInternetConnectivityException)
            {
                this.UserDialogs.HideLoading();
                await this.UserDialogs.AlertAsync(AppResources.Document_Search_NoInternetConnection);
            }
        }

        private async Task OnRemoveSelectedFilter(SearchFilterEntity item)
        {
            this.SelectedFilterItems.Remove(item);
            await SearchAsync(this.SelectedFilterItems.ToList());
        }

        private async Task ViewDocumentNavigation(string title, string pdfLink, long pageNumber, string highlightedText)
        {
            var parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.DocumentTitle, title);
            parameters.Add(NavigationConstants.DocumentFilePath, pdfLink);
            parameters.Add(NavigationConstants.DocumentPageNumber, pageNumber);
            parameters.Add(NavigationConstants.DocumentHighlightedText, highlightedText);

            await NavigationUtilities.NavigateAsync(this.NavigationService, ViewNames.DocumentViewPage, parameters, null, false);
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.GetNavigationMode() == NavigationMode.New || parameters.ContainsKey(NavigationConstants.DocumentSelectedFilter))
            {
                if (parameters.ContainsKey(NavigationConstants.DocumentSelectedFilter))
                {
                    this.SelectedFilterItems = parameters.GetValue<ObservableCollection<SearchFilterEntity>>(NavigationConstants.DocumentSelectedFilter);
                }

                await SearchAsync(this.SelectedFilterItems?.ToList());

            }
        }
    }
}
