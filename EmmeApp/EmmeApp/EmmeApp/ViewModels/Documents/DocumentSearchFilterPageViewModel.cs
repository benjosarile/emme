﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Exceptions;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Entities;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers;
using EmmeApp.Managers.Abstractions;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace EmmeApp.ViewModels
{
    public class DocumentSearchFilterPageViewModel : ViewModelBase
    {
        private readonly IDocumentsManager _documentsManager;
        private readonly IKeyboardHelper _keyboardHelper;
        private readonly CompositeDisposable _subscriptions;

        public DocumentSearchFilterPageViewModel(INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IUserDialogs userDialogs,
            IKeyboardHelper keyboardHelper,
            IDocumentsManager documentsManager) : base(navigationService, navigationHelperService, appCenterService, userDialogs)
        {
            _documentsManager = documentsManager;
            _keyboardHelper = keyboardHelper;

            this.BackCommand = new DelegateCommand(async () => await OnBack());
            this.SelectedFilterItemCommand = new DelegateCommand<SearchFilterEntity>(async (item) => await OnSelectedFilterItem(item));
            this.SearchDocumentCommand = new DelegateCommand(async () => await OnSearchDocument());

            _subscriptions = new CompositeDisposable();
            var searchEventPattern = Observable.FromEventPattern<PropertyChangedEventArgs>(this, nameof(PropertyChanged))
                .Where(x => x.EventArgs.PropertyName == nameof(this.SearchQuery))
                .Throttle(TimeSpan.FromMilliseconds(250))
                .Subscribe(foundList => ExecuteTextChangedCommand());

            _subscriptions.Add(searchEventPattern);
        }

        private string _searchQuery;
        public string SearchQuery
        {
            get => _searchQuery;
            set => SetProperty(ref _searchQuery, value);
        }

        private ObservableCollection<Grouping<string, SearchFilterEntity>> _originalFilterItems;
        public ObservableCollection<Grouping<string, SearchFilterEntity>> OriginalFilterItems
        {
            get => _originalFilterItems;
            set => SetProperty(ref _originalFilterItems, value);
        }

        private ObservableCollection<Grouping<string, SearchFilterEntity>> _filterItems;
        public ObservableCollection<Grouping<string, SearchFilterEntity>> FilterItems
        {
            get => _filterItems;
            set => SetProperty(ref _filterItems, value);
        }

        private ObservableCollection<SearchFilterEntity> _selectedFilterItems;
        public ObservableCollection<SearchFilterEntity> SelectedFilterItems
        {
            get => _selectedFilterItems;
            set => SetProperty(ref _selectedFilterItems, value);
        }

        public DelegateCommand BackCommand { get; private set; }
        public DelegateCommand<SearchFilterEntity> SelectedFilterItemCommand { get; private set; }
        public DelegateCommand SearchDocumentCommand { get; set; }

        private async Task OnBack()
        {
            _keyboardHelper.HideKeyboard();
            await NavigationUtilities.GoBackAsync(this.NavigationService, null, null, false);
        }

        private async Task OnSelectedFilterItem(SearchFilterEntity item)
        {
            if (this.SelectedFilterItems == null)
            {
                this.SelectedFilterItems = new ObservableCollection<SearchFilterEntity>();
            }

            string itemId = item.Id.ToLower();

            if (itemId.StartsWith("catalog_") && itemId.Any(char.IsDigit))
            {
                try
                {
                    this.UserDialogs.ShowLoading(AppResources.Document_Search_Opening);
                    var resultEntity = await _documentsManager.GetCatalogDetails(AppConstants.DocumentPortal, itemId.Split('_')[1]);

                    if (resultEntity != null)
                    {
                        string temporaryPdfLink = string.Format("{0}{1}", resultEntity.CatalogDownloadUrl, "&inline=true");
                        string pdfLink = await _documentsManager.GetLink(temporaryPdfLink);

                        if (pdfLink != null && pdfLink.Contains("?"))
                        {
                            this.UserDialogs.HideLoading();
                            pdfLink = pdfLink.Split('?').FirstOrDefault();
                            await ViewDocumentNavigation(item.Name, pdfLink, 0, string.Empty);
                        }
                        else
                        {
                            this.UserDialogs.HideLoading();
                            await this.UserDialogs.AlertAsync(AppResources.Document_Search_CantLoadDocument);
                        }
                    }
                    this.UserDialogs.HideLoading();
                }
                catch (NoInternetConnectivityException)
                {
                    this.UserDialogs.HideLoading();
                    await this.UserDialogs.AlertAsync(AppResources.Document_Search_NoInternetConnection);
                }
            }
            else
            {
                this.SelectedFilterItems.Add(item);

                var parameters = new NavigationParameters();
                parameters.Add(NavigationConstants.DocumentSelectedFilter, this.SelectedFilterItems);
                await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
            }
        }

        private async Task OnSearchDocument()
        {
            var parameters = new NavigationParameters();

            if(this.SelectedFilterItems == null)
            {
                this.SelectedFilterItems = new ObservableCollection<SearchFilterEntity>();
            }

            if (!string.IsNullOrEmpty(this.SearchQuery) && !this.SelectedFilterItems.Any(x => x.Id == this.SearchQuery))
            {
                var searchFilterEntity = new SearchFilterEntity();
                searchFilterEntity.Id = this.SearchQuery;
                searchFilterEntity.Name = this.SearchQuery;
                this.SelectedFilterItems.Add(searchFilterEntity);

                parameters.Add(NavigationConstants.DocumentSelectedFilter, this.SelectedFilterItems);
            }

            await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
            this.SearchQuery = string.Empty;
        }

        private void ExecuteTextChangedCommand()
        {
            if (string.IsNullOrEmpty(this.SearchQuery))
            {
                this.FilterItems = this.OriginalFilterItems;
            }
            else
            {
                var filterItems = new List<Grouping<string, SearchFilterEntity>>();

                if (OriginalFilterItems != null)
                {
                    foreach (var groupItems in this.OriginalFilterItems)
                    {
                        var filteredGroupItems = groupItems.Where(x => x.Name.ToUpper().Contains(this.SearchQuery.ToUpper())).ToList();
                        if (filteredGroupItems.Any())
                        {
                            filterItems.Add(new Grouping<string, SearchFilterEntity>(groupItems.Key, filteredGroupItems));
                        }
                    }
                }

                this.FilterItems = new ObservableCollection<Grouping<string, SearchFilterEntity>>(filterItems);
            }
        }

        private async Task ViewDocumentNavigation(string title, string pdfLink, long pageNumber, string highlightedText)
        {
            var parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.DocumentTitle, title);
            parameters.Add(NavigationConstants.DocumentFilePath, pdfLink);
            parameters.Add(NavigationConstants.DocumentPageNumber, pageNumber);
            parameters.Add(NavigationConstants.DocumentHighlightedText, highlightedText);

            await NavigationUtilities.NavigateAsync(this.NavigationService, ViewNames.DocumentViewPage, parameters, null, false);
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.DocumentFilter))
            {
                this.OriginalFilterItems = new ObservableCollection<Grouping<string, SearchFilterEntity>>(parameters.GetValue<ObservableCollection<Grouping<string, SearchFilterEntity>>>(NavigationConstants.DocumentFilter));
                this.FilterItems = new ObservableCollection<Grouping<string, SearchFilterEntity>>(this.OriginalFilterItems);
            }

            if (parameters.ContainsKey(NavigationConstants.DocumentSelectedFilter))
            {
                this.SelectedFilterItems = new ObservableCollection<SearchFilterEntity>(parameters.GetValue<ObservableCollection<SearchFilterEntity>>(NavigationConstants.DocumentSelectedFilter));
            }
        }

        public override void Destroy()
        {
            base.Destroy();
            _subscriptions.Dispose();
        }
    }
}
