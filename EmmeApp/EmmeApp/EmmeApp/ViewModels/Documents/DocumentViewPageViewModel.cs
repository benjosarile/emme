﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Utilities.Abstractions;
using Prism.Commands;
using Prism.Navigation;
using System.Threading.Tasks;

namespace EmmeApp.ViewModels
{
    public class DocumentViewPageViewModel : ViewModelBase
	{
		public DocumentViewPageViewModel(INavigationService navigationService, 
			INavigationHelperService navigationHelperService, 
			IAppCenterCustomService appCenterService, 
			IUserDialogs userDialogs) : base(navigationService, navigationHelperService, appCenterService, userDialogs)
		{
			this.BackCommand = new DelegateCommand(async () => await OnBack());
		}

		private string _title;
		public string Title
		{
			get => _title;
			set => SetProperty(ref _title, value);
		}

		private string _filePath;
		public string FilePath                                                                                                                                     
		{
			get => _filePath;
			set => SetProperty(ref _filePath, value);
		}

		private long _pageNumber;
		public long PageNumber
		{
			get => _pageNumber;
			set => SetProperty(ref _pageNumber, value);
		}

		private string _highlightedText;
		public string HighlightedText
		{
			get => _highlightedText;
			set => SetProperty(ref _highlightedText, value);
		}

		public DelegateCommand BackCommand { get; private set; }

		private async Task OnBack()
		{
			await NavigationUtilities.GoBackAsync(this.NavigationService, null, null, false);
		}

		public override void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);

			if (parameters.ContainsKey(NavigationConstants.DocumentTitle))
			{
				Title = parameters.GetValue<string>(NavigationConstants.DocumentTitle);
			}

			if (parameters.ContainsKey(NavigationConstants.DocumentPageNumber))
			{
				PageNumber = parameters.GetValue<long>(NavigationConstants.DocumentPageNumber);
			}

			if (parameters.ContainsKey(NavigationConstants.DocumentHighlightedText))
			{
				HighlightedText = parameters.GetValue<string>(NavigationConstants.DocumentHighlightedText);
			}

			if (parameters.ContainsKey(NavigationConstants.DocumentFilePath))
			{
				FilePath = parameters.GetValue<string>(NavigationConstants.DocumentFilePath);
			}
		}
	}
}
