﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities.External;
using EmmeApp.Helpers;
using EmmeApp.Localization.Resources;
using Humanizer;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmmeApp.ViewModels
{
    public class InitialStartupForm4PumpAndMotorConfigurationPageViewModel : ViewModelBase, IChainModalViewModel
	{
		private readonly IPopupNavigation _popupNavigation;
		private readonly IEnumProvider _enumHelper;

		private ModalChainStack _wizardModalStack = null;

		public InitialStartupForm4PumpAndMotorConfigurationPageViewModel(INavigationService navigationService,
			INavigationHelperService navigationHelperService,
			IAppCenterCustomService appCenterService,
			IUserDialogs userDialogs,
			IEventAggregator eventAggregator,
			IPopupNavigation popupNavigation,
			IEnumProvider enumHelper)
			: base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
		{
			_popupNavigation = popupNavigation;
			_enumHelper = enumHelper;

			this.WizardCloseCommand = new DelegateCommand(async () => await OnWizardClose());
			this.WizardLeftButtonCommand = new DelegateCommand(async () => await OnBack());
			this.WizardRightButtonCommand = new DelegateCommand(async () => await OnNextButton());
		}

		private int _wizardStepNumber = 4;
		public int WizardStepNumber
		{
			get => _wizardStepNumber;
			set => SetProperty(ref _wizardStepNumber, value);
		}

		private string _wizardErrorMessage;
		public string WizardErrorMessage
		{
			get => _wizardErrorMessage;
			set => SetProperty(ref _wizardErrorMessage, value);
		}
		private bool _wizardErrorMessageVisibility = false;
		public bool WizardErrorMessageVisibility
		{
			get => _wizardErrorMessageVisibility;
			set => SetProperty(ref _wizardErrorMessageVisibility, value);
		}

		private string _wizardFooterLeftLabel = AppResources.InitialStartup_Wizard_Back;
		public string WizardFooterLeftLabel
		{
			get => _wizardFooterLeftLabel;
			set => SetProperty(ref _wizardFooterLeftLabel, value);
		}

		private string _wizardFooterRightLabel = AppResources.InitialStartup_Wizard_Next;
		public string WizardFooterRightLabel
		{
			get => _wizardFooterRightLabel;
			set => SetProperty(ref _wizardFooterRightLabel, value);
		}

		private string _helpResourceKey = ResourceConstants.InitialStartup_Form4_PumpAndMotorConfigurationPage_Help;
		public string HelpResourceKey
		{
			get => _helpResourceKey;
			set => SetProperty(ref _helpResourceKey, value);
		}

		private InitialStartupWizardModel _wizardModel;
		public InitialStartupWizardModel WizardModel
		{
			get => _wizardModel;
			set => SetProperty(ref _wizardModel, value);
		}

		private List<ParameterControlUnitCharacteristicExternalEntity> _parameterCharacteristicList;
		public List<ParameterControlUnitCharacteristicExternalEntity> ParameterCharacteristicList
		{
			get => _parameterCharacteristicList;
			set => SetProperty(ref _parameterCharacteristicList, value);
		}

		private List<string> _numberOfPumpList;
		public List<string> NumberOfPumpList
		{
			get => _numberOfPumpList;
			set => SetProperty(ref _numberOfPumpList, value);
		}

		private List<string> _systemMotorModeList;
		public List<string> SystemMotorModeList
		{
			get => _systemMotorModeList;
			set => SetProperty(ref _systemMotorModeList, value);
		}

		private string _selectedNumberOfPump = "1";
		public string SelectedNumberOfPump
		{
			get => _selectedNumberOfPump;
			set
			{
				if (value.Equals(AppResources.ControlUnit_SystemDoubleMotor_2Pumps))
				{
					this.IsPump2Visible = true;
				}
				else
				{
					this.IsPump2Visible = false;
				}

				SetNumberOfPumpsValue(value);
				SetProperty(ref _selectedNumberOfPump, value);
			}
		}

		private bool _isPump2Visible;
		public bool IsPump2Visible
		{
			get => _isPump2Visible;
			set => SetProperty(ref _isPump2Visible, value);
		}

		private bool _systemMotorModePump1HasError;
		public bool SystemMotorModePump1HasError
		{
			get => _systemMotorModePump1HasError;
			set => SetProperty(ref _systemMotorModePump1HasError, value);
		}

		private string _selectedSystemMotorModePump1;
		public string SelectedSystemMotorModePump1
		{
			get => _selectedSystemMotorModePump1;
			set
			{
				SetControlUnitMotorModeValue(value, 1);
				SetProperty(ref _selectedSystemMotorModePump1, value);
				SystemMotorModePumpPlausibilityCheck();
			}
		}

		private bool _systemMotorModePump2HasError;
		public bool SystemMotorModePump2HasError
		{
			get => _systemMotorModePump2HasError;
			set => SetProperty(ref _systemMotorModePump2HasError, value);
		}

		private string _selectedSystemMotorModePump2;
		public string SelectedSystemMotorModePump2
		{
			get => _selectedSystemMotorModePump2;
			set
			{
				SetControlUnitMotorModeValue(value, 2);
				SetProperty(ref _selectedSystemMotorModePump2, value);
				SystemMotorModePumpPlausibilityCheck();
			}
		}

		private List<ParameterTExternalEntity> _initialParameters;
		public List<ParameterTExternalEntity> InitialParameters
		{
			get => _initialParameters;
			set => SetProperty(ref _initialParameters, value);
		}

		private string _pumpAndMotorConfigurationMotorRateCurrentPump1Unit = AppResources.WizardForm_UnitInA;
		public string PumpAndMotorConfigurationMotorRateCurrentPump1Unit
		{
			get => _pumpAndMotorConfigurationMotorRateCurrentPump1Unit;
			set => SetProperty(ref _pumpAndMotorConfigurationMotorRateCurrentPump1Unit, value);
		}

		private double _pumpAndMotorConfigurationMotorRateCurrentPump1Max = 65;
		public double PumpAndMotorConfigurationMotorRateCurrentPump1Max
		{
			get => _pumpAndMotorConfigurationMotorRateCurrentPump1Max;
			set => SetProperty(ref _pumpAndMotorConfigurationMotorRateCurrentPump1Max, value);
		}

		private double _pumpAndMotorConfigurationMotorRateCurrentPump1Min;
		public double PumpAndMotorConfigurationMotorRateCurrentPump1Min
		{
			get => _pumpAndMotorConfigurationMotorRateCurrentPump1Min;
			set => SetProperty(ref _pumpAndMotorConfigurationMotorRateCurrentPump1Min, value);
		}

		private double _pumpAndMotorConfigurationMotorRateCurrentPump1Step;
		public double PumpAndMotorConfigurationMotorRateCurrentPump1Step
		{
			get => _pumpAndMotorConfigurationMotorRateCurrentPump1Step;
			set => SetProperty(ref _pumpAndMotorConfigurationMotorRateCurrentPump1Step, value);
		}

		private string _pumpAndMotorConfigurationMotorRateCurrentPump2Unit = AppResources.WizardForm_UnitInA;
		public string PumpAndMotorConfigurationMotorRateCurrentPump2Unit
		{
			get => _pumpAndMotorConfigurationMotorRateCurrentPump2Unit;
			set => SetProperty(ref _pumpAndMotorConfigurationMotorRateCurrentPump2Unit, value);
		}

		private double _pumpAndMotorConfigurationMotorRateCurrentPump2Max = 65;
		public double PumpAndMotorConfigurationMotorRateCurrentPump2Max
		{
			get => _pumpAndMotorConfigurationMotorRateCurrentPump2Max;
			set => SetProperty(ref _pumpAndMotorConfigurationMotorRateCurrentPump2Max, value);
		}

		private double _pumpAndMotorConfigurationMotorRateCurrentPump2Min;
		public double PumpAndMotorConfigurationMotorRateCurrentPump2Min
		{
			get => _pumpAndMotorConfigurationMotorRateCurrentPump2Min;
			set => SetProperty(ref _pumpAndMotorConfigurationMotorRateCurrentPump2Min, value);
		}

		private double _pumpAndMotorConfigurationMotorRateCurrentPump2Step;
		public double PumpAndMotorConfigurationMotorRateCurrentPump2Step
		{
			get => _pumpAndMotorConfigurationMotorRateCurrentPump2Step;
			set => SetProperty(ref _pumpAndMotorConfigurationMotorRateCurrentPump2Step, value);
		}


		public DelegateCommand WizardCloseCommand { get; private set; }
		public DelegateCommand WizardLeftButtonCommand { get; private set; }
		public DelegateCommand WizardRightButtonCommand { get; private set; }
		public DelegateCommand<string> SelectSystemTypeCommand { get; private set; }

		private async Task OnNextButton()
		{
			if (this.WizardModel != null)
			{
				this.WizardErrorMessage = string.Empty;
				this.WizardErrorMessageVisibility = false;


				if (PlausibilityCheckFailed(out string errorMessage))
				{
					this.WizardErrorMessage = errorMessage;
					this.WizardErrorMessageVisibility = true;
				}
				else
				{
					INavigationParameters parameters = new NavigationParameters();
					parameters.Add(NavigationConstants.InitialParameters, this.InitialParameters);
					parameters.Add(NavigationConstants.InitialStartupWizardModel, this.WizardModel);
					parameters.Add(NavigationConstants.ParameterCharacteristics, this.ParameterCharacteristicList);
					parameters.Add(NavigationConstants.ChainModalStack, _wizardModalStack);

					await NavigationUtilities.NavigateAsync(this.NavigationService, ViewNames.InitialStartupForm5TimingConfigurationPage, parameters, null, false);
				}
			}
		}

		private bool PlausibilityCheckFailed(out string errorMessage)
		{
			errorMessage = string.Empty;
			this.SystemMotorModePump1HasError = string.IsNullOrEmpty(this.SelectedSystemMotorModePump1);
			this.SystemMotorModePump2HasError = string.IsNullOrEmpty(this.SelectedSystemMotorModePump2) && this.IsPump2Visible;
			if (this.SystemMotorModePump1HasError)
			{
				errorMessage = AppResources.InitialStartupForm4_PumpAndMotorConfigurationPage_Picker_SystemMotorMode_ErrorMessage;
				return true;
			}

			if (this.SystemMotorModePump2HasError)
			{
				errorMessage = AppResources.InitialStartupForm4_PumpAndMotorConfigurationPage_Picker_SystemMotorMode_ErrorMessage;
				return true;
			}

			if (this.WizardModel.PumpAndMotorConfigurationMotorRateCurrentPump1.Equals(0))
			{
				errorMessage = AppResources.InitialStartupForm4_PumpAndMotorConfigurationPage_Slider_MotorRateCurrentPump1_ErrorMessage;
				return true;
			}
			if (this.WizardModel.PumpAndMotorConfigurationMotorRateCurrentPump2.Equals(0) && this.IsPump2Visible)
			{
				errorMessage = AppResources.InitialStartupForm4_PumpAndMotorConfigurationPage_Slider_MotorRateCurrentPump2_ErrorMessage;
				return true;
			}

			return false;
		}

		private void SystemMotorModePumpPlausibilityCheck()
		{
			this.SystemMotorModePump1HasError = string.IsNullOrEmpty(this.SelectedSystemMotorModePump1) && this.WizardErrorMessageVisibility;
			this.SystemMotorModePump2HasError = string.IsNullOrEmpty(this.SelectedSystemMotorModePump2) && this.WizardErrorMessageVisibility && this.IsPump2Visible;
			if (!this.SystemMotorModePump1HasError && !this.SystemMotorModePump2HasError)
			{
				this.WizardErrorMessage = string.Empty;
				this.WizardErrorMessageVisibility = false;
				if (this.WizardModel != null)
				{

					if (this.WizardModel.PumpAndMotorConfigurationMotorRateCurrentPump1.Equals(0) && !string.IsNullOrEmpty(this.SelectedSystemMotorModePump1))
					{
						this.WizardErrorMessage = AppResources.InitialStartupForm4_PumpAndMotorConfigurationPage_Slider_MotorRateCurrentPump1_ErrorMessage;
						this.WizardErrorMessageVisibility = true;
					}
					else if (this.WizardModel.PumpAndMotorConfigurationMotorRateCurrentPump2.Equals(0) && !string.IsNullOrEmpty(this.SelectedSystemMotorModePump2) && this.IsPump2Visible)
					{
						this.WizardErrorMessage = AppResources.InitialStartupForm4_PumpAndMotorConfigurationPage_Slider_MotorRateCurrentPump2_ErrorMessage;
						this.WizardErrorMessageVisibility = true;
					}
				}
			}
		}


		private void SetNumberOfPumpsValue(string value)
		{
			if (this.WizardModel != null && !string.IsNullOrEmpty(value))
			{
				this.WizardModel.PumpAndMotorConfigurationNumberOfPumpValue = EnumDehumanizeExtensions.DehumanizeTo<ControlUnitSystemDoubleMotorValue>(value);
			}
		}

		private void SetSelectedNumberOfPump(ControlUnitSystemDoubleMotorValue value)
		{
			this.SelectedNumberOfPump = value.Humanize();

		}

		private void SetControlUnitMotorModeValue(string value, int pumpNumber)
		{
			var controlUnitMotorModeValue = ControlUnitMotorModeValue.NotConfigured;

			if (this.WizardModel != null)
			{
				if (!string.IsNullOrEmpty(value))
				{
					controlUnitMotorModeValue = EnumDehumanizeExtensions.DehumanizeTo<ControlUnitMotorModeValue>(value);
				}

				if (pumpNumber.Equals(1))
				{
					this.WizardModel.PumpAndMotorConfigurationSystemMotorModePump1Value = controlUnitMotorModeValue;
				}
				else
				{
					this.WizardModel.PumpAndMotorConfigurationSystemMotorModePump2Value = controlUnitMotorModeValue;
				}
			}
		}


		private void SetSelectedSystemMotorMode(ControlUnitMotorModeValue value, int pumpNumber)
		{
			string systemMotorModePump = string.Empty;

			if (value != ControlUnitMotorModeValue.NotConfigured)
			{
				systemMotorModePump = value.Humanize();
			}

			if (pumpNumber.Equals(1))
			{
				this.SelectedSystemMotorModePump1 = systemMotorModePump;
			}
			else
			{
				this.SelectedSystemMotorModePump2 = systemMotorModePump;
			}
		}


		private async Task OnWizardClose()
		{
			DialogAlert dialogAlert = new DialogAlert();
			dialogAlert.LeftButtonText = AppResources.InitialStartup_Wizard_Popup_Close_Back;
			dialogAlert.RightButtonText = AppResources.InitialStartup_Wizard_Popup_Close_Confirm;
			dialogAlert.ContentText = AppResources.InitialStartup_Wizard_Popup_Close_Content;
			dialogAlert.Title = AppResources.InitialStartup_Wizard_Popup_Close_Title;
			dialogAlert.RightButtonCommand = new DelegateCommand(async () => await CloseWizardPage());
			await _popupNavigation.PushAsync(dialogAlert, false);
		}

		private async Task CloseWizardPage()
		{
			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.ShouldExitWizard, "");

			await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
		}

		private async Task OnBack()
		{
			if (this.WizardModel != null)
			{
				INavigationParameters parameters = new NavigationParameters();
				parameters.Add(NavigationConstants.InitialParameters, this.InitialParameters);
				parameters.Add(NavigationConstants.InitialStartupWizardModel, this.WizardModel);
				parameters.Add(NavigationConstants.ParameterCharacteristics, this.ParameterCharacteristicList);

				await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
			}
		}

		private void PopulateList()
		{
			this.NumberOfPumpList = _enumHelper.PopulateList<ControlUnitSystemDoubleMotorValue>()
				.Select(x => x.Humanize())
				.ToList();

			this.SystemMotorModeList = _enumHelper.PopulateList<ControlUnitMotorModeValue>()
				.FindAll(u => !(new[] { ControlUnitMotorModeValue.NotConfigured })
				.Contains(u))
				.Select(x => x.Humanize())
				.ToList();
		}

		private void SetMinimumMaximumValue()
		{
			var pumpAndMotorConfigurationMotorRateCurrentPump1Param = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.MotorRatedCurrentMotor0);

			if (pumpAndMotorConfigurationMotorRateCurrentPump1Param != null && pumpAndMotorConfigurationMotorRateCurrentPump1Param.Scaling != null)
			{
				double step = Math.Round(pumpAndMotorConfigurationMotorRateCurrentPump1Param.Scaling.Step, 5);

				var valMax = pumpAndMotorConfigurationMotorRateCurrentPump1Param.Scaling.Max * step;
				var valMin = pumpAndMotorConfigurationMotorRateCurrentPump1Param.Scaling.Min * step;

				if (valMax > valMin)
				{
					if (valMax > this.PumpAndMotorConfigurationMotorRateCurrentPump1Min)
					{
						this.PumpAndMotorConfigurationMotorRateCurrentPump1Max = valMax;
					}
					else
					{
						this.PumpAndMotorConfigurationMotorRateCurrentPump1Min = valMin;
						this.PumpAndMotorConfigurationMotorRateCurrentPump1Max = valMax;
					}
				}

				this.PumpAndMotorConfigurationMotorRateCurrentPump1Min = valMin;
				this.PumpAndMotorConfigurationMotorRateCurrentPump1Step = step;
				string unit = _enumHelper.GetWizardUnitText(pumpAndMotorConfigurationMotorRateCurrentPump1Param.Unit);
				this.PumpAndMotorConfigurationMotorRateCurrentPump1Unit = string.IsNullOrEmpty(unit) ? PumpAndMotorConfigurationMotorRateCurrentPump1Unit : unit;
			}

			var pumpAndMotorConfigurationMotorRateCurrentPump2Param = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.MotorRatedCurrentMotor1);

			if (pumpAndMotorConfigurationMotorRateCurrentPump2Param != null && pumpAndMotorConfigurationMotorRateCurrentPump2Param.Scaling != null)
			{
				double step = Math.Round(pumpAndMotorConfigurationMotorRateCurrentPump2Param.Scaling.Step, 5);

				var valMax = pumpAndMotorConfigurationMotorRateCurrentPump2Param.Scaling.Max * step;
				var valMin = pumpAndMotorConfigurationMotorRateCurrentPump2Param.Scaling.Min * step;

				if (valMax > valMin)
				{
					if (valMax > this.PumpAndMotorConfigurationMotorRateCurrentPump2Min)
					{
						this.PumpAndMotorConfigurationMotorRateCurrentPump2Max = valMax;
					}
					else
					{
						this.PumpAndMotorConfigurationMotorRateCurrentPump2Min = valMin;
						this.PumpAndMotorConfigurationMotorRateCurrentPump2Max = valMax;
					}
				}

				this.PumpAndMotorConfigurationMotorRateCurrentPump2Min = valMin;
				this.PumpAndMotorConfigurationMotorRateCurrentPump2Step = step;
				string unit = _enumHelper.GetWizardUnitText(pumpAndMotorConfigurationMotorRateCurrentPump2Param.Unit);
				this.PumpAndMotorConfigurationMotorRateCurrentPump2Unit = string.IsNullOrEmpty(unit) ? PumpAndMotorConfigurationMotorRateCurrentPump2Unit : unit;
			}
		}

		private void SetMaxValuesToDefault()
		{
			WizardModel.PumpAndMotorConfigurationMotorRateCurrentPump1 = 0;
			PumpAndMotorConfigurationMotorRateCurrentPump1Max = 65;
			PumpAndMotorConfigurationMotorRateCurrentPump1Min = 0;

			WizardModel.PumpAndMotorConfigurationMotorRateCurrentPump2 = 0;
			PumpAndMotorConfigurationMotorRateCurrentPump2Max = 65;
			PumpAndMotorConfigurationMotorRateCurrentPump2Min = 0;
		}

		public override async void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);

			if (parameters.ContainsKey(NavigationConstants.ShouldExitWizard))
			{
				SetMaxValuesToDefault();
				await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
			}

			if (parameters.ContainsKey(NavigationConstants.InitialParameters))
			{
				this.InitialParameters = parameters.GetValue<List<ParameterTExternalEntity>>(NavigationConstants.InitialParameters);
				SetMinimumMaximumValue();
			}

			if (parameters.ContainsKey(NavigationConstants.InitialStartupWizardModel))
			{
				PopulateList();

				this.WizardModel = parameters.GetValue<InitialStartupWizardModel>(NavigationConstants.InitialStartupWizardModel);
				SetSelectedNumberOfPump(this.WizardModel.PumpAndMotorConfigurationNumberOfPumpValue);

				var systemMotorMode1 = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.SystemMotorMode0);

				if (systemMotorMode1 != null && (int)this.WizardModel.PumpAndMotorConfigurationSystemMotorModePump1Value > systemMotorMode1.Scaling.Max)
				{
					this.WizardModel.PumpAndMotorConfigurationSystemMotorModePump1Value = ControlUnitMotorModeValue.NotConfigured;
				}

				var systemMotorMode2 = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.SystemMotorMode1);

				if (systemMotorMode2 != null && (int)this.WizardModel.PumpAndMotorConfigurationSystemMotorModePump2Value > systemMotorMode2.Scaling.Max)
				{
					this.WizardModel.PumpAndMotorConfigurationSystemMotorModePump2Value = ControlUnitMotorModeValue.NotConfigured;
				}

				SetSelectedSystemMotorMode(this.WizardModel.PumpAndMotorConfigurationSystemMotorModePump1Value, 1);
				SetSelectedSystemMotorMode(this.WizardModel.PumpAndMotorConfigurationSystemMotorModePump2Value, 2);
			}

			if (parameters.ContainsKey(NavigationConstants.ParameterCharacteristics))
			{
				this.ParameterCharacteristicList = parameters.GetValue<List<ParameterControlUnitCharacteristicExternalEntity>>(NavigationConstants.ParameterCharacteristics);
			}

			if (parameters.ContainsKey(NavigationConstants.ChainModalStack))
			{
				_wizardModalStack = parameters.GetValue<ModalChainStack>(NavigationConstants.ChainModalStack);

				if (_wizardModalStack == null)
					_wizardModalStack = new ModalChainStack();

				_wizardModalStack.Push(this);
			}
			else
			{
				_wizardModalStack = new ModalChainStack().ChainPush(this);
			}
		}

		public async Task CloseAsync()
		{
			await CloseWizardPage();
		}
	}
}
