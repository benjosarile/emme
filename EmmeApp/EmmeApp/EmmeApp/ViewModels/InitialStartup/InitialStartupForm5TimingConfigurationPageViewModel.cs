﻿using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities.External;
using EmmeApp.Helpers;
using EmmeApp.Localization.Resources;
using Prism.Commands;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmmeApp.ViewModels
{
    public class InitialStartupForm5TimingConfigurationPageViewModel : ViewModelBase, IChainModalViewModel
    {
        private readonly IPopupNavigation _popupNavigation;
        private readonly IEnumProvider _enumProvider;

        private ModalChainStack _wizardModalStack = null;

        public InitialStartupForm5TimingConfigurationPageViewModel(INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IPopupNavigation popupNavigation,
            IEnumProvider enumProvider)
            : base(navigationService, navigationHelperService, appCenterService)
        {
            _popupNavigation = popupNavigation;
            _enumProvider = enumProvider;

            this.WizardCloseCommand = new DelegateCommand(async () => await OnWizardClose());
            this.WizardLeftButtonCommand = new DelegateCommand(async () => await OnBack());
            this.WizardRightButtonCommand = new DelegateCommand(async () => await OnNextButton());
        }

        private int _wizardStepNumber = 5;
        public int WizardStepNumber
        {
            get => _wizardStepNumber;
            set => SetProperty(ref _wizardStepNumber, value);
        }

        private string _wizardErrorMessage;
        public string WizardErrorMessage
        {
            get => _wizardErrorMessage;
            set => SetProperty(ref _wizardErrorMessage, value);
        }
        private bool _wizardErrorMessageVisibility = false;
        public bool WizardErrorMessageVisibility
        {
            get => _wizardErrorMessageVisibility;
            set => SetProperty(ref _wizardErrorMessageVisibility, value);
        }

        private string _helpResourceKey = ResourceConstants.InitialStartup_Form5_TimingConfigurationPage_Help;
        public string HelpResourceKey
        {
            get => _helpResourceKey;
            set => SetProperty(ref _helpResourceKey, value);
        }


        private string _wizardFooterLeftLabel = AppResources.InitialStartup_Wizard_Back;
        public string WizardFooterLeftLabel
        {
            get => _wizardFooterLeftLabel;
            set => SetProperty(ref _wizardFooterLeftLabel, value);
        }

        private string _wizardFooterRightLabel = AppResources.InitialStartup_Wizard_Next;
        public string WizardFooterRightLabel
        {
            get => _wizardFooterRightLabel;
            set => SetProperty(ref _wizardFooterRightLabel, value);
        }

        private InitialStartupWizardModel _wizardModel;
        public InitialStartupWizardModel WizardModel
        {
            get => _wizardModel;
            set => SetProperty(ref _wizardModel, value);
        }

        private List<ParameterControlUnitCharacteristicExternalEntity> _parameterCharacteristicList;
        public List<ParameterControlUnitCharacteristicExternalEntity> ParameterCharacteristicList
        {
            get => _parameterCharacteristicList;
            set => SetProperty(ref _parameterCharacteristicList, value);
        }

        private List<ParameterTExternalEntity> _initialParameters;
        public List<ParameterTExternalEntity> InitialParameters
        {
            get => _initialParameters;
            set => SetProperty(ref _initialParameters, value);
        }

        private string _timingConfigurationMotorDelayedOffTimeUnit = AppResources.WizardForm_UnitsInSeconds;
        public string TimingConfigurationMotorDelayedOffTimeUnit
        {
            get => _timingConfigurationMotorDelayedOffTimeUnit;
            set => SetProperty(ref _timingConfigurationMotorDelayedOffTimeUnit, value);
        }

        private double _timingConfigurationMotorDelayedOffTimeMax = 60;
        public double TimingConfigurationMotorDelayedOffTimeMax
        {
            get => _timingConfigurationMotorDelayedOffTimeMax;
            set => SetProperty(ref _timingConfigurationMotorDelayedOffTimeMax, value);
        }

        private double _timingConfigurationMotorDelayedOffTimeMin;
        public double TimingConfigurationMotorDelayedOffTimeMin
        {
            get => _timingConfigurationMotorDelayedOffTimeMin;
            set => SetProperty(ref _timingConfigurationMotorDelayedOffTimeMin, value);
        }

        private double _timingConfigurationMotorDelayedOffTimeStep = 1;
        public double TimingConfigurationMotorDelayedOffTimeStep
        {
            get => _timingConfigurationMotorDelayedOffTimeStep;
            set => SetProperty(ref _timingConfigurationMotorDelayedOffTimeStep, value);
        }

        private string _timingConfigurationMotorForceRetentionTimeUnit = AppResources.WizardForm_UnitsInDays;
        public string TimingConfigurationMotorForceRetentionTimeUnit
        {
            get => _timingConfigurationMotorForceRetentionTimeUnit;
            set => SetProperty(ref _timingConfigurationMotorForceRetentionTimeUnit, value);
        }

        private double _timingConfigurationMotorForceRetentionTimeMax = 60;
        public double TimingConfigurationMotorForceRetentionTimeMax
        {
            get => _timingConfigurationMotorForceRetentionTimeMax;
            set => SetProperty(ref _timingConfigurationMotorForceRetentionTimeMax, value);
        }

        private double _timingConfigurationMotorForceRetentionTimeMin;
        public double TimingConfigurationMotorForceRetentionTimeMin
        {
            get => _timingConfigurationMotorForceRetentionTimeMin;
            set => SetProperty(ref _timingConfigurationMotorForceRetentionTimeMin, value);
        }

        private double _timingConfigurationMotorForceRetentionTimeStep = 1;
        public double TimingConfigurationMotorForceRetentionTimeStep
        {
            get => _timingConfigurationMotorForceRetentionTimeStep;
            set => SetProperty(ref _timingConfigurationMotorForceRetentionTimeStep, value);
        }

        private string _timingConfigurationMotorForceRunningTimeUnit = AppResources.WizardForm_UnitsInSeconds;
        public string TimingConfigurationMotorForceRunningTimeUnit
        {
            get => _timingConfigurationMotorForceRunningTimeUnit;
            set => SetProperty(ref _timingConfigurationMotorForceRunningTimeUnit, value);
        }

        private double _timingConfigurationMotorForceRunningTimeMax = 60;
        public double TimingConfigurationMotorForceRunningTimeMax
        {
            get => _timingConfigurationMotorForceRunningTimeMax;
            set => SetProperty(ref _timingConfigurationMotorForceRunningTimeMax, value);
        }

        private double _timingConfigurationMotorForceRunningTimeMin;
        public double TimingConfigurationMotorForceRunningTimeMin
        {
            get => _timingConfigurationMotorForceRunningTimeMin;
            set => SetProperty(ref _timingConfigurationMotorForceRunningTimeMin, value);
        }

        private double _timingConfigurationMotorForceRunningTimeStep = 1;
        public double TimingConfigurationMotorForceRunningTimeStep
        {
            get => _timingConfigurationMotorForceRunningTimeStep;
            set => SetProperty(ref _timingConfigurationMotorForceRunningTimeStep, value);
        }

        private string _timingConfigurationDelayedOnTimeUnit = AppResources.WizardForm_UnitsInSeconds;
        public string TimingConfigurationDelayedOnTimeUnit
        {
            get => _timingConfigurationDelayedOnTimeUnit;
            set => SetProperty(ref _timingConfigurationDelayedOnTimeUnit, value);
        }

        private double _timingConfigurationDelayedOnTimeMax = 60;
        public double TimingConfigurationDelayedOnTimeMax
        {
            get => _timingConfigurationDelayedOnTimeMax;
            set => SetProperty(ref _timingConfigurationDelayedOnTimeMax, value);
        }

        private double _timingConfigurationDelayedOnTimeMin;
        public double TimingConfigurationDelayedOnTimeMin
        {
            get => _timingConfigurationDelayedOnTimeMin;
            set => SetProperty(ref _timingConfigurationDelayedOnTimeMin, value);
        }

        private double _timingConfigurationDelayedOnTimeStep = 1;
        public double TimingConfigurationDelayedOnTimeStep
        {
            get => _timingConfigurationDelayedOnTimeStep;
            set => SetProperty(ref _timingConfigurationDelayedOnTimeStep, value);
        }

        private double _timingConfigurationMaxCyclesMax = 10;
        public double TimingConfigurationMaxCyclesMax
        {
            get => _timingConfigurationMaxCyclesMax;
            set => SetProperty(ref _timingConfigurationMaxCyclesMax, value);
        }

        private double _timingConfigurationMaxCyclesMin;
        public double TimingConfigurationMaxCyclesMin
        {
            get => _timingConfigurationMaxCyclesMin;
            set => SetProperty(ref _timingConfigurationMaxCyclesMin, value);
        }

        private double _timingConfigurationMaxCyclesStep = 1;
        public double TimingConfigurationMaxCyclesStep
        {
            get => _timingConfigurationMaxCyclesStep;
            set => SetProperty(ref _timingConfigurationMaxCyclesStep, value);
        }

        private string _timingConfigurationObserverTimeSpanUnit = AppResources.WizardForm_UnitsInSeconds;
        public string TimingConfigurationObserverTimeSpanUnit
        {
            get => _timingConfigurationObserverTimeSpanUnit;
            set => SetProperty(ref _timingConfigurationObserverTimeSpanUnit, value);
        }

        private double _timingConfigurationObserverTimeSpanMax = 300;
        public double TimingConfigurationObserverTimeSpanMax
        {
            get => _timingConfigurationObserverTimeSpanMax;
            set => SetProperty(ref _timingConfigurationObserverTimeSpanMax, value);
        }

        private double _timingConfigurationObserverTimeSpanMin;
        public double TimingConfigurationObserverTimeSpanMin
        {
            get => _timingConfigurationObserverTimeSpanMin;
            set => SetProperty(ref _timingConfigurationObserverTimeSpanMin, value);
        }

        private double _timingConfigurationObserverTimeSpanStep = 1;
        public double TimingConfigurationObserverTimeSpanStep
        {
            get => _timingConfigurationObserverTimeSpanStep;
            set => SetProperty(ref _timingConfigurationObserverTimeSpanStep, value);
        }

        private string _timingConfigurationObserverRetentionTimeUnit = AppResources.WizardForm_UnitsInSeconds;
        public string TimingConfigurationObserverRetentionTimeUnit
        {
            get => _timingConfigurationObserverRetentionTimeUnit;
            set => SetProperty(ref _timingConfigurationObserverRetentionTimeUnit, value);
        }

        private double _timingConfigurationObserverRetentionTimeMax = 60;
        public double TimingConfigurationObserverRetentionTimeMax
        {
            get => _timingConfigurationObserverRetentionTimeMax;
            set => SetProperty(ref _timingConfigurationObserverRetentionTimeMax, value);
        }

        private double _timingConfigurationObserverRetentionTimeMin;
        public double TimingConfigurationObserverRetentionTimeMin
        {
            get => _timingConfigurationObserverRetentionTimeMin;
            set => SetProperty(ref _timingConfigurationObserverRetentionTimeMin, value);
        }

        private double _timingConfigurationObserverRetentionTimeStep = 1;
        public double TimingConfigurationObserverRetentionTimeStep
        {
            get => _timingConfigurationObserverRetentionTimeStep;
            set => SetProperty(ref _timingConfigurationObserverRetentionTimeStep, value);
        }

        private string _timingConfigurationAtexTimeoutUnit = AppResources.WizardForm_UnitsInSeconds;
        public string TimingConfigurationAtexTimeoutUnit
        {
            get => _timingConfigurationAtexTimeoutUnit;
            set => SetProperty(ref _timingConfigurationAtexTimeoutUnit, value);
        }

        private double _timingConfigurationAtexTimeoutMax = 120;
        public double TimingConfigurationAtexTimeoutMax
        {
            get => _timingConfigurationAtexTimeoutMax;
            set => SetProperty(ref _timingConfigurationAtexTimeoutMax, value);
        }

        private double _timingConfigurationAtexTimeoutMin;
        public double TimingConfigurationAtexTimeoutMin
        {
            get => _timingConfigurationAtexTimeoutMin;
            set => SetProperty(ref _timingConfigurationAtexTimeoutMin, value);
        }

        private double _timingConfigurationAtexTimeoutStep = 1;
        public double TimingConfigurationAtexTimeoutStep
        {
            get => _timingConfigurationAtexTimeoutStep;
            set => SetProperty(ref _timingConfigurationAtexTimeoutStep, value);
        }

        private string _timingConfigurationAtexLockTimeoutUnit = AppResources.WizardForm_UnitsInSeconds;
        public string TimingConfigurationAtexLockTimeoutUnit
        {
            get => _timingConfigurationAtexLockTimeoutUnit;
            set => SetProperty(ref _timingConfigurationAtexLockTimeoutUnit, value);
        }

        private double _timingConfigurationAtexLockTimeoutMax = 120;
        public double TimingConfigurationAtexLockTimeoutMax
        {
            get => _timingConfigurationAtexLockTimeoutMax;
            set => SetProperty(ref _timingConfigurationAtexLockTimeoutMax, value);
        }

        private double _timingConfigurationAtexLockTimeoutMin;
        public double TimingConfigurationAtexLockTimeoutMin
        {
            get => _timingConfigurationAtexLockTimeoutMin;
            set => SetProperty(ref _timingConfigurationAtexLockTimeoutMin, value);
        }

        private double _timingConfigurationAtexLockTimeoutStep = 1;
        public double TimingConfigurationAtexLockTimeoutStep
        {
            get => _timingConfigurationAtexLockTimeoutStep;
            set => SetProperty(ref _timingConfigurationAtexLockTimeoutStep, value);
        }

        public DelegateCommand WizardCloseCommand { get; private set; }
        public DelegateCommand WizardLeftButtonCommand { get; private set; }
        public DelegateCommand WizardRightButtonCommand { get; private set; }

        private async Task OnNextButton()
        {
            if (this.WizardModel != null)
            {
                INavigationParameters parameters = new NavigationParameters();
                parameters.Add(NavigationConstants.InitialParameters, this.InitialParameters);
                parameters.Add(NavigationConstants.InitialStartupWizardModel, this.WizardModel);
                parameters.Add(NavigationConstants.ParameterCharacteristics, this.ParameterCharacteristicList);
                parameters.Add(NavigationConstants.ChainModalStack, _wizardModalStack);

                await NavigationUtilities.NavigateAsync(this.NavigationService, ViewNames.InitialStartupForm6MaintenanceAndRelayConfigurationPage, parameters, null, false);
            }
        }

        private async Task OnWizardClose()
        {
            DialogAlert dialogAlert = new DialogAlert();
            dialogAlert.LeftButtonText = AppResources.InitialStartup_Wizard_Popup_Close_Back;
            dialogAlert.RightButtonText = AppResources.InitialStartup_Wizard_Popup_Close_Confirm;
            dialogAlert.ContentText = AppResources.InitialStartup_Wizard_Popup_Close_Content;
            dialogAlert.Title = AppResources.InitialStartup_Wizard_Popup_Close_Title;
            dialogAlert.RightButtonCommand = new DelegateCommand(async () => await CloseWizardPage());
            await _popupNavigation.PushAsync(dialogAlert, false);
        }

        private async Task CloseWizardPage()
        {
            INavigationParameters parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.ShouldExitWizard, "");

            await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
        }

        private async Task OnBack()
        {
            if (this.WizardModel != null)
            {
                INavigationParameters parameters = new NavigationParameters();
                parameters.Add(NavigationConstants.InitialParameters, this.InitialParameters);
                parameters.Add(NavigationConstants.InitialStartupWizardModel, this.WizardModel);
                parameters.Add(NavigationConstants.ParameterCharacteristics, this.ParameterCharacteristicList);

                await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
            }
        }

        private void SetMinimumMaximumValue()
        {
            var timingConfigurationMotorDelayedOffTimeParam = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.MotorDelayedOffTime);

            if (timingConfigurationMotorDelayedOffTimeParam != null && timingConfigurationMotorDelayedOffTimeParam.Scaling != null)
            {
                double step = Math.Round(timingConfigurationMotorDelayedOffTimeParam.Scaling.Step, 5);

                var valMax = timingConfigurationMotorDelayedOffTimeParam.Scaling.Max * step;
                var valMin = timingConfigurationMotorDelayedOffTimeParam.Scaling.Min * step;

                if (valMax > valMin)
                {
                    if (valMax > this.TimingConfigurationMotorDelayedOffTimeMin)
                    {
                        this.TimingConfigurationMotorDelayedOffTimeMax = valMax;
                    }
                    else
                    {
                        this.TimingConfigurationMotorDelayedOffTimeMin = valMin;
                        this.TimingConfigurationMotorDelayedOffTimeMax = valMax;
                    }
                }

                this.TimingConfigurationMotorDelayedOffTimeMin = valMin;
                this.TimingConfigurationMotorDelayedOffTimeStep = step;
                string unit = _enumProvider.GetWizardUnitText(timingConfigurationMotorDelayedOffTimeParam.Unit);
                this.TimingConfigurationMotorDelayedOffTimeUnit = string.IsNullOrEmpty(unit) ? TimingConfigurationMotorDelayedOffTimeUnit : unit;
            }

            var timingConfigurationMotorForceRetentionTimeParam
                = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.MotorForceRetentionTime);

            if (timingConfigurationMotorForceRetentionTimeParam != null && timingConfigurationMotorForceRetentionTimeParam.Scaling != null)
            {
                double step = Math.Round(timingConfigurationMotorForceRetentionTimeParam.Scaling.Step, 5);

                var valMax = timingConfigurationMotorForceRetentionTimeParam.Scaling.Max * step;
                var valMin = timingConfigurationMotorForceRetentionTimeParam.Scaling.Min * step;

                if (valMax > valMin)
                {
                    if (valMax > this.TimingConfigurationMotorForceRetentionTimeMin)
                    {
                        this.TimingConfigurationMotorForceRetentionTimeMax = valMax;
                    }
                    else
                    {
                        this.TimingConfigurationMotorForceRetentionTimeMin = valMin;
                        this.TimingConfigurationMotorForceRetentionTimeMax = valMax;
                    }
                }

                this.TimingConfigurationMotorForceRetentionTimeMin = valMin;
                this.TimingConfigurationMotorForceRetentionTimeStep = step;
                string unit = _enumProvider.GetWizardUnitText(timingConfigurationMotorForceRetentionTimeParam.Unit);
                this.TimingConfigurationMotorForceRetentionTimeUnit = string.IsNullOrEmpty(unit) ? TimingConfigurationMotorForceRetentionTimeUnit : unit;
            }

            var timingConfigurationMotorForceRunningTimeParam = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.MotorForceRunningOnTime);

            if (timingConfigurationMotorForceRunningTimeParam != null && timingConfigurationMotorForceRunningTimeParam.Scaling != null)
            {
                double step = Math.Round(timingConfigurationMotorForceRunningTimeParam.Scaling.Step, 5);

                var valMax = timingConfigurationMotorForceRunningTimeParam.Scaling.Max * step;
                var valMin = timingConfigurationMotorForceRunningTimeParam.Scaling.Min * step;

                if (valMax > valMin)
                {
                    if (valMax > this.TimingConfigurationMotorForceRunningTimeMin)
                    {
                        this.TimingConfigurationMotorForceRunningTimeMax = valMax;
                    }
                    else
                    {
                        this.TimingConfigurationMotorForceRunningTimeMin = valMin;
                        this.TimingConfigurationMotorForceRunningTimeMax = valMax;
                    }
                }

                this.TimingConfigurationMotorForceRunningTimeMin = valMin;
                this.TimingConfigurationMotorForceRunningTimeStep = step;
                string unit = _enumProvider.GetWizardUnitText(timingConfigurationMotorForceRunningTimeParam.Unit);
                this.TimingConfigurationMotorForceRunningTimeUnit = string.IsNullOrEmpty(unit) ? TimingConfigurationMotorForceRunningTimeUnit : unit;
            }

            var timingConfigurationMotorDelayedOnTimeParam = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.MotorDelayedOnTime);

            if (timingConfigurationMotorDelayedOnTimeParam != null && timingConfigurationMotorDelayedOnTimeParam.Scaling != null)
            {
                double step = Math.Round(timingConfigurationMotorDelayedOnTimeParam.Scaling.Step, 5);

                var valMax = timingConfigurationMotorDelayedOnTimeParam.Scaling.Max * step;
                var valMin = timingConfigurationMotorDelayedOnTimeParam.Scaling.Min * step;

                if (valMax > valMin)
                {
                    if (valMax > this.TimingConfigurationDelayedOnTimeMin)
                    {
                        this.TimingConfigurationDelayedOnTimeMax = valMax;
                    }
                    else
                    {
                        this.TimingConfigurationDelayedOnTimeMin = valMin;
                        this.TimingConfigurationDelayedOnTimeMax = valMax;
                    }
                }

                this.TimingConfigurationDelayedOnTimeMin = valMin;
                this.TimingConfigurationDelayedOnTimeStep = step;
                string unit = _enumProvider.GetWizardUnitText(timingConfigurationMotorDelayedOnTimeParam.Unit);
                this.TimingConfigurationDelayedOnTimeUnit = string.IsNullOrEmpty(unit) ? TimingConfigurationDelayedOnTimeUnit : unit;
            }

            var timingConfigurationMaxCyclesParam = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.MotorRuntimeObserverMaxCycle);

            if (timingConfigurationMaxCyclesParam != null && timingConfigurationMaxCyclesParam.Scaling != null)
            {
                double step = Math.Round(timingConfigurationMaxCyclesParam.Scaling.Step, 5);

                var valMax = timingConfigurationMaxCyclesParam.Scaling.Max * step;
                var valMin = timingConfigurationMaxCyclesParam.Scaling.Min * step;

                if (valMax > valMin)
                {
                    if (valMax > this.TimingConfigurationMaxCyclesMin)
                    {
                        this.TimingConfigurationMaxCyclesMax = valMax;
                    }
                    else
                    {
                        this.TimingConfigurationMaxCyclesMin = valMin;
                        this.TimingConfigurationMaxCyclesMax = valMax;
                    }
                }

                this.TimingConfigurationMaxCyclesMin = valMin;
                this.TimingConfigurationMaxCyclesStep = step;
            }

            var timingConfigurationObserverTimeSpanParam = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.MotorRuntimeObserverTimespan);

            if (timingConfigurationObserverTimeSpanParam != null && timingConfigurationObserverTimeSpanParam.Scaling != null)
            {
                double step = Math.Round(timingConfigurationObserverTimeSpanParam.Scaling.Step, 5);

                var valMax = timingConfigurationObserverTimeSpanParam.Scaling.Max * step;
                var valMin = timingConfigurationObserverTimeSpanParam.Scaling.Min * step;

                if (valMax > valMin)
                {
                    if (valMax > this.TimingConfigurationObserverTimeSpanMin)
                    {
                        this.TimingConfigurationObserverTimeSpanMax = valMax;
                    }
                    else
                    {
                        this.TimingConfigurationObserverTimeSpanMin = valMin;
                        this.TimingConfigurationObserverTimeSpanMax = valMax;
                    }
                }

                this.TimingConfigurationObserverTimeSpanMin = valMin;
                this.TimingConfigurationObserverTimeSpanStep = step;
                string unit = _enumProvider.GetWizardUnitText(timingConfigurationObserverTimeSpanParam.Unit);
                this.TimingConfigurationObserverTimeSpanUnit = string.IsNullOrEmpty(unit) ? TimingConfigurationObserverTimeSpanUnit : unit;
            }

            var timingConfigurationObserverRetentionTimeParam = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.MotorRuntimeObserverRetention);

            if (timingConfigurationObserverRetentionTimeParam != null && timingConfigurationObserverRetentionTimeParam.Scaling != null)
            {
                double step = Math.Round(timingConfigurationObserverRetentionTimeParam.Scaling.Step, 5);

                var valMax = timingConfigurationObserverRetentionTimeParam.Scaling.Max * step;
                var valMin = timingConfigurationObserverRetentionTimeParam.Scaling.Min * step;

                if (valMax > valMin)
                {
                    if (valMax > this.TimingConfigurationObserverRetentionTimeMin)
                    {
                        this.TimingConfigurationObserverRetentionTimeMax = valMax;
                    }
                    else
                    {
                        this.TimingConfigurationObserverRetentionTimeMin = valMin;
                        this.TimingConfigurationObserverRetentionTimeMax = valMax;
                    }
                }

                this.TimingConfigurationObserverRetentionTimeMin = valMin;
                this.TimingConfigurationObserverRetentionTimeStep = step;
                string unit = _enumProvider.GetWizardUnitText(timingConfigurationObserverRetentionTimeParam.Unit);
                this.TimingConfigurationObserverRetentionTimeUnit = string.IsNullOrEmpty(unit) ? TimingConfigurationObserverRetentionTimeUnit : unit;
            }

            var timingConfigurationAtexTimeoutParam = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.MotorAtexTimeout);

            if (timingConfigurationAtexTimeoutParam != null && timingConfigurationAtexTimeoutParam.Scaling != null)
            {
                double step = Math.Round(timingConfigurationAtexTimeoutParam.Scaling.Step, 5);

                var valMax = timingConfigurationAtexTimeoutParam.Scaling.Max * step;
                var valMin = timingConfigurationAtexTimeoutParam.Scaling.Min * step;

                if (valMax > valMin)
                {
                    if (valMax > this.TimingConfigurationAtexTimeoutMin)
                    {
                        this.TimingConfigurationAtexTimeoutMax = valMax;
                    }
                    else
                    {
                        this.TimingConfigurationAtexTimeoutMin = valMin;
                        this.TimingConfigurationAtexTimeoutMax = valMax;
                    }
                }

                this.TimingConfigurationAtexTimeoutMin = valMin;
                this.TimingConfigurationAtexTimeoutStep = step;
                string unit = _enumProvider.GetWizardUnitText(timingConfigurationAtexTimeoutParam.Unit);
                this.TimingConfigurationAtexTimeoutUnit = string.IsNullOrEmpty(unit) ? TimingConfigurationAtexTimeoutUnit : unit;
            }

            var timingConfigurationAtexLockTimeoutParam = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.MotorAtexTimeoutLock);

            if (timingConfigurationAtexLockTimeoutParam != null && timingConfigurationAtexLockTimeoutParam.Scaling != null)
            {
                double step = Math.Round(timingConfigurationAtexLockTimeoutParam.Scaling.Step, 5);

                var valMax = timingConfigurationAtexLockTimeoutParam.Scaling.Max * step;
                var valMin = timingConfigurationAtexLockTimeoutParam.Scaling.Min * step;

                if (valMax > valMin)
                {
                    if (valMax > this.TimingConfigurationAtexLockTimeoutMin)
                    {
                        this.TimingConfigurationAtexLockTimeoutMax = valMax;
                    }
                    else
                    {
                        this.TimingConfigurationAtexLockTimeoutMin = valMin;
                        this.TimingConfigurationAtexLockTimeoutMax = valMax;
                    }
                }

                this.TimingConfigurationAtexLockTimeoutMin = valMin;
                this.TimingConfigurationAtexLockTimeoutStep = step;
                string unit = _enumProvider.GetWizardUnitText(timingConfigurationAtexLockTimeoutParam.Unit);
                this.TimingConfigurationAtexLockTimeoutUnit = string.IsNullOrEmpty(unit) ? TimingConfigurationAtexLockTimeoutUnit : unit;
            }
        }

        private void SetMaxValuesToDefault()
        {
            WizardModel.TimingConfigurationMotorDelayedOffTime = 0;
            TimingConfigurationMotorDelayedOffTimeMax = 60;
            TimingConfigurationMotorDelayedOffTimeMin = 0;

            WizardModel.TimingConfigurationMotorForceRetentionTime = 0;
            TimingConfigurationMotorForceRetentionTimeMax = 60;
            TimingConfigurationMotorForceRetentionTimeMin = 0;

            WizardModel.TimingConfigurationMotorForceRunningTime = 0;
            TimingConfigurationMotorForceRunningTimeMax = 60;
            TimingConfigurationMotorForceRunningTimeMin = 0;

            WizardModel.TimingConfigurationMotorDelayedOnTime = 0;
            TimingConfigurationDelayedOnTimeMax = 60;
            TimingConfigurationDelayedOnTimeMin = 0;

            WizardModel.TimingConfigurationMaxCycles = 0;
            TimingConfigurationMaxCyclesMax = 10;
            TimingConfigurationMaxCyclesMin = 0;

            WizardModel.TimingConfigurationObserverTimeSpan = 0;
            TimingConfigurationObserverTimeSpanMax = 300;
            TimingConfigurationObserverTimeSpanMin = 0;

            WizardModel.TimingConfigurationObserverRetentionTime = 0;
            TimingConfigurationObserverRetentionTimeMax = 60;
            TimingConfigurationObserverRetentionTimeMin = 0;

            WizardModel.TimingConfigurationAtexTimeout = 0;
            TimingConfigurationAtexTimeoutMax = 120;
            TimingConfigurationAtexTimeoutMin = 0;

            WizardModel.TimingConfigurationAtexLockTimeout = 0;
            TimingConfigurationAtexLockTimeoutMax = 120;
            TimingConfigurationAtexLockTimeoutMin = 0;
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.ShouldExitWizard))
            {
                SetMaxValuesToDefault();
                await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
            }

            if (parameters.ContainsKey(NavigationConstants.InitialParameters))
            {
                this.InitialParameters = parameters.GetValue<List<ParameterTExternalEntity>>(NavigationConstants.InitialParameters);
                SetMinimumMaximumValue();
            }

            if (parameters.ContainsKey(NavigationConstants.InitialStartupWizardModel))
            {
                this.WizardModel = parameters.GetValue<InitialStartupWizardModel>(NavigationConstants.InitialStartupWizardModel);
            }

            if (parameters.ContainsKey(NavigationConstants.ParameterCharacteristics))
            {
                this.ParameterCharacteristicList = parameters.GetValue<List<ParameterControlUnitCharacteristicExternalEntity>>(NavigationConstants.ParameterCharacteristics);
            }

            if (parameters.ContainsKey(NavigationConstants.ChainModalStack))
            {
                _wizardModalStack = parameters.GetValue<ModalChainStack>(NavigationConstants.ChainModalStack);

                if (_wizardModalStack == null)
                    _wizardModalStack = new ModalChainStack();

                _wizardModalStack.Push(this);
            }
            else
            {
                _wizardModalStack = new ModalChainStack().ChainPush(this);
            }
        }

        public async Task CloseAsync()
        {
            await CloseWizardPage();
        }
    }
}
