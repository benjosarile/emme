﻿using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities.External;
using EmmeApp.Helpers;
using EmmeApp.Localization.Resources;
using Humanizer;
using Prism.Commands;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmmeApp.ViewModels
{
    public class InitialStartupForm2SystemDataConfigurationPageViewModel : ViewModelBase, IChainModalViewModel
	{
		private readonly IPopupNavigation _popupNavigation;
		private readonly IKeyboardHelper _keyboardHelper;
		private readonly IEnumProvider _enumHelper;

		private ModalChainStack _wizardModalStack = null;

		public InitialStartupForm2SystemDataConfigurationPageViewModel(INavigationService navigationService, INavigationHelperService navigationHelperService, IAppCenterCustomService appCenterService, IPopupNavigation popupNavigation, IKeyboardHelper keyboardHelper, IEnumProvider enumHelper) : base(navigationService, navigationHelperService, appCenterService)
		{
			_popupNavigation = popupNavigation;
			_keyboardHelper = keyboardHelper;
			_enumHelper = enumHelper;

			this.WizardCloseCommand = new DelegateCommand(async () => await OnWizardClose());
			this.SelectSystemTypeCommand = new DelegateCommand<string>((systemType) => OnSelectSystemType(systemType));
			this.WizardLeftButtonCommand = new DelegateCommand(async () => await OnBack());
			this.WizardRightButtonCommand = new DelegateCommand(async () => await OnNextButton());
		}

		private bool _isSystemTypeShaftHasChecked;
		public bool IsSystemTypeShaftHasChecked
		{
			get => _isSystemTypeShaftHasChecked;
			set => SetProperty(ref _isSystemTypeShaftHasChecked, value);
		}

		private bool _isSystemTypeReserviorHasChecked;
		public bool IsSystemTypeReserviorHasChecked
		{
			get => _isSystemTypeReserviorHasChecked;
			set => SetProperty(ref _isSystemTypeReserviorHasChecked, value);
		}

		private List<string> _systemTypeList;
		public List<string> SystemTypeList
		{
			get => _systemTypeList;
			set => SetProperty(ref _systemTypeList, value);
		}

		private int _wizardStepNumber = 2;
		public int WizardStepNumber
		{
			get => _wizardStepNumber;
			set => SetProperty(ref _wizardStepNumber, value);
		}

		private string _wizardFooterLeftLabel = AppResources.InitialStartup_Wizard_Back;
		public string WizardFooterLeftLabel
		{
			get => _wizardFooterLeftLabel;
			set => SetProperty(ref _wizardFooterLeftLabel, value);
		}

		private string _wizardFooterRightLabel = AppResources.InitialStartup_Wizard_Next;
		public string WizardFooterRightLabel
		{
			get => _wizardFooterRightLabel;
			set => SetProperty(ref _wizardFooterRightLabel, value);
		}

		private string _helpResourceKey = ResourceConstants.InitialStartup_Form2_SystemDataConfigurationPage_Help;
		public string HelpResourceKey
		{
			get => _helpResourceKey;
			set => SetProperty(ref _helpResourceKey, value);
		}

		private string _wizardErrorMessage;
		public string WizardErrorMessage
		{
			get => _wizardErrorMessage;
			set => SetProperty(ref _wizardErrorMessage, value);
		}

		private bool _wizardErrorMessageVisibility = false;
		public bool WizardErrorMessageVisibility
		{
			get => _wizardErrorMessageVisibility;
			set => SetProperty(ref _wizardErrorMessageVisibility, value);
		}

		private List<ParameterControlUnitCharacteristicExternalEntity> _parameterCharacteristicList;
		public List<ParameterControlUnitCharacteristicExternalEntity> ParameterCharacteristicList
		{
			get => _parameterCharacteristicList;
			set => SetProperty(ref _parameterCharacteristicList, value);
		}

		private InitialStartupWizardModel _wizardModel;
		public InitialStartupWizardModel WizardModel
		{
			get => _wizardModel;
			set => SetProperty(ref _wizardModel, value);
		}

		private bool _systemNameHasError;
		public bool SystemNameHasError
		{
			get => _systemNameHasError;
			set
			{
				SetProperty(ref _systemNameHasError, value);
				PlausibilityCheck();
			}
		}

		private bool _motor1NamehasError;
		public bool Motor1NameHasError
		{
			get => _motor1NamehasError;
			set
			{
				SetProperty(ref _motor1NamehasError, value);
				PlausibilityCheck();
			}
		}

		private bool _motor2NameHasError;
		public bool Motor2NameHasError
		{
			get => _motor2NameHasError;
			set
			{
				SetProperty(ref _motor2NameHasError, value);
				PlausibilityCheck();
			}
		}

		private bool _systemTypeHasError;
		public bool SystemTypeHasError
		{
			get => _systemTypeHasError;
			set => SetProperty(ref _systemTypeHasError, value);
		}

		private List<ParameterTExternalEntity> _initialParameters;
		public List<ParameterTExternalEntity> InitialParameters
		{
			get => _initialParameters;
			set => SetProperty(ref _initialParameters, value);
		}

		public DelegateCommand WizardCloseCommand { get; private set; }
		public DelegateCommand WizardLeftButtonCommand { get; private set; }
		public DelegateCommand WizardRightButtonCommand { get; private set; }
		public DelegateCommand<string> SelectSystemTypeCommand { get; private set; }

		private async Task OnNextButton()
		{
			if (this.WizardModel != null)
			{
				_keyboardHelper.HideKeyboard();

				this.WizardErrorMessage = string.Empty;
				this.WizardErrorMessageVisibility = false;

				if (PlausibilityCheckFailed(out string errorMessage))
				{
					this.WizardErrorMessage = errorMessage;
					this.WizardErrorMessageVisibility = true;
				}
				else
				{
					INavigationParameters parameters = new NavigationParameters();
					parameters.Add(NavigationConstants.InitialParameters, this.InitialParameters);
					parameters.Add(NavigationConstants.InitialStartupWizardModel, this.WizardModel);
					parameters.Add(NavigationConstants.ParameterCharacteristics, ParameterCharacteristicList);
					parameters.Add(NavigationConstants.ChainModalStack, _wizardModalStack);

					await NavigationUtilities.NavigateAsync(this.NavigationService, ViewNames.InitialStartupForm3LevelProbeConfigurationPage, parameters, null, false);
				}
			}
		}


		private async Task OnWizardClose()
		{
			DialogAlert dialogAlert = new DialogAlert();
			dialogAlert.LeftButtonText = AppResources.InitialStartup_Wizard_Popup_Close_Back;
			dialogAlert.RightButtonText = AppResources.InitialStartup_Wizard_Popup_Close_Confirm;
			dialogAlert.ContentText = AppResources.InitialStartup_Wizard_Popup_Close_Content;
			dialogAlert.Title = AppResources.InitialStartup_Wizard_Popup_Close_Title;
			dialogAlert.RightButtonCommand = new DelegateCommand(async () => await CloseWizardPage());
			await _popupNavigation.PushAsync(dialogAlert, false);
		}

		private async Task CloseWizardPage()
		{
			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.ShouldExitWizard, "");

			await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
		}

		private async Task OnBack()
		{
			if (this.WizardModel != null)
			{
				_keyboardHelper.HideKeyboard();

				INavigationParameters parameters = new NavigationParameters();
				parameters.Add(NavigationConstants.InitialParameters, this.InitialParameters);
				parameters.Add(NavigationConstants.InitialStartupWizardModel, this.WizardModel);
				parameters.Add(NavigationConstants.ParameterCharacteristics, ParameterCharacteristicList);

				await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
			}
		}

		public bool PlausibilityCheckFailed(out string errorMessage)
		{
			errorMessage = string.Empty;
			bool textFieldErrors = this.SystemNameHasError || this.Motor1NameHasError || this.Motor2NameHasError;
			this.SystemTypeHasError = this.WizardModel.SystemDataConfigurationSystemType == ControlUnitSystemTypeValue.NotConfigured;
			if (textFieldErrors)
			{
				errorMessage = AppResources.InitialStartup_Wizard_Error_Message_Entry_Limit_32;
				return textFieldErrors;
			}
			else if (this.SystemTypeHasError)
			{
				errorMessage = AppResources.InitialStartupForm2_SystemDataConfigurationPage_SystemType_ErrorMessage;
				return this.SystemTypeHasError;
			}

			return false;
		}

		private void PlausibilityCheck()
		{
			this.WizardErrorMessage = string.Empty;
			this.WizardErrorMessageVisibility = false;

			if (PlausibilityCheckFailed(out string errorMessage))
			{
				this.WizardErrorMessage = errorMessage;
				this.WizardErrorMessageVisibility = true;
			}
		}

		private void OnSelectSystemType(string systemType)
		{
			switch (systemType)
			{
				case "Shaft System": this.WizardModel.SystemDataConfigurationSystemType = ControlUnitSystemTypeValue.ShaftSystem; break;
				case "Reservoir System": this.WizardModel.SystemDataConfigurationSystemType = ControlUnitSystemTypeValue.ReservoirSystem; break;
				default: this.WizardModel.SystemDataConfigurationSystemType = ControlUnitSystemTypeValue.NotConfigured; break;
			}

			SetSystemType(this.WizardModel.SystemDataConfigurationSystemType);
			PlausibilityCheck();
		}

		private void SetSystemType(ControlUnitSystemTypeValue value)
		{
			this.IsSystemTypeShaftHasChecked = value == ControlUnitSystemTypeValue.ShaftSystem;
			this.IsSystemTypeReserviorHasChecked = value == ControlUnitSystemTypeValue.ReservoirSystem;
		}

		private void PopulateList()
		{
			this.SystemTypeList = _enumHelper.PopulateList<ControlUnitSystemTypeValue>()
				.FindAll(u => !(new[] { ControlUnitSystemTypeValue.NotConfigured })
				.Contains(u)).Select(x => x.Humanize()).ToList();
		}

		public override async void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);

			if (parameters.ContainsKey(NavigationConstants.ShouldExitWizard))
			{
				await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
			}

			if (parameters.ContainsKey(NavigationConstants.InitialParameters))
			{
				this.InitialParameters = parameters.GetValue<List<ParameterTExternalEntity>>(NavigationConstants.InitialParameters);
			}

			if (parameters.ContainsKey(NavigationConstants.InitialStartupWizardModel))
			{
				PopulateList();
				this.WizardModel = parameters.GetValue<InitialStartupWizardModel>(NavigationConstants.InitialStartupWizardModel);

				var systemType = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.SystemType);
				if (systemType != null && (int)this.WizardModel.SystemDataConfigurationSystemType > systemType.Scaling.Max)
				{
					this.WizardModel.SystemDataConfigurationSystemType = ControlUnitSystemTypeValue.NotConfigured;
				}

				SetSystemType(this.WizardModel.SystemDataConfigurationSystemType);
			}

			if (parameters.ContainsKey(NavigationConstants.ParameterCharacteristics))
			{
				this.ParameterCharacteristicList = parameters.GetValue<List<ParameterControlUnitCharacteristicExternalEntity>>(NavigationConstants.ParameterCharacteristics);
			}

			if (parameters.ContainsKey(NavigationConstants.ChainModalStack))
			{
				_wizardModalStack = parameters.GetValue<ModalChainStack>(NavigationConstants.ChainModalStack);

				if (_wizardModalStack == null)
					_wizardModalStack = new ModalChainStack();

				_wizardModalStack.Push(this);
			}
			else
			{
				_wizardModalStack = new ModalChainStack().ChainPush(this);
			}
		}

		public async Task CloseAsync()
		{
			await CloseWizardPage();
		}
	}
}
