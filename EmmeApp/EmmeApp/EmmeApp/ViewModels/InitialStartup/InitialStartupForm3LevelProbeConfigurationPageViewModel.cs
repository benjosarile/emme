﻿using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities.External;
using EmmeApp.Helpers;
using EmmeApp.Localization.Resources;
using Humanizer;
using Prism.Commands;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmmeApp.ViewModels
{
    public class InitialStartupForm3LevelProbeConfigurationPageViewModel : ViewModelBase, IChainModalViewModel
    {
        private readonly IPopupNavigation _popupNavigation;
        private readonly IEnumProvider _enumHelper;

        private ModalChainStack _wizardModalStack = null;

        public InitialStartupForm3LevelProbeConfigurationPageViewModel(
            INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IPopupNavigation popupNavigation, IEnumProvider enumHelper)
            : base(navigationService, navigationHelperService, appCenterService)
        {
            _popupNavigation = popupNavigation;
            _enumHelper = enumHelper;

            this.WizardCloseCommand = new DelegateCommand(async () => await OnWizardClose());
            this.WizardLeftButtonCommand = new DelegateCommand(async () => await OnBack());
            this.WizardRightButtonCommand = new DelegateCommand(async () => await OnNextButton());
        }

        private List<string> _levelProbeConfigurationList;
        public List<string> LevelProbeConfigurationList
        {
            get => _levelProbeConfigurationList;
            set => SetProperty(ref _levelProbeConfigurationList, value);
        }

        private InitialStartupWizardModel _wizardModel;
        public InitialStartupWizardModel WizardModel
        {
            get => _wizardModel;
            set => SetProperty(ref _wizardModel, value);
        }

        private List<ParameterControlUnitCharacteristicExternalEntity> _parameterCharacteristicList;
        public List<ParameterControlUnitCharacteristicExternalEntity> ParameterCharacteristicList
        {
            get => _parameterCharacteristicList;
            set => SetProperty(ref _parameterCharacteristicList, value);
        }

        private string _selectedLevelProbeConfiguration;
        public string SelectedLevelProbeConfiguration
        {
            get => _selectedLevelProbeConfiguration;
            set
            {
                SetLevelProbeConfigurationLevelValue(value);
                ResetErrorMessage();
                this.ShowSliders = value == AppResources.ControlUnit_LevelProbeConfiguration_Analogue;
                SetProperty(ref _selectedLevelProbeConfiguration, value);
            }
        }

        private bool _levelProbeConfigurationHasError;
        public bool LevelProbeConfigurationHasError
        {
            get => _levelProbeConfigurationHasError;
            set => SetProperty(ref _levelProbeConfigurationHasError, value);
        }

        private bool _levelswitch2HasError;
        public bool Levelswitch2HasError
        {
            get => _levelswitch2HasError;
            set => SetProperty(ref _levelswitch2HasError, value);
        }

        private bool _levelswitch1HasError;
        public bool Levelswitch1HasError
        {
            get => _levelswitch1HasError;
            set => SetProperty(ref _levelswitch1HasError, value);
        }

        private bool _showSliders;
        public bool ShowSliders
        {
            get => _showSliders;
            set => SetProperty(ref _showSliders, value);
        }

        private string _helpResourceKey = ResourceConstants.InitialStartup_Form3_LevelProbeConfigurationPage_Help;
        public string HelpResourceKey
        {
            get => _helpResourceKey;
            set => SetProperty(ref _helpResourceKey, value);
        }

        private int _wizardStepNumber = 3;
        public int WizardStepNumber
        {
            get => _wizardStepNumber;
            set => SetProperty(ref _wizardStepNumber, value);
        }

        private string _wizardErrorMessage;
        public string WizardErrorMessage
        {
            get => _wizardErrorMessage;
            set => SetProperty(ref _wizardErrorMessage, value);
        }

        private bool _wizardErrorMessageVisibility = false;
        public bool WizardErrorMessageVisibility
        {
            get => _wizardErrorMessageVisibility;
            set => SetProperty(ref _wizardErrorMessageVisibility, value);
        }

        private string _wizardFooterLeftLabel = AppResources.InitialStartup_Wizard_Back;
        public string WizardFooterLeftLabel
        {
            get => _wizardFooterLeftLabel;
            set => SetProperty(ref _wizardFooterLeftLabel, value);
        }

        private string _wizardFooterRightLabel = AppResources.InitialStartup_Wizard_Next;
        public string WizardFooterRightLabel
        {
            get => _wizardFooterRightLabel;
            set => SetProperty(ref _wizardFooterRightLabel, value);
        }

        private List<ParameterTExternalEntity> _initialParameters;
        public List<ParameterTExternalEntity> InitialParameters
        {
            get => _initialParameters;
            set => SetProperty(ref _initialParameters, value);
        }

        private string _levelProbeConfigurationLevelAnalogThreshold2Unit = AppResources.WizardForm_UnitsInMilliampere;
        public string LevelProbeConfigurationLevelAnalogThreshold2Unit
        {
            get => _levelProbeConfigurationLevelAnalogThreshold2Unit;
            set => SetProperty(ref _levelProbeConfigurationLevelAnalogThreshold2Unit, value);
        }

        private double _levelProbeConfigurationLevelAnalogThreshold2Min;
        public double LevelProbeConfigurationLevelAnalogThreshold2Min
        {
            get => _levelProbeConfigurationLevelAnalogThreshold2Min;
            set => SetProperty(ref _levelProbeConfigurationLevelAnalogThreshold2Min, value);
        }

        private double _levelProbeConfigurationLevelAnalogThreshold2Max = 10;
        public double LevelProbeConfigurationLevelAnalogThreshold2Max
        {
            get => _levelProbeConfigurationLevelAnalogThreshold2Max;
            set => SetProperty(ref _levelProbeConfigurationLevelAnalogThreshold2Max, value);
        }

        private double _levelProbeConfigurationLevelAnalogThreshold2Step;
        public double LevelProbeConfigurationLevelAnalogThreshold2Step
        {
            get => _levelProbeConfigurationLevelAnalogThreshold2Step;
            set => SetProperty(ref _levelProbeConfigurationLevelAnalogThreshold2Step, value);
        }

        private string _levelProbeConfigurationLevelAnalogThreshold1Unit = AppResources.WizardForm_UnitsInMilliampere;
        public string LevelProbeConfigurationLevelAnalogThreshold1Unit
        {
            get => _levelProbeConfigurationLevelAnalogThreshold1Unit;
            set => SetProperty(ref _levelProbeConfigurationLevelAnalogThreshold1Unit, value);
        }

        private double _levelProbeConfigurationLevelAnalogThreshold1Min;
        public double LevelProbeConfigurationLevelAnalogThreshold1Min
        {
            get => _levelProbeConfigurationLevelAnalogThreshold1Min;
            set => SetProperty(ref _levelProbeConfigurationLevelAnalogThreshold1Min, value);
        }

        private double _levelProbeConfigurationLevelAnalogThreshold1Max = 10;
        public double LevelProbeConfigurationLevelAnalogThreshold1Max
        {
            get => _levelProbeConfigurationLevelAnalogThreshold1Max;
            set => SetProperty(ref _levelProbeConfigurationLevelAnalogThreshold1Max, value);
        }

        private double _levelProbeConfigurationLevelAnalogThreshold1Step;
        public double LevelProbeConfigurationLevelAnalogThreshold1Step
        {
            get => _levelProbeConfigurationLevelAnalogThreshold1Step;
            set => SetProperty(ref _levelProbeConfigurationLevelAnalogThreshold1Step, value);
        }

        private string _levelProbeConfigurationLevelAnalogThreshold0Unit = AppResources.WizardForm_UnitsInMilliampere;
        public string LevelProbeConfigurationLevelAnalogThreshold0Unit
        {
            get => _levelProbeConfigurationLevelAnalogThreshold0Unit;
            set => SetProperty(ref _levelProbeConfigurationLevelAnalogThreshold0Unit, value);
        }

        private double _levelProbeConfigurationLevelAnalogThreshold0Min;
        public double LevelProbeConfigurationLevelAnalogThreshold0Min
        {
            get => _levelProbeConfigurationLevelAnalogThreshold0Min;
            set => SetProperty(ref _levelProbeConfigurationLevelAnalogThreshold0Min, value);
        }

        private double _levelProbeConfigurationLevelAnalogThreshold0Max = 10;
        public double LevelProbeConfigurationLevelAnalogThreshold0Max
        {
            get => _levelProbeConfigurationLevelAnalogThreshold0Max;
            set => SetProperty(ref _levelProbeConfigurationLevelAnalogThreshold0Max, value);
        }

        private double _levelProbeConfigurationLevelAnalogThreshold0Step;
        public double LevelProbeConfigurationLevelAnalogThreshold0Step
        {
            get => _levelProbeConfigurationLevelAnalogThreshold0Step;
            set => SetProperty(ref _levelProbeConfigurationLevelAnalogThreshold0Step, value);
        }

        public DelegateCommand WizardCloseCommand { get; private set; }
        public DelegateCommand WizardHelpCommand { get; private set; }
        public DelegateCommand WizardLeftButtonCommand { get; private set; }
        public DelegateCommand WizardRightButtonCommand { get; private set; }

        private async Task OnWizardClose()
        {
            DialogAlert dialogAlert = new DialogAlert();
            dialogAlert.LeftButtonText = AppResources.InitialStartup_Wizard_Popup_Close_Back;
            dialogAlert.RightButtonText = AppResources.InitialStartup_Wizard_Popup_Close_Confirm;
            dialogAlert.ContentText = AppResources.InitialStartup_Wizard_Popup_Close_Content;
            dialogAlert.Title = AppResources.InitialStartup_Wizard_Popup_Close_Title;
            dialogAlert.RightButtonCommand = new DelegateCommand(async () => await CloseWizardPage());
            await _popupNavigation.PushAsync(dialogAlert, false);
        }

        private async Task CloseWizardPage()
        {
            INavigationParameters parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.ShouldExitWizard, "");
            await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
        }

        private async Task OnBack()
        {
            if (this.WizardModel != null)
            {
                INavigationParameters parameters = new NavigationParameters();
                parameters.Add(NavigationConstants.InitialParameters, this.InitialParameters);
                parameters.Add(NavigationConstants.InitialStartupWizardModel, this.WizardModel);
                parameters.Add(NavigationConstants.ParameterCharacteristics, this.ParameterCharacteristicList);

                await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
            }
        }

        private async Task OnNextButton()
        {
            if (this.WizardModel != null)
            {
                ResetErrorMessage();

                if (PlausibilityCheckFailed(out string errorMessage))
                {
                    ResetErrorMessage(errorMessage: errorMessage);
                }
                else
                {
                    INavigationParameters parameters = new NavigationParameters();
                    parameters.Add(NavigationConstants.InitialParameters, this.InitialParameters);
                    parameters.Add(NavigationConstants.InitialStartupWizardModel, this.WizardModel);
                    parameters.Add(NavigationConstants.ParameterCharacteristics, this.ParameterCharacteristicList);
                    parameters.Add(NavigationConstants.ChainModalStack, _wizardModalStack);

                    await NavigationUtilities.NavigateAsync(this.NavigationService, ViewNames.InitialStartupForm4PumpAndMotorConfigurationPage, parameters, null, false);
                }
            }
        }

        private void ResetErrorMessage(string errorMessage = "")
        {
            this.WizardErrorMessage = errorMessage;
            this.WizardErrorMessageVisibility = !string.IsNullOrEmpty(errorMessage);
        }

        private bool PlausibilityCheckFailed(out string errorMessage)
        {
            errorMessage = string.Empty;
            this.LevelProbeConfigurationHasError = string.IsNullOrEmpty(this.SelectedLevelProbeConfiguration);
            if (this.LevelProbeConfigurationHasError)
            {
                errorMessage = AppResources.InitialStartupForm3_LevelProbeConfigurationPage_LevelProbeConfiguration_ErrorMessage;
                return this.LevelProbeConfigurationHasError;
            }
            if (this.ShowSliders)
            {
                this.Levelswitch2HasError = this.WizardModel.LevelProbeConfigurationLevelAnalogThreshold2 <= this.WizardModel.LevelProbeConfigurationLevelAnalogThreshold1;
                if (this.Levelswitch2HasError)
                {
                    errorMessage = AppResources.InitialStartupForm3_LevelProbeConfigurationPage_Levelswitch2_ErrorMessage;
                    return this.Levelswitch2HasError;
                }

                this.Levelswitch1HasError = this.WizardModel.LevelProbeConfigurationLevelAnalogThreshold1 <= this.WizardModel.LevelProbeConfigurationLevelAnalogThreshold0;
                if (this.Levelswitch1HasError)
                {
                    errorMessage = AppResources.InitialStartupForm3_LevelProbeConfigurationPage_Levelswitch1_ErrorMessage;
                    return this.Levelswitch1HasError;
                }
            }

            return false;
        }

        private void SetLevelProbeConfigurationLevelValue(string value)
        {
            if (this.WizardModel != null && !string.IsNullOrEmpty(value))
            {
                this.WizardModel.LevelProbeConfigurationValue = EnumDehumanizeExtensions.DehumanizeTo<LevelProbeConfigurationValue>(value);
            }
        }

        private void SetSelectedLevelProbeConfiguration(LevelProbeConfigurationValue value)
        {
            if (value == LevelProbeConfigurationValue.NotConfigured)
            {
                this.SelectedLevelProbeConfiguration = null;
                return;
            }

            this.SelectedLevelProbeConfiguration = value.Humanize();

        }

        private void PopulateList()
        {
            this.LevelProbeConfigurationList = _enumHelper.PopulateList<LevelProbeConfigurationValue>()
                .FindAll(u => !(new[] { LevelProbeConfigurationValue.NotConfigured })
                .Contains(u))
                .Select(x => x.Humanize())
                .ToList();
        }

        private void SetMinimumMaximumValue()
        {
            var levelProbeConfigurationLevelAnalogThreshold2Param = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.LevelAnalogThreshold2Current);

            if (levelProbeConfigurationLevelAnalogThreshold2Param != null && levelProbeConfigurationLevelAnalogThreshold2Param.Scaling != null)
            {
                double step = Math.Round(levelProbeConfigurationLevelAnalogThreshold2Param.Scaling.Step, 5);

                var valMax = levelProbeConfigurationLevelAnalogThreshold2Param.Scaling.Max * step;
                var valMin = levelProbeConfigurationLevelAnalogThreshold2Param.Scaling.Min * step;

                if (valMax > valMin)
                {
                    if (valMax > this.LevelProbeConfigurationLevelAnalogThreshold2Min)
                    {
                        this.LevelProbeConfigurationLevelAnalogThreshold2Max = valMax;
                    }
                    else
                    {
                        this.LevelProbeConfigurationLevelAnalogThreshold2Min = valMin;
                        this.LevelProbeConfigurationLevelAnalogThreshold2Max = valMax;
                    }
                }


                this.LevelProbeConfigurationLevelAnalogThreshold2Min = valMin;
                this.LevelProbeConfigurationLevelAnalogThreshold2Step = step;
                string unit = _enumHelper.GetWizardUnitText(levelProbeConfigurationLevelAnalogThreshold2Param.Unit);
                this.LevelProbeConfigurationLevelAnalogThreshold2Unit = string.IsNullOrEmpty(unit) ? LevelProbeConfigurationLevelAnalogThreshold2Unit : unit;
            }

            var levelProbeConfigurationLevelAnalogThreshold1Param = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.LevelAnalogThreshold1Current);

            if (levelProbeConfigurationLevelAnalogThreshold1Param != null && levelProbeConfigurationLevelAnalogThreshold1Param.Scaling != null)
            {
                double step = Math.Round(levelProbeConfigurationLevelAnalogThreshold1Param.Scaling.Step, 5);

                var valMax = levelProbeConfigurationLevelAnalogThreshold1Param.Scaling.Max * step;
                var valMin = levelProbeConfigurationLevelAnalogThreshold1Param.Scaling.Min * step;

                if (valMax > valMin)
                {
                    if (valMax > this.LevelProbeConfigurationLevelAnalogThreshold1Min)
                    {
                        this.LevelProbeConfigurationLevelAnalogThreshold1Max = valMax;
                    }
                    else
                    {
                        this.LevelProbeConfigurationLevelAnalogThreshold1Min = valMin;
                        this.LevelProbeConfigurationLevelAnalogThreshold1Max = valMax;
                    }
                }

                this.LevelProbeConfigurationLevelAnalogThreshold1Min = valMin;
                this.LevelProbeConfigurationLevelAnalogThreshold1Step = step;
                string unit = _enumHelper.GetWizardUnitText(levelProbeConfigurationLevelAnalogThreshold1Param.Unit);
                this.LevelProbeConfigurationLevelAnalogThreshold1Unit = string.IsNullOrEmpty(unit) ? LevelProbeConfigurationLevelAnalogThreshold1Unit : unit;
            }

            var levelProbeConfigurationLevelAnalogThreshold0Param = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.LevelAnalogThreshold0Current);

            if (levelProbeConfigurationLevelAnalogThreshold0Param != null && levelProbeConfigurationLevelAnalogThreshold0Param.Scaling != null)
            {
                double step = Math.Round(levelProbeConfigurationLevelAnalogThreshold0Param.Scaling.Step, 5);

                var valMax = levelProbeConfigurationLevelAnalogThreshold0Param.Scaling.Max * step;
                var valMin = levelProbeConfigurationLevelAnalogThreshold0Param.Scaling.Min * step;

                if (valMax > valMin)
                {
                    if (valMax > this.LevelProbeConfigurationLevelAnalogThreshold0Min)
                    {
                        this.LevelProbeConfigurationLevelAnalogThreshold0Max = valMax;
                    }
                    else
                    {
                        this.LevelProbeConfigurationLevelAnalogThreshold0Min = valMin;
                        this.LevelProbeConfigurationLevelAnalogThreshold0Max = valMax;
                    }
                }

                this.LevelProbeConfigurationLevelAnalogThreshold0Min = valMin;
                this.LevelProbeConfigurationLevelAnalogThreshold0Step = step;
                string unit = _enumHelper.GetWizardUnitText(levelProbeConfigurationLevelAnalogThreshold0Param.Unit);
                this.LevelProbeConfigurationLevelAnalogThreshold0Unit = string.IsNullOrEmpty(unit) ? LevelProbeConfigurationLevelAnalogThreshold0Unit : unit;
            }
        }

        private void SetMaxValuesToDefault()
        {
            WizardModel.LevelProbeConfigurationLevelAnalogThreshold0 = 0;
            LevelProbeConfigurationLevelAnalogThreshold0Max = 10;
            LevelProbeConfigurationLevelAnalogThreshold0Min = 0;
            WizardModel.LevelProbeConfigurationLevelAnalogThreshold1 = 0;
            LevelProbeConfigurationLevelAnalogThreshold1Max = 10;
            LevelProbeConfigurationLevelAnalogThreshold1Min = 0;
            WizardModel.LevelProbeConfigurationLevelAnalogThreshold2 = 0;
            LevelProbeConfigurationLevelAnalogThreshold2Max = 10;
            LevelProbeConfigurationLevelAnalogThreshold2Min = 0;
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.ShouldExitWizard))
            {
                SetMaxValuesToDefault();
                await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
            }

            if (parameters.ContainsKey(NavigationConstants.InitialParameters))
            {
                this.InitialParameters = parameters.GetValue<List<ParameterTExternalEntity>>(NavigationConstants.InitialParameters);

                SetMinimumMaximumValue();
            }

            if (parameters.ContainsKey(NavigationConstants.InitialStartupWizardModel))
            {
                PopulateList();
                this.WizardModel = parameters.GetValue<InitialStartupWizardModel>(NavigationConstants.InitialStartupWizardModel);

                var levelProbeConfiguration = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.LevelProbeConfiguration);

                if (levelProbeConfiguration != null && (int)this.WizardModel.LevelProbeConfigurationValue > levelProbeConfiguration.Scaling.Max)
                {
                    this.WizardModel.LevelProbeConfigurationValue = LevelProbeConfigurationValue.NotConfigured;
                }

                SetSelectedLevelProbeConfiguration(this.WizardModel.LevelProbeConfigurationValue);
            }

            if (parameters.ContainsKey(NavigationConstants.ParameterCharacteristics))
            {
                this.ParameterCharacteristicList = parameters.GetValue<List<ParameterControlUnitCharacteristicExternalEntity>>(NavigationConstants.ParameterCharacteristics);
            }

            if (parameters.ContainsKey(NavigationConstants.ChainModalStack))
            {
                _wizardModalStack = parameters.GetValue<ModalChainStack>(NavigationConstants.ChainModalStack);

                if (_wizardModalStack == null)
                    _wizardModalStack = new ModalChainStack();

                _wizardModalStack.Push(this);
            }
            else
            {
                _wizardModalStack = new ModalChainStack().ChainPush(this);
            }
        }

        public async Task CloseAsync()
        {
            await CloseWizardPage();
        }
    }
}
