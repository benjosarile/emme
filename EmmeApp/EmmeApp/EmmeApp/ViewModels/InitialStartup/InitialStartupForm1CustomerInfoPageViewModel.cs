﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities.External;
using EmmeApp.Events;
using EmmeApp.Localization.Resources;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EmmeApp.ViewModels
{
    public class InitialStartupForm1CustomerInfoPageViewModel : ViewModelBase, IChainModalViewModel
    {
        private readonly IPopupNavigation _popupNavigation;
        private readonly IKeyboardHelper _keyboardHelper;

        private DialogAlertSingle _lostConnectionAlertDialog = null;
        private readonly SemaphoreSlim _connectionManagementThrottle = new SemaphoreSlim(1, 1);

        private readonly SubscriptionToken _connectionEstablishedSubToken;
        private readonly SubscriptionToken _connectionLostSubToken;

        private readonly ModalChainStack _wizardModalStack;

        public InitialStartupForm1CustomerInfoPageViewModel(INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IPopupNavigation popupNavigation,
            IKeyboardHelper keyboardHelper)
            : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
        {
            _popupNavigation = popupNavigation;
            _keyboardHelper = keyboardHelper;

            _wizardModalStack = new ModalChainStack().ChainPush(this);

            _connectionEstablishedSubToken = EventAggregator.GetEvent<ConnectionEstablishedEvent>().Subscribe(async () => await OnConnectionEstablished());
            _connectionLostSubToken = EventAggregator.GetEvent<ConnectionLostEvent>().Subscribe(async () => await OnConnectionLost());

            this.WizardCloseCommand = new DelegateCommand(async () => await OnWizardClose());
            this.WizardLeftButtonCommand = new DelegateCommand(async () => await OnWizardClose());
            this.WizardRightButtonCommand = new DelegateCommand(async () => await OnNextButton());
        }

        private int _wizardStepNumber = 1;
        public int WizardStepNumber
        {
            get => _wizardStepNumber;
            set => SetProperty(ref _wizardStepNumber, value);
        }

        private string _wizardFooterLeftLabel = AppResources.InitialStartup_Wizard_Cancel;
        public string WizardFooterLeftLabel
        {
            get => _wizardFooterLeftLabel;
            set => SetProperty(ref _wizardFooterLeftLabel, value);
        }
        private string _wizardFooterRightLabel = AppResources.InitialStartup_Wizard_Next;
        public string WizardFooterRightLabel
        {
            get => _wizardFooterRightLabel;
            set => SetProperty(ref _wizardFooterRightLabel, value);
        }
        private string _wizardErrorMessage;
        public string WizardErrorMessage
        {
            get => _wizardErrorMessage;
            set => SetProperty(ref _wizardErrorMessage, value);
        }
        private bool _wizardErrorMessageVisibility = false;
        public bool WizardErrorMessageVisibility
        {
            get => _wizardErrorMessageVisibility;
            set => SetProperty(ref _wizardErrorMessageVisibility, value);
        }

        private List<ParameterControlUnitCharacteristicExternalEntity> _parameterCharacteristicList;
        public List<ParameterControlUnitCharacteristicExternalEntity> ParameterCharacteristicList
        {
            get => _parameterCharacteristicList;
            set => SetProperty(ref _parameterCharacteristicList, value);
        }

        private InitialStartupWizardModel _wizardModel;
        public InitialStartupWizardModel WizardModel
        {
            get => _wizardModel;
            set => SetProperty(ref _wizardModel, value);
        }


        private bool _nameIsError;
        public bool NameIsError
        {
            get => _nameIsError;
            set
            {
                SetProperty(ref _nameIsError, value);
                PlausibilityCheck();
            }
        }

        private bool _phoneIsError;
        public bool PhoneIsError
        {
            get => _phoneIsError;
            set
            {
                SetProperty(ref _phoneIsError, value);
                PlausibilityCheck();
            }
        }

        private bool _streetIsError;
        public bool StreetIsError
        {
            get => _streetIsError;
            set
            {
                SetProperty(ref _streetIsError, value);
                PlausibilityCheck();
            }
        }

        private bool _cityIsError;
        public bool CityIsError
        {
            get => _cityIsError;
            set
            {
                SetProperty(ref _cityIsError, value);
                PlausibilityCheck();
            }
        }

        private bool _zipCodeIsError;
        public bool ZipCodeIsError
        {
            get => _zipCodeIsError;
            set
            {
                SetProperty(ref _zipCodeIsError, value);
                PlausibilityCheck();
            }
        }

        private bool _countryIsError;
        public bool CountryIsError
        {
            get => _countryIsError;
            set
            {
                SetProperty(ref _countryIsError, value);
                PlausibilityCheck();
            }
        }

        private string _helpResourceKey = ResourceConstants.InitialStartup_Form1_CustomerInfoPage_Help;
        public string HelpResourceKey
        {
            get => _helpResourceKey;
            set => SetProperty(ref _helpResourceKey, value);
        }

        private List<ParameterTExternalEntity> _initialParameters;
        public List<ParameterTExternalEntity> InitialParameters
        {
            get => _initialParameters;
            set => SetProperty(ref _initialParameters, value);
        }

        public DelegateCommand WizardCloseCommand { get; private set; }
        public DelegateCommand WizardLeftButtonCommand { get; private set; }
        public DelegateCommand WizardRightButtonCommand { get; private set; }

        private async Task OnWizardClose()
        {
            _keyboardHelper.HideKeyboard();

            DialogAlert dialogAlert = new DialogAlert();
            dialogAlert.LeftButtonText = AppResources.InitialStartup_Wizard_Popup_Close_Back;
            dialogAlert.RightButtonText = AppResources.InitialStartup_Wizard_Popup_Close_Confirm;
            dialogAlert.ContentText = AppResources.InitialStartup_Wizard_Popup_Close_Content;
            dialogAlert.Title = AppResources.InitialStartup_Wizard_Popup_Close_Title;
            dialogAlert.RightButtonCommand = new DelegateCommand(async () => await CloseWizardPage());
            await _popupNavigation.PushAsync(dialogAlert, false);
        }

        private async Task CloseWizardPage()
        {
            _keyboardHelper.HideKeyboard();

            await NavigationUtilities.GoBackAsync(this.NavigationService, null, null, false);
        }

        private async Task OnNextButton()
        {
            if (this.WizardModel != null)
            {
                _keyboardHelper.HideKeyboard();

                this.WizardErrorMessage = string.Empty;
                this.WizardErrorMessageVisibility = false;

                if (PlausibilityCheckFailed(out string errorMessage))
                {
                    this.WizardErrorMessage = errorMessage;
                    this.WizardErrorMessageVisibility = true;
                }
                else
                {
                    INavigationParameters parameters = new NavigationParameters();
                    parameters.Add(NavigationConstants.InitialParameters, InitialParameters);
                    parameters.Add(NavigationConstants.ParameterCharacteristics, ParameterCharacteristicList);
                    parameters.Add(NavigationConstants.InitialStartupWizardModel, this.WizardModel);
                    parameters.Add(NavigationConstants.ChainModalStack, _wizardModalStack);

                    await NavigationUtilities.NavigateAsync(this.NavigationService, ViewNames.InitialStartupForm2SystemDataConfigurationPage, parameters, null, false);
                }
            }
        }

        public bool PlausibilityCheckFailed(out string errorMessage)
        {
            errorMessage = string.Empty;
            bool textFieldErrors = this.NameIsError || this.PhoneIsError || this.StreetIsError || this.CityIsError || this.ZipCodeIsError || this.CountryIsError;
            if (textFieldErrors)
            {
                errorMessage = AppResources.InitialStartup_Wizard_Error_Message_Entry_Limit_32;
                return textFieldErrors;
            }

            return false;
        }

        private void PlausibilityCheck()
        {
            this.WizardErrorMessage = string.Empty;
            this.WizardErrorMessageVisibility = false;

            if (PlausibilityCheckFailed(out string errorMessage))
            {
                this.WizardErrorMessage = errorMessage;
                this.WizardErrorMessageVisibility = true;
            }
        }

        private async Task OnConnectionEstablished()
        {
            if (!(await _connectionManagementThrottle.WaitAsync(0)))
                return;

            await RemoveAlertForLostConnection();

            _connectionManagementThrottle.Release();
        }

        private async Task OnConnectionLost()
        {
            if (!(await _connectionManagementThrottle.WaitAsync(0)))
                return;

            await AlertUserOfLostConnection();

            _connectionManagementThrottle.Release();
        }

        private async Task AlertUserOfLostConnection()
        {
            if (_lostConnectionAlertDialog != null)
                return;

            _lostConnectionAlertDialog = new DialogAlertSingle();
            _lostConnectionAlertDialog.ButtonText = AppResources.Alert_Message_OK;
            _lostConnectionAlertDialog.ContentText = AppResources.Alert_MessageBody_BLE_LostConnection;
            _lostConnectionAlertDialog.Title = AppResources.Alert_MessageHeader_BLE_LostConnection;
            _lostConnectionAlertDialog.ButtonCommand =
                new DelegateCommand(
                    async () =>
                    {
                        _lostConnectionAlertDialog = null;
                        await _wizardModalStack.Peek().CloseAsync();
                    });
            await _popupNavigation.PushAsync(_lostConnectionAlertDialog, false);
        }

        private async Task RemoveAlertForLostConnection()
        {
            if (_lostConnectionAlertDialog != null && _popupNavigation.PopupStack.Any(x => x == _lostConnectionAlertDialog))
            {
                await _popupNavigation.RemovePageAsync(_lostConnectionAlertDialog);
                _lostConnectionAlertDialog = null;
            }
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.ShouldExitWizard))
            {
                await CloseWizardPage();
            }

            if (parameters.ContainsKey(NavigationConstants.InitialParameters))
            {
                InitialParameters = parameters.GetValue<List<ParameterTExternalEntity>>(NavigationConstants.InitialParameters);
            }
            if (parameters.ContainsKey(NavigationConstants.InitialStartupWizardModel))
            {
                this.WizardModel = parameters.GetValue<InitialStartupWizardModel>(NavigationConstants.InitialStartupWizardModel);
            }

            if (parameters.ContainsKey(NavigationConstants.ParameterCharacteristics))
            {
                this.ParameterCharacteristicList = parameters.GetValue<List<ParameterControlUnitCharacteristicExternalEntity>>(NavigationConstants.ParameterCharacteristics);
            }
        }

        public async Task CloseAsync()
        {
            await CloseWizardPage();
        }

        public override void Destroy()
        {
            base.Destroy();
            _lostConnectionAlertDialog = null;
            this.EventAggregator.GetEvent<ConnectionEstablishedEvent>().Unsubscribe(_connectionEstablishedSubToken);
            this.EventAggregator.GetEvent<ConnectionLostEvent>().Unsubscribe(_connectionLostSubToken);
        }
    }
}
