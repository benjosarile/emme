﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities.External;
using EmmeApp.Helpers;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Humanizer;
using Plugin.BluetoothLE;
using Plugin.Messaging;
using Prism.Commands;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EmmeApp.ViewModels
{
    public class InitialStartupForm6MaintenanceAndRelayConfigurationPageViewModel : ViewModelBase, IChainModalViewModel
	{
		private readonly IPopupNavigation _popupNavigation;
		private readonly IMessaging _messagingPlugin;
		private readonly IFileService _fileService;
		private readonly IEnumProvider _enumHelper;
		private readonly IBleManager _bleManager;
		private readonly IConfigurationControlUnitManager _configurationControlUnitManager;
		private readonly ICockpitControlUnitManager _cockpitControlUnitManager;
		private readonly ILogControlUnitManager _logControlUnitManager;

		private ModalChainStack _wizardModalStack = null;

		public InitialStartupForm6MaintenanceAndRelayConfigurationPageViewModel(INavigationService navigationService,
			INavigationHelperService navigationHelperService,
			IAppCenterCustomService appCenterService,
			IUserDialogs userDialogs,
			IPopupNavigation popupNavigation,
			IMessaging messagingPlugin,
			IFileService fileService,
			IEnumProvider enumHelper,
			IBleManager bleManager,
			IConfigurationControlUnitManager configurationControlUnitManager,
			ICockpitControlUnitManager cockpitControlUnitManager,
			ILogControlUnitManager logControlUnitManager)
			: base(navigationService, navigationHelperService, appCenterService, userDialogs)
		{
			_popupNavigation = popupNavigation;
			_messagingPlugin = messagingPlugin;
			_fileService = fileService;
			_enumHelper = enumHelper;
			_bleManager = bleManager;
			_configurationControlUnitManager = configurationControlUnitManager;
			_cockpitControlUnitManager = cockpitControlUnitManager;
			_logControlUnitManager = logControlUnitManager;

			this.WizardCloseCommand = new DelegateCommand(async () => await OnWizardClose());
			this.WizardLeftButtonCommand = new DelegateCommand(async () => await OnBack());
			this.WizardRightButtonCommand = new DelegateCommand(async () => await OnNextButton());

		}

		private IDisposable _notifyDisposable;
		private List<byte[]> _bleParameterData;
		private bool _shouldAcquireNewData = false;

		private List<string> _relayConfigurationList;
		public List<string> RelayConfigurationList
		{
			get => _relayConfigurationList;
			set => SetProperty(ref _relayConfigurationList, value);
		}

		private int _wizardStepNumber = 6;
		public int WizardStepNumber
		{
			get => _wizardStepNumber;
			set => SetProperty(ref _wizardStepNumber, value);
		}

		private string _selectedRelayConfigurationRelay0 = string.Empty;
		public string SelectedRelayConfigurationRelay0
		{
			get => _selectedRelayConfigurationRelay0;
			set
			{
				SetRelayConfigurationValue(value, 0);
				SetProperty(ref _selectedRelayConfigurationRelay0, value);
				PlausibilityCheck();
			}
		}

		private string _selectedRelayConfigurationRelay1 = string.Empty;
		public string SelectedRelayConfigurationRelay1
		{
			get => _selectedRelayConfigurationRelay1;
			set
			{
				SetRelayConfigurationValue(value, 1);
				SetProperty(ref _selectedRelayConfigurationRelay1, value);
				PlausibilityCheck();
			}
		}

		private string _selectedRelayConfigurationRelay2 = string.Empty;
		public string SelectedRelayConfigurationRelay2
		{
			get => _selectedRelayConfigurationRelay2;
			set
			{
				SetRelayConfigurationValue(value, 2);
				SetProperty(ref _selectedRelayConfigurationRelay2, value);
				PlausibilityCheck();
			}
		}

		private string _wizardErrorMessage;
		public string WizardErrorMessage
		{
			get => _wizardErrorMessage;
			set => SetProperty(ref _wizardErrorMessage, value);
		}
		private bool _wizardErrorMessageVisibility = false;
		public bool WizardErrorMessageVisibility
		{
			get => _wizardErrorMessageVisibility;
			set => SetProperty(ref _wizardErrorMessageVisibility, value);
		}

		private string _helpResourceKey = ResourceConstants.InitialStartup_Form6_MaintenanceAndRelayConfigurationPage_Help;
		public string HelpResourceKey
		{
			get => _helpResourceKey;
			set => SetProperty(ref _helpResourceKey, value);
		}

		private bool _relay0HasError;
		public bool Relay0HasError
		{
			get => _relay0HasError;
			set => SetProperty(ref _relay0HasError, value);
		}

		private bool _relay1HasError;
		public bool Relay1HasError
		{
			get => _relay1HasError;
			set => SetProperty(ref _relay1HasError, value);
		}

		private bool _relay2HasError;
		public bool Relay2HasError
		{
			get => _relay2HasError;
			set => SetProperty(ref _relay2HasError, value);
		}


		private string _wizardFooterLeftLabel = AppResources.InitialStartup_Wizard_Back;
		public string WizardFooterLeftLabel
		{
			get => _wizardFooterLeftLabel;
			set => SetProperty(ref _wizardFooterLeftLabel, value);
		}

		private string _wizardFooterRightLabel = AppResources.InitialStartup_Wizard_Finish;
		public string WizardFooterRightLabel
		{
			get => _wizardFooterRightLabel;
			set => SetProperty(ref _wizardFooterRightLabel, value);
		}

		public ControlUnitLogRootModel LogRootModel { get; set; }
		public CockpitControlUnitRootModel ControlUnitRootModel { get; set; }

		private InitialStartupWizardModel _wizardModel;
		public InitialStartupWizardModel WizardModel
		{
			get => _wizardModel;
			set => SetProperty(ref _wizardModel, value);
		}

		private List<ParameterTExternalEntity> _initialParameters;
		public List<ParameterTExternalEntity> InitialParameters
		{
			get => _initialParameters;
			set => SetProperty(ref _initialParameters, value);
		}

		private List<ParameterControlUnitCharacteristicExternalEntity> _parameterCharacteristicList;
		public List<ParameterControlUnitCharacteristicExternalEntity> ParameterCharacteristicList
		{
			get => _parameterCharacteristicList;
			set => SetProperty(ref _parameterCharacteristicList, value);
		}

		private string _maintenanceWarningIntervalBetweenWarningsUnit = AppResources.WizardForm_UnitsInMonths;
		public string MaintenanceWarningIntervalBetweenWarningsUnit
		{
			get => _maintenanceWarningIntervalBetweenWarningsUnit;
			set => SetProperty(ref _maintenanceWarningIntervalBetweenWarningsUnit, value);
		}


		private double _MaintenanceWarningIntervalBetweenWarningsMax = 24;
		public double MaintenanceWarningIntervalBetweenWarningsMax
		{
			get => _MaintenanceWarningIntervalBetweenWarningsMax;
			set => SetProperty(ref _MaintenanceWarningIntervalBetweenWarningsMax, value);
		}

		private double _maintenanceWarningIntervalBetweenWarningsMin;
		public double MaintenanceWarningIntervalBetweenWarningsMin
		{
			get => _maintenanceWarningIntervalBetweenWarningsMin;
			set => SetProperty(ref _maintenanceWarningIntervalBetweenWarningsMin, value);
		}

		private double _MaintenanceWarningIntervalBetweenWarningsStep = 1;
		public double MaintenanceWarningIntervalBetweenWarningsStep
		{
			get => _MaintenanceWarningIntervalBetweenWarningsStep;
			set => SetProperty(ref _MaintenanceWarningIntervalBetweenWarningsStep, value);
		}

		public DelegateCommand WizardCloseCommand { get; private set; }
		public DelegateCommand WizardLeftButtonCommand { get; private set; }
		public DelegateCommand WizardRightButtonCommand { get; private set; }

		private async Task OnNextButton()
		{
			if (this.WizardModel != null)
			{
				this.WizardErrorMessage = string.Empty;
				this.WizardErrorMessageVisibility = false;

				if (PlausibilityCheckFailed(out string errorMessage))
				{
					this.WizardErrorMessage = errorMessage;
					this.WizardErrorMessageVisibility = true;
				}
				else
				{
					this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);

					var initialData = await GetInitialParameters(this.WizardModel.SelectedDeviceUuid);
					var connectedDevice = initialData.Item4;

					_bleParameterData = new List<byte[]>();
					_shouldAcquireNewData = true;

					if (connectedDevice != null && connectedDevice.Status == ConnectionStatus.Connected)
					{
						var characteristic = await _bleManager.GetDeviceWriteCharacteristic(connectedDevice);

						if (characteristic != null)
						{
							var parametersList = _configurationControlUnitManager.MapWizardModelToParameters(this.WizardModel, this.ParameterCharacteristicList);

							bool isWriteSuccess = true;
							foreach (var parameters in parametersList)
							{
								var parametersT = new ParametersTExternalEntity() { Parameters = parameters };
								isWriteSuccess = await _bleManager.WriteCommand(parametersT, characteristic, ExternalIdOneofCaseValue.ControlUnit);

								if (!isWriteSuccess)
								{
									break;
								}
							}

							if (isWriteSuccess)
							{
								_shouldAcquireNewData = false;

								var initParameters = initialData.Item2;
								var initLogs = initialData.Item3;

								var model = new CockpitControlUnitRootModel();
								var modelResult = _cockpitControlUnitManager.MapParamtersToModel(model, initParameters);
								var parameters = new List<ParameterTExternalEntity>();
								foreach (var rowData in _bleParameterData)
								{
									parameters.AddRange(_bleManager.GetInitializeParameters(rowData));
								}

								modelResult = _cockpitControlUnitManager.MapParamtersToModel(modelResult.Item1, parameters);
								this.ControlUnitRootModel = modelResult.Item1;
								this.LogRootModel = new ControlUnitLogRootModel();
								this.LogRootModel = _logControlUnitManager.MapParametersToWizardModel(this.LogRootModel, initLogs);

								this.UserDialogs.HideLoading();
								var dialogAlert = new DialogAlert();
								dialogAlert.LeftButtonText = AppResources.InitialStartup_Wizard_Popup_Finish_SendEmail;
								dialogAlert.RightButtonText = AppResources.InitialStartup_Wizard_Popup_Finish_Done;
								dialogAlert.ContentText = AppResources.InitialStartup_Wizard_Popup_Finish_Content;
								dialogAlert.Title = AppResources.InitialStartup_Wizard_Popup_Finish_Title;

								dialogAlert.RightButtonCommand = new DelegateCommand(async () => await CloseWizardPage());
								dialogAlert.LeftButtonCommand = new DelegateCommand(async () => await SendEmail());
								await _popupNavigation.PushAsync(dialogAlert, false);
							}
							else
							{
								this.UserDialogs.HideLoading();
								await this.UserDialogs.AlertAsync(AppResources.Alert_Message_BLE_CannotWriteData);
								//Error dialog
							}
						}
					}
					else
					{
						this.UserDialogs.HideLoading();
						await this.UserDialogs.AlertAsync(AppResources.Alert_Message_BLE_DisconnectedFromDevice);
					}

					this.UserDialogs.HideLoading();
				}
			}
		}

		private async Task SendEmail()
		{
			if (_messagingPlugin.EmailMessenger.CanSendEmail)
			{
				this.UserDialogs.ShowLoading(AppResources.Loading_Message_EmailRedirect);

				this.ControlUnitRootModel.FactorySerialNumber = this.ControlUnitRootModel.FactorySerialNumber ?? string.Empty;
				var dateNow = DateTime.Now;
				string subject = $"{this.ControlUnitRootModel.FactorySerialNumber}_{dateNow.ToString("yyyyMMddhhmmss")}";
				string filename = $"{this.ControlUnitRootModel.FactorySerialNumber}_{dateNow.ToString("yyyyMMddhhmmss")}.txt";

				string filePath = _fileService.GetFilePath(filename);
				string newLine = "\r\n";
				
				if (Device.RuntimePlatform == Device.iOS)
				{
					newLine = "\n";
				}

				var configurationSb = _configurationControlUnitManager.GenerateEmailContent(this.WizardModel,newLine);
				var cockpitSb = _cockpitControlUnitManager.GenerateEmailContent(this.ControlUnitRootModel, newLine);
				var logSb = _logControlUnitManager.GenerateEmailContent(this.LogRootModel, newLine);

				configurationSb.Append(newLine);
				configurationSb.Append(cockpitSb);
				configurationSb.Append(newLine);
				configurationSb.Append(logSb);

				_fileService.WriteToAFile(filePath, cockpitSb.ToString(), false);

				var email = new EmailMessageBuilder()
								.Subject(subject)
								.Body(configurationSb.ToString())
								.WithAttachment(filePath, "text/plain")
								.Build();

				_messagingPlugin.EmailMessenger.SendEmail(email);

				this.UserDialogs.HideLoading();
			}
			else
			{
				await this.UserDialogs.AlertAsync(AppResources.Alert_Message_FailedToCreateMail_Message, AppResources.Alert_Message_FailedToCreateMail_Title);
			}
		}

		private async Task OnWizardClose()
		{
			DialogAlert dialogAlert = new DialogAlert();
			dialogAlert.LeftButtonText = AppResources.InitialStartup_Wizard_Popup_Close_Back;
			dialogAlert.RightButtonText = AppResources.InitialStartup_Wizard_Popup_Close_Confirm;
			dialogAlert.ContentText = AppResources.InitialStartup_Wizard_Popup_Close_Content;
			dialogAlert.Title = AppResources.InitialStartup_Wizard_Popup_Close_Title;
			dialogAlert.RightButtonCommand = new DelegateCommand(async () => await CloseWizardPage());
			await _popupNavigation.PushAsync(dialogAlert, false);
		}

		private async Task CloseWizardPage()
		{
			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.ShouldExitWizard, "");

			await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
		}

		private async Task OnBack()
		{
			if (this.WizardModel != null)
			{
				INavigationParameters parameters = new NavigationParameters();
				parameters.Add(NavigationConstants.InitialParameters, this.InitialParameters);
				parameters.Add(NavigationConstants.InitialStartupWizardModel, this.WizardModel);
				parameters.Add(NavigationConstants.ParameterCharacteristics, this.ParameterCharacteristicList);
				await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
			}
		}

		private void ReadParameters(CharacteristicGattResult result)
		{
			if (result.Data != null && _shouldAcquireNewData)
			{
				var resultData = result.Data;
				var hexDataValue = BitConverter.ToString(resultData);

				Debug.WriteLine(hexDataValue);
				_bleParameterData.Add(resultData);
			}
		}

		private async Task<Tuple<InitCommunicationTExternalEntity, List<ParameterTExternalEntity>, List<LogEntryTExternalEntity>, IDevice>> GetInitialParameters(Guid deviceUuid)
		{
			var connectedDevice = await _bleManager.GetKnownDevice(deviceUuid);

			if (connectedDevice != null)
			{
				var newDevice = await _bleManager.RestartTheDevice(connectedDevice);

				if (newDevice != null && newDevice.Status == ConnectionStatus.Connected)
				{
					var characteristic = await _bleManager.GetDeviceReadCharacteristic(newDevice);

					if (characteristic != null)
					{
						_bleParameterData = new List<byte[]>();

						_notifyDisposable?.Dispose();

						_notifyDisposable = characteristic
							.WhenNotificationReceived()
							.Subscribe(
								result => ReadParameters(result),
								ex => this.UserDialogs.Alert(ex.ToString())
							);

						_shouldAcquireNewData = true;

						await characteristic.EnableNotifications();

						//TODO UserStory #2023
						//Reduced maxRetryCount from 150 to 50
						int maxRetryCount = 50;
						int retryCount = 0;

						int parameterCtr = 0;
						while (parameterCtr < 100)
						{
							await Task.Delay(50);
							parameterCtr = _bleParameterData.Count;
							retryCount++;
							if (retryCount >= maxRetryCount)
							{
								break;
							}
						}

						_shouldAcquireNewData = false;

						newDevice = await _bleManager.GetConnectedDevice();

						if (newDevice != null && newDevice.Status == ConnectionStatus.Connected)
						{
							_shouldAcquireNewData = false;

							var initData = _bleManager.GetInitialData(_bleParameterData);
							return new Tuple<InitCommunicationTExternalEntity, List<ParameterTExternalEntity>, List<LogEntryTExternalEntity>, IDevice>(initData.Item1, initData.Item2, initData.Item3, newDevice);
						}
					}

					return new Tuple<InitCommunicationTExternalEntity, List<ParameterTExternalEntity>, List<LogEntryTExternalEntity>, IDevice>(null, null, null, newDevice);
				}

				return new Tuple<InitCommunicationTExternalEntity, List<ParameterTExternalEntity>, List<LogEntryTExternalEntity>, IDevice>(null, null, null, newDevice);
			}

			return null;
		}

		private bool PlausibilityCheckFailed(out string errorMessage)
		{
			errorMessage = string.Empty;
			this.Relay0HasError = string.IsNullOrEmpty(this.SelectedRelayConfigurationRelay0);
			this.Relay1HasError = string.IsNullOrEmpty(this.SelectedRelayConfigurationRelay1);
			this.Relay2HasError = string.IsNullOrEmpty(this.SelectedRelayConfigurationRelay2);
			if (this.Relay0HasError)
			{
				errorMessage = AppResources.InitialStartupForm6_MaintenanceAndRelayConfigurationPage_Relay0_ErrorMessage;
				return true;
			}

			if (this.Relay1HasError)
			{
				errorMessage = AppResources.InitialStartupForm6_MaintenanceAndRelayConfigurationPage_Relay1_ErrorMessage;
				return true;
			}

			if (this.Relay2HasError)
			{
				errorMessage = AppResources.InitialStartupForm6_MaintenanceAndRelayConfigurationPage_Relay2_ErrorMessage;
				return true;
			}

			return false;
		}

		private void PlausibilityCheck()
		{
			this.WizardErrorMessage = string.Empty;
			this.WizardErrorMessageVisibility = false;

			if (PlausibilityCheckFailed(out string errorMessage))
			{
				this.WizardErrorMessage = errorMessage;
				this.WizardErrorMessageVisibility = true;
			}
		}

		private void SetRelayConfigurationValue(string value, int relay)
		{
			if (this.WizardModel != null)
			{
				var selectedRelayConfiguration = EnumDehumanizeExtensions.DehumanizeTo<ControlUnitCustomRelayConfigurationValue>(value);

				switch (relay)
				{
					case 0: this.WizardModel.RelayConfigurationRelay0Value = selectedRelayConfiguration; break;
					case 1: this.WizardModel.RelayConfigurationRelay1Value = selectedRelayConfiguration; break;
					case 2: this.WizardModel.RelayConfigurationRelay2Value = selectedRelayConfiguration; break;
				}
			}
		}

		private void SetRelayConfiguration(ControlUnitCustomRelayConfigurationValue value, int relay)
		{
			var selectedRelayConfiguration = value.Humanize();
			if (!string.IsNullOrEmpty(selectedRelayConfiguration))
			{
				switch (relay)
				{
					case 0: this.SelectedRelayConfigurationRelay0 = selectedRelayConfiguration; break;
					case 1: this.SelectedRelayConfigurationRelay1 = selectedRelayConfiguration; break;
					case 2: this.SelectedRelayConfigurationRelay2 = selectedRelayConfiguration; break;
				}
			}

		}

		private void PopulateList()
		{
			this.RelayConfigurationList = _enumHelper.PopulateList<ControlUnitCustomRelayConfigurationValue>()
					.FindAll(u => !(new[] { ControlUnitCustomRelayConfigurationValue.CMN_M2M_NOT_CONFIGURED })
					.Contains(u)).Select(x => x.Humanize()).ToList();
		}

		private void SetMinimumMaximumValue()
		{
			var maintenanceWarningIntervalBetweenWarningsParam = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.SystemMaintenanceWarningInterval);

			if (maintenanceWarningIntervalBetweenWarningsParam != null && maintenanceWarningIntervalBetweenWarningsParam.Scaling != null)
			{
				double step = Math.Round(maintenanceWarningIntervalBetweenWarningsParam.Scaling.Step, 5);

				var valMax = maintenanceWarningIntervalBetweenWarningsParam.Scaling.Max * step;
				var valMin = maintenanceWarningIntervalBetweenWarningsParam.Scaling.Min * step;

				if (valMax > valMin)
				{
					if (valMax > this.MaintenanceWarningIntervalBetweenWarningsMin)
					{
						this.MaintenanceWarningIntervalBetweenWarningsMax = valMax;
					}
					else
					{
						this.MaintenanceWarningIntervalBetweenWarningsMin = valMin;
						this.MaintenanceWarningIntervalBetweenWarningsMax = valMax;
					}
				}

				this.MaintenanceWarningIntervalBetweenWarningsMin = valMin;
				this.MaintenanceWarningIntervalBetweenWarningsStep = step;
				string unit = _enumHelper.GetWizardUnitText(maintenanceWarningIntervalBetweenWarningsParam.Unit);
				this.MaintenanceWarningIntervalBetweenWarningsUnit = string.IsNullOrEmpty(unit) ? MaintenanceWarningIntervalBetweenWarningsUnit : unit;
			}
		}

		private void SetMaxValuesToDefault()
		{
			WizardModel.MaintenanceWarningIntervalBetweenWarnings = 0;
			MaintenanceWarningIntervalBetweenWarningsMax = 24;
			MaintenanceWarningIntervalBetweenWarningsMin = 0;
		}

		public override async void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);

			if (parameters.ContainsKey(NavigationConstants.ShouldExitWizard))
			{
				SetMaxValuesToDefault();
				await NavigationUtilities.GoBackAsync(this.NavigationService, parameters, null, false);
			}

			if (parameters.ContainsKey(NavigationConstants.InitialParameters))
			{
				this.InitialParameters = parameters.GetValue<List<ParameterTExternalEntity>>(NavigationConstants.InitialParameters);
				SetMinimumMaximumValue();
			}

			if (parameters.ContainsKey(NavigationConstants.InitialStartupWizardModel))
			{
				this.WizardModel = parameters.GetValue<InitialStartupWizardModel>(NavigationConstants.InitialStartupWizardModel);
				PopulateList();

				var relayConfiguration0 = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.M2mCustomRelay0Configuration);

				if (relayConfiguration0 != null && (int)this.WizardModel.RelayConfigurationRelay0Value > relayConfiguration0.Scaling.Max)
				{
					this.WizardModel.RelayConfigurationRelay0Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_NOT_CONFIGURED;
				}

				var relayConfiguration1 = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.M2mCustomRelay1Configuration);

				if (relayConfiguration1 != null && (int)this.WizardModel.RelayConfigurationRelay1Value > relayConfiguration1.Scaling.Max)
				{
					this.WizardModel.RelayConfigurationRelay1Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_NOT_CONFIGURED;
				}

				var relayConfiguration2 = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == ExternalControlUnitParameterIdValue.M2mCustomRelay2Configuration);

				if (relayConfiguration2 != null && (int)this.WizardModel.RelayConfigurationRelay2Value > relayConfiguration2.Scaling.Max)
				{
					this.WizardModel.RelayConfigurationRelay2Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_NOT_CONFIGURED;
				}

				SetRelayConfiguration(this.WizardModel.RelayConfigurationRelay0Value, 0);
				SetRelayConfiguration(this.WizardModel.RelayConfigurationRelay1Value, 1);
				SetRelayConfiguration(this.WizardModel.RelayConfigurationRelay2Value, 2);
			}

			if (parameters.ContainsKey(NavigationConstants.ParameterCharacteristics))
			{
				this.ParameterCharacteristicList = parameters.GetValue<List<ParameterControlUnitCharacteristicExternalEntity>>(NavigationConstants.ParameterCharacteristics);
			}

			if (parameters.ContainsKey(NavigationConstants.ChainModalStack))
			{
				_wizardModalStack = parameters.GetValue<ModalChainStack>(NavigationConstants.ChainModalStack);

				if (_wizardModalStack == null)
					_wizardModalStack = new ModalChainStack();

				_wizardModalStack.Push(this);
			}
			else
			{
				_wizardModalStack = new ModalChainStack().ChainPush(this);
			}
		}

		public async Task CloseAsync()
		{
			await CloseWizardPage();
		}

		public override void Destroy()
		{
			base.Destroy();
			_notifyDisposable?.Dispose();
		}
	}
}
