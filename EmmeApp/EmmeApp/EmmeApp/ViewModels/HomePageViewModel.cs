﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities;
using EmmeApp.Events;
using EmmeApp.ItemModels;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Resources.HelpViews;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace EmmeApp.ViewModels
{
    public class HomePageViewModel : ViewModelBase
	{
		private readonly IPopupNavigation _popupNavigation;
		private readonly IKeyboardHelper _keyboardHelper;
		private readonly IPumpReplacementUpdateManager _pumpReplacementUpdateManager;
		private readonly ISortimentManager _sortimentManager;
		private readonly IBiralGtcManager _biralGtcManager;

		public HomePageViewModel(INavigationService navigationService,
			INavigationHelperService navigationHelperService,
			IAppCenterCustomService appCenterService,
			IUserDialogs userDialogs,
			IEventAggregator eventAggregator,
			IPopupNavigation popupNavigation,
			IKeyboardHelper keyboardHelper,
			IPumpReplacementUpdateManager pumpReplacementUpdateManager,
			ISortimentManager sortimentManager,
			IBiralGtcManager biralGtcManager) : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
		{
			_popupNavigation = popupNavigation;
			_keyboardHelper = keyboardHelper;
			_pumpReplacementUpdateManager = pumpReplacementUpdateManager;
			_sortimentManager = sortimentManager;
			_biralGtcManager = biralGtcManager;

			this.ToggleMenuCommand = new DelegateCommand(() => OnToggleMenuTapped());
			this.SearchDocumentCommand = new DelegateCommand(() => OnSearchDocumentTapped());
			this.PumpReplacementCommand = new DelegateCommand(() => OnPumpReplacementTapped());
			this.ConfigurationCommand = new DelegateCommand(() => OnConfigurationTapped());
			this.DocumentsCommand = new DelegateCommand(() => OnDocumentsTapped());
			this.CockpitCommand = new DelegateCommand(() => OnCockpitTapped());
			this.LogCommand = new DelegateCommand(() => OnLogTapped());
		}

		private string _documentSearchQuery = string.Empty;
		public string DocumentSearchQuery
		{
			get => _documentSearchQuery;
			set => SetProperty(ref _documentSearchQuery, value);
		}

		public DelegateCommand ToggleMenuCommand { get; private set; }
		public DelegateCommand SearchDocumentCommand { get; private set; }
		public DelegateCommand PumpReplacementCommand { get; private set; }
		public DelegateCommand ConfigurationCommand { get; private set; }
		public DelegateCommand DocumentsCommand { get; private set; }
		public DelegateCommand CockpitCommand { get; private set; }
		public DelegateCommand LogCommand { get; private set; }

		private void OnToggleMenuTapped()
		{
			_keyboardHelper.HideKeyboard();
			this.EventAggregator.GetEvent<HamburgerTappedEvent>().Publish();
		}

		private void OnSearchDocumentTapped()
		{
			_keyboardHelper.HideKeyboard();

			var parameters = new NavigationParameters();

			this.DocumentSearchQuery = this.DocumentSearchQuery.Trim();

			if (!string.IsNullOrEmpty(this.DocumentSearchQuery))
			{
				var searchFilterEntity = new SearchFilterEntity();
				searchFilterEntity.Id = this.DocumentSearchQuery;
				searchFilterEntity.Name = this.DocumentSearchQuery;

				var selectedFilterItems = new ObservableCollection<SearchFilterEntity>();
				selectedFilterItems.Add(searchFilterEntity);
				parameters.Add(NavigationConstants.DocumentSelectedFilter, selectedFilterItems);
			}

			this.EventAggregator.GetEvent<HamburgerMenuItemExecuteEvent>().Publish(new HamburgerMenuModel() { Menu = HamburgerMenuValue.Documents, Parameters = parameters });
		}

		private void OnPumpReplacementTapped()
		{
			_keyboardHelper.HideKeyboard();
			this.EventAggregator.GetEvent<HamburgerMenuItemExecuteEvent>().Publish(new HamburgerMenuModel() { Menu = HamburgerMenuValue.PumpReplacement });
		}

		private void OnConfigurationTapped()
		{
			_keyboardHelper.HideKeyboard();
			this.EventAggregator.GetEvent<HamburgerMenuItemExecuteEvent>().Publish(new HamburgerMenuModel() { Menu = HamburgerMenuValue.Configuration });
		}

		private void OnDocumentsTapped()
		{
			_keyboardHelper.HideKeyboard();
			this.EventAggregator.GetEvent<HamburgerMenuItemExecuteEvent>().Publish(new HamburgerMenuModel() { Menu = HamburgerMenuValue.Documents });
		}

		private void OnCockpitTapped()
		{
			_keyboardHelper.HideKeyboard();
			this.EventAggregator.GetEvent<HamburgerMenuItemExecuteEvent>().Publish(new HamburgerMenuModel() { Menu = HamburgerMenuValue.Cockpit });
		}

		private void OnLogTapped()
		{
			_keyboardHelper.HideKeyboard();
			this.EventAggregator.GetEvent<HamburgerMenuItemExecuteEvent>().Publish(new HamburgerMenuModel() { Menu = HamburgerMenuValue.Log });
		}

		private async Task UpdatePumpReplacement()
		{
			this.UserDialogs.ShowLoading(AppResources.PumpReplacement_Update_Popup_Updating);

			bool isSuccess = await _pumpReplacementUpdateManager.Update();

			var dialogAlert = new DialogAlertSingle();

			if (isSuccess)
			{
				dialogAlert.ContentText = AppResources.PumpReplacement_Update_Popup_UpdateSuccess;

				_sortimentManager.SetPumpReplacementLastModified(DateTimeOffset.UtcNow);
			}
			else
			{
				dialogAlert.ContentText = AppResources.PumpReplacement_Update_Popup_UpdateFailed;
			}

			dialogAlert.Title = AppResources.PumpReplacement_Update_Popup_Title;
			dialogAlert.ButtonText = AppResources.PumpReplacement_Update_Popup_Okay;
			await _popupNavigation.PushAsync(dialogAlert, false);

			this.UserDialogs.HideLoading();
		}

		private async Task ShowUpdatePopup(bool hasUpdate, bool hasGtc)
		{
			if (hasUpdate)
			{
				if (!hasGtc)
				{
					UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
					await Task.Delay(2000);
					UserDialogs.HideLoading();
				}

				var dialogAlert = new DialogAlert();
				dialogAlert.LeftButtonText = AppResources.PumpReplacement_Update_Popup_Cancel;
				dialogAlert.RightButtonText = AppResources.PumpReplacement_Update_Popup_Update;
				dialogAlert.ContentText = AppResources.PumpReplacement_Update_Popup_NewUpdateAvailable;
				dialogAlert.Title = AppResources.PumpReplacement_Update_Popup_Title;
				dialogAlert.RightButtonCommand = new DelegateCommand(async () => await UpdatePumpReplacement());
				await _popupNavigation.PushAsync(dialogAlert, false);
			}
		}
		private async Task ShowGtcPopup(bool hasUpdate)
		{
			UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
			await Task.Delay(2000);
			UserDialogs.HideLoading();

			var currentBiralGtc = _biralGtcManager.GetCurrentBiralGtc();
			var dialogAlertSingleContent = new DialogAlertSingleContent();
			dialogAlertSingleContent.ButtonText = AppResources.Alert_Message_BLE_Permission_Accept;
			dialogAlertSingleContent.Title = AppResources.Alert_Message_GTC_Title;
			dialogAlertSingleContent.ButtonCommand = new DelegateCommand(async () =>
			{
				_biralGtcManager.ConfirmGtc();
				await ShowUpdatePopup(hasUpdate, true);
			});

			var view = new GtcContentView();
			view.Url = currentBiralGtc.Link;
			dialogAlertSingleContent.View = view;

			await _popupNavigation.PushAsync(dialogAlertSingleContent, false);
		}

		public override async void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);

			var hasUpdate = false;
			var hasGtcViewed = _biralGtcManager.HasGtcConfirmed();

			if (parameters.ContainsKey(NavigationConstants.HasUpdate))
			{
				hasUpdate = parameters.GetValue<bool>(NavigationConstants.HasUpdate);
			}

			if (!hasGtcViewed)
			{
				await ShowGtcPopup(hasUpdate);
				return;
			}

			if (hasUpdate)
			{
				await ShowUpdatePopup(hasUpdate, false);
			}
		}

	}
}
