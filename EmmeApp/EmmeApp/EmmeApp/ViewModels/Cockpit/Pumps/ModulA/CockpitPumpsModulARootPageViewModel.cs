﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities.External;
using EmmeApp.Events;
using EmmeApp.ItemModels;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Plugin.BluetoothLE;
using Plugin.Messaging;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EmmeApp.ViewModels
{
    public class CockpitPumpsModulARootPageViewModel : ViewModelBase
    {
        private readonly IMessaging _messagingPlugin;
        private readonly IFileService _fileService;
        private readonly IBleManager _bleManager;
        private readonly ICockpitPumpsModulAManager _cockpitPumpsModulAManager;
        private readonly ILogPumpManager _logPumpManager;
        private readonly IPopupNavigation _popupNavigation;

        private DialogAlertSingle _lostConnectionAlertDialog = null;
        private bool _isIntentionallyReconnecting = false;
        private readonly SemaphoreSlim _connectionManagementThrottle = new SemaphoreSlim(1, 1);

        private readonly SubscriptionToken _configureEventSubToken;
        private readonly SubscriptionToken _buttonSendEventSubToken;
        private readonly SubscriptionToken _connectionEstablishedSubToken;
        private readonly SubscriptionToken _connectionLostSubToken;

        public CockpitPumpsModulARootPageViewModel(INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IUserDialogs userDialogs,
            IAppCenterCustomService appCenterService,
            IEventAggregator eventAggregator,
            IMessaging messagingPlugin,
            IFileService fileService,
            IBleManager bleManager,
            ICockpitPumpsModulAManager cockpitPumpsModulAManager,
            ILogPumpManager logPumpManager,
            IPopupNavigation popupNavigation)
            : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
        {
            _messagingPlugin = messagingPlugin;
            _fileService = fileService;
            _bleManager = bleManager;
            _cockpitPumpsModulAManager = cockpitPumpsModulAManager;
            _logPumpManager = logPumpManager;
            _popupNavigation = popupNavigation;

            _configureEventSubToken = this.EventAggregator.GetEvent<PumpConfigurationEvent>().Subscribe(async () => await OnConfigure());
            _buttonSendEventSubToken = this.EventAggregator.GetEvent<CockpitPumpModulAButtonSendEvent>().Subscribe(async (model) => await OnButtonSend(model));
            _connectionEstablishedSubToken = this.EventAggregator.GetEvent<ConnectionEstablishedEvent>().Subscribe(async () => await OnConnectionEstablished());
            _connectionLostSubToken = this.EventAggregator.GetEvent<ConnectionLostEvent>().Subscribe(async () => await OnConnectionLost());

            this.ShareCommand = new DelegateCommand(async () => await OnShare(), () => this.CanExecuteCommand).ObservesProperty(() => this.CanExecuteCommand);
            this.ToggleMenuCommand = new DelegateCommand(() => OnToggleMenu(), () => this.CanExecuteCommand).ObservesProperty(() => this.CanExecuteCommand);
            this.ConfigureCommand = new DelegateCommand(async () => await OnConfigure(), () => this.CanExecuteCommand).ObservesProperty(() => this.CanExecuteCommand); 
        }

        public CockpitPumpsModulAOverviewPageViewModel CockpitPumpsModulAOverviewPageViewModel { get; private set; }
        public CockpitPumpsModulAStatusPageViewModel CockpitPumpsModulAStatusPageViewModel { get; private set; }
        public CockpitPumpsModulASettingsPageViewModel CockpitPumpsModulASettingsPageViewModel { get; private set; }

        public PumpLogRootModel LogRootModel { get; private set; }

        private List<ParameterTExternalEntity> _storedParameters;
        private IGattCharacteristic _characteristic;
        private IDisposable _notifyDisposable;
        private IDisposable _deviceStatusDisposable;
        private List<LogEntryTExternalEntity> _logs;

        private CockpitPumpsModulARootModel _rootModel;
        public CockpitPumpsModulARootModel RootModel
        {
            get => _rootModel;
            set => SetProperty(ref _rootModel, value);
        }

        private List<ParameterTExternalEntity> _initialParameters;
        public List<ParameterTExternalEntity> InitialParameters
        {
            get => _initialParameters;
            set => SetProperty(ref _initialParameters, value);
        }

        private List<ParameterPumpModulACharacteristicExternalEntity> _parameterCharacteristicList;
        public List<ParameterPumpModulACharacteristicExternalEntity> ParameterCharacteristicList
        {
            get => _parameterCharacteristicList;
            set => SetProperty(ref _parameterCharacteristicList, value);
        }

        private bool _canExecuteCommand = true;
        public bool CanExecuteCommand
        {
            get => _canExecuteCommand;
            set => SetProperty(ref _canExecuteCommand, value);
        }

        public DelegateCommand ToggleMenuCommand { get; private set; }
        public DelegateCommand ShareCommand { get; private set; }
        public DelegateCommand ConfigureCommand { get; private set; }

        private void OnToggleMenu()
        {
            this.CanExecuteCommand = false;
            this.EventAggregator.GetEvent<HamburgerTappedEvent>().Publish();
            this.CanExecuteCommand = true;
        }

        private async Task OnShare()
        {
            this.CanExecuteCommand = false;
            if (_messagingPlugin.EmailMessenger.CanSendEmail)
            {
                this.UserDialogs.ShowLoading(AppResources.Loading_Message_EmailRedirect);

                RootModel.SerialNumber = RootModel.SerialNumber ?? string.Empty;
                var dateNow = DateTime.Now;
                string subject = $"{RootModel.SerialNumber}_{dateNow.ToString("yyyyMMddhhmmss")}";
                string filename = $"{RootModel.SerialNumber}_{dateNow.ToString("yyyyMMddhhmmss")}.txt";

                string filePath = _fileService.GetFilePath(filename);
                string newLine = "\r\n";

                if (Device.RuntimePlatform == Device.iOS)
                {
                    newLine = "\n";
                }

                var cockpitSb = _cockpitPumpsModulAManager.GenerateEmailContent(this.RootModel, newLine);
                var logSb = _logPumpManager.GenerateEmailContent(this.LogRootModel, newLine);

                cockpitSb.Append(newLine);
                cockpitSb.Append(logSb);

                _fileService.WriteToAFile(filePath, cockpitSb.ToString(), false);

                var email = new EmailMessageBuilder()
                    .Subject(subject)
                    .Body(cockpitSb.ToString())
                    .WithAttachment(filePath, "text/plain")
                    .Build();

                _messagingPlugin.EmailMessenger.SendEmail(email);

                this.UserDialogs.HideLoading();
            }
            else
            {
                await this.UserDialogs.AlertAsync(AppResources.Alert_Message_FailedToCreateMail_Message, AppResources.Alert_Message_FailedToCreateMail_Title);
            }

            this.CanExecuteCommand = true;
        }

        private async Task OnConfigure()
        {
            this.CanExecuteCommand = false;

            await UnsubscribeFromCharacteristicNotifications();

            var model = _cockpitPumpsModulAManager.MapToConfigurationModel(this.RootModel);
            INavigationParameters parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.PumpConfigurationModulAWizardModel, model);
            parameters.Add(NavigationConstants.InitialParameters, this.InitialParameters);
            parameters.Add(NavigationConstants.ParameterCharacteristics, this.ParameterCharacteristicList);
            parameters.Add(NavigationConstants.CockpitPumpsModulARootModel, RootModel);
            parameters.Add(NavigationConstants.Logs, _logs);

            string navigationPath = $"{ViewNames.PumpConfigurationModulAForm1Page}";
            this.EventAggregator.GetEvent<MasterNavigationEvent>().Publish(new NavigationItemModel()
            {
                NavigationPath = navigationPath,
                Parameters = parameters,
                IsModalNavigation = true
            });

            this.CanExecuteCommand = true;
        }

        private async Task OnButtonSend(CockpitPumpModulAButtonSendModel model)
        {
            var connectedDevice = await _bleManager.GetConnectedDevice();

            if (connectedDevice != null)
            {
                var characteristic = await _bleManager.GetDeviceWriteCharacteristic(connectedDevice);

                if (characteristic != null)
                {
                    var parametersT = new ParametersTExternalEntity();
                    parametersT.Parameters.Add(new ParameterTExternalEntity()
                    {
                        IdCase = ExternalIdOneofCaseValue.ModulA,
                        ModulA = model.ParameterId,
                        Value = new ValueTExternalEntity()
                        {
                            Number = model.Value,
                            ValueCase = ExternalValueOneofCaseValue.Number
                        }
                    });

                    await _bleManager.WriteCommand(parametersT, characteristic, ExternalIdOneofCaseValue.ModulA);
                }
            }
        }

        public void SetCockpitPumpsModulAOverviewPageViewModel(CockpitPumpsModulAOverviewPageViewModel vm)
        {
            this.CockpitPumpsModulAOverviewPageViewModel = vm;
        }

        public void SetCockpitPumpsModulAStatusPageViewModel(CockpitPumpsModulAStatusPageViewModel vm)
        {

            this.CockpitPumpsModulAStatusPageViewModel = vm;
        }

        public void SetCockpitPumpsModulASettingsPageViewModel(CockpitPumpsModulASettingsPageViewModel vm)
        {
            this.CockpitPumpsModulASettingsPageViewModel = vm;
        }

        private void ReadParameter(CharacteristicGattResult result)
        {
            if (result.Data != null && this.RootModel != null)
            {
                var resultData = result.Data;

                var hexDataValue = BitConverter.ToString(resultData);
                Debug.WriteLine(hexDataValue);

                var parameters = _bleManager.GetInitializeParameters(result.Data);

                if (parameters.All(x => x.Value != null))
                {
                    foreach (var newParameter in parameters)
                    {
                        var parameter = this.InitialParameters.FirstOrDefault(x => x.ModulA == newParameter.ModulA);
                        if (parameter != null)
                        {
                            parameter.Value = newParameter.Value;

                            if (newParameter.Unit != ExternalUnitTValue.UnknownUnit)
                            {
                                parameter.Unit = newParameter.Unit;
                            }

                            if (newParameter.Scaling != null)
                            {
                                parameter.Scaling = parameter.Scaling ?? new ScalingTExternalEntity();
                                parameter.Scaling.Step = newParameter.Scaling.Step > 0 ? newParameter.Scaling.Step : parameter.Scaling.Step;
                                parameter.Scaling.Max = newParameter.Scaling.Max > 0 ? newParameter.Scaling.Max : parameter.Scaling.Max;
                                parameter.Scaling.Min = newParameter.Scaling.Min;
                            }

                            newParameter.Unit = parameter.Unit;
                            newParameter.Scaling = parameter.Scaling;
                        }
                    }

                    var modelResult = _cockpitPumpsModulAManager.MapParametersToModel(this.RootModel, parameters);
                    this.RootModel = modelResult.Item1;

                    if (this.CockpitPumpsModulAOverviewPageViewModel != null)
                    {
                        this.CockpitPumpsModulAOverviewPageViewModel.SetOverviewModelEvent(this.RootModel);
                        this.CockpitPumpsModulAOverviewPageViewModel.SetParameterEvent(InitialParameters);
                    }

                    if (this.CockpitPumpsModulAStatusPageViewModel != null)
                    {
                        this.CockpitPumpsModulAStatusPageViewModel.SetStatusModelEvent(this.RootModel);
                        this.CockpitPumpsModulAStatusPageViewModel.SetParameterEvent(InitialParameters);
                    }
                }
            }
        }

        private async Task<bool> SetBleCharacteristic()
        {
            await Task.Delay(100);
            this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);

            _isIntentionallyReconnecting = true;
            var connectedDevice = await _bleManager.GetConnectedDevice();

            if (_characteristic != null && _characteristic.IsNotifying && connectedDevice != null && connectedDevice.Status == ConnectionStatus.Connected)
            {
                await _characteristic.DisableNotifications();
                _notifyDisposable?.Dispose();
            }

            _characteristic = null;

            var newConnectedDevice = await _bleManager.RestartTheDevice(connectedDevice);
            await Task.Delay(2000); //TODO Waiting was adjusted from 2000 to 200
            if (newConnectedDevice != null && newConnectedDevice.Status == ConnectionStatus.Connected)
            {
                var characteristic = await _bleManager.GetDeviceReadCharacteristic(newConnectedDevice);

                if (characteristic != null)
                {
                    if (_storedParameters == null || !_storedParameters.Any())
                    {
                        _storedParameters = new List<ParameterTExternalEntity>();
                    }

                    _notifyDisposable?.Dispose();
                    _notifyDisposable =
                        characteristic
                            .WhenNotificationReceived()
                            .Subscribe(
                                result => ReadParameter(result),
                                ex => this.UserDialogs.Alert(ex.ToString())
                            );

                    _deviceStatusDisposable?.Dispose();
                    _deviceStatusDisposable =
                        newConnectedDevice
                            .WhenStatusChanged()
                            .Subscribe(
                                async (connectionStatus) =>
                                {
                                    switch (connectionStatus)
                                    {
                                        case ConnectionStatus.Connected:
                                            await OnConnectionEstablished();
                                            break;

                                        case ConnectionStatus.Disconnected:
                                            await OnConnectionLost();
                                            break;
                                    }
                                });

                    await characteristic.EnableNotifications();
                    this.UserDialogs.HideLoading();
                    _characteristic = characteristic;

                    return true;

                }
            }

            this.UserDialogs.HideLoading();
            return false;
        }

        private async Task OnConnectionEstablished()
        {
            if (!(await _connectionManagementThrottle.WaitAsync(0)))
                return;

            _isIntentionallyReconnecting = false;

            await RemoveAlertForLostConnection();

            _connectionManagementThrottle.Release();
        }

        private async Task OnConnectionLost()
        {
            if (!(await _connectionManagementThrottle.WaitAsync(0)))
                return;

            if (_isIntentionallyReconnecting)
            {
                _connectionManagementThrottle.Release();
                return;
            }

            await UnsubscribeFromCharacteristicNotifications();
            await AlertUserOfLostConnection();

            _connectionManagementThrottle.Release();
        }

        private async Task AlertUserOfLostConnection()
        {
            if (_lostConnectionAlertDialog != null)
                return;

            _lostConnectionAlertDialog = new DialogAlertSingle();
            _lostConnectionAlertDialog.ButtonText = AppResources.Alert_Message_OK;
            _lostConnectionAlertDialog.ContentText = AppResources.Alert_MessageBody_BLE_LostConnection;
            _lostConnectionAlertDialog.Title = AppResources.Alert_MessageHeader_BLE_LostConnection;
            _lostConnectionAlertDialog.ButtonCommand =
                new DelegateCommand(
                    () =>
                    {
                        _lostConnectionAlertDialog = null;
                        EventAggregator.GetEvent<HamburgerMenuItemExecuteEvent>().Publish(new ItemModels.HamburgerMenuModel() { Menu = HamburgerMenuValue.Home, Parameters = null });
                    });
            await _popupNavigation.PushAsync(_lostConnectionAlertDialog, false);
        }

        private async Task RemoveAlertForLostConnection()
        {
            if (_lostConnectionAlertDialog != null)
            {
                await _popupNavigation.RemovePageAsync(_lostConnectionAlertDialog);
                _lostConnectionAlertDialog = null;
            }
        }

        private async Task UnsubscribeFromCharacteristicNotifications()
        {
            try
            {
                if (_characteristic != null && _characteristic.IsNotifying)
                {
                    var connectedDevice = await _bleManager.GetConnectedDevice();
                    if (connectedDevice != null && connectedDevice.Status == ConnectionStatus.Connected)
                    {
                        await _characteristic.DisableNotifications();
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                try
                {
                    _notifyDisposable?.Dispose();
                    _notifyDisposable = null;
                    _deviceStatusDisposable?.Dispose();
                    _deviceStatusDisposable = null;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }

                _characteristic = null;
            }
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.CockpitPumpsModulARootModel))
            {
                var model = parameters.GetValue<CockpitPumpsModulARootModel>(NavigationConstants.CockpitPumpsModulARootModel);

                if (model != null)
                {
                    this.RootModel = model;
                }
            }

            if (parameters.ContainsKey(NavigationConstants.InitialParameters))
            {
                InitialParameters = parameters.GetValue<List<ParameterTExternalEntity>>(NavigationConstants.InitialParameters);
            }

            if (parameters.ContainsKey(NavigationConstants.ParameterCharacteristics))
            {
                this.ParameterCharacteristicList = parameters.GetValue<List<ParameterPumpModulACharacteristicExternalEntity>>(NavigationConstants.ParameterCharacteristics);
            }

            if (parameters.ContainsKey(NavigationConstants.PumpLogRootModel))
            {
                var model = parameters.GetValue<PumpLogRootModel>(NavigationConstants.PumpLogRootModel);
                this.LogRootModel = model;
            }

            if (parameters.ContainsKey(NavigationConstants.Logs))
            {
                _logs = parameters.GetValue<List<LogEntryTExternalEntity>>(NavigationConstants.Logs);
            }

            var setResult = await SetBleCharacteristic();

            if (!setResult)
            {
                await this.UserDialogs.AlertAsync(AppResources.Alert_Message_CantFetchData);
                this.EventAggregator.GetEvent<MasterNavigationEvent>().Publish(new NavigationItemModel
                {
                    NavigationPath = $"{ViewNames.NavigationPage}/{ViewNames.HomePage}",
                    Parameters = new NavigationParameters(),
                    IsModalNavigation = false
                });
            }
        }

        public override async void Destroy()
        {
            await UnsubscribeFromCharacteristicNotifications();
            _lostConnectionAlertDialog = null;
            EventAggregator.GetEvent<PumpConfigurationEvent>().Unsubscribe(_configureEventSubToken);
            EventAggregator.GetEvent<CockpitPumpModulAButtonSendEvent>().Unsubscribe(_buttonSendEventSubToken);
            EventAggregator.GetEvent<ConnectionEstablishedEvent>().Unsubscribe(_connectionEstablishedSubToken);
            EventAggregator.GetEvent<ConnectionLostEvent>().Unsubscribe(_connectionLostSubToken);
        }
    }
}