﻿using Acr.UserDialogs;
using EmmeApp.Common.Attributes;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities.External;
using EmmeApp.Events;
using EmmeApp.ItemModels;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Resources.HelpViews;
using Humanizer;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EmmeApp.ViewModels
{
    public class CockpitPumpsModulAOverviewPageViewModel : ViewModelBase
    {
        private readonly IServiceEntityMapper _serviceEntityMapper;
        private readonly IConfigurationPumpModulAManager _configurationPumpModulAManager;
        private readonly IPopupNavigation _popupNavigation;

        public CockpitPumpsModulAOverviewPageViewModel(
            INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IServiceEntityMapper serviceEntityMapper,
            IConfigurationPumpModulAManager configurationPumpModulAManager,
            IPopupNavigation popupNavigation)
            : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
        {
            _serviceEntityMapper = serviceEntityMapper;
            _configurationPumpModulAManager = configurationPumpModulAManager;
            _popupNavigation = popupNavigation;

            this.SetOverviewModelEvent = (model) => SetOverviewModel(model);
            this.SetParameterEvent = (parameter) => SetParameters(parameter);

            this.ConfirmAlarmPressed = new DelegateCommand(() => OnConfirmAlarmPressed());
            this.ConfirmAlarmReleased = new DelegateCommand(() => OnConfirmAlarmReleased());
            this.ViewFailureCodeHelpTextCommand = new DelegateCommand(async () => await OnViewFailureCodeHelpText());

        }

        public Action<CockpitPumpsModulARootModel> SetOverviewModelEvent { get; private set; }
        public Action<List<ParameterTExternalEntity>> SetParameterEvent { get; private set; }

        private string _failureCodeTitle;
        private string _failureCodeMeasure;
        private string _failureCodeCause;

        private CockpitPumpsModulAOverviewModel _overviewModel;
        public CockpitPumpsModulAOverviewModel OverviewModel
        {
            get => _overviewModel;
            set => SetProperty(ref _overviewModel, value);
        }

        private string _selectedControlType = PumpControlTypeValue.ConstantPressure.Humanize();
        public string SelectedControlType
        {
            get => _selectedControlType;
            set
            {
                SetProperty(ref _selectedControlType, value);
            }
        }

        private string _selectedControlTypeIcon = AppConstants.ConstantPressureIcon;
        public string SelectedControlTypeIcon
        {
            get => _selectedControlTypeIcon;
            set
            {
                SetProperty(ref _selectedControlTypeIcon, value);
            }
        }

        private string _selectedSystemStatus = PumpSystemStatusValue.No_Fault.Humanize();
        public string SelectedSystemStatus
        {
            get => _selectedSystemStatus;
            set => SetProperty(ref _selectedSystemStatus, value);
        }

        private string _selectedTypeOfOperation = PumpTypeOfOperationValue.ON.Humanize();
        public string SelectedTypeOfOperation
        {
            get => _selectedTypeOfOperation;
            set => SetProperty(ref _selectedTypeOfOperation, value);
        }

        private bool _isSystemStatusFail;
        public bool IsSystemStatusFail
        {
            get => _isSystemStatusFail;
            set => SetProperty(ref _isSystemStatusFail, value);
        }

        private string _systemStatusMessage;
        public string SystemStatusMessage
        {
            get => _systemStatusMessage;
            set => SetProperty(ref _systemStatusMessage, value);
        }

        private string _failureCode;
        public string FailureCode
        {
            get => _failureCode;
            set => SetProperty(ref _failureCode, value);
        }

        private bool _failureCodeHasHelpText;
        public bool FailureCodeHasHelpText
        {
            get => _failureCodeHasHelpText;
            set => SetProperty(ref _failureCodeHasHelpText, value);
        }



        private string _nominalValueUnit = AppResources.Label_UnitOfMeasurement_Meter;
        public string NominalValueUnit
        {
            get => _nominalValueUnit;
            set => SetProperty(ref _nominalValueUnit, value);
        }

        private Color _indicatorColor = Color.Red;
        public Color IndicatorColor
        {
            get => _indicatorColor;
            set => SetProperty(ref _indicatorColor, value);
        }


        public DelegateCommand ConfirmAlarmPressed { get; private set; }
        public DelegateCommand ConfirmAlarmReleased { get; private set; }
        public DelegateCommand ViewFailureCodeHelpTextCommand { get; private set; }

        private void OnConfirmAlarmPressed()
        {
            this.IndicatorColor = Color.Green;
            this.EventAggregator.GetEvent<CockpitPumpModulAButtonSendEvent>().Publish(new CockpitPumpModulAButtonSendModel()
            {
                ParameterId = ExternalPumpModulAParameterIdValue.CmdSystemConfirmAlarm,
                Value = 1
            });
        }

        private void OnConfirmAlarmReleased()
        {
            this.IndicatorColor = Color.Red;
            this.EventAggregator.GetEvent<CockpitPumpModulAButtonSendEvent>().Publish(new CockpitPumpModulAButtonSendModel()
            {
                ParameterId = ExternalPumpModulAParameterIdValue.CmdSystemConfirmAlarm,
                Value = 0
            });
        }

        private async Task OnViewFailureCodeHelpText()
        {
            if (FailureCodeHasHelpText && !_popupNavigation.PopupStack.Any())
            {
                var popupOverlay = new DialogInformationOverlay();
                popupOverlay.Title = _failureCodeTitle;

                var view = new LogAlarmWarningItemHelp();
                view.FindByName<Label>(UIConstants.LabelCauseMessage).Text = _failureCodeCause;
                view.FindByName<Label>(UIConstants.LabelMeasureMessage).Text = _failureCodeMeasure;

                popupOverlay.View = view;

                await _popupNavigation.PushAsync(popupOverlay);
            }
        }

        private void SetOverviewModel(CockpitPumpsModulARootModel model)
        {
            this.OverviewModel = _serviceEntityMapper.Map<CockpitPumpsModulAOverviewModel>(model);

            SetSystemStatus();
            SetSystemParameters();
        }

        private void SetParameters(List<ParameterTExternalEntity> parameter)
        {
            var nominalValueUnit = _configurationPumpModulAManager.GetUnit(ExternalPumpModulAParameterIdValue.NominalValue, parameter);
            this.NominalValueUnit = OverviewModel.ControlType == PumpControlTypeValue.ConstantSpeed ? AppResources.Label_UnitOfMeasurement_Percent : nominalValueUnit;
        }

        private void SetSystemStatus()
        {
            FailureCodeHasHelpText = false;
            _failureCodeTitle = string.Empty;
            _failureCodeMeasure = string.Empty;
            _failureCodeCause = string.Empty;

            this.IsSystemStatusFail = this.OverviewModel.SystemStatus != PumpSystemStatusValue.No_Fault;
            if (this.IsSystemStatusFail)
            {
                this.SystemStatusMessage = AppResources.CockpitControlUnitStatusPage_Label_Status_FailureCode;

                var type = OverviewModel.SystemStatus.GetType();
                var memInfo = type.GetMember(OverviewModel.SystemStatus.ToString());

                if (memInfo.Any())
                {
                    var attributes = memInfo[0].GetCustomAttributes(typeof(LogHelpTextAttribute), false);
                    if (attributes.Any())
                    {
                        var attribute = (LogHelpTextAttribute)attributes.FirstOrDefault();
                        _failureCodeTitle = attribute.GetTranslation(attribute.TitleResource);
                        _failureCodeMeasure = attribute.GetTranslation(attribute.MeasureResource);
                        _failureCodeCause = attribute.GetTranslation(attribute.CauseResource);
                        FailureCodeHasHelpText = true;
                    }
                }

                this.FailureCode = this.OverviewModel.SystemStatus.Humanize();
            }
            else
            {
                this.SystemStatusMessage = AppResources.CockpitControlUnitStatusPage_Label_Status_Good;
                this.FailureCode = string.Empty;
            }
        }

        private void SetSystemParameters()
        {
            if(this.OverviewModel != null)
            {
                this.SelectedSystemStatus = this.OverviewModel.SystemStatus.Humanize().ToUpper();
                this.SelectedControlType = this.OverviewModel.ControlType.Humanize();
                SetControlTypeIcon(this.OverviewModel.ControlType);
                this.SelectedTypeOfOperation = this.OverviewModel.TypeOfOperation.Humanize();
            }
        }

        private void SetControlTypeIcon(PumpControlTypeValue type)
        {
            switch (type)
            {
                case PumpControlTypeValue.ConstantPressure:
                    this.SelectedControlTypeIcon = AppConstants.ConstantPressureIcon;
                    break;
                case PumpControlTypeValue.ProportionalPressure:
                    this.SelectedControlTypeIcon = AppConstants.ProportionalPressureIcon;
                    break;
                case PumpControlTypeValue.ConstantSpeed:
                    this.SelectedControlTypeIcon = AppConstants.ConstantSpeedIcon;
                    break;
                default:
                    break;
            }
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (parameters.ContainsKey(NavigationConstants.CockpitPumpsModulARootModel))
            {
                var model = parameters.GetValue<CockpitPumpsModulARootModel>(NavigationConstants.CockpitPumpsModulARootModel);
                SetOverviewModel(model);
            }
        }
    }
}
