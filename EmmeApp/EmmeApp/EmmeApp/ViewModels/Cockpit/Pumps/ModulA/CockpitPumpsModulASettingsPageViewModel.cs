﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Events;
using EmmeApp.ItemModels;
using EmmeApp.Managers.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;

namespace EmmeApp.ViewModels
{
    public class CockpitPumpsModulASettingsPageViewModel : ViewModelBase
	{
		private readonly IServiceEntityMapper _serviceEntityMapper;
		public CockpitPumpsModulASettingsPageViewModel(INavigationService navigationService, 
			INavigationHelperService navigationHelperService, 
			IAppCenterCustomService appCenterService,
			IUserDialogs userDialogs, 
			IEventAggregator eventAggregator, 
			IServiceEntityMapper serviceEntityMapper) : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
		{
			_serviceEntityMapper = serviceEntityMapper;

			this.ConfirmAlarmPressed = new DelegateCommand(() => OnConfirmAlarmPressed());
			this.ConfirmAlarmReleased = new DelegateCommand(() => OnConfirmAlarmReleased());
			this.ConfigurationWizard = new DelegateCommand(() => OnConfigurationWizard());
		}

		private CockpitPumpsModulASettingsModel _settingsModel;
		public CockpitPumpsModulASettingsModel SettingsModel
		{
			get => _settingsModel;
			set => SetProperty(ref _settingsModel, value);
		}

		public DelegateCommand ConfirmAlarmPressed { get; private set; }
		public DelegateCommand ConfirmAlarmReleased { get; private set; }
		public DelegateCommand ConfigurationWizard { get; private set; }

		private void SetSettingsModel(CockpitPumpsModulARootModel model)
		{
			this.SettingsModel = _serviceEntityMapper.Map<CockpitPumpsModulASettingsModel>(model);

		}

		private void OnConfirmAlarmPressed()
		{
			this.SettingsModel.CmdSystemConfirmAlarm = true;
            this.EventAggregator.GetEvent<CockpitPumpModulAButtonSendEvent>().Publish(new CockpitPumpModulAButtonSendModel()
            {
                ParameterId = ExternalPumpModulAParameterIdValue.CmdSystemConfirmAlarm,
                Value = 1
            });
        }

		private void OnConfirmAlarmReleased()
		{
			this.SettingsModel.CmdSystemConfirmAlarm = false;
            this.EventAggregator.GetEvent<CockpitPumpModulAButtonSendEvent>().Publish(new CockpitPumpModulAButtonSendModel()
            {
                ParameterId = ExternalPumpModulAParameterIdValue.CmdSystemConfirmAlarm,
                Value = 0
            });
        }

		private void OnConfigurationWizard()
		{
			this.EventAggregator.GetEvent<PumpConfigurationEvent>().Publish();
		}

		public override void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);
			if (parameters.ContainsKey(NavigationConstants.CockpitPumpsModulARootModel))
			{
				var model = parameters.GetValue<CockpitPumpsModulARootModel>(NavigationConstants.CockpitPumpsModulARootModel);
				SetSettingsModel(model);
			}
		}
	}
}
