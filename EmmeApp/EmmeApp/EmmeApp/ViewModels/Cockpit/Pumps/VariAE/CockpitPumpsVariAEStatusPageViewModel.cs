﻿using Acr.UserDialogs;
using EmmeApp.Common.Attributes;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities.External;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Resources.HelpViews;
using Humanizer;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EmmeApp.ViewModels
{
    public class CockpitPumpsVariAEStatusPageViewModel : ViewModelBase
    {
        private readonly IServiceEntityMapper _serviceEntityMapper;
        private readonly IConfigurationPumpVariAManager _configurationPumpVariAManager;
        private readonly IPopupNavigation _popupNavigation;

        public CockpitPumpsVariAEStatusPageViewModel(INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IServiceEntityMapper serviceEntityMapper,
            IConfigurationPumpVariAManager configurationPumpVariAManager,
            IPopupNavigation popupNavigation)
            : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
        {
            _serviceEntityMapper = serviceEntityMapper;
            _configurationPumpVariAManager = configurationPumpVariAManager;
            _popupNavigation = popupNavigation;

            SetStatusModelEvent = (model) => SetStatusModel(model);
            SetParameterEvent = (parameter) => SetParameters(parameter);
        }

        public Action<CockpitPumpsVariAERootModel> SetStatusModelEvent { get; private set; }
        public Action<List<ParameterTExternalEntity>> SetParameterEvent { get; private set; }

        private CockpitPumpsVariAEStatusModel _statusModel;
        public CockpitPumpsVariAEStatusModel StatusModel
        {
            get => _statusModel;
            set => SetProperty(ref _statusModel, value);
        }

        private PumpAlarmAndWarningLogModel _alarmLog;
        public PumpAlarmAndWarningLogModel AlarmLog
        {
            get => _alarmLog;
            set => SetProperty(ref _alarmLog, value);
        }

        private PumpAlarmAndWarningLogModel _warningLog;
        public PumpAlarmAndWarningLogModel WarningLog
        {
            get => _warningLog;
            set => SetProperty(ref _warningLog, value);
        }

        private string _additionalModuleText;
        public string AdditionalModuleText
        {
            get => _additionalModuleText;
            set => SetProperty(ref _additionalModuleText, value);
        }

        private string _nominalValueUnit = AppResources.Label_UnitOfMeasurement_Meter;
        public string NominalValueUnit
        {
            get => _nominalValueUnit;
            set => SetProperty(ref _nominalValueUnit, value);
        }

        private void SetStatusModel(CockpitPumpsVariAERootModel model)
        {
            var statusModel = _serviceEntityMapper.Map<CockpitPumpsVariAEStatusModel>(model);
            StatusModel = statusModel;
        }

        private async Task ExecuteViewHelpTextCommand(PumpAlarmAndWarningLogModel log)
        {
            if (log != null && log.HasHelpText)
            {
                var popupOverlay = new DialogInformationOverlay();
                popupOverlay.Title = log.Title;

                var view = new LogAlarmWarningItemHelp();
                view.FindByName<Label>(UIConstants.LabelCauseMessage).Text = log.Cause;
                view.FindByName<Label>(UIConstants.LabelMeasureMessage).Text = log.Measure;

                popupOverlay.View = view;

                await _popupNavigation.PushAsync(popupOverlay);
            }
        }

        private void SetParameters(List<ParameterTExternalEntity> parameters)
        {
            var nominalValueUnit = _configurationPumpVariAManager.GetUnit(ExternalPumpVariAParameterIdValue.NominalValue, parameters);
            this.NominalValueUnit = StatusModel.ControlType == PumpControlTypeValue.ConstantSpeed ? AppResources.Label_UnitOfMeasurement_Percent : nominalValueUnit;

            if (AlarmLog != null)
            {
                AlarmLog.HasHelpText = false;

                var type = StatusModel.CurrentAlarm.GetType();
                var memInfo = type.GetMember(StatusModel.CurrentAlarm.ToString());

                if (memInfo.Any())
                {
                    var attributes = memInfo[0].GetCustomAttributes(typeof(LogHelpTextAttribute), false);
                    if (attributes.Any())
                    {
                        var attribute = (LogHelpTextAttribute)attributes.FirstOrDefault();
                        AlarmLog.HasHelpText = true;
                        AlarmLog.Title = attribute.GetTranslation(attribute.TitleResource);
                        AlarmLog.Measure = attribute.GetTranslation(attribute.MeasureResource);
                        AlarmLog.Cause = attribute.GetTranslation(attribute.CauseResource);
                    }
                }
            }

            if (WarningLog != null)
            {
                WarningLog.HasHelpText = false;

                var type = StatusModel.CurrentWarning.GetType();
                var memInfo = type.GetMember(StatusModel.CurrentWarning.ToString());

                if (memInfo.Any())
                {
                    var attributes = memInfo[0].GetCustomAttributes(typeof(LogHelpTextAttribute), false);
                    if (attributes.Any())
                    {
                        var attribute = (LogHelpTextAttribute)attributes.FirstOrDefault();
                        WarningLog.HasHelpText = true;
                        WarningLog.Title = attribute.GetTranslation(attribute.TitleResource);
                        WarningLog.Measure = attribute.GetTranslation(attribute.MeasureResource);
                        WarningLog.Cause = attribute.GetTranslation(attribute.CauseResource);
                    }
                }
            }

            var additionalModuleVal = Convert.ToInt32(StatusModel.AdditionalModule);

            if (!Enum.IsDefined(typeof(PumpAdditionalModuleValue), additionalModuleVal))
            {
                AdditionalModuleText = AppResources.Pump_AdditionalModule_UnknownModule;
            }
            else
            {
                AdditionalModuleText = EnumHumanizeExtensions.Humanize(StatusModel.AdditionalModule);
            }

            AlarmLog.AlarmLogStatus = StatusModel.CurrentAlarm;
            WarningLog.WarningLogStatus = StatusModel.CurrentWarning;
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.CockpitPumpsVariAERootModel))
            {
                var model = parameters.GetValue<CockpitPumpsVariAERootModel>(NavigationConstants.CockpitPumpsVariAERootModel);

                AlarmLog = new PumpAlarmAndWarningLogModel();
                AlarmLog.ViewHelpTextCommand = new DelegateCommand(async () => await ExecuteViewHelpTextCommand(AlarmLog));

                WarningLog = new PumpAlarmAndWarningLogModel();
                WarningLog.ViewHelpTextCommand = new DelegateCommand(async () => await ExecuteViewHelpTextCommand(WarningLog));

                SetStatusModel(model);
            }
        }
    }
}
