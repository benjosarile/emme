﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Events;
using EmmeApp.ItemModels;
using EmmeApp.Managers.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;

namespace EmmeApp.ViewModels
{
    public class CockpitPumpsVariAESettingsPageViewModel : ViewModelBase
	{
		private readonly IServiceEntityMapper _serviceEntityMapper;

		public CockpitPumpsVariAESettingsPageViewModel(INavigationService navigationService, 
            INavigationHelperService navigationHelperService, 
            IAppCenterCustomService appCenterService, 
            IUserDialogs userDialogs, 
            IEventAggregator eventAggregator, 
            IServiceEntityMapper serviceEntityMapper) : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
		{
			_serviceEntityMapper = serviceEntityMapper;

            this.ConfirmAlarmPressed = new DelegateCommand(() => OnConfirmAlarmPressed());
            this.ConfirmAlarmReleased = new DelegateCommand(() => OnConfirmAlarmReleased());
            this.ConfigurationWizard = new DelegateCommand(() => OnConfigurationWizard());
        }

        private CockpitPumpsVariAESettingsModel _settingsModel;
        public CockpitPumpsVariAESettingsModel SettingsModel
        {
            get => _settingsModel;
            set => SetProperty(ref _settingsModel, value);
        }

        public DelegateCommand ConfirmAlarmPressed { get; private set; }
        public DelegateCommand ConfirmAlarmReleased { get; private set; }
        public DelegateCommand ConfigurationWizard { get; private set; }

        public void SetSettingsModel(CockpitPumpsVariAERootModel model)
        {
            SettingsModel = _serviceEntityMapper.Map<CockpitPumpsVariAESettingsModel>(model);
        }

        private void OnConfirmAlarmPressed()
        {
            SettingsModel.CmdSystemConfirmAlarm = true;
            this.EventAggregator.GetEvent<CockpitPumpVariAEButtonSendEvent>().Publish(new CockpitPumpVariAEButtonSendModel()
            {
                ParameterId = ExternalPumpVariAParameterIdValue.CmdSystemConfirmAlarm,
                Value = 1
            });
        }

        private void OnConfirmAlarmReleased()
        {
            SettingsModel.CmdSystemConfirmAlarm = false;
            this.EventAggregator.GetEvent<CockpitPumpVariAEButtonSendEvent>().Publish(new CockpitPumpVariAEButtonSendModel()
            {
                ParameterId = ExternalPumpVariAParameterIdValue.CmdSystemConfirmAlarm,
                Value = 0
            });
        }

        private void OnConfigurationWizard()
        {
            EventAggregator.GetEvent<PumpConfigurationEvent>().Publish();
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);

			if (parameters.ContainsKey(NavigationConstants.CockpitPumpsVariAERootModel))
			{
				var model = parameters.GetValue<CockpitPumpsVariAERootModel>(NavigationConstants.CockpitPumpsVariAERootModel);
                SetSettingsModel(model);
            }
		}
	}
}
