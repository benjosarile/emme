﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Utilities.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmmeApp.ViewModels
{
    public class CockpitControlUnitLogPageViewModel : ViewModelBase
	{
		public CockpitControlUnitLogPageViewModel(INavigationService navigationService, INavigationHelperService navigationHelperService, IAppCenterCustomService appCenterService, IUserDialogs userDialogs, IEventAggregator eventAggregator) : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
		{
			this.BackCommand = new DelegateCommand(async () => await OnBack());
		}

		private List<string> _logDataList;
		public List<string> LogDataList
		{
			get => _logDataList;
			set => SetProperty(ref _logDataList, value);
		}

		public DelegateCommand BackCommand { get; private set; }

		private async Task OnBack()
		{
			await NavigationUtilities.GoBackAsync(this.NavigationService, null, null, false);
		}

		public override void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);

			var logs = parameters.GetValue<List<string>>(NavigationConstants.Logs);

			if (logs != null)
			{
				this.LogDataList = logs;
				return;
			}

			BackCommand?.Execute();
		}
	}
}
