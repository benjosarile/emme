﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Events;
using EmmeApp.ItemModels;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Humanizer;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using System;
using Xamarin.Forms;

namespace EmmeApp.ViewModels
{
    public class CockpitControlUnitOverviewPageViewModel : ViewModelBase
	{
		private readonly IServiceEntityMapper _serviceEntityMapper;

		public CockpitControlUnitOverviewPageViewModel(INavigationService navigationService, 
			INavigationHelperService navigationHelperService, 
			IAppCenterCustomService appCenterService,
			IUserDialogs userDialogs, 
			IEventAggregator eventAggregator, 
			IServiceEntityMapper serviceEntityMapper)
			: base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
		{
			_serviceEntityMapper = serviceEntityMapper;

			this.SetOverviewModelEvent = (model) => SetOverviewModel(model);

			this.ConfirmAlarmPressed = new DelegateCommand(() => OnConfirmAlarmPressed());
			this.ConfirmAlarmReleased = new DelegateCommand(() => OnConfirmAlarmReleased());
		}

		public Action<CockpitControlUnitRootModel> SetOverviewModelEvent { get; private set; }

		private CockpitControlUnitOverviewModel _overviewModel;
		public CockpitControlUnitOverviewModel OverviewModel
		{
			get => _overviewModel;
			set => SetProperty(ref _overviewModel, value);
		}

		private bool _isSystemStatusFail;
		public bool IsSystemStatusFail
		{
			get => _isSystemStatusFail;
			set => SetProperty(ref _isSystemStatusFail, value);
		}

		private string _systemStatusMessage;
		public string SystemStatusMessage
		{
			get => _systemStatusMessage;
			set => SetProperty(ref _systemStatusMessage, value);
		}

		private string _failureCode;
		public string FailureCode
		{
			get => _failureCode;
			set => SetProperty(ref _failureCode, value);
		}

		private bool _isPump1Active = false;
		public bool IsPump1Active
		{
			get => _isPump1Active;
			set => SetProperty(ref _isPump1Active, value);
		}

		private bool _isPump2Available = false;
		public bool IsPump2Available
		{
			get => _isPump2Available;
			set => SetProperty(ref _isPump2Available, value);
		}

		private bool _isPump2Active = false;
		public bool IsPump2Active
		{
			get => _isPump2Active;
			set => SetProperty(ref _isPump2Active, value);
		}

		private string _motorStatus0;
		public string MotorStatus0
		{
			get => _motorStatus0;
			set
			{
				SetProperty(ref _motorStatus0, value);
			}
		}

		private string _motorStatus1;
		public string MotorStatus1
		{
			get => _motorStatus1;
			set
			{
				SetProperty(ref _motorStatus1, value);
			}
		}

		private string _motorMode0;
		public string MotorMode0
		{
			get => _motorMode0;
			set
			{
				SetProperty(ref _motorMode0, value);
			}
		}

		private string _motorMode1;
		public string MotorMode1
		{
			get => _motorMode1;
			set
			{
				SetProperty(ref _motorMode1, value);
			}
		}

		private string _motorPhase1Current0;
		public string MotorPhase1Current0
		{
			get => _motorPhase1Current0;
			set
			{
				SetProperty(ref _motorPhase1Current0, value);
			}
		}

		private string _motorPhase1Current1;
		public string MotorPhase1Current1
		{
			get => _motorPhase1Current1;
			set
			{
				SetProperty(ref _motorPhase1Current1, value);
			}
		}

		private bool _isSegmentLevel4Filled;
		public bool IsSegmentLevel4Filled
		{
			get => _isSegmentLevel4Filled;
			set => SetProperty(ref _isSegmentLevel4Filled, value);
		}

		private bool _isSegmentLevel3Filled;
		public bool IsSegmentLevel3Filled
		{
			get => _isSegmentLevel3Filled;
			set => SetProperty(ref _isSegmentLevel3Filled, value);
		}

		private bool _isSegmentLevel2Filled;
		public bool IsSegmentLevel2Filled
		{
			get => _isSegmentLevel2Filled;
			set => SetProperty(ref _isSegmentLevel2Filled, value);
		}

		private bool _isSegmentLevel1Filled;
		public bool IsSegmentLevel1Filled
		{
			get => _isSegmentLevel1Filled;
			set => SetProperty(ref _isSegmentLevel1Filled, value);
		}

		private Color _indicatorColor = Color.Red;
		public Color IndicatorColor
		{
			get => _indicatorColor;
			set => SetProperty(ref _indicatorColor, value);
		}

		public DelegateCommand ConfirmAlarmPressed { get; private set; }
		public DelegateCommand ConfirmAlarmReleased { get; private set; }

		private void SetOverviewModel(CockpitControlUnitRootModel model)
		{
			Device.BeginInvokeOnMainThread(() =>
			{
				this.OverviewModel = _serviceEntityMapper.Map<CockpitControlUnitOverviewModel>(model);
				SetSystemStatus();
				SetPumpAvailability();
				this.MotorStatus0 = this.OverviewModel.MotorStatusMotor0.Humanize();
				this.MotorStatus1 = this.OverviewModel.MotorStatusMotor1.Humanize();
				this.MotorMode0 = this.OverviewModel.MotorModeMotor0.Humanize();
				this.MotorMode1 = this.OverviewModel.MotorModeMotor1.Humanize();
				SetMotorCurrentMode();
				SetActiveLevel();
			});
		}

		private void OnConfirmAlarmPressed()
		{
			this.IndicatorColor = Color.Green;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdSystemConfirmAlarm,
				Value = 1
			});
		}

		private void OnConfirmAlarmReleased()
		{
			this.IndicatorColor = Color.Red;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdSystemConfirmAlarm,
				Value = 0
			});
		}

		private void SetSystemStatus()
		{
			this.IsSystemStatusFail = this.OverviewModel.SystemFailure != ControlUnitSystemFailureValue.CMN_FAILURE_NO_FAILURE;

			if (this.IsSystemStatusFail)
			{
				this.SystemStatusMessage = AppResources.CockpitControlUnitStatusPage_Label_Status_FailureCode;
				this.FailureCode = this.OverviewModel.SystemFailure.Humanize();
			}
			else
			{
				this.SystemStatusMessage = AppResources.CockpitControlUnitStatusPage_Label_Status_Good;
				this.FailureCode = string.Empty;
			}
		}

		private void SetPumpAvailability()
		{
			this.IsPump2Available = this.OverviewModel.SystemDoubleMotor == ControlUnitSystemDoubleMotorValue.TwoPumps;

			this.IsPump1Active = this.OverviewModel.MotorModeMotor0 != ControlUnitMotorModeValue.Inactive && this.OverviewModel.MotorModeMotor0 != ControlUnitMotorModeValue.NotConfigured;

			this.IsPump2Active = this.IsPump2Available && this.OverviewModel.MotorModeMotor1 != ControlUnitMotorModeValue.Inactive && this.OverviewModel.MotorModeMotor1 != ControlUnitMotorModeValue.NotConfigured;
		}
		private void SetMotorCurrentMode()
		{
			this.MotorPhase1Current0 = $"{this.OverviewModel.MotorPhase1CurrentMotor0.ToString("N1")} [{AppResources.CockpitControlUnit_UnitOfMeasure_Ampere}]";
			this.MotorPhase1Current1 = $"{this.OverviewModel.MotorPhase1CurrentMotor1.ToString("N1")} [{AppResources.CockpitControlUnit_UnitOfMeasure_Ampere}]";
		}

		private void SetActiveLevel()
		{
			this.IsSegmentLevel1Filled = false;
			this.IsSegmentLevel2Filled = false;
			this.IsSegmentLevel3Filled = false;
			this.IsSegmentLevel4Filled = false;

			if (this.OverviewModel.LevelActiveLevel == ControlUnitLevelActiveValue.CMN_LEVEL_1)
			{
				this.IsSegmentLevel1Filled = true;
			}
			else if (this.OverviewModel.LevelActiveLevel == ControlUnitLevelActiveValue.CMN_LEVEL_2)
			{
				this.IsSegmentLevel1Filled = true;
				this.IsSegmentLevel2Filled = true;
			}
			else if (this.OverviewModel.LevelActiveLevel == ControlUnitLevelActiveValue.CMN_LEVEL_3)
			{
				this.IsSegmentLevel1Filled = true;
				this.IsSegmentLevel2Filled = true;
				this.IsSegmentLevel3Filled = true;
			}
			else if (this.OverviewModel.LevelActiveLevel == ControlUnitLevelActiveValue.CMN_LEVEL_4)
			{
				this.IsSegmentLevel1Filled = true;
				this.IsSegmentLevel2Filled = true;
				this.IsSegmentLevel3Filled = true;
				this.IsSegmentLevel4Filled = true;
			}
		}

		public override void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);

			if (parameters.ContainsKey(NavigationConstants.CockpitControlUnitRootModel))
			{
				var model = parameters.GetValue<CockpitControlUnitRootModel>(NavigationConstants.CockpitControlUnitRootModel);
				SetOverviewModel(model);
			}
		}

	}
}
