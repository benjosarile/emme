﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Events;
using EmmeApp.ItemModels;
using EmmeApp.Managers.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;

namespace EmmeApp.ViewModels
{
    public class CockpitControlUnitSettingsPageViewModel : ViewModelBase
	{
		private readonly IServiceEntityMapper _serviceEntityMapper;

		public CockpitControlUnitSettingsPageViewModel(INavigationService navigationService, 
			INavigationHelperService navigationHelperService, 
			IAppCenterCustomService appCenterService, 
			IUserDialogs userDialogs, 
			IEventAggregator eventAggregator, 
			IServiceEntityMapper serviceEntityMapper) : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
		{
			_serviceEntityMapper = serviceEntityMapper;

			this.ConfirmAlarmPressed = new DelegateCommand(() => OnConfirmAlarmPressed());
			this.ConfirmAlarmReleased = new DelegateCommand(() => OnConfirmAlarmReleased());
			this.ConfirmMaintenanceWarningPressed = new DelegateCommand(() => OnConfirmMaintenanceWarningPressed());
			this.ConfirmMaintenanceWarningReleased = new DelegateCommand(() => OnConfirmMaintenanceWarningReleased());
			this.TestAlarmRelayPressed = new DelegateCommand(() => OnTestAlarmRelayPressed());
			this.TestAlarmRelayReleased = new DelegateCommand(() => OnTestAlarmRelayReleased());
			this.TestCustomRelay0Pressed = new DelegateCommand(() => OnTestCustomRelay0Pressed());
			this.TestCustomRelay0Released = new DelegateCommand(() => OnTestCustomRelay0Released());
			this.TestCustomRelay1Pressed = new DelegateCommand(() => OnTestCustomRelay1Pressed());
			this.TestCustomRelay1Released = new DelegateCommand(() => OnTestCustomRelay1Released());
			this.TestCustomRelay2Pressed = new DelegateCommand(() => OnTestCustomRelay2Pressed());
			this.TestCustomRelay2Released = new DelegateCommand(() => OnTestCustomRelay2Released());
			this.TestAlarmHornPressed = new DelegateCommand(() => OnTestAlarmHornPressed());
			this.TestAlarmHornReleased = new DelegateCommand(() => OnTestAlarmHornReleased());
			this.TestAlarmBuzzerPressed = new DelegateCommand(() => OnTestAlarmBuzzerPressed());
			this.TestAlarmBuzzerReleased = new DelegateCommand(() => OnTestAlarmBuzzerReleased());
			this.ResetStatisticsPressed = new DelegateCommand(() => OnResetStatisticsPressed());
			this.ResetStatisticsReleased = new DelegateCommand(() => OnResetStatisticsReleased());
			this.ConfigurationWizard = new DelegateCommand(() => OnConfigurationWizard());
		}

		private CockpitControlUnitSettingsModel _settingsModel;
		public CockpitControlUnitSettingsModel SettingsModel
		{
			get => _settingsModel;
			set => SetProperty(ref _settingsModel, value);
		}

		public DelegateCommand ConfirmAlarmPressed { get; private set; }
		public DelegateCommand ConfirmAlarmReleased { get; private set; }
		public DelegateCommand ConfirmMaintenanceWarningPressed { get; private set; }
		public DelegateCommand ConfirmMaintenanceWarningReleased { get; private set; }
		public DelegateCommand TestAlarmRelayPressed { get; private set; }
		public DelegateCommand TestAlarmRelayReleased { get; private set; }
		public DelegateCommand TestCustomRelay0Pressed { get; private set; }
		public DelegateCommand TestCustomRelay0Released { get; private set; }
		public DelegateCommand TestCustomRelay1Pressed { get; private set; }
		public DelegateCommand TestCustomRelay1Released { get; private set; }
		public DelegateCommand TestCustomRelay2Pressed { get; private set; }
		public DelegateCommand TestCustomRelay2Released { get; private set; }
		public DelegateCommand TestAlarmHornPressed { get; private set; }
		public DelegateCommand TestAlarmHornReleased { get; private set; }
		public DelegateCommand TestAlarmBuzzerPressed { get; private set; }
		public DelegateCommand TestAlarmBuzzerReleased { get; private set; }
		public DelegateCommand ResetStatisticsPressed { get; private set; }
		public DelegateCommand ResetStatisticsReleased { get; private set; }
		public DelegateCommand ConfigurationWizard { get; private set; }

		public void SetSettingsModel(CockpitControlUnitRootModel model)
		{
			this.SettingsModel = _serviceEntityMapper.Map<CockpitControlUnitSettingsModel>(model);
		}

		private void OnConfirmAlarmPressed()
		{
			this.SettingsModel.CmdSystemConfirmAlarm = true;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdSystemConfirmAlarm,
				Value = 1
			});
		}

		private void OnConfirmAlarmReleased()
		{
			this.SettingsModel.CmdSystemConfirmAlarm = false;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdSystemConfirmAlarm,
				Value = 0
			});

		}

		private void OnConfirmMaintenanceWarningPressed()
		{
			this.SettingsModel.CmdSystemConfirmAlarmMaintenance = true;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdSystemConfirmMaintenanceWarning,
				Value = 1
			});
		}

		private void OnConfirmMaintenanceWarningReleased()
		{
			this.SettingsModel.CmdSystemConfirmAlarmMaintenance = false;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdSystemConfirmMaintenanceWarning,
				Value = 0
			});
		}

		private void OnTestAlarmRelayPressed()
		{
			this.SettingsModel.CmdM2MTestAlarmRelay = true;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdM2mTestAlarmRelay,
				Value = 1
			});
		}

		private void OnTestAlarmRelayReleased()
		{
			this.SettingsModel.CmdM2MTestAlarmRelay = false;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdM2mTestAlarmRelay,
				Value = 0
			});
		}

		private void OnTestCustomRelay0Pressed()
		{
			this.SettingsModel.CmdM2MTestCustomRelay0 = true;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdM2mTestCustomRelay0,
				Value = 1
			});
		}

		private void OnTestCustomRelay0Released()
		{
			this.SettingsModel.CmdM2MTestCustomRelay0 = false;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdM2mTestCustomRelay0,
				Value = 0
			});
		}

		private void OnTestCustomRelay1Pressed()
		{
			this.SettingsModel.CmdM2MTestCustomRelay1 = true;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdM2mTestCustomRelay1,
				Value = 1
			});
		}

		private void OnTestCustomRelay1Released()
		{
			this.SettingsModel.CmdM2MTestCustomRelay1 = false;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdM2mTestCustomRelay1,
				Value = 0
			});
		}

		private void OnTestCustomRelay2Pressed()
		{
			this.SettingsModel.CmdM2MTestCustomRelay2 = true;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdM2mTestCustomRelay2,
				Value = 1
			});
		}

		private void OnTestCustomRelay2Released()
		{
			this.SettingsModel.CmdM2MTestCustomRelay2 = false;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdM2mTestCustomRelay2,
				Value = 0
			});
		}

		private void OnTestAlarmHornPressed()
		{
			this.SettingsModel.CmdHmiTestAlarmHorn = true;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdHmiTestAlarmHorn,
				Value = 1
			});
		}

		private void OnTestAlarmHornReleased()
		{
			this.SettingsModel.CmdHmiTestAlarmHorn = false;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdHmiTestAlarmHorn,
				Value = 0
			});
		}

		private void OnTestAlarmBuzzerPressed()
		{
			this.SettingsModel.CmdHmiTestAlarmBuzzer = true;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel() {

				ParameterId = ExternalControlUnitParameterIdValue.CmdHmiTestAlarmBuzzer,
				Value = 1
			});
		}

		private void OnTestAlarmBuzzerReleased()
		{
			this.SettingsModel.CmdHmiTestAlarmBuzzer = false;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdHmiTestAlarmBuzzer,
				Value = 0
			});
		}

		private void OnResetStatisticsPressed()
		{
			this.SettingsModel.CmdStatisticReset = true;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel() {
				ParameterId = ExternalControlUnitParameterIdValue.CmdStatisticReset,
				Value = 1
			});
		}
		private void OnResetStatisticsReleased()
		{
			this.SettingsModel.CmdStatisticReset = false;
			this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(new CockpitControlUnitButtonSendModel()
			{
				ParameterId = ExternalControlUnitParameterIdValue.CmdStatisticReset,
				Value = 0
			});
		}

		private void OnConfigurationWizard()
		{
			this.EventAggregator.GetEvent<CockpitControlUnitToGoConfigurationEvent>().Publish();
		}

		public override void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);

			if (parameters.ContainsKey(NavigationConstants.CockpitControlUnitRootModel))
			{
				var model = parameters.GetValue<CockpitControlUnitRootModel>(NavigationConstants.CockpitControlUnitRootModel);
				SetSettingsModel(model);
			}
		}
	}
}
