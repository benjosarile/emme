﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Managers.Abstractions;
using Prism.Events;
using Prism.Navigation;
using System;

namespace EmmeApp.ViewModels
{
    public class CockpitControlUnitStatusPageViewModel : ViewModelBase
	{
		private readonly IServiceEntityMapper _serviceEntityMapper;

		public CockpitControlUnitStatusPageViewModel(INavigationService navigationService, INavigationHelperService navigationHelperService,
			IAppCenterCustomService appCenterService,
			IUserDialogs userDialogs, IEventAggregator eventAggregator, IServiceEntityMapper serviceEntityMapper)
			: base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
		{
			_serviceEntityMapper = serviceEntityMapper;

			SetStatusModelEvent = (model) => SetStatusModel(model);
		}

		public Action<CockpitControlUnitRootModel> SetStatusModelEvent { get; private set; }

		private CockpitControlUnitStatusModel _controlUnitStatusModel;
		public CockpitControlUnitStatusModel ControlUnitStatusModel
		{
			get => _controlUnitStatusModel;
			set => SetProperty(ref _controlUnitStatusModel, value);
		}

		private void SetStatusModel(CockpitControlUnitRootModel model)
		{
			ControlUnitStatusModel = _serviceEntityMapper.Map<CockpitControlUnitStatusModel>(model);
		}

		public override void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);

			if (parameters.ContainsKey(NavigationConstants.CockpitControlUnitRootModel))
			{
				var model = parameters.GetValue<CockpitControlUnitRootModel>(NavigationConstants.CockpitControlUnitRootModel);
				SetStatusModel(model);
			}
		}
	}
}
