﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities.External;
using EmmeApp.Events;
using EmmeApp.ItemModels;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Plugin.BluetoothLE;
using Plugin.Messaging;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EmmeApp.ViewModels
{
    public class CockpitControlUnitRootPageViewModel : ViewModelBase
    {
        private readonly IMessaging _messagingPlugin;
        private readonly IFileService _fileService;
        private readonly IBleManager _bleManager;
        private readonly ICockpitControlUnitManager _cockpitControlUnitManager;
        private readonly ILogControlUnitManager _logControlUnitManager;

        private readonly IPopupNavigation _popupNavigation;
        private DialogAlertSingle _lostConnectionAlertDialog = null;
        private bool _isIntentionallyReconnecting = false;
        private readonly SemaphoreSlim _connectionManagementThrottle = new SemaphoreSlim(1, 1);

        private readonly SubscriptionToken _configureEventSubToken;
        private readonly SubscriptionToken _buttonSendEventSubToken;
        private readonly SubscriptionToken _connectionEstablishedSubToken;
        private readonly SubscriptionToken _connectionLostSubToken;

        public CockpitControlUnitRootPageViewModel(INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IMessaging messagingPlugin,
            IFileService fileService,
            IBleManager bleManager,
            ICockpitControlUnitManager cockpitControlUnitManager,
            ILogControlUnitManager logControlUnitManager,
            IPopupNavigation popupNavigation)
            : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
        {
            _messagingPlugin = messagingPlugin;
            _fileService = fileService;
            _bleManager = bleManager;
            _cockpitControlUnitManager = cockpitControlUnitManager;
            _logControlUnitManager = logControlUnitManager;
            _popupNavigation = popupNavigation;

            _configureEventSubToken = this.EventAggregator.GetEvent<CockpitControlUnitToGoConfigurationEvent>().Subscribe(async () => await OnConfigure());
            _buttonSendEventSubToken = this.EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Subscribe(async (model) => await OnButtonSend(model));
            _connectionEstablishedSubToken = EventAggregator.GetEvent<ConnectionEstablishedEvent>().Subscribe(async () => await OnConnectionEstablished());
            _connectionLostSubToken = EventAggregator.GetEvent<ConnectionLostEvent>().Subscribe(async () => await OnConnectionLost());

            this.ConfigureCommand = new DelegateCommand(async () => await OnConfigure(), () => this.CanExecuteCommand).ObservesProperty(() => this.CanExecuteCommand);
            this.ShareCommand = new DelegateCommand(async () => await OnShare(), () => this.CanExecuteCommand).ObservesProperty(() => this.CanExecuteCommand);
            this.ToggleMenuCommand = new DelegateCommand(() => OnToggleMenu(), () => this.CanExecuteCommand).ObservesProperty(() => this.CanExecuteCommand);
            this.ViewLogCommand = new DelegateCommand(async () => await ViewLog(), () => this.CanExecuteCommand).ObservesProperty(() => this.CanExecuteCommand);

        }

        public CockpitControlUnitOverviewPageViewModel CockpitControlUnitOverviewPageViewModel { get; private set; }
        public CockpitControlUnitStatusPageViewModel CockpitControlUnitStatusPageViewModel { get; private set; }
        public CockpitControlUnitSettingsPageViewModel CockpitControlUnitSettingsPageViewModel { get; private set; }
        public CockpitControlUnitRootModel RootModel { get; private set; }
        public ControlUnitLogRootModel LogRootModel { get; set; }

        private List<ParameterTExternalEntity> _storedParameters;

        private List<ParameterTExternalEntity> _initialParameters;
        public List<ParameterTExternalEntity> InitialParameters
        {
            get => _initialParameters;
            set => SetProperty(ref _initialParameters, value);
        }

        private IDisposable _notifyDisposable;
        private IDisposable _deviceStatusDisposable;
        private IGattCharacteristic _characteristic;

        private bool _canExecuteCommand = true;
        public bool CanExecuteCommand
        {
            get => _canExecuteCommand;
            set => SetProperty(ref _canExecuteCommand, value);
        }

        private List<ParameterControlUnitCharacteristicExternalEntity> _parameterCharacteristicList;
        public List<ParameterControlUnitCharacteristicExternalEntity> ParameterCharacteristicList
        {
            get => _parameterCharacteristicList;
            set => SetProperty(ref _parameterCharacteristicList, value);
        }

        public DelegateCommand ConfigureCommand { get; private set; }
        public DelegateCommand ShareCommand { get; private set; }
        public DelegateCommand ToggleMenuCommand { get; private set; }
        public DelegateCommand ViewLogCommand { get; private set; }

        private async Task OnShare()
        {
            this.CanExecuteCommand = false;
            if (_messagingPlugin.EmailMessenger.CanSendEmail)
            {
                this.UserDialogs.ShowLoading(AppResources.Loading_Message_EmailRedirect);

                this.RootModel.FactorySerialNumber = this.RootModel.FactorySerialNumber ?? string.Empty;
                var dateNow = DateTime.Now;
                string subject = $"{this.RootModel.FactorySerialNumber}_{dateNow.ToString("yyyyMMddhhmmss")}";
                string filename = $"{this.RootModel.FactorySerialNumber}_{dateNow.ToString("yyyyMMddhhmmss")}.txt";

                string filePath = _fileService.GetFilePath(filename);
                string newLine = "\r\n";

                if (Device.RuntimePlatform == Device.iOS)
                {
                    newLine = "\n";
                }

                var cockpitSb = _cockpitControlUnitManager.GenerateEmailContent(this.RootModel, newLine);
                var logSb = _logControlUnitManager.GenerateEmailContent(this.LogRootModel, newLine);

                cockpitSb.Append(newLine);
                cockpitSb.Append(logSb);

                _fileService.WriteToAFile(filePath, cockpitSb.ToString(), false);

                var email = new EmailMessageBuilder()
                    .Subject(subject)
                    .Body(cockpitSb.ToString())
                    .WithAttachment(filePath, "text/plain")
                    .Build();

                _messagingPlugin.EmailMessenger.SendEmail(email);

                this.UserDialogs.HideLoading();
            }
            else
            {
                await this.UserDialogs.AlertAsync(AppResources.Alert_Message_FailedToCreateMail_Message, AppResources.Alert_Message_FailedToCreateMail_Title);
            }

            this.CanExecuteCommand = true;
        }

        private async Task OnConfigure()
        {
            this.CanExecuteCommand = false;

            await UnsubscribeFromCharacteristicNotifications();

            var model = _cockpitControlUnitManager.MapToConfigurationModel(this.RootModel);
            INavigationParameters parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.InitialStartupWizardModel, model);
            parameters.Add(NavigationConstants.InitialParameters, this.InitialParameters);
            parameters.Add(NavigationConstants.ParameterCharacteristics, this.ParameterCharacteristicList);

            string navigationPath = $"{ViewNames.InitialStartupForm1CustomerInfoPage}";
            this.EventAggregator.GetEvent<MasterNavigationEvent>().Publish(new NavigationItemModel()
            {
                NavigationPath = navigationPath,
                Parameters = parameters,
                IsModalNavigation = true
            });
            this.CanExecuteCommand = true;
        }

        private void OnToggleMenu()
        {
            this.CanExecuteCommand = false;
            this.EventAggregator.GetEvent<HamburgerTappedEvent>().Publish();
            this.CanExecuteCommand = true;
        }

        private async Task ViewLog()
        {
            this.CanExecuteCommand = false;

            await UnsubscribeFromCharacteristicNotifications();

            var logs = _storedParameters.Select(parameter =>
            {
                return $"{parameter.ControlUnit}: {parameter.Value.Number} {parameter.Value.Text}";
            }).ToList();


            INavigationParameters parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.Logs, logs);

            this.EventAggregator.GetEvent<MasterNavigationEvent>().Publish(new NavigationItemModel()
            {
                NavigationPath = ViewNames.CockpitControlUnitLogPage,
                Parameters = parameters,
                IsModalNavigation = true
            });

            this.CanExecuteCommand = true;
        }

        private async Task OnButtonSend(CockpitControlUnitButtonSendModel model)
        {
            var connectedDevice = await _bleManager.GetConnectedDevice();

            if (connectedDevice != null)
            {
                var characteristic = await _bleManager.GetDeviceWriteCharacteristic(connectedDevice);

                if (characteristic != null)
                {
                    var parametersT = new ParametersTExternalEntity();
                    parametersT.Parameters.Add(new ParameterTExternalEntity()
                    {
                        IdCase = ExternalIdOneofCaseValue.ControlUnit,
                        ControlUnit = model.ParameterId,
                        Value = new ValueTExternalEntity()
                        {
                            Number = model.Value,
                            ValueCase = ExternalValueOneofCaseValue.Number
                        }
                    });

                    await _bleManager.WriteCommand(parametersT, characteristic, ExternalIdOneofCaseValue.ControlUnit);
                }
            }
        }

        public void SetCockpitControlUnitOverviewPageViewModel(CockpitControlUnitOverviewPageViewModel vm)
        {
            this.CockpitControlUnitOverviewPageViewModel = vm;
        }

        public void SetCockpitControlUnitStatusPageViewModel(CockpitControlUnitStatusPageViewModel vm)
        {
            this.CockpitControlUnitStatusPageViewModel = vm;
        }

        public void SetCockpitControlUnitSettingsPageViewModel(CockpitControlUnitSettingsPageViewModel vm)
        {
            this.CockpitControlUnitSettingsPageViewModel = vm;
        }

        private void ReadParameter(CharacteristicGattResult result)
        {
            if (result.Data != null && this.RootModel != null)
            {
                var resultData = result.Data;

                var hexDataValue = BitConverter.ToString(resultData);
                Debug.WriteLine(hexDataValue);

                var parameters = _bleManager.GetInitializeParameters(result.Data);

                if (parameters.All(x => x.Value != null))
                {
                    foreach (var newParameter in parameters)
                    {
                        var parameter = this.InitialParameters.FirstOrDefault(x => x.ControlUnit == newParameter.ControlUnit);
                        if (parameter != null)
                        {
                            parameter.Value = newParameter.Value;

                            if (newParameter.Unit != ExternalUnitTValue.UnknownUnit)
                            {
                                parameter.Unit = newParameter.Unit;
                            }

                            if (newParameter.Scaling != null)
                            {
                                parameter.Scaling = newParameter.Scaling;
                            }

                            newParameter.Unit = parameter.Unit;
                            newParameter.Scaling = parameter.Scaling;
                        }
                    }

                    var modelResult = _cockpitControlUnitManager.MapParamtersToModel(this.RootModel, parameters);
                    this.RootModel = modelResult.Item1;

                    if (this.CockpitControlUnitOverviewPageViewModel != null)
                    {
                        this.CockpitControlUnitOverviewPageViewModel.SetOverviewModelEvent(this.RootModel);
                    }

                    if (this.CockpitControlUnitOverviewPageViewModel != null)
                    {
                        this.CockpitControlUnitStatusPageViewModel.SetStatusModelEvent(this.RootModel);
                    }
                }
            }
        }

        private async Task<bool> SetBleCharacteristic()
        {
            await Task.Delay(100);
            this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);

            var connectedDevice = await _bleManager.GetConnectedDevice();

            if (_characteristic != null && _characteristic.IsNotifying && connectedDevice != null)
            {
                await _characteristic.DisableNotifications();
                _notifyDisposable?.Dispose();
            }

            _characteristic = null;

            _isIntentionallyReconnecting = true;
            var newConnectedDevice = await _bleManager.RestartTheDevice(connectedDevice);
            await Task.Delay(200);//TODO Waiting was adjusted from 2000 to 200
            if (newConnectedDevice != null && newConnectedDevice.Status == ConnectionStatus.Connected)
            {
                var characteristic = await _bleManager.GetDeviceReadCharacteristic(newConnectedDevice);

                if (characteristic != null)
                {
                    if (_storedParameters == null || !_storedParameters.Any())
                    {
                        _storedParameters = new List<ParameterTExternalEntity>();
                    }

                    _notifyDisposable?.Dispose();
                    _notifyDisposable =
                        characteristic
                            .WhenNotificationReceived()
                            .Subscribe(
                                result => ReadParameter(result),
                                ex => this.UserDialogs.Alert(ex.ToString())
                            );

                    _deviceStatusDisposable?.Dispose();
                    _deviceStatusDisposable =
                        newConnectedDevice
                            .WhenStatusChanged()
                            .Subscribe(
                                async (connectionStatus) =>
                                {
                                    switch (connectionStatus)
                                    {
                                        case ConnectionStatus.Connected:
                                           await  OnConnectionEstablished();
                                            break;

                                        case ConnectionStatus.Disconnected:
                                           await OnConnectionLost();
                                            break;
                                    }
                                });

                    await characteristic.EnableNotifications();
                    this.UserDialogs.HideLoading();
                    _characteristic = characteristic;

                    return true;
                }
            }

            this.UserDialogs.HideLoading();
            return false;
        }

        private async Task OnConnectionEstablished()
        {
            if (!(await _connectionManagementThrottle.WaitAsync(0)))
                return;

            _isIntentionallyReconnecting = false;

            await RemoveAlertForLostConnection();

            _connectionManagementThrottle.Release();
        }

        private async Task OnConnectionLost()
        {
            if (!(await _connectionManagementThrottle.WaitAsync(0)))
                return;

            if (_isIntentionallyReconnecting)
            {
                _connectionManagementThrottle.Release();
                return;
            }

            await UnsubscribeFromCharacteristicNotifications();
            await AlertUserOfLostConnection();

            _connectionManagementThrottle.Release();
        }

        private async Task AlertUserOfLostConnection()
        {
            if (_lostConnectionAlertDialog != null)
                return;

            _lostConnectionAlertDialog = new DialogAlertSingle();
            _lostConnectionAlertDialog.ButtonText = AppResources.Alert_Message_OK;
            _lostConnectionAlertDialog.ContentText = AppResources.Alert_MessageBody_BLE_LostConnection;
            _lostConnectionAlertDialog.Title = AppResources.Alert_MessageHeader_BLE_LostConnection;
            _lostConnectionAlertDialog.ButtonCommand =
                new DelegateCommand(
                    () =>
                    {
                        _lostConnectionAlertDialog = null;
                        EventAggregator.GetEvent<HamburgerMenuItemExecuteEvent>().Publish(new ItemModels.HamburgerMenuModel() { Menu = HamburgerMenuValue.Home, Parameters = null });
                    });
            await _popupNavigation.PushAsync(_lostConnectionAlertDialog, false);
        }

        private async Task RemoveAlertForLostConnection()
        {
            if (_lostConnectionAlertDialog != null)
            {
                await _popupNavigation.RemovePageAsync(_lostConnectionAlertDialog);
                _lostConnectionAlertDialog = null;
            }
        }

        private async Task UnsubscribeFromCharacteristicNotifications()
        {
            try
            {
                if (_characteristic != null && _characteristic.IsNotifying)
                {
                    var connectedDevice = await _bleManager.GetConnectedDevice();
                    if (_characteristic != null && _characteristic.IsNotifying && connectedDevice != null && connectedDevice.Status == ConnectionStatus.Connected)
                    {
                        await _characteristic.DisableNotifications();
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                try
                {
                    _notifyDisposable?.Dispose();
                    _notifyDisposable = null;
                    _deviceStatusDisposable?.Dispose();
                    _deviceStatusDisposable = null;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }

                _characteristic = null;
            }

        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.CockpitControlUnitRootModel))
            {
                var model = parameters.GetValue<CockpitControlUnitRootModel>(NavigationConstants.CockpitControlUnitRootModel);

                if (model != null)
                {
                    this.RootModel = model;
                }
            }

            if (parameters.ContainsKey(NavigationConstants.InitialParameters))
            {
                this.InitialParameters = parameters.GetValue<List<ParameterTExternalEntity>>(NavigationConstants.InitialParameters);
            }

            if (parameters.ContainsKey(NavigationConstants.ParameterCharacteristics))
            {
                this.ParameterCharacteristicList = parameters.GetValue<List<ParameterControlUnitCharacteristicExternalEntity>>(NavigationConstants.ParameterCharacteristics);
            }

            if (parameters.ContainsKey(NavigationConstants.ControlUnitLogRootModel))
            {
                this.LogRootModel = parameters.GetValue<ControlUnitLogRootModel>(NavigationConstants.ControlUnitLogRootModel);
            }

            var setResult = await SetBleCharacteristic();

            if (!setResult)
            {
                await this.UserDialogs.AlertAsync(AppResources.Alert_Message_CantFetchData);
                this.EventAggregator.GetEvent<MasterNavigationEvent>().Publish(new NavigationItemModel
                {
                    NavigationPath = $"{ViewNames.NavigationPage}/{ViewNames.HomePage}",
                    Parameters = new NavigationParameters(),
                    IsModalNavigation = false
                });
            }
        }

        public override async void Destroy()
        {
            await UnsubscribeFromCharacteristicNotifications();
            _lostConnectionAlertDialog = null;
            EventAggregator.GetEvent<CockpitControlUnitToGoConfigurationEvent>().Unsubscribe(_configureEventSubToken);
            EventAggregator.GetEvent<CockpitControlUnitButtonSendEvent>().Unsubscribe(_buttonSendEventSubToken);
            EventAggregator.GetEvent<ConnectionEstablishedEvent>().Unsubscribe(_connectionEstablishedSubToken);
            EventAggregator.GetEvent<ConnectionLostEvent>().Unsubscribe(_connectionLostSubToken);
        }
    }
}
