﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.Configuration;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Events;
using EmmeApp.ItemModels;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Plugin.BluetoothLE;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

/// <summary>
/// Structure List
/// 1. Private properties expect get-set private properties
/// 2. Constructor
/// 3. Public Properties (get-set)
/// 4. Command
/// 5. Command Methods
/// 6. Private Methods
/// 7. Overide Methods (OnNavigatingTo, OnNavigated, OnNavigatedFrom, Destroy) 
/// </summary>
namespace EmmeApp.ViewModels
{
    public class EmmeMasterDetailPageViewModel : ViewModelBase
    {
        private static readonly string HOME_NAVIGATION_PATH = $"{ViewNames.NavigationPage}/{ViewNames.HomePage}";

        private readonly IBleManager _bleManager;
        private readonly IParameterDataCacheManager _parameterDataCacheManager = null;
        private readonly ICockpitControlUnitManager _cockpitControlUnitManager;
        private readonly IConfigurationControlUnitManager _configurationControlUnitManager;
        private readonly IConfigurationPumpModulAManager _configurationPumpModulAManager;
        private readonly IConfigurationPumpVariAManager _configurationPumpVariAManager;
        private readonly ILogControlUnitManager _logControlUnitManager;
        private readonly ILogPumpManager _logPumpManager;
        private readonly ICockpitPumpsModulAManager _cockpitPumpsModulAManager;
        private readonly ICockpitPumpsVariAEManager _cockpitPumpsVariAEManager;
        private readonly IAdapter _adapter;

        private IDisposable _scanDisposable;
        private IDevice _previouslyConnectedDevice;

        private readonly SemaphoreSlim _connectionManagementThrottle = new SemaphoreSlim(1, 1);
        private readonly SubscriptionToken _masterNavigationEventSubToken;
        private readonly SubscriptionToken _hamburgerMenuItemExecuteEventSubToken;
        private readonly SubscriptionToken _gatewayParametersEventSubToken;

        public EmmeMasterDetailPageViewModel(INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IBleManager bleManager,
            IParameterDataCacheManager parameterDataCacheManager,
            IAdapter adapter,
            ICockpitControlUnitManager cockpitControlUnitManager,
            IConfigurationControlUnitManager configurationControlUnitManager,
            IConfigurationPumpModulAManager configurationPumpModulAManager,
            IConfigurationPumpVariAManager configurationPumpVariAManager,
            ILogControlUnitManager logControlUnitManager,
            ILogPumpManager logPumpManager,
            ICockpitPumpsModulAManager cockpitPumpsModulAManager,
            ICockpitPumpsVariAEManager cockpitPumpsVariAEManager)
            : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
        {
            _bleManager = bleManager;
            _parameterDataCacheManager = parameterDataCacheManager;
            _cockpitControlUnitManager = cockpitControlUnitManager;
            _configurationControlUnitManager = configurationControlUnitManager;
            _configurationPumpModulAManager = configurationPumpModulAManager;
            _configurationPumpVariAManager = configurationPumpVariAManager;
            _cockpitPumpsModulAManager = cockpitPumpsModulAManager;
            _cockpitPumpsVariAEManager = cockpitPumpsVariAEManager;
            _adapter = adapter;
            _logControlUnitManager = logControlUnitManager;
            _logPumpManager = logPumpManager;

            _masterNavigationEventSubToken = this.EventAggregator.GetEvent<MasterNavigationEvent>().Subscribe(async (item) => await OnNavigation(item));
            _hamburgerMenuItemExecuteEventSubToken = this.EventAggregator.GetEvent<HamburgerMenuItemExecuteEvent>().Subscribe(async (item) => await OnHamburgerMenuSelect(item));
            _gatewayParametersEventSubToken = this.EventAggregator.GetEvent<GatewayParametersEvent>().Subscribe(async (item) => await OnNavigateToGatewayPage(item));
            
            this.CloseMenuCommand = new DelegateCommand(() => OnCloseMenu());
            this.SelectMasterDetailItemCommand = new DelegateCommand<MasterNavigationItemModel>(async (item) => await OnSelectMasterDetailItem(item));

            this.MasterDetailItems = new ObservableCollection<MasterNavigationItemModel>();
        }

        private MasterNavigationItemModel _selectedMasterDetailItem;
        public MasterNavigationItemModel SelectedMasterDetailItem
        {
            get => _selectedMasterDetailItem;
            set => SetProperty(ref _selectedMasterDetailItem, value);
        }

        private ObservableCollection<MasterNavigationItemModel> _masterDetailItems;
        public ObservableCollection<MasterNavigationItemModel> MasterDetailItems
        {
            get => _masterDetailItems;
            set => SetProperty(ref _masterDetailItems, value);
        }

        private bool _isBleConnected;
        public bool IsBleConnected
        {
            get => _isBleConnected;
            set => SetProperty(ref _isBleConnected, value);
        }


        public DelegateCommand CloseMenuCommand { get; private set; }
        public DelegateCommand<MasterNavigationItemModel> SelectMasterDetailItemCommand { get; private set; }

        private void OnCloseMenu()
        {
            this.EventAggregator.GetEvent<HamburgerHideEvent>().Publish();
        }

        private async Task OnSelectMasterDetailItem(MasterNavigationItemModel item)
        {
            foreach (var menuItem in this.MasterDetailItems)
            {
                menuItem.IsSelected = false;
            }

            item.IsSelected = true;
            this.SelectedMasterDetailItem = item;

            this.EventAggregator.GetEvent<HamburgerHideEvent>().Publish();

            if (item.Action != null)
            {
                item.Action.Invoke();
            }
            else
            {
                await Navigate(item.NavigationPath, item.Parameters, item.IsModalNavigation);
            }
        }

        /// <summary>
        /// This is use for navigating to pages triggered by external event.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private async Task OnNavigation(NavigationItemModel item)
        {
            await Navigate(item.NavigationPath, item.Parameters, item.IsModalNavigation);
        }

        private async Task Navigate(string navigationPath, INavigationParameters parameters, bool isModalNavigation)
        {
            string page = $"{navigationPath}";

            var navigationResult = await NavigationUtilities.NavigateAsync(this.NavigationService, page, parameters, isModalNavigation, false);

            if (!navigationResult.Success)
            {
                throw navigationResult.Exception;
            }
        }

        public async Task OnNavigateToGatewayPage(NavigationItemModel gatewayParameters)
        {
            await Task.Delay(1);

            this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);

            await Task.Delay(200);

            this.UserDialogs.HideLoading();

            string page = $"{gatewayParameters.NavigationPath}";

            var navigationResult = await NavigationUtilities.NavigateAsync(this.NavigationService, page, gatewayParameters.Parameters, gatewayParameters.IsModalNavigation, false);

            if (!navigationResult.Success)
            {
                throw navigationResult.Exception;
            }
        }

        private void PopulateMenuItems()
        {
            if (!this.MasterDetailItems.Any())
            {
                this.AppCenterService.TrackEvent($"{ViewNames.EmmeMasterDetailPage} - Populate Menu Items");

                this.MasterDetailItems.Add(new MasterNavigationItemModel()
                {
                    MenuItemId = HamburgerMenuValue.Home,
                    Title = AppResources.Hamburger_Menu_Home,
                    IsSelected = false,
                    IsModalNavigation = false,
                    NavigationPath = HOME_NAVIGATION_PATH
                });

                this.MasterDetailItems.Add(new MasterNavigationItemModel()
                {
                    MenuItemId = HamburgerMenuValue.Connect,
                    Title = AppResources.Hamburger_Menu_Connect,
                    Action = async () => await ExecuteConnectToGatewayCommand()
                });

                this.MasterDetailItems.Add(new MasterNavigationItemModel()
                {
                    MenuItemId = HamburgerMenuValue.PumpReplacement,
                    Title = AppResources.Hamburger_Menu_Pump_Replacement,
                    IsSelected = false,
                    IsModalNavigation = true,
                    NavigationPath = ViewNames.SearchPumpPage
                });

                this.MasterDetailItems.Add(new MasterNavigationItemModel()
                {
                    MenuItemId = HamburgerMenuValue.Documents,
                    Title = AppResources.Hamburger_Menu_Documents,
                    IsSelected = false,
                    IsModalNavigation = true,
                    NavigationPath = ViewNames.DocumentSearchPage
                });

                this.MasterDetailItems.Add(new MasterNavigationItemModel()
                {
                    MenuItemId = HamburgerMenuValue.Cockpit,
                    Title = AppResources.Hamburger_Menu_Cockpit,
                    Action = async () => await CockpitNavigationAsync()
                });

                this.MasterDetailItems.Add(new MasterNavigationItemModel()
                {
                    MenuItemId = HamburgerMenuValue.Configuration,
                    Title = AppResources.Hamburger_Menu_Configuration,
                    IsSelected = false,
                    Action = async () => await ConfigurationNavigationAsync()
                });

                this.MasterDetailItems.Add(new MasterNavigationItemModel()
                {
                    MenuItemId = HamburgerMenuValue.Log,
                    Title = AppResources.Hamburger_Menu_Log,
                    Action = async () => await LogNavigationAsync()
                });

                this.MasterDetailItems.Add(new MasterNavigationItemModel()
                {
                    MenuItemId = HamburgerMenuValue.Contact,
                    Title = AppResources.Hamburger_Menu_Contact,
                    IsSelected = false,
                    IsModalNavigation = true,
                    NavigationPath = ViewNames.ContactPage
                });

                this.MasterDetailItems.Add(new MasterNavigationItemModel()
                {
                    MenuItemId = HamburgerMenuValue.About,
                    Title = AppResources.Hamburger_Menu_About,
                    IsSelected = false,
                    IsModalNavigation = true,
                    NavigationPath = ViewNames.AboutPage
                });

                this.MasterDetailItems.Add(new MasterNavigationItemModel()
                {
                    MenuItemId = HamburgerMenuValue.AppSettings,
                    Title = AppResources.Hamburger_Menu_AppSettings,
                    IsSelected = false,
                    IsModalNavigation = true,
                    NavigationPath = ViewNames.AppSettingsPage
                });
            }
        }

        private async Task OnHamburgerMenuSelect(HamburgerMenuModel menuModel)
        {
            var menuItem = this.MasterDetailItems.FirstOrDefault(x => x.MenuItemId == menuModel.Menu);

            if (menuItem != null)
            {
                menuItem.Parameters = menuModel.Parameters ?? new NavigationParameters();
                await OnSelectMasterDetailItem(menuItem);
            }
        }

        private async Task ExecuteConnectToGatewayCommand()
        {
            if (this.IsBleConnected)
            {
                var connectedDevices = await _adapter.GetConnectedDevices();
                foreach (var connectedDevice in connectedDevices)
                {
                    connectedDevice.CancelConnection();
                }

                _parameterDataCacheManager.Reset();

                SetBleMenuItemTitle();
            }
            else
            {
                _parameterDataCacheManager.Reset();

                var parameters = new NavigationParameters();
                await Navigate($"{ViewNames.ConnectToGatewayPage}", parameters, true);
            }

        }

        private void SetBleMenuItemTitle()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var bleMenuItem = this.MasterDetailItems.FirstOrDefault(m => m.MenuItemId == HamburgerMenuValue.Connect);

                if (bleMenuItem != null)
                {
                    bleMenuItem.Title = this.IsBleConnected ? AppResources.Hamburger_Menu_Disconnect : AppResources.Hamburger_Menu_Connect;
                }
            });
        }

        private void SubscribeConnectionStatus()
        {
            if (_scanDisposable == null)
            {
                this.AppCenterService.TrackEvent($"{ViewNames.EmmeMasterDetailPage} - SubscribeConnectionStatus");

                _scanDisposable =
                    Observable.Interval(TimeSpan.FromSeconds(1))
                        .ObserveOn(RxApp.MainThreadScheduler)
                        .Subscribe(async (x) =>
                        {
                            if (!(await _connectionManagementThrottle.WaitAsync(0)))
                                return;

                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            var hasConnectedDevice = connectedDevice != null;

                            if (this.IsBleConnected != hasConnectedDevice)
                            {
                                this.IsBleConnected = hasConnectedDevice;

                                if (this.IsBleConnected)
                                {
                                    _previouslyConnectedDevice = connectedDevice;
                                    EventAggregator.GetEvent<ConnectionEstablishedEvent>().Publish();
                                }
                                else
                                {
                                    _previouslyConnectedDevice = null;
                                    EventAggregator.GetEvent<ConnectionLostEvent>().Publish();
                                }

                                SetBleMenuItemTitle();
                            }
                            _connectionManagementThrottle.Release();
                        });
            }
        }

        private async Task ConfigurationNavigationAsync()
        {
            try
            {
                this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);

                var initialData = await _parameterDataCacheManager.GetAndCacheParameters(onError: ex => this.UserDialogs.Alert(ex.ToString()));

                if (initialData != null && initialData.Item1 != null && initialData.Item2 != null)
                {
                    var initCommunication = initialData.Item1;
                    var initParameters = initialData.Item2;
                    var initLogs = initialData.Item3;

                    if (initCommunication != null)
                    {
                        INavigationParameters parameters = new NavigationParameters();

                        if (initCommunication.ProfileId == ExternalComProfileValue.Dismessa)
                        {
                            var model = new InitialStartupWizardModel();
                            var modelResult = _configurationControlUnitManager.MapParametersToWizardModel(model, initParameters);
                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            model = modelResult.Item1;
                            model.SelectedDeviceUuid = connectedDevice.Uuid;

                            var logRootModel = new ControlUnitLogRootModel();
                            logRootModel = _logControlUnitManager.MapParametersToWizardModel(logRootModel, initLogs);

                            parameters.Add(NavigationConstants.ControlUnitLogRootModel, logRootModel);
                            parameters.Add(NavigationConstants.InitialStartupWizardModel, model);
                            parameters.Add(NavigationConstants.ParameterCharacteristics, modelResult.Item2);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);

                            string navigationPath = $"{ViewNames.InitialStartupForm1CustomerInfoPage}";
                            this.UserDialogs.HideLoading();

                            await OnNavigation(new NavigationItemModel()
                            {
                                NavigationPath = navigationPath,
                                Parameters = parameters,
                                IsModalNavigation = true
                            });

                            return;
                        }
                        else if (initCommunication.ProfileId == ExternalComProfileValue.ModulA)
                        {
                            var model = new PumpConfigurationModulAWizardModel();
                            var modelResult = _configurationPumpModulAManager.MapParametersToWizardModel(model, initParameters);
                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            model = modelResult.Item1;
                            model.SelectedDeviceUuid = connectedDevice.Uuid;

                            var cockpitPumpRootModel = new CockpitPumpsModulARootModel();
                            var rootModelResult = _cockpitPumpsModulAManager.MapParametersToModel(cockpitPumpRootModel, initParameters);
                            cockpitPumpRootModel = rootModelResult.Item1;

                            parameters.Add(NavigationConstants.PumpConfigurationModulAWizardModel, model);
                            parameters.Add(NavigationConstants.ParameterCharacteristics, modelResult.Item2);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);
                            parameters.Add(NavigationConstants.Logs, initLogs);
                            parameters.Add(NavigationConstants.CockpitPumpsModulARootModel, cockpitPumpRootModel);

                            string navigationPath = $"{ViewNames.PumpConfigurationModulAForm1Page}";
                            await OnNavigation(new NavigationItemModel()
                            {
                                NavigationPath = navigationPath,
                                Parameters = parameters,
                                IsModalNavigation = true
                            });
                            this.UserDialogs.HideLoading();
                            return;
                        }
                        else if (initCommunication.ProfileId == ExternalComProfileValue.VariA)
                        {
                            var model = new PumpConfigurationVariAWizardModel();
                            var modelResult = _configurationPumpVariAManager.MapParametersToWizardModel(model, initParameters);
                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            model = modelResult.Item1;
                            model.SelectedDeviceUuid = connectedDevice.Uuid;

                            var cockpitPumpRootModel = new CockpitPumpsVariAERootModel();
                            var rootModelResult = _cockpitPumpsVariAEManager.MapParametersToModel(cockpitPumpRootModel, initParameters);
                            cockpitPumpRootModel = rootModelResult.Item1;

                            parameters.Add(NavigationConstants.PumpConfigurationVariAWizardModel, model);
                            parameters.Add(NavigationConstants.ParameterCharacteristics, modelResult.Item2);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);
                            parameters.Add(NavigationConstants.Logs, initLogs);
                            parameters.Add(NavigationConstants.CockpitPumpsVariAERootModel, cockpitPumpRootModel);

                            string navigationPath = $"{ViewNames.PumpConfigurationVariAForm1Page}";
                            await OnNavigation(new NavigationItemModel()
                            {
                                NavigationPath = navigationPath,
                                Parameters = parameters,
                                IsModalNavigation = true
                            });

                            this.UserDialogs.HideLoading();
                            return;
                        }
                    }
                    else
                    {
                        this.UserDialogs.HideLoading();
                        await this.UserDialogs.AlertAsync(AppResources.Alert_Message_CantGetParameters, okText: AppResources.Alert_Message_BLE_Permission_Ok);
                    }
                }

                this.UserDialogs.HideLoading();

                if (initialData == null || initialData.Item4 == null)
                {
                    INavigationParameters pageNameParam = new NavigationParameters();
                    pageNameParam.Add(NavigationConstants.PageNameParameter, AppConstants.Configuration);

                    await OnNavigation(new NavigationItemModel()
                    {
                        NavigationPath = $"{ViewNames.ConnectToGatewayPage}",
                        Parameters = pageNameParam,
                        IsModalNavigation = true
                    });
                }
                else if (initialData.Item1 == null || initialData.Item2 == null || initialData.Item3 == null)
                {
                    await this.UserDialogs.AlertAsync(AppResources.Alert_Message_CantGetParameters, okText: AppResources.Alert_Message_BLE_Permission_Ok);
                }

            }
            catch (Exception ex)
            {
                var properties = new Dictionary<string, string> {
                    { "exception", ex.Message},
                    { "viewModel", nameof(EmmeMasterDetailPageViewModel) },
                    { "method", "ConfigurationNavigation"},
                };

                this.AppCenterService.TrackError(new Exception(ex.Message), properties);
            }
            finally
            {
                this.UserDialogs.HideLoading();
            }
        }

        private async Task CockpitNavigationAsync()
        {
            try
            {
                this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
                var initialData = await _parameterDataCacheManager.GetAndCacheParameters(onError: ex => this.UserDialogs.Alert(ex.ToString()));

                if (initialData != null && initialData.Item1 != null && initialData.Item2 != null)
                {
                    var initCommunication = initialData.Item1;
                    var initParameters = initialData.Item2;
                    var initLogs = initialData.Item3;

                    if (initCommunication != null)
                    {
                        INavigationParameters parameters = new NavigationParameters();

                        if (initCommunication.ProfileId == ExternalComProfileValue.Dismessa)
                        {
                            var rootModel = new CockpitControlUnitRootModel();
                            var rootModelResult = _cockpitControlUnitManager.MapParamtersToModel(rootModel, initParameters);
                            var logRootModel = new ControlUnitLogRootModel();
                            logRootModel = _logControlUnitManager.MapParametersToWizardModel(logRootModel, initLogs);

                            rootModel = rootModelResult.Item1;
                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            rootModel.SelectedDeviceUuid = connectedDevice.Uuid;

                            parameters.Add(NavigationConstants.CockpitControlUnitRootModel, rootModel);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);
                            parameters.Add(NavigationConstants.ParameterCharacteristics, rootModelResult.Item2);
                            parameters.Add(NavigationConstants.ControlUnitLogRootModel, logRootModel);
                            parameters.Add(NavigationConstants.Logs, initLogs);

                            await OnNavigation(new NavigationItemModel()
                            {
                                NavigationPath = $"{ViewNames.NavigationPage}/{ViewNames.CockpitControlUnitRootPage}",
                                Parameters = parameters,
                                IsModalNavigation = false
                            });

                            this.UserDialogs.HideLoading();
                            return;
                        }
                        else if (initCommunication.ProfileId == ExternalComProfileValue.ModulA)
                        {
                            var rootModel = new CockpitPumpsModulARootModel();
                            var rootModelResult = _cockpitPumpsModulAManager.MapParametersToModel(rootModel, initParameters);
                            rootModel = rootModelResult.Item1;
                            var logRootModel = new PumpLogRootModel();
                            logRootModel = _logPumpManager.MapParametersToWizardModel(logRootModel, initLogs);
                            logRootModel.ProfileId = initCommunication.ProfileId;

                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            rootModel.SelectedDeviceUuid = connectedDevice.Uuid;

                            parameters.Add(NavigationConstants.CockpitPumpsModulARootModel, rootModel);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);
                            parameters.Add(NavigationConstants.ParameterCharacteristics, rootModelResult.Item2);
                            parameters.Add(NavigationConstants.PumpLogRootModel, logRootModel);
                            parameters.Add(NavigationConstants.Logs, initLogs);

                            await OnNavigation(new NavigationItemModel()
                            {
                                NavigationPath = $"{ViewNames.NavigationPage}/{ViewNames.CockpitPumpsModulARootPage}",
                                Parameters = parameters,
                                IsModalNavigation = false
                            });

                            this.UserDialogs.HideLoading();
                            return;
                        }
                        else if (initCommunication.ProfileId == ExternalComProfileValue.VariA)
                        {
                            var rootModel = new CockpitPumpsVariAERootModel();
                            var rootModelResult = _cockpitPumpsVariAEManager.MapParametersToModel(rootModel, initParameters);
                            rootModel = rootModelResult.Item1;
                            var logRootModel = new PumpLogRootModel();
                            logRootModel = _logPumpManager.MapParametersToWizardModel(logRootModel, initLogs);
                            logRootModel.ProfileId = initCommunication.ProfileId;

                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            rootModel.SelectedDeviceUuid = connectedDevice.Uuid;

                            parameters.Add(NavigationConstants.CockpitPumpsVariAERootModel, rootModel);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);
                            parameters.Add(NavigationConstants.ParameterCharacteristics, rootModelResult.Item2);
                            parameters.Add(NavigationConstants.PumpLogRootModel, logRootModel);
                            parameters.Add(NavigationConstants.Logs, initLogs);

                            await OnNavigation(new NavigationItemModel()
                            {
                                NavigationPath = $"{ViewNames.NavigationPage}/{ViewNames.CockpitPumpsVariAERootPage}",
                                Parameters = parameters,
                                IsModalNavigation = false
                            });

                            this.UserDialogs.HideLoading();
                            return;
                        }
                    }
                }

                this.UserDialogs.HideLoading();

                if (initialData == null || initialData.Item4 == null)
                {
                    INavigationParameters pageNameParam = new NavigationParameters();
                    pageNameParam.Add(NavigationConstants.PageNameParameter, AppConstants.Cockpit);

                    await OnNavigation(new NavigationItemModel()
                    {
                        NavigationPath = $"{ViewNames.ConnectToGatewayPage}",
                        Parameters = pageNameParam,
                        IsModalNavigation = true
                    });
                }
                else if (initialData.Item1 == null || initialData.Item2 == null || initialData.Item3 == null)
                {
                    await this.UserDialogs.AlertAsync(AppResources.Alert_Message_CantGetParameters, okText: AppResources.Alert_Message_BLE_Permission_Ok);
                }
            }
            catch (Exception ex)
            {
                var properties = new Dictionary<string, string> {
                    { "exception", ex.Message},
                    { "viewModel", nameof(EmmeMasterDetailPageViewModel) },
                    { "method", "CockpitNavigation"},
                };

                this.AppCenterService.TrackError(new Exception(ex.Message), properties);
            }
            finally
            {
                this.UserDialogs.HideLoading();
            }
        }

        private async Task LogNavigationAsync()
        {
            try
            {
                this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
                var initialData = await _parameterDataCacheManager.GetAndCacheParameters(onError: ex => this.UserDialogs.Alert(ex.ToString()));

                if (initialData != null && initialData.Item1 != null && initialData.Item2 != null)
                {
                    var initCommunication = initialData.Item1;
                    var initParameters = initialData.Item2;
                    var initLogs = initialData.Item3;

                    if (initCommunication != null)
                    {
                        INavigationParameters parameters = new NavigationParameters();
                        if (initCommunication.ProfileId == ExternalComProfileValue.Dismessa)
                        {
                            var rootModel = new CockpitControlUnitRootModel();
                            var rootModelResult = _cockpitControlUnitManager.MapParamtersToModel(rootModel, initParameters);
                            var logRootModel = new ControlUnitLogRootModel();
                            logRootModel = _logControlUnitManager.MapParametersToWizardModel(logRootModel, initLogs);

                            rootModel = rootModelResult.Item1;
                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            rootModel.SelectedDeviceUuid = connectedDevice.Uuid;

                            parameters.Add(NavigationConstants.CockpitControlUnitRootModel, rootModel);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);
                            parameters.Add(NavigationConstants.ParameterCharacteristics, rootModelResult.Item2);
                            parameters.Add(NavigationConstants.ControlUnitLogRootModel, logRootModel);
                            parameters.Add(NavigationConstants.Logs, initLogs);

                            await OnNavigation(new NavigationItemModel()
                            {
                                NavigationPath = $"{ViewNames.NavigationPage}/{ViewNames.ControlUnitLogRootPage}",
                                Parameters = parameters,
                                IsModalNavigation = false
                            });

                            this.UserDialogs.HideLoading();
                            return;
                        }
                        else if (initCommunication.ProfileId == ExternalComProfileValue.ModulA || initCommunication.ProfileId == ExternalComProfileValue.VariA)
                        {
                            var logRootModel = new PumpLogRootModel();
                            logRootModel = _logPumpManager.MapParametersToWizardModel(logRootModel, initLogs);
                            logRootModel.ProfileId = initCommunication.ProfileId;

                            var connectedDevice = await _bleManager.GetConnectedDevice();
                            logRootModel.SelectedDeviceUuid = connectedDevice.Uuid;

                            parameters.Add(NavigationConstants.PumpLogRootModel, logRootModel);
                            parameters.Add(NavigationConstants.Logs, initLogs);
                            parameters.Add(NavigationConstants.InitialParameters, initParameters);

                            if (logRootModel.ProfileId == ExternalComProfileValue.ModulA)
                            {
                                var rootModel = new CockpitPumpsModulARootModel();
                                var rootModelResult = _cockpitPumpsModulAManager.MapParametersToModel(rootModel, initParameters);
                                rootModel = rootModelResult.Item1;

                                parameters.Add(NavigationConstants.CockpitPumpsModulARootModel, rootModel);
                                parameters.Add(NavigationConstants.ParameterCharacteristics, rootModelResult.Item2);

                            }
                            else if (logRootModel.ProfileId == ExternalComProfileValue.VariA)
                            {
                                var rootModel = new CockpitPumpsVariAERootModel();
                                var rootModelResult = _cockpitPumpsVariAEManager.MapParametersToModel(rootModel, initParameters);
                                rootModel = rootModelResult.Item1;

                                parameters.Add(NavigationConstants.CockpitPumpsVariAERootModel, rootModel);
                                parameters.Add(NavigationConstants.ParameterCharacteristics, rootModelResult.Item2);
                            }

                            await OnNavigation(new NavigationItemModel()
                            {
                                NavigationPath = $"{ViewNames.NavigationPage}/{ViewNames.PumpLogRootPage}",
                                Parameters = parameters,
                                IsModalNavigation = false
                            });

                            this.UserDialogs.HideLoading();
                            return;
                        }
                    }
                }

                this.UserDialogs.HideLoading();

                if (initialData == null || initialData.Item4 == null)
                {
                    INavigationParameters pageNameParam = new NavigationParameters();
                    pageNameParam.Add(NavigationConstants.PageNameParameter, AppConstants.Log);

                    await OnNavigation(new NavigationItemModel()
                    {
                        NavigationPath = $"{ViewNames.ConnectToGatewayPage}",
                        Parameters = pageNameParam,
                        IsModalNavigation = true
                    });
                }
                else if (initialData.Item1 == null || initialData.Item2 == null || initialData.Item3 == null)
                {
                    await this.UserDialogs.AlertAsync(AppResources.Alert_Message_CantGetParameters, okText: AppResources.Alert_Message_BLE_Permission_Ok);
                }
            }
            catch (Exception ex)
            {
                var properties = new Dictionary<string, string> {
                    { "exception", ex.Message},
                    { "viewModel", nameof(EmmeMasterDetailPageViewModel) },
                    { "method", "LogNavigation"},
                };

                this.AppCenterService.TrackError(new Exception(ex.Message), properties);
            }
            finally
            {
                this.UserDialogs.HideLoading();
            }
        }

        public override void Initialize(INavigationParameters parameters)
        {
            base.Initialize(parameters);

            PopulateMenuItems();
            SubscribeConnectionStatus();
        }

        public override void Destroy()
        {
            base.Destroy();
            this.EventAggregator.GetEvent<MasterNavigationEvent>().Unsubscribe(_masterNavigationEventSubToken);
            this.EventAggregator.GetEvent<HamburgerMenuItemExecuteEvent>().Unsubscribe(_hamburgerMenuItemExecuteEventSubToken);
            this.EventAggregator.GetEvent<GatewayParametersEvent>().Unsubscribe(_gatewayParametersEventSubToken);
            _scanDisposable?.Dispose();
            _scanDisposable = null;
        }
    }
}
