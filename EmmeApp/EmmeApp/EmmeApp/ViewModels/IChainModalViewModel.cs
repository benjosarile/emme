﻿using System.Threading.Tasks;

namespace EmmeApp.ViewModels
{
    public interface IChainModalViewModel
   {
      Task CloseAsync();
   }
}
