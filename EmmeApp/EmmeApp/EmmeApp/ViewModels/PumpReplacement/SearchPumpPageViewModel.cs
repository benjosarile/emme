﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Exceptions;
using EmmeApp.Common.Utilities;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Resources.HelpViews;
using EmmeApp.Services;
using Plugin.Media.Abstractions;
using Plugin.Permissions.Abstractions;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EmmeApp.ViewModels
{
    public class SearchPumpPageViewModel : ViewModelBase
    {
        private readonly IMasterPumpReplacementManager _masterPumpReplacementManager;
        private readonly IKeyboardHelper _keyboardHelper;
        private readonly IAsyncDelayer _asyncDelayer;
        private readonly IMedia _mediaUtils;
        private readonly IPermissionsService _permissionsService;
        private readonly IOcrManager _ocrManager;
        private readonly IPopupNavigation _popupNavigation;
        private readonly IPumpReplacementManager _pumpReplacementManager;
        private readonly CompositeDisposable _subscriptions;

        public SearchPumpPageViewModel(INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IKeyboardHelper keyboardHelper,
            IMasterPumpReplacementManager masterPumpReplacementManager,
            IMedia mediaUtils,
            IPermissionsService permissionsService,
            IPumpReplacementManager pumpReplacementManager,
            IOcrManager ocrManager,
            IPopupNavigation popupNavigation,
            IAsyncDelayer asyncDelayer)
            : base(navigationService, navigationHelperService, appCenterService, userDialogs, eventAggregator)
        {
            _masterPumpReplacementManager = masterPumpReplacementManager;
            _keyboardHelper = keyboardHelper;
            _asyncDelayer = asyncDelayer;
            _mediaUtils = mediaUtils;
            _permissionsService = permissionsService;
            _ocrManager = ocrManager;
            _popupNavigation = popupNavigation;
            _pumpReplacementManager = pumpReplacementManager;

            this.SelectPumpCommand = new DelegateCommand<MasterPumpReplacementEntity>(async (pump) => await OnSelectPump(pump));
            this.ScanCommand = new DelegateCommand(async () => await OnScan());
            this.BackCommand = new DelegateCommand(async () => await OnBack());

            this.MatchList = new List<MasterPumpReplacementEntity>();
            this.Pumps = new List<MasterPumpReplacementEntity>();

            _subscriptions = new CompositeDisposable();
            var searchEventPattern = Observable.FromEventPattern<PropertyChangedEventArgs>(this, nameof(PropertyChanged))
                .Where(x => x.EventArgs.PropertyName == nameof(this.SearchKey))
                .Throttle(TimeSpan.FromMilliseconds(250))
                .Select(_ => Observable.FromAsync(async cts =>
                {
                    await _asyncDelayer.Delay(50);
                    return ProcessMatchingValue(cts);
                }))
                .Switch()
                .Subscribe(foundList => this.MatchList = foundList);

            _subscriptions.Add(searchEventPattern);
        }

        private Coresearch _coresearch;
        public CancellationTokenSource CancellationTokenSource;
        public bool IsOcrRunning;
        private DialogLoading _dialogLoading = null;

        private List<MasterPumpReplacementEntity> _pumps;
        public List<MasterPumpReplacementEntity> Pumps
        {
            get => _pumps;
            set => SetProperty(ref _pumps, value);
        }

        private List<MasterPumpReplacementEntity> _matchList;
        public List<MasterPumpReplacementEntity> MatchList
        {
            get => _matchList;
            set => SetProperty(ref _matchList, value);
        }

        private string _searchKey;
        public string SearchKey
        {
            get => _searchKey;
            set => SetProperty(ref _searchKey, value);
        }

        private bool _isSearching;
        public bool IsSearching
        {
            get => _isSearching;
            set => SetProperty(ref _isSearching, value);
        }

        private bool _isSearchFocus;
        public bool IsSearchFocus
        {
            get => _isSearchFocus;
            set => SetProperty(ref _isSearchFocus, value);
        }

        private bool _isOcrEnabled;
        public bool IsOcrEnabled
        {
            get => _isOcrEnabled;
            set => SetProperty(ref _isOcrEnabled, value);
        }

        public DelegateCommand BackCommand { get; private set; }
        public DelegateCommand<MasterPumpReplacementEntity> SelectPumpCommand { get; private set; }
        public DelegateCommand ScanCommand { get; private set; }

        private async Task OnBack()
        {
            _keyboardHelper.HideKeyboard();
            await NavigationUtilities.GoBackAsync(this.NavigationService, null, null, false);
        }

        private async Task OnSelectPump(MasterPumpReplacementEntity pump)
        {
            _keyboardHelper.HideKeyboard();

            this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);
            var oldReplacementPump = _pumpReplacementManager.GetOldPumpDetails(pump.Type);

            if (oldReplacementPump != null)
            {
                pump.Diameter = oldReplacementPump.Diameter;
                pump.Pressure = oldReplacementPump.Pressure;
                pump.Length = oldReplacementPump.Length;
            }

            await _asyncDelayer.Delay(1000);
            this.UserDialogs.HideLoading();

            INavigationParameters parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.SelectedReplacementPump, pump);

            await NavigationUtilities.NavigateAsync(this.NavigationService, ViewNames.PumpFittingProductsPage, parameters, null, false);
        }

        private async Task OnScan()
        {
            if (!_ocrManager.CanCaptureImage)
            {
                var dialogAlert = DisplaySingleAlert(AppResources.Alert_Message_DataConnectionLost, AppResources.Document_Search_NoInternetConnection, AppResources.Alert_Message_OK, null);
                await _popupNavigation.PushAsync(dialogAlert, false);
                return;
            }

            var isStoragePermissionGranted = await _permissionsService.RequestDevicePermission(Permission.Storage);

            if (!isStoragePermissionGranted)
            {
                var dialogAlert = DisplaySingleAlert(AppResources.Permissions_Required, AppResources.Permissions_StorageNotGranted, AppResources.Alert_Message_OK, null);
                await _popupNavigation.PushAsync(dialogAlert, false);
                return;
            }

            var isCameraPermissionGranted = await _permissionsService.RequestDevicePermission(Permission.Camera);

            if (!isCameraPermissionGranted)
            {
                var dialogAlert = DisplaySingleAlert(AppResources.Permissions_Required, AppResources.Permissions_CameraNotGranted, AppResources.Alert_Message_OK, null);
                await _popupNavigation.PushAsync(dialogAlert, false);
                return;
            }

            if (!_ocrManager.DidOcrWasUsed())
            {
                var dialogAlert = DisplaySingleAlert(AppResources.Alert_Message_Hint, AppResources.Alert_Message_OcrFirstUse, AppResources.Alert_Message_OK,
                    buttonCommand: new DelegateCommand(async () =>
                    {
                        _ocrManager.ConfirmOcrUsed();
                        await CapturePhotoAndRunOcr();
                    }));
                await _popupNavigation.PushAsync(dialogAlert, false);
            }
            else
            {
                await CapturePhotoAndRunOcr();
            }

        }

        private async Task CapturePhotoAndRunOcr()
        {
            var mediaFile = await _mediaUtils.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                DefaultCamera = CameraDevice.Rear,
                Directory = "Temp",
                Name = $"scanOcr.jpg",
                RotateImage = false,
                PhotoSize = PhotoSize.Medium
            });

            if (mediaFile != null)
            {
                try
                {
                    using (CancellationTokenSource = new CancellationTokenSource())
                    {
                        await RunOcr(mediaFile, CancellationTokenSource.Token);
                    }
                }
                catch (OcrProcessCancelledException)
                {
                }
                finally
                {
                    await RemoveDialogLoader();
                    IsOcrRunning = false;
                }
            }
        }

        private async Task RemoveDialogLoader()
        {
            if (_dialogLoading != null)
            {
                await _popupNavigation.RemovePageAsync(_dialogLoading);
                _dialogLoading = null;
            }
        }

        private async Task RunOcr(MediaFile mediaFile, CancellationToken cancellationToken)
        {
            IsOcrRunning = true;

            UserDialogs.ShowLoading(AppResources.Loading_ProcessingTheImage);
            await Task.Delay(700);
            string directory = Path.GetDirectoryName(mediaFile.Path);
            ProduceRotateImage(directory, mediaFile.Path);
            UserDialogs.HideLoading();

            _dialogLoading = new DialogLoading();
            _dialogLoading.ButtonText = AppResources.Alert_Message_Cancel;
            _dialogLoading.ContentText = AppResources.Loading_AnalyzingImage;
            _dialogLoading.ButtonCommand = new DelegateCommand(() =>
            {
                try
                {
                    CancellationTokenSource.Cancel();
                    _dialogLoading = null;
                }
                catch { }
            });

            await _popupNavigation.PushAsync(_dialogLoading, false);

            try
            {
                var words = new List<string>();

                var origImageWords = await ReadTextOnImage(mediaFile.Path, cancellationToken);

                if (cancellationToken.IsCancellationRequested)
                {
                    throw new OcrProcessCancelledException();
                }

                var rotate90ImageWords = await ReadTextOnImage(directory + "/scanOcrRotate90.jpg", cancellationToken);

                if (cancellationToken.IsCancellationRequested)
                {
                    throw new OcrProcessCancelledException();
                }

                words.AddRange(origImageWords);
                words.AddRange(rotate90ImageWords);

                words = CleanUpWords(words);

                var searchTerm = FindSearchTerm(words);

                await RemoveDialogLoader();

                if (cancellationToken.IsCancellationRequested)
                {
                    throw new OcrProcessCancelledException();
                }

                if (!string.IsNullOrEmpty(searchTerm) && searchTerm.Length > 3)
                {
                    SearchKey = searchTerm;
                }
                else
                {
                    await ShowNoTextDetectedPopup();
                }
            }
            catch (NoInternetConnectivityException)
            {
                await RemoveDialogLoader();
                var dialogAlert = DisplaySingleAlert(AppResources.Permissions_Required, AppResources.Document_Search_NoInternetConnection, AppResources.Alert_Message_OK, null);
                await _popupNavigation.PushAsync(dialogAlert, false);
            }
        }

        public async Task ShowNoTextDetectedPopup()
        {
            var dialogAlertContent = new DialogAlertContent();
            dialogAlertContent.LeftButtonText = AppResources.Alert_Message_Contact;
            dialogAlertContent.RightButtonText = AppResources.Alert_Message_OK;
            dialogAlertContent.Title = AppResources.SearchPumpPage_Scan_NoTextDetectedTitle;
            dialogAlertContent.LeftButtonCommand = new DelegateCommand(async () =>
            {
                var navParameters = new NavigationParameters();
                navParameters.Add(NavigationConstants.ShowNoTextPumpReplacement, "");
                await NavigationUtilities.NavigateAsync(this.NavigationService, ViewNames.ContactPage, navParameters, null, false);
            });

            var view = new NoPumpReplacementHelpView();
            dialogAlertContent.View = view;

            await _popupNavigation.PushAsync(dialogAlertContent, false);
        }

        private string FindSearchTerm(List<string> words)
        {
            var results = new List<string>();

            var analyticsProperties = new Dictionary<string, string>();

            foreach (var word in words)
            {
                analyticsProperties.Add(word, word);
                results.AddRange(_coresearch.Get(word));
            }

            if (words.Any())
            {
                AppCenterService.TrackEvent("OCR", analyticsProperties);
            }

            var filteredResults = results
                .Select(x => new Tuple<string, List<string>>(x, words.Where(y => x.ToLower().Contains(y.ToLower())).ToList()))
                .OrderByDescending(x => x.Item2.Select(y => y.Length).Sum())
                .ToList();

            string resultStr = string.Empty;

            if (filteredResults.Any())
            {
                var filteredResult = filteredResults.FirstOrDefault();

                var originalPhrase = filteredResult.Item1.Split(' ').ToList();
                var wordPositioning = new List<Tuple<string, int>>();
                foreach (var word in filteredResult.Item2)
                {
                    var indexOf = originalPhrase.FindIndex(x => x.ToLower().StartsWith(word.ToLower()));
                    if (indexOf > -1)
                    {
                        var existWordPositioningIdx = wordPositioning.FindIndex(y => y.Item2 == indexOf);

                        if (existWordPositioningIdx > -1)
                        {
                            if (wordPositioning[existWordPositioningIdx].Item1.Length < word.Length)
                            {
                                wordPositioning[existWordPositioningIdx] = new Tuple<string, int>(originalPhrase[indexOf], indexOf);
                            }
                        }
                        else
                        {
                            wordPositioning.Add(new Tuple<string, int>(originalPhrase[indexOf], indexOf));
                        }
                    }
                }

                wordPositioning = wordPositioning.OrderBy(x => x.Item2).ToList();

                if (wordPositioning.Any())
                {
                    var finalWordPositioning = new List<Tuple<string, int>>();
                    int lastValue = wordPositioning.First().Item2;
                    finalWordPositioning.Add(wordPositioning[0]);

                    for (int i = 1; i < wordPositioning.Count; i++)
                    {
                        if (wordPositioning[i].Item2 - 1 != lastValue)
                        {
                            break;
                        }

                        lastValue = wordPositioning[i].Item2;
                        finalWordPositioning.Add(wordPositioning[i]);
                    }

                }
                resultStr = string.Join(" ", wordPositioning.Select(x => x.Item1));
            }

            resultStr = resultStr.Trim();

            return resultStr;
        }

        private List<string> CleanUpWords(List<string> words)
        {
            for (int i = 0; i < words.Count; i++)
            {
                var word = words[i];
                word = word.Replace("(", "");
                word = word.Replace(")", "");
                word = word.Replace("_", "");
                word = word.Replace("\\", "");
                word = word.Replace("@", "");
                word = word.Replace("$", "");
                word = word.Replace("=", "");
                word = word.Replace(" ", "_");

                word = word.Trim();
                words[i] = word;
            }

            words = words.Where(x => !string.IsNullOrEmpty(x)).Distinct().ToList();
            return words;
        }

        private async Task<List<string>> ReadTextOnImage(string filePath, CancellationToken cancellationToken)
        {
            var byteData = File.ReadAllBytes(filePath);
            var entityObject = await _ocrManager.ReadTextOnImage(byteData, cancellationToken);

            entityObject = entityObject ?? new OcrObjectRootEntity();

            var words = new List<string>();
            foreach (var region in entityObject.Regions)
            {
                foreach (var line in region.Lines)
                {
                    words.AddRange(line.Words.Select(x => x.Text));
                }
            }

            return words;
        }

        void ProduceRotateImage(string directory, string filePath)
        {
            using (Image<Rgba32> image = Image.Load<Rgba32>(filePath))
            {
                string rotate90FilePath = directory + "/scanOcrRotate90.jpg";

                if (File.Exists(rotate90FilePath))
                {
                    File.Delete(rotate90FilePath);
                }

                image.Mutate(x => x.Rotate(RotateMode.Rotate90));
                image.Save(rotate90FilePath);
            }
        }


        private DialogAlertSingle DisplaySingleAlert(string title, string content, string buttonText, DelegateCommand buttonCommand)
        {
            var dialogAlert = new DialogAlertSingle();
            dialogAlert.Title = title;
            dialogAlert.ContentText = content;
            dialogAlert.ButtonText = buttonText;
            dialogAlert.ButtonCommand = buttonCommand;
            return dialogAlert;
        }
        private void SeedData()
        {
            var masterListPumps = _masterPumpReplacementManager.GetList();
            var oldListPumps = _pumpReplacementManager.GetOldPumps();

            foreach (var oldPump in oldListPumps)
            {
                var isAny = masterListPumps.Any(x => x.Type == oldPump.Type);
                if (!isAny)
                {
                    masterListPumps.Add(new MasterPumpReplacementEntity()
                    {
                        Type = oldPump.Type,
                        Diameter = oldPump.Diameter,
                        Length = oldPump.Length,
                        Pressure = oldPump.Pressure,
                        Engine = oldPump.Engine,
                    });
                }
            }

            this.Pumps = masterListPumps.OrderBy(x => x.Type).ToList();
            _coresearch = new Coresearch(normalize: false);

            foreach (var pump in Pumps)
            {
                var pumpType = pump.Type;
                pumpType = string.Join(" ", pumpType.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
                _coresearch.InsertResource(pumpType, pumpType);
            }

        }

        private List<MasterPumpReplacementEntity> ProcessMatchingValue(CancellationToken cts)
        {
            this.IsSearching = !string.IsNullOrEmpty(this.SearchKey);
            var foundList = GetMatchingValues(this.SearchKey);
            if (cts.IsCancellationRequested)
            {
                return new List<MasterPumpReplacementEntity>();
            }
            return foundList;
        }

        public List<MasterPumpReplacementEntity> GetMatchingValues(string searchKey)
        {
            string key = ValidatePumpType(searchKey);

            return this.Pumps.GroupBy(gr => gr.Type)
                                     .Select(i => i.First())
                                     .Where(x => ValidatePumpType(x.Type)
                                                    .Contains(key))
                                     .OrderBy(f => ValidatePumpType(f.Type).IndexOf(key))
                                                    .ToList();

        }

        private string ValidatePumpType(string term)
        {
            return string.Join(" ", term.Split(new char[] { ' ', '-', '/', '\\' }, StringSplitOptions.RemoveEmptyEntries)).ToLower();
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            IsOcrEnabled = false;

            var navigationMode = this.NavigationHelperService.GetNavigationMode(parameters);
            if (navigationMode == NavigationMode.New)
            {
                this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);

                await _asyncDelayer.Delay(100);
                SeedData();
                this.UserDialogs.HideLoading();
                IsSearchFocus = true;
            }

            if (parameters.ContainsKey(NavigationConstants.ShowNoTextPumpReplacement))
            {
                await ShowNoTextDetectedPopup();
            }
        }

        public override void Destroy()
        {
            base.Destroy();
            _dialogLoading = null;
            _subscriptions.Dispose();
        }


    }
}
