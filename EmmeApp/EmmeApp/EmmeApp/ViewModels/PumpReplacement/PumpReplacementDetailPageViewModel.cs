﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Entities;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Plugin.Messaging;
using Prism.Commands;
using Prism.Navigation;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace EmmeApp.ViewModels
{
    public class PumpReplacementDetailPageViewModel : ViewModelBase
    {
        private readonly IMessaging _messagingPlugin;
        private readonly IFileService _fileService;
        private readonly IBiralContactManager _biralContactManager;
        private readonly ISortimentManager _sortimentManager;
        private readonly IMarketManager _marketManager;

        public PumpReplacementDetailPageViewModel(INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterService,
            IUserDialogs userDialogs,
            IMessaging messagingPlugin,
            IFileService fileService,
            IMarketManager marketManager,
            IBiralContactManager biralContactManager,
            IPumpReplacementManager pumpReplacementManager,
            ISortimentManager sortimentManager) : base(navigationService, navigationHelperService, appCenterService, userDialogs)
        {
            _messagingPlugin = messagingPlugin;
            _fileService = fileService;
            _biralContactManager = biralContactManager;
            _sortimentManager = sortimentManager;
            _marketManager = marketManager;

            this.BackCommand = new DelegateCommand(async () => await OnBack());
            this.ShareCommand = new DelegateCommand<string>(async (to) => await OnShare(to));
            this.CallCommand = new DelegateCommand(async () => await OnCall(), () => this.CanNavigateToWeb).ObservesProperty(() => this.CanNavigateToWeb);
            this.NavigateToWebCommand = new DelegateCommand<string>(async (param) => await OnNavigateToWebCall(param), (param) => this.CanNavigateToWeb)
                .ObservesProperty(() => this.CanNavigateToWeb);
        }

        private bool _canNavigateToWeb = true;
        public bool CanNavigateToWeb
        {
            get => _canNavigateToWeb;
            set => SetProperty(ref _canNavigateToWeb, value);
        }

        private BiralProductEntity _biralProduct;
        public BiralProductEntity BiralProduct
        {
            get => _biralProduct;
            set => SetProperty(ref _biralProduct, value);
        }

        private string _remarksText = null;
        public string RemarksText
        {
            get => _remarksText;
            set => SetProperty(ref _remarksText, value);
        }

        private string _adapterText = null;
        public string AdapterText
        {
            get => _adapterText;
            set => SetProperty(ref _adapterText, value);
        }

        private MasterPumpReplacementEntity _selectedReplacementPump;
        public MasterPumpReplacementEntity SelectedReplacementPump
        {
            get => _selectedReplacementPump;
            set => SetProperty(ref _selectedReplacementPump, value);
        }

        private BiralContactEntity _biralContact;
        public BiralContactEntity BiralContact
        {
            get => _biralContact;
            set => SetProperty(ref _biralContact, value);
        }

        public DelegateCommand BackCommand { get; private set; }
        public DelegateCommand<string> ShareCommand { get; private set; }
        public DelegateCommand CallCommand { get; private set; }
        public DelegateCommand<string> NavigateToWebCommand { get; private set; }

        private async Task OnBack()
        {
            await NavigationUtilities.GoBackAsync(this.NavigationService, null, null, false);
        }

        private async Task OnShare(string to)
        {
            string productType = this.BiralProduct.Type;
            productType = Regex.Replace(productType, @"[^0-9a-zA-Z]+", "");
            string filename = $"{productType}.txt";

            string filePath = _fileService.GetFilePath(filename);
            string newLine = "\r\n";

            if (Device.RuntimePlatform == Device.iOS)
            {
                newLine = "\n";
            }

            StringBuilder sb = new StringBuilder();
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Details}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Type}: {this.BiralProduct.Type}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Item_Number}: {this.BiralProduct.ProductId}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Nominal_Diameter}: {this.BiralProduct.Diameter}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Nominal_Pressure}: {this.BiralProduct.Pressure}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Installed_Length}: {this.BiralProduct.Length}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Motor}: {this.BiralProduct.Engine}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Remarks}: {this.BiralProduct.Remarks}{newLine}");
            sb.Append(newLine);

            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Adapter}{newLine}{ComposeAdapterText()}{newLine}");
            sb.Append(newLine);

            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_OldPump}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Type}: {this.SelectedReplacementPump.Type}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Nominal_Diameter}: {this.SelectedReplacementPump.Diameter}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Nominal_Pressure}: {this.SelectedReplacementPump.Pressure}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Installed_Length}: {this.SelectedReplacementPump.Length}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Motor}: {this.SelectedReplacementPump.Engine}{newLine}");
            sb.Append(newLine);

            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_MoreInformation}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Datasheet}: {this.BiralProduct.DataSheetLink}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Operating_Instructions}: {this.BiralProduct.OperatingInstructionsLink}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Other_Information}: {this.BiralProduct.OtherInformationLink}{newLine}");
            sb.Append(newLine);

            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Contact_Information}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Address}: {this.BiralContact.Address1} {this.BiralContact.Address2}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Contact_Number}: {this.BiralContact.ContactNumber}{newLine}");
            sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Email_Address}: {this.BiralContact.EmailAddress}{newLine}");

            _fileService.WriteToAFile(filePath, sb.ToString(), false);

            if (_messagingPlugin.EmailMessenger.CanSendEmail)
            {
                this.UserDialogs.ShowLoading(AppResources.Loading_Message_EmailRedirect);

                var emailBuilder = new EmailMessageBuilder()
                    .Subject(this.BiralProduct.Type)
                    .Body(sb.ToString())
                    .WithAttachment(filePath, "text/plain");


                if (!string.IsNullOrEmpty(to))
                {
                    emailBuilder.To(to);
                }

                var emailMessage = emailBuilder.Build();

                _messagingPlugin.EmailMessenger.SendEmail(emailMessage);

                this.UserDialogs.HideLoading();
            }
            else
            {
                await this.UserDialogs.AlertAsync(AppResources.Alert_Message_FailedToCreateMail_Message, AppResources.Alert_Message_FailedToCreateMail_Title);
            }
        }

        private async Task OnNavigateToWebCall(string param)
        {
            this.CanNavigateToWeb = false;
            this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);

            string url = string.Empty;
            switch (param)
            {
                case "datasheet": url = this.BiralProduct.DataSheetLink; break;
                case "operating-instructions": url = this.BiralProduct.OperatingInstructionsLink; break;
                case "product": url = this.BiralProduct.OtherInformationLink; break;
            }

            this.UserDialogs.HideLoading();
            if (!string.IsNullOrEmpty(url))
            {
                await Launcher.TryOpenAsync(url);
            }

            this.CanNavigateToWeb = true;
        }

        private async Task OnCall()
        {
            this.CanNavigateToWeb = false;
            this.UserDialogs.ShowLoading(AppResources.Alert_Message_Loading);

            this.UserDialogs.HideLoading();
            await Launcher.TryOpenAsync(string.Format("tel:{0}", BiralContact.ContactNumber));
            this.CanNavigateToWeb = true;
        }

        private void SetBiralProductProperties()
        {
            RemarksText = !string.IsNullOrEmpty(this.BiralProduct.Remarks.ToString()) ? this.BiralProduct.Remarks.ToString() : AppResources.Pump_Replacement_Detail_Page_No_Remarks_Here;

            AdapterText = ComposeAdapterText();
        }

        private string ComposeAdapterText()
        {
            string adapterText = null;

            var adapterInfos = BiralProduct.AdapterInfos;

            if (adapterInfos != null && adapterInfos.Any())
            {
                var adapterInfoCount = adapterInfos.Count();

                if (adapterInfoCount == 1)
                {
                    var singleAdapterInfo = adapterInfos.SingleOrDefault();
                    if (singleAdapterInfo != null)
                    {
                        if (singleAdapterInfo.ItemNumber != 0)
                        {
                            adapterText = $"{singleAdapterInfo.NumberOfPieces}x {singleAdapterInfo.Code} ({singleAdapterInfo.ItemNumber})";
                        }
                        else
                        {
                            adapterText = singleAdapterInfo.Code;
                        }
                    }

                }
                else if (adapterInfoCount == 2)
                {
                    var adapterInfo1Text = string.Empty;
                    var adapterInfo1 = adapterInfos.ElementAtOrDefault(0);
                    if (adapterInfo1 != null)
                    {
                        adapterInfo1Text = $"{adapterInfo1.NumberOfPieces}x {adapterInfo1.Code} ({adapterInfo1.ItemNumber})";
                    }

                    var adapterInfo2Text = string.Empty;
                    var adapterInfo2 = adapterInfos.ElementAtOrDefault(1);
                    if (adapterInfo2 != null)
                    {
                        adapterInfo2Text = $"{adapterInfo2.NumberOfPieces}x {adapterInfo2.Code} ({adapterInfo2.ItemNumber})";
                    }

                    adapterText = $"{adapterInfo1Text}\r\n{adapterInfo2Text}";
                }
            }

            return adapterText;
        }

        private void SetProductLink()
        {
            var marketLanguage = _marketManager.GetMarketLanguage();

            var sortimentMarket = _sortimentManager.GetSortimentMarketsFromDB(marketLanguage.Item1);

            if (sortimentMarket != null)
            {
                string marketLink = string.Empty;

                switch (marketLanguage.Item2)
                {
                    case AppConstants.LanguageCodeDE: marketLink = sortimentMarket.MarketLink.De.OriginalString; break;
                    case AppConstants.LanguageCodeIT: marketLink = sortimentMarket.MarketLink.It.OriginalString; break;
                    case AppConstants.LanguageCodeEN: marketLink = sortimentMarket.MarketLink.En.OriginalString; break;
                    case AppConstants.LanguageCodeFR: marketLink = sortimentMarket.MarketLink.Fr.OriginalString; break;
                    case AppConstants.LanguageCodeNL: marketLink = sortimentMarket.MarketLink.Nl.OriginalString; break;
                }

                if (!string.IsNullOrEmpty(marketLink))
                {
                    this.BiralProduct.DataSheetLink = marketLink.Replace("$1", this.BiralProduct.ProductId.ToString()).Replace("$2", "datasheet");
                    this.BiralProduct.OperatingInstructionsLink = marketLink.Replace("$1", this.BiralProduct.ProductId.ToString()).Replace("$2", "operating-instructions");
                    this.BiralProduct.OtherInformationLink = marketLink.Replace("$1", this.BiralProduct.ProductId.ToString()).Replace("$2", "product");
                }
            }
        }


        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.SelectedBiralProduct))
            {
                this.BiralProduct = (BiralProductEntity)parameters[NavigationConstants.SelectedBiralProduct];
                SetBiralProductProperties();
            }

            if (parameters.ContainsKey(NavigationConstants.SelectedReplacementPump))
            {
                SelectedReplacementPump = (MasterPumpReplacementEntity)parameters[NavigationConstants.SelectedReplacementPump];
            }

            this.BiralContact = _biralContactManager.GetCurrentBiralContact();
            SetProductLink();
        }

    }
}
