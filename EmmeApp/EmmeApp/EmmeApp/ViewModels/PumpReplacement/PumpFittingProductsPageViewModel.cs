﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Entities;
using EmmeApp.Managers.Abstractions;
using Prism.Commands;
using Prism.Navigation;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace EmmeApp.ViewModels
{
    public class PumpFittingProductsPageViewModel : ViewModelBase
	{
		private readonly IAsyncDelayer _asyncDelayer;
		private readonly IMarketManager _marketManager;
		private readonly IPumpReplacementProductManager _pumpReplacementProductManager;
		private readonly IBiralProductManager _biralProductManager;

		public PumpFittingProductsPageViewModel(INavigationService navigationService, 
			INavigationHelperService navigationHelperService,
			IAppCenterCustomService appCenterService, 
			IUserDialogs userDialogs,
			IMarketManager marketManager,
			IPumpReplacementProductManager pumpReplacementProductManager, 
			IBiralProductManager biralProductManager,
			IAsyncDelayer asyncDelayer)
			: base(navigationService, navigationHelperService, appCenterService, userDialogs)
		{
			_marketManager = marketManager;
			_pumpReplacementProductManager = pumpReplacementProductManager;
			_biralProductManager = biralProductManager;
			_asyncDelayer = asyncDelayer;

			this.BackCommand = new DelegateCommand(async () => await OnBack());
			this.SelectProductCommand = new DelegateCommand<BiralProductEntity>(async (product) => await OnSelectProduct(product));
		}

		private MasterPumpReplacementEntity _selectedReplacementPump;

		private string _type;
		public string Type
		{
			get => _type;
			set => SetProperty(ref _type, value);
		}

		private ObservableCollection<BiralProductEntity> _fittingProducts;
		public ObservableCollection<BiralProductEntity> FittingProducts
		{
			get => _fittingProducts;
			set => SetProperty(ref _fittingProducts, value);
		}

		private bool _hasFittingProducts;
		public bool HasFittingProducts
		{
			get => _hasFittingProducts;
			set => SetProperty(ref _hasFittingProducts, value);
		}

		private bool _willShowNoFittingProductsLabel;
		public bool WillShowNoFittingProductsLabel
		{
			get => _willShowNoFittingProductsLabel;
			set => SetProperty(ref _willShowNoFittingProductsLabel, value);
		}

		public DelegateCommand BackCommand { get; private set; }
		public DelegateCommand<BiralProductEntity> SelectProductCommand { get; private set; }

		private async Task OnSelectProduct(BiralProductEntity product)
		{
			INavigationParameters paramters = new NavigationParameters();
			paramters.Add(NavigationConstants.SelectedBiralProduct, product);
			paramters.Add(NavigationConstants.SelectedReplacementPump, _selectedReplacementPump);

			await NavigationUtilities.NavigateAsync(this.NavigationService, ViewNames.PumpReplacementDetailPage, paramters, null, false);
		}

		private async Task OnBack()
		{
			await _asyncDelayer.Delay(100);
			await NavigationUtilities.GoBackAsync(this.NavigationService, null, null, false);
		}

		public void GetFittingProducts(string type)
		{
			var pumpReplacementProducts = _pumpReplacementProductManager.GetRelatedProducts(type);

			this.FittingProducts = new ObservableCollection<BiralProductEntity>();
			foreach (var pumpReplacementProduct in pumpReplacementProducts)
			{
				var productEntity = _biralProductManager.GetBiralProductDetail(pumpReplacementProduct);

				if (productEntity != null)
				{
					this.FittingProducts.Add(productEntity);
				}
			}

			this.HasFittingProducts = this.FittingProducts.Any();
			this.WillShowNoFittingProductsLabel = !this.HasFittingProducts;
		}

		public override async void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);

			NavigationMode navigationMode = this.NavigationHelperService.GetNavigationMode(parameters);

			if (navigationMode.Equals(NavigationMode.New))
			{
				if (parameters.ContainsKey(NavigationConstants.SelectedReplacementPump))
				{
					_selectedReplacementPump = (MasterPumpReplacementEntity)parameters[NavigationConstants.SelectedReplacementPump];
					this.Type = _selectedReplacementPump.Type;
					GetFittingProducts(this.Type);
				}
				else
				{
					await NavigationUtilities.GoBackAsync(this.NavigationService, null, null, false);
				}
			}
		}


	}
}
