﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities.External;
using EmmeApp.Events;
using EmmeApp.ItemModels;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Plugin.Messaging;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using ControlUnitLogEvent = EmmeApp.Events.ControlUnitLogEvent;

namespace EmmeApp.ViewModels
{
    public class ControlUnitLogRootPageViewModel : ViewModelBase
    {
        private readonly IMessaging _messagingPlugin;
        private readonly IFileService _fileService;
        private readonly ICockpitControlUnitManager _cockpitControlUnitManager;
        private readonly ILogControlUnitManager _logControlUnitManager;

        private readonly IPopupNavigation _popupNavigation;

        private DialogAlertSingle _lostConnectionAlertDialog = null;
        private readonly SemaphoreSlim _connectionManagementThrottle = new SemaphoreSlim(1, 1);

        private readonly SubscriptionToken _configureEventSubToken;
        private readonly SubscriptionToken _connectionEstablishedSubToken;
        private readonly SubscriptionToken _connectionLostSubToken;

        public ControlUnitLogRootPageViewModel(
            INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterCustomService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IMessaging messagingPlugin,
            IFileService fileService,
            ICockpitControlUnitManager cockpitControlUnitManager,
            ILogControlUnitManager logControlUnitManager,
            IPopupNavigation popupNavigation)
            : base(navigationService, navigationHelperService, appCenterCustomService, userDialogs, eventAggregator)
        {
            _messagingPlugin = messagingPlugin;
            _fileService = fileService;
            _cockpitControlUnitManager = cockpitControlUnitManager;
            _logControlUnitManager = logControlUnitManager;
            _popupNavigation = popupNavigation;

            _configureEventSubToken = this.EventAggregator.GetEvent<ControlUnitLogEvent>().Subscribe(OnConfigure);
            _connectionEstablishedSubToken = this.EventAggregator.GetEvent<ConnectionEstablishedEvent>().Subscribe(async () => await OnConnectionEstablished());
            _connectionLostSubToken = this.EventAggregator.GetEvent<ConnectionLostEvent>().Subscribe(async () => await OnConnectionLost());

            this.ShareCommand = new DelegateCommand(async () => await OnShare(), () => this.CanExecuteCommand).ObservesProperty(() => this.CanExecuteCommand);
            this.ToggleMenuCommand = new DelegateCommand(() => OnToggleMenu(), () => this.CanExecuteCommand).ObservesProperty(() => this.CanExecuteCommand);
            this.ConfigureCommand = new DelegateCommand(() => OnConfigure(), () => this.CanExecuteCommand).ObservesProperty(() => this.CanExecuteCommand);
        }

        public ControlUnitLogEventPageViewModel ControlUnitLogEventPageViewModel { get; private set; }

        public CockpitControlUnitRootModel RootModel { get; private set; }
        public ControlUnitLogRootModel LogRootModel { get; set; }
        private List<ParameterTExternalEntity> _initialParameters;

        private bool _canExecuteCommand = true;
        public bool CanExecuteCommand
        {
            get => _canExecuteCommand;
            set => SetProperty(ref _canExecuteCommand, value);
        }

        public DelegateCommand ToggleMenuCommand { get; private set; }
        public DelegateCommand ShareCommand { get; private set; }
        public DelegateCommand ConfigureCommand { get; private set; }

        private void OnToggleMenu()
        {
            this.CanExecuteCommand = false;
            this.EventAggregator.GetEvent<HamburgerTappedEvent>().Publish();
            this.CanExecuteCommand = true;
        }

        private async Task OnShare()
        {
            this.CanExecuteCommand = false;
            if (_messagingPlugin.EmailMessenger.CanSendEmail)
            {
                this.UserDialogs.ShowLoading(AppResources.Loading_Message_EmailRedirect);

                RootModel.FactorySerialNumber = RootModel.FactorySerialNumber ?? string.Empty;
                var dateNow = DateTime.Now;
                string subject = $"{RootModel.FactorySerialNumber}_{dateNow.ToString("yyyyMMddhhmmss")}";
                string filename = $"{RootModel.FactorySerialNumber}_{dateNow.ToString("yyyyMMddhhmmss")}.txt";

                string filePath = _fileService.GetFilePath(filename);
                string newLine = "\r\n";

                if (Device.RuntimePlatform == Device.iOS)
                {
                    newLine = "\n";
                }

                var cockpitSb = _cockpitControlUnitManager.GenerateEmailContent(this.RootModel, newLine);
                var logSb = _logControlUnitManager.GenerateEmailContent(this.LogRootModel, newLine);

                cockpitSb.Append(newLine);
                cockpitSb.Append(logSb);

                _fileService.WriteToAFile(filePath, cockpitSb.ToString(), false);

                var email = new EmailMessageBuilder()
                    .Subject(subject)
                    .Body(cockpitSb.ToString())
                    .WithAttachment(filePath, "text/plain")
                    .Build();

                _messagingPlugin.EmailMessenger.SendEmail(email);

                this.UserDialogs.HideLoading();
            }
            else
            {
                await this.UserDialogs.AlertAsync(AppResources.Alert_Message_FailedToCreateMail_Message, AppResources.Alert_Message_FailedToCreateMail_Title);
            }

            this.CanExecuteCommand = true;
        }

        private void OnConfigure()
        {
            this.CanExecuteCommand = false;

            var controlUnitRootModel = new CockpitControlUnitRootModel();
            var modelResult = _cockpitControlUnitManager.MapParamtersToModel(controlUnitRootModel, _initialParameters);
            controlUnitRootModel = modelResult.Item1;
            var model = _cockpitControlUnitManager.MapToConfigurationModel(controlUnitRootModel);
            model.SelectedDeviceUuid = this.LogRootModel.SelectedDeviceUuid;

            INavigationParameters parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.InitialStartupWizardModel, model);
            parameters.Add(NavigationConstants.InitialParameters, _initialParameters);
            parameters.Add(NavigationConstants.ParameterCharacteristics, modelResult.Item2);

            string navigationPath = $"{ViewNames.InitialStartupForm1CustomerInfoPage}";
            this.EventAggregator.GetEvent<MasterNavigationEvent>().Publish(new NavigationItemModel()
            {
                NavigationPath = navigationPath,
                Parameters = parameters,
                IsModalNavigation = true
            });
            this.CanExecuteCommand = true;
        }

        public void SetControlUnitLogEventPageViewModel(ControlUnitLogEventPageViewModel vm)
        {
            this.ControlUnitLogEventPageViewModel = vm;
        }

        private async Task OnConnectionEstablished()
        {
            if (!(await _connectionManagementThrottle.WaitAsync(0)))
                return;

            await RemoveAlertForLostConnection();

            _connectionManagementThrottle.Release();
        }

        private async Task OnConnectionLost()
        {
            if (!(await _connectionManagementThrottle.WaitAsync(0)))
                return;

            await AlertUserOfLostConnection();

            _connectionManagementThrottle.Release();
        }

        private async Task AlertUserOfLostConnection()
        {
            if (_lostConnectionAlertDialog != null)
                return;

            _lostConnectionAlertDialog = new DialogAlertSingle();
            _lostConnectionAlertDialog.ButtonText = AppResources.Alert_Message_OK;
            _lostConnectionAlertDialog.ContentText = AppResources.Alert_MessageBody_BLE_LostConnection;
            _lostConnectionAlertDialog.Title = AppResources.Alert_MessageHeader_BLE_LostConnection;
            _lostConnectionAlertDialog.ButtonCommand =
                new DelegateCommand(
                    () =>
                    {
                        _lostConnectionAlertDialog = null;
                        EventAggregator.GetEvent<HamburgerMenuItemExecuteEvent>().Publish(new ItemModels.HamburgerMenuModel() { Menu = HamburgerMenuValue.Home, Parameters = null });
                    });
            await _popupNavigation.PushAsync(_lostConnectionAlertDialog, false);
        }

        private async Task RemoveAlertForLostConnection()
        {
            if (_lostConnectionAlertDialog != null)
            {
                await _popupNavigation.RemovePageAsync(_lostConnectionAlertDialog);
                _lostConnectionAlertDialog = null;
            }
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.ControlUnitLogRootModel))
            {
                var model = parameters.GetValue<ControlUnitLogRootModel>(NavigationConstants.ControlUnitLogRootModel);

                if (model != null)
                {
                    this.LogRootModel = model;
                    if (this.ControlUnitLogEventPageViewModel != null)
                    {
                        this.ControlUnitLogEventPageViewModel.SetEventLogModel(this.LogRootModel);
                    }
                }
            }

            if (parameters.ContainsKey(NavigationConstants.InitialParameters))
            {
                _initialParameters = parameters.GetValue<List<ParameterTExternalEntity>>(NavigationConstants.InitialParameters);
            }

            if (parameters.ContainsKey(NavigationConstants.CockpitControlUnitRootModel))
            {
                var model = parameters.GetValue<CockpitControlUnitRootModel>(NavigationConstants.CockpitControlUnitRootModel);
                this.RootModel = model;
            }
        }

        public override void Destroy()
        {
            base.Destroy();
            _lostConnectionAlertDialog = null;
            this.EventAggregator.GetEvent<ControlUnitLogEvent>().Unsubscribe(_configureEventSubToken);
            this.EventAggregator.GetEvent<ConnectionEstablishedEvent>().Unsubscribe(_connectionEstablishedSubToken);
            this.EventAggregator.GetEvent<ConnectionLostEvent>().Unsubscribe(_connectionLostSubToken);
        }
    }
}
