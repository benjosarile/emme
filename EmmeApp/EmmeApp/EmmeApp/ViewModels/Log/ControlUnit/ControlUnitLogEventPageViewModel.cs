﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Managers.Abstractions;
using IncrementalListView.FormsPlugin;
using Prism.Events;
using Prism.Navigation;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace EmmeApp.ViewModels
{
    public class ControlUnitLogEventPageViewModel : ViewModelBase, ISupportIncrementalLoading
    {
        private readonly IServiceEntityMapper _serviceEntityMapper;

        public ControlUnitLogEventPageViewModel(
            INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterCustomService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IServiceEntityMapper serviceEntityMapper)
            : base(navigationService, navigationHelperService, appCenterCustomService, userDialogs, eventAggregator)
        {
            _serviceEntityMapper = serviceEntityMapper;

            this.LoadMoreItemsCommand = new Command(async () => await OnLoadMoreItems());

            this.HasMoreItems = true;
        }

        public int PageSize { get; set; } = 20;

        public ICommand LoadMoreItemsCommand { get; set; }

        bool isLoadingIncrementally;
        public bool IsLoadingIncrementally
        {
            get => isLoadingIncrementally;
            set => SetProperty(ref isLoadingIncrementally, value);
        }

        bool hasMoreItems;
        public bool HasMoreItems
        {
            get => hasMoreItems;
            set => SetProperty(ref hasMoreItems, value);
        }


        private ControlUnitLogEventModel _eventLogModel;
        public ControlUnitLogEventModel EventLogModel
        {
            get => _eventLogModel;
            set => SetProperty(ref _eventLogModel, value);
        }

        private ObservableCollection<ControlUnitLogModel> _eventLogList;
        public ObservableCollection<ControlUnitLogModel> EventLogList
        {
            get => _eventLogList;
            set => SetProperty(ref _eventLogList, value);
        }

        private List<ControlUnitLogModel> _tempeventLogList = new List<ControlUnitLogModel>();
        public List<ControlUnitLogModel> TempEventLogList
        {
            get => _tempeventLogList;
            set => SetProperty(ref _tempeventLogList, value);
        }

        public void SetEventLogModel(ControlUnitLogRootModel model)
        {
            this.EventLogModel = _serviceEntityMapper.Map<ControlUnitLogEventModel>(model);
        }

        //public void IncrementalScrollView(double scrollY, double scrollSpace)
        //{
        //    Device.BeginInvokeOnMainThread(() => { 
        //    if (scrollY >= scrollSpace)
        //    {
        //        int i = this.EventLogList.Count;
        //        int itemCount = this.EventLogList.Count;
        //        for (; i < itemCount + 15; i++)
        //        {
        //            this.EventLogList.Add(this.TempEventLogList[i]);
        //        }
        //    }
        //    });
        //}

        async Task OnLoadMoreItems()
        {
            if (this.EventLogList.Count != 1500)
            {
                this.IsLoadingIncrementally = true;

                await Task.Delay(200);

                int i = this.EventLogList.Count;
                int itemCount = this.EventLogList.Count;
                int remainingCount = this.TempEventLogList.Count - this.EventLogList.Count;
                int loadCount = remainingCount < 100 ? remainingCount : 100;
                for (; i < itemCount + loadCount; i++)
                {
                    this.EventLogList.Add(this.TempEventLogList[i]);
                }


                this.IsLoadingIncrementally = false;
            }
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (parameters.ContainsKey(NavigationConstants.ControlUnitLogRootModel))
            {
                var model = parameters.GetValue<ControlUnitLogRootModel>(NavigationConstants.ControlUnitLogRootModel);
                SetEventLogModel(model);
                if (this.EventLogModel.EventControlUnitLogList.Any())
                {
                    this.TempEventLogList = this.EventLogModel.EventControlUnitLogList.OrderByDescending(x => x.AppearanceDateTime).ToList();
                    this.EventLogList = new ObservableCollection<ControlUnitLogModel>();
                    foreach (var eventLog in this.TempEventLogList.Take(100))
                    {
                        this.EventLogList.Add(eventLog);
                    }
                }
            }
        }
    }
}
