﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Resources.HelpViews;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EmmeApp.ViewModels
{
    public class PumpLogAlertPageViewModel : ViewModelBase
    {
        private readonly IServiceEntityMapper _serviceEntityMapper;
        private readonly IPopupNavigation _popupNavigation;

        public PumpLogAlertPageViewModel(
            INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterCustomService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IServiceEntityMapper serviceEntityMapper,
            IPopupNavigation popupNavigation)
            : base(navigationService, navigationHelperService, appCenterCustomService, userDialogs, eventAggregator)
        {
            _serviceEntityMapper = serviceEntityMapper;
            _popupNavigation = popupNavigation;

            this.SetPumpLogAlertEvent = (model) => SetAlarmModel(model);
        }

        private async Task ExecuteViewHelpTextCommand(string title, string cause, string measure)
        {
            var popupOverlay = new DialogInformationOverlay();
            popupOverlay.Title = title;

            var view = new LogAlarmWarningItemHelp();
            view.FindByName<Label>(UIConstants.LabelCauseMessage).Text = cause;
            view.FindByName<Label>(UIConstants.LabelMeasureMessage).Text = measure;

            popupOverlay.View = view;

            await _popupNavigation.PushAsync(popupOverlay);
        }

        public Action<PumpLogRootModel> SetPumpLogAlertEvent { get; private set; }

        private PumpLogAlertModel _alarmModel;
        public PumpLogAlertModel AlarmModel
        {
            get => _alarmModel;
            set => SetProperty(ref _alarmModel, value);
        }

        private List<PumpAlarmAndWarningLogModel> _alarmLogList = new List<PumpAlarmAndWarningLogModel>();
        public List<PumpAlarmAndWarningLogModel> AlarmLogList
        {
            get => _alarmLogList;
            set => SetProperty(ref _alarmLogList, value);
        }

        private void SetAlarmModel(PumpLogRootModel model)
        {
            this.AlarmModel = _serviceEntityMapper.Map<PumpLogAlertModel>(model);
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.PumpLogRootModel))
            {
                var model = parameters.GetValue<PumpLogRootModel>(NavigationConstants.PumpLogRootModel);
                SetAlarmModel(model);
                if (AlarmModel.AlarmPumpLogList.Any())
                {
                    var alarmLogList = this.AlarmModel.AlarmPumpLogList.Skip(Math.Max(0, this.AlarmModel.AlarmPumpLogList.Count - 5)).OrderByDescending(x => x.AppearanceDateTime).ToList();

                    this.AlarmLogList = alarmLogList;

                    foreach (var alarmLog in this.AlarmLogList)
                    {
                        if (alarmLog.HasHelpText)
                        {
                            alarmLog.ViewHelpTextCommand = new DelegateCommand(async ()
                                => await ExecuteViewHelpTextCommand(alarmLog.Title, alarmLog.Cause, alarmLog.Measure));
                        }
                    }
                }
            }
        }
    }
}
