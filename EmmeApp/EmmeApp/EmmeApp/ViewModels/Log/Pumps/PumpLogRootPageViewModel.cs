﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.Configuration;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Entities.External;
using EmmeApp.Events;
using EmmeApp.ItemModels;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Plugin.Messaging;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EmmeApp.ViewModels
{
    public class PumpLogRootPageViewModel : ViewModelBase
    {
        private readonly IMessaging _messagingPlugin;
        private readonly IFileService _fileService;
        private readonly IConfigurationPumpModulAManager _configurationPumpModulAManager;
        private readonly IConfigurationPumpVariAManager _configurationPumpVariAManager;
        private readonly ICockpitPumpsModulAManager _cockpitPumpsModulAManager;
        private readonly ICockpitPumpsVariAEManager _cockpitPumpsVariAEManager;
        private readonly ILogPumpManager _logPumpManager;

        private readonly IPopupNavigation _popupNavigation;

        private DialogAlertSingle _lostConnectionAlertDialog = null;
        private readonly SemaphoreSlim _connectionManagementThrottle = new SemaphoreSlim(1, 1);

        private readonly SubscriptionToken _configureEventSubToken;
        private readonly SubscriptionToken _connectionEstablishedSubToken;
        private readonly SubscriptionToken _connectionLostSubToken;

        public PumpLogRootPageViewModel(
            INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterCustomService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IMessaging messagingPlugin,
            IFileService fileService,
            IConfigurationPumpModulAManager configurationPumpModulAManager,
            IConfigurationPumpVariAManager configurationPumpVariAManager,
            ICockpitPumpsModulAManager cockpitPumpsModulAManager,
            ICockpitPumpsVariAEManager cockpitPumpsVariAEManager,
            ILogPumpManager logPumpManager,
            IPopupNavigation popupNavigation)
            : base(navigationService, navigationHelperService, appCenterCustomService, userDialogs, eventAggregator)
        {
            _messagingPlugin = messagingPlugin;
            _fileService = fileService;
            _configurationPumpModulAManager = configurationPumpModulAManager;
            _configurationPumpVariAManager = configurationPumpVariAManager;
            _cockpitPumpsModulAManager = cockpitPumpsModulAManager;
            _cockpitPumpsVariAEManager = cockpitPumpsVariAEManager;
            _logPumpManager = logPumpManager;
            _popupNavigation = popupNavigation;

            _configureEventSubToken = this.EventAggregator.GetEvent<PumpLogEvent>().Subscribe(OnConfigure);
            _connectionEstablishedSubToken = this.EventAggregator.GetEvent<ConnectionEstablishedEvent>().Subscribe(async () => await OnConnectionEstablished());
            _connectionLostSubToken = this.EventAggregator.GetEvent<ConnectionLostEvent>().Subscribe(async () => await OnConnectionLost());

            this.ShareCommand = new DelegateCommand(async () => await OnShare(), () => this.CanExecuteCommand).ObservesProperty(() => this.CanExecuteCommand);
            this.ToggleMenuCommand = new DelegateCommand(() => OnToggleMenu(), () => this.CanExecuteCommand).ObservesProperty(() => this.CanExecuteCommand);
            this.ConfigureCommand = new DelegateCommand(() => OnConfigure(), () => this.CanExecuteCommand).ObservesProperty(() => this.CanExecuteCommand);
        }

        public CockpitPumpsModulARootModel RootModelModulA { get; set; }
        public CockpitPumpsVariAERootModel RootModelVariAE { get; set; }

        public PumpLogAlertPageViewModel PumpLogAlertPageViewModel { get; private set; }
        public PumpLogWarningPageViewModel PumpLogWarningPageViewModel { get; private set; }
        public PumpLogRootModel LogRootModel { get; private set; }
        private List<ParameterTExternalEntity> _initialParameters;

        private bool _canExecuteCommand = true;
        public bool CanExecuteCommand
        {
            get => _canExecuteCommand;
            set => SetProperty(ref _canExecuteCommand, value);
        }

        public DelegateCommand ToggleMenuCommand { get; private set; }
        public DelegateCommand ShareCommand { get; private set; }
        public DelegateCommand ConfigureCommand { get; private set; }

        private void OnToggleMenu()
        {
            this.CanExecuteCommand = false;
            this.EventAggregator.GetEvent<HamburgerTappedEvent>().Publish();
            this.CanExecuteCommand = true;
        }

        private async Task OnShare()
        {
            this.CanExecuteCommand = false;
            if (_messagingPlugin.EmailMessenger.CanSendEmail)
            {
                this.UserDialogs.ShowLoading(AppResources.Loading_Message_EmailRedirect);

                string subject = string.Empty;
                string filename = string.Empty;
                var dateNow = DateTime.Now;
                string newLine = "\r\n";

                if (Device.RuntimePlatform == Device.iOS)
                {
                    newLine = "\n";
                }

                var cockpitSb = new StringBuilder();

                if (this.LogRootModel.ProfileId == ExternalComProfileValue.ModulA)
                {
                    this.RootModelModulA.SerialNumber = this.RootModelModulA.SerialNumber ?? string.Empty;

                    subject = $"{this.RootModelModulA.SerialNumber}_{dateNow.ToString("yyyyMMddhhmmss")}";
                    filename = $"{this.RootModelModulA.SerialNumber}_{dateNow.ToString("yyyyMMddhhmmss")}.txt";

                    cockpitSb = _cockpitPumpsModulAManager.GenerateEmailContent(this.RootModelModulA, newLine);
                }
                else if (this.LogRootModel.ProfileId == ExternalComProfileValue.VariA)
                {
                    this.RootModelVariAE.SerialNumber = this.RootModelVariAE.SerialNumber ?? string.Empty;

                    subject = $"{this.RootModelVariAE.SerialNumber}_{dateNow.ToString("yyyyMMddhhmmss")}";
                    filename = $"{this.RootModelVariAE.SerialNumber}_{dateNow.ToString("yyyyMMddhhmmss")}.txt";

                    cockpitSb = _cockpitPumpsVariAEManager.GenerateEmailContent(this.RootModelVariAE, newLine);
                }

                string filePath = _fileService.GetFilePath(filename);

                var logSb = _logPumpManager.GenerateEmailContent(this.LogRootModel, newLine);

                cockpitSb.Append(newLine);
                cockpitSb.Append(logSb);

                _fileService.WriteToAFile(filePath, cockpitSb.ToString(), false);

                var email = new EmailMessageBuilder()
                    .Subject(subject)
                    .Body(cockpitSb.ToString())
                    .WithAttachment(filePath, "text/plain")
                    .Build();

                _messagingPlugin.EmailMessenger.SendEmail(email);

                this.UserDialogs.HideLoading();
            }
            else
            {
                await this.UserDialogs.AlertAsync(AppResources.Alert_Message_FailedToCreateMail_Message, AppResources.Alert_Message_FailedToCreateMail_Title);
            }

            this.CanExecuteCommand = true;
        }

        private void OnConfigure()
        {
            this.CanExecuteCommand = false;

            INavigationParameters parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.InitialParameters, _initialParameters);

            if (this.LogRootModel.ProfileId == ExternalComProfileValue.ModulA)
            {
                var model = new PumpConfigurationModulAWizardModel();
                var modelResult = _configurationPumpModulAManager.MapParametersToWizardModel(model, _initialParameters);
                model = modelResult.Item1;
                model.SelectedDeviceUuid = this.LogRootModel.SelectedDeviceUuid;

                parameters.Add(NavigationConstants.PumpConfigurationModulAWizardModel, model);
                parameters.Add(NavigationConstants.ParameterCharacteristics, modelResult.Item2);
                parameters.Add(NavigationConstants.InitialParameters, _initialParameters);
                parameters.Add(NavigationConstants.CockpitPumpsModulARootModel, RootModelModulA);

                string navigationPath = $"{ViewNames.PumpConfigurationModulAForm1Page}";
                this.EventAggregator.GetEvent<MasterNavigationEvent>().Publish(new NavigationItemModel()
                {
                    NavigationPath = navigationPath,
                    Parameters = parameters,
                    IsModalNavigation = true
                });
            }
            else if (this.LogRootModel.ProfileId == ExternalComProfileValue.VariA)
            {
                var model = new PumpConfigurationVariAWizardModel();
                var modelResult = _configurationPumpVariAManager.MapParametersToWizardModel(model, _initialParameters);
                model = modelResult.Item1;
                model.SelectedDeviceUuid = this.LogRootModel.SelectedDeviceUuid;

                parameters.Add(NavigationConstants.PumpConfigurationVariAWizardModel, model);
                parameters.Add(NavigationConstants.ParameterCharacteristics, modelResult.Item2);
                parameters.Add(NavigationConstants.InitialParameters, _initialParameters);
                parameters.Add(NavigationConstants.CockpitPumpsVariAERootModel, RootModelVariAE);

                string navigationPath = $"{ViewNames.PumpConfigurationVariAForm1Page}";
                this.EventAggregator.GetEvent<MasterNavigationEvent>().Publish(new NavigationItemModel()
                {
                    NavigationPath = navigationPath,
                    Parameters = parameters,
                    IsModalNavigation = true
                });
            }

            this.CanExecuteCommand = true;
        }

        public void SetPumpLogAlertPageViewModel(PumpLogAlertPageViewModel vm)
        {
            this.PumpLogAlertPageViewModel = vm;
        }

        public void SetPumpLogWarningPageViewModel(PumpLogWarningPageViewModel vm)
        {
            this.PumpLogWarningPageViewModel = vm;
        }

        private async Task OnConnectionEstablished()
        {
            if (!(await _connectionManagementThrottle.WaitAsync(0)))
                return;

            await RemoveAlertForLostConnection();

            _connectionManagementThrottle.Release();
        }

        private async Task OnConnectionLost()
        {
            if (!(await _connectionManagementThrottle.WaitAsync(0)))
                return;

            await AlertUserOfLostConnection();

            _connectionManagementThrottle.Release();
        }

        private async Task AlertUserOfLostConnection()
        {
            if (_lostConnectionAlertDialog != null)
                return;

            _lostConnectionAlertDialog = new DialogAlertSingle();
            _lostConnectionAlertDialog.ButtonText = AppResources.Alert_Message_OK;
            _lostConnectionAlertDialog.ContentText = AppResources.Alert_MessageBody_BLE_LostConnection;
            _lostConnectionAlertDialog.Title = AppResources.Alert_MessageHeader_BLE_LostConnection;
            _lostConnectionAlertDialog.ButtonCommand =
                new DelegateCommand(
                    () =>
                    {
                        _lostConnectionAlertDialog = null;
                        EventAggregator.GetEvent<HamburgerMenuItemExecuteEvent>().Publish(new ItemModels.HamburgerMenuModel() { Menu = HamburgerMenuValue.Home, Parameters = null });
                    });
            await _popupNavigation.PushAsync(_lostConnectionAlertDialog, false);
        }

        private async Task RemoveAlertForLostConnection()
        {
            if (_lostConnectionAlertDialog != null)
            {
                await _popupNavigation.RemovePageAsync(_lostConnectionAlertDialog);
                _lostConnectionAlertDialog = null;
            }
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.PumpLogRootModel))
            {
                var model = parameters.GetValue<PumpLogRootModel>(NavigationConstants.PumpLogRootModel);

                if (model != null)
                {
                    this.LogRootModel = model;
                    if (this.PumpLogAlertPageViewModel != null)
                    {
                        this.PumpLogAlertPageViewModel.SetPumpLogAlertEvent(this.LogRootModel);
                    }

                    if (this.PumpLogWarningPageViewModel != null)
                    {
                        this.PumpLogWarningPageViewModel.SetPumpLogWarningEvent(this.LogRootModel);
                    }
                }
            }

            if (parameters.ContainsKey(NavigationConstants.InitialParameters))
            {
                _initialParameters = parameters.GetValue<List<ParameterTExternalEntity>>(NavigationConstants.InitialParameters);
            }

            if (this.LogRootModel.ProfileId == ExternalComProfileValue.ModulA && parameters.ContainsKey(NavigationConstants.CockpitPumpsModulARootModel))
            {
                var model = parameters.GetValue<CockpitPumpsModulARootModel>(NavigationConstants.CockpitPumpsModulARootModel);
                this.RootModelModulA = model;
            }
            else if (this.LogRootModel.ProfileId == ExternalComProfileValue.VariA && parameters.ContainsKey(NavigationConstants.CockpitPumpsVariAERootModel))
            {
                var model = parameters.GetValue<CockpitPumpsVariAERootModel>(NavigationConstants.CockpitPumpsVariAERootModel);
                this.RootModelVariAE = model;
            }


        }

        public override void Destroy()
        {
            base.Destroy();
            _lostConnectionAlertDialog = null;
            this.EventAggregator.GetEvent<PumpLogEvent>().Unsubscribe(_configureEventSubToken);
            this.EventAggregator.GetEvent<ConnectionEstablishedEvent>().Unsubscribe(_connectionEstablishedSubToken);
            this.EventAggregator.GetEvent<ConnectionLostEvent>().Unsubscribe(_connectionLostSubToken);
        }
    }
}
