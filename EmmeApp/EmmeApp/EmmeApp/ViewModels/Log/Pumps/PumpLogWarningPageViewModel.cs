﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.CustomControls;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Resources.HelpViews;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EmmeApp.ViewModels
{
    public class PumpLogWarningPageViewModel : ViewModelBase
    {
        private readonly IServiceEntityMapper _serviceEntityMapper;
        private readonly IPopupNavigation _popupNavigation;

        public PumpLogWarningPageViewModel(
            INavigationService navigationService,
            INavigationHelperService navigationHelperService,
            IAppCenterCustomService appCenterCustomService,
            IUserDialogs userDialogs,
            IEventAggregator eventAggregator,
            IServiceEntityMapper serviceEntityMapper,
            IPopupNavigation popupNavigation)
          : base(navigationService, navigationHelperService, appCenterCustomService, userDialogs, eventAggregator)
        {
            _serviceEntityMapper = serviceEntityMapper;
            _popupNavigation = popupNavigation;

            this.SetPumpLogWarningEvent = (model) => SetWarningModel(model);
        }
        private async Task ExecuteViewHelpTextCommand(string title, string cause, string measure)
        {
            var popupOverlay = new DialogInformationOverlay();
            popupOverlay.Title = title;

            var view = new LogAlarmWarningItemHelp();
            view.FindByName<Label>(UIConstants.LabelCauseMessage).Text = cause;
            view.FindByName<Label>(UIConstants.LabelMeasureMessage).Text = measure;

            popupOverlay.View = view;

            await _popupNavigation.PushAsync(popupOverlay);
        }

        public Action<PumpLogRootModel> SetPumpLogWarningEvent { get; private set; }

        private PumpLogWarningModel _warningModel;
        public PumpLogWarningModel WarningModel
        {
            get => _warningModel;
            set => SetProperty(ref _warningModel, value);
        }

        private List<PumpAlarmAndWarningLogModel> _warningLogList = new List<PumpAlarmAndWarningLogModel>();
        public List<PumpAlarmAndWarningLogModel> WarningLogList
        {
            get => _warningLogList;
            set => SetProperty(ref _warningLogList, value);
        }

        private void SetWarningModel(PumpLogRootModel model)
        {
            this.WarningModel = _serviceEntityMapper.Map<PumpLogWarningModel>(model);
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.PumpLogRootModel))
            {
                var model = parameters.GetValue<PumpLogRootModel>(NavigationConstants.PumpLogRootModel);
                SetWarningModel(model);
                if (WarningModel.WarningPumpLogList.Any())
                {
                    var warningLogList = this.WarningModel.WarningPumpLogList.Skip(Math.Max(0, this.WarningModel.WarningPumpLogList.Count - 5)).OrderByDescending(x => x.AppearanceDateTime).ToList();

                    this.WarningLogList = warningLogList;

                    foreach (var warningLog in this.WarningLogList)
                    {
                        if (warningLog.HasHelpText)
                        {
                            warningLog.ViewHelpTextCommand = new DelegateCommand(async ()
                                => await ExecuteViewHelpTextCommand(warningLog.Title, warningLog.Cause, warningLog.Measure));
                        }
                    }

                }
            }
        }
    }
}
