﻿using System.Collections.Generic;

namespace EmmeApp.ViewModels
{
    public class ModalChainStack : Stack<IChainModalViewModel>
   {
      public ModalChainStack ChainPush(IChainModalViewModel item)
      {
         Push(item);
         return this;
      }
   }
}
