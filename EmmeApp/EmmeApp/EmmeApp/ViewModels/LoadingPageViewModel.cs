﻿using EmmeApp.Common.Constants;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Helpers;
using EmmeApp.Managers.Abstractions;
using Prism.Navigation;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EmmeApp.ViewModels
{
    public class LoadingPageViewModel : ViewModelBase
	{
		private readonly ISortimentManager _sortimentManager;
		private readonly IMasterPumpReplacementManager _masterPumpReplacementManager;
        private readonly ICultureInfoService _cultureInfoService;
        private readonly ICrossMultilingualService _crossMultilingualService;

        public LoadingPageViewModel(INavigationService navigationService,
			INavigationHelperService navigationHelperService,
			IAppCenterCustomService appCenterService,
			ISortimentManager sortimentManager,
            ICultureInfoService cultureInfoService,
            ICrossMultilingualService crossMultilingualService,
			IMasterPumpReplacementManager masterPumpReplacementManager)
			: base(navigationService, navigationHelperService, appCenterService)
		{
			_sortimentManager = sortimentManager;
			_masterPumpReplacementManager = masterPumpReplacementManager;
            _cultureInfoService = cultureInfoService;
            _crossMultilingualService = crossMultilingualService;
		}

		public override async void OnNavigatedTo(INavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);

			this.AppCenterService.TrackEvent($"{ViewNames.LoadingPage} - OnNavigatedTo");

            var ic = _cultureInfoService.GetCurrentLanguage();
            _crossMultilingualService.SetCurrentLanguage(ic);

			await SeedData();
		}

		private async Task SeedData()
		{
			await Task.Run(async () =>
			{
				await Task.Delay(3000);

				this.AppCenterService.TrackEvent($"{ViewNames.LoadingPage} - SeedData");

				Device.BeginInvokeOnMainThread(async () =>
				{
					int sortimentLatestVersion = _sortimentManager.GetSortimentVersionFromAssemblyResource();
					int sortimentCurrentVersion = _sortimentManager.GetSortimentVersionFromDB();

					if (sortimentLatestVersion > sortimentCurrentVersion)
					{
						this.AppCenterService.TrackEvent($"{ViewNames.LoadingPage} - Seed Data - PopulateSortiment");

						var sortiment = _sortimentManager.GetSortimentFromAssemblyResource();
						_masterPumpReplacementManager.DeleteAllPumpReplacement();

						foreach (var market in sortiment.Markets)
						{
							_masterPumpReplacementManager.PopulatePumpReplacementFile(market.Key);
						}
						_masterPumpReplacementManager.PopulatePumpAdditionalInformation();
						_masterPumpReplacementManager.PopulateMasterPumpReplacementData();
						_sortimentManager.SaveSortimentsToDB();
					}

					var lastModified = _sortimentManager.GetPumpReplacementLastModified();

					bool hasUpdate = false;
					try
					{
						hasUpdate = await _sortimentManager.HasUpdate(lastModified);
					}
					catch (Exception)
					{
					}

					INavigationParameters parameters = new NavigationParameters();
					parameters.Add(NavigationConstants.HasUpdate, hasUpdate);

					await NavigationUtilities.NavigateAsync(this.NavigationService, $"../{ViewNames.EmmeMasterDetailPage}/{ViewNames.NavigationPage}/{ViewNames.HomePage}", parameters, null, false);
				});
			});
		}
	}
}
