﻿using EmmeApp.Common.Constants;
using Xamarin.Forms;

namespace EmmeApp.Effects
{
    public class SizeToFit : RoutingEffect
	{
		public SizeToFit() : base($"{EffectsConstants.BaseNamespace}.{nameof(SizeToFit)}")
		{

		}
	}
}
