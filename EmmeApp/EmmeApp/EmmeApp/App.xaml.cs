﻿using Acr.UserDialogs;
using DLToolkit.Forms.Controls;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Services;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Localization.Resources;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Crashes;
using Plugin.DeviceInfo.Abstractions;
using Prism;
using Prism.DryIoc;
using Prism.Ioc;
using System.Text;
using System.Threading.Tasks;
using EmmeApp.Providers;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace EmmeApp
{
    public partial class App : PrismApplication
	{
		public App(IPlatformInitializer initializer) : base(initializer)
		{
			InitializeCrashReport();
			FlowListView.Init();
		}


		protected override async void OnInitialized()
		{
			InitializeComponent();

			AiForms.Effects.EffectConfig.EnableTriggerProperty = false;

			var analyticsService = Resolve<IAnalyticsService>();
			var deviceInfo = Resolve<IDeviceInfo>();

			analyticsService.LogEvent("AppStart", "Version", deviceInfo.AppVersion);

			await NavigationUtilities.NavigateAsync(NavigationService, $"/{ViewNames.TransitionNavigationPage}/{ViewNames.LoadingPage}", null, null, true);
		}

		protected override void RegisterTypes(IContainerRegistry containerRegistry)
		{
		}

		public static T Resolve<T>()
		{
			return Current.Container.Resolve<T>();
		}

		public static T Resolve<T>(string name)
		{
			return Current.Container.Resolve<T>(name);
		}

		protected override async void OnStart()
		{
			InitAppCenter();
		}

		private void InitAppCenter()
		{
			//IAppCenterCustomService appCenterService = Resolve<IAppCenterCustomService>();
			//appCenterService.Init();
			//AppCenter.Start($"android={AppConstants.AppCenterAndroidKey};ios={AppConstants.AppCenteriOSKey}",
			//				   typeof(Analytics), typeof(Crashes));
			//AppCenter.LogLevel = LogLevel.Verbose;
			//Analytics.SetEnabledAsync(true);
		}

		private void InitializeCrashReport()
		{
			Crashes.SendingErrorReport += SendingErrorReportHandler;
			Crashes.SentErrorReport += SentErrorReportHandler;
			Crashes.FailedToSendErrorReport += FailedToSendErrorReportHandler;
			Crashes.ShouldAwaitUserConfirmation = () =>
			{

				UserDialogs.Instance.ActionSheet(
					new ActionSheetConfig()
					.SetDestructive(AppResources.AppCenter_Crashlytics_Destructive)
					.SetTitle(AppResources.AppCenter_Crashlytics_Title)
					.Add(AppResources.AppCenter_Crashlytics_AlwaysSend, () => Crashes.NotifyUserConfirmation(UserConfirmation.AlwaysSend))
					.Add(AppResources.AppCenter_Crashlytics_Send, () => Crashes.NotifyUserConfirmation(UserConfirmation.Send))
					.Add(AppResources.AppCenter_Crashlytics_DontSend, () => Crashes.NotifyUserConfirmation(UserConfirmation.DontSend)));

				return true;
			};
		}

		void SendingErrorReportHandler(object sender, SendingErrorReportEventArgs e)
		{
			AppCenterLog.Info(AppConstants.AppCenterLogTag, "Sending error report");

			var args = e;
			var report = args.Report;
			LogReport(report, null);
		}

		void SentErrorReportHandler(object sender, SentErrorReportEventArgs e)
		{
			AppCenterLog.Info(AppConstants.AppCenterLogTag, "Sent error report");

			var args = e;
			var report = args.Report;
			LogReport(report, null);
		}

		void FailedToSendErrorReportHandler(object sender, FailedToSendErrorReportEventArgs e)
		{
			AppCenterLog.Info(AppConstants.AppCenterLogTag, "Failed to send error report");

			var args = e;
			var report = args.Report;
			LogReport(report, e.Exception);
		}

		private void LogReport(ErrorReport report, object exception)
		{
			string message = "";
			if (report.Exception != null)
			{
				var inner = report.Exception.InnerException;
				message = report.Exception.Message + report.Exception.StackTrace;

				while (inner != null)
				{
					var innerex = inner.InnerException;
					StringBuilder strBuilder = new StringBuilder();
					strBuilder.Append(message);
					strBuilder.Append(inner.Message);
					strBuilder.Append(inner.StackTrace);
					message = strBuilder.ToString();

					inner = innerex;
				}

				AppCenterLog.Verbose(AppConstants.AppCenterLogTag, message, report.Exception);
			}

			else
			{
				AppCenterLog.Verbose(AppConstants.AppCenterLogTag, "No system exception was found");
			}

			if (report.AndroidDetails != null)
			{
				AppCenterLog.Verbose(AppConstants.AppCenterLogTag, report.AndroidDetails.ThreadName);
			}
			else if (report.iOSDetails != null)
			{
				AppCenterLog.Verbose(AppConstants.AppCenterLogTag, report.iOSDetails.ExceptionName);
			}

			if (exception != null)
			{
				AppCenterLog.Verbose(AppConstants.AppCenterLogTag, "There is an exception associated with the failure");
			}
		}

	}
}
