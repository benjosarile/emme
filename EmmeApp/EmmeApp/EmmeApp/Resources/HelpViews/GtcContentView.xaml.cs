﻿using System;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace EmmeApp.Resources.HelpViews
{
    public partial class GtcContentView : ContentView
    {
        public static readonly BindableProperty UrlProperty = BindableProperty.Create(nameof(Url), typeof(string), typeof(GtcContentView), string.Empty, BindingMode.TwoWay);

        public string Url
        {
            get { return (string)GetValue(UrlProperty); }
            set { SetValue(UrlProperty, value); }
        }


        public GtcContentView()
        {
            InitializeComponent();
        }

        private async void lblGtc_Tapped(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Url))
            {
                await Launcher.TryOpenAsync(Url);
            }
        }
    }
}