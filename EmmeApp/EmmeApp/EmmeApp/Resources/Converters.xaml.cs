﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EmmeApp.Resources
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Converters : ResourceDictionary
    {
        public Converters()
        {
            InitializeComponent();
        }
    }
}