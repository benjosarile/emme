﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EmmeApp.Resources
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ContentViews : ResourceDictionary
	{
		public ContentViews()
		{
			InitializeComponent();
		}
	}
}