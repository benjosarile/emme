﻿namespace EmmeApp.Helpers
{
	public interface ICrossMultilingualService
	{
		bool SetCurrentLanguage(string twoLetterIsoLanguageName);
	}
}