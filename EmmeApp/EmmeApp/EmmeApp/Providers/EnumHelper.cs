﻿using EmmeApp.Common.Enum;
using EmmeApp.Localization.Resources;
using Humanizer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EmmeApp.Helpers
{
	public class EnumProvider : IEnumProvider
	{
		public List<T> PopulateList<T>() where T : struct, IConvertible
		{
			if (!typeof(T).IsEnum)
			{
				throw new ArgumentException("T must be an enumerated type");
			}

			List<T> list = Enum.GetValues(typeof(T))
				.Cast<T>()
				.ToList();

			return list;
		}

		public T GetEnumValue<T>(string value) where T : struct, IConvertible
		{
			if (!typeof(T).IsEnum)
			{
				throw new ArgumentException("T must be an enumerated type");
			}

			T retVal = (T)Enum.Parse(typeof(T), value.DehumanizeTo(typeof(T), OnNoMatch.ReturnsNull).ToString());
			return retVal;
		}

		public string GetWizardUnitText(ExternalUnitTValue value)
		{
			string text = string.Empty;
			switch (value)
			{
				case ExternalUnitTValue.Ampere: text = AppResources.WizardForm_UnitInA; break;
				case ExternalUnitTValue.Celsius: text = AppResources.WizardForm_UnitsInCelsius; break;
				case ExternalUnitTValue.M3: text = AppResources.WizardForm_UnitsInCubicMeter; break;
				case ExternalUnitTValue.Days: text = AppResources.WizardForm_UnitsInDays; break;
				case ExternalUnitTValue.Hours: text = AppResources.WizardForm_UnitsInHours; break;
				case ExternalUnitTValue.Kwh: text = AppResources.WizardForm_UnitsInKiloWattHour; break;
				case ExternalUnitTValue.CubicMeterPerHour: text = AppResources.WizardForm_UnitsInm3h; break;
				case ExternalUnitTValue.Meter: text = AppResources.WizardForm_UnitsInMeters; break;
				case ExternalUnitTValue.Miliampere: text = AppResources.WizardForm_UnitsInMilliampere; break;
				case ExternalUnitTValue.Percent: text = AppResources.WizardForm_UnitsInPercent; break;
				case ExternalUnitTValue.Pmin: text = AppResources.WizardForm_UnitsInPerMinute; break;
				case ExternalUnitTValue.Seconds: text = AppResources.WizardForm_UnitsInSeconds; break;
				case ExternalUnitTValue.Watt: text = AppResources.WizardForm_UnitsInWatt; break;

			}
			return text;
		}
	}
}
