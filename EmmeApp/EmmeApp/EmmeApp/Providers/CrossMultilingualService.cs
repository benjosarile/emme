﻿using EmmeApp.Localization.Resources;
using Plugin.Multilingual;
using System;
using System.Linq;

namespace EmmeApp.Helpers
{
	public class CrossMultilingualService : ICrossMultilingualService
	{
		public bool SetCurrentLanguage(string twoLetterIsoLanguageName)
		{
			bool result = false;
			try
			{
				var cultureInfo = CrossMultilingual.Current.NeutralCultureInfoList.AsEnumerable().FirstOrDefault(element => element.TwoLetterISOLanguageName.Contains(twoLetterIsoLanguageName));

				if (cultureInfo != null)
				{
					result = true;
					AppResources.Culture = cultureInfo;

				}
			}
			catch(Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);
			}

			return result;
		}
	}
}
