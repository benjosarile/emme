using System;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace EmmeApp.Providers
{
    public interface IAppTrackingTransparencyPermission
    {
        /// <summary>
        /// This method checks if current status of the permission
        /// </summary>
        /// <returns></returns>
        Task<PermissionStatus> CheckPermissionStatusAsync();

        /// <summary>
        /// Requests the user to accept or deny a permission
        /// </summary>
        /// <returns></returns>
        void RequestPermissionAsync(Action<PermissionStatus> completion);
    }
}