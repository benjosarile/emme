﻿using EmmeApp.Common.Enum;
using System;
using System.Collections.Generic;

namespace EmmeApp.Helpers
{
	public interface IEnumProvider
	{
		List<T> PopulateList<T>() where T : struct, IConvertible;

        T GetEnumValue<T>(string value) where T : struct, IConvertible;
		string GetWizardUnitText(ExternalUnitTValue value);

	}
}