﻿using Plugin.Permissions.Abstractions;
using System.Threading.Tasks;

namespace EmmeApp.Services
{
    public class PermissionsService : IPermissionsService
	{
		private readonly IPermissions _permissionsUtils;

		public PermissionsService(IPermissions permissionsUtils)
		{
			_permissionsUtils = permissionsUtils;
		}

		public async Task<bool> RequestDevicePermission(Permission permission)
		{
			var status = await _permissionsUtils.CheckPermissionStatusAsync(permission);

			if (status != PermissionStatus.Granted)
			{
				var dictPermissions = await _permissionsUtils.RequestPermissionsAsync(permission);
				status = dictPermissions[permission];
			}

			return status == PermissionStatus.Granted;
		}

	}
}
