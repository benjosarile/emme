﻿using System.IO;

namespace EmmeApp.Providers
{
    public static class StreamHelper
	{
		public static byte[] ToByteArray(this Stream stream)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				stream.Seek(0, SeekOrigin.Begin);
				stream.CopyTo(ms);
				return ms.ToArray();
			}
		}
	}
}
