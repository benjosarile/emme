﻿using Plugin.Permissions.Abstractions;
using System.Threading.Tasks;

namespace EmmeApp.Services
{
	public interface IPermissionsService
	{
		Task<bool> RequestDevicePermission(Permission permission);

	}
}