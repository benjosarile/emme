﻿using EmmeApp.Common.Enum;

namespace EmmeApp.ItemModels
{
	public class CockpitControlUnitButtonSendModel
    {
		public ExternalControlUnitParameterIdValue ParameterId { get; set; }
		public long Value { get; set; }
	}
}
