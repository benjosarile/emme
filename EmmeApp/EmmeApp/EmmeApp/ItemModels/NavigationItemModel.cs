﻿using Prism.Mvvm;
using Prism.Navigation;

namespace EmmeApp.ItemModels
{
    public class NavigationItemModel : BindableBase
    {
		public string NavigationPath { get; set; }
		public INavigationParameters Parameters { get; set; } = new NavigationParameters();

		private bool _isModalNavigation;
		public bool IsModalNavigation
		{
			get => _isModalNavigation;
			set => SetProperty(ref _isModalNavigation, value);
		}
	}
}
