﻿using EmmeApp.Common.Enum;
using System;

namespace EmmeApp.ItemModels
{
	public class MasterNavigationItemModel : NavigationItemModel
	{
		public HamburgerMenuValue MenuItemId { get; set; }

		private string _title;
		public string Title
		{
			get => _title;
			set => SetProperty(ref _title, value);
		}

		private bool _isSelected;
		public bool IsSelected
		{
			get => _isSelected;
			set => SetProperty(ref _isSelected, value);
		}

		public Action Action { get; set; }

	}
}
