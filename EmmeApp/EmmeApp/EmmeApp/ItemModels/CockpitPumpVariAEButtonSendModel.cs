﻿using EmmeApp.Common.Enum;

namespace EmmeApp.ItemModels
{
	public class CockpitPumpVariAEButtonSendModel
    {
        public ExternalPumpVariAParameterIdValue ParameterId { get; set; }
        public int Value { get; set; }
    }
}
