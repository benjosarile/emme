﻿using EmmeApp.Common.Enum;
using Prism.Navigation;

namespace EmmeApp.ItemModels
{
    public class HamburgerMenuModel
    {
		public HamburgerMenuValue Menu { get; set; }
		public NavigationParameters Parameters { get; set; }
	}
}
