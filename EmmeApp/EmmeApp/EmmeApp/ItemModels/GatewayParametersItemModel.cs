﻿using Prism.Navigation;

namespace EmmeApp.ItemModels
{
    public class GatewayParametersItemModel
    {
        public string NavigationPath { get; set; }
        public INavigationParameters NavigationParameter { get; set; }
    }
}
