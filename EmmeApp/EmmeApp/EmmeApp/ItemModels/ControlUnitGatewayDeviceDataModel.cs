﻿using EmmeApp.Common.Enum;
using EmmeApp.Entities.External;
using System.Collections.Generic;
using System.Linq;

namespace EmmeApp.ItemModels
{
	public class ControlUnitGatewayDeviceDataModel
	{
		public InitCommunicationTExternalEntity InitCommunication { get; set; }
		public List<LogEntryTExternalEntity> LogEntries { get; set; }
		public List<ParameterTExternalEntity> Parameters { get; set; }

		public ControlUnitGatewayDeviceDataModel()
		{
			this.LogEntries = new List<LogEntryTExternalEntity>();
			this.Parameters = new List<ParameterTExternalEntity>();
		}

		public bool TrySet(InitCommunicationTExternalEntity initCommunication)
		{
			if (this.InitCommunication == null)
			{
				this.InitCommunication = initCommunication;
				return true;
			}

			return false;
		}

		public bool TrySet(List<LogEntryTExternalEntity> logEntries)
		{
			if (!logEntries.Any() || logEntries.Any(x => x.Source == ExternalLogSourceValue.UnknownLogSource))
			{
				return false;
			}

			this.LogEntries.AddRange(logEntries);
			return true;
		}

		public bool TrySet(List<ParameterTExternalEntity> parameters)
		{
			if (!parameters.Any() || parameters.Any(x => x.IdCase == ExternalIdOneofCaseValue.None))
			{
				return false;
			}

			foreach (var parameter in parameters)
			{
				var currentParameter = this.Parameters.FirstOrDefault(x => x.ControlUnit == parameter.ControlUnit);

				if (currentParameter != null)
				{
					currentParameter.Value = parameter.Value;
				}
				else
				{
					this.Parameters.Add(parameter);
				}
			}

			return true;
		}
	}
}
