﻿using EmmeApp.Common.Enum;

namespace EmmeApp.ItemModels
{
	public class CockpitPumpModulAButtonSendModel
    {
        public ExternalPumpModulAParameterIdValue ParameterId { get; set; }
        public int Value { get; set; }
    }
}
