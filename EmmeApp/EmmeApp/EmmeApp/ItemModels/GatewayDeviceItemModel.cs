﻿using Plugin.BluetoothLE;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace EmmeApp.ItemModels
{
    public class GatewayDeviceItemModel : BindableBase
    {

        private IDevice _device;
        public IDevice Device
        {
            get => _device;
            set => SetProperty(ref _device, value);
        }

        private string _name;
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private bool _isConnected;
        public bool IsConnected
        {
            get => _isConnected;
            set => SetProperty(ref _isConnected, value);
        }

        private Guid _uuid;
        public Guid Uuid
        {
            get => _uuid;
            set => SetProperty(ref _uuid, value);
        }

        private int _rssi;
        public int Rssi
        {
            get => _rssi;
            set => SetProperty(ref _rssi, value);
        }

        private string _deviceUuid;
        public string DeviceUuid
        {
            get => _deviceUuid;
            set => SetProperty(ref _deviceUuid, value);
        }

        private List<byte> _manufacturerData;
        public List<byte> ManufacturerData
        {
            get => _manufacturerData;
            set => SetProperty(ref _manufacturerData, value);
        }

        public bool TrySet(IScanResult result)
        {
            var response = false;

            try
            {
                if (this.Uuid == Guid.Empty)
                {
                    this.ManufacturerData = (result.AdvertisementData.ManufacturerData?.ToList() ?? new List<byte>());

                    if (!this.ManufacturerData.Any())
                    {
                        this.ManufacturerData.Add(0xFF);
                        this.ManufacturerData.Add(0xFF);
                    }

                    this.Device = result.Device;
                    this.Uuid = this.Device.Uuid;
                    switch (Xamarin.Forms.Device.RuntimePlatform)
                    {
                        case Xamarin.Forms.Device.Android:
                            this.DeviceUuid = this.Device.NativeDevice.GetType().GetProperty("Address").GetValue(Device.NativeDevice).ToString();
                            break;
                        case Xamarin.Forms.Device.iOS:
                            this.DeviceUuid = this.Uuid.ToString();
                            break;
                    }

                    response = true;
                }
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }


            if (result != null && result.Device != null && this.Uuid == result.Device.Uuid)
            {
                response = true;

                this.Name = result.Device.Name;
                this.Rssi = result.Rssi;
            }

            return response;
        }
    }
}
