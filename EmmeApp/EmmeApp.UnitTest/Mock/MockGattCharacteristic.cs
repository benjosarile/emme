﻿using Plugin.BluetoothLE;
using System;
using System.Reactive.Linq;

namespace EmmeApp.Mock
{
	public class MockGattCharacteristic : IGattCharacteristic
	{
		public IGattService Service { get; set; }

		public Guid Uuid { get; set; }

		public string Description { get; set; }

		public bool IsNotifying { get; set; }

		public CharacteristicProperties Properties { get; set; }

		public byte[] Value { get; set; }

		public IObservable<CharacteristicGattResult> DisableNotifications()
		{
			return Observable.Create<CharacteristicGattResult>(o =>
			{
				return Observable.Repeat<CharacteristicGattResult>(new CharacteristicGattResult(null, null) { }, 1).Subscribe(o);
			});
		}

		public IObservable<IGattDescriptor> DiscoverDescriptors()
		{
			throw new NotImplementedException();
		}

		public IObservable<CharacteristicGattResult> EnableNotifications(bool useIndicationIfAvailable = false)
		{
			return Observable.Create<CharacteristicGattResult>(o =>
			{
				return Observable.Repeat<CharacteristicGattResult>(new CharacteristicGattResult(null, null) { }, 1).Subscribe(o);
			});
		}

		public IObservable<CharacteristicGattResult> Read()
		{
			return Observable.Create<CharacteristicGattResult>(o =>
			{
				return Observable.Repeat<CharacteristicGattResult>(new CharacteristicGattResult(null, null) { }, 1).Subscribe(o);
			});
		}

		public IObservable<CharacteristicGattResult> WhenNotificationReceived()
		{
			return Observable.Create<CharacteristicGattResult>(o =>
			{
				return Observable.Repeat<CharacteristicGattResult>(new CharacteristicGattResult(null, null) { }, 1).Subscribe(o);
			});
		}

		public IObservable<CharacteristicGattResult> Write(byte[] value)
		{
			return Observable.Create<CharacteristicGattResult>(o =>
			{
				return Observable.Repeat<CharacteristicGattResult>(new CharacteristicGattResult(null, null) { }, 1).Subscribe(o);
			});
		}

		public IObservable<CharacteristicGattResult> WriteWithoutResponse(byte[] value)
		{
			return Observable.Create<CharacteristicGattResult>(o =>
			{
				return Observable.Repeat<CharacteristicGattResult>(new CharacteristicGattResult(null, null) { }, 1).Subscribe(o);
			});
		}
	}
}
