﻿using Plugin.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmmeApp.Mock
{
	public class MockGattService : IGattService
	{
		public IDevice Device { get; set; }

		public Guid Uuid { get; set; }

		public string Description { get; set; }

		public IObservable<IGattCharacteristic> DiscoverCharacteristics()
		{
			return Observable.Create<IGattCharacteristic>(o =>
			{
				return Observable.Repeat<IGattCharacteristic>(DiscoveredCharacteristic, 1).Subscribe(o);
			});
		}

		public IObservable<IGattCharacteristic> GetKnownCharacteristics(params Guid[] characteristicIds)
		{
			return Observable.Create<IGattCharacteristic>(o =>
			{
				return Observable.Repeat<IGattCharacteristic>(KnownCharacteristics, 1).Subscribe(o);
			});
		}

		public MockGattCharacteristic DiscoveredCharacteristic { get; set; }
		public MockGattCharacteristic KnownCharacteristics { get; set; }
	}
}
