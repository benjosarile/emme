﻿using Plugin.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace EmmeApp.Mock
{
	public class MockDevice : IDevice
	{
		public object NativeDevice => throw new NotImplementedException();

		public DeviceFeatures Features => throw new NotImplementedException();

		public string Name { get; set; }

		public Guid Uuid { get; set; }

		public int MtuSize => throw new NotImplementedException();

		public PairingStatus PairingStatus => throw new NotImplementedException();

		public ConnectionStatus Status => throw new NotImplementedException();

		public MockGattService KnownService { get; set; }

		public IGattReliableWriteTransaction BeginReliableWriteTransaction()
		{
			throw new NotImplementedException();
		}

		public void CancelConnection()
		{
			throw new NotImplementedException();
		}

		public void Connect(ConnectionConfig config = null)
		{
			throw new NotImplementedException();
		}

		public IObservable<IGattService> DiscoverServices()
		{
			throw new NotImplementedException();
		}

		public IObservable<IGattService> GetKnownService(Guid serviceUuid)
		{
			return Observable.Create<IGattService>(o =>
			{
				return Observable.Repeat<IGattService>(KnownService, 1).Subscribe(o);
			});
		}

		public IObservable<bool> PairingRequest(string pin = null)
		{
			throw new NotImplementedException();
		}

		public IObservable<int> ReadRssi()
		{
			throw new NotImplementedException();
		}

		public IObservable<int> RequestMtu(int size)
		{
			throw new NotImplementedException();
		}

		public IObservable<BleException> WhenConnectionFailed()
		{
			throw new NotImplementedException();
		}

		public IObservable<int> WhenMtuChanged()
		{
			throw new NotImplementedException();
		}

		public IObservable<string> WhenNameUpdated()
		{
			throw new NotImplementedException();
		}

		public IObservable<ConnectionStatus> WhenStatusChanged()
		{
			throw new NotImplementedException();
		}
	}
}
