﻿using Plugin.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmmeApp.Mock
{
    public class MockIAdvertisementData : IAdvertisementData
    {
        public string LocalName => throw new NotImplementedException();

        public bool IsConnectable => throw new NotImplementedException();

        public IReadOnlyList<byte[]> ServiceData => throw new NotImplementedException();

        public byte[] ManufacturerData => throw new NotImplementedException();

        public Guid[] ServiceUuids => throw new NotImplementedException();

        public int TxPower => throw new NotImplementedException();
    }
}
