﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Entities.External;
using Moq;
using NUnit.Framework;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using Shouldly;
using System.Collections.Generic;
using System.Threading.Tasks;
using EmmeApp.Common.Utilities.Abstractions;

namespace EmmeApp.ViewModels
{
	[TestFixture]
    public class InitialStartupForm1CustomerInfoPageViewModelTests
    {
        private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);
        private Mock<INavigationService> _mockNavigationService;
        private Mock<INavigationHelperService> _mockNavigationHelperService;
        private Mock<IUserDialogs> _mockUserDialogs;
        private Mock<IEventAggregator> _mockEventAggregator;
        private Mock<IPopupNavigation> _mockPopupNavigation;
        private Mock<IKeyboardHelper> _mockKeyboardHelper;
		private Mock<IAppCenterCustomService> _mockAppCenterService;

		private InitialStartupForm1CustomerInfoPageViewModel _systemUnderTest;

        [SetUp]
        public void Setup()
        {
            _mockRepository.Create<INavigationService>();
            _mockNavigationService = _mockRepository.Create<INavigationService>();
            _mockNavigationHelperService = _mockRepository.Create<INavigationHelperService>();
            _mockUserDialogs = _mockRepository.Create<IUserDialogs>();
            _mockEventAggregator = _mockRepository.Create<IEventAggregator>();
            _mockPopupNavigation = _mockRepository.Create<IPopupNavigation>();
            _mockKeyboardHelper = _mockRepository.Create<IKeyboardHelper>();
			_mockAppCenterService = _mockRepository.Create<IAppCenterCustomService>();

            _systemUnderTest = new InitialStartupForm1CustomerInfoPageViewModel(
                _mockNavigationService.Object,
                _mockNavigationHelperService.Object,
				_mockAppCenterService.Object,
                _mockUserDialogs.Object,
                _mockEventAggregator.Object,
                _mockPopupNavigation.Object,
                _mockKeyboardHelper.Object);
        }

        [Test]
        public void Ctor_Always_PerformsExpectedWork()
        {
			_mockNavigationService.Verify();
			_mockNavigationHelperService.Verify();
			_mockPopupNavigation.Verify();
			_mockUserDialogs.Verify();
            _mockRepository.Verify();
        }



        [Test]
        public void WizardRightButtonCommand_FillsAllFieldsCorrectly_AlwaysSucceed()
        {
            string max32char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456";
            _systemUnderTest.WizardModel = new InitialStartupWizardModel()
            {
                CustomerInformationName = max32char,
                CustomerInformationPhone = max32char,
                CustomerInformationStreet = max32char,
                CustomerInformationCity = max32char,
                CustomerInformationZipCode = max32char,
                CustomerInformationCountry = max32char
            };


            int numberOfCalls = 0;

            NavigationUtilities.NavigateAsync = (navSvc, page, navParams, isModal, isAnimated) =>
            {
                if (page.Equals(ViewNames.InitialStartupForm2SystemDataConfigurationPage))
                {
                    numberOfCalls++;
                }
                return Task.FromResult<INavigationResult>(null);
            };

            _mockKeyboardHelper.Setup(k => k.HideKeyboard());

            _systemUnderTest.WizardRightButtonCommand?.Execute();

            _mockKeyboardHelper.Verify(k => k.HideKeyboard());

            numberOfCalls.ShouldBe(1);

            Assert.IsTrue(_systemUnderTest.WizardModel.CustomerInformationName.Length <= 32);
            Assert.IsTrue(_systemUnderTest.WizardModel.CustomerInformationPhone.Length <= 32);
            Assert.IsTrue(_systemUnderTest.WizardModel.CustomerInformationStreet.Length <= 32);
            Assert.IsTrue(_systemUnderTest.WizardModel.CustomerInformationCity.Length <= 32);
            Assert.IsTrue(_systemUnderTest.WizardModel.CustomerInformationZipCode.Length <= 32);
            Assert.IsTrue(_systemUnderTest.WizardModel.CustomerInformationCountry.Length <= 32);

            _systemUnderTest.WizardModel.ShouldBeOfType<InitialStartupWizardModel>();
            _systemUnderTest.WizardErrorMessage.ShouldBeEmpty();
            _systemUnderTest.WizardErrorMessageVisibility.ShouldBe(false);
        }

        [Test]
        public void WizardRightButtonCommand_FieldsHasExceed32CharacterLimit_ContainsErrorMessage()
        {
            var morethan32Char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
            _systemUnderTest.WizardModel = new InitialStartupWizardModel()
            {
                CustomerInformationName = morethan32Char,
                CustomerInformationPhone = morethan32Char,
                CustomerInformationStreet = morethan32Char,
                CustomerInformationCity = morethan32Char,
                CustomerInformationZipCode = morethan32Char,
                CustomerInformationCountry = morethan32Char
            };

			_systemUnderTest.NameIsError = true;
			_systemUnderTest.PhoneIsError = true;
			_systemUnderTest.StreetIsError = true;
			_systemUnderTest.CityIsError = true;
			_systemUnderTest.ZipCodeIsError = true;
			_systemUnderTest.CountryIsError = true;

			_mockKeyboardHelper.Setup(k => k.HideKeyboard());

            _systemUnderTest.WizardRightButtonCommand?.Execute();

            _mockKeyboardHelper.Verify(k => k.HideKeyboard());

			Assert.IsTrue(_systemUnderTest.NameIsError);
			Assert.IsTrue(_systemUnderTest.PhoneIsError);
			Assert.IsTrue(_systemUnderTest.StreetIsError);
			Assert.IsTrue(_systemUnderTest.CityIsError);
			Assert.IsTrue(_systemUnderTest.ZipCodeIsError);
			Assert.IsTrue(_systemUnderTest.CountryIsError);

            _systemUnderTest.WizardErrorMessage.ShouldBe("Entry is limited to 32 characters.");
            _systemUnderTest.WizardErrorMessageVisibility.ShouldBe(true);
        }


        [Test]
        public void OnNavigatedTo_ParameterContainsShouldExitWizard_ShouldGoBack()
        {
            var numberOfCalls = 0;

            NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
            {
                ++numberOfCalls;
                return Task.FromResult<INavigationResult>(null);
            };
            var parameters = new NavigationParameters {{NavigationConstants.ShouldExitWizard, ""}};

            _mockKeyboardHelper.Setup(k => k.HideKeyboard());

            _systemUnderTest.OnNavigatedTo(parameters);

            _mockKeyboardHelper.Verify(k => k.HideKeyboard());

            numberOfCalls.ShouldBe(1);
        }

        [Test]
        public void OnNavigatedTo_ParameterContainsInitialParameters_ShouldContainInitialParameter()
        { 
            var initialParameters = new List<ParameterTExternalEntity>();

            var parameters = new NavigationParameters {{NavigationConstants.InitialParameters, initialParameters}};

            _systemUnderTest.OnNavigatedTo(parameters);

            _systemUnderTest.InitialParameters.ShouldNotBeNull();

        }

        [Test]
        public void OnNavigatedTo_ParameterContainsInitialStartupWizardModels_ShouldContainInitialStartupWizardModel()
        {
            var wizardModel = new InitialStartupWizardModel();
            var parameters = new NavigationParameters {{NavigationConstants.InitialStartupWizardModel, wizardModel}};

            _systemUnderTest.OnNavigatedTo(parameters);

            _systemUnderTest.WizardModel.ShouldNotBeNull();

        }
    }
}
