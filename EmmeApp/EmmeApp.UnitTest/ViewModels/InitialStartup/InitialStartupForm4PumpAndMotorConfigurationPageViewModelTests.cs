﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Entities.External;
using EmmeApp.Helpers;
using Moq;
using NUnit.Framework;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EmmeApp.Common.Utilities.Abstractions;

namespace EmmeApp.ViewModels
{
	[TestFixture]
	public class InitialStartupForm4PumpAndMotorConfigurationPageViewModelTests
	{
		private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);
		private Mock<INavigationService> _mockNavigationService;
		private Mock<INavigationHelperService> _mockNavigationHelperService;
		private Mock<IUserDialogs> _mockUserDialogs;
		private Mock<IEventAggregator> _mockEventAggregator;
		private Mock<IPopupNavigation> _mockPopupNavigation;
		private Mock<IEnumProvider> _mockEnumHelper;
		private Mock<IAppCenterCustomService> _mockAppCenterService;

		private InitialStartupForm4PumpAndMotorConfigurationPageViewModel _systemUnderTest;

		[SetUp]
		public void Setup()
		{
			_mockRepository.Create<INavigationService>();
			_mockNavigationService = _mockRepository.Create<INavigationService>();
			_mockNavigationHelperService = _mockRepository.Create<INavigationHelperService>();
			_mockUserDialogs = _mockRepository.Create<IUserDialogs>();
			_mockEventAggregator = _mockRepository.Create<IEventAggregator>();
			_mockPopupNavigation = _mockRepository.Create<IPopupNavigation>();
			_mockEnumHelper = _mockRepository.Create<IEnumProvider>();
			_mockAppCenterService = _mockRepository.Create<IAppCenterCustomService>();

			_mockEnumHelper.Setup(o => o.PopulateList<ControlUnitSystemDoubleMotorValue>())
				.Returns(new List<ControlUnitSystemDoubleMotorValue>()
				{
					ControlUnitSystemDoubleMotorValue.OnePump,
					ControlUnitSystemDoubleMotorValue.TwoPumps,
				});

			_mockEnumHelper.Setup(o => o.PopulateList<ControlUnitMotorModeValue>())
						.Returns(new List<ControlUnitMotorModeValue>()
						{
							ControlUnitMotorModeValue.Auto,
							ControlUnitMotorModeValue.Inactive,
							ControlUnitMotorModeValue.Manual,
						});

			_mockAppCenterService.Setup(o => o.TrackError(It.IsAny<Exception>(), It.IsAny<Dictionary<string, string>>()));
			_mockAppCenterService.Setup(o => o.TrackEvent(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>()));

			_systemUnderTest = new InitialStartupForm4PumpAndMotorConfigurationPageViewModel(
				_mockNavigationService.Object,
				_mockNavigationHelperService.Object,
				_mockAppCenterService.Object,
				_mockUserDialogs.Object,
				_mockEventAggregator.Object,
				_mockPopupNavigation.Object,
				_mockEnumHelper.Object);
		}

		[Test]
		public void Ctor_Always_PerformsExpectedWork()
		{
			_mockNavigationService.Verify();
			_mockNavigationHelperService.Verify();
			_mockPopupNavigation.Verify();
			_mockUserDialogs.Verify();
			_mockRepository.Verify();
		}

		[Test]
		public void WizardLeftButtonCommand_WhenLeftButtonPressed_ShouldContainsWizardModel()
		{
			var numberOfCalls = 0;
			var isContainsWizardModel = false;

			NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				isContainsWizardModel = navParams.ContainsKey(NavigationConstants.InitialStartupWizardModel);
				return Task.FromResult<INavigationResult>(null);
			};

			_systemUnderTest.WizardLeftButtonCommand?.Execute();

			numberOfCalls.ShouldBe(1);
			isContainsWizardModel.ShouldBe(true);
		}

		[Test]
		public void OnNavigatedTo_ContainsInitialStartupWizardModelParameter_WizardModelShouldNotBeNull()
		{
			var wizardModel = new InitialStartupWizardModel();

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);

			_systemUnderTest.OnNavigatedTo(parameters);

			wizardModel.ShouldNotBeNull();
		}

		[Test]
		public void OnNavigatedTo_TriggerCloseInitialStartupWizardAndContainsShouldExitWizardParameter_ShouldGoBack()
		{
			int numberOfCalls = 0;
			bool isContainsShouldExistWizard = false;

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.ShouldExitWizard, "");

			NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				isContainsShouldExistWizard = navParams.ContainsKey(NavigationConstants.ShouldExitWizard);

				return Task.FromResult<INavigationResult>(null);
			};

			_systemUnderTest.OnNavigatedTo(parameters);

			numberOfCalls.ShouldBe(1);
			isContainsShouldExistWizard.ShouldBe(true);
		}

		[Test]
		public void OnNavigatedTo_TriggerCloseInitialStartupWizardAndDoesntContainsShouldExitWizardParameter_ShouldDoNothing()
		{
			int numberOfCalls = 0;
			bool isContainsShouldExistWizard = false;

			INavigationParameters parameters = new NavigationParameters();

			NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				isContainsShouldExistWizard = navParams.ContainsKey(NavigationConstants.ShouldExitWizard);

				return Task.FromResult<INavigationResult>(null);
			};

			_systemUnderTest.OnNavigatedTo(parameters);

			numberOfCalls.ShouldBe(0);
			isContainsShouldExistWizard.ShouldBe(false);
		}

		[Test]
		public void WizardRightButtonCommand_FillTheFieldsAccordingly_ShouldProceed()
		{
			int numberOfCalls = 0;

			NavigationUtilities.NavigateAsync = (navSvc, navPage, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			var wizardModel = new InitialStartupWizardModel
			{
				PumpAndMotorConfigurationSystemMotorModePump1Value = ControlUnitMotorModeValue.Auto,
				PumpAndMotorConfigurationSystemMotorModePump2Value = ControlUnitMotorModeValue.Auto,
				PumpAndMotorConfigurationMotorRateCurrentPump1 = 1,
				PumpAndMotorConfigurationMotorRateCurrentPump2 = 2
			};

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);
			parameters.Add(NavigationConstants.InitialParameters, new List<ParameterTExternalEntity>());

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.WizardRightButtonCommand?.Execute();



			numberOfCalls.ShouldBe(1);
		}

		[Test]
		public void WizardRightButtonCommand_SetSystemMotorModePump1ValueToNotConfigured_ShouldNotProceed()
		{
			var numberOfCalls = 0;

			NavigationUtilities.NavigateAsync = (navSvc, navPage, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			var wizardModel = new InitialStartupWizardModel
			{
				PumpAndMotorConfigurationSystemMotorModePump1Value = ControlUnitMotorModeValue.NotConfigured,
				PumpAndMotorConfigurationSystemMotorModePump2Value = ControlUnitMotorModeValue.Auto,
				PumpAndMotorConfigurationMotorRateCurrentPump1 = 1,
				PumpAndMotorConfigurationMotorRateCurrentPump2 = 2
			};

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.WizardRightButtonCommand?.Execute();


			numberOfCalls.ShouldBe(0);
		}

		[TestCase(ControlUnitMotorModeValue.Auto)]
		[TestCase(ControlUnitMotorModeValue.Inactive)]
		[TestCase(ControlUnitMotorModeValue.Manual)]
		public void WizardRightButtonCommand_SetSystemMotorModePump1ValueToAnyExceptNotConfigured_ShouldProceed(ControlUnitMotorModeValue value)
		{
			var numberOfCalls = 0;

			NavigationUtilities.NavigateAsync = (navSvc, navPage, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			var wizardModel = new InitialStartupWizardModel
			{
				PumpAndMotorConfigurationSystemMotorModePump1Value = value,
				PumpAndMotorConfigurationSystemMotorModePump2Value = ControlUnitMotorModeValue.Auto,
				PumpAndMotorConfigurationMotorRateCurrentPump1 = 1,
				PumpAndMotorConfigurationMotorRateCurrentPump2 = 2
			};

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);
			parameters.Add(NavigationConstants.InitialParameters, new List<ParameterTExternalEntity>());

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.WizardRightButtonCommand?.Execute();


			numberOfCalls.ShouldBe(1);
		}

		[TestCase(ControlUnitMotorModeValue.Auto)]
		[TestCase(ControlUnitMotorModeValue.Inactive)]
		[TestCase(ControlUnitMotorModeValue.Manual)]
		public void WizardRightButtonCommand_SetSystemMotorModePump2ValueToAnyExceptNotConfigured_ShouldProceed(ControlUnitMotorModeValue value)
		{
			var numberOfCalls = 0;

			NavigationUtilities.NavigateAsync = (navSvc, navPage, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			var wizardModel = new InitialStartupWizardModel
				{
					PumpAndMotorConfigurationSystemMotorModePump1Value = ControlUnitMotorModeValue.Auto,
					PumpAndMotorConfigurationSystemMotorModePump2Value = value,
					PumpAndMotorConfigurationMotorRateCurrentPump1 = 1,
					PumpAndMotorConfigurationMotorRateCurrentPump2 = 2
				};

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);
			parameters.Add(NavigationConstants.InitialParameters, new List<ParameterTExternalEntity>());

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.WizardRightButtonCommand?.Execute();


			numberOfCalls.ShouldBe(1);
		}

		[Test]
		public void WizardRightButtonCommand_SetPumpAndMotorConfigurationMotorRateCurrentPump1To0_ShouldNotProceed()
		{
			var numberOfCalls = 0;

			NavigationUtilities.NavigateAsync = (navSvc, navPage, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			var wizardModel = new InitialStartupWizardModel
				{
					PumpAndMotorConfigurationSystemMotorModePump1Value = ControlUnitMotorModeValue.Auto,
					PumpAndMotorConfigurationSystemMotorModePump2Value = ControlUnitMotorModeValue.Auto,
					PumpAndMotorConfigurationMotorRateCurrentPump1 = 0,
					PumpAndMotorConfigurationMotorRateCurrentPump2 = 2,
					PumpAndMotorConfigurationNumberOfPumpValue = ControlUnitSystemDoubleMotorValue.OnePump
				};

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.WizardRightButtonCommand?.Execute();

			numberOfCalls.ShouldBe(0);
		}

		[Test]
		public void WizardRightButtonCommand_SetPumpAndMotorConfigurationMotorRateCurrentPump2To0_ShouldNotProceed()
		{
			var numberOfCalls = 0;

			NavigationUtilities.NavigateAsync = (navSvc, navPage, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			var wizardModel = new InitialStartupWizardModel
				{
					PumpAndMotorConfigurationSystemMotorModePump1Value = ControlUnitMotorModeValue.Auto,
					PumpAndMotorConfigurationSystemMotorModePump2Value = ControlUnitMotorModeValue.Auto,
					PumpAndMotorConfigurationMotorRateCurrentPump1 = 1,
					PumpAndMotorConfigurationMotorRateCurrentPump2 = 0,
					PumpAndMotorConfigurationNumberOfPumpValue = ControlUnitSystemDoubleMotorValue.TwoPumps
				};

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.WizardRightButtonCommand?.Execute();


			numberOfCalls.ShouldBe(0);
		}

		[Test]
		public void WizardRightButtonCommand_SetPumpAndMotorConfigurationMotorRateCurrentPump1GreaterThanTo0_ShouldProceed()
		{
			var numberOfCalls = 0;

			NavigationUtilities.NavigateAsync = (navSvc, navPage, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			var wizardModel = new InitialStartupWizardModel
			{
				PumpAndMotorConfigurationSystemMotorModePump1Value = ControlUnitMotorModeValue.Auto,
				PumpAndMotorConfigurationSystemMotorModePump2Value = ControlUnitMotorModeValue.Auto,
				PumpAndMotorConfigurationMotorRateCurrentPump1 = 10,
				PumpAndMotorConfigurationMotorRateCurrentPump2 = 1,
				PumpAndMotorConfigurationNumberOfPumpValue = ControlUnitSystemDoubleMotorValue.OnePump
			};

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);
			parameters.Add(NavigationConstants.InitialParameters, new List<ParameterTExternalEntity>());

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.WizardRightButtonCommand?.Execute();


			numberOfCalls.ShouldBe(1);
		}

		[Test]
		public void WizardRightButtonCommand_SetPumpAndMotorConfigurationMotorRateCurrentPump2GreaterThanTo0_ShouldProceed()
		{
			var numberOfCalls = 0;

			NavigationUtilities.NavigateAsync = (navSvc, navPage, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			var wizardModel = new InitialStartupWizardModel
				{
					PumpAndMotorConfigurationSystemMotorModePump1Value = ControlUnitMotorModeValue.Auto,
					PumpAndMotorConfigurationSystemMotorModePump2Value = ControlUnitMotorModeValue.Auto,
					PumpAndMotorConfigurationMotorRateCurrentPump1 = 1,
					PumpAndMotorConfigurationMotorRateCurrentPump2 = 10,
					PumpAndMotorConfigurationNumberOfPumpValue = ControlUnitSystemDoubleMotorValue.TwoPumps
				};

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);
			parameters.Add(NavigationConstants.InitialParameters, new List<ParameterTExternalEntity>());

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.WizardRightButtonCommand?.Execute();


			numberOfCalls.ShouldBe(1);
		}

		[Test]
		public void WizardRightButtonCommand_SetOnePumpAndSetCurrentPump2To0_ShouldProceed()
		{
			int numberOfCalls = 0;

			NavigationUtilities.NavigateAsync = (navSvc, navPage, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			var wizardModel = new InitialStartupWizardModel
				{
					PumpAndMotorConfigurationSystemMotorModePump1Value = ControlUnitMotorModeValue.Auto,
					PumpAndMotorConfigurationSystemMotorModePump2Value = ControlUnitMotorModeValue.Auto,
					PumpAndMotorConfigurationMotorRateCurrentPump1 = 1,
					PumpAndMotorConfigurationMotorRateCurrentPump2 = 0,
					PumpAndMotorConfigurationNumberOfPumpValue = ControlUnitSystemDoubleMotorValue.OnePump
				};

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);
			parameters.Add(NavigationConstants.InitialParameters, new List<ParameterTExternalEntity>());

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.WizardRightButtonCommand?.Execute();


			numberOfCalls.ShouldBe(1);
		}

		[Test]
		public void WizardRightButtonCommand_SetOnePumpAndMotorModePump2ValueToNotConfigured_ShouldProceed()
		{
			var numberOfCalls = 0;

			NavigationUtilities.NavigateAsync = (navSvc, navPage, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			var wizardModel = new InitialStartupWizardModel
				{
					PumpAndMotorConfigurationSystemMotorModePump1Value = ControlUnitMotorModeValue.Auto,
					PumpAndMotorConfigurationSystemMotorModePump2Value = ControlUnitMotorModeValue.NotConfigured,
					PumpAndMotorConfigurationMotorRateCurrentPump1 = 1,
					PumpAndMotorConfigurationMotorRateCurrentPump2 = 0,
					PumpAndMotorConfigurationNumberOfPumpValue = ControlUnitSystemDoubleMotorValue.OnePump
				};

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);
			parameters.Add(NavigationConstants.InitialParameters, new List<ParameterTExternalEntity>());

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.WizardRightButtonCommand?.Execute();


			numberOfCalls.ShouldBe(1);
		}
	}
}
