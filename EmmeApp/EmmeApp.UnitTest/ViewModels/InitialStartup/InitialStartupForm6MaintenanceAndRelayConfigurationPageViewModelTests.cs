﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Entities.External;
using EmmeApp.Helpers;
using Moq;
using NUnit.Framework;
using Plugin.Messaging;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using Shouldly;
using System.Collections.Generic;
using System.Threading.Tasks;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Managers.Abstractions;

namespace EmmeApp.ViewModels
{
    [TestFixture]
    public class InitialStartupForm6MaintenanceAndRelayConfigurationPageViewModelTests
    {
        private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);
        private Mock<INavigationService> _mockNavigationService;
        private Mock<INavigationHelperService> _mockNavigationHelperService;
        private Mock<IUserDialogs> _mockUserDialogs;
        private Mock<IPopupNavigation> _mockPopupNavigation;
        private Mock<IMessaging> _mockMessagingPlugin;
        private Mock<IFileService> _mockFileService;
        private Mock<IEnumProvider> _mockEnumHelper;
        private Mock<IAppCenterCustomService> _mockAppCenterService;
        private Mock<IBleManager> _mockBleManager;
        private Mock<IConfigurationControlUnitManager> _mockConfigurationControlUnitManager;
        private Mock<ICockpitControlUnitManager> _mockCockpitControlUnitManager;
        private Mock<ILogControlUnitManager> _mockLogControlUnitManager;

		private InitialStartupForm6MaintenanceAndRelayConfigurationPageViewModel _systemUnderTest;

        [SetUp]
        public void Setup()
        {
            _mockRepository.Create<INavigationService>();
            _mockNavigationService = _mockRepository.Create<INavigationService>();
            _mockNavigationHelperService = _mockRepository.Create<INavigationHelperService>();
            _mockUserDialogs = _mockRepository.Create<IUserDialogs>();
            _mockPopupNavigation = _mockRepository.Create<IPopupNavigation>();
            _mockMessagingPlugin = _mockRepository.Create<IMessaging>();
            _mockFileService = _mockRepository.Create<IFileService>();
            _mockEnumHelper = _mockRepository.Create<IEnumProvider>();
            _mockAppCenterService = _mockRepository.Create<IAppCenterCustomService>();
            _mockBleManager = _mockRepository.Create<IBleManager>();
            _mockConfigurationControlUnitManager = _mockRepository.Create<IConfigurationControlUnitManager>();
			_mockCockpitControlUnitManager = _mockRepository.Create<ICockpitControlUnitManager>();
			_mockLogControlUnitManager = _mockRepository.Create<ILogControlUnitManager>();

			_mockEnumHelper.Setup(o => o.PopulateList<ControlUnitCustomRelayConfigurationValue>())
                .Returns(new List<ControlUnitCustomRelayConfigurationValue>()
                {
                    ControlUnitCustomRelayConfigurationValue.CMN_M2M_ALARM_MOTOR_0,
                    ControlUnitCustomRelayConfigurationValue.CMN_M2M_ALARM_MOTOR_1,
                    ControlUnitCustomRelayConfigurationValue.CMN_M2M_ALARM_MOTOR_0_1,
                    ControlUnitCustomRelayConfigurationValue.CMN_M2M_LEVEL_HIGH,
                    ControlUnitCustomRelayConfigurationValue.CMN_M2M_OPERATING_MOTOR_0,
                    ControlUnitCustomRelayConfigurationValue.CMN_M2M_OPERATING_MOTOR_1,
                    ControlUnitCustomRelayConfigurationValue.CMN_M2M_OPERATING_MOTOR_0_1,
                    ControlUnitCustomRelayConfigurationValue.CMN_M2M_SERVICE,
                    ControlUnitCustomRelayConfigurationValue.CMN_M2M_WARNING,
                });

            _systemUnderTest = new InitialStartupForm6MaintenanceAndRelayConfigurationPageViewModel(
                _mockNavigationService.Object,
                _mockNavigationHelperService.Object,
                _mockAppCenterService.Object,
                _mockUserDialogs.Object,
                _mockPopupNavigation.Object,
                _mockMessagingPlugin.Object,
                _mockFileService.Object,
                _mockEnumHelper.Object,
                _mockBleManager.Object,
                _mockConfigurationControlUnitManager.Object,
				_mockCockpitControlUnitManager.Object,
				_mockLogControlUnitManager.Object);
        }

        [Test]
        public void Ctor_Always_PerformsExpectedWork()
        {
            _mockNavigationService.Verify();
            _mockNavigationHelperService.Verify();
            _mockPopupNavigation.Verify();
            _mockUserDialogs.Verify();
            _mockMessagingPlugin.Verify();
            _mockFileService.Verify();
            _mockRepository.Verify();
        }

        [Test]
        public void OnNavigatedTo_ContainsInitialStartupWizardModelParameter_WizardModelShouldNotBeNull()
        {
            var wizardModel = new InitialStartupWizardModel();

            INavigationParameters parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);

            _systemUnderTest.OnNavigatedTo(parameters);

            wizardModel.ShouldNotBeNull();
        }

        [Test]
        public void OnNavigatedTo_TriggerCloseInitialStartupWizardAndContainsShouldExitWizardParameter_ShouldGoBack()
        {
            var numberOfCalls = 0;
            var isContainsShouldExistWizard = false;

            INavigationParameters parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.ShouldExitWizard, "");

            NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
            {
                ++numberOfCalls;
                isContainsShouldExistWizard = navParams.ContainsKey(NavigationConstants.ShouldExitWizard);

                return Task.FromResult<INavigationResult>(null);
            };

            _systemUnderTest.OnNavigatedTo(parameters);

            numberOfCalls.ShouldBe(1);
            isContainsShouldExistWizard.ShouldBe(true);
        }

        [Test]
        public void OnNavigatedTo_TriggerCloseInitialStartupWizardAndDoesntContainsShouldExitWizardParameter_ShouldDoNothing()
        {
            var numberOfCalls = 0;
            var isContainsShouldExistWizard = false;

            INavigationParameters parameters = new NavigationParameters();

            NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
            {
                ++numberOfCalls;
                isContainsShouldExistWizard = navParams.ContainsKey(NavigationConstants.ShouldExitWizard);

                return Task.FromResult<INavigationResult>(null);
            };

            _systemUnderTest.OnNavigatedTo(parameters);

            numberOfCalls.ShouldBe(0);
            isContainsShouldExistWizard.ShouldBe(false);
        }

        [Test]
        public void WizardLeftButtonCommand_WhenLeftButtonPressed_ShouldContainsWizardModel()
        {
            var numberOfCalls = 0;
            var isContainsWizardModel = false;

            NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
            {
                ++numberOfCalls;
                isContainsWizardModel = navParams.ContainsKey(NavigationConstants.InitialStartupWizardModel);
                return Task.FromResult<INavigationResult>(null);
            };

            _systemUnderTest.WizardLeftButtonCommand?.Execute();

            numberOfCalls.ShouldBe(1);
            isContainsWizardModel.ShouldBe(true);
        }

        [Test]
        public void WizardRightButtonCommand_FillTheFieldsAccordingly_ShouldProceed()
        {
            var wizardModel = new InitialStartupWizardModel
            {
                RelayConfigurationRelay0Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_ALARM_MOTOR_0,
                RelayConfigurationRelay1Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_ALARM_MOTOR_0,
                RelayConfigurationRelay2Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_ALARM_MOTOR_0
            };

            INavigationParameters parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);
			parameters.Add(NavigationConstants.InitialParameters, new List<ParameterTExternalEntity>());

            _systemUnderTest.OnNavigatedTo(parameters);
            _systemUnderTest.WizardRightButtonCommand?.Execute();

            _systemUnderTest.WizardErrorMessage.ShouldBe(string.Empty);
            _systemUnderTest.WizardErrorMessageVisibility.ShouldBe(false);
        }

        //[Test]
        //public void WizardRightButtonCommand_SetRelayConfigurationRelay0ValueToNotConfigured_ShouldNotProceed()
        //{
        //	InitialStartupWizardModel wizardModel = new InitialStartupWizardModel();
        //	wizardModel.RelayConfigurationRelay0Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_NOT_CONFIGURED;
        //	wizardModel.RelayConfigurationRelay1Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_ALARM_MOTOR_0;
        //	wizardModel.RelayConfigurationRelay2Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_ALARM_MOTOR_0;


        //	INavigationParameters parameters = new NavigationParameters();
        //	parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);

        //	_systemUnderTest.OnNavigatingTo(parameters);
        //	_systemUnderTest.OnNavigatedTo(parameters);
        //	_systemUnderTest.WizardRightButtonCommand?.Execute();

        //	_systemUnderTest.WizardErrorMessage.ShouldNotBe(string.Empty);
        //	_systemUnderTest.WizardErrorMessageVisibility.ShouldBe(true);
        //}

        //[Test]
        //public void WizardRightButtonCommand_SetRelayConfigurationRelay1ValueToNotConfigured_ShouldNotProceed()
        //{
        //	InitialStartupWizardModel wizardModel = new InitialStartupWizardModel();
        //	wizardModel.RelayConfigurationRelay0Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_ALARM_MOTOR_0;
        //	wizardModel.RelayConfigurationRelay1Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_NOT_CONFIGURED;
        //	wizardModel.RelayConfigurationRelay2Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_ALARM_MOTOR_0;


        //	INavigationParameters parameters = new NavigationParameters();
        //	parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);

        //	_systemUnderTest.OnNavigatingTo(parameters);
        //	_systemUnderTest.OnNavigatedTo(parameters);
        //	_systemUnderTest.WizardRightButtonCommand?.Execute();

        //	_systemUnderTest.WizardErrorMessage.ShouldNotBe(string.Empty);
        //	_systemUnderTest.WizardErrorMessageVisibility.ShouldBe(true);
        //}

        //[Test]
        //public void WizardRightButtonCommand_SetRelayConfigurationRelay2ValueToNotConfigured_ShouldNotProceed()
        //{
        //	InitialStartupWizardModel wizardModel = new InitialStartupWizardModel();
        //	wizardModel.RelayConfigurationRelay0Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_ALARM_MOTOR_0;
        //	wizardModel.RelayConfigurationRelay1Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_ALARM_MOTOR_0;
        //	wizardModel.RelayConfigurationRelay2Value = ControlUnitCustomRelayConfigurationValue.CMN_M2M_NOT_CONFIGURED;


        //	INavigationParameters parameters = new NavigationParameters();
        //	parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);

        //	_systemUnderTest.OnNavigatingTo(parameters);
        //	_systemUnderTest.OnNavigatedTo(parameters);
        //	_systemUnderTest.WizardRightButtonCommand?.Execute();

        //	_systemUnderTest.WizardErrorMessage.ShouldNotBe(string.Empty);
        //	_systemUnderTest.WizardErrorMessageVisibility.ShouldBe(true);
        //}

    }
}
