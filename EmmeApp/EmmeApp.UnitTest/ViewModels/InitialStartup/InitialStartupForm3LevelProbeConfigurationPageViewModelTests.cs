﻿using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Helpers;
using Moq;
using NUnit.Framework;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using Rg.Plugins.Popup.Pages;
using Shouldly;
using System.Collections.Generic;
using System.Threading.Tasks;
using EmmeApp.Common.Utilities.Abstractions;

namespace EmmeApp.ViewModels
{
	[TestFixture]
    public class InitialStartupForm3LevelProbeConfigurationPageViewModelTests
    {
        private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);
        private Mock<INavigationService> _mockNavigationService;
        private Mock<INavigationHelperService> _mockNavigationHelperService;
        private Mock<IPopupNavigation> _mockPopupNavigation;
		private Mock<IAppCenterCustomService> _mockAppCenterService;
		private Mock<IEnumProvider> _mockEnumHelper;

		private InitialStartupForm3LevelProbeConfigurationPageViewModel _systemUnderTest;

        [SetUp]
        public void Setup()
        {
            _mockNavigationService = _mockRepository.Create<INavigationService>();
            _mockNavigationHelperService = _mockRepository.Create<INavigationHelperService>();
            _mockPopupNavigation = _mockRepository.Create<IPopupNavigation>();
			_mockEnumHelper = _mockRepository.Create<IEnumProvider>();
			_mockAppCenterService = _mockRepository.Create<IAppCenterCustomService>();
			_mockEnumHelper.Setup(o => o.PopulateList<LevelProbeConfigurationValue>())
				.Returns(new List<LevelProbeConfigurationValue>()
				{
					LevelProbeConfigurationValue.ThreeLevelswitches,
					LevelProbeConfigurationValue.FourLevelswitches,
					LevelProbeConfigurationValue.Analogue,
				});

			_mockPopupNavigation.Setup(o => o.PushAsync(It.IsAny<PopupPage>(), It.IsAny<bool>()));

			_systemUnderTest = new InitialStartupForm3LevelProbeConfigurationPageViewModel(
                _mockNavigationService.Object,
                _mockNavigationHelperService.Object,
				_mockAppCenterService.Object,
                _mockPopupNavigation.Object,
				_mockEnumHelper.Object);
        }

        [Test]
        public void Ctor_Always_PerformsExpectedWork()
        {
            _mockNavigationService.Verify();
            _mockNavigationHelperService.Verify();
            _mockPopupNavigation.Verify();
            _mockRepository.Verify();
        }

        [Test]
        public void WizardLeftButtonCommand_WhenLeftButtonPressed_ShouldGoBack()
        {
            int numberOfCalls = 0;

            NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) => {
                ++numberOfCalls;
                return Task.FromResult<INavigationResult>(null);
            };

            _systemUnderTest.WizardLeftButtonCommand?.Execute();

            numberOfCalls.ShouldBe(1);
        }        

        [Test]
        public void WizardRightButtonCommand_LevelProbeConfigurationPlausibilityCheckWhenRightButtonPressed_ShouldHaveNoError()
        {
            _systemUnderTest.WizardErrorMessage = string.Empty;
            _systemUnderTest.WizardErrorMessageVisibility = false;
            _systemUnderTest.SelectedLevelProbeConfiguration = "3 Levelswithches";

            _systemUnderTest.WizardRightButtonCommand?.Execute();


            _systemUnderTest.WizardErrorMessage.ShouldBe(string.Empty);
            _systemUnderTest.WizardErrorMessageVisibility.ShouldBe(false);
        }

        [Test]
        public void WizardRightButtonCommand_LevelProbeConfigurationPlausibilityCheckWhenRightButtonPressed_ShouldHaveError()
        {
            _systemUnderTest.WizardErrorMessage = string.Empty;
            _systemUnderTest.WizardErrorMessageVisibility = false;
            _systemUnderTest.SelectedLevelProbeConfiguration = string.Empty;

            _systemUnderTest.WizardRightButtonCommand?.Execute();

            _systemUnderTest.ShowSliders.ShouldBe(false);
            _systemUnderTest.WizardErrorMessage.ShouldNotBe(string.Empty);
            _systemUnderTest.WizardErrorMessageVisibility.ShouldBe(true);
            _systemUnderTest.LevelProbeConfigurationHasError.ShouldBe(true);
        }

        [Test]
        public void WizardRightButtonCommand_LevelSwitchesPlausibilityCheckWhenNotAnalogue_ShouldHaveNoErrors()
        {
            _systemUnderTest.WizardErrorMessage = string.Empty;
            _systemUnderTest.WizardErrorMessageVisibility = false;
            _systemUnderTest.SelectedLevelProbeConfiguration = "3 Levelswithches";

            _systemUnderTest.WizardRightButtonCommand?.Execute();

            _systemUnderTest.ShowSliders.ShouldBe(false);
            _systemUnderTest.WizardErrorMessage.ShouldBe(string.Empty);
            _systemUnderTest.WizardErrorMessageVisibility.ShouldBe(false);
            _systemUnderTest.Levelswitch2HasError.ShouldBe(false);
            _systemUnderTest.Levelswitch1HasError.ShouldBe(false);
        }

        [Test]
        public void WizardRightButtonCommand_LevelSwitch2PlausibilityCheckWhenAnalogue_ShouldHaveErrors()
        {
            _systemUnderTest.WizardErrorMessage = string.Empty;
            _systemUnderTest.WizardErrorMessageVisibility = false;
            _systemUnderTest.SelectedLevelProbeConfiguration = "Analogue";
            _systemUnderTest.WizardModel = new InitialStartupWizardModel
            {
                LevelProbeConfigurationLevelAnalogThreshold2 = 3,
                LevelProbeConfigurationLevelAnalogThreshold1 = 4,
                LevelProbeConfigurationLevelAnalogThreshold0 = 3.5
            };

            _systemUnderTest.WizardRightButtonCommand?.Execute();


            _systemUnderTest.ShowSliders.ShouldBe(true);
            _systemUnderTest.WizardErrorMessage.ShouldNotBe(string.Empty);
            _systemUnderTest.WizardErrorMessageVisibility.ShouldBe(true);
            _systemUnderTest.Levelswitch2HasError.ShouldBe(true);
            _systemUnderTest.Levelswitch1HasError.ShouldBe(false);
        }

        [Test]
        public void WizardRightButtonCommand_LevelSwitch1PlausibilityCheckWhenAnalogue_ShouldHaveErrors()
        {
            _systemUnderTest.WizardErrorMessage = string.Empty;
            _systemUnderTest.WizardErrorMessageVisibility = false;
            _systemUnderTest.SelectedLevelProbeConfiguration = "Analogue";
            _systemUnderTest.WizardModel = new InitialStartupWizardModel
            {
                LevelProbeConfigurationLevelAnalogThreshold2 = 4,
                LevelProbeConfigurationLevelAnalogThreshold1 = 3,
                LevelProbeConfigurationLevelAnalogThreshold0 = 3.5
            };

            _systemUnderTest.WizardRightButtonCommand?.Execute();


            _systemUnderTest.ShowSliders.ShouldBe(true);
            _systemUnderTest.WizardErrorMessage.ShouldNotBe(string.Empty);
            _systemUnderTest.WizardErrorMessageVisibility.ShouldBe(true);
            _systemUnderTest.Levelswitch2HasError.ShouldBe(false);
            _systemUnderTest.Levelswitch1HasError.ShouldBe(true);
        }

        [Test]
        public void OnNavigatedTo_HasShouldExitWizardParameter_ShouldGoBack()
        {
            var numberOfCalls = 0;

            var parameter = new NavigationParameters {{NavigationConstants.ShouldExitWizard, ""}};
            _mockNavigationHelperService.Setup(o => o.GetNavigationMode(parameter)).Returns(NavigationMode.New);

            NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) => {
                ++numberOfCalls;
                return Task.FromResult<INavigationResult>(null);
            };

            _systemUnderTest.OnNavigatedTo(parameter);

            numberOfCalls.ShouldBe(1);
        }

        [Test]
        public void OnNavigatedTo_HasWizardModelParameter_ShouldInitializeWizardModel()
        {
            var model = new InitialStartupWizardModel();
            var parameter = new NavigationParameters {{NavigationConstants.InitialStartupWizardModel, model}};

            _mockNavigationHelperService.Setup(o => o.GetNavigationMode(parameter)).Returns(NavigationMode.New);

            NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) => Task.FromResult<INavigationResult>(null);

            _systemUnderTest.OnNavigatedTo(parameter);

            _systemUnderTest.WizardModel.ShouldNotBeNull();
        }
    }
}
