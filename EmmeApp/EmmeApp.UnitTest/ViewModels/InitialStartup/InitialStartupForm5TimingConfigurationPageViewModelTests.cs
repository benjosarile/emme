﻿using EmmeApp.Common.Constants;
using EmmeApp.Helpers;
using Moq;
using NUnit.Framework;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using Shouldly;
using System.Threading.Tasks;
using EmmeApp.Common.Utilities.Abstractions;

namespace EmmeApp.ViewModels
{
    [TestFixture]
    public class InitialStartupForm5TimingConfigurationPageViewModelTests
    {
        private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);
        private Mock<INavigationService> _mockNavigationService;
        private Mock<INavigationHelperService> _mockNavigationHelperService;
        private Mock<IPopupNavigation> _mockPopupNavigation;
		private Mock<IAppCenterCustomService> _mockAppCenterService;
		private Mock<IEnumProvider> _mockEnumProvider;
		private InitialStartupForm5TimingConfigurationPageViewModel _systemUnderTest;

        [SetUp]
        public void Setup()
        {
            _mockNavigationService = _mockRepository.Create<INavigationService>();
            _mockNavigationHelperService = _mockRepository.Create<INavigationHelperService>();
            _mockPopupNavigation = _mockRepository.Create<IPopupNavigation>();
			_mockAppCenterService = _mockRepository.Create<IAppCenterCustomService>();
			_mockEnumProvider = _mockRepository.Create<IEnumProvider>();

            _systemUnderTest = new InitialStartupForm5TimingConfigurationPageViewModel(
                _mockNavigationService.Object,
                _mockNavigationHelperService.Object,
				_mockAppCenterService.Object,
                _mockPopupNavigation.Object,
				_mockEnumProvider.Object);
        }

        [Test]
        public void Ctor_Always_PerformsExpectedWork()
        {
            _mockNavigationService.Verify();
            _mockNavigationHelperService.Verify();
            _mockPopupNavigation.Verify();
            _mockRepository.Verify();
        }

        [Test]
        public void WizardLeftButtonCommand_WhenLeftButtonPressed_ShouldGoBack()
        {
            var numberOfCalls = 0;

            NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) => {
                ++numberOfCalls;
                return Task.FromResult<INavigationResult>(null);
            };

            _systemUnderTest.WizardLeftButtonCommand?.Execute();

            numberOfCalls.ShouldBe(1);
        }

        [Test]
        public void WizardRightButtonCommand_WhenRightButtonPressed_ShouldAlwaysProceed()
        {
            _systemUnderTest.WizardErrorMessage = string.Empty;
            _systemUnderTest.WizardErrorMessageVisibility = false;

            _systemUnderTest.WizardRightButtonCommand?.Execute();

            _systemUnderTest.WizardErrorMessage.ShouldBe(string.Empty);
            _systemUnderTest.WizardErrorMessageVisibility.ShouldBe(false);
        }

        [Test]
        public void OnNavigatingTo_ParametersContainsShouldExitWizard_ShouldGoBack()
        {
            var numberOfCalls = 0;
            var isContainsShouldExistWizard = false;

            INavigationParameters parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.ShouldExitWizard, "");

            NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
            {
                ++numberOfCalls;
                isContainsShouldExistWizard = navParams.ContainsKey(NavigationConstants.ShouldExitWizard);

                return Task.FromResult<INavigationResult>(null);
            };

            _systemUnderTest.OnNavigatedTo(parameters);

            numberOfCalls.ShouldBe(1);
            isContainsShouldExistWizard.ShouldBe(true);
        }
    }
}
