﻿using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Helpers;
using Moq;
using NUnit.Framework;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using Shouldly;
using System.Collections.Generic;
using System.Threading.Tasks;
using EmmeApp.Common.Utilities.Abstractions;

namespace EmmeApp.ViewModels
{
	[TestFixture]
	public class InitialStartupForm2SystemDataConfigurationPageViewModelTests
	{
		private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);
		private Mock<INavigationService> _mockNavigationService;
		private Mock<INavigationHelperService> _mockNavigationHelperService;
		private Mock<IPopupNavigation> _mockPopupNavigation;
		private Mock<IKeyboardHelper> _mockKeyboardHelper;
		private Mock<IEnumProvider> _mockEnumHelper;
		private Mock<IAppCenterCustomService> _mockAppCenterService;

		private InitialStartupForm2SystemDataConfigurationPageViewModel _systemUnderTest;

		[SetUp]
		public void Setup()
		{
			_mockNavigationService = _mockRepository.Create<INavigationService>();
			_mockNavigationHelperService = _mockRepository.Create<INavigationHelperService>();
			_mockPopupNavigation = _mockRepository.Create<IPopupNavigation>();
			_mockKeyboardHelper = _mockRepository.Create<IKeyboardHelper>();
			_mockEnumHelper = _mockRepository.Create<IEnumProvider>();
			_mockAppCenterService = _mockRepository.Create<IAppCenterCustomService>();

			
			_mockKeyboardHelper.Setup(o => o.HideKeyboard());

			_mockEnumHelper.Setup(o => o.PopulateList<ControlUnitSystemTypeValue>())
				.Returns(new List<ControlUnitSystemTypeValue>()
				{
					ControlUnitSystemTypeValue.ReservoirSystem,
					ControlUnitSystemTypeValue.ShaftSystem
				});

			_systemUnderTest = new InitialStartupForm2SystemDataConfigurationPageViewModel(
				_mockNavigationService.Object,
				_mockNavigationHelperService.Object,
				_mockAppCenterService.Object,
				_mockPopupNavigation.Object,
				_mockKeyboardHelper.Object,
				_mockEnumHelper.Object);
		}


		[Test]
		public void Ctor_Always_PerformsExpectedWork()
		{
			_mockRepository.Verify();
		}

		[Test]
		public void OnNavigatedTo_ParametersContainsInitialStartupWizardModel_WizardModelShouldNotBeNull()
		{
			InitialStartupWizardModel wizardModel = new InitialStartupWizardModel();

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);

			_systemUnderTest.OnNavigatedTo(parameters);

			wizardModel.ShouldNotBeNull();
		}

		[Test]
		public void OnNavigatedTo_ParametersContainsShouldExitWizard_ShouldGoBack()
		{
			int numberOfCalls = 0;
			bool isContainsShouldExistWizard = false;

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.ShouldExitWizard, "");

			NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				isContainsShouldExistWizard = navParams.ContainsKey(NavigationConstants.ShouldExitWizard);

				return Task.FromResult<INavigationResult>(null);
			};

			_systemUnderTest.OnNavigatedTo(parameters);

			numberOfCalls.ShouldBe(1);
			isContainsShouldExistWizard.ShouldBe(true);
		}

		[Test]
		public void OnNavigatedTo_TriggerCloseInitialStartupWizardAndParameterDoesntContainsShouldExitWizardParameter_ShouldDoNothing()
		{
			int numberOfCalls = 0;
			bool isContainsShouldExistWizard = false;

			INavigationParameters parameters = new NavigationParameters();

			NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				isContainsShouldExistWizard = navParams.ContainsKey(NavigationConstants.ShouldExitWizard);

				return Task.FromResult<INavigationResult>(null);
			};

			_systemUnderTest.OnNavigatedTo(parameters);

			numberOfCalls.ShouldBe(0);
			isContainsShouldExistWizard.ShouldBe(false);
		}

		[Test]
		public void WizardLeftButtonCommand_WhenLeftButtonPressed_ShouldContainsWizardModel()
		{
			int numberOfCalls = 0;
			bool isContainsWizardModel = false;

			NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				isContainsWizardModel = navParams.ContainsKey(NavigationConstants.InitialStartupWizardModel);
				return Task.FromResult<INavigationResult>(null);
			};

			_systemUnderTest.WizardLeftButtonCommand?.Execute();

			numberOfCalls.ShouldBe(1);
			isContainsWizardModel.ShouldBe(true);
		}

		[TestCase("Shaft System", ControlUnitSystemTypeValue.ShaftSystem)]
		[TestCase("Reservoir System", ControlUnitSystemTypeValue.ReservoirSystem)]
		public void SelectSystemTypeCommand_SetValidSystemType_AlwaysSucceed(string systemType, ControlUnitSystemTypeValue value)
		{
			InitialStartupWizardModel wizardModel = new InitialStartupWizardModel();

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.SelectSystemTypeCommand.Execute(systemType);
			_systemUnderTest.WizardModel.SystemDataConfigurationSystemType.ShouldBe(value);
		}

		[TestCase("Drive Shaft")]
		[TestCase("Drive Reservoir System")]
		public void SelectSystemTypeCommand_SetInvalidSystemType_SetSystemDataConfigurationSystemTypeToNotConfigured(string systemType)
		{
			InitialStartupWizardModel wizardModel = new InitialStartupWizardModel();

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.SelectSystemTypeCommand.Execute(systemType);
			_systemUnderTest.WizardModel.SystemDataConfigurationSystemType.ShouldBe(ControlUnitSystemTypeValue.NotConfigured);
		}

		[Test]
		public void WizardRightButtonCommand_SystemNameHasError_ShouldNotProceed()
		{
			InitialStartupWizardModel wizardModel = new InitialStartupWizardModel
 {
	 SystemDataConfigurationSystemName = "Adolph Blaine Charles David Earl Frederick Gerald Hubert Irvin John Kenneth Lloyd Martin Nero Oliver Paul Quincy Randolph Sherman Thomas Uncas Victor William Xerxes Yancy Wolfeschlegelsteinhausenbergerdorff, Senior"
 };

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.SystemNameHasError = true;
			_systemUnderTest.WizardRightButtonCommand?.Execute();

			_systemUnderTest.SystemNameHasError.ShouldBe(true);
		}

		[Test]
		public void WizardRightButtonCommand_Motor1NameHasError_ShouldNotProceed()
		{
			var wizardModel = new InitialStartupWizardModel
			{
				SystemDataConfigurationMotor1Name =
					"Adolph Blaine Charles David Earl Frederick Gerald Hubert Irvin John Kenneth Lloyd Martin Nero Oliver Paul Quincy Randolph Sherman Thomas Uncas Victor William Xerxes Yancy Wolfeschlegelsteinhausenbergerdorff, Senior"
			};

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.Motor1NameHasError = true;
			_systemUnderTest.WizardRightButtonCommand?.Execute();
			_systemUnderTest.Motor1NameHasError.ShouldBe(true);
		}


		[Test]
		public void WizardRightButtonCommand_Motor2NameHasError_ShouldNotProceed()
		{
			var wizardModel = new InitialStartupWizardModel
			{
				SystemDataConfigurationMotor2Name = "Adolph Blaine Charles David Earl Frederick Gerald Hubert Irvin John Kenneth Lloyd Martin Nero Oliver Paul Quincy Randolph Sherman Thomas Uncas Victor William Xerxes Yancy Wolfeschlegelsteinhausenbergerdorff, Senior"
			};

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.Motor2NameHasError = true;
			_systemUnderTest.WizardRightButtonCommand?.Execute();
			_systemUnderTest.Motor2NameHasError.ShouldBe(true);
		}

		[Test]
		public void WizardRightButtonCommand_FillTheFieldsAccordingly_ShouldAlwaysProceed()
		{
			int numberOfCalls = 0;

			NavigationUtilities.NavigateAsync = (navSvc, navPage, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			var wizardModel = new InitialStartupWizardModel
			{
				SystemDataConfigurationSystemType = ControlUnitSystemTypeValue.ShaftSystem
			};

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.InitialStartupWizardModel, wizardModel);

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.WizardRightButtonCommand?.Execute();



			numberOfCalls.ShouldBe(1);
		}

	}
}
