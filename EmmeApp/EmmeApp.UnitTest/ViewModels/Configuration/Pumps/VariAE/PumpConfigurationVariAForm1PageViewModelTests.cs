﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Configuration;
using EmmeApp.Entities.External;
using EmmeApp.Helpers;
using Moq;
using NUnit.Framework;
using Prism.Navigation;
using Rg.Plugins.Popup.Contracts;
using Rg.Plugins.Popup.Pages;
using Shouldly;
using System.Collections.Generic;
using System.Threading.Tasks;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Managers.Abstractions;
using Prism.Events;

namespace EmmeApp.ViewModels
{
    public class PumpConfigurationVariAForm1PageViewModelTests
    {
        private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);
        private Mock<INavigationService> _mockNavigationService;
        private Mock<INavigationHelperService> _mockNavigationHelperService;
        private Mock<IAppCenterCustomService> _mockAppCenterService;
        private Mock<IUserDialogs> _mockUserDialogs;
        private Mock<IEnumProvider> _mockEnumHelper;
        private Mock<IPopupNavigation> _mockPopupNavigation;
        private Mock<IKeyboardHelper> _mockKeyboardHelper;
        private Mock<IUnixTimestampService> _mockUnixTimestampService;
        private Mock<IBleManager> _mockBleManager;
        private Mock<IConfigurationPumpVariAManager> _mockConfigurationPumpVariAManager;
		private Mock<ICockpitPumpsVariAEManager> _mockCockpitPumpsVariAEManager;
		private Mock<ILogPumpManager> _mockLogPumpManager;
        private Mock<IEventAggregator> _mockEventAggregator;
		private PumpConfigurationVariAForm1PageViewModel _systemUnderTest;

        [SetUp]
        public void Setup()
        {
            _mockRepository.Create<INavigationService>();
            _mockNavigationService = _mockRepository.Create<INavigationService>();
            _mockNavigationHelperService = _mockRepository.Create<INavigationHelperService>();
            _mockAppCenterService = _mockRepository.Create<IAppCenterCustomService>();
            _mockUserDialogs = _mockRepository.Create<IUserDialogs>();
            _mockEnumHelper = _mockRepository.Create<IEnumProvider>();
            _mockPopupNavigation = _mockRepository.Create<IPopupNavigation>();
            _mockKeyboardHelper = _mockRepository.Create<IKeyboardHelper>();
            _mockUnixTimestampService = _mockRepository.Create<IUnixTimestampService>();
            _mockBleManager = _mockRepository.Create<IBleManager>();
            _mockConfigurationPumpVariAManager = _mockRepository.Create<IConfigurationPumpVariAManager>();
			_mockCockpitPumpsVariAEManager = _mockRepository.Create<ICockpitPumpsVariAEManager>();
			_mockLogPumpManager = _mockRepository.Create<ILogPumpManager>();
            _mockEventAggregator = _mockRepository.Create<IEventAggregator>();
            
			_mockEnumHelper.Setup(e => e.PopulateList<PumpControlTypeValue>()).Returns(new List<PumpControlTypeValue>()
                {
                    PumpControlTypeValue.ConstantPressure,
                    PumpControlTypeValue.ConstantSpeed,
                    PumpControlTypeValue.ProportionalPressure
                });

            _mockEnumHelper.Setup(e => e.PopulateList<PumpTypeOfOperationValue>()).Returns(new List<PumpTypeOfOperationValue>()
                {
                    PumpTypeOfOperationValue.MAX,
                    PumpTypeOfOperationValue.ON,
                });

            _mockEnumHelper.Setup(e => e.PopulateList<PumpSignalOriginValue>()).Returns(new List<PumpSignalOriginValue>()
                {
                    PumpSignalOriginValue.Pump,
                    PumpSignalOriginValue.Remote,
                });

            _mockKeyboardHelper.Setup(k => k.HideKeyboard());
            _mockPopupNavigation.Setup(o => o.PushAsync(It.IsAny<PopupPage>(), It.IsAny<bool>()));

            _systemUnderTest = new PumpConfigurationVariAForm1PageViewModel(
                            _mockNavigationService.Object,
                            _mockNavigationHelperService.Object,
                            _mockAppCenterService.Object,
                            _mockUserDialogs.Object,
                            _mockEventAggregator.Object,
                            _mockEnumHelper.Object,
                            _mockPopupNavigation.Object,
                            _mockKeyboardHelper.Object,
                            _mockUnixTimestampService.Object,
                            _mockBleManager.Object,
                            _mockConfigurationPumpVariAManager.Object,
							_mockCockpitPumpsVariAEManager.Object,
							_mockLogPumpManager.Object);
        }
       
        [Test]
        public void Ctor_Always_PerformsExpectedWork()
        {
            _mockNavigationService.Verify();
            _mockNavigationHelperService.Verify();
            _mockAppCenterService.Verify();
            _mockUserDialogs.Verify();
            _mockEnumHelper.Verify();
            _mockPopupNavigation.Verify();
            _mockKeyboardHelper.Verify();
            _mockUnixTimestampService.Verify();
            _mockBleManager.Verify();
            _mockConfigurationPumpVariAManager.Verify();
        }

        [Test]
        public void OnNavigatedTo_HasShouldExitWizardParameter_ShouldGoBack()
        {
            var numberOfCalls = 0;

            var parameter = new NavigationParameters {{NavigationConstants.ShouldExitWizard, ""}};

            NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) => {
                ++numberOfCalls;
                return Task.FromResult<INavigationResult>(null);
            };

            _systemUnderTest.OnNavigatedTo(parameter);

            numberOfCalls.ShouldBe(1);
        }

        [Test]
        public void OnNavigatedTo_HasInitialParameter_ParameterShouldNotBeNull()
        {
            var initialParams = new List<ParameterTExternalEntity>();
            INavigationParameters parameter = new NavigationParameters();
            parameter.Add(NavigationConstants.InitialParameters, initialParams);

            _systemUnderTest.OnNavigatedTo(parameter);

            _systemUnderTest.InitialParameters.ShouldNotBeNull();
        }

        [Test]
        public void OnNavigatedTo_HasPumpModulAConfigurationParameter_ModelShouldNotBeNull()
        {
            var model = new PumpConfigurationVariAWizardModel();
            INavigationParameters parameter = new NavigationParameters();
            parameter.Add(NavigationConstants.PumpConfigurationVariAWizardModel, model);

            _systemUnderTest.OnNavigatedTo(parameter);

            _systemUnderTest.WizardModel.ShouldNotBeNull();
        }

        [Test]
        public void WizardRightButtonCommand_NavigateToNextPage_ShouldProceed()
        {
            int numberOfCalls = 0;

            NavigationUtilities.NavigateAsync = (navSvc, navPage, navParams, isModal, isAnimated) =>
            {
                ++numberOfCalls;
                return Task.FromResult<INavigationResult>(null);
            };

            PumpConfigurationModulAWizardModel wizardModel = new PumpConfigurationModulAWizardModel();

            INavigationParameters parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.PumpConfigurationModulAWizardModel, wizardModel);

            _systemUnderTest.WizardRightButtonCommand?.Execute();

            numberOfCalls.ShouldBe(1);
        }
    }
}
