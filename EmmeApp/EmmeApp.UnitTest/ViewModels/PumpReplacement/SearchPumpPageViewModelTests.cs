﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Entities;
using Moq;
using NUnit.Framework;
using Prism.Navigation;
using Shouldly;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Services;
using Plugin.Media.Abstractions;
using Prism.Events;
using Rg.Plugins.Popup.Contracts;

namespace EmmeApp.ViewModels
{
	[TestFixture]
	public class SearchPumpPageViewModelTests
	{
		private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);
		private Mock<IMasterPumpReplacementManager> _mockPumpReplacementManager;
		private Mock<IUserDialogs> _mockUserDialogs;
		private Mock<IKeyboardHelper> _mockKeyboardHelper;
		private Mock<INavigationService> _mockNavigationService;
		private Mock<INavigationHelperService> _mockNavigationHelperService;
		private Mock<IAsyncDelayer> _mockAsyncDelayer;
		private Mock<IAppCenterCustomService> _mockAppCenterService;
		private Mock<IEventAggregator> _eventAggregator;
		private Mock<IMedia> _mediaUtils;
		private Mock<IPermissionsService> _permissionsService;
		private Mock<IPumpReplacementManager> _pumpReplacementManager;
		private Mock<IOcrManager> _ocrManager;
		private Mock<IPopupNavigation> _popupNavigation;
		private Mock<IAsyncDelayer> _asyncDelayer;


		private SearchPumpPageViewModel _systemUnderTest;

		[SetUp]
		public void Setup()
		{
			_mockNavigationService = _mockRepository.Create<INavigationService>();
			_mockUserDialogs = _mockRepository.Create<IUserDialogs>();
			_mockKeyboardHelper = _mockRepository.Create<IKeyboardHelper>();
			_mockPumpReplacementManager = _mockRepository.Create<IMasterPumpReplacementManager>();
			_mockNavigationHelperService = _mockRepository.Create<INavigationHelperService>();
			_mockAsyncDelayer = _mockRepository.Create<IAsyncDelayer>();
			_mockAppCenterService = _mockRepository.Create<IAppCenterCustomService>();
			_mediaUtils = new Mock<IMedia>();
			_permissionsService = new Mock<IPermissionsService>();
			_pumpReplacementManager = new Mock<IPumpReplacementManager>();
			_ocrManager = new Mock<IOcrManager>();
			_popupNavigation = new Mock<IPopupNavigation>();
			_asyncDelayer =new Mock<IAsyncDelayer>();
			_eventAggregator = _mockRepository.Create<IEventAggregator>();
			_mockPumpReplacementManager.Setup(mr => mr.GetList()).Returns(_mockMasterData);
			_mockUserDialogs.Setup(mr => mr.ShowLoading(It.IsAny<string>(), It.IsAny<MaskType?>()));
			_mockUserDialogs.Setup(mr => mr.HideLoading());
			_mockKeyboardHelper.Setup(x => x.HideKeyboard());

			_systemUnderTest = new SearchPumpPageViewModel(_mockNavigationService.Object
				, _mockNavigationHelperService.Object
				, _mockAppCenterService.Object
				, _mockUserDialogs.Object
				, _eventAggregator.Object
				, _mockKeyboardHelper.Object
				, _mockPumpReplacementManager.Object
				, _mediaUtils.Object
				, _permissionsService.Object
				, _pumpReplacementManager.Object
				, _ocrManager.Object
				, _popupNavigation.Object
				, _asyncDelayer.Object);
		}
		
		[Test]
		public void Ctor_Always_PerformedExpectedWork()
		{
			_mockPumpReplacementManager.Verify();
			_mockUserDialogs.Verify();
			_mockKeyboardHelper.Verify();
			_mockRepository.Verify();
		}

		[TestCase("Biral")]
		public void GetMatchingValues_SetSearchKey_ShouldReturnsList(string searchKey)
		{
			List<MasterPumpReplacementEntity> mockData = _mockPumpReplacementManager.Object.GetList();

			_systemUnderTest.Pumps = mockData;

			var results = _systemUnderTest.GetMatchingValues(searchKey);
			int totalCount = results.Count;

			results.ShouldNotBeNull();
			results.ShouldBeOfType<List<MasterPumpReplacementEntity>>();
			totalCount.ShouldBe(5);
		}

		[TestCase("Biral M  10-4", "Biral M 10-4")]
		[TestCase(" Biral M 10-4 ", "Biral M 10-4")]
		[TestCase("Biral M 10 - 4", "Biral M 10-4")]
		[TestCase("Biral    M   10  4", "Biral M 10-4")]
		[TestCase(" M  10   4", "Biral M 10-4")]
		[TestCase(" M  10  -4", "Biral M 10-4")]
		public void GetMatchingValues_SearchKeyContainsMultipleSpaces_ShouldReturnSpecificData(string searchKey, string type)
		{
			var mockData = _mockPumpReplacementManager.Object.GetList();

			_systemUnderTest.Pumps = mockData;

			var results = _systemUnderTest.GetMatchingValues(searchKey);

			results.FirstOrDefault()?.Type.ShouldBe(type);
		}

		[TestCase("Biral AX 15-4 130 RED", "Biral AX 15-4 130 RED")]
		[TestCase("Biral AX 15 4 130 RED", "Biral AX 15-4 130 RED")]
		public void GetMatchingValues_ReplaceSearchKeySearchSpaceByHyphen_ShouldReturnSpecificData(string searchKey, string type)
		{
			List<MasterPumpReplacementEntity> mockData = _mockPumpReplacementManager.Object.GetList();

			_systemUnderTest.Pumps = mockData;

			var results = _systemUnderTest.GetMatchingValues(searchKey);

			results.FirstOrDefault()?.Type.ShouldBe(type);
		}

		[TestCase("Grundfos UPSD 65-60/4 F", "Grundfos UPSD 65-60/4 F")]
		[TestCase("Grundfos UPSD 65 60/4 F", "Grundfos UPSD 65-60/4 F")]
		[TestCase("Grundfos UPSD 65-60 4 F", "Grundfos UPSD 65-60/4 F")]
		public void GetMatchingValues_ReplaceSearchKeySearchSpaceByHyphenAndSlash_ShouldReturnSpecificData(string searchKey, string type)
		{
			var mockData = _mockPumpReplacementManager.Object.GetList();

			_systemUnderTest.Pumps = mockData;

			var results = _systemUnderTest.GetMatchingValues(searchKey);

			results.FirstOrDefault()?.Type.ShouldBe(type);
		}

		[TestCase("Stork DCB 213-G3/4\"", "Stork DCB 213-G3/4\"")]
		[TestCase("Stork DCB 213 G3/4\"", "Stork DCB 213-G3/4\"")]
		[TestCase("Stork DCB 213-G3 4\"", "Stork DCB 213-G3/4\"")]
		[TestCase("Stork DCB 213-G3/4 ", "Stork DCB 213-G3/4\"")]
		public void GetMatchingValues_ReplaceSearchKeySearchSpaceByQuoteHyphenAndSlash_ShouldReturnSpecificData(string searchKey, string type)
		{
			var mockData = _mockPumpReplacementManager.Object.GetList();
			_systemUnderTest.Pumps = mockData;

			var results = _systemUnderTest.GetMatchingValues(searchKey);

			results.FirstOrDefault()?.Type.ShouldBe(type);
		}

		[TestCase("stork dcb 213-G3/4\"")]
		[TestCase("grundfos upsd 65-60/4 F")]
		[TestCase("biral ax 15-4 130 red")]
		[TestCase("biral ")]
		public void GetMatchingValues_SearchPumpTypeNumberLowerCase_ShouldReturnList(string searchKey)
		{
			var mockData = _mockPumpReplacementManager.Object.GetList();
			_systemUnderTest.Pumps = mockData;

			var results = _systemUnderTest.GetMatchingValues(searchKey);
			var totalCount = results.Count;

			results.ShouldNotBeNull();
			results.ShouldBeOfType<List<MasterPumpReplacementEntity>>();
			totalCount.ShouldBeGreaterThanOrEqualTo(1);
		}

		[TestCase("g", "Grundfos UPSD 65-60/4 F")]
		[TestCase("r", "Grundfos UPSD 65-60/4 F")]
		public void GetMatchingValues_SearchPumpTypeNumber_ReturnSortedByIndexData(string searchKey, string type)
		{
			List<MasterPumpReplacementEntity> mockData = _mockPumpReplacementManager.Object.GetList();
			_systemUnderTest.Pumps = mockData;

			var results = _systemUnderTest.GetMatchingValues(searchKey);

			var result = results.FirstOrDefault();

			result?.Type.ShouldBe(type);
		}

		[Test]
		public void BackCommand_WhenBackButtonPressed_ShouldGoBack()
		{
			var numberOfCalls = 0;

			NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
			{
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			_systemUnderTest.BackCommand?.Execute();

			numberOfCalls.ShouldBe(1);
		}

		[Test]
		public void SelectPumpCommand_OnSelectPumpType_ShouldNavigateToNextPage()
		{
			_mockAsyncDelayer.Setup(mr => mr.Delay(It.IsAny<int>())).Returns(Task.FromResult(0));

			var pump = new MasterPumpReplacementEntity
			{
				Type = "Biral AX 12-4"
			};
			var parameters = new NavigationParameters {{NavigationConstants.SelectedReplacementPump, pump}};

			_mockNavigationHelperService.Setup(o => o.GetNavigationMode(parameters)).Returns(NavigationMode.New);

			var numberOfCalls = 0;

			NavigationUtilities.NavigateAsync = (navSvc, page, navParams, isModal, isAnimated) =>
			{
				if (page.Equals(ViewNames.PumpFittingProductsPage))
				{
					numberOfCalls++;
				}
				return Task.FromResult<INavigationResult>(null);
			};

			_systemUnderTest.SelectPumpCommand.Execute(pump);
			numberOfCalls.ShouldBe(1);
		}

		[Test]
		public void OnNavigatedTo_SeedData_ShouldReturnData()
		{
			_mockAsyncDelayer.Setup(mr => mr.Delay(It.IsAny<int>())).Returns(Task.FromResult(0));

			var navParams = new NavigationParameters();
			_mockNavigationHelperService.Setup(o => o.GetNavigationMode(navParams)).Returns(NavigationMode.New);

			_systemUnderTest.OnNavigatedTo(navParams);

			_systemUnderTest.Pumps.ShouldNotBeNull();

		}

		private readonly List<MasterPumpReplacementEntity> _mockMasterData = new List<MasterPumpReplacementEntity>
		{
			new MasterPumpReplacementEntity
			{
				ArticleNumber = "2205290150",
				CommentCodes = "-NA-",
				Diameter = "1\"",
				DiameterUnit =  "G",
				Engine = "1x230V",
				Length = "130",
				LengthUnit ="mm",
				Pressure = "10",
				PressureUnit = "PN",
				Type = "Biral AX 12-4"
			},
			new MasterPumpReplacementEntity
			{
				ArticleNumber = "2205300150",
				CommentCodes = "-NA-",
				Diameter = "1\"",
				DiameterUnit =  "G",
				Engine = "1x230V",
				Length = "130",
				LengthUnit ="mm",
				Pressure = "10",
				PressureUnit = "PN",
				Type = "Biral AX 13-4"
			},
			new MasterPumpReplacementEntity
			{
				ArticleNumber = "2205290150",
				CommentCodes = "-NA-",
				Diameter = "1\"",
				DiameterUnit =  "G",
				Engine = "1x230V",
				Length = "130",
				LengthUnit ="mm",
				Pressure = "10",
				PressureUnit = "PN",
				Type = "Biral AX 15-4 130 RED"
			},
			new MasterPumpReplacementEntity
			{
				ArticleNumber = "2205300150",
				CommentCodes = "-NA-",
				Diameter = "1\"",
				DiameterUnit =  "G",
				Engine = "1x230V",
				Length = "130",
				LengthUnit ="mm",
				Pressure = "10",
				PressureUnit = "PN",
				Type = "Biral AX 15-6 130 RED"
			},
			new MasterPumpReplacementEntity
			{
				ArticleNumber = "2205290150",
				CommentCodes = "-NA-",
				Diameter = "1\"",
				DiameterUnit =  "G",
				Engine = "1x230V",
				Length = "130",
				LengthUnit ="mm",
				Pressure = "10",
				PressureUnit = "PN",
				Type = "Biral M 10-4"
			},

			new MasterPumpReplacementEntity
			{
				ArticleNumber = "2201840350",
				CommentCodes = "-NA-",
				Diameter = "1\"",
				DiameterUnit =  "DN 65",
				Engine = "1x230V",
				Length = "340",
				LengthUnit ="mm",
				Pressure = "10",
				PressureUnit = "PN 6/16",
				Type = "Grundfos UPSD 65-60/4 F"
			},

			new MasterPumpReplacementEntity
			{
				ArticleNumber = "2206510150",
				CommentCodes = "-NA-",
				Diameter = "1 1/4\"",
				DiameterUnit =  "G",
				Engine = "1x230V",
				Length = "120",
				LengthUnit ="mm",
				Pressure = "10",
				PressureUnit = "PN 6/16",
				Type = "Stork DCB 213-G3/4\""
			},
		};
	}
}
