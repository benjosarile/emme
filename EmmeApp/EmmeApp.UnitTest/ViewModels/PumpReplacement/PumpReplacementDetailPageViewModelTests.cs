﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Entities;
using EmmeApp.Localization.Resources;
using Moq;
using NUnit.Framework;
using Plugin.Messaging;
using Prism.Navigation;
using Shouldly;
using System.Text;
using System.Threading.Tasks;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Managers.Abstractions;

namespace EmmeApp.ViewModels
{
	[TestFixture]
	public class PumpReplacementDetailPageViewModelTests
	{
		private Mock<INavigationService> _mockNavigationService;
		private Mock<INavigationHelperService> _mockNavigationHelperService;
		private Mock<IUserDialogs> _mockUserDialog;
		private Mock<IMessaging> _mockMessaging;
		private Mock<IFileService> _mockFileService;
		private Mock<IAppCenterCustomService> _mockAppCenterService;
		private Mock<IBiralContactManager> _mockBiralContactManager;
		private Mock<ISortimentManager> _mockSortimentManager;
		private Mock<IMarketManager> _mockMarketManager;
		private PumpReplacementDetailPageViewModel _pumpReplacementDetailPageViewModel;
		private Mock<IPumpReplacementManager> _mockPumpReplacementManager;
		
        [SetUp]
        public void Setup()
        {
            _mockNavigationService = new Mock<INavigationService>();
			_mockNavigationHelperService = new Mock<INavigationHelperService>();
			_mockUserDialog = new Mock<IUserDialogs>();
            _mockMessaging = new Mock<IMessaging>();
            _mockFileService = new Mock<IFileService>();
			_mockAppCenterService = new Mock<IAppCenterCustomService>();
			_mockBiralContactManager = new Mock<IBiralContactManager>();
			_mockSortimentManager = new Mock<ISortimentManager>();
			_mockMarketManager = new Mock<IMarketManager>();
			_mockPumpReplacementManager = new Mock<IPumpReplacementManager>();
			_pumpReplacementDetailPageViewModel = new PumpReplacementDetailPageViewModel(
                _mockNavigationService.Object, 
				_mockNavigationHelperService.Object,
				_mockAppCenterService.Object,
                _mockUserDialog.Object, 
                _mockMessaging.Object, 
                _mockFileService.Object,
				_mockMarketManager.Object,
				_mockBiralContactManager.Object,
                _mockPumpReplacementManager.Object,
				_mockSortimentManager.Object);
        }

        [Test]
        public void BackCommand_Always_Succeed()
        {
			//_navigationService.Setup(o => o.GoBackAsync(It.IsAny<INavigationParameters>(), It.IsAny<bool?>(), It.IsAny<bool>())).ReturnsAsync(new NavigationResult());
			int numberOfCalls = 0;

			NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) => {
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			_pumpReplacementDetailPageViewModel.BackCommand?.Execute();
			numberOfCalls.ShouldBe(1);
		}

        [Test]
        public void ShareCommand_ShouldSendMailWithAttachment_Successfully()
        {
            _pumpReplacementDetailPageViewModel.BiralProduct = new BiralProductEntity
            {
                Category = 1,
                CategoryValue = "Highly efficient pump",
                Diameter = "G 1½",
                Engine = "1x230 V",
                Length = "180 mm",
                MarketKey = "",
                Pressure = "PN 10",
                ProductId = 2204540350,
                Remarks = "Old pump 3x 400 V, new 1x 230 V",
                Type = "Biral AX 12-4"
            };

			_pumpReplacementDetailPageViewModel.SelectedReplacementPump = new MasterPumpReplacementEntity();
			_pumpReplacementDetailPageViewModel.BiralContact = new BiralContactEntity();

            string newLine = "\r\n";
            var sb = new StringBuilder();
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Details}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Type}: {_pumpReplacementDetailPageViewModel.BiralProduct.Type}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Item_Number}: {_pumpReplacementDetailPageViewModel.BiralProduct.ProductId}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Nominal_Diameter} [G/DN]: {_pumpReplacementDetailPageViewModel.BiralProduct.Diameter}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Nominal_Pressure} [PN]: {_pumpReplacementDetailPageViewModel.BiralProduct.Pressure}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Installed_Length} [mm]: {_pumpReplacementDetailPageViewModel.BiralProduct.Length}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Motor}: {_pumpReplacementDetailPageViewModel.BiralProduct.Engine}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Adapter}: {_pumpReplacementDetailPageViewModel.BiralProduct.AdapterInfos}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Remarks}: {_pumpReplacementDetailPageViewModel.BiralProduct.Remarks}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_OldPump}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Type}: {_pumpReplacementDetailPageViewModel.SelectedReplacementPump.Type}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Nominal_Diameter} [G/DN]: {_pumpReplacementDetailPageViewModel.SelectedReplacementPump.Diameter}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Nominal_Pressure} [PN]: {_pumpReplacementDetailPageViewModel.SelectedReplacementPump.Pressure}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Installed_Length} [mm]: {_pumpReplacementDetailPageViewModel.SelectedReplacementPump.Length}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Motor}: {_pumpReplacementDetailPageViewModel.SelectedReplacementPump.Engine}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_MoreInformation}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Datasheet}: {_pumpReplacementDetailPageViewModel.BiralProduct.DataSheetLink}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Operating_Instructions}: {_pumpReplacementDetailPageViewModel.BiralProduct.OperatingInstructionsLink}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Other_Information}: {_pumpReplacementDetailPageViewModel.BiralProduct.OtherInformationLink}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Contact_Information}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Address}: {_pumpReplacementDetailPageViewModel.BiralContact.Address1} {_pumpReplacementDetailPageViewModel.BiralContact.Address2}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Contact_Number}: {_pumpReplacementDetailPageViewModel.BiralContact.ContactNumber}{newLine}");
			sb.Append($"{AppResources.Pump_Replacement_Detail_Page_Email_Address}: {_pumpReplacementDetailPageViewModel.BiralContact.EmailAddress}{newLine}");

			_mockFileService.Setup(p => p.GetFilePath(It.IsAny<string>())).Returns("Biral_AX_12-4.txt");
            _mockFileService.Setup(p => p.WriteToAFile(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()));

            _mockMessaging.Setup(p => p.EmailMessenger.CanSendEmail).Returns(true);
            _mockUserDialog.Setup(p => p.ShowLoading(It.IsAny<string>(), It.IsAny<MaskType>()));
            _mockUserDialog.Setup(p => p.HideLoading());
            _mockMessaging.Setup(p => p.EmailMessenger.SendEmail(It.IsAny<IEmailMessage>()));

			_pumpReplacementDetailPageViewModel.ShareCommand?.Execute(string.Empty);

            _mockFileService.Verify(p => p.GetFilePath("Biral_AX_12-4.txt"));
            _mockFileService.Verify(p => p.WriteToAFile("Biral_AX_12-4.txt", sb.ToString(), false));
            _mockUserDialog.Verify(p => p.ShowLoading("Redirecting to a third party app...", null));
            _mockMessaging.Verify(p => p.EmailMessenger.CanSendEmail);
            _mockMessaging.Verify(p => p.EmailMessenger.SendEmail(It.IsAny<IEmailMessage>()), Times.Once);
        }

        


        [Test]
        public void OnNavigatedTo_Always_Succeed()
        {
            _pumpReplacementDetailPageViewModel.BiralProduct = new BiralProductEntity
            {
                Category = 1,
                CategoryValue = "Highly efficient pump",
                Diameter = "G 1½",
                Engine = "1x230 V",
                Length = "180 mm",
                MarketKey = "",
                Pressure = "PN 10",
                ProductId = 2204540350,
                Remarks = "Old pump 3x 400 V, new 1x 230 V",
                Type = "Biral AX 12-4"
            };
			_mockMarketManager.Setup(m => m.GetMarketLanguage()).Returns(new System.Tuple<string, string>("EU","EN"));

            var parameters = new NavigationParameters {{NavigationConstants.SelectedBiralProduct, _pumpReplacementDetailPageViewModel.BiralProduct}};

            _pumpReplacementDetailPageViewModel.OnNavigatedTo(parameters);

            _pumpReplacementDetailPageViewModel.BiralProduct.ShouldNotBeNull();
            _pumpReplacementDetailPageViewModel.BiralProduct.ShouldBeOfType<BiralProductEntity>();
        }
    }
}
