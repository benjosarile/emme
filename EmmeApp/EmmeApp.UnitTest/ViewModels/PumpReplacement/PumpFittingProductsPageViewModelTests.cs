﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Entities;
using Moq;
using NUnit.Framework;
using Prism.Navigation;
using Shouldly;
using System.Collections.Generic;
using System.Threading.Tasks;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Managers.Abstractions;

namespace EmmeApp.ViewModels
{
	[TestFixture]
	public class PumpFittingProductsPageViewModelTests
	{
		private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);

		private Mock<INavigationService> _mockNavigationService;
		private Mock<IUserDialogs> _mockUserDialogs;
		private Mock<IMarketManager> _mockMarketManager;
		private Mock<IPumpReplacementProductManager> _mockPumpReplacementProductManager;
		private Mock<IBiralProductManager> _mockBiralProductManager;
		private Mock<IAsyncDelayer> _mockAsyncDelayer;
		private Mock<INavigationHelperService> _mockNavigationHelperService;
		private Mock<IAppCenterCustomService> _mockAppCenterService;
		private PumpFittingProductsPageViewModel _systemUnderTest;

		[SetUp]
		public void Setup()
		{
			//var mockContainer = new Mock<IContainerExtension>();
			//var mockApplicationProvider = new Mock<IApplicationProvider>();
			//var mockPageBehaviorProvider = new Mock<IPageBehaviorFactory>();
			//var mockLoggerFacade = new Mock<ILoggerFacade>();

			//var fakeNavigationService = new FakeNavigationService(
			//	mockContainer.Object,
			//	mockApplicationProvider.Object,
			//	mockPageBehaviorProvider.Object,
			//	mockLoggerFacade.Object);
			_mockRepository.Create<INavigationService>();
			_mockNavigationService = _mockRepository.Create<INavigationService>();
			_mockUserDialogs = _mockRepository.Create<IUserDialogs>();
			_mockMarketManager = _mockRepository.Create<IMarketManager>();
			_mockPumpReplacementProductManager = _mockRepository.Create<IPumpReplacementProductManager>();
			_mockBiralProductManager = _mockRepository.Create<IBiralProductManager>();
			_mockAsyncDelayer = _mockRepository.Create<IAsyncDelayer>();
			_mockNavigationHelperService = _mockRepository.Create<INavigationHelperService>();
			_mockAppCenterService = _mockRepository.Create<IAppCenterCustomService>();

			_mockUserDialogs.Setup(mr => mr.ShowLoading(It.IsAny<string>(), It.IsAny<MaskType>()));
			_mockUserDialogs.Setup(mr => mr.HideLoading());

			_systemUnderTest = new PumpFittingProductsPageViewModel(
				_mockNavigationService.Object,
				_mockNavigationHelperService.Object,
				_mockAppCenterService.Object,
				_mockUserDialogs.Object,
				_mockMarketManager.Object,
				_mockPumpReplacementProductManager.Object,
				_mockBiralProductManager.Object,
				_mockAsyncDelayer.Object);
		}

		[Test]
		public void Ctor_Always_PerformsExpectedWork()
		{
			_mockUserDialogs.Verify();
			_mockRepository.Verify();
		}

		[Test]
		public void OnNavigatedTo_NavigateWhenThereAreNoParameters_ShouldGoBack()
		{
			int numberOfCalls = 0;

			NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) => {
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			var parameters = new NavigationParameters();
			_mockNavigationHelperService.Setup(o => o.GetNavigationMode(parameters)).Returns(NavigationMode.New);

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.Type.ShouldBeNull();
			numberOfCalls.ShouldBe(1);
		}

		[Test]
		public void OnNavigatedTo_ThereAreIsAnExpectedParameter_ShouldSetPumpType()
		{
			int numberOfCalls = 0;

			NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) => {
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			var masterPumpReplacement = new MasterPumpReplacementEntity
			{
				Type = "Biral AX 12-4"
			};

			_mockPumpReplacementProductManager.Setup(o => o.GetRelatedProducts(masterPumpReplacement.Type)).Returns(new List<PumpReplacementProductEntity>());

			var parameters = new NavigationParameters {{NavigationConstants.SelectedReplacementPump, masterPumpReplacement}};

			_mockNavigationHelperService.Setup(o => o.GetNavigationMode(parameters)).Returns(NavigationMode.New);

			_systemUnderTest.OnNavigatedTo(parameters);
			_systemUnderTest.Type.ShouldNotBeNull();
			_systemUnderTest.Type.ShouldBeOfType<string>();
			numberOfCalls.ShouldBe(0);
		}

		[Test]
		public void BackCommand_Always_AlwaysSucceed()
		{
			//_mockNavigationService.Setup(o => o.GoBackAsync(It.IsAny<INavigationParameters>(), It.IsAny<bool?>(), It.IsAny<bool>())).ReturnsAsync(new NavigationResult());
			int numberOfCalls = 0;

			NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) => {
				++numberOfCalls;
				return Task.FromResult<INavigationResult>(null);
			};

			_mockAsyncDelayer.Setup(o => o.Delay(It.IsAny<int>())).Returns(Task.FromResult(0));

			_systemUnderTest.BackCommand?.Execute();

			numberOfCalls.ShouldBe(1);
		}
	}
}
