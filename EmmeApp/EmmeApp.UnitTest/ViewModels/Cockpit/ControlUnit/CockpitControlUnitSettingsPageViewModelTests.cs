﻿using Acr.UserDialogs;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Events;
using EmmeApp.ItemModels;
using EmmeApp.Managers.Abstractions;
using Moq;
using NUnit.Framework;
using Prism.Events;
using Prism.Navigation;
using Shouldly;

namespace EmmeApp.ViewModels
{
	[TestFixture]
	public class CockpitControlUnitSettingsPageViewModelTests
	{
		private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);
		private Mock<INavigationService> _mockNavigationService;
		private Mock<INavigationHelperService> _mockNavigationHelperService;
		private Mock<IUserDialogs> _mockUserDialogs;
		private Mock<IEventAggregator> _mockEventAggregator;
		private Mock<IServiceEntityMapper> _mockServiceEntityMapper;
		private Mock<IAppCenterCustomService> _mockAppCenterService;

		private CockpitControlUnitSettingsPageViewModel _systemUnderTest;

		[SetUp]
		public void Setup()
		{
			_mockRepository.Create<INavigationService>();
			_mockNavigationService = _mockRepository.Create<INavigationService>();
			_mockNavigationHelperService = _mockRepository.Create<INavigationHelperService>();
			_mockUserDialogs = _mockRepository.Create<IUserDialogs>();
			_mockEventAggregator = _mockRepository.Create<IEventAggregator>();
			_mockServiceEntityMapper = _mockRepository.Create<IServiceEntityMapper>();
			_mockAppCenterService = _mockRepository.Create<IAppCenterCustomService>();

			_systemUnderTest = new CockpitControlUnitSettingsPageViewModel(
							_mockNavigationService.Object,
							_mockNavigationHelperService.Object,
							_mockAppCenterService.Object,
							_mockUserDialogs.Object,
							_mockEventAggregator.Object,
							_mockServiceEntityMapper.Object);

			_mockEventAggregator.Setup(o => o.GetEvent<CockpitControlUnitButtonSendEvent>().Publish(It.IsAny<CockpitControlUnitButtonSendModel>()));
			}
		[Test]
		public void Ctor_Always_PerformsExpectedWork()
		{
			_mockNavigationService.Verify();
			_mockNavigationHelperService.Verify();
			_mockUserDialogs.Verify();
			_mockEventAggregator.Verify();
			_mockRepository.Verify();
		}

		[Test]
		public void ConfirmAlarmPressedCommand_CmdSystemConfirmAlarm_ShouldBeTrue()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.ConfirmAlarmPressed?.Execute();
			_systemUnderTest.SettingsModel.CmdSystemConfirmAlarm.ShouldBe(true);
		}

		[Test]
		public void ConfirmAlarmReleasedCommand_CmdSystemConfirmAlarm_ShouldBeFalse()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.ConfirmAlarmReleased?.Execute();
			_systemUnderTest.SettingsModel.CmdSystemConfirmAlarm.ShouldBe(false);
		}

		[Test]
		public void ConfirmMaintenanceWarningPressedCommand_CmdSystemConfirmAlarmMaintenance_ShouldBeTrue()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.ConfirmMaintenanceWarningPressed?.Execute();
			_systemUnderTest.SettingsModel.CmdSystemConfirmAlarmMaintenance.ShouldBe(true);
		}

		[Test]
		public void ConfirmMaintenanceWarningReleasedCommand_CmdSystemConfirmAlarmMaintenance_ShouldBeFalse()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.ConfirmMaintenanceWarningReleased?.Execute();
			_systemUnderTest.SettingsModel.CmdSystemConfirmAlarmMaintenance.ShouldBe(false);
		}

		[Test]
		public void TestAlarmRelayPressed_CmdM2MTestAlarmRelay_ShouldBeTrue()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.TestAlarmRelayPressed?.Execute();
			_systemUnderTest.SettingsModel.CmdM2MTestAlarmRelay.ShouldBe(true);
		}

		[Test]
		public void TestAlarmRelayReleased_CmdM2MTestAlarmRelay_ShouldBeFalse()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.TestAlarmRelayReleased?.Execute();
			_systemUnderTest.SettingsModel.CmdM2MTestAlarmRelay.ShouldBe(false);
		}

		[Test]
		public void TestCustomRelay0Pressed_CmdM2MTestCustomRelay0_ShouldBeTrue()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.TestCustomRelay0Pressed?.Execute();
			_systemUnderTest.SettingsModel.CmdM2MTestCustomRelay0.ShouldBe(true);
		}

		[Test]
		public void TestCustomRelay0Released_CmdM2MTestCustomRelay0_ShouldBeFalse()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.TestCustomRelay0Released?.Execute();
			_systemUnderTest.SettingsModel.CmdM2MTestCustomRelay0.ShouldBe(false);
		}

		[Test]
		public void TestCustomRelay1Pressed_CmdM2MTestCustomRelay1_ShouldBeTrue()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.TestCustomRelay1Pressed?.Execute();
			_systemUnderTest.SettingsModel.CmdM2MTestCustomRelay1.ShouldBe(true);
		}

		[Test]
		public void TestCustomRelay1Released_CmdM2MTestCustomRelay1_ShouldBeFalse()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.TestCustomRelay1Released?.Execute();
			_systemUnderTest.SettingsModel.CmdM2MTestCustomRelay1.ShouldBe(false);
		}

		[Test]
		public void TestCustomRelay2Pressed_CmdM2MTestCustomRelay2_ShouldBeTrue()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.TestCustomRelay2Pressed?.Execute();
			_systemUnderTest.SettingsModel.CmdM2MTestCustomRelay2.ShouldBe(true);
		}

		[Test]
		public void TestCustomRelay2Released_CmdM2MTestCustomRelay2_ShouldBeFalse()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.TestCustomRelay2Released?.Execute();
			_systemUnderTest.SettingsModel.CmdM2MTestCustomRelay2.ShouldBe(false);
		}


		[Test]
		public void TestAlarmHornPressedCommand_CmdHmiTestAlarmHorn_ShouldBeTrue()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.TestAlarmHornPressed?.Execute();
			_systemUnderTest.SettingsModel.CmdHmiTestAlarmHorn.ShouldBe(true);
		}

		[Test]
		public void TestAlarmHornReleasedCommand_CmdHmiTestAlarmHorn_ShouldBeFalse()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.TestAlarmHornReleased?.Execute();
			_systemUnderTest.SettingsModel.CmdHmiTestAlarmHorn.ShouldBe(false);
		}

		[Test]
		public void TestAlarmBuzzerPressedCommand_CmdHmiTestAlarmBuzzer_ShouldBeTrue()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.TestAlarmBuzzerPressed?.Execute();
			_systemUnderTest.SettingsModel.CmdHmiTestAlarmBuzzer.ShouldBe(true);
		}

		[Test]
		public void TestAlarmBuzzerReleasedCommand_CmdHmiTestAlarmBuzzer_ShouldBeFalse()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.TestAlarmBuzzerReleased?.Execute();
			_systemUnderTest.SettingsModel.CmdHmiTestAlarmBuzzer.ShouldBe(false);
		}

		[Test]
		public void ResetStatisticsPressedCommand_CmdStatisticReset_ShouldBeTrue()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.ResetStatisticsPressed?.Execute();
			_systemUnderTest.SettingsModel.CmdStatisticReset.ShouldBe(true);
		}

		[Test]
		public void ResetStatisticsReleasedCommand_CmdSystemConfirmAlarmMaintenance_ShouldBeFalse()
		{
			_systemUnderTest.SettingsModel = new CockpitControlUnitSettingsModel();
			_systemUnderTest.ResetStatisticsReleased?.Execute();
			_systemUnderTest.SettingsModel.CmdStatisticReset.ShouldBe(false);
		}

	}
}
