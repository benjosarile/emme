﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Entities.External;
using EmmeApp.Events;
using Moq;
using NUnit.Framework;
using Plugin.BluetoothLE;
using Plugin.Messaging;
using Prism.Events;
using Prism.Navigation;
using Shouldly;
using System.Collections.Generic;
using System.Threading.Tasks;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Managers.Abstractions;
using Rg.Plugins.Popup.Contracts;

namespace EmmeApp.ViewModels
{
    [TestFixture]
    public class CockpitPumpsModulARootPageViewModelTests
    {
        private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);
        private Mock<INavigationService> _mockNavigationService;
        private Mock<INavigationHelperService> _mockNavigationHelperService;
        private Mock<IUserDialogs> _mockUserDialogs;
        private Mock<IAppCenterCustomService> _mockAppCenterCustomService;
        private Mock<IEventAggregator> _mockEventAggregator;
        private Mock<IMessaging> _mockMessaging;
        private Mock<IFileService> _mockFileService;
        private Mock<IBleManager> _mockBleManager;
        private Mock<ICockpitPumpsModulAManager> _mockCockpitPumpsModulAManager;
		private Mock<ILogPumpManager> _mockLogPumpManager;
        private Mock<IPopupNavigation> _mockPopUpNavigation;
		private CockpitPumpsModulARootPageViewModel _systemUnderTest;

        [SetUp]
        public void Setup()
        {
            _mockRepository.Create<INavigationService>();
            _mockNavigationService = _mockRepository.Create<INavigationService>();
            _mockNavigationHelperService = _mockRepository.Create<INavigationHelperService>();
            _mockUserDialogs = _mockRepository.Create<IUserDialogs>();
            _mockAppCenterCustomService = _mockRepository.Create<IAppCenterCustomService>();
            _mockEventAggregator = _mockRepository.Create<IEventAggregator>();
            _mockMessaging = _mockRepository.Create<IMessaging>();
            _mockFileService = _mockRepository.Create<IFileService>();
            _mockBleManager = _mockRepository.Create<IBleManager>();
            _mockCockpitPumpsModulAManager = _mockRepository.Create<ICockpitPumpsModulAManager>();
			_mockLogPumpManager = _mockRepository.Create<ILogPumpManager>();
            _mockPopUpNavigation = _mockRepository.Create<IPopupNavigation>();
            
			var pumpConfigurationEvent = new Mock<PumpConfigurationEvent>();
            _mockEventAggregator.Setup(o => o.GetEvent<PumpConfigurationEvent>()).Returns(pumpConfigurationEvent.Object);

            var cockpitPumpModulAButtonSendEvent = new Mock<CockpitPumpModulAButtonSendEvent>();
            _mockEventAggregator.Setup(o => o.GetEvent<CockpitPumpModulAButtonSendEvent>()).Returns(cockpitPumpModulAButtonSendEvent.Object);

            _mockUserDialogs.Setup(mr => mr.ShowLoading(It.IsAny<string>(), It.IsAny<MaskType?>()));
            _mockUserDialogs.Setup(mr => mr.HideLoading());

            _mockBleManager.Setup(b => b.GetConnectedDevice()).ReturnsAsync(It.IsAny<IDevice>());
            _mockBleManager.Setup(b => b.RestartTheDevice(It.IsAny<IDevice>())).ReturnsAsync(It.IsAny<IDevice>());

            _systemUnderTest = new CockpitPumpsModulARootPageViewModel(
                _mockNavigationService.Object,
                _mockNavigationHelperService.Object,
                _mockUserDialogs.Object,
                _mockAppCenterCustomService.Object,
                _mockEventAggregator.Object,
                _mockMessaging.Object,
                _mockFileService.Object,
                _mockBleManager.Object,
                _mockCockpitPumpsModulAManager.Object,
				_mockLogPumpManager.Object,
                _mockPopUpNavigation.Object);
        }

        [Test]
        public void Ctor_Always_PerformsExpectedWork()
        {
            _mockNavigationService.Verify();
            _mockNavigationHelperService.Verify();
            _mockUserDialogs.Verify();
            _mockAppCenterCustomService.Verify();
            _mockEventAggregator.Verify();
            _mockMessaging.Verify();
            _mockFileService.Verify();
            _mockBleManager.Verify();
            _mockCockpitPumpsModulAManager.Verify();

        }

        [Test]
        public void OnNavigatedTo_ParameterContainsPumpConfigurationModulAWizardModel_ShouldContainPumpConfigurationModulAWizardModel()
        {
            NavigationUtilities.NavigateAsync = (navSvc, navPage, navParams, isModal, isAnimated) => Task.FromResult<INavigationResult>(null);

            var wizardModel = new CockpitPumpsModulARootModel();

            INavigationParameters parameters = new NavigationParameters();
            parameters.Add(NavigationConstants.CockpitPumpsModulARootModel, wizardModel);

            _systemUnderTest.OnNavigatedTo(parameters);

            _systemUnderTest.RootModel.ShouldNotBeNull();
        }

        [Test]
        public void OnNavigatedTo_ParameterContainsInitialParameters_ShouldContainInitialParameter()
        {
            var initialParameters = new List<ParameterTExternalEntity>();

            var parameters = new NavigationParameters {{NavigationConstants.InitialParameters, initialParameters}};

            _systemUnderTest.OnNavigatedTo(parameters);

            _systemUnderTest.InitialParameters.ShouldNotBeNull();

        }

    }
}
