﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Managers.Abstractions;
using Moq;
using NUnit.Framework;
using Prism.Events;
using Prism.Navigation;
using Shouldly;

namespace EmmeApp.ViewModels.Cockpit.Pumps.VariAE
{
	public class CockpitPumpsVariAESettingsPageViewModelTests
	{
		private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);
		private Mock<INavigationService> _mockNavigationService;
		private Mock<INavigationHelperService> _mockNavigationHelperService;
		private Mock<IUserDialogs> _mockUserDialogs;
		private Mock<IEventAggregator> _mockEventAggregator;
		private Mock<IServiceEntityMapper> _mockServiceEntityMapper;
		private Mock<IAppCenterCustomService> _mockAppCenterService;

		private CockpitPumpsVariAESettingsPageViewModel _systemUnderTest;

		[SetUp]
		public void Setup()
		{
			_mockRepository.Create<INavigationService>();
			_mockNavigationService = _mockRepository.Create<INavigationService>();
			_mockNavigationHelperService = _mockRepository.Create<INavigationHelperService>();
			_mockUserDialogs = _mockRepository.Create<IUserDialogs>();
			_mockEventAggregator = _mockRepository.Create<IEventAggregator>();
			_mockServiceEntityMapper = _mockRepository.Create<IServiceEntityMapper>();
			_mockAppCenterService = _mockRepository.Create<IAppCenterCustomService>();

			_systemUnderTest = new CockpitPumpsVariAESettingsPageViewModel(
							_mockNavigationService.Object,
							_mockNavigationHelperService.Object,
							_mockAppCenterService.Object,
							_mockUserDialogs.Object,
							_mockEventAggregator.Object,
							_mockServiceEntityMapper.Object);
		}

		[Test]
		public void Ctor_Always_PerformsExpectedWork()
		{
			_mockNavigationService.Verify();
			_mockNavigationHelperService.Verify();
			_mockUserDialogs.Verify();
			_mockEventAggregator.Verify();
			_mockRepository.Verify();
		}

		[Test]
		public void ConfirmAlarmPressedCommand_CmdSystemConfirmAlarm_ShouldBeTrue()
		{
			_systemUnderTest.SettingsModel = new CockpitPumpsVariAESettingsModel();
			_systemUnderTest.ConfirmAlarmPressed?.Execute();
			_systemUnderTest.SettingsModel.CmdSystemConfirmAlarm.ShouldBe(true);
		}

		[Test]
		public void ConfirmAlarmReleasedCommand_CmdSystemConfirmAlarm_ShouldBeFalse()
		{
			_systemUnderTest.SettingsModel = new CockpitPumpsVariAESettingsModel();
			_systemUnderTest.ConfirmAlarmReleased?.Execute();
			_systemUnderTest.SettingsModel.CmdSystemConfirmAlarm.ShouldBe(false);
		}

		[Test]
		public void OnNavigated_CockpitPumpsVariaAERootModelParameters_ContainsKey_Map_Successfully()
		{
			var model = new CockpitPumpsVariAERootModel();
			var variAESettings = new CockpitPumpsVariAESettingsModel();
			_mockServiceEntityMapper.Setup(x => x.Map<CockpitPumpsVariAESettingsModel>(model)).Returns(variAESettings);

			INavigationParameters parameters = new NavigationParameters();
			parameters.Add(NavigationConstants.CockpitPumpsVariAERootModel, model);

			_systemUnderTest.OnNavigatedTo(parameters);

			_mockServiceEntityMapper.Verify(x => x.Map<CockpitPumpsVariAESettingsModel>(model), Times.Once);
		}
	}
}
