﻿using Acr.UserDialogs;
using Moq;
using NUnit.Framework;
using Plugin.BluetoothLE;
using Prism.Navigation;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Managers.Abstractions;
using Prism.Events;

namespace EmmeApp.ViewModels
{
    [TestFixture]
    public class GatewaySubPageViewModelTests
    {
        private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);
        private Mock<INavigationService> _mockNavigationService;
        private Mock<INavigationHelperService> _mockNavigationHelperService;
        private Mock<IUserDialogs> _mockUserDialogs;
        private Mock<IBleManager> _mockBleManager;
		private Mock<IAdapter> _mockAdapter;
		private Mock<IAppCenterCustomService> _mockAppCenterService;
        private Mock<IEventAggregator> _mockEventAggregator;
        private Mock<IParameterDataCacheManager> _mockParameterDataCacheManager;
        private Mock<ICockpitControlUnitManager> _mockCockpitControlUnitManager;
        private Mock<IConfigurationControlUnitManager> _mockConfigurationControlUnitManager;
        private Mock<IConfigurationPumpModulAManager> _mockConfigurationPumpModulAManager;
        private Mock<IConfigurationPumpVariAManager> _mockConfigurationPumpVariAManager;
        private Mock<ILogControlUnitManager> _mockLogControlUnitManager;
        private Mock<ILogPumpManager> _mockLogPumpManager;
        private Mock<ICockpitPumpsModulAManager> _mockCockpitPumpsModulAManager;
        private Mock<ICockpitPumpsVariAEManager> _mockCockpitPumpsVariAEManager;
        
		private GatewaySubPageViewModel _systemUnderTest;

        [SetUp]
        public void Setup()
        {
            _mockNavigationService = _mockRepository.Create<INavigationService>();
            _mockNavigationHelperService = _mockRepository.Create<INavigationHelperService>();
            _mockUserDialogs = _mockRepository.Create<IUserDialogs>();
            _mockBleManager = _mockRepository.Create<IBleManager>();
			_mockAdapter = _mockRepository.Create<IAdapter>();
			_mockAppCenterService = _mockRepository.Create<IAppCenterCustomService>();
            _mockEventAggregator = _mockRepository.Create<IEventAggregator>();
            _mockParameterDataCacheManager = _mockRepository.Create<IParameterDataCacheManager>();
            _mockCockpitControlUnitManager = _mockRepository.Create<ICockpitControlUnitManager>();
            _mockConfigurationControlUnitManager = _mockRepository.Create<IConfigurationControlUnitManager>();
            _mockConfigurationPumpModulAManager = _mockRepository.Create<IConfigurationPumpModulAManager>();
            _mockConfigurationPumpVariAManager = _mockRepository.Create<IConfigurationPumpVariAManager>();
            _mockLogControlUnitManager = _mockRepository.Create<ILogControlUnitManager>();
            _mockLogPumpManager = _mockRepository.Create<ILogPumpManager>();
            _mockCockpitPumpsModulAManager = _mockRepository.Create<ICockpitPumpsModulAManager>();
            _mockCockpitPumpsVariAEManager = _mockRepository.Create<ICockpitPumpsVariAEManager>();
            
			_mockAppCenterService.Setup(o => o.TrackError(It.IsAny<Exception>(), It.IsAny<Dictionary<string, string>>()));
			_mockAppCenterService.Setup(o => o.TrackEvent(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>()));

			_systemUnderTest = new GatewaySubPageViewModel(
                _mockNavigationService.Object,
                _mockNavigationHelperService.Object,
				_mockAppCenterService.Object,
                _mockUserDialogs.Object,
                _mockEventAggregator.Object,
                _mockAdapter.Object,
                _mockBleManager.Object,
                _mockParameterDataCacheManager.Object,
                _mockCockpitControlUnitManager.Object,
                _mockConfigurationControlUnitManager.Object,
                _mockConfigurationPumpModulAManager.Object,
                _mockConfigurationPumpVariAManager.Object,
                _mockLogControlUnitManager.Object,
                _mockLogPumpManager.Object,
                _mockCockpitPumpsModulAManager.Object,
                _mockCockpitPumpsVariAEManager.Object);
        }
       
        [Test]
        public void Ctor_Always_PerformsExpectedWork()
        {
            _mockNavigationService.Verify();
            _mockNavigationHelperService.Verify();
            _mockUserDialogs.Verify();
            _mockBleManager.Verify();
            _mockRepository.Verify();
        }        

        [Test]
        public void ExitCommand_AfterCheckingOfDeviceAllowed_ShouldGoBackAndExit()
        {
            var numberOfCalls = 0;

            NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
            {
                ++numberOfCalls;
                return Task.FromResult<INavigationResult>(null);
            };

            _systemUnderTest.ExitCommand?.Execute();

            numberOfCalls.ShouldBe(1);
        }

    }
}
