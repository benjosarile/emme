﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.Mock;
using Moq;
using NUnit.Framework;
using Plugin.BluetoothLE;
using Prism.Navigation;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using System.Threading.Tasks;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Managers.Abstractions;
using Rg.Plugins.Popup.Contracts;
using Xamarin.Forms.Mocks;

namespace EmmeApp.ViewModels
{
	[TestFixture]
	public class ConnectToGatewayPageViewModelTests
	{
		private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);
		private Mock<IAdapter> _mockAdapter;
		private Mock<IUserDialogs> _mockUserDialogs;
		private Mock<INavigationService> _mockNavigationService;
		private Mock<INavigationHelperService> _mockNavigationHelperService;
		private Mock<IScheduler> _mockScheduler;
		private Mock<IAsyncDelayer> _mockAsyncDelayer;
		private Mock<IBleManager> _mockBleManager;
		private Mock<IAppCenterCustomService> _mockAppCenterService;
		private Mock<IPopupNavigation> _mockPopupNavigation;
		private ConnectToGatewayPageViewModel _systemUnderTest;

		[SetUp]
		public void Setup()
		{
			MockForms.Init();
			_mockRepository.Create<INavigationService>();
			_mockNavigationService = _mockRepository.Create<INavigationService>();
			_mockNavigationHelperService = _mockRepository.Create<INavigationHelperService>();
			_mockUserDialogs = _mockRepository.Create<IUserDialogs>();
			_mockAdapter = _mockRepository.Create<IAdapter>();
			_mockScheduler = _mockRepository.Create<IScheduler>();
			_mockAsyncDelayer = _mockRepository.Create<IAsyncDelayer>();
			_mockBleManager = _mockRepository.Create<IBleManager>();
			_mockAppCenterService = _mockRepository.Create<IAppCenterCustomService>();
			_mockPopupNavigation = _mockRepository.Create<IPopupNavigation>();
			
			_mockAppCenterService.Setup(o => o.TrackError(It.IsAny<Exception>(), It.IsAny<Dictionary<string, string>>()));
			_mockAppCenterService.Setup(o => o.TrackEvent(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>()));

			_systemUnderTest = new ConnectToGatewayPageViewModel(
				_mockNavigationService.Object,
				_mockNavigationHelperService.Object,
				_mockAppCenterService.Object,
				_mockUserDialogs.Object,
				_mockPopupNavigation.Object,
				_mockAsyncDelayer.Object,
				_mockAdapter.Object,
				_mockBleManager.Object);
		}
		
		[Test]
		public void Ctor_Always_PerformsExpectedWork()
		{
			_mockNavigationService.Verify();
			_mockNavigationHelperService.Verify();
			_mockUserDialogs.Verify();
			_mockAdapter.Verify();
			_mockScheduler.Verify();
			_mockAsyncDelayer.Verify();
			_mockRepository.Verify();
		}

		[Test]
		public async Task TimerElapsedCallback_TriggeredAfter5Seconds_ShouldStopScanningAndNavigate()
		{
			_mockAdapter.Setup(a => a.IsScanning).Returns(true);
			_mockAdapter.Setup(a => a.StopScan());
			_mockAsyncDelayer.Setup(o => o.Delay(It.IsAny<int>())).Returns(Task.FromResult(0));

			int numberOfCalls = 0;
			NavigationUtilities.NavigateAsync = (navSvc, page, navParams, isModal, isAnimated) =>
			{
				if (page.Equals(ViewNames.ConnectToGatewayListPage))
				{
					numberOfCalls++;
				}
				return Task.FromResult<INavigationResult>(null);
			};


			await _systemUnderTest.TimerElapsedCallback();

			_systemUnderTest.IsScanning.ShouldBe(false);
			numberOfCalls.ShouldBe(1);
		}

		[TestCase(1, 5)]
		[TestCase(2, 6)]
		[TestCase(3, 0)]
		[TestCase(4, 0)]
		public void ScanResultCallback_OnScanDevices_ShouldNotBeNull(int testCase, int count)
		{
			_systemUnderTest.Devices = new List<ItemModels.GatewayDeviceItemModel>();

			var results = new List<IScanResult>();
			switch (testCase)
			{
				case 1: results = _mockScanResults1; break;
				case 2: results = _mockScanResults2; break;
				case 3: results = _mockScanResults3; break;
			}

			_systemUnderTest.ScanResultCallback(results);

			_systemUnderTest.Devices.ShouldNotBeNull();
			_systemUnderTest.Devices.Count.ShouldBe(count);

		}

		//[Test]
		//public void OnNavigatedTo_InitialProperty_ShouldNotBeNull()
		//{
		//    _mockAdapter.Setup(a => a.WhenStatusChanged());
		//    var navParams = new NavigationParameters();
		//    _systemUnderTest.OnNavigatedTo(navParams);

		//    _systemUnderTest.Devices.ShouldNotBeNull();
		//}

		//[Test]
		//public void ConnectionStatusCallback_OnAdapterStatusPoweredOn_ShouldExecuteScanCommand()
		//{
		//    var status = AdapterStatus.PoweredOn;
		//    _mockAdapter.Setup(a => a.Status).Returns(AdapterStatus.PoweredOn);
		//    _systemUnderTest.IsScanning = true;

		//    _systemUnderTest.ConnectionStatusCallback(status);

		//    _systemUnderTest.ScanCommand.IsActive.ShouldBe(true);
		//}


		private readonly List<IScanResult> _mockScanResults1 = new List<IScanResult>
		{
		   new  ScanResult(new MockDevice{ Name = "Biral", Uuid = new Guid("00000000-0000-0000-0000-000000000001")}, 123, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = "Biral", Uuid = new Guid("00000000-0000-0000-0000-000000000001")}, 123, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = null, Uuid = new Guid("00000000-0000-0000-0000-000000000002")}, 456, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = "Biral", Uuid = new Guid("00000000-0000-0000-0000-000000000003")}, 789, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = "Biral", Uuid = new Guid("00000000-0000-0000-0000-000000000004")}, 1011, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = null, Uuid = new Guid("00000000-0000-0000-0000-000000000005")}, 1213, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = "Biral", Uuid = new Guid("00000000-0000-0000-0000-000000000006")}, 1415, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = "Biral", Uuid = new Guid("00000000-0000-0000-0000-000000000007")}, 1617, new MockIAdvertisementData()),
		};

		private readonly List<IScanResult> _mockScanResults2 = new List<IScanResult>
		{
		   new  ScanResult(new MockDevice{ Name = "Biral", Uuid = new Guid("00000000-0000-0000-0000-000000000001")}, 123, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = "Biral", Uuid = new Guid("00000000-0000-0000-0000-000000000001")}, 123, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = "Biral_1", Uuid = new Guid("00000000-0000-0000-0000-000000000002")}, 456, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = "Biral", Uuid = new Guid("00000000-0000-0000-0000-000000000003")}, 789, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = "Biral_2", Uuid = new Guid("00000000-0000-0000-0000-000000000004")}, 1011, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = "Biral_3", Uuid = new Guid("00000000-0000-0000-0000-000000000005")}, 1213, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = "Biral_6", Uuid = new Guid("00000000-0000-0000-0000-000000000007")}, 1415, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = "Biral_6", Uuid = new Guid("00000000-0000-0000-0000-000000000007")}, 1617, new MockIAdvertisementData()),
		};

		private readonly List<IScanResult> _mockScanResults3 = new List<IScanResult>
		{
		   new  ScanResult(new MockDevice{ Name = null, Uuid = new Guid("00000000-0000-0000-0000-000000000001")}, 123, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = null, Uuid = new Guid("00000000-0000-0000-0000-000000000001")}, 123, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = null, Uuid = new Guid("00000000-0000-0000-0000-000000000002")}, 456, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = null, Uuid = new Guid("00000000-0000-0000-0000-000000000003")}, 789, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = null, Uuid = new Guid("00000000-0000-0000-0000-000000000004")}, 1011, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = null, Uuid = new Guid("00000000-0000-0000-0000-000000000005")}, 1213, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = null, Uuid = new Guid("00000000-0000-0000-0000-000000000006")}, 1415, new MockIAdvertisementData()),
		   new  ScanResult(new MockDevice{ Name = null, Uuid = new Guid("00000000-0000-0000-0000-000000000007")}, 1617, new MockIAdvertisementData()),
		};

	}
}
