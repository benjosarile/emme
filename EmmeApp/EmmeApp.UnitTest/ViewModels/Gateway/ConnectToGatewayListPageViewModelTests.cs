﻿using Acr.UserDialogs;
using EmmeApp.Common.Constants;
using EmmeApp.ItemModels;
using EmmeApp.Mock;
using Moq;
using NUnit.Framework;
using Prism.Events;
using Prism.Navigation;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EmmeApp.Common.Utilities.Abstractions;

namespace EmmeApp.ViewModels
{
    [TestFixture]
    public class ConnectToGatewayListPageViewModelTests
    {
        private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);
        private Mock<IUserDialogs> _mockUserDialogs;
        private Mock<INavigationService> _mockNavigationService;
        private Mock<INavigationHelperService> _mockNavigationHelperService;
        private Mock<IEventAggregator> _mockEventAggregator;
		private Mock<IAppCenterCustomService> _mockAppCenterService;

		private ConnectToGatewayListPageViewModel _systemUnderTest;

        [SetUp]
        public void Setup()
        {
            _mockRepository.Create<INavigationService>();
            _mockNavigationService = _mockRepository.Create<INavigationService>();
            _mockNavigationHelperService = _mockRepository.Create<INavigationHelperService>();
            _mockUserDialogs = _mockRepository.Create<IUserDialogs>();
            _mockEventAggregator = _mockRepository.Create<IEventAggregator>();
			_mockAppCenterService = _mockRepository.Create<IAppCenterCustomService>();

			_mockAppCenterService.Setup(o => o.TrackError(It.IsAny<Exception>(), It.IsAny<Dictionary<string, string>>()));
			_mockAppCenterService.Setup(o => o.TrackEvent(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>()));

			_systemUnderTest = new ConnectToGatewayListPageViewModel(
                _mockNavigationService.Object,
                _mockNavigationHelperService.Object,
				_mockAppCenterService.Object,
                _mockUserDialogs.Object,
                _mockEventAggregator.Object);
        }

        [Test]
        public void Ctor_Always_PerformsExpectedWork()
        {
            _mockNavigationService.Verify();
            _mockNavigationHelperService.Verify();
            _mockUserDialogs.Verify();
            _mockEventAggregator.Verify();
            _mockRepository.Verify();
        }

        [Test]
        public void OnNavigatedTo_OnNavigationModeNew_GatewayListShouldNotBeNull()
        {
            var navParams = new NavigationParameters();
            List<GatewayDeviceItemModel> devices = new List<GatewayDeviceItemModel>();
            
            navParams.Add(NavigationConstants.GatewayConnectionListModel, devices);
            _mockNavigationHelperService.Setup(o => o.GetNavigationMode(navParams)).Returns(NavigationMode.New);

            _systemUnderTest.OnNavigatedTo(navParams);

            _systemUnderTest.GatewayList.Count.ShouldBe(0);
            _systemUnderTest.HelpResourceKey.ShouldBe(ResourceConstants.Connect_To_Gateway_No_Devices_Found_Help);
            _systemUnderTest.GatewayList.ShouldNotBeNull();
        }

        [Test]
        public void OnNavigatedTo_OnNavigationModeNew_GatewayListCountShouldGreaterThanZero()
        {
            var navParams = new NavigationParameters();
            List<GatewayDeviceItemModel> devices = _mockData;

            navParams.Add(NavigationConstants.GatewayConnectionListModel, devices);
            _mockNavigationHelperService.Setup(o => o.GetNavigationMode(navParams)).Returns(NavigationMode.New);

            _systemUnderTest.OnNavigatedTo(navParams);

            _systemUnderTest.GatewayList.Count.ShouldBe(3);
            _systemUnderTest.HelpResourceKey.ShouldBe(ResourceConstants.Connect_To_Gateway_Devices_Found_Help);
            _systemUnderTest.GatewayList.ShouldNotBeNull();
        }

        [Test]
        public void OnNavigatedTo_OnShouldExitConnection_ShouldGoBack()
        {
            int numberOfCalls = 0;

            NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
            {
                ++numberOfCalls;
                return Task.FromResult<INavigationResult>(null);
            };

            var param = new NavigationParameters {{NavigationConstants.ShouldExitConnection, ""}};

            _mockNavigationHelperService.Setup(o => o.GetNavigationMode(param)).Returns(NavigationMode.New);

            
            _systemUnderTest.OnNavigatedTo(param);

            numberOfCalls.ShouldBe(1);
        }


        [Test]
        public void BackCommand_OnBackButtonClicked_ShouldGoBack()
        {
            var numberOfCalls = 0;

            NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
            {
                ++numberOfCalls;
                return Task.FromResult<INavigationResult>(null);
            };
            
            _systemUnderTest.BackCommand?.Execute();

            numberOfCalls.ShouldBe(1);
        }

        [Test]
        public void SelectGatewayCommand_OnSelectGatewayItemTapped_ShouldNavigate()
        {
            var numberOfCalls = 0;

            NavigationUtilities.NavigateAsync = (navSvc, page, navParams, isModal, isAnimated) =>
            {
                if (page.Equals(ViewNames.GatewaySubPage))
                {
                    numberOfCalls++;
                }
                return Task.FromResult<INavigationResult>(null);
            };

            var device = new GatewayDeviceItemModel
            {
                Device = new MockDevice { Name = "Biral", Uuid = new Guid("00000000-0000-0000-0000-000000000001") },
                Name = "Biral"
            };
            
            _systemUnderTest.SelectGatewayCommand?.Execute(device);

            numberOfCalls.ShouldBe(1);
        }

        [Test]
        public void TryAgainCommand_OnTryAgainClicked_ShouldGoBack()
        {
            int numberOfCalls = 0;

            NavigationUtilities.GoBackAsync = (navSvc, navParams, isModal, isAnimated) =>
            {
                ++numberOfCalls;
                return Task.FromResult<INavigationResult>(null);
            };
            
            _systemUnderTest.TryAgainCommand?.Execute();

            numberOfCalls.ShouldBe(1);
        }


        private readonly List<GatewayDeviceItemModel> _mockData = new List<GatewayDeviceItemModel>
        {
            new GatewayDeviceItemModel
            {
                Device = new MockDevice { Name = "Biral", Uuid = new Guid("00000000-0000-0000-0000-000000000001") },
                Name = "Biral"
            },
            new GatewayDeviceItemModel
            {
                Device = new MockDevice { Name = "Biral", Uuid = new Guid("00000000-0000-0000-0000-000000000002") },
                Name = "Biral"
            },
            new GatewayDeviceItemModel
            {
                Device = new MockDevice { Name = "Biral", Uuid = new Guid("00000000-0000-0000-0000-000000000003") },
                Name = "Biral"
            },
        };
    }
}
