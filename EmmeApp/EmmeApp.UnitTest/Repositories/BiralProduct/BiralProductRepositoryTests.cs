﻿using EmmeApp.DataObjects;
using Moq;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using EmmeApp.Repositories.Abstractions;

namespace EmmeApp.Repositories
{
	[TestFixture]
	public class BiralProductRepositoryTests
	{
		private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);

		private Mock<IMobileDatabase> _mockMobileDatabase;

		private BiralProductRepository _systemUnderTest;

		[SetUp]
		public void Setup()
		{

			_mockMobileDatabase = _mockRepository.Create<IMobileDatabase>();

			_mockMobileDatabase.Setup(mr => mr.Count<BiralProductDataObject>()).Returns(_mockData.Count);
			_mockMobileDatabase.Setup(mr => mr.Count(It.IsAny<Expression<Func<BiralProductDataObject, bool>>>()))
				.Returns((Expression<Func<BiralProductDataObject, bool>> predicate) =>
				{
					var func = predicate.Compile();
					return _mockData.Count(func);
				});

			_mockMobileDatabase.Setup(mr => mr.DeleteAll<BiralProductDataObject>()).Returns(1);
			_mockMobileDatabase.Setup(mr => mr.DeleteItem<BiralProductDataObject>(It.IsAny<long>())).Returns(1);
			_mockMobileDatabase.Setup(mr => mr.BulkInsert(It.IsAny<List<BiralProductDataObject>>()));

			_mockMobileDatabase.Setup(mr => mr.GetAll<BiralProductDataObject>()).Returns(_mockData);
			_mockMobileDatabase.Setup(mr => mr.GetItem<BiralProductDataObject>(It.IsAny<long>()))
				.Returns((long i) => _mockData.FirstOrDefault(x => x.Id == i));

			_mockMobileDatabase.Setup(mr => mr.FirstOrDefault(It.IsAny<Expression<Func<BiralProductDataObject, bool>>>()))
					.Returns((Expression<Func<BiralProductDataObject, bool>> predicate) =>
					{
						var func = predicate.Compile();
						return _mockData.FirstOrDefault(func);
					});

			_mockMobileDatabase.Setup(mr => mr.SaveItem(It.IsAny<BiralProductDataObject>()))
				.Returns((BiralProductDataObject item) => 1);

			_systemUnderTest = new BiralProductRepository(
				_mockMobileDatabase.Object);

		}

		[Test]
		public void Ctor_Always_PerformsExpectedWork()
		{
			_mockMobileDatabase.Verify();
			_mockRepository.Verify();
		}


		[Test]
		public void Count_CountAll_ReturnEqualsToMockData()
		{
			int totalCount = _systemUnderTest.Count();
			int dataCount = _mockData.Count;

			totalCount.ShouldBe(dataCount);
		}

		[Test]
		public void Count_CountWhereBiralProductIdIsGreaterThan3_ReturnTwoDataObjects()
		{
			int totalCount = _systemUnderTest.Count(x => x.Id > 3);
			int dataCount = _mockData.Count(x => x.Id > 3);

			totalCount.ShouldBe(dataCount);
		}

		[TestCase(1, 2205290150)]
		[TestCase(2, 2205300150)]
		[TestCase(3, 2205280150)]
		[TestCase(4, 2205310150)]
		[TestCase(5, 2204520350)]
		[TestCase(6, 2204530350)]
		[TestCase(7, 2204540350)]
		[TestCase(8, 2204550350)]
		[TestCase(9, 2205360150)]
		[TestCase(10, 2205370150)]
		public void FindBy_ExistingSingleBiralProductById_ReturnsSingleData(long id, long productId)
		{
			BiralProductDataObject testObject = _systemUnderTest.FirstOrDefault(x => x.Id == id);

			testObject.ShouldNotBeNull();
			testObject.ShouldBeOfType<BiralProductDataObject>();
			testObject.ProductId.ShouldBe(productId);
			testObject.Id.ShouldBe(id);
		}

		[Test]
		public void SaveItem_Always_Success()
		{
			BiralProductDataObject dto = new BiralProductDataObject()
			{
				Id = 11,
				Type = "PrimAX 15-4 130 RED",
				Diameter = "G 1\"",
				Pressure = "PN 10",
				Length = "130 mm",
				Engine = "1x230 V",
				Category = 3,
				MarketKey = "ch",
				ProductId = 2205290150
			};

			long result = _systemUnderTest.Save(dto);

			result.ShouldBe(1);
		}

		private readonly List<BiralProductDataObject> _mockData = new List<BiralProductDataObject>()
		{
			new BiralProductDataObject() {
				Id = 1,
				Type = "PrimAX 15-4 130 RED",
				Diameter ="G 1\"",
				Pressure ="PN 10",
				Length="130 mm",
				Engine= "1x230 V",
				Category= 3,
				MarketKey = "ch",
				ProductId = 2205290150
			},
			new BiralProductDataObject() {
				Id = 2,
				Type = "PrimAX 15-6 130 RED",
				Diameter = "G 1\"",
				Pressure ="PN 10",
				Length="130 mm",
				Engine= "1x230 V",
				Category= 3,
				MarketKey = "ch",
				ProductId = 2205300150
			},
			new BiralProductDataObject() {
				Id = 3,
				Type = "PrimAX 15-3 130 RED",
				Diameter = "G 1\"",
				Pressure ="PN 10",
				Length="130 mm",
				Engine= "1x230 V",
				Category= 3,
				MarketKey = "ch",
				ProductId = 2205280150
			},
			new BiralProductDataObject() {
				Id = 4,
				Type = "PrimAX 15-8 130 RED",
				Diameter = "G 1\"",
				Pressure ="PN 10",
				Length="130 mm",
				Engine= "1x230 V",
				Category= 3,
				MarketKey = "ch",
				ProductId = 2205310150
			},
			new BiralProductDataObject() {
				Id = 5,
				Type = "ModulA 25-4 180 RED",
				Diameter ="G 1½\"",
				Pressure ="PN 10",
				Length="180 mm",
				Engine= "1x230 V",
				Category= 3,
				MarketKey = "ch",
				ProductId = 2204520350
			},
			new BiralProductDataObject() {
				Id = 6,
				Type = "ModulA 25-6 180 RED",
				Diameter ="G 1½\"",
				Pressure ="PN 10",
				Length="180 mm",
				Engine= "1x230 V",
				Category= 3,
				MarketKey = "ch",
				ProductId = 2204530350
			},
			new BiralProductDataObject() {
				Id = 7,
				Type = "ModulA 25-8 180 RED",
				Diameter ="G 1½\"",
				Pressure ="PN 10",
				Length="180 mm",
				Engine= "1x230 V",
				Category= 3,
				MarketKey = "ch",
				ProductId = 2204540350
			},
			new BiralProductDataObject() {
				Id = 8,
				Type ="ModulA 25-10 180 RED",
				Diameter ="G 1½\"",
				Pressure ="PN 10",
				Length="180 mm",
				Engine= "1x230 V",
				Category= 3,
				MarketKey = "ch",
				ProductId = 2204550350
			},
			new BiralProductDataObject() {
				Id = 9,
				Type ="PrimAX 25-3 180 RED",
				Diameter ="G 1½\"",
				Pressure ="PN 10",
				Length="180 mm",
				Engine= "1x230 V",
				Category= 3,
				MarketKey = "ch",
				ProductId = 2205360150
			},
			new BiralProductDataObject() {
				Id = 10,
				Type = "PrimAX 25-4 180 RED",
				Diameter ="G 1½\"",
				Pressure ="PN 10",
				Length="180 mm",
				Engine= "1x230 V",
				Category= 3,
				MarketKey = "ch",
				ProductId = 2205370150
			},

		};
	}
}
