﻿using EmmeApp.DataObjects;
using Moq;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using EmmeApp.Repositories.Abstractions;

namespace EmmeApp.Repositories
{
	public class PumpReplacementRepositoryTests
	{
		private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);

		private Mock<IMobileDatabase> _mockMobileDatabase;

		private PumpReplacementRepository _systemUnderTest;

		[SetUp]
		public void Setup()
		{

			_mockMobileDatabase = _mockRepository.Create<IMobileDatabase>();

			_mockMobileDatabase.Setup(mr => mr.Count<PumpReplacementDataObject>()).Returns(_mockData.Count);
			_mockMobileDatabase.Setup(mr => mr.Count(It.IsAny<Expression<Func<PumpReplacementDataObject, bool>>>()))
				.Returns((Expression<Func<PumpReplacementDataObject, bool>> predicate) =>
				{
					var func = predicate.Compile();
					return _mockData.Count(func);
				});

			_mockMobileDatabase.Setup(mr => mr.DeleteAll<PumpReplacementDataObject>()).Returns(1);
			_mockMobileDatabase.Setup(mr => mr.DeleteItem<PumpReplacementDataObject>(It.IsAny<long>())).Returns(1);
			_mockMobileDatabase.Setup(mr => mr.BulkInsert(It.IsAny<List<PumpReplacementDataObject>>()));

			_mockMobileDatabase.Setup(mr => mr.GetAll<PumpReplacementDataObject>()).Returns(_mockData);
			_mockMobileDatabase.Setup(mr => mr.GetItem<PumpReplacementDataObject>(It.IsAny<long>()))
				.Returns((long i) => _mockData.FirstOrDefault(x => x.Id == i));

			_mockMobileDatabase.Setup(mr => mr.FirstOrDefault(It.IsAny<Expression<Func<PumpReplacementDataObject, bool>>>()))
					.Returns((Expression<Func<PumpReplacementDataObject, bool>> predicate) =>
					{
						var func = predicate.Compile();
						return _mockData.FirstOrDefault(func);
					});

			_mockMobileDatabase.Setup(mr => mr.SaveItem(It.IsAny<PumpReplacementDataObject>()))
				.Returns((PumpReplacementDataObject item) => 1);

			_systemUnderTest = new PumpReplacementRepository(
				_mockMobileDatabase.Object);

		}

		[Test]
		public void Ctor_Always_PerformsExpectedWork()
		{
			_mockMobileDatabase.Verify();
			_mockRepository.Verify();
		}


		[Test]
		public void Count_CountAll_ReturnEqualsToMockData()
		{
			int totalCount = _systemUnderTest.Count();
			int dataCount = _mockData.Count;

			totalCount.ShouldBe(dataCount);
		}

		[Test]
		public void Count_WherePumpReplacementIdIsGreaterThan3_ReturnTwoDataObjects()
		{
			int totalCount = _systemUnderTest.Count(x => x.Id > 3);
			int dataCount = _mockData.Count(x => x.Id > 3);

			totalCount.ShouldBe(dataCount);
		}

		[TestCase(1, "Biral AX 12-4")]
		[TestCase(2, "Biral AX 13-4")]
		[TestCase(3, "Biral AX 15-6 130 RED")]
		[TestCase(4, "Biral AX 15-6 130 RED")]
		[TestCase(5, "Biral M 10-4")]
		[TestCase(6, "Biral M 12-4")]
		[TestCase(7, "Biral M 13-4")]
		[TestCase(8, "Biral MX 10-4")]
		[TestCase(9, "Biral MX 12-4")]
		[TestCase(10,"Biral MX 13-4")]
		public void FindBy_ExistingSinglePumpReplacementById_ReturnsSingleData(long id, string type)
		{
			PumpReplacementDataObject testObject = _systemUnderTest.FirstOrDefault(x => x.Id == id);

			testObject.ShouldNotBeNull();
			testObject.ShouldBeOfType<PumpReplacementDataObject>();
			testObject.Type.ShouldBe(type);
			testObject.Id.ShouldBe(id);
		}

		[Test]
		public void SaveItem_Always_Succeed()
		{
			PumpReplacementDataObject dto = new PumpReplacementDataObject()
			{
				Id = 11,
				Diameter = "1 1/2\"",
				Engine = "1x230V",
				Length = "180",
				MarketKey = "ch",
				Pressure = "-NA-",
				Type = "Speck NE 25/30",
			};

			long result = _systemUnderTest.Save(dto);

			result.ShouldBe(1);
		}

		private readonly List<PumpReplacementDataObject> _mockData = new List<PumpReplacementDataObject>()
		{
			new PumpReplacementDataObject()
			{
				 Id = 1,
				 Diameter = "1\"",
				 Engine= "1x230V",
				 Length=  "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral AX 12-4",
			},
			new PumpReplacementDataObject()
			{
				 Id = 2,
				 Diameter = "1\"",
				 Engine= "1x230V",
				 Length=  "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral AX 13-4",
			},
			new PumpReplacementDataObject()
			{
				 Id = 3,
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral AX 15-6 130 RED",
			},
			new PumpReplacementDataObject()
			{
				 Id = 4,
				 Diameter = "1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral AX 15-6 130 RED",
			},
			new PumpReplacementDataObject()
			{
				 Id = 5,
				 Diameter = "1\"",
				 Engine= "1x230V",
				 Length=  "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =   "Biral M 10-4",
			},
			new PumpReplacementDataObject()
			{
				 Id = 6,
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral M 12-4",
			},
			new PumpReplacementDataObject()
			{
				 Id = 7,
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type = "Biral M 13-4",
			},
			new PumpReplacementDataObject()
			{
				 Id = 8,
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral MX 10-4",
			},
			new PumpReplacementDataObject()
			{
				 Id = 9,
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =   "Biral MX 12-4",
			},
			new PumpReplacementDataObject()
			{
				 Id = 10,
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =   "Biral MX 13-4",
			},

		};
	}
}
