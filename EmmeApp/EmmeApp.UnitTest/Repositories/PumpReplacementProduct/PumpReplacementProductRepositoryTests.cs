﻿using EmmeApp.DataObjects;
using Moq;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using EmmeApp.Repositories.Abstractions;

namespace EmmeApp.Repositories
{
	public class PumpReplacementProductRepositoryTests
	{
		private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);

		private Mock<IMobileDatabase> _mockMobileDatabase;

		private PumpReplacementProductRepository _systemUnderTest;

		[SetUp]
		public void Setup()
		{

			_mockMobileDatabase = _mockRepository.Create<IMobileDatabase>();

			_mockMobileDatabase.Setup(mr => mr.Count<PumpReplacementProductDataObject>()).Returns(_mockData.Count);
			_mockMobileDatabase.Setup(mr => mr.Count(It.IsAny<Expression<Func<PumpReplacementProductDataObject, bool>>>()))
				.Returns((Expression<Func<PumpReplacementProductDataObject, bool>> predicate) =>
				{
					var func = predicate.Compile();
					return _mockData.Count(func);
				});

			_mockMobileDatabase.Setup(mr => mr.DeleteAll<PumpReplacementProductDataObject>()).Returns(1);
			_mockMobileDatabase.Setup(mr => mr.DeleteItem<PumpReplacementProductDataObject>(It.IsAny<long>())).Returns(1);
			_mockMobileDatabase.Setup(mr => mr.BulkInsert(It.IsAny<List<PumpReplacementProductDataObject>>()));

			_mockMobileDatabase.Setup(mr => mr.GetAll<PumpReplacementProductDataObject>()).Returns(_mockData);
			_mockMobileDatabase.Setup(mr => mr.GetItem<PumpReplacementProductDataObject>(It.IsAny<long>()))
				.Returns((long i) => _mockData.FirstOrDefault(x => x.Id == i));

			_mockMobileDatabase.Setup(mr => mr.FirstOrDefault(It.IsAny<Expression<Func<PumpReplacementProductDataObject, bool>>>()))
					.Returns((Expression<Func<PumpReplacementProductDataObject, bool>> predicate) =>
					{
						var func = predicate.Compile();
						return _mockData.FirstOrDefault(func);
					});

			_mockMobileDatabase.Setup(mr => mr.SaveItem(It.IsAny<PumpReplacementProductDataObject>()))
				.Returns((PumpReplacementProductDataObject item) => 1);

			_systemUnderTest = new PumpReplacementProductRepository(
				_mockMobileDatabase.Object);

		}

		[Test]
		public void PumpReplacementProductRepository_ConstructorAlways_PerformsExpectedWork()
		{
			_mockMobileDatabase.Verify();
			_mockRepository.Verify();
		}


		[Test]
		public void PumpReplacementProductRepository_CountAll_ReturnEqualsToMockData()
		{
			var totalCount = _systemUnderTest.Count();
			var dataCount = _mockData.Count;

			totalCount.ShouldBe(dataCount);
		}

		[Test]
		public void PumpReplacementProductRepository_CountWherePumpReplacementProductIdIsGreaterThan3_ReturnTwoDataObjects()
		{
			var totalCount = _systemUnderTest.Count(x => x.Id > 3);
			var dataCount = _mockData.Count(x => x.Id > 3);

			totalCount.ShouldBe(dataCount);
		}

		[TestCase(1, 2205290150)]
		[TestCase(2, 2206350150)]
		[TestCase(3, 2205300150)]
		[TestCase(4, 2206360150)]
		[TestCase(5, 2205290150)]
		[TestCase(6, 2206350150)]
		[TestCase(7, 2205300150)]
		[TestCase(8, 2206360150)]
		[TestCase(9, 2205290150)]
		[TestCase(10, 2206350150)]
		public void PumpReplacementProductRepository_FindByExistingSingleMasterPumpReplacementById_ReturnsSingleData(long id, long productId)
		{
			PumpReplacementProductDataObject testObject = _systemUnderTest.FirstOrDefault(x => x.Id == id);

			testObject.ShouldNotBeNull();
			testObject.ShouldBeOfType<PumpReplacementProductDataObject>();
			testObject.ProductId.ShouldBe(productId);
			testObject.Id.ShouldBe(id);
		}

		[Test]
		public void PumpReplacementProductRepository_SaveItem_AlwaysSuccess()
		{
			PumpReplacementProductDataObject dto = new PumpReplacementProductDataObject()
			{
				Id = 11,
				CommentCodes = "",
				ConnectorInformation = "-",
				MarketKey = "ch",
				ProductId = 2205290150,
				Type = "Biral M 12-4"
			};

			long result = _systemUnderTest.Save(dto);

			result.ShouldBe(1);
		}

		private readonly List<PumpReplacementProductDataObject> _mockData = new List<PumpReplacementProductDataObject>()
		{
			new PumpReplacementProductDataObject()
			{
				 Id = 1,
				 CommentCodes = "",
				 ConnectorInformation = "-",
				 MarketKey = "ch",
				 ProductId = 2205290150,
				 Type = "Biral AX 12-4"
			},
			new PumpReplacementProductDataObject()
			{
				 Id = 2,
				 CommentCodes = "",
				 ConnectorInformation = "-",
				 MarketKey = "ch",
				 ProductId = 2206350150,
				 Type = "Biral AX 12-4"
			},
			new PumpReplacementProductDataObject()
			{
				 Id = 3,
				 CommentCodes = "",
				 ConnectorInformation = "-",
				 MarketKey = "ch",
				 ProductId = 2205300150,
				 Type = "Biral AX 13-4"
			},
			new PumpReplacementProductDataObject()
			{
				 Id = 4,
				 CommentCodes = "",
				 ConnectorInformation = "-",
				 MarketKey = "ch",
				 ProductId = 2206360150,
				 Type = "Biral AX 13-4"
			},
			new PumpReplacementProductDataObject()
			{
				 Id = 5,
				 CommentCodes = "",
				 ConnectorInformation = "-",
				 MarketKey = "ch",
				 ProductId = 2205290150,
				 Type = "Biral AX 15-4 130 RED"
			},
			new PumpReplacementProductDataObject()
			{
				 Id = 6,
				 CommentCodes = "",
				 ConnectorInformation = "-",
				 MarketKey = "ch",
				 ProductId = 2206350150,
				 Type = "Biral AX 15-4 130 RED"
			},
			new PumpReplacementProductDataObject()
			{
				 Id = 7,
				 CommentCodes = "",
				 ConnectorInformation = "-",
				 MarketKey = "ch",
				 ProductId = 2205300150,
				 Type = "Biral AX 15-6 130 RED"
			},
			new PumpReplacementProductDataObject()
			{
				 Id = 8,
				 CommentCodes = "",
				 ConnectorInformation = "-",
				 MarketKey = "ch",
				 ProductId = 2206360150,
				 Type = "Biral AX 15-6 130 RED"
			},
			new PumpReplacementProductDataObject()
			{
				 Id = 9,
				 CommentCodes = "",
				 ConnectorInformation = "-",
				 MarketKey = "ch",
				 ProductId = 2205290150,
				 Type = "Biral M 10-4"
			},
			new PumpReplacementProductDataObject()
			{
				 Id = 10,
				 CommentCodes = "",
				 ConnectorInformation = "-",
				 MarketKey = "ch",
				 ProductId = 2206350150,
				 Type = "Biral M 10-4"
			},
		};
	}
}
