﻿using EmmeApp.DataObjects;
using Moq;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using EmmeApp.Repositories.Abstractions;

namespace EmmeApp.Repositories
{
	[TestFixture]
	public class MasterPumpReplacementRepositoryTests
	{
		private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);

		private Mock<IMobileDatabase> _mockMobileDatabase;

		private MasterPumpReplacementRepository _systemUnderTest;

		[SetUp]
		public void Setup()
		{

			_mockMobileDatabase = _mockRepository.Create<IMobileDatabase>();

			_mockMobileDatabase.Setup(mr => mr.Count<MasterPumpReplacementDataObject>()).Returns(_mockMasterData.Count);
			_mockMobileDatabase.Setup(mr => mr.Count(It.IsAny<Expression<Func<MasterPumpReplacementDataObject, bool>>>()))
				.Returns((Expression<Func<MasterPumpReplacementDataObject, bool>> predicate) =>
				{
					var func = predicate.Compile();
					return _mockMasterData.Count(func);
				});

			_mockMobileDatabase.Setup(mr => mr.DeleteAll<MasterPumpReplacementDataObject>()).Returns(1);
			_mockMobileDatabase.Setup(mr => mr.DeleteItem<MasterPumpReplacementDataObject>(It.IsAny<long>())).Returns(1);
			_mockMobileDatabase.Setup(mr => mr.BulkInsert(It.IsAny<List<MasterPumpReplacementDataObject>>()));

			_mockMobileDatabase.Setup(mr => mr.GetAll<MasterPumpReplacementDataObject>()).Returns(_mockMasterData);
			_mockMobileDatabase.Setup(mr => mr.GetItem<MasterPumpReplacementDataObject>(It.IsAny<long>()))
				.Returns((long i) => _mockMasterData.FirstOrDefault(x => x.Id == i));

			_mockMobileDatabase.Setup(mr => mr.FirstOrDefault(It.IsAny<Expression<Func<MasterPumpReplacementDataObject, bool>>>()))
					.Returns((Expression<Func<MasterPumpReplacementDataObject, bool>> predicate) =>
					{
						var func = predicate.Compile();
						return _mockMasterData.FirstOrDefault(func);
					});

			_mockMobileDatabase.Setup(mr => mr.SaveItem(It.IsAny<MasterPumpReplacementDataObject>()))
				.Returns((MasterPumpReplacementDataObject item) => 1);

			_systemUnderTest = new MasterPumpReplacementRepository(
				_mockMobileDatabase.Object);

		}

		[Test]
		public void Ctor_Always_PerformsExpectedWork()
		{
			_mockMobileDatabase.Verify();
			_mockRepository.Verify();
		}

		
		[Test]
		public void Count_CountAllMasterData_ReturnEqualsToMockData()
		{
			int totalCount = _systemUnderTest.Count();
			int dataCount = _mockMasterData.Count;

			totalCount.ShouldBe(dataCount);
		}

		[Test]
		public void Count_MasterDataWhereMasterDataIdIsGreaterThan3_ReturnTwoDataObjects()
		{
			int totalCount = _systemUnderTest.Count(x => x.Id > 3);
			int dataCount = _mockMasterData.Count(x => x.Id > 3);

			totalCount.ShouldBe(2);
			totalCount.ShouldBe(dataCount);
		}

		[TestCase(1, "2205290150")]
		[TestCase(2, "2205300150")]
		[TestCase(3, "2205290150")]
		[TestCase(4, "2205300150")]
		[TestCase(5, "2205290150")]
		public void FindBy_ExistingSingleMasterPumpReplacementById_ReturnsSingleData(long id, string articleNumber)
		{
			MasterPumpReplacementDataObject testObject = _systemUnderTest.FirstOrDefault(x => x.Id == id);

			testObject.ShouldNotBeNull();
			testObject.ShouldBeOfType<MasterPumpReplacementDataObject>();
			testObject.ArticleNumber.ShouldBe(articleNumber);
			testObject.Id.ShouldBe(id);
		}

		[Test]
		public void SaveItem_Always_Success()
		{
			MasterPumpReplacementDataObject dto = new MasterPumpReplacementDataObject
			{
				ArticleNumber = "2205290150",
				CommentCodes = "-NA-",
				Diameter = "1\"",
				DiameterUnit = "G",
				Engine = "1x230V",
				Id = 6,
				Length = "130",
				LengthUnit = "mm",
				Pressure = "10",
				PressureUnit = "PN",
				Type = "Biral M 10-4"
			};

			long result = _systemUnderTest.Save(dto);

			result.ShouldBe(1);
		}


		private readonly List<MasterPumpReplacementDataObject> _mockMasterData = new List<MasterPumpReplacementDataObject>
		{
			new MasterPumpReplacementDataObject
			{
				ArticleNumber = "2205290150",
				CommentCodes = "-NA-",
				Diameter = "1\"",
				DiameterUnit =  "G",
				Engine = "1x230V",
				Id = 1,
				Length = "130",
				LengthUnit ="mm",
				Pressure = "10",
				PressureUnit = "PN",
				Type = "Biral AX 12-4"
			},
			new MasterPumpReplacementDataObject
			{
				ArticleNumber = "2205300150",
				CommentCodes = "-NA-",
				Diameter = "1\"",
				DiameterUnit =  "G",
				Engine = "1x230V",
				Id = 2,
				Length = "130",
				LengthUnit ="mm",
				Pressure = "10",
				PressureUnit = "PN",
				Type = "Biral AX 13-4"
			},
			new MasterPumpReplacementDataObject
			{
				ArticleNumber = "2205290150",
				CommentCodes = "-NA-",
				Diameter = "1\"",
				DiameterUnit =  "G",
				Engine = "1x230V",
				Id = 3,
				Length = "130",
				LengthUnit ="mm",
				Pressure = "10",
				PressureUnit = "PN",
				Type = "Biral AX 15-4 130 RED"
			},
			new MasterPumpReplacementDataObject
			{
				ArticleNumber = "2205300150",
				CommentCodes = "-NA-",
				Diameter = "1\"",
				DiameterUnit =  "G",
				Engine = "1x230V",
				Id = 4,
				Length = "130",
				LengthUnit ="mm",
				Pressure = "10",
				PressureUnit = "PN",
				Type = "Biral AX 15-6 130 RED"
			},
			new MasterPumpReplacementDataObject
			{
				ArticleNumber = "2205290150",
				CommentCodes = "-NA-",
				Diameter = "1\"",
				DiameterUnit =  "G",
				Engine = "1x230V",
				Id = 5,
				Length = "130",
				LengthUnit ="mm",
				Pressure = "10",
				PressureUnit = "PN",
				Type = "Biral M 10-4"
			},
		};


	}
}
