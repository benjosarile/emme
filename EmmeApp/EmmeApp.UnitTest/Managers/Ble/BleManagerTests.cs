﻿using EmmeApp.Common.Constants;
using EmmeApp.Entities;
using EmmeApp.LocalData.Models;
using EmmeApp.Mock;
using Moq;
using NUnit.Framework;
using Plugin.BluetoothLE;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using EmmeApp.Managers.Abstractions;
using EmmeApp.ProtocolServices.Abstractions;

namespace EmmeApp.Managers
{
	[TestFixture]
	public class BleManagerTests
	{
		private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);

		private Mock<IBleAdapterService> _mockBleAdapterService;
		private Mock<IAssemblyResourceManager> _mockFileManager;
		private Mock<IServiceEntityMapper> _mockServiceEntityMapper;
		private Mock<IProtocolBufferService> _mockProtocolBufferService;
		private Mock<IJsonService> _mockJsonService;
		private Mock<IAdapter> _mockAdapter;
		private BleManager _systemUnderTest;

		[SetUp]
		public void Setup()
		{
			_mockBleAdapterService = _mockRepository.Create<IBleAdapterService>();
			_mockFileManager = _mockRepository.Create<IAssemblyResourceManager>();
			_mockServiceEntityMapper = _mockRepository.Create<IServiceEntityMapper>();
			_mockProtocolBufferService = _mockRepository.Create<IProtocolBufferService>();
			_mockJsonService = _mockRepository.Create<IJsonService>();
			_mockAdapter = _mockRepository.Create<IAdapter>();
			string fileContent = "{\"services\":[{\"name\":\"NORDIC UART Service\",\"uuid\":\"6E400001-B5A3-F393-E0A9-E50E24DCCA9E\",\"characteristics\":[{\"name\":\"RX Characteristic\",\"uuid\":\"6E400002-B5A3-F393-E0A9-E50E24DCCA9E\",\"role\":\"Write\"},{\"name\":\"TX Characteristic\",\"uuid\":\"6E400003-B5A3-F393-E0A9-E50E24DCCA9E\",\"role\":\"Notification\"}]}]}";


			_mockFileManager.Setup(o => o.GetResourceAsString(It.Is<string>(x => x == AssemblyResourceNames.BleServices))).Returns(fileContent);

			_mockJsonService.Setup(o => o.DeserializeObject<BleServiceRootLocalData>(It.Is<string>(x => x == fileContent))).Returns(new BleServiceRootLocalData()
			{
				BleServices = new List<BleServiceLocalData>()
				{
					new BleServiceLocalData() { Name = "NORDIC UART Service", Uuid = "6E400001-B5A3-F393-E0A9-E50E24DCCA9E",
						Characteristics = new List<BleServiceCharacteristicLocalData>() {
							new BleServiceCharacteristicLocalData() { Name = "RX Characteristic", Role = "Write", Uuid = "6E400002-B5A3-F393-E0A9-E50E24DCCA9E" },
							new BleServiceCharacteristicLocalData() { Name = "TX Characteristic", Role = "Notification", Uuid = "6E400003-B5A3-F393-E0A9-E50E24DCCA9E" }
						}
					}
				}
			});

			_mockServiceEntityMapper.Setup(mr => mr.Map<List<BleServiceEntity>>(It.IsAny<List<BleServiceLocalData>>())).Returns(new List<BleServiceEntity>()
			{
					new BleServiceEntity() { Name = "NORDIC UART Service", Uuid = "6E400001-B5A3-F393-E0A9-E50E24DCCA9E",
						Characteristics = new List<BleServiceCharacteristicEntity>() {
							new BleServiceCharacteristicEntity() { Name = "RX Characteristic", Role = "Write", Uuid = "6E400002-B5A3-F393-E0A9-E50E24DCCA9E" },
							new BleServiceCharacteristicEntity() { Name = "TX Characteristic", Role = "Notification", Uuid = "6E400003-B5A3-F393-E0A9-E50E24DCCA9E" }
						}
					}
			});

			_mockBleAdapterService.Setup(o => o.GetConnectedDevicesAsync(It.Is<Guid>(x => x == Guid.Parse("6E400001-B5A3-F393-E0A9-E50E24DCCA9E")))).ReturnsAsync(() => new List<IDevice>() { new MockDevice() { Name = "Biral_PA", Uuid = Guid.Parse("1DF2F1A3-A0A8-4D69-87F9-96D5C9B88E73") } });

			_systemUnderTest = new BleManager(_mockAdapter.Object,
				_mockBleAdapterService.Object,
				_mockServiceEntityMapper.Object,
				_mockFileManager.Object,
				_mockProtocolBufferService.Object,
				_mockJsonService.Object);
		}

		[Test]
		public void Ctor_Always_PerformsExpectedWork()
		{
			_mockBleAdapterService.Verify();
			_mockFileManager.Verify();
			_mockServiceEntityMapper.Verify();
			_mockProtocolBufferService.Verify();
			_mockRepository.Verify();
		}

		[Test]
		public void GetAvailableServices_FileHasValidContent_ReturnsList()
		{
			var data = _systemUnderTest.GetAvailableServices();

			data.Count.ShouldBeGreaterThan(0);
		}

		[Test]
		public void GetAvailableServices_FileHasInvalidContent_ReturnsNothing()
		{
			string fileContent = "{\"services\":[{\"n\"Notification\"}]}]}";

			_mockFileManager.Setup(o => o.GetResourceAsString(It.Is<string>(x => x == AssemblyResourceNames.BleServices))).Returns(fileContent);

			_mockJsonService.Setup(o => o.DeserializeObject<BleServiceRootLocalData>(It.Is<string>(x => x == fileContent))).Returns(new BleServiceRootLocalData());

			_mockServiceEntityMapper.Setup(mr => mr.Map<List<BleServiceEntity>>(It.IsAny<List<BleServiceLocalData>>())).Returns(new List<BleServiceEntity>());

			var data = _systemUnderTest.GetAvailableServices();

			data.Count.ShouldBe(0);
		}

		[Test]
		public void GetConnectedDevice_HasListOfConnectedDeviceAndDevicesAreCapableOfNordicService_ShouldReturnFirst()
		{
			_mockBleAdapterService.Setup(o => o.GetConnectedDevices(It.IsAny<Guid?>())).Returns(
				() =>
				{
					return Observable.Create<List<IDevice>>(o => Observable.Repeat(new List<IDevice>()
					{
						{ new MockDevice()
							{
								Name = "Biral_PA",
								Uuid = Guid.Parse("1DF2F1A3-A0A8-4D69-87F9-96D5C9B88E73")
							}
						}
					}, 1).Subscribe(o));
				});

			var data = _systemUnderTest.GetConnectedDevice().Result;
			data.ShouldNotBe(null);
		}

		[Test]
		public void GetConnectedDevice_HasListOfConnectedDeviceAndDevicesAreNotCapableOfNordicService_ShouldReturnNull()
		{
			_mockServiceEntityMapper.Setup(mr => mr.Map<List<BleServiceEntity>>(It.IsAny<List<BleServiceLocalData>>())).Returns(new List<BleServiceEntity>()
			{
					new BleServiceEntity() { Name = "NORDIC UART Service", Uuid = "6E40000A-B5A3-F393-E0A9-E50E24DCCA9E"
					}
			});

			_mockBleAdapterService.Setup(o => o.GetConnectedDevices(It.IsAny<Guid?>())).Returns(
				() =>
				{
					return Observable.Create<List<IDevice>>(o => Observable.Repeat(new List<IDevice>()
						, 1).Subscribe(o));
				});

			var data = _systemUnderTest.GetConnectedDevice().Result;

			data.ShouldBe(null);
		}
	}
}