﻿using EmmeApp.DataObjects;
using EmmeApp.Entities;
using Moq;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.Linq;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Repositories.Abstractions;

namespace EmmeApp.Managers
{
	[TestFixture]
	public class PumpReplacementManagerTests
	{
		private readonly MockRepository _mockRepository = new MockRepository(MockBehavior.Strict);

		private Mock<IServiceEntityMapper> _mockMapper;
		private Mock<IPumpReplacementRepository> _mockPumpReplacementRepository;
		private Mock<IMarketManager> _mockMarketManager;
		private PumpReplacementManager _systemUnderTest;

		[SetUp]
		public void Setup()
		{
			_mockMapper = new Mock<IServiceEntityMapper>();
			_mockPumpReplacementRepository = new Mock<IPumpReplacementRepository>();
			_mockMarketManager = new Mock<IMarketManager>();
			_mockPumpReplacementRepository.Setup(mr => mr.GetAll()).Returns(_mockDtoData);

			_mockMapper.Setup(mr => mr.Map<List<PumpReplacementEntity>>(It.IsAny<List<PumpReplacementDataObject>>()))
				.Returns(new List<PumpReplacementEntity>());

			_systemUnderTest = new PumpReplacementManager(_mockMapper.Object, _mockPumpReplacementRepository.Object, _mockMarketManager.Object);
		}

		[Test]
		public void Ctor_Always_PerformsExpectedWork()
		{
			_mockMapper.Verify();
			_mockPumpReplacementRepository.Verify();
			_mockRepository.Verify();
		}

		[TestCase("ch", "Biral AX 12-4")]
		[TestCase("ch", "Biral AX 13-4")]
		[TestCase("ch", "Biral AX 15-6 130 RED")]
		[TestCase("ch", "Biral AX 15-6 130 RED")]
		[TestCase("ch", "Biral M 10-4")]
		[TestCase("ch", "Biral M 12-4")]
		[TestCase("ch", "Biral M 13-4")]
		[TestCase("ch", "Biral MX 10-4")]
		[TestCase("ch", "Biral MX 12-4")]
		[TestCase("ch", "Biral MX 13-4")]
		public void GetOldPumpDetails_GetExistingData_ShouldReturnEntity(string marketKey, string pumpType)
		{
			var mockReturnDto = _mockDtoData.FirstOrDefault(x => x.Type.Equals(pumpType));
			var mockReturnEntity = _mockEntityData.FirstOrDefault(x => x.Type.Equals(pumpType));

			_mockMapper.Setup(mr => mr.Map<PumpReplacementEntity>(It.Is<PumpReplacementDataObject>(x => x == mockReturnDto)))
				.Returns(mockReturnEntity);

			var returnData = _systemUnderTest.GetOldPumpDetails(pumpType);

			returnData.ShouldBeOfType(typeof(PumpReplacementEntity));
			returnData.ShouldNotBeNull();
			returnData.ShouldBe(mockReturnEntity);
		}

		[TestCase("ch", "Poso Negro AX 12-4")]
		[TestCase("ch", "Poso Negro AX 13-4")]
		[TestCase("ch", "Poso Negro AX 15-6 130 RED")]
		[TestCase("ch", "Poso Negro AX 15-6 130 RED")]
		[TestCase("ch", "Poso Negro M 10-4")]
		[TestCase("ch", "Poso Negro M 12-4")]
		[TestCase("ch", "Poso Negro M 13-4")]
		[TestCase("ch", "Poso Negro MX 10-4")]
		[TestCase("ch", "Poso Negro MX 12-4")]
		[TestCase("ch", "Poso Negro MX 13-4")]
		public void GetOldPumpDetails_GetDataThatDoesntExist_ShouldReturnNull(string marketKey, string pumpType)
		{
			var mockReturnDto = _mockDtoData.FirstOrDefault(x => x.Type.Equals(pumpType));
			var mockReturnEntity = _mockEntityData.FirstOrDefault(x => x.Type.Equals(pumpType));

			_mockMapper.Setup(mr => mr.Map<PumpReplacementEntity>(It.Is<PumpReplacementDataObject>(x => x == mockReturnDto)))
				.Returns(mockReturnEntity);

			var returnData = _systemUnderTest.GetOldPumpDetails(pumpType);

			returnData.ShouldBeNull();
		}

		private readonly List<PumpReplacementDataObject> _mockDtoData = new List<PumpReplacementDataObject>()
		{
			new PumpReplacementDataObject()
			{
				 Id = 1,
				 Diameter = "1\"",
				 Engine= "1x230V",
				 Length=  "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral AX 12-4",
			},
			new PumpReplacementDataObject()
			{
				 Id = 2,
				 Diameter = "1\"",
				 Engine= "1x230V",
				 Length=  "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral AX 13-4",
			},
			new PumpReplacementDataObject()
			{
				 Id = 3,
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral AX 15-6 130 RED",
			},
			new PumpReplacementDataObject()
			{
				 Id = 4,
				 Diameter = "1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral AX 15-6 130 RED",
			},
			new PumpReplacementDataObject()
			{
				 Id = 5,
				 Diameter = "1\"",
				 Engine= "1x230V",
				 Length=  "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =   "Biral M 10-4",
			},
			new PumpReplacementDataObject()
			{
				 Id = 6,
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral M 12-4",
			},
			new PumpReplacementDataObject()
			{
				 Id = 7,
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type = "Biral M 13-4",
			},
			new PumpReplacementDataObject()
			{
				 Id = 8,
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral MX 10-4",
			},
			new PumpReplacementDataObject()
			{
				 Id = 9,
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =   "Biral MX 12-4",
			},
			new PumpReplacementDataObject()
			{
				 Id = 10,
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =   "Biral MX 13-4",
			},

		};

		private readonly List<PumpReplacementEntity> _mockEntityData = new List<PumpReplacementEntity>()
		{
			new PumpReplacementEntity()
			{
				 Diameter = "1\"",
				 Engine= "1x230V",
				 Length=  "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral AX 12-4",
			},
			new PumpReplacementEntity()
			{
				 Diameter = "1\"",
				 Engine= "1x230V",
				 Length=  "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral AX 13-4",
			},
			new PumpReplacementEntity()
			{
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral AX 15-6 130 RED",
			},
			new PumpReplacementEntity()
			{
				 Diameter = "1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral AX 15-6 130 RED",
			},
			new PumpReplacementEntity()
			{
				 Diameter = "1\"",
				 Engine= "1x230V",
				 Length=  "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =   "Biral M 10-4",
			},
			new PumpReplacementEntity()
			{
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral M 12-4",
			},
			new PumpReplacementEntity()
			{
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type = "Biral M 13-4",
			},
			new PumpReplacementEntity()
			{
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =  "Biral MX 10-4",
			},
			new PumpReplacementEntity()
			{
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =   "Biral MX 12-4",
			},
			new PumpReplacementEntity()
			{
				 Diameter ="1\"",
				 Engine= "1x230V",
				 Length= "130",
				 MarketKey = "ch",
				 Pressure = "10",
				 Type =   "Biral MX 13-4",
			},

		};

	}
}
