﻿namespace EmmeApp.DataObjects.Base
{
    public interface IDataObjectBase
	{
		long Id { get; set; }
	}
}
