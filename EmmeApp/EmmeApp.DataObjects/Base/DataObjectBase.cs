﻿using SQLite;

namespace EmmeApp.DataObjects.Base
{
    public abstract class DataObjectBase : IDataObjectBase
	{
		[PrimaryKey, AutoIncrement]
		public virtual long Id { get; set; }
	}
}