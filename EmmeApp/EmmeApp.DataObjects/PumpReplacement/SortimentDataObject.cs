﻿using EmmeApp.DataObjects.Base;

namespace EmmeApp.DataObjects
{
    public class SortimentDataObject : DataObjectBase
	{
		public int Version { get; set; }
		public string MarketContent { get; set; }
	}
}
