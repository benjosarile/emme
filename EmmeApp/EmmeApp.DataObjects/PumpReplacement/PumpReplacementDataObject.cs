﻿using EmmeApp.DataObjects.Base;
using SQLite;

namespace EmmeApp.DataObjects
{
    public class PumpReplacementDataObject : DataObjectBase
	{
		[Indexed]
		public string MarketKey { get; set; }
		public string Type { get; set; }

		public string Diameter { get; set; }
		public string Length { get; set; }
		public string Pressure { get; set; }
		public string Engine { get; set; }
	}
}
