﻿using EmmeApp.DataObjects.Base;
using SQLite;

namespace EmmeApp.DataObjects
{
    public class PumpReplacementProductDataObject : DataObjectBase
	{
		[Indexed]
		public string MarketKey { get; set; }
		public string Type { get; set; }

		public long ProductId { get; set; }
		public string ConnectorInformation { get; set; }
		public string CommentCodes { get; set; }
	}
}
