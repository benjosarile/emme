﻿using EmmeApp.DataObjects.Base;

namespace EmmeApp.DataObjects
{
    public class MasterPumpReplacementDataObject : DataObjectBase
	{
		public string Type { get; set; }

		public string Diameter { get; set; }
		public string DiameterUnit { get; set; }

		public string Length { get; set; }
		public string LengthUnit { get; set; }

		public string Pressure { get; set; }
		public string PressureUnit { get; set; }

		public string Engine { get; set; }
		public string ArticleNumber { get; set; }

		public string CommentCodes { get; set; }
	}
}
