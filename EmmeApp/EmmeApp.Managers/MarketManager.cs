﻿using EmmeApp.Common.Constants;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Managers.Abstractions;
using System;

namespace EmmeApp.Managers
{
    public class MarketManager : IMarketManager
	{
		private readonly ICultureInfoService _cultureInfoService;
		public MarketManager(ICultureInfoService cultureInfoService)
		{
			_cultureInfoService = cultureInfoService;
		}

		public Tuple<string, string> GetMarketLanguage()
		{
			var countryCode = _cultureInfoService.GetCurrentCountryCode();
			string market = string.Empty;

			switch (countryCode)
			{
				case "CH": market = AppConstants.MarketCH; break;
				case "LI": market = AppConstants.MarketCH; break;
				case "DE": market = AppConstants.MarketDE; break;
				case "AT": market = AppConstants.MarketAT; break;
				case "IT": market = AppConstants.MarketIT; break;
				case "NL": market = AppConstants.MarketNL; break;
				case "ES": market = AppConstants.MarketEU; break;
				default: market = AppConstants.MarketEU; break;
			}

			var languageCode = _cultureInfoService.GetCurrentLanguage();
			
			if(market == AppConstants.MarketCH && languageCode != AppConstants.LanguageCodeDE && languageCode != AppConstants.LanguageCodeIT && languageCode != AppConstants.LanguageCodeFR)
			{
				languageCode = AppConstants.LanguageCodeDE;
			}
			else if (market == AppConstants.MarketDE)
			{
				languageCode = AppConstants.LanguageCodeDE;
			}
			else if (market == AppConstants.MarketAT)
			{
				languageCode = AppConstants.LanguageCodeDE;
			}
			else if (market == AppConstants.MarketIT && languageCode != AppConstants.LanguageCodeDE && languageCode != AppConstants.LanguageCodeIT)
			{
				languageCode = AppConstants.LanguageCodeIT;
			}
			else if (market == AppConstants.MarketNL && languageCode != AppConstants.LanguageCodeDE && languageCode != AppConstants.LanguageCodeNL)
			{
				languageCode = AppConstants.LanguageCodeNL;
			}
			else if (market == AppConstants.MarketEU && languageCode != AppConstants.LanguageCodeES)
			{
				languageCode = AppConstants.LanguageCodeEN;
			}

			return new Tuple<string, string>(market, languageCode);
		}

		public Tuple<string, string> GetDocumentLanguage()
		{
			var countryCode = _cultureInfoService.GetCurrentCountryCode();
			string market = string.Empty;

			switch (countryCode)
			{
				case "CH": market = AppConstants.MarketCH; break;
				case "LI": market = AppConstants.MarketCH; break;
				case "DE": market = AppConstants.MarketDE; break;
				case "AT": market = AppConstants.MarketAT; break;
				case "IT": market = AppConstants.MarketIT; break;
				case "NL": market = AppConstants.MarketNL; break;
				case "ES": market = AppConstants.MarketES; break;
				default: market = AppConstants.MarketEU; break;
			}

			var languageCode = _cultureInfoService.GetCurrentLanguage();

			if (market == AppConstants.MarketCH && languageCode != AppConstants.LanguageCodeDE && languageCode != AppConstants.LanguageCodeIT && languageCode != AppConstants.LanguageCodeFR)
			{
				languageCode = AppConstants.LanguageCodeDE;
			}
			else if (market == AppConstants.MarketDE)
			{
				languageCode = AppConstants.LanguageCodeDE;
			}
			else if (market == AppConstants.MarketAT)
			{
				market = AppConstants.MarketDE;
				languageCode = AppConstants.LanguageCodeDE;
			}
			else if (market == AppConstants.MarketIT && languageCode != AppConstants.LanguageCodeIT)
			{
				languageCode = AppConstants.LanguageCodeIT;
			}
			else if (market == AppConstants.MarketNL && languageCode != AppConstants.LanguageCodeNL)
			{
				languageCode = AppConstants.LanguageCodeNL;
			}
			else if (market == AppConstants.MarketES && languageCode != AppConstants.LanguageCodeES)
			{
				languageCode = AppConstants.LanguageCodeES;
			}
			else if (market == AppConstants.MarketEU)
			{
				market = string.Empty;
				languageCode = AppConstants.LanguageCodeEN;
			}

			return new Tuple<string, string>(market, languageCode);
		}
	}
}
