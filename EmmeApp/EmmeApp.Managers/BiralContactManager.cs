﻿using EmmeApp.Common.Constants;
using EmmeApp.Entities;
using EmmeApp.LocalData.Models;
using EmmeApp.Managers.Abstractions;
using EmmeApp.ProtocolServices.Abstractions;
using System.Collections.Generic;
using System.Linq;

namespace EmmeApp.Managers
{
    public class BiralContactManager : IBiralContactManager
	{
		private readonly IAssemblyResourceManager _fileManager;
		private readonly IServiceEntityMapper _serviceEntityMapper;
		private readonly IJsonService _jsonService;
		private readonly IMarketManager _marketManager;
		public BiralContactManager(IServiceEntityMapper serviceEntityMapper,
			IAssemblyResourceManager fileManager,
			IJsonService jsonService, 
			IMarketManager marketManager)
		{
			_fileManager = fileManager;
			_serviceEntityMapper = serviceEntityMapper;
			_jsonService = jsonService;
			_marketManager = marketManager;
		}

		private List<BiralContactEntity> GetBiralContacts()
		{
			string fileContent = _fileManager.GetResourceAsString(AssemblyResourceNames.BiralContacts);
			var contactRoot = _jsonService.DeserializeObject<BiralContactRootLocalData>(fileContent);
			var contacts = _serviceEntityMapper.Map<List<BiralContactEntity>>(contactRoot.Contacts);
			return contacts;
		}

		public BiralContactEntity GetCurrentBiralContact()
		{
			var marketLanguage = _marketManager.GetMarketLanguage();
			var biralContactList = GetBiralContacts();

			var selectedBiralContact = biralContactList.FirstOrDefault(c => c.Market == marketLanguage.Item1 && c.LanguageCode == marketLanguage.Item2);

			if(selectedBiralContact == null)
			{
				string market = AppConstants.MarketEU;
				selectedBiralContact = biralContactList.FirstOrDefault(c => c.Market == market); 
			}

			return selectedBiralContact;
		}

		public BiralContactEntity GetDefaultBiralContact()
		{
			var biralContactList = GetBiralContacts();

			var selectedBiralContact = biralContactList.FirstOrDefault(c => c.Market == AppConstants.MarketCH);

			return selectedBiralContact;
		}
	}
}
