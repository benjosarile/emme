﻿using EmmeApp.Common.Attributes;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Entities.External;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Humanizer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmmeApp.Managers
{
    public class LogPumpManager : ManagerBase, ILogPumpManager
	{
		private readonly IUnixTimestampService _unixTimestampService;

		public LogPumpManager(IServiceEntityMapper mapper, IUnixTimestampService unixTimestampService) : base(mapper)
		{
			_unixTimestampService = unixTimestampService;
		}

		public StringBuilder GenerateEmailContent(PumpLogRootModel rootModel, string newLine = "\r\n")
		{
			string dateFormat = "HH:mm:ss dd.MM.yy";

			var sb = new StringBuilder();
			sb.Append($"{AppResources.PumpLog_Alarm_Title}{newLine}");
			sb.Append(newLine);

			rootModel.AlarmPumpLogList = rootModel.AlarmPumpLogList ?? new List<PumpAlarmAndWarningLogModel>();
			rootModel.WarningPumpLogList = rootModel.WarningPumpLogList ?? new List<PumpAlarmAndWarningLogModel>();

			var rootModelAlarms = rootModel.AlarmPumpLogList.Skip(Math.Max(0, rootModel.AlarmPumpLogList.Count - 5)).OrderByDescending(x => x.AppearanceDateTime).ToList();

			foreach (var rootModelAlarm in rootModelAlarms)
			{
				string appearanceDateTime = rootModelAlarm.AppearanceDateTime != 0 ?
				  _unixTimestampService.ConvertToLocalTime(Convert.ToInt64(rootModelAlarm.AppearanceDateTime)).ToString(dateFormat) : string.Empty;
				string disappearanceDateTime = rootModelAlarm.DisappearanceDateTime != 0 ?
					_unixTimestampService.ConvertToLocalTime(Convert.ToInt64(rootModelAlarm.DisappearanceDateTime)).ToString(dateFormat) : string.Empty;
				string separator = rootModelAlarm.DisappearanceDateTime != 0 ? AppResources.PumpLog_NoDate : string.Empty;
				string appearanceAndDisappearanceDateTime = string.Empty;
				if (!string.IsNullOrEmpty(appearanceDateTime) && !string.IsNullOrEmpty(disappearanceDateTime))
				{
					appearanceAndDisappearanceDateTime = $"{appearanceDateTime} {separator} {disappearanceDateTime}";
				}
				else if (!string.IsNullOrEmpty(appearanceDateTime) && string.IsNullOrEmpty(separator) && string.IsNullOrEmpty(disappearanceDateTime))
				{
					appearanceAndDisappearanceDateTime = appearanceDateTime;
				}
				else if (string.IsNullOrEmpty(appearanceDateTime) && !string.IsNullOrEmpty(disappearanceDateTime))
				{
					appearanceAndDisappearanceDateTime = disappearanceDateTime;
				}
				else
				{
					appearanceAndDisappearanceDateTime = AppResources.PumpLog_NA;
				}

				sb.Append($"{appearanceAndDisappearanceDateTime}: {rootModelAlarm.AlarmLogStatus.Humanize()}{newLine}");
				sb.Append(newLine);
			}
			sb.Append(newLine);
			sb.Append($"{AppResources.PumpLog_Warning_Title}{newLine}");
			sb.Append(newLine);

			var rootModelWarnings = rootModel.WarningPumpLogList.Skip(Math.Max(0, rootModel.WarningPumpLogList.Count - 5)).OrderByDescending(x => x.AppearanceDateTime).ToList();

			foreach (var rootModelWarning in rootModelWarnings)
			{
				string appearanceDateTime = rootModelWarning.AppearanceDateTime != 0 ?
					_unixTimestampService.ConvertToLocalTime(Convert.ToInt64(rootModelWarning.AppearanceDateTime)).ToString(dateFormat) : string.Empty;
				string disappearanceDateTime = rootModelWarning.DisappearanceDateTime != 0 ?
					_unixTimestampService.ConvertToLocalTime(Convert.ToInt64(rootModelWarning.DisappearanceDateTime)).ToString(dateFormat) : string.Empty;

				string separator = rootModelWarning.DisappearanceDateTime != 0 ? AppResources.PumpLog_NoDate : string.Empty;
				string appearanceAndDisappearanceDateTime = string.Empty;
				if (!string.IsNullOrEmpty(appearanceDateTime) && !string.IsNullOrEmpty(disappearanceDateTime))
				{
					appearanceAndDisappearanceDateTime = $"{appearanceDateTime} {separator} {disappearanceDateTime}";
				}
				else if (!string.IsNullOrEmpty(appearanceDateTime) && string.IsNullOrEmpty(separator) && string.IsNullOrEmpty(disappearanceDateTime))
				{
					appearanceAndDisappearanceDateTime = appearanceDateTime;
				}
				else if (string.IsNullOrEmpty(appearanceDateTime) && !string.IsNullOrEmpty(disappearanceDateTime))
				{
					appearanceAndDisappearanceDateTime = disappearanceDateTime;
				}
				else
				{
					appearanceAndDisappearanceDateTime = AppResources.PumpLog_NA;
				}

				sb.Append($"{appearanceAndDisappearanceDateTime}: {rootModelWarning.WarningLogStatus.Humanize()}{newLine}");
				sb.Append(newLine);
			}

			return sb;
		}

		public PumpLogRootModel MapParametersToWizardModel(PumpLogRootModel rootModel, List<LogEntryTExternalEntity> logs)
		{
			if (rootModel.AlarmPumpLogList == null)
			{
				rootModel.AlarmPumpLogList = new List<PumpAlarmAndWarningLogModel>();
			}

			if (rootModel.WarningPumpLogList == null)
			{
				rootModel.WarningPumpLogList = new List<PumpAlarmAndWarningLogModel>();
			}

			if (logs != null && logs.Count > 0)
			{
				foreach (var log in logs)
				{
					var logStatus = GetEnumType(log.Type, log.LogId.ToString());

					if (logStatus != null)
					{
						if (log.Type == ExternalLogTypeValue.Alarm)
						{
							var alarmLogModel = new PumpAlarmAndWarningLogModel()
							{
								AppearanceDateTime = log.TimeAppear,
								DisappearanceDateTime = log.TimeDisappear,
								AlarmLogStatus = (PumpCurrentAlarmValue)logStatus
							};

							var type = alarmLogModel.AlarmLogStatus.GetType();
							var memInfo = type.GetMember(alarmLogModel.AlarmLogStatus.ToString());

							if (memInfo.Any())
							{
								var attributes = memInfo[0].GetCustomAttributes(typeof(LogHelpTextAttribute), false);

								if (attributes.Any())
								{
									var attribute = (LogHelpTextAttribute)attributes.FirstOrDefault();
									alarmLogModel.HasHelpText = true;
									alarmLogModel.Title = attribute.GetTranslation(attribute.TitleResource);
									alarmLogModel.Measure = attribute.GetTranslation(attribute.MeasureResource);
									alarmLogModel.Cause = attribute.GetTranslation(attribute.CauseResource);
								}
							}

							rootModel.AlarmPumpLogList.Add(alarmLogModel);
						}
						else if (log.Type == ExternalLogTypeValue.Warning)
						{
							var warningLogModel = new PumpAlarmAndWarningLogModel()
							{
								AppearanceDateTime = log.TimeAppear,
								DisappearanceDateTime = log.TimeDisappear,
								WarningLogStatus = (PumpCurrentWarningValue)logStatus
							};

							var type = warningLogModel.WarningLogStatus.GetType();
							var memInfo = type.GetMember(warningLogModel.WarningLogStatus.ToString());

							if (memInfo.Any())
							{
								var attributes = memInfo[0].GetCustomAttributes(typeof(LogHelpTextAttribute), false);

								if (attributes.Any())
								{
									var attribute = (LogHelpTextAttribute)attributes.FirstOrDefault();
									warningLogModel.HasHelpText = true;
									warningLogModel.Title = attribute.GetTranslation(attribute.TitleResource);
									warningLogModel.Measure = attribute.GetTranslation(attribute.MeasureResource);
									warningLogModel.Cause = attribute.GetTranslation(attribute.CauseResource);
								}
							}

							rootModel.WarningPumpLogList.Add(warningLogModel);
						}
					}
				}
			}

			return rootModel;
		}

		private Enum GetEnumType(ExternalLogTypeValue logType, string val)
		{
			switch (logType)
			{
				case ExternalLogTypeValue.Alarm: return (PumpCurrentAlarmValue)Enum.Parse(typeof(PumpCurrentAlarmValue), val);
				case ExternalLogTypeValue.Warning: return (PumpCurrentWarningValue)Enum.Parse(typeof(PumpCurrentWarningValue), val);
				default: break;
			}

			return null;
		}
	}
}
