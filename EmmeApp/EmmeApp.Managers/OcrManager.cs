﻿using EmmeApp.Common.Constants;
using EmmeApp.Common.Exceptions;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Entities;
using EmmeApp.Managers.Abstractions;
using EmmeApp.WebServices.Abstractions;
using Plugin.Connectivity.Abstractions;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace EmmeApp.Managers
{
    public class OcrManager : ManagerBase, IOcrManager
    {
        private readonly IOcrWebService _ocrWebService;
        private readonly ISettingsService _settingsService;

        public OcrManager(IConnectivity connectivity, IServiceEntityMapper mapper, IOcrWebService ocrWebService, ISettingsService settingsService) : base(connectivity, mapper)
        {
            _ocrWebService = ocrWebService;
            _settingsService = settingsService;
        }

        public bool CanCaptureImage => Connectivity.IsConnected;

        public async Task<OcrObjectRootEntity> ReadTextOnImage(byte[] byteData, CancellationToken cancellationToken)
        {
            if (!Connectivity.IsConnected)
            {
                throw new NoInternetConnectivityException();
            }

            try
            {
                var ocrObjectRootResponseContract = await _ocrWebService.ReadTextOnImage(byteData, cancellationToken);

                if (ocrObjectRootResponseContract != null)
                {
                    return Mapper.Map<OcrObjectRootEntity>(ocrObjectRootResponseContract);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            return null;
        }

        public bool DidOcrWasUsed()
        {
            return !string.IsNullOrEmpty(_settingsService.GetValue(SettingsConstant.DidOcrWasUsed));
        }

        public void ConfirmOcrUsed()
        {
            _settingsService.SetValue(SettingsConstant.DidOcrWasUsed, DateTime.Now.ToString());
        }
    }
}
