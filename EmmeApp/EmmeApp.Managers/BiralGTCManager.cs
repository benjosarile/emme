﻿using EmmeApp.Common.Constants;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Entities;
using EmmeApp.Managers.Abstractions;
using EmmeApp.ProtocolServices.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EmmeApp.Managers
{
    public class BiralGtcManager : IBiralGtcManager
	{
		private readonly IAssemblyResourceManager _fileManager;
		private readonly IServiceEntityMapper _serviceEntityMapper;
		private readonly IJsonService _jsonService;
		private readonly IMarketManager _marketManager;
		private readonly ISettingsService _settingsService;

		public BiralGtcManager(IServiceEntityMapper serviceEntityMapper,
			IAssemblyResourceManager fileManager,
			IJsonService jsonService,
			IMarketManager marketManager,
			ISettingsService settingsService)
		{
			_fileManager = fileManager;
			_serviceEntityMapper = serviceEntityMapper;
			_jsonService = jsonService;
			_marketManager = marketManager;
			_settingsService = settingsService;
		}

		public bool HasGtcConfirmed()
		{
			return !string.IsNullOrEmpty(_settingsService.GetValue(SettingsConstant.HasGtcConfirmed));
		}

		public void ConfirmGtc()
		{
			_settingsService.SetValue(SettingsConstant.HasGtcConfirmed, DateTime.Now.ToString());
		}

		private List<BiralGtcEntity> GetBiralGtcs()
		{
			string fileContent = _fileManager.GetResourceAsString(AssemblyResourceNames.BiralGtcs);
			var contactRoot = _jsonService.DeserializeObject<List<BiralGtcEntity>>(fileContent);
			var contacts = _serviceEntityMapper.Map<List<BiralGtcEntity>>(contactRoot);
			return contacts;
		}

		public BiralGtcEntity GetCurrentBiralGtc()
		{
			var marketLanguage = _marketManager.GetMarketLanguage();
			var biralGtcList = GetBiralGtcs();

			var selectedBiralGtc = biralGtcList.FirstOrDefault(c => c.CountryCode == marketLanguage.Item1 && c.Language == marketLanguage.Item2);

			if (selectedBiralGtc == null)
			{
				string market = AppConstants.MarketEU;
				selectedBiralGtc = biralGtcList.FirstOrDefault(c => c.CountryCode == market);
			}

			return selectedBiralGtc;
		}

	}
}
