﻿using EmmeApp.Entities.External;
using EmmeApp.Managers.Abstractions;
using Plugin.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace EmmeApp.Managers
{
    public class ParameterDataCacheManager : IParameterDataCacheManager
    {
        private readonly IBleManager _bleManager = null;
        private IDisposable _notifyDisposable = null;
        private IDisposable _deviceStatusDisposable;
        private bool _shouldAcquireNewData = false;

        public ParameterDataCacheManager(IBleManager bleManager)
        {
            _bleManager = bleManager;
        }

        public InitCommunicationTExternalEntity InitCommunication
        {
            get
            {
                if (!HasValidInitialData)
                    return null;

                return _initialData.Item1;
            }
        }

        private Tuple<InitCommunicationTExternalEntity, List<ParameterTExternalEntity>, List<LogEntryTExternalEntity>, IDevice> _initialData = null;

        public bool HasValidInitialData =>
           _initialData != null
           && _initialData.Item1 != null
           && _initialData.Item2 != null && _initialData.Item2.Count > 0
           && _initialData.Item3 != null
           && _initialData.Item4 != null;

        public async Task<Tuple<InitCommunicationTExternalEntity, List<ParameterTExternalEntity>, List<LogEntryTExternalEntity>, IDevice>> GetAndCacheParameters(bool restartDevice = false, Action<Exception> onError = null)
        {
            var connectedDevice = await _bleManager.GetConnectedDevice();
            if (connectedDevice == null)
            {
                Reset();
                _initialData = null;
                return _initialData;
            }

            if (!restartDevice && _initialData != null)
                return _initialData;

            if (restartDevice || connectedDevice.Status != ConnectionStatus.Connected)
                connectedDevice = await _bleManager.RestartTheDevice(connectedDevice);

            if (connectedDevice != null)
            {
                if (connectedDevice.Status == ConnectionStatus.Connected)
                {
                    var characteristic = await _bleManager.GetDeviceReadCharacteristic(connectedDevice);

                    if (characteristic != null)
                    {
                        var bleParameterData = new List<byte[]>();

                        _notifyDisposable?.Dispose();
                        _notifyDisposable = characteristic
                            .WhenNotificationReceived()
                            .Subscribe(
                                result => bleParameterData.Add(ReadParameters(result)),
                                ex => onError?.Invoke(ex)
                            );

                        _shouldAcquireNewData = true;

                        await characteristic.EnableNotifications();

                        await WaitForDataFill(bleParameterData); //TODO This is taking too long

                        _shouldAcquireNewData = false;

                        connectedDevice = await _bleManager.GetConnectedDevice();

                        if (connectedDevice != null && connectedDevice.Status == ConnectionStatus.Connected)
                        {
                            _shouldAcquireNewData = false;

                            var copyOfBleParameterData = new List<byte[]>(bleParameterData);
                            var initData = _bleManager.GetInitialData(copyOfBleParameterData);

                            _initialData = new Tuple<InitCommunicationTExternalEntity, List<ParameterTExternalEntity>, List<LogEntryTExternalEntity>, IDevice>(initData.Item1, initData.Item2, initData.Item3, connectedDevice);

                            return _initialData;
                        }
                    }

                    _initialData = new Tuple<InitCommunicationTExternalEntity, List<ParameterTExternalEntity>, List<LogEntryTExternalEntity>, IDevice>(null, null, null, connectedDevice);

                    return _initialData;
                }

                _initialData = new Tuple<InitCommunicationTExternalEntity, List<ParameterTExternalEntity>, List<LogEntryTExternalEntity>, IDevice>(null, null, null, connectedDevice);

                return _initialData;
            }

            _initialData = null;
            return _initialData;
        }

        private async Task WaitForDataFill(List<byte[]> bleParameterData)
        {
            await Task.Run(
               () =>
               {
                   double preferredMaxLoadingTimeInSeconds = 8; //TODO Original is 15
                   int paramaterCountThreshold = 300;

                   System.Threading.SpinWait.SpinUntil(() => bleParameterData.Count >= paramaterCountThreshold, TimeSpan.FromSeconds(preferredMaxLoadingTimeInSeconds));
               });
        }

        private byte[] ReadParameters(CharacteristicGattResult result)
        {
            if (result.Data != null && _shouldAcquireNewData)
            {
                var resultData = result.Data;

                var hexDataValue = BitConverter.ToString(resultData);
                Debug.WriteLine(hexDataValue);

                return resultData;
            }

            return new List<byte>().ToArray();
        }

        public void Reset()
        {
            _initialData = null;

            _notifyDisposable?.Dispose();
            _notifyDisposable = null;

            _deviceStatusDisposable?.Dispose();
            _deviceStatusDisposable = null;

            _shouldAcquireNewData = false;
        }

    }
}
