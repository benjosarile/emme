﻿using AutoMapper;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Managers.Mappers.Profiles;
using System;
using System.Diagnostics;

namespace EmmeApp.Managers.Mappers
{
	public class ServiceEntityMapper : IServiceEntityMapper
	{
		private readonly Mapper _mapper;
		public ServiceEntityMapper()
		{
			try
			{
				var config = new MapperConfiguration(cfg =>
				{
					cfg.AddProfile<DtoLocalDataProfile>();
					cfg.AddProfile<EntityContractProfile>();
					cfg.AddProfile<EntityDtoProfile>();
					cfg.AddProfile<EntityLocalDataProfile>();
					cfg.AddProfile<ContractLocalDataProfile>();
					cfg.AddProfile<ModelProfile>();
				});

				_mapper = new Mapper(config);
			}
			catch (InvalidOperationException ex)
			{
				Debug.WriteLine(ex.Message);
				Debug.WriteLine("==============================================================================================");
			}
			catch (AutoMapperConfigurationException ex)
			{
				Debug.WriteLine(ex.Message);
				Debug.WriteLine("==============================================================================================");
			}
			catch (AutoMapperMappingException ex)
			{
				Debug.WriteLine(ex.Message);
				Debug.WriteLine("==============================================================================================");
			}
		}

		public TDestination Map<TSource, TDestination>(TSource value) => _mapper.Map<TSource, TDestination>(value);
		public TDestination Map<TDestination>(object value) => _mapper.Map<TDestination>(value);
	}
}
