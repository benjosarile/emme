﻿using AutoMapper;
using EmmeApp.Common.Enum;
using EmmeApp.DataContracts.Enums;
using EmmeApp.DataContracts.External;
using EmmeApp.DataContracts.Response;
using EmmeApp.Entities;
using EmmeApp.Entities.External;

namespace EmmeApp.Managers.Mappers.Profiles
{
	public class EntityContractProfile : Profile
	{
		public EntityContractProfile()
		{
			CreateMap<ExternalAccessLevelValue, ExternalAccessLevelEnumContract>();
			CreateMap<ExternalAccessLevelEnumContract, ExternalAccessLevelValue>();

			CreateMap<ExternalComProfileValue, ExternalComProfileEnumContract>();
			CreateMap<ExternalComProfileEnumContract, ExternalComProfileValue>();

			CreateMap<ExternalControlUnitParameterIdValue, ExternalControlUnitParameterIdEnumContract>();
			CreateMap<ExternalControlUnitParameterIdEnumContract, ExternalControlUnitParameterIdValue>();

			CreateMap<ExternalPumpModulAParameterIdValue, ExternalPumpModulAParameterIdEnumContract>();
			CreateMap<ExternalPumpModulAParameterIdEnumContract, ExternalPumpModulAParameterIdValue>();

			CreateMap<ExternalPumpVariAParameterIdValue, ExternalPumpVariAParameterIdEnumContract>();
			CreateMap<ExternalPumpVariAParameterIdEnumContract, ExternalPumpVariAParameterIdValue>();

			CreateMap<ExternalIdOneofCaseValue, ExternalIdOneofCaseEnumContract>();
			CreateMap<ExternalIdOneofCaseEnumContract, ExternalIdOneofCaseValue>();

			CreateMap<ExternalUnitTValue, ExternalUnitTEnumContract>();
			CreateMap<ExternalUnitTEnumContract, ExternalUnitTValue>();

			CreateMap<ExternalValueOneofCaseValue, ExternalValueOneofCaseEnumContract>();
			CreateMap<ExternalValueOneofCaseEnumContract, ExternalValueOneofCaseValue>();

			CreateMap<ExternalVariAParameterIdValue, ExternalVariAParameterIdEnumContract>();
			CreateMap<ExternalVariAParameterIdEnumContract, ExternalVariAParameterIdValue>();

			CreateMap<ExternalLogTypeValue, ExternalLogTypeEnumContract>();
			CreateMap<ExternalLogTypeEnumContract, ExternalLogTypeValue>();

			CreateMap<ExternalLogSourceValue, ExternalLogSourceEnumContract>();
			CreateMap<ExternalLogSourceEnumContract, ExternalLogSourceValue>();

			CreateMap<BaseExternalEntity, BaseExternalContract>();
			CreateMap<BaseExternalContract, BaseExternalEntity>();

			CreateMap<InitCommunicationTExternalEntity, InitCommunicationTExternalContract>();
			CreateMap<InitCommunicationTExternalContract, InitCommunicationTExternalEntity>();

			CreateMap<ScalingTExternalEntity, ScalingTExternalContract>();
			CreateMap<ScalingTExternalContract, ScalingTExternalEntity>();

			CreateMap<ValueTExternalEntity, ValueTExternalContract>();
			CreateMap<ValueTExternalContract, ValueTExternalEntity>();

			CreateMap<ParameterTExternalEntity, ParameterTExternalContract>();
			CreateMap<ParameterTExternalContract, ParameterTExternalEntity>();

			CreateMap<ParametersTExternalEntity, ParametersTExternalContract>();
			CreateMap<ParametersTExternalContract, ParametersTExternalEntity>();

			CreateMap<LogEntryTExternalEntity, LogEntryTExternalContract>();
			CreateMap<LogEntryTExternalContract, LogEntryTExternalEntity>();

			CreateMap<LogEntriesTExternalEntity, LogEntriesTExternalContract>();
			CreateMap<LogEntriesTExternalContract, LogEntriesTExternalEntity>();

			CreateMap<SearchPagesByCatalogRootContract, SearchPagesByCatalogRootEntity>();
			CreateMap<SearchContentByTypeRootContract, SearchContentByTypeRootEntity>();
			CreateMap<SearchCurrentFilterContract, SearchCurrentFilterEntity>();
			CreateMap<SearchFilterBoxContract, SearchFilterBoxEntity>();
			CreateMap<SearchFilterContract, SearchFilterEntity>();
			CreateMap<SearchCatalogContract, SearchCatalogEntity>();
			CreateMap<SearchGroupContract, SearchGroupEntity>();
			CreateMap<SearchPageContract, SearchPageEntity>();
			CreateMap<CatalogDetailsContract, CatalogDetailsEntity>();

			CreateMap<OcrWordContract, OcrWordEntity>();
			CreateMap<OcrLineContract, OcrLineEntity>();
			CreateMap<OcrRegionContract, OcrRegionEntity>();
			CreateMap<OcrObjectRootResponseContract, OcrObjectRootEntity>();
		}
	}
}