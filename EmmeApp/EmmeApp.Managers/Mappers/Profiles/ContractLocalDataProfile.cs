﻿using AutoMapper;
using EmmeApp.DataContracts.Response;
using EmmeApp.LocalData.Models;

namespace EmmeApp.Managers.Mappers.Profiles
{
    public class ContractLocalDataProfile : Profile
	{
		public ContractLocalDataProfile()
		{
			CreateMap<PumpReplacementLocalData, PumpReplacementContract>();
			CreateMap<PumpReplacementContract, PumpReplacementLocalData>();

			CreateMap<PumpReplacementProductLocalData, PumpReplacementProductContract>();
			CreateMap<PumpReplacementProductContract, PumpReplacementProductLocalData>();

			CreateMap<BiralProductLocalData, BiralProductContract>();
			CreateMap<BiralProductContract, BiralProductLocalData>();

			CreateMap<ProductCategoryLocalData, ProductCategoryContract>();
			CreateMap<ProductCategoryContract, ProductCategoryLocalData>();

			CreateMap<ProductCommentLocalData, ProductCommentContract>();
			CreateMap<ProductCommentContract, ProductCommentLocalData>();

			CreateMap<ProductConnectorLocalData, ProductConnectorContract>();
			CreateMap<ProductConnectorContract, ProductConnectorLocalData>();

			CreateMap<PumpReplacementRootLocalData, PumpReplacementRootContract>();
			CreateMap<PumpReplacementRootContract, PumpReplacementRootLocalData>();

			CreateMap<PumpReplacementConnectorInformationLocalData, PumpReplacementConnectorInformationContract>();
			CreateMap<PumpReplacementConnectorInformationContract, PumpReplacementConnectorInformationLocalData>();

			CreateMap<LocalizationLocalData, LocalizationContract>();
			CreateMap<LocalizationContract, LocalizationLocalData>();

			CreateMap<LocalizationDetailLocalData, LocalizationDetailContract>();
			CreateMap<LocalizationDetailContract, LocalizationDetailLocalData>();

		}
	}
}
