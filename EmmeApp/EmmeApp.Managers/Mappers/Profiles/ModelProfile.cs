﻿using AutoMapper;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.Configuration;
using EmmeApp.Common.PageModels.External;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Entities.External;

namespace EmmeApp.Managers.Mappers.Profiles
{
    public class ModelProfile : Profile
    {
        public ModelProfile()
        {
			CreateMap<ValueTExternalEntity, ValueTExternalModel>();
			CreateMap<ScalingTExternalEntity, ScalingTExternalModel>();

			CreateMap<CockpitControlUnitRootModel, InitialStartupWizardModel>()
            .ForMember(d => d.CustomerInformationName, o => o.MapFrom(m => m.CustomerName))
            .ForMember(d => d.CustomerInformationPhone, o => o.MapFrom(m => m.CustomerPhoneNumber))
            .ForMember(d => d.CustomerInformationStreet, o => o.MapFrom(m => m.CustomerStreet))
            .ForMember(d => d.CustomerInformationCity, o => o.MapFrom(m => m.CustomerCity))
            .ForMember(d => d.CustomerInformationZipCode, o => o.MapFrom(m => m.CustomerZip))
            .ForMember(d => d.CustomerInformationCountry, o => o.MapFrom(m => m.CustomerCountry))
            .ForMember(d => d.SystemDataConfigurationSystemName, o => o.MapFrom(m => m.SystemName))
            .ForMember(d => d.SystemDataConfigurationMotor1Name, o => o.MapFrom(m => m.MotorType0))
            .ForMember(d => d.SystemDataConfigurationMotor2Name, o => o.MapFrom(m => m.MotorType1))
            .ForMember(d => d.SystemDataConfigurationSystemType, o => o.MapFrom(m => m.SystemType))
            .ForMember(d => d.LevelProbeConfigurationValue, o => o.MapFrom(m => m.LevelProbeConfiguration))
            .ForMember(d => d.LevelProbeConfigurationLevelAnalogThreshold0, o => o.MapFrom(m => m.LevelAnalogThreshold0Current))
            .ForMember(d => d.LevelProbeConfigurationLevelAnalogThreshold1, o => o.MapFrom(m => m.LevelAnalogThreshold1Current))
            .ForMember(d => d.LevelProbeConfigurationLevelAnalogThreshold2, o => o.MapFrom(m => m.LevelAnalogThreshold2Current))
            .ForMember(d => d.PumpAndMotorConfigurationNumberOfPumpValue, o => o.MapFrom(m => m.SystemDoubleMotor))
            .ForMember(d => d.PumpAndMotorConfigurationOSK, o => o.MapFrom(m => m.MotorOSKEnable))
            .ForMember(d => d.PumpAndMotorConfigurationSystemMotorModePump1Value, o => o.MapFrom(m => m.SystemMotorMode0))
            .ForMember(d => d.PumpAndMotorConfigurationSystemMotorModePump2Value, o => o.MapFrom(m => m.SystemMotorMode1))
            .ForMember(d => d.PumpAndMotorConfigurationMotorRateCurrentPump1, o => o.MapFrom(m => m.MotorRatedCurrentMotor0))
            .ForMember(d => d.PumpAndMotorConfigurationMotorRateCurrentPump2, o => o.MapFrom(m => m.MotorRatedCurrentMotor1))
            .ForMember(d => d.PumpAndMotorConfigurationThermalProtectionPump1, o => o.MapFrom(m => m.MotorWSKMotor0Enable))
            .ForMember(d => d.PumpAndMotorConfigurationThermalProtectionPump2, o => o.MapFrom(m => m.MotorWSKMotor1Enable))
            .ForMember(d => d.TimingConfigurationMotorDelayedOffEnable, o => o.MapFrom(m => m.MotorDelayedOffEnable))
            .ForMember(d => d.TimingConfigurationMotorDelayedOffTime, o => o.MapFrom(m => m.MotorDelayedOffTime))
            .ForMember(d => d.TimingConfigurationMotorForceRunningEnable, o => o.MapFrom(m => m.MotorForceRunningEnable))
            .ForMember(d => d.TimingConfigurationMotorForceRetentionTime, o => o.MapFrom(m => m.MotorForceRetentionTime))
            .ForMember(d => d.TimingConfigurationMotorForceRunningTime, o => o.MapFrom(m => m.MotorForceRunningOnTime))
            .ForMember(d => d.TimingConfigurationMotorDelayedOnEnable, o => o.MapFrom(m => m.MotorDelayedOnEnable))
            .ForMember(d => d.TimingConfigurationMotorDelayedOnTime, o => o.MapFrom(m => m.MotorDelayedOnTime))
            .ForMember(d => d.TimingConfigurationMotorRuntimeObserver, o => o.MapFrom(m => m.MotorRuntimeObserverEnable))
            .ForMember(d => d.TimingConfigurationMaxCycles, o => o.MapFrom(m => m.MotorRuntimeObserverMaxCycle))
            .ForMember(d => d.TimingConfigurationObserverTimeSpan, o => o.MapFrom(m => m.MotorRuntimeObserverTimespan))
            .ForMember(d => d.TimingConfigurationObserverRetentionTime, o => o.MapFrom(m => m.MotorRuntimeObserverRetention))
            .ForMember(d => d.TimingConfigurationMotorAtexEnable, o => o.MapFrom(m => m.MotorAtexEnable))
            .ForMember(d => d.TimingConfigurationAtexTimeout, o => o.MapFrom(m => m.MotorAtexTimeout))
            .ForMember(d => d.TimingConfigurationAtexLockTimeout, o => o.MapFrom(m => m.MotorAtexTimeoutLock))
            .ForMember(d => d.MaintenanceWarningIsWarningEnable, o => o.MapFrom(m => System.Convert.ToInt32(m.MaintenanceWarningEnable)))
            .ForMember(d => d.MaintenanceWarningIntervalBetweenWarnings, o => o.MapFrom(m => m.MaintenanceWarningInterval))
            .ForMember(d => d.RelayConfigurationRelay0Value, o => o.MapFrom(m => m.M2mCustomRelay0Configuration))
            .ForMember(d => d.RelayConfigurationRelay1Value, o => o.MapFrom(m => m.M2mCustomRelay1Configuration))
            .ForMember(d => d.RelayConfigurationRelay2Value, o => o.MapFrom(m => m.M2mCustomRelay2Configuration));

			CreateMap<CockpitPumpsModulARootModel, PumpConfigurationModulAWizardModel>()
				.ForMember(d => d.IsParameterChanged, o => o.Ignore())
				.ForMember(d => d.SystemName, o => o.Ignore())
				.ForMember(d => d.GroupName, o => o.Ignore())
				.ForMember(d => d.SystemAddress, o => o.Ignore())
				.ForMember(d => d.ContactName, o => o.Ignore())
				.ForMember(d => d.Phone, o => o.Ignore())
				.ForMember(d => d.EmailAddress, o => o.Ignore())
				.ForMember(d => d.Remarks, o => o.Ignore());

			CreateMap<CockpitPumpsVariAERootModel, PumpConfigurationVariAWizardModel>()
				.ForMember(d => d.IsParameterChanged, o => o.Ignore())
				.ForMember(d => d.SystemName, o => o.Ignore())
				.ForMember(d => d.GroupName, o => o.Ignore())
				.ForMember(d => d.SystemAddress, o => o.Ignore())
				.ForMember(d => d.ContactName, o => o.Ignore())
				.ForMember(d => d.Phone, o => o.Ignore())
				.ForMember(d => d.EmailAddress, o => o.Ignore())
				.ForMember(d => d.Remarks, o => o.Ignore());


            CreateMap<CockpitControlUnitRootModel, CockpitControlUnitOverviewModel>();
            CreateMap<CockpitControlUnitRootModel, CockpitControlUnitStatusModel>();
            CreateMap<CockpitControlUnitRootModel, CockpitControlUnitSettingsModel>();

            CreateMap<ControlUnitLogRootModel, ControlUnitLogEventModel>();

            CreateMap<CockpitPumpsModulARootModel, CockpitPumpsModulAStatusModel>();
            CreateMap<CockpitPumpsModulARootModel, CockpitPumpsModulAOverviewModel>();
            CreateMap<CockpitPumpsModulARootModel, CockpitPumpsModulASettingsModel>();

            CreateMap<CockpitPumpsVariAERootModel, CockpitPumpsVariAEStatusModel>();
            CreateMap<CockpitPumpsVariAERootModel, CockpitPumpsVariAEOverviewModel>();
            CreateMap<CockpitPumpsVariAERootModel, CockpitPumpsVariAESettingsModel>();

            CreateMap<PumpLogRootModel, PumpLogAlertModel>();
            CreateMap<PumpLogRootModel, PumpLogWarningModel>();
        }
    }
}
