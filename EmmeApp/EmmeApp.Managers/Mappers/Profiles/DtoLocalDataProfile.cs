﻿using AutoMapper;
using EmmeApp.DataObjects;
using EmmeApp.LocalData.Models;

namespace EmmeApp.Managers.Mappers.Profiles
{
    public class DtoLocalDataProfile : Profile
	{
		public DtoLocalDataProfile()
		{
			CreateMap<PumpReplacementLocalData, PumpReplacementDataObject>()
				.ForMember(d => d.MarketKey, o => o.Ignore())
				.ForMember(d => d.Id, o => o.Ignore());
			CreateMap<PumpReplacementDataObject, PumpReplacementLocalData>()
					.ForMember(d => d.PumpReplacementProducts, o => o.Ignore());

			CreateMap<PumpReplacementProductLocalData, PumpReplacementProductDataObject>()
					.ForMember(d => d.MarketKey, o => o.Ignore())
					.ForMember(d => d.Type, o => o.Ignore())
					.ForMember(d => d.Id, o => o.Ignore());
			CreateMap<PumpReplacementProductDataObject, PumpReplacementProductLocalData>();

			CreateMap<BiralProductLocalData, BiralProductDataObject>()
				.ForMember(d => d.MarketKey, o => o.Ignore())
				.ForMember(d => d.Id, o => o.Ignore());

			CreateMap<BiralProductDataObject, BiralProductLocalData>();


		}
	}
}
