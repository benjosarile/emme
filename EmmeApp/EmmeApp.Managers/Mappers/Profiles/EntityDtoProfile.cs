﻿using AutoMapper;
using EmmeApp.DataObjects;
using EmmeApp.Entities;

namespace EmmeApp.Managers.Mappers.Profiles
{
	public class EntityDtoProfile : Profile
	{
		public EntityDtoProfile()
		{
			CreateMap<BiralProductEntity, BiralProductDataObject>()
				.ForMember(d => d.Id, o => o.Ignore());
			CreateMap<BiralProductDataObject, BiralProductEntity>()
				.ForMember(d => d.CategoryValue, o => o.Ignore())
				.ForMember(d => d.DataSheetLink, o => o.Ignore())
				.ForMember(d => d.OperatingInstructionsLink, o => o.Ignore())
				.ForMember(d => d.OtherInformationLink, o => o.Ignore())
				.ForMember(d => d.Remarks, o => o.Ignore())
				.ForMember(d => d.AdapterInfos, o => o.Ignore());

			CreateMap<PumpReplacementProductEntity, PumpReplacementProductDataObject>()
				.ForMember(d => d.Id, o => o.Ignore());

			CreateMap<PumpReplacementProductDataObject, PumpReplacementProductEntity>();

			CreateMap<PumpReplacementEntity, PumpReplacementDataObject>()
				.ForMember(d => d.Id, o => o.Ignore());
			CreateMap<PumpReplacementDataObject, PumpReplacementEntity>();

			CreateMap<ProductCategoryEntity, ProductCategoryDataObject>()
				.ForMember(d => d.Id, o => o.Ignore());
			CreateMap<ProductCategoryDataObject, ProductCategoryEntity>();

			CreateMap<ProductCommentEntity, ProductCommentDataObject>()
				.ForMember(d => d.Id, o => o.Ignore());
			CreateMap<ProductCommentDataObject, ProductCommentEntity>();

			CreateMap<ProductConnectorEntity, ProductConnectorDataObject>()
				.ForMember(d => d.Id, o => o.Ignore());
			CreateMap<ProductConnectorDataObject, ProductConnectorEntity>();

			CreateMap<MasterPumpReplacementDataObject, MasterPumpReplacementEntity>();
		}
	}
}