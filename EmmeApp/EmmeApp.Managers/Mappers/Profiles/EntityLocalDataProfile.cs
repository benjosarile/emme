﻿using AutoMapper;
using EmmeApp.Entities;
using EmmeApp.LocalData.Models;

namespace EmmeApp.Managers.Mappers.Profiles
{
    public class EntityLocalDataProfile : Profile
	{
		public EntityLocalDataProfile()
		{
			CreateMap<SortimentEntity, SortimentLocalData>();
			CreateMap<SortimentLocalData, SortimentEntity>();

			CreateMap<SortimentMarketEntity, SortimentMarketLocalData>();
			CreateMap<SortimentMarketLocalData, SortimentMarketEntity>();

			CreateMap<MarketLinkEntity, MarketLinkLocalData>();
			CreateMap<MarketLinkLocalData, MarketLinkEntity>();

			CreateMap<BleServiceEntity, BleServiceLocalData>();
			CreateMap<BleServiceLocalData, BleServiceEntity>();

			CreateMap<BleServiceCharacteristicEntity, BleServiceCharacteristicLocalData>();
			CreateMap<BleServiceCharacteristicLocalData, BleServiceCharacteristicEntity>();

			CreateMap<BiralContactEntity, BiralContactLocalData>();
			CreateMap<BiralContactLocalData, BiralContactEntity>();

			CreateMap<BiralGtcEntity, BiralGtcLocalData>();
			CreateMap<BiralGtcLocalData, BiralGtcEntity>();
		}
	}
}