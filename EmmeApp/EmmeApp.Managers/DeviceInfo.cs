﻿using System;

namespace EmmeApp.Managers
{
    public abstract class DeviceInfo
	{
		public virtual string Name { get; }
		public virtual Guid UUID { get; }
	}
}
