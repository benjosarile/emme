﻿using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Log;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Entities.External;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Humanizer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmmeApp.Managers
{
    public class LogControlUnitManager : ManagerBase, ILogControlUnitManager
	{
		private readonly IUnixTimestampService _unixTimestampService;
		public LogControlUnitManager(IServiceEntityMapper mapper, IUnixTimestampService unixTimestampService) : base(mapper)
		{
			_unixTimestampService = unixTimestampService;
		}

		public StringBuilder GenerateEmailContent(ControlUnitLogRootModel rootModel, string newLine = "\r\n")
		{
			string dateFormat = "HH:mm:ss dd.MM.yy";

			var sb = new StringBuilder();
			sb.Append($"{AppResources.ControlUnitLog_Event_Title}{newLine}");
			sb.Append(newLine);

			var rootModelLogEvents = rootModel.EventControlUnitLogList.Skip(Math.Max(0, rootModel.EventControlUnitLogList.Count - 1500)).OrderByDescending(x => x.AppearanceDateTime).ToList();

			foreach (var rootModelLogEvent in rootModelLogEvents)
			{
				string appearanceDateTime = rootModelLogEvent.AppearanceDateTime != 0 ?
				  _unixTimestampService.ConvertToLocalTime(Convert.ToInt64(rootModelLogEvent.AppearanceDateTime)).ToString(dateFormat) : string.Empty;
				string appearanceAndDisappearanceDateTime = string.Empty;

				if (!string.IsNullOrEmpty(appearanceDateTime))
				{
					appearanceAndDisappearanceDateTime = appearanceDateTime;
				}
				else
				{
					appearanceAndDisappearanceDateTime = AppResources.PumpLog_NA;
				}

				sb.Append($"{appearanceAndDisappearanceDateTime}: {rootModelLogEvent.TypeValue.Humanize()}{newLine}");
				sb.Append(newLine);
			}

			return sb;
		}

		public ControlUnitLogRootModel MapParametersToWizardModel(ControlUnitLogRootModel rootModel, List<LogEntryTExternalEntity> logs)
		{
			if (rootModel.EventControlUnitLogList == null)
			{
				rootModel.EventControlUnitLogList = new List<ControlUnitLogModel>();
			}

			logs = logs ?? new List<LogEntryTExternalEntity>();

			foreach (var log in logs)
			{
				var enumValue = GetEnumType(log.Type, log.LogId.ToString());

				if (enumValue != null)
				{
					rootModel.EventControlUnitLogList.Add(new ControlUnitLogModel()
					{
						LogId = log.LogId,
						AppearanceDateTime = log.TimeAppear,
						TypeValue = enumValue
					});
				}
			}

			return rootModel;
		}

		private Enum GetEnumType(ExternalLogTypeValue logType, string val)
		{
			switch (logType)
			{
				case ExternalLogTypeValue.Alarm: return (ControlUnitSystemAlarmValue)Enum.Parse(typeof(ControlUnitSystemAlarmValue), val);
				case ExternalLogTypeValue.Warning: return (ControlUnitSystemWarningValue)Enum.Parse(typeof(ControlUnitSystemWarningValue), val);
				case ExternalLogTypeValue.Failure: return (ControlUnitSystemFailureValue)Enum.Parse(typeof(ControlUnitSystemFailureValue), val);
				default: break;
			}

			return null;
		}
	}
}
