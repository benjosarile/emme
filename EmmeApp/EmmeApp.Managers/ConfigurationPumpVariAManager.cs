﻿using EmmeApp.Common.Attributes;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Configuration;
using EmmeApp.Common.PageModels.External;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Entities.External;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace EmmeApp.Managers
{
    public class ConfigurationPumpVariAManager : ManagerBase, IConfigurationPumpVariAManager
    {
        private readonly IUnixTimestampService _unixTimestampService;

        public ConfigurationPumpVariAManager(IServiceEntityMapper mapper,
            IUnixTimestampService unixTimestampService) : base(mapper)
        {
            _unixTimestampService = unixTimestampService;
        }

        public StringBuilder GenerateEmailContent(PumpConfigurationVariAWizardModel wizardModel, string newLine = "\r\n")
        {
            var sb = new StringBuilder();
            
			sb.Append($"{AppResources.PumpConfiguration_SystemDescription_Label_SystemInformation}{newLine}");
            sb.Append($"{AppResources.PumpConfiguration_SystemDescription_Label_SystemName}: {wizardModel.SystemName}{newLine}");
            sb.Append($"{AppResources.PumpConfiguration_SystemDescription_Label_GroupName}: {wizardModel.GroupName}{newLine}");
            sb.Append($"{AppResources.PumpConfiguration_SystemDescription_Label_SystemAddress}: {wizardModel.SystemAddress}{newLine}");
            sb.Append(newLine);
            sb.Append($"{AppResources.PumpConfiguration_SystemDescription_Label_ContactInformation}{newLine}");
            sb.Append($"{AppResources.PumpConfiguration_SystemDescription_Label_Name}: {wizardModel.ContactName}{newLine}");
            sb.Append($"{AppResources.PumpConfiguration_SystemDescription_Label_Phone}: {wizardModel.Phone}{newLine}");
            sb.Append($"{AppResources.PumpConfiguration_SystemDescription_Label_EmailAddress}: {wizardModel.EmailAddress}{newLine}");
            sb.Append($"{AppResources.PumpConfiguration_SystemDescription_Label_Remarks}: {wizardModel.Remarks}{newLine}");

            return sb;
        }


        public Tuple<PumpConfigurationVariAWizardModel, List<ParameterPumpVariACharacteristicExternalEntity>> MapParametersToWizardModel(PumpConfigurationVariAWizardModel rootModel, List<ParameterTExternalEntity> parameters)
        {
            var objtype = rootModel.GetType();
			var parameterCharacteristicList = new List<ParameterPumpVariACharacteristicExternalEntity>();

			parameters = parameters ?? new List<ParameterTExternalEntity>();

			foreach (var parameter in parameters)
            {
				if (parameter.IdCase == ExternalIdOneofCaseValue.VariA && parameter.Value != null)
                {
                    if (parameter.Scaling != null && parameter.Scaling.Step >= 0)
                    {
                        var characteristic = parameterCharacteristicList.FirstOrDefault(x => x.VariA == parameter.VariA);

                        if (characteristic == null)
                        {
                            parameterCharacteristicList.Add(new ParameterPumpVariACharacteristicExternalEntity()
                            {
                                VariA = parameter.VariA,
                                Scaling = parameter.Scaling,
                                Unit = parameter.Unit
                            });
                        }
                    }

                    var property = objtype.GetProperties()
                        .FirstOrDefault(p =>
                                            p.CustomAttributes
                                            .Any(catt =>
                                            catt.AttributeType == typeof(PumpVariAParamAttribute) &&
                                            (ExternalPumpVariAParameterIdValue)catt.ConstructorArguments?[0].Value == parameter.VariA));

					if (property != null && parameter != null)
					{
						var catt = (PumpVariAParamAttribute)property.GetCustomAttributes(typeof(PumpVariAParamAttribute)).FirstOrDefault();
						if (parameter.VariA == catt.VariaAParameterId)
						{
							var parameterValue = Mapper.Map<ValueTExternalModel>(parameter.Value);
							var parameterScaling = Mapper.Map<ScalingTExternalModel>(parameter.Scaling);

							var value = catt.GetValue(parameter.IdCase, parameterValue, parameterScaling);
							if (value != null)
							{
								property.SetValue(rootModel, value);
							}
						}
					}
				}
            }

			return new Tuple<PumpConfigurationVariAWizardModel, List<ParameterPumpVariACharacteristicExternalEntity>>(rootModel, parameterCharacteristicList);
		}

        public List<List<ParameterTExternalEntity>> MapWizardModelToParameters(PumpConfigurationVariAWizardModel rootModel, List<ParameterPumpVariACharacteristicExternalEntity> characteristics, ExternalPumpVariAParameterIdValue parameterIdValue)
        {
            var parameters = new List<ParameterTExternalEntity>();

            var objtype = rootModel.GetType();

            var properties = objtype.GetProperties()
                        .Where(p => p.CustomAttributes.Any(catt =>
                        catt.AttributeType == typeof(PumpVariAParamAttribute)));

            foreach (var property in properties)
            {
                var catt = (PumpVariAParamAttribute)property.GetCustomAttributes(false).FirstOrDefault();
                if (catt.CanWrite && catt.VariaAParameterId == parameterIdValue)
                {
					var parameter = GetParameter(catt.VariaAParameterId, characteristics, catt.Type, property.GetValue(rootModel));

					if (parameter != null)
					{
						parameters.Add(parameter);
					}
				}
            }

            int groupBy = 2;
            var groupOfs = parameters.Select((str, index) => new { str, index }).GroupBy(x => x.index / groupBy)
                .Select(g => g.Select(x => x.str).ToList())
                .ToList();

            return groupOfs;
        }

        private ParameterTExternalEntity GetParameter(ExternalPumpVariAParameterIdValue variaA, List<ParameterPumpVariACharacteristicExternalEntity> characteristics, Type type, object value)
        {
            ValueTExternalEntity valueExternalEntity = null;

            var characteristic = characteristics.FirstOrDefault(x => x.VariA == variaA);

			if(value != null)
			{
				if (type == typeof(string))
				{
					valueExternalEntity = new ValueTExternalEntity()
					{
						Text = value.ToString(),
						ValueCase = ExternalValueOneofCaseValue.Text
					};
				}
				else if (characteristic != null)
				{
					long number = 0;
					if (type == typeof(bool) || type.IsEnum)
					{
						number = Convert.ToInt64(value);
					}
					else if (type == typeof(double))
					{
						if (characteristic.Scaling == null || characteristic.Scaling.Step == 0)
						{
							number = Convert.ToInt64(value);
						}
						else
						{
							double step = Math.Round(characteristic.Scaling.Step, 5);
							number = Convert.ToInt64(Convert.ToDouble(value) / step);
						}
					}
					else if (type == typeof(long))
					{
						number = Convert.ToInt64(value);
					}

					valueExternalEntity = new ValueTExternalEntity()
					{
						Number = number,
						ValueCase = ExternalValueOneofCaseValue.Number
					};
				}
			}

			return new ParameterTExternalEntity()
            {
                IdCase = ExternalIdOneofCaseValue.VariA,
                VariA = variaA,
                Value = valueExternalEntity
            };
        }

		public string GetUnit(ExternalPumpVariAParameterIdValue parameterId, List<ParameterTExternalEntity> parameters)
		{
			string unit = string.Empty;
			if (parameters != null)
			{
				var parameter = parameters.FirstOrDefault(x => x.VariA == parameterId);

				if (parameter != null)
				{
					switch (parameter.Unit)
					{
						case ExternalUnitTValue.Ampere: unit = AppResources.Label_UnitOfMeasurement_Ampere; break;
						case ExternalUnitTValue.M3: unit = AppResources.Label_UnitOfMeasurement_CubicMeter; break;
						case ExternalUnitTValue.Celsius: unit = AppResources.Label_UnitOfMeasurement_Celsius; break;
						case ExternalUnitTValue.CubicMeterPerHour: unit = AppResources.Label_UnitOfMeasurement_CubicMeterPerHour; break;
						case ExternalUnitTValue.Days: unit = AppResources.Label_UnitOfMeasurement_Days; break;
						case ExternalUnitTValue.Hours: unit = AppResources.Label_UnitOfMeasurement_Hours; break;
						case ExternalUnitTValue.Kwh: unit = AppResources.Label_UnitOfMeasurement_KiloWattHour; break;
						case ExternalUnitTValue.Meter: unit = AppResources.Label_UnitOfMeasurement_Meter; break;
						case ExternalUnitTValue.Miliampere: unit = AppResources.Label_UnitOfMeasurement_Milliampere; break;
						case ExternalUnitTValue.Percent: unit = AppResources.Label_UnitOfMeasurement_Percent; break;
						case ExternalUnitTValue.Pmin: unit = AppResources.Label_UnitOfMeasurement_PerMinute; break;
						case ExternalUnitTValue.Seconds: unit = AppResources.Label_UnitOfMeasurement_Seconds; break;
						case ExternalUnitTValue.Watt: unit = AppResources.Label_UnitOfMeasurement_Watt; break;
					}
				}
			}

			return unit;
		}
	}
}
