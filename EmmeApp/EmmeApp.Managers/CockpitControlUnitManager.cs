﻿using EmmeApp.Common.Attributes;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.External;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Entities.External;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

namespace EmmeApp.Managers
{
    public class CockpitControlUnitManager : ManagerBase, ICockpitControlUnitManager
	{
		private readonly IUnixTimestampService _unixTimestampService;

		public CockpitControlUnitManager(IServiceEntityMapper mapper, IUnixTimestampService unixTimestampService) : base(mapper)
		{
			_unixTimestampService = unixTimestampService;
		}

		public StringBuilder GenerateEmailContent(CockpitControlUnitRootModel rootModel, string newLine = "\r\n")
		{
			string dateFormat = "yyyyMMdd hh:mm tt";

			var sb = new StringBuilder();

			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ProductID}: {rootModel.CmnProductID}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ParameterProfileID}: {rootModel.CmnSystemParameterProfileID}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_EventProfileID}: {rootModel.CmnSystemEventProfileID}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_FirmwareVersion}: {rootModel.CmnSystemFirmwareVersion}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_FirmwareSignature}: {rootModel.CmnSystemFirmwareSignature}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_LogSize}: {rootModel.LogSize}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_LogCount}: {rootModel.LogCount}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_LogIndex}: {rootModel.LogIndex}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_RTOS_Version}: {rootModel.RTOSVersion}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_HAL_Version}: {rootModel.HALVersion}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_CMIS_Version}: {rootModel.CMSISVersion}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_BDLC_Version}: {rootModel.BDLCVersion}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ProductName}: {rootModel.FactoryProductName}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ItemNumber}: {rootModel.FactoryItemNumber}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_SerialNumber}: {rootModel.FactorySerialNumber}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_AssemblyTime}: {rootModel.FactoryAssemblyTime}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_OrderNumber}: {rootModel.FactoryOrderNumber}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_CustomerName}: {rootModel.CustomerName}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_PhoneNumber}: {rootModel.CustomerPhoneNumber}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Street}: {rootModel.CustomerStreet}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_City}: {rootModel.CustomerCity}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Zip}: {rootModel.CustomerZip}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Country}: {rootModel.CustomerCountry}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_SystemName}: {rootModel.SystemName}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor_0_Type}: {rootModel.MotorType0}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor_1_Type}: {rootModel.MotorType1}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_SystemType}: {(int)rootModel.SystemType}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_DoubleMotor}: {(int)rootModel.SystemDoubleMotor}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor_0_Mode}: {(int)rootModel.SystemMotorMode0}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor_1_Mode}: {(int)rootModel.SystemMotorMode1}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_MaintenanceWarningEnable}: {rootModel.MaintenanceWarningEnable}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_MaintenanceWarningTrigger}: {rootModel.MaintenanceWarningTrigger}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_MaintenanceWarningInterval}: {rootModel.MaintenanceWarningInterval}{newLine}");
			var time = _unixTimestampService.ConvertToLocalTime(rootModel.SystemUnixTimestamp);
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Time}: {time.ToString(dateFormat, CultureInfo.InvariantCulture)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_VirginFlag}: {rootModel.SystemVirginIsActive}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_SystemFailure}: {(int)rootModel.SystemFailure}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_SystemAlarm}: {(int)rootModel.SystemAlarm}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_SystemWarning}: {(int)rootModel.SystemWarning}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_SystemStatus}: {(int)rootModel.SystemStatus}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor0Mode}: {(int)rootModel.MotorModeMotor0}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor0RatedCurrent}: {rootModel.MotorRatedCurrentMotor0} A{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor0Phase1Current}: {rootModel.MotorPhase1CurrentMotor0} A{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor0Phase3Current}: {rootModel.MotorPhase3CurrentMotor0} A{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor0Status}: {(int)rootModel.MotorStatusMotor0}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor0WSKEnable}: {Convert.ToBoolean((int)rootModel.MotorWSKMotor0Enable)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor0ThermalProtectionEnable}: {Convert.ToBoolean((int)rootModel.MotorProtectionMotor0Enable)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor1Mode}: {(int)rootModel.MotorModeMotor1}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor1RatedCurrent}: {rootModel.MotorRatedCurrentMotor1} A{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor1Phase1Current}: {rootModel.MotorPhase1CurrentMotor1} A{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor1Phase3Current}: {rootModel.MotorPhase3CurrentMotor1} A{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor1Status}: {(int)rootModel.MotorStatusMotor1}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor1WSKEnable}: {Convert.ToBoolean((int)rootModel.MotorWSKMotor1Enable)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Motor1ThermalProtectionEnable}: {Convert.ToBoolean((int)rootModel.MotorProtectionMotor1Enable)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_OSKEnable}: {Convert.ToBoolean((int)rootModel.MotorOSKEnable)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_DelayedOffEnable}: {Convert.ToBoolean((int)rootModel.MotorDelayedOffEnable)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_DelayedOffTime}: {rootModel.MotorDelayedOffTime} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ForceRunningEnable}: {Convert.ToBoolean((int)rootModel.MotorForceRunningEnable)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_MotorForceRetentionTime}: {rootModel.MotorForceRetentionTime} d{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_MotorForceRunningOnTime}: {rootModel.MotorForceRunningOnTime} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_DelayedOnEnable}: {Convert.ToBoolean((int)rootModel.MotorDelayedOnEnable)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_MotorDelayedOnTime}: {rootModel.MotorDelayedOnTime} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_RuntimeObserverEnable}: {Convert.ToBoolean((int)rootModel.MotorRuntimeObserverEnable)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_MaxCycles}: {rootModel.MotorRuntimeObserverMaxCycle}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Timespan}: {rootModel.MotorRuntimeObserverTimespan} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_RuntimeObserverRetentionTime}: {rootModel.MotorRuntimeObserverRetention} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_AtexEnable}: {Convert.ToBoolean((int)rootModel.MotorAtexEnable)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_AtexTimeout}: {rootModel.MotorAtexTimeout} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_AtexTimeoutLock}: {rootModel.MotorAtexTimeoutLock} s{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_LevelProbeConfiguration}: {(int)rootModel.LevelProbeConfiguration}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ActiveLevel}: {(int)rootModel.LevelActiveLevel}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Threshold0Status}: {(int)rootModel.LevelThreshold0Status}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Threshold1Status}: {(int)rootModel.LevelThreshold1Status}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Threshold2Status}: {(int)rootModel.LevelThreshold2Status}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Threshold3Status}: {(int)rootModel.LevelThreshold3Status}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Levelswitch0Status}: {(int)rootModel.Levelswitch0Status}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Levelswitch1Status}: {(int)rootModel.Levelswitch1Status}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Levelswitch2Status}: {(int)rootModel.Levelswitch2Status}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Levelswitch3Status}: {(int)rootModel.Levelswitch3Status}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_AnalogStatus}: {(int)rootModel.LevelAnalogStatus}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_AnalogActiveCurrent}: {rootModel.LevelAnalogActiveCurrent} mA{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Threshold0Current}: {rootModel.LevelAnalogThreshold0Current} mA{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Threshold1Current}: {rootModel.LevelAnalogThreshold1Current} mA{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_Threshold2Current}: {rootModel.LevelAnalogThreshold2Current} mA{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_OperatingRelay}: {Convert.ToBoolean((int)rootModel.M2mOperatingRelay)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_CustomRelay0}: {Convert.ToBoolean((int)rootModel.M2mCustomRelay0)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_CustomRelay0Configuration}: {(int)rootModel.M2mCustomRelay0Configuration}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_CustomRelay1}: {Convert.ToBoolean((int)rootModel.M2mCustomRelay1)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_CustomRelay1Configuration}: {(int)rootModel.M2mCustomRelay1Configuration}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_CustomRelay2}: {Convert.ToBoolean((int)rootModel.M2mCustomRelay2)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_CustomRelay2Configuration}: {(int)rootModel.M2mCustomRelay2Configuration}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ExternalOff}: {Convert.ToBoolean((int)rootModel.M2mExternalOffActive)}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_RemoteStatus}: {(int)rootModel.DatalinkRemoteStatus}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ServiceStatus}: {(int)rootModel.DatalinkServiceStatus}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_PendingTimeout}: {rootModel.DatalinkRemotePendingTimeout} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ConnectionTimeout}: {rootModel.DatalinkConnectionTimeout} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ResponseTimeout}: {rootModel.DatalinkResponseTimeout} s{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ButtonMotor0Manual}: {Convert.ToBoolean((int)rootModel.HmiBtnMotor0ManualIn)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ButtonMotor0Auto}: {Convert.ToBoolean((int)rootModel.HmiBtnMotor0AutoOffIn)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ButtonMotor1Manual}: {Convert.ToBoolean((int)rootModel.HmiBtnMotor1ManualIn)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ButtonMotor1Auto}: {Convert.ToBoolean((int)rootModel.HmiBtnMotor1AutoOffIn)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ButtonMuteShort}: {Convert.ToBoolean((int)rootModel.HmiBtnMuteShortIn)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ButtonMuteLong}: {Convert.ToBoolean((int)rootModel.HmiBtnMuteLongIn)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ButtonConnection}: {Convert.ToBoolean((int)rootModel.HmiBtnConnectionIn)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_KeylockStatus}: {Convert.ToBoolean((int)rootModel.HmiBtnKeylockStatus)}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_ButtonFeedback}: {Convert.ToBoolean((int)rootModel.HmiButtonFeedback)}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_SystemOperatingTotal}: {rootModel.StatisticSystemOperatingTotal} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_MotorOperatingTotal}: {rootModel.StatisticMotorOperatingTotal} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_MotorSwitches}: {rootModel.StatisticMotorSwitchesTotal}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_OperatingTimeMotor0Total}: {rootModel.StatisticMotorOperatingMotor0Total} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_OperatingTimeMotor0Last24h}: {rootModel.StatisticMotorOperatingMotor024h} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_OperatingTimeMotor0Last7days}: {rootModel.StatisticMotorOperatingMotor07d} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_OperatingTimeMotor0LastMonth}: {rootModel.StatisticMotorOperatingMotor01m} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_NumberOfSwitchesMotor0Total}: {rootModel.StatisticMotorSwitchesMotor0Total}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_NumberOfSwitchesMotor0Last24h}: {rootModel.StatisticMotorSwitchesMotor024h}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_NumberOfSwitchesMotor0Last7days}: {rootModel.StatisticMotorSwitchesMotor07d}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_NumberOfSwitchesMotor0LastMonth}: {rootModel.StatisticMotorSwitchesMotor01m}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_OperatingTimeMotor1Total}: {rootModel.StatisticMotorOperatingMotor1Total} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_OperatingTimeMotor1Last24h}: {rootModel.StatisticMotorOperatingMotor124h} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_OperatingTimeMotor1Last7days}: {rootModel.StatisticMotorOperatingMotor17d} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_OperatingTimeMotor1LastMonth}: {rootModel.StatisticMotorOperatingMotor11m} s{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_NumberOfSwitchesMotor1Total}: {rootModel.StatisticMotorSwitchesMotor1Total}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_NumberOfSwitchesMotor1Last24h}: {rootModel.StatisticMotorSwitchesMotor124h}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_NumberOfSwitchesMotor1Last7days}: {rootModel.StatisticMotorSwitchesMotor17d}{newLine}");
			sb.Append($"{AppResources.CockpitControlUnitStatusPage_Label_Parameter_NumberOfSwitchesMotor1LastMonth}: {rootModel.StatisticMotorSwitchesMotor11m}{newLine}");

			return sb;
		}

		public string GetUnit(ExternalControlUnitParameterIdValue parameterId, List<ParameterTExternalEntity> parameters)
		{
			string unit = string.Empty;
			if (parameters != null)
			{
				var parameter = parameters.FirstOrDefault(x => x.ControlUnit == parameterId);

				if (parameter != null)
				{
					switch (parameter.Unit)
					{
						case ExternalUnitTValue.Ampere: unit = AppResources.Label_UnitOfMeasurement_Ampere; break;
						case ExternalUnitTValue.M3: unit = AppResources.Label_UnitOfMeasurement_CubicMeter; break;
						case ExternalUnitTValue.Celsius: unit = AppResources.Label_UnitOfMeasurement_Celsius; break;
						case ExternalUnitTValue.CubicMeterPerHour: unit = AppResources.Label_UnitOfMeasurement_CubicMeterPerHour; break;
						case ExternalUnitTValue.Days: unit = AppResources.Label_UnitOfMeasurement_Days; break;
						case ExternalUnitTValue.Hours: unit = AppResources.Label_UnitOfMeasurement_Hours; break;
						case ExternalUnitTValue.Kwh: unit = AppResources.Label_UnitOfMeasurement_KiloWattHour; break;
						case ExternalUnitTValue.Meter: unit = AppResources.Label_UnitOfMeasurement_Meter; break;
						case ExternalUnitTValue.Miliampere: unit = AppResources.Label_UnitOfMeasurement_Milliampere; break;
						case ExternalUnitTValue.Percent: unit = AppResources.Label_UnitOfMeasurement_Percent; break;
						case ExternalUnitTValue.Pmin: unit = AppResources.Label_UnitOfMeasurement_PerMinute; break;
						case ExternalUnitTValue.Seconds: unit = AppResources.Label_UnitOfMeasurement_Seconds; break;
						case ExternalUnitTValue.Watt: unit = AppResources.Label_UnitOfMeasurement_Watt; break;
					}
				}
			}

			return unit;
		}

		public Tuple<CockpitControlUnitRootModel, List<ParameterControlUnitCharacteristicExternalEntity>> MapParamtersToModel(CockpitControlUnitRootModel rootModel, List<ParameterTExternalEntity> parameters)
		{
			var parameterCharacteristicList = new List<ParameterControlUnitCharacteristicExternalEntity>();

			parameters = parameters ?? new List<ParameterTExternalEntity>();

			foreach (var parameter in parameters)
			{
				var objtype = rootModel.GetType();

				if (parameter.IdCase == ExternalIdOneofCaseValue.ControlUnit && parameter.Value != null)
				{
					if (parameter.Scaling != null && parameter.Scaling.Step >= 0)
					{
						var characteristic = parameterCharacteristicList.FirstOrDefault(x => x.ControlUnit == parameter.ControlUnit);

						if (characteristic == null)
						{
							parameterCharacteristicList.Add(new ParameterControlUnitCharacteristicExternalEntity()
							{
								ControlUnit = parameter.ControlUnit,
								Scaling = parameter.Scaling,
								Unit = parameter.Unit
							});
						}
					}

					var property = objtype.GetProperties()
						.FirstOrDefault(p =>
											p.CustomAttributes
											.Any(catt =>
											catt.AttributeType == typeof(ControlUnitParamAttribute) &&
											(ExternalControlUnitParameterIdValue)catt.ConstructorArguments?[0].Value == parameter.ControlUnit));

					if (property != null && parameter != null)
					{
						var catt = (ControlUnitParamAttribute)property.GetCustomAttributes(typeof(ControlUnitParamAttribute)).FirstOrDefault();
						if (parameter.ControlUnit == catt.ControlUnitParameterId)
						{
							var parameterValue = Mapper.Map<ValueTExternalModel>(parameter.Value);
							var parameterScaling = Mapper.Map<ScalingTExternalModel>(parameter.Scaling);

							var value = catt.GetValue(parameter.IdCase, parameterValue, parameterScaling);
							if (value != null)
							{
								property.SetValue(rootModel, value);
							}
						}

					}
				}
			}

			return new Tuple<CockpitControlUnitRootModel, List<ParameterControlUnitCharacteristicExternalEntity>>(rootModel, parameterCharacteristicList);
		}

		public InitialStartupWizardModel MapToConfigurationModel(CockpitControlUnitRootModel rootModel)
		{
			return Mapper.Map<InitialStartupWizardModel>(rootModel);
		}
	}
}
