﻿using EmmeApp.Common.Exceptions;
using EmmeApp.DataContracts.Response;
using EmmeApp.Entities;
using EmmeApp.Managers.Abstractions;
using EmmeApp.WebServices.Abstractions;
using Plugin.Connectivity.Abstractions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace EmmeApp.Managers
{
    public class DocumentsManager : ManagerBase, IDocumentsManager
	{
		private readonly IDocumentsWebService _documentsWebService;
		public DocumentsManager(IConnectivity connectivity, IServiceEntityMapper mapper, IDocumentsWebService documentsWebService) : base(connectivity, mapper)
		{
			_documentsWebService = documentsWebService;
		}

		public async Task<Tuple<List<Grouping<string, SearchResultItemEntity>>, List<Grouping<string, SearchFilterEntity>>>> Search(string searchQuery, string filterLang, string lang, string filterCountry)
		{
			if (!Connectivity.IsConnected)
			{
				throw new NoInternetConnectivityException();
			}

			var searchRootContract = await _documentsWebService.Search(searchQuery, filterLang, lang, filterCountry);

			if (searchRootContract != null)
			{
				if (searchRootContract.GetType() == typeof(SearchContentByTypeRootContract))
				{
					var searchRootEntity = Mapper.Map<SearchContentByTypeRootEntity>(searchRootContract);
					return GetDataContentByType(searchRootEntity);
				}
				else if (searchRootContract.GetType() == typeof(SearchPagesByCatalogRootContract))
				{
					var searchRootEntity = Mapper.Map<SearchPagesByCatalogRootEntity>(searchRootContract);
					return GetDataPagesByCatalog(searchRootEntity);
				}
			}

			return null;
		}

		private Tuple<List<Grouping<string, SearchResultItemEntity>>, List<Grouping<string, SearchFilterEntity>>> GetDataContentByType(SearchContentByTypeRootEntity searchRootEntity)
		{
			var result = searchRootEntity;

			var catalogsListResult = result.Groups.Select(x => x.Catalogs).ToList();
			var searchResultItemList = new List<SearchResultItemEntity>();

			foreach (var catalogListResult in catalogsListResult)
			{
				var searchResultItems = catalogListResult.Select(x => new SearchResultItemEntity()
				{
					Key = x.TypeName,
					Name = x.Name,
					PreviewImage = x.MediumUrl,
					DocumentLink = string.Format("{0}{1}", x.DownloadUrl, "&inline=true")
				});

#if DEBUG
				foreach (var item in catalogListResult)
				{
					Debug.WriteLine($"{nameof(SearchCatalogEntity.MediumUrl)}: {item.MediumUrl} ----- {nameof(SearchCatalogEntity.PreviewUrl)}: {item.PreviewUrl}");
				}
#endif

				searchResultItemList.AddRange(searchResultItems);
			}

			var groupSortedResults = searchResultItemList
				.GroupBy(item => item.Key.ToUpper().ToString())
				.Select(itemGroup => new Grouping<string, SearchResultItemEntity>(itemGroup.Key, itemGroup))
				.ToList();

			var groupSortedFilters = GetGroupFilters(searchRootEntity.Filters);

			return new Tuple<List<Grouping<string, SearchResultItemEntity>>, List<Grouping<string, SearchFilterEntity>>>(groupSortedResults, groupSortedFilters);
		}

		private Tuple<List<Grouping<string, SearchResultItemEntity>>, List<Grouping<string, SearchFilterEntity>>> GetDataPagesByCatalog(SearchPagesByCatalogRootEntity searchRootEntity)
		{
			var result = searchRootEntity;

			var catalogsListResult = result.Groups.ToList();
			var searchResultItemList = new List<SearchResultItemEntity>();

			foreach (var catalogListResult in catalogsListResult)
			{
				var searchResultItems = new List<SearchResultItemEntity>();
				searchResultItems.Add(new SearchResultItemEntity()
				{
					Name = string.Empty,
					Key = catalogListResult.Name,
					PreviewImage = catalogListResult.MediumUrl,
					DocumentLink = string.Format("{0}{1}", catalogListResult.DownloadUrl, "&inline=true")
				});

				foreach (var page in catalogListResult.Pages)
				{
					searchResultItems.Add(new SearchResultItemEntity()
					{
						Name = page.PageName,
						Key = catalogListResult.Name,
						PreviewImage = page.MediumUrl,
						PageNumber = page.PagePage,
						DocumentLink = string.Format("{0}{1}", catalogListResult.DownloadUrl, "&inline=true")
					});
				}

				searchResultItemList.AddRange(searchResultItems);
			}

			var groupSortedResults = searchResultItemList
				.GroupBy(item => item.Key.ToUpper().ToString())
				.Select(itemGroup => new Grouping<string, SearchResultItemEntity>(itemGroup.Key, itemGroup))
				.ToList();

			var groupSortedFilters = GetGroupFilters(searchRootEntity.Filters);

			return new Tuple<List<Grouping<string, SearchResultItemEntity>>, List<Grouping<string, SearchFilterEntity>>>(groupSortedResults, groupSortedFilters);
		}

		private List<Grouping<string, SearchFilterEntity>> GetGroupFilters(List<SearchFilterEntity> searchFilterListEntity)
		{
			var sortedFilter = searchFilterListEntity
				.GroupBy(item => item.Group.ToUpper().ToString())
				.Select(itemGroup => new Grouping<string, SearchFilterEntity>(itemGroup.Key, itemGroup))
				.ToList();

			return sortedFilter;
		}

		public async Task<string> GetLink(string pdfLink)
		{
			if (!Connectivity.IsConnected)
			{
				throw new NoInternetConnectivityException();
			}

			return await _documentsWebService.GetLink(pdfLink);
		}

		public async Task<CatalogDetailsEntity> GetCatalogDetails(string portal, string catalog)
		{
			if (!Connectivity.IsConnected)
			{
				throw new NoInternetConnectivityException();
			}

			var resultContract = await _documentsWebService.GetCatalogDetails(portal, catalog);
			var resultEntity = Mapper.Map<CatalogDetailsEntity>(resultContract);

			return resultEntity;
		}
	}
}
