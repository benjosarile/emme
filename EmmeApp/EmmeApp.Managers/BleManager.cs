﻿using EmmeApp.Common.Constants;
using EmmeApp.Common.Enum;
using EmmeApp.DataContracts.Enums;
using EmmeApp.DataContracts.External;
using EmmeApp.Entities;
using EmmeApp.Entities.External;
using EmmeApp.LocalData.Models;
using EmmeApp.Managers.Abstractions;
using EmmeApp.ProtocolServices.Abstractions;
using Plugin.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace EmmeApp.Managers
{
    public class BleManager : IBleManager
    {
        private class DeviceInfoImpl : DeviceInfo
        {
            private readonly IDevice _device = null;

            public override string Name => _device.Name;
            public override Guid UUID => _device.Uuid;

            public DeviceInfoImpl(IDevice device)
            {
                _device = device ?? throw new ArgumentNullException(nameof(device));
            }
        }

        private readonly IAdapter _adapter;
        private readonly IBleAdapterService _adapterService;
        private readonly IAssemblyResourceManager _assemblyResourceManager;
        private readonly IServiceEntityMapper _serviceEntityMapper;
        private readonly IProtocolBufferService _protocolBufferService;
        private readonly IJsonService _jsonService;
        private readonly SemaphoreSlim _writeCommandThrottle = new SemaphoreSlim(1, 1);

        public BleManager(
           IAdapter adapter,
           IBleAdapterService adapterService,
           IServiceEntityMapper serviceEntityMapper,
           IAssemblyResourceManager assemblyResourceManager,
           IProtocolBufferService protocolBufferService,
           IJsonService jsonService)
        {
            _adapter = adapter;
            _adapterService = adapterService;
            _serviceEntityMapper = serviceEntityMapper;
            _assemblyResourceManager = assemblyResourceManager;
            _protocolBufferService = protocolBufferService;
            _jsonService = jsonService;
        }

        public async Task<IEnumerable<DeviceInfo>> ScanForDevices()
        {
            var scannedDevicesByUUID = new Dictionary<Guid, DeviceInfo>();

            void scanResultCallback(IScanResult scanResult)
            {
                var device = scanResult.Device;
                var deviceName = scanResult.Device?.Name ?? null;

                if (string.IsNullOrWhiteSpace(deviceName))
                    return;

                if (scannedDevicesByUUID.ContainsKey(device.Uuid))
                    return;

                scannedDevicesByUUID.Add(device.Uuid, new DeviceInfoImpl(device));
            }

            var scanSubscriptionHandle =
               _adapter
                  .Scan()
                  .Subscribe(scanResultCallback);

            await Task.Delay(3000);
            scanSubscriptionHandle?.Dispose();
            _adapter.StopScan();

            return scannedDevicesByUUID.Values;
        }

        public async Task<IDevice> GetKnownDevice(Guid deviceId)
        {
            return await _adapterService.GetKnownDeviceAsync(deviceId);
        }

        public List<BleServiceEntity> GetAvailableServices()
        {
            var resourceAsString = _assemblyResourceManager.GetResourceAsString(AssemblyResourceNames.BleServices);
            var bleServiceRoot = _jsonService.DeserializeObject<BleServiceRootLocalData>(resourceAsString);
            var bleServices = _serviceEntityMapper.Map<List<BleServiceEntity>>(bleServiceRoot.BleServices);
            return bleServices;
        }

        public async Task<IDevice> GetConnectedDevice()
        {
            var availableServices = GetAvailableServices();

            //TODO: 2162-B Add an empty entry below for devices with no names
            List<string> deviceNames = new List<string>()
            {
                "BIRAL",
                "MODULA",
                "MODULA-D",
                "VARIA",
                "VARIA-E",
                "VIVARA",
                ""
            };

            foreach (var availableService in availableServices)
            {
                //TODO: 2162-B if device has no name, 
                //this will result to null the will push the UI to the dashboard
                var results = await _adapterService.GetConnectedDevices(Guid.Parse(availableService.Uuid));
                var connectedDevice = results.FirstOrDefault(x =>
                {
                    
                    var name = x.Name ?? string.Empty;
                    name = name.ToUpper();

                    return deviceNames.Any(y => name.Contains(y));
                });

                if (connectedDevice != null)
                {
                    return connectedDevice;
                }
            }

            return null;
        }

        public async Task<IGattCharacteristic> GetDeviceReadCharacteristic(IDevice connectedDevice)
        {
            IGattCharacteristic selectedCharacteristic = null;
            if (connectedDevice != null)
            {
                var availableServices = GetAvailableServices();

                foreach (var availableService in availableServices)
                {
                    var serviceGuid = Guid.Parse(availableService.Uuid);

                    try
                    {
                        var gattService = await connectedDevice.GetKnownService(serviceGuid);

                        if (gattService != null)
                        {
                            var characteristicIds = availableService.Characteristics.Where(x => x.Role == "Notification").Select(x => Guid.Parse(x.Uuid)).ToArray();

                            var gattCharacteristic = await connectedDevice.GetKnownCharacteristics(serviceGuid, characteristicIds);

                            if (gattCharacteristic != null)
                            {
                                selectedCharacteristic = gattCharacteristic;
                            }
                        }
                    }
                    catch
                    {
                        selectedCharacteristic = null;
                    }

                    if (selectedCharacteristic != null)
                    {
                        return selectedCharacteristic;
                    }
                }
            }

            return null;
        }

        public async Task<IGattCharacteristic> GetDeviceWriteCharacteristic(IDevice connectedDevice)
        {
            IGattCharacteristic selectedCharacteristic = null;
            if (connectedDevice != null)
            {
                var availableServices = GetAvailableServices();

                foreach (var availableService in availableServices)
                {
                    var serviceGuid = Guid.Parse(availableService.Uuid);

                    try
                    {
                        var gattService = await connectedDevice.GetKnownService(serviceGuid);
                        if (gattService != null)
                        {
                            var characteristicIds = availableService.Characteristics.Where(x => x.Role == "Write").Select(x => Guid.Parse(x.Uuid)).ToArray();
                            var gattCharacteristic = await connectedDevice.GetKnownCharacteristics(serviceGuid, characteristicIds);

                            if (gattCharacteristic != null)
                            {
                                selectedCharacteristic = gattCharacteristic;
                            }
                        }
                    }
                    catch
                    {
                        selectedCharacteristic = null;
                    }

                    if (selectedCharacteristic != null)
                    {
                        return selectedCharacteristic;
                    }
                }
            }

            return null;
        }

        public async Task<Tuple<List<byte[]>, IDisposable>> TriggerRead(IDevice connectedDevice)
        {
            await Task.Delay(2000);
            var newDevice = await RestartTheDevice(connectedDevice);

            IDisposable notifyDisposable = null;
            bool shouldAcquireNewData = false;

            if (newDevice != null)
            {
                var characteristic = await GetDeviceReadCharacteristic(newDevice);

                if (characteristic != null)
                {
                    var bleParameterData = new List<byte[]>();

                    shouldAcquireNewData = true;
                    notifyDisposable = characteristic
                       .WhenNotificationReceived()
                       .Subscribe(
                          result =>
                          {
                              if (result.Data != null && shouldAcquireNewData)
                              {
                                  var resultData = result.Data;
                                  bleParameterData.Add(resultData);
                              }
                          }, ex => { Debug.WriteLine(ex.Message); });

                    await characteristic.EnableNotifications();

                    int parameterCtr = 0;
                    while (parameterCtr < 70)
                    {
                        parameterCtr = bleParameterData.Count;
                        await Task.Delay(100);
                    }

                    shouldAcquireNewData = true;

                    newDevice = await GetConnectedDevice();

                    if (newDevice != null)
                    {
                        shouldAcquireNewData = false;
                        return new Tuple<List<byte[]>, IDisposable>(bleParameterData, notifyDisposable);
                    }
                }
            }

            return new Tuple<List<byte[]>, IDisposable>(new List<byte[]>(), notifyDisposable);
        }

        public IDevice ConnectToDevice(IDevice device)
        {
            device.CancelConnection();
            device.Connect(new ConnectionConfig()
            {
                AndroidConnectionPriority = ConnectionPriority.Normal, //TODO
                AutoConnect = true
            });

            return device;
        }

        public async Task<IDevice> RestartTheDevice(IDevice device)
        {
            if (device != null)
            {
                var selectedDevice = await Task.Run<IDevice>(async () =>
                {
                    device.CancelConnection();
                    int currentConnectionCount = 0;

                    //TODO UserStory 2023
                    //Changed maxConnectionCount from 5000 to 1000
                    int maxConnectionCount = 5000;
                    while (device.Status != ConnectionStatus.Disconnected)
                    {
                        if (currentConnectionCount == maxConnectionCount)
                        {
                            return null;
                        }
                        await Task.Delay(200);
                        currentConnectionCount++;
                    }

                    await Task.Delay(2000);
                    //await Task.Delay(2000);

                    currentConnectionCount = 0;
                    var newDevice = await _adapterService.GetKnownDeviceAsync(device.Uuid);

                    newDevice.Connect(new ConnectionConfig()
                    {
                        AndroidConnectionPriority = ConnectionPriority.Normal,
                        AutoConnect = true
                    });

                    while (newDevice.Status != ConnectionStatus.Connected)
                    {
                        if (currentConnectionCount == maxConnectionCount)
                        {
                            return null;
                        }

                        await Task.Delay(200);
                        currentConnectionCount++;
                    }

                    return newDevice;
                });

                return selectedDevice;
            }

            return null;

        }

        public InitCommunicationTExternalEntity GetInitCommunication(byte[] data)
        {
            var resultContract = _protocolBufferService.Deserialize<InitCommunicationTExternalContract>(data);
            var resultEntity = _serviceEntityMapper.Map<InitCommunicationTExternalEntity>(resultContract);

            return resultEntity;
        }

        public List<ParameterTExternalEntity> GetInitializeParameters(byte[] data)
        {
            var resultContract = _protocolBufferService.Deserialize<ParametersTExternalContract>(data);
            var resultEntity = _serviceEntityMapper.Map<ParametersTExternalEntity>(resultContract);

            if (resultEntity != null)
            {
                foreach (var parameter in resultEntity.Parameters)
                {
                    if (parameter.Scaling != null)
                    {
                        parameter.Scaling.Step = Math.Round(parameter.Scaling.Step, 5);
                    }
                }
            }

            return resultEntity != null ? resultEntity.Parameters : new List<ParameterTExternalEntity>();
        }

        public List<LogEntryTExternalEntity> GetLogEntries(byte[] data)
        {
            var resultContract = _protocolBufferService.Deserialize<LogEntriesTExternalContract>(data);
            var resultEntity = _serviceEntityMapper.Map<LogEntriesTExternalEntity>(resultContract);

            return resultEntity != null ? resultEntity.LogEntries : new List<LogEntryTExternalEntity>();
        }

        public Tuple<InitCommunicationTExternalEntity, List<ParameterTExternalEntity>, List<LogEntryTExternalEntity>> GetInitialData(List<byte[]> bleParameterData)
        {
            InitCommunicationTExternalEntity initCommunication = null;
            var parameters = new List<ParameterTExternalEntity>();
            var logs = new List<LogEntryTExternalEntity>();

            bleParameterData = bleParameterData ?? new List<byte[]>();

            foreach (var rowData in bleParameterData)
            {

                if (rowData == null)
                {
                    continue;
                }

                if (initCommunication == null)
                {
                    initCommunication = GetInitCommunication(rowData);
                    continue;
                }

                var initParameters = GetInitializeParameters(rowData);
                parameters.AddRange(initParameters);

                var logEntries = GetLogEntries(rowData);
                if (logEntries != null)
                {
                    logs.AddRange(logEntries);
                }
            }

            var newParameters = parameters.Where(x => x.Read != ExternalAccessLevelValue.UnknownLevel && x.Write != ExternalAccessLevelValue.UnknownLevel).ToList();

            foreach (var currentParameter in parameters)
            {
                var newParameter = newParameters.FirstOrDefault(x => x.ControlUnit == currentParameter.ControlUnit && x.ModulA == currentParameter.ModulA && x.VariA == currentParameter.VariA);

                if (newParameter != null)
                {
                    newParameter.Value = currentParameter.Value;

                    if (currentParameter.Unit != ExternalUnitTValue.UnknownUnit)
                    {
                        newParameter.Unit = currentParameter.Unit;
                    }

                    if (currentParameter.Scaling != null)
                    {
                        newParameter.Scaling = currentParameter.Scaling;
                    }
                }
            }

            return new Tuple<InitCommunicationTExternalEntity, List<ParameterTExternalEntity>, List<LogEntryTExternalEntity>>(initCommunication, newParameters, logs);
        }

        public async Task<bool> WriteCommand(ParametersTExternalEntity parametersEntity, IGattCharacteristic characteristic, ExternalIdOneofCaseValue idCase)
        {
            try
            {
                await _writeCommandThrottle.WaitAsync();

                List<byte> bufferList = new List<byte>();

                if (idCase == ExternalIdOneofCaseValue.ControlUnit)
                {
                    var parametersContract = new WriteParametersControlUnitExternalContract();

                    foreach (var parameter in parametersEntity.Parameters)
                    {
                        var parameterContract = new WriteParameterControlUnitExternalContract();
                        parameterContract.ControlUnit = (ExternalControlUnitParameterIdEnumContract)Convert.ToInt32(parameter.ControlUnit);

                        if (parameter.Value != null)
                        {
                            if (parameter.Value.ValueCase == ExternalValueOneofCaseValue.Number)
                            {
                                parameterContract.Value.Number = parameter.Value.Number;
                            }
                            else if (parameter.Value.ValueCase == ExternalValueOneofCaseValue.Text)
                            {
                                parameterContract.Value.Text = parameter.Value.Text;
                            }

                            parametersContract.Parameters.Add(parameterContract);
                        }
                    }

                    bufferList = _protocolBufferService.Serialize(parametersContract).ToList();
                }
                else if (idCase == ExternalIdOneofCaseValue.ModulA)
                {
                    var parametersContract = new WriteParametersPumpModulAExternalContract();

                    foreach (var parameter in parametersEntity.Parameters)
                    {
                        var parameterContract = new WriteParameterPumpModulAExternalContract();
                        parameterContract.ModulA = (ExternalPumpModulAParameterIdEnumContract)Convert.ToInt32(parameter.ModulA);

                        if (parameter.Value != null)
                        {
                            if (parameter.Value.ValueCase == ExternalValueOneofCaseValue.Number)
                            {
                                parameterContract.Value.Number = parameter.Value.Number;
                            }
                            else if (parameter.Value.ValueCase == ExternalValueOneofCaseValue.Text)
                            {
                                parameterContract.Value.Text = parameter.Value.Text;
                            }

                            parametersContract.Parameters.Add(parameterContract);
                        }
                    }

                    bufferList = _protocolBufferService.Serialize(parametersContract).ToList();
                }
                else if (idCase == ExternalIdOneofCaseValue.VariA)
                {
                    var parametersContract = new WriteParametersPumpVariAExternalContract();

                    foreach (var parameter in parametersEntity.Parameters)
                    {
                        var parameterContract = new WriteParameterPumpVariAExternalContract();
                        parameterContract.VariA = (ExternalPumpVariAParameterIdEnumContract)Convert.ToInt32(parameter.VariA);

                        if (parameter.Value != null)
                        {
                            if (parameter.Value.ValueCase == ExternalValueOneofCaseValue.Number)
                            {
                                parameterContract.Value.Number = parameter.Value.Number;
                            }
                            else if (parameter.Value.ValueCase == ExternalValueOneofCaseValue.Text)
                            {
                                parameterContract.Value.Text = parameter.Value.Text;
                            }

                            parametersContract.Parameters.Add(parameterContract);
                        }
                    }

                    bufferList = _protocolBufferService.Serialize(parametersContract).ToList();
                }

                try
                {
                    var result = await characteristic.Write(bufferList.ToArray());
                    return result != null;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }

                return false;
            }
            finally
            {
                _writeCommandThrottle.Release();
            }
        }
    }
}