﻿using EmmeApp.Managers.Abstractions;
using Plugin.Connectivity.Abstractions;

namespace EmmeApp.Managers
{
    public class ManagerBase 
	{
		protected readonly IConnectivity Connectivity;
		protected readonly IServiceEntityMapper Mapper;

		public ManagerBase(IServiceEntityMapper mapper)
		{
			this.Mapper = mapper;
		}

		public ManagerBase(IConnectivity connectivity, IServiceEntityMapper mapper)
		{
			this.Connectivity = connectivity;
			this.Mapper = mapper;
		}

		public bool IsInternetAvailable()
		{
			return Connectivity.IsConnected;
		}
	}
}
