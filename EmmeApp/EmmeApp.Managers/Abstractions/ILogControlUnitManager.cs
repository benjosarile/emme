﻿using EmmeApp.Common.PageModels.Log;
using EmmeApp.Entities.External;
using System.Collections.Generic;
using System.Text;

namespace EmmeApp.Managers.Abstractions
{
	public interface ILogControlUnitManager
	{
		StringBuilder GenerateEmailContent(ControlUnitLogRootModel rootModel, string newLine = "\r\n");
		ControlUnitLogRootModel MapParametersToWizardModel(ControlUnitLogRootModel rootModel, List<LogEntryTExternalEntity> logs);
	}
}