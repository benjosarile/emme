﻿using EmmeApp.Entities;

namespace EmmeApp.Managers.Abstractions
{
	public interface IBiralProductManager
	{
		BiralProductEntity GetBiralProductDetail(PumpReplacementProductEntity pumpReplacementProductEntity);
	}
}