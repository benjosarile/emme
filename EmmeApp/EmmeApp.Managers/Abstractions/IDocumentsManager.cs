﻿using EmmeApp.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmmeApp.Managers.Abstractions
{
	public interface IDocumentsManager
	{
		Task<Tuple<List<Grouping<string, SearchResultItemEntity>>, List<Grouping<string, SearchFilterEntity>>>> Search(string searchQuery, string filterLang, string lang, string filterCountry);
		Task<string> GetLink(string pdfLink);
		Task<CatalogDetailsEntity> GetCatalogDetails(string portal, string catalog);
	}
}