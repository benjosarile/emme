﻿using EmmeApp.Entities;

namespace EmmeApp.Managers.Abstractions
{
	public interface IProductCategoryManager
	{
		ProductCategoryEntity GetCategory(long categoryId);
	}
}