﻿using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Configuration;
using EmmeApp.Entities.External;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmmeApp.Managers.Abstractions
{
    public interface IConfigurationPumpVariAManager
    {
		Tuple<PumpConfigurationVariAWizardModel, List<ParameterPumpVariACharacteristicExternalEntity>> MapParametersToWizardModel(PumpConfigurationVariAWizardModel rootModel, List<ParameterTExternalEntity> parameters);
		List<List<ParameterTExternalEntity>> MapWizardModelToParameters(PumpConfigurationVariAWizardModel rootModel, List<ParameterPumpVariACharacteristicExternalEntity> characteristics, ExternalPumpVariAParameterIdValue parameterIdValue);
        StringBuilder GenerateEmailContent(PumpConfigurationVariAWizardModel wizardModel, string newLine = "\r\n");
		string GetUnit(ExternalPumpVariAParameterIdValue parameterId, List<ParameterTExternalEntity> parameters);

	}
}
