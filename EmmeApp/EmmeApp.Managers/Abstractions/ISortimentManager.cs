﻿using EmmeApp.Entities;
using System;
using System.Threading.Tasks;

namespace EmmeApp.Managers.Abstractions
{
	public interface ISortimentManager
	{
		SortimentEntity GetSortimentFromAssemblyResource();
		int GetSortimentVersionFromAssemblyResource();
		int GetSortimentVersionFromDB();
		void SaveSortimentsToDB();
		SortimentEntity GetSortimentsFromDB();
		SortimentMarketEntity GetSortimentMarketsFromDB(string marketKey);
		DateTimeOffset? GetPumpReplacementLastModified();
		void SetPumpReplacementLastModified(DateTimeOffset lastModified);
		Task<bool> HasUpdate(DateTimeOffset? lastModified);
	}
}