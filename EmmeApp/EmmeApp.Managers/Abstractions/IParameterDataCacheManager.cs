﻿using EmmeApp.Entities.External;
using Plugin.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmmeApp.Managers.Abstractions
{
    public interface IParameterDataCacheManager
   {
      bool HasValidInitialData { get; }

      InitCommunicationTExternalEntity InitCommunication { get; }

      Task<Tuple<InitCommunicationTExternalEntity, List<ParameterTExternalEntity>, List<LogEntryTExternalEntity>, IDevice>> GetAndCacheParameters(bool restartDevice = false, Action<Exception> onError = null);

      void Reset();
   }
}
