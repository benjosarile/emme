﻿using EmmeApp.Entities;

namespace EmmeApp.Managers.Abstractions
{
    public interface IBiralContactManager
	{
		BiralContactEntity GetCurrentBiralContact();
		BiralContactEntity GetDefaultBiralContact();
	}
}