﻿namespace EmmeApp.Managers.Abstractions
{
	public interface IServiceEntityMapper
	{
		TDestination Map<TSource, TDestination>(TSource value);
		TDestination Map<TDestination>(object value);

	}
}