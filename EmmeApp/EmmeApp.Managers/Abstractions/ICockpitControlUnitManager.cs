﻿using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Entities.External;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmmeApp.Managers.Abstractions
{
	public interface ICockpitControlUnitManager
	{
		string GetUnit(ExternalControlUnitParameterIdValue parameterId, List<ParameterTExternalEntity> parameters);
		Tuple<CockpitControlUnitRootModel, List<ParameterControlUnitCharacteristicExternalEntity>> MapParamtersToModel(CockpitControlUnitRootModel rootModel, List<ParameterTExternalEntity> parameters);
		InitialStartupWizardModel MapToConfigurationModel(CockpitControlUnitRootModel rootModel);
		StringBuilder GenerateEmailContent(CockpitControlUnitRootModel rootModel, string newLine = "\r\n");
	}
}