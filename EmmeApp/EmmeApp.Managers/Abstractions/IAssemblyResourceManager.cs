﻿namespace EmmeApp.Managers.Abstractions
{
    public interface IAssemblyResourceManager
	{
		string GetResourceAsString(string filename);
	}
}
