﻿using EmmeApp.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace EmmeApp.Managers.Abstractions
{
    public interface IOcrManager
    {
        bool CanCaptureImage { get; }
        Task<OcrObjectRootEntity> ReadTextOnImage(byte[] byteData, CancellationToken cancellationToken);
        bool DidOcrWasUsed();
        void ConfirmOcrUsed();
    }
}