﻿using EmmeApp.Entities;
using System.Collections.Generic;

namespace EmmeApp.Managers.Abstractions
{
	public interface IPumpReplacementManager
	{
		PumpReplacementEntity GetOldPumpDetails(string pumpType);
		List<PumpReplacementEntity> GetOldPumps();
	}
}