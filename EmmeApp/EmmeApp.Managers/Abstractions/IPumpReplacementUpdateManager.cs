﻿using System.Threading.Tasks;

namespace EmmeApp.Managers.Abstractions
{
	public interface IPumpReplacementUpdateManager
	{
		Task<bool> Update();
	}
}