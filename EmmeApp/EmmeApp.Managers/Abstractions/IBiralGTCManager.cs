﻿using EmmeApp.Entities;

namespace EmmeApp.Managers.Abstractions
{
	public interface IBiralGtcManager
	{
		bool HasGtcConfirmed();
		void ConfirmGtc();
		BiralGtcEntity GetCurrentBiralGtc();
	}
}