﻿using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.Configuration;
using EmmeApp.Entities.External;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmmeApp.Managers.Abstractions
{
    public interface ICockpitPumpsModulAManager
    {
		Tuple<CockpitPumpsModulARootModel, List<ParameterPumpModulACharacteristicExternalEntity>> MapParametersToModel(CockpitPumpsModulARootModel rootModel, List<ParameterTExternalEntity> parameters);
        StringBuilder GenerateEmailContent(CockpitPumpsModulARootModel rootModel, string newLine = "\r\n");
        PumpConfigurationModulAWizardModel MapToConfigurationModel(CockpitPumpsModulARootModel rootModel);
    }
}
