﻿using EmmeApp.Common.Enum;
using EmmeApp.Entities;
using EmmeApp.Entities.External;
using Plugin.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmmeApp.Managers.Abstractions
{
    public interface IBleManager
	{
		Task<IEnumerable<DeviceInfo>> ScanForDevices();

		List<BleServiceEntity> GetAvailableServices();

		Task<IDevice> GetKnownDevice(Guid deviceId);
		Task<IDevice> GetConnectedDevice();
		Task<IGattCharacteristic> GetDeviceReadCharacteristic(IDevice connectedDevice);
		Task<IGattCharacteristic> GetDeviceWriteCharacteristic(IDevice connectedDevice);
		IDevice ConnectToDevice(IDevice device);
		Task<IDevice> RestartTheDevice(IDevice device);
		Task<Tuple<List<byte[]>, IDisposable>> TriggerRead(IDevice connectedDevice);

		InitCommunicationTExternalEntity GetInitCommunication(byte[] data);
		List<ParameterTExternalEntity> GetInitializeParameters(byte[] data);
		List<LogEntryTExternalEntity> GetLogEntries(byte[] data);
		Tuple<InitCommunicationTExternalEntity, List<ParameterTExternalEntity>, List<LogEntryTExternalEntity>> GetInitialData(List<byte[]> bleParameterData);
		Task<bool> WriteCommand(ParametersTExternalEntity parametersEntity, IGattCharacteristic characteristic, ExternalIdOneofCaseValue idCase);
	}
}
