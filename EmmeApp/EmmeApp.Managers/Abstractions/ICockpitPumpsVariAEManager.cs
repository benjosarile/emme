﻿using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.Configuration;
using EmmeApp.Entities.External;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmmeApp.Managers.Abstractions
{
    public interface ICockpitPumpsVariAEManager
    {
		Tuple<CockpitPumpsVariAERootModel, List<ParameterPumpVariACharacteristicExternalEntity>> MapParametersToModel(CockpitPumpsVariAERootModel rootModel, List<ParameterTExternalEntity> parameters);
        StringBuilder GenerateEmailContent(CockpitPumpsVariAERootModel rootModel, string newLine = "\r\n");
        PumpConfigurationVariAWizardModel MapToConfigurationModel(CockpitPumpsVariAERootModel rootModel);
    }
}
