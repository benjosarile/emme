﻿using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Configuration;
using EmmeApp.Entities.External;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmmeApp.Managers.Abstractions
{
    public interface IConfigurationPumpModulAManager
    {
		string GetUnit(ExternalPumpModulAParameterIdValue parameterId, List<ParameterTExternalEntity> parameters);
		Tuple<PumpConfigurationModulAWizardModel, List<ParameterPumpModulACharacteristicExternalEntity>> MapParametersToWizardModel(PumpConfigurationModulAWizardModel rootModel, List<ParameterTExternalEntity> parameters);
		List<List<ParameterTExternalEntity>> MapWizardModelToParameters(PumpConfigurationModulAWizardModel rootModel, List<ParameterPumpModulACharacteristicExternalEntity> characteristics, ExternalPumpModulAParameterIdValue parameterIdValue);
        StringBuilder GenerateEmailContent(PumpConfigurationModulAWizardModel wizardModel, string newLine = "\r\n");
    }
}
