﻿using EmmeApp.DataContracts.Response;
using EmmeApp.Entities;
using System.Collections.Generic;

namespace EmmeApp.Managers.Abstractions
{
    public interface IMasterPumpReplacementManager
    {
        List<MasterPumpReplacementEntity> GetList();
		void PopulateMasterPumpReplacementData();
		void PopulatePumpReplacementFile(string marketKey);
		void PopulatePumpReplacement(string marketKey, PumpReplacementRootContract pumpReplacementRootContract);
		void PopulatePumpAdditionalInformation();
		void DeleteAllPumpReplacement();

		void PopulateCategories(List<ProductCategoryContract> categories);
		void PopulateComments(List<ProductCommentContract> comments);
		void PopulateConnectors(List<ProductConnectorContract> connectors);
	}
}
