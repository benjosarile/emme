﻿using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Entities.External;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmmeApp.Managers.Abstractions
{
	public interface IConfigurationControlUnitManager
	{
		Tuple<InitialStartupWizardModel, List<ParameterControlUnitCharacteristicExternalEntity>> MapParametersToWizardModel(InitialStartupWizardModel rootModel, List<ParameterTExternalEntity> parameters);
		List<List<ParameterTExternalEntity>> MapWizardModelToParameters(InitialStartupWizardModel rootModel, List<ParameterControlUnitCharacteristicExternalEntity> characteristics);
		StringBuilder GenerateEmailContent(InitialStartupWizardModel wizardModel, string newLine = "\r\n");
	}
}
