﻿using EmmeApp.Entities;
using System.Collections.Generic;

namespace EmmeApp.Managers.Abstractions
{
    public interface IPumpReplacementProductManager
    {
        PumpReplacementProductEntity GetRelatedProduct(string type, long productId);
        List<PumpReplacementProductEntity> GetRelatedProducts(string pumpType);
    }
}