﻿using EmmeApp.Entities;
using System.Collections.Generic;

namespace EmmeApp.Managers.Abstractions
{
	public interface IProductCommentManager
	{
		List<ProductCommentEntity> GetComments();
		ProductCommentEntity GetComment(long commentId);

	}
}