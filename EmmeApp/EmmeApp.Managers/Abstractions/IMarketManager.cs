﻿using System;

namespace EmmeApp.Managers.Abstractions
{
	public interface IMarketManager
	{
		Tuple<string, string> GetMarketLanguage();
		Tuple<string, string> GetDocumentLanguage();
	}
}