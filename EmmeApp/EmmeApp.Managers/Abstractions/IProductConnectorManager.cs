﻿using EmmeApp.Entities;
using System.Collections.Generic;

namespace EmmeApp.Managers.Abstractions
{
	public interface IProductConnectorManager
	{
		List<ProductConnectorEntity> GetConnectors();
		ProductConnectorEntity GetConnector(string connectorId);
	}
}