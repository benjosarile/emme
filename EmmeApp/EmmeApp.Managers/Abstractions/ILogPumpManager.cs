﻿using EmmeApp.Common.PageModels.Log;
using EmmeApp.Entities.External;
using System.Collections.Generic;
using System.Text;

namespace EmmeApp.Managers.Abstractions
{
	public interface ILogPumpManager
	{
		PumpLogRootModel MapParametersToWizardModel(PumpLogRootModel rootModel, List<LogEntryTExternalEntity> logs);
		StringBuilder GenerateEmailContent(PumpLogRootModel rootModel, string newLine = "\r\n");
	}
}