﻿using EmmeApp.Common.Attributes;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.External;
using EmmeApp.Common.PageModels.InitialStartup;
using EmmeApp.Entities.External;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Humanizer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace EmmeApp.Managers
{
    public class ConfigurationControlUnitManager : ManagerBase, IConfigurationControlUnitManager
	{
		public ConfigurationControlUnitManager(IServiceEntityMapper mapper) : base(mapper)
		{
		}

		public StringBuilder GenerateEmailContent(InitialStartupWizardModel wizardModel, string newLine = "\r\n")
		{

			var sb = new StringBuilder();
			sb.Append($"{AppResources.InitialStartupForm1_CustomerInfoPage_Title}{newLine}");
			sb.Append($"{AppResources.InitialStartupForm1_CustomerInfoPage_ControlTitle_Name}: {wizardModel.CustomerInformationName}{newLine}");
			sb.Append($"{AppResources.InitialStartupForm1_CustomerInfoPage_ControlTitle_Phone}: {wizardModel.CustomerInformationPhone}{newLine}");
			sb.Append($"{AppResources.InitialStartupForm1_CustomerInfoPage_ControlTitle_Street}: {wizardModel.CustomerInformationStreet}{newLine}");
			sb.Append($"{AppResources.InitialStartupForm1_CustomerInfoPage_ControlTitle_City}: {wizardModel.CustomerInformationCity}{newLine}");
			sb.Append($"{AppResources.InitialStartupForm1_CustomerInfoPage_ControlTitle_ZipCode}: {wizardModel.CustomerInformationZipCode}{newLine}");
			sb.Append($"{AppResources.InitialStartupForm1_CustomerInfoPage_ControlTitle_Country}: {wizardModel.CustomerInformationCountry}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.InitialStartupForm2_SystemDataConfigurationPage_Title}{newLine}");
			sb.Append($"{AppResources.InitialStartupForm2_SystemDataConfigurationPage_ControlTitle_SystemName}: {wizardModel.SystemDataConfigurationSystemName}{newLine}");
			sb.Append($"{AppResources.InitialStartupForm2_SystemDataConfigurationPage_ControlTitle_Motor1Name}: {wizardModel.SystemDataConfigurationMotor1Name}{newLine}");
			sb.Append($"{AppResources.InitialStartupForm2_SystemDataConfigurationPage_ControlTitle_Motor2Name}: {wizardModel.SystemDataConfigurationMotor2Name}{newLine}");
			sb.Append($"{AppResources.InitialStartupForm2_SystemDataConfigurationPage_ControlTitle_SystemType}: {wizardModel.SystemDataConfigurationSystemType.Humanize()}{newLine}");

			return sb;
		}

		public Tuple<InitialStartupWizardModel, List<ParameterControlUnitCharacteristicExternalEntity>> MapParametersToWizardModel(InitialStartupWizardModel rootModel, List<ParameterTExternalEntity> parameters)
		{
			var objtype = rootModel.GetType();
			var parameterCharacteristicList = new List<ParameterControlUnitCharacteristicExternalEntity>();

			parameters = parameters ?? new List<ParameterTExternalEntity>();

			foreach (var parameter in parameters)
			{
				if (parameter.IdCase == ExternalIdOneofCaseValue.ControlUnit && parameter.Value != null)
				{
					if (parameter.Scaling != null && parameter.Scaling.Step >= 0)
					{
						var characteristic = parameterCharacteristicList.FirstOrDefault(x => x.ControlUnit == parameter.ControlUnit);

						if (characteristic == null)
						{
							parameterCharacteristicList.Add(new ParameterControlUnitCharacteristicExternalEntity()
							{
								ControlUnit = parameter.ControlUnit,
								Scaling = parameter.Scaling,
								Unit = parameter.Unit
							});
						}
					}

					var property = objtype.GetProperties()
						.FirstOrDefault(p =>
											p.CustomAttributes
											.Any(catt =>
											catt.AttributeType == typeof(ControlUnitParamAttribute) &&
											(ExternalControlUnitParameterIdValue)catt.ConstructorArguments?[0].Value == parameter.ControlUnit));

					if (property != null && parameter != null)
					{
						var catt = (ControlUnitParamAttribute)property.GetCustomAttributes(typeof(ControlUnitParamAttribute)).FirstOrDefault();
						if (parameter.ControlUnit == catt.ControlUnitParameterId)
						{
							var parameterValue = Mapper.Map<ValueTExternalModel>(parameter.Value);
							var parameterScaling = Mapper.Map<ScalingTExternalModel>(parameter.Scaling);

							var value = catt.GetValue(parameter.IdCase, parameterValue, parameterScaling);
							if (value != null)
							{
								property.SetValue(rootModel, value);
							}
						}

					}
				}
			}

			return new Tuple<InitialStartupWizardModel, List<ParameterControlUnitCharacteristicExternalEntity>>(rootModel, parameterCharacteristicList);
		}

		

		public List<List<ParameterTExternalEntity>> MapWizardModelToParameters(InitialStartupWizardModel rootModel, List<ParameterControlUnitCharacteristicExternalEntity> characteristics)
		{
			var parameters = new List<ParameterTExternalEntity>();
			var objtype = rootModel.GetType();

			var properties = objtype.GetProperties()
						.Where(p => p.CustomAttributes.Any(catt =>
						catt.AttributeType == typeof(ControlUnitParamAttribute)));

			foreach (var property in properties)
			{
				var catt = (ControlUnitParamAttribute)property.GetCustomAttributes(typeof(ControlUnitParamAttribute)).FirstOrDefault();

				var parameter = GetParameter(catt.ControlUnitParameterId, characteristics, catt.Type, property.GetValue(rootModel));

				if(parameter != null)
				{
					parameters.Add(parameter);
				}
			}

			int groupBy = 2;
			var groupOfs = parameters.Select((str, index) => new { str, index }).GroupBy(x => x.index / groupBy)
				.Select(g => g.Select(x => x.str).ToList())
				.ToList();

			return groupOfs;
		}

		private ParameterTExternalEntity GetParameter(ExternalControlUnitParameterIdValue controlUnit, List<ParameterControlUnitCharacteristicExternalEntity> characteristics, Type type, object value)
		{
			ValueTExternalEntity valueExternalEntity = null;

			var characteristic = characteristics.FirstOrDefault(x => x.ControlUnit == controlUnit);

			if(value != null)
			{
				if (type == typeof(string))
				{
					valueExternalEntity = new ValueTExternalEntity()
					{
						Text = value.ToString(),
						ValueCase = ExternalValueOneofCaseValue.Text
					};
				}
				else if (characteristic != null)
				{
					long number = 0;
					if (type == typeof(bool) || type.IsEnum)
					{
						number = Convert.ToInt64(value);
					}
					else if (type == typeof(double))
					{
						if (characteristic.Scaling == null || characteristic.Scaling.Step == 0)
						{
							number = Convert.ToInt64(value);
						}
						else
						{
							double step = Math.Round(characteristic.Scaling.Step, 5);
							number = Convert.ToInt64(Convert.ToDouble(value) / step);
						}
					}
					else if (type == typeof(int))
					{
						number = Convert.ToInt64(value);
					}

					valueExternalEntity = new ValueTExternalEntity()
					{
						Number = number,
						ValueCase = ExternalValueOneofCaseValue.Number
					};
				}

			}

			return new ParameterTExternalEntity()
			{
				IdCase = ExternalIdOneofCaseValue.ControlUnit,
				ControlUnit = controlUnit,
				Value = valueExternalEntity
			};
		}
	}
}
