﻿using EmmeApp.Managers.Abstractions;
using System.IO;
using System.Reflection;

namespace EmmeApp.Managers
{
    public class AssemblyResourceManager : IAssemblyResourceManager
	{
		public string GetResourceAsString(string filename)
		{
			var assembly = IntrospectionExtensions.GetTypeInfo(typeof(EmmeApp.LocalData.MainClass)).Assembly;
			var stream = assembly.GetManifestResourceStream(filename);
			string text = string.Empty;
			using (var reader = new StreamReader(stream))
			{
				text = reader.ReadToEnd();
			}
			return text;
		}
	}
}