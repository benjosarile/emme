﻿using EmmeApp.Entities;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Repositories.Abstractions;
using System.Collections.Generic;

namespace EmmeApp.Managers
{
    public class PumpReplacementProductManager : ManagerBase, IPumpReplacementProductManager
    {
        private readonly IPumpReplacementProductRepository _pumpReplacementProductRepository;
        private readonly IMarketManager _marketManager;

        public PumpReplacementProductManager(IServiceEntityMapper mapper,
            IPumpReplacementProductRepository pumpReplacementProductRepository,
            IMarketManager marketManager) : base(mapper)
        {
            _pumpReplacementProductRepository = pumpReplacementProductRepository;
            _marketManager = marketManager;
        }

        public PumpReplacementProductEntity GetRelatedProduct(string type, long productId)
        {
            var marketKey = _marketManager.GetMarketLanguage().Item1;

            var pumpReplacementProductDto = this._pumpReplacementProductRepository.FirstOrDefault(x => x.MarketKey == marketKey && x.Type == type && x.ProductId == productId);
            return this.Mapper.Map<PumpReplacementProductEntity>(pumpReplacementProductDto);
        }

        public List<PumpReplacementProductEntity> GetRelatedProducts(string pumpType)
        {
            var marketKey = _marketManager.GetMarketLanguage().Item1;

            var relatedProductsDto = this._pumpReplacementProductRepository.Where(x => x.MarketKey == marketKey && x.Type == pumpType);
            var relatedProductsEntity = this.Mapper.Map<List<PumpReplacementProductEntity>>(relatedProductsDto);
            return relatedProductsEntity;
        }
    }
}
