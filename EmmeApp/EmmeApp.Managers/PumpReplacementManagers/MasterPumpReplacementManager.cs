﻿using EmmeApp.Common.Constants;
using EmmeApp.DataContracts.Response;
using EmmeApp.DataObjects;
using EmmeApp.Entities;
using EmmeApp.LocalData.Models;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Repositories.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EmmeApp.Managers
{
    public class MasterPumpReplacementManager : ManagerBase, IMasterPumpReplacementManager
	{
		private readonly IAssemblyResourceManager _fileManager;
		private readonly IMasterPumpReplacementRepository _masterPumpReplacementRepository;
		private readonly IPumpReplacementRepository _pumpReplacementRepository;
		private readonly IPumpReplacementProductRepository _pumpReplacementProductRepository;
		private readonly IBiralProductRepository _biralProductRepository;
		private readonly IProductCategoryRepository _productCategoryRepository;
		private readonly IProductCommentRepository _productCommentRepository;
		private readonly IProductConnectorRepository _productConnectorRepository;

		public MasterPumpReplacementManager(IServiceEntityMapper mapper,
			IAssemblyResourceManager fileManager,
			IMasterPumpReplacementRepository masterPumpReplacementRepository,
			IPumpReplacementRepository pumpReplacementRepository,
			IPumpReplacementProductRepository pumpReplacementProductRepository,
			IBiralProductRepository biralProductRepository,
			IProductCategoryRepository productCategoryRepository,
			IProductCommentRepository productCommentRepository, IProductConnectorRepository productConnectorRepository) : base(mapper)
		{
			_fileManager = fileManager;
			_masterPumpReplacementRepository = masterPumpReplacementRepository;
			_pumpReplacementRepository = pumpReplacementRepository;
			_pumpReplacementProductRepository = pumpReplacementProductRepository;
			_biralProductRepository = biralProductRepository;
			_productCategoryRepository = productCategoryRepository;
			_productCommentRepository = productCommentRepository;
			_productConnectorRepository = productConnectorRepository;
		}

		public List<MasterPumpReplacementEntity> GetList()
		{
			var temp = _masterPumpReplacementRepository.GetAll();
			return Mapper.Map<List<MasterPumpReplacementEntity>>(temp);
		}

		public void PopulateMasterPumpReplacementData()
		{
			string filename = AssemblyResourceNames.PumpReplacementMasterData;
			string masterPumpReplacementContent = _fileManager.GetResourceAsString(filename);

			var rows = masterPumpReplacementContent.Split(new string[] { "\r\n" }, options: StringSplitOptions.RemoveEmptyEntries).ToList();
			var datas = new List<MasterPumpReplacementDataObject>();

			foreach (var row in rows)
			{
				var columns = row.Split('|').ToList();
				var data = new MasterPumpReplacementDataObject();
				data.Type = columns[0];
				data.Diameter = columns[3];
				data.DiameterUnit = columns[8];
				data.Length = columns[4];
				data.LengthUnit = columns[9];
				data.Pressure = columns[1];
				data.PressureUnit = columns[7];
				data.Engine = columns[2];
				data.ArticleNumber = columns[5];
				data.CommentCodes = columns[6];

				datas.Add(data);
			}

			_masterPumpReplacementRepository.DeleteAll();
			_masterPumpReplacementRepository.SaveList(datas);
		}

		public void PopulatePumpReplacementFile(string marketKey)
		{
			string filename = AssemblyResourceNames.PumpReplacementMarketCh;

			switch (marketKey)
			{
				case AppConstants.MarketCH: filename = AssemblyResourceNames.PumpReplacementMarketCh; break;
				case AppConstants.MarketDE: filename = AssemblyResourceNames.PumpReplacementMarketDe; break;
				case AppConstants.MarketIT: filename = AssemblyResourceNames.PumpReplacementMarketIt; break;
				case AppConstants.MarketNL: filename = AssemblyResourceNames.PumpReplacementMarketNl; break;
				case AppConstants.MarketEU: filename = AssemblyResourceNames.PumpReplacementMarketEu; break;
				case AppConstants.MarketAT: filename = AssemblyResourceNames.PumpReplacementMarketAt; break;
				default: break;
			}

			string pumpReplacmentContent = _fileManager.GetResourceAsString(filename);
			var pumpReplacementRoot = JsonConvert.DeserializeObject<PumpReplacementRootLocalData>(pumpReplacmentContent);

			PopulatePumpReplacements(marketKey, pumpReplacementRoot.PumpReplacements);
			PopulateBiralProducts(marketKey, pumpReplacementRoot.Products);
		}


		public void PopulatePumpReplacement(string marketKey, PumpReplacementRootContract pumpReplacementRootContract)
		{
			var pumpReplacementRoot = Mapper.Map<PumpReplacementRootLocalData>(pumpReplacementRootContract);
			PopulatePumpReplacements(marketKey, pumpReplacementRoot.PumpReplacements);
			PopulateBiralProducts(marketKey, pumpReplacementRoot.Products);
		}

		public void PopulatePumpAdditionalInformation()
		{
			PopulateCategories();
			PopulateComments();
			PopulateConnectors();
		}

		public void DeleteAllPumpReplacement()
		{
			_pumpReplacementRepository.DeleteAll();
			_pumpReplacementProductRepository.DeleteAll();
			_biralProductRepository.DeleteAll();
		}

		private void PopulatePumpReplacements(string marketKey, List<PumpReplacementLocalData> pumpReplacements)
		{
			var pumpReplacementsDto = new List<PumpReplacementDataObject>();
			var pumpReplacementProductsDto = new List<PumpReplacementProductDataObject>();

			foreach (var pumpReplacement in pumpReplacements)
			{
				var pumpReplacementDto = Mapper.Map<PumpReplacementDataObject>(pumpReplacement);
				foreach (var pumpReplacementProduct in pumpReplacement.PumpReplacementProducts)
				{
					var pumpReplacementProductDto = Mapper.Map<PumpReplacementProductDataObject>(pumpReplacementProduct);
					pumpReplacementProductDto.ConnectorInformation = JsonConvert.SerializeObject(pumpReplacementProduct.ConnectorInformation);
					pumpReplacementProductDto.CommentCodes = string.Join(",", pumpReplacementProduct.CommentCodes);
					pumpReplacementProductDto.MarketKey = marketKey;
					pumpReplacementProductDto.Type = pumpReplacement.Type;

					pumpReplacementProductsDto.Add(pumpReplacementProductDto);
				}

				pumpReplacementDto.MarketKey = marketKey;
				pumpReplacementsDto.Add(pumpReplacementDto);
			}

			_pumpReplacementRepository.SaveList(pumpReplacementsDto);
			_pumpReplacementProductRepository.SaveList(pumpReplacementProductsDto);
		}

		private void PopulateBiralProducts(string marketKey, List<BiralProductLocalData> products)
		{
			var biralProductsDto = Mapper.Map<List<BiralProductDataObject>>(products);

			foreach (var biralProductDto in biralProductsDto)
			{
				biralProductDto.MarketKey = marketKey;
			}

			_biralProductRepository.SaveList(biralProductsDto);
		}

		private void PopulateCategories()
		{
			string filename = AssemblyResourceNames.PumpReplacementCategories;
			string categoriesContent = _fileManager.GetResourceAsString(filename);
			var categoryRoot = JsonConvert.DeserializeObject<ProductCategoryRootLocalData>(categoriesContent);

			var productCategoriesDto = new List<ProductCategoryDataObject>();

			foreach (var productCategory in categoryRoot.Categories)
			{
				var productCategoryDto = new ProductCategoryDataObject();
				productCategoryDto.CategoryId = productCategory.CategoryId;
				productCategoryDto.Text = productCategory.Name ?? string.Empty;
				productCategoryDto.EnTextValue = productCategory.L10N?.En?.Name ?? string.Empty;
				productCategoryDto.DeTextValue = productCategory.L10N?.De?.Name ?? string.Empty;
				productCategoryDto.ItTextValue = productCategory.L10N?.It?.Name ?? string.Empty;
				productCategoryDto.FrTextValue = productCategory.L10N?.Fr?.Name ?? string.Empty;
				productCategoryDto.NlTextValue = productCategory.L10N?.Nl?.Name ?? string.Empty;

				productCategoriesDto.Add(productCategoryDto);
			}

			_productCategoryRepository.DeleteAll();
			_productCategoryRepository.SaveList(productCategoriesDto);
		}

		public void PopulateCategories(List<ProductCategoryContract> categories)
		{
			var productCategoriesDto = new List<ProductCategoryDataObject>();

			foreach (var productCategory in categories)
			{
				var productCategoryDto = new ProductCategoryDataObject();
				productCategoryDto.CategoryId = productCategory.CategoryId;
				productCategoryDto.Text = productCategory.Name ?? string.Empty;
				productCategoryDto.EnTextValue = productCategory.L10N?.En?.Name ?? string.Empty;
				productCategoryDto.DeTextValue = productCategory.L10N?.De?.Name ?? string.Empty;
				productCategoryDto.ItTextValue = productCategory.L10N?.It?.Name ?? string.Empty;
				productCategoryDto.FrTextValue = productCategory.L10N?.Fr?.Name ?? string.Empty;
				productCategoryDto.NlTextValue = productCategory.L10N?.Nl?.Name ?? string.Empty;

				productCategoriesDto.Add(productCategoryDto);
			}

			_productCategoryRepository.DeleteAll();
			_productCategoryRepository.SaveList(productCategoriesDto);
		}

		private void PopulateComments()
		{
			string filename = AssemblyResourceNames.PumpReplacementComments;
			string commentsContent = _fileManager.GetResourceAsString(filename);
			var commentRoot = JsonConvert.DeserializeObject<ProductCommentRootLocalData>(commentsContent);

			var productCommentsDto = new List<ProductCommentDataObject>();

			foreach (var comment in commentRoot.Comments)
			{
				var productCommentDto = new ProductCommentDataObject();
				productCommentDto.CommentId = comment.CommentId;
				productCommentDto.Text = comment.Text ?? string.Empty;
				productCommentDto.EnTextValue = comment.L10N?.En?.Text ?? string.Empty;
				productCommentDto.DeTextValue = comment.L10N?.De?.Text ?? string.Empty;
				productCommentDto.ItTextValue = comment.L10N?.It?.Text ?? string.Empty;
				productCommentDto.FrTextValue = comment.L10N?.Fr?.Text ?? string.Empty;
				productCommentDto.NlTextValue = comment.L10N?.Nl?.Text ?? string.Empty;

				productCommentsDto.Add(productCommentDto);
			}

			_productCommentRepository.DeleteAll();
			_productCommentRepository.SaveList(productCommentsDto);
		}

		public void PopulateComments(List<ProductCommentContract> comments)
		{
			var productCommentsDto = new List<ProductCommentDataObject>();

			foreach (var comment in comments)
			{
				var productCommentDto = new ProductCommentDataObject();
				productCommentDto.CommentId = comment.CommentId;
				productCommentDto.Text = comment.Text ?? string.Empty;
				productCommentDto.EnTextValue = comment.L10N?.En?.Text ?? string.Empty;
				productCommentDto.DeTextValue = comment.L10N?.De?.Text ?? string.Empty;
				productCommentDto.ItTextValue = comment.L10N?.It?.Text ?? string.Empty;
				productCommentDto.FrTextValue = comment.L10N?.Fr?.Text ?? string.Empty;
				productCommentDto.NlTextValue = comment.L10N?.Nl?.Text ?? string.Empty;

				productCommentsDto.Add(productCommentDto);
			}

			_productCommentRepository.DeleteAll();
			_productCommentRepository.SaveList(productCommentsDto);
		}

		private void PopulateConnectors()
		{
			string filename = AssemblyResourceNames.PumpReplacementConnectors;
			string commentsContent = _fileManager.GetResourceAsString(filename);
			var co = JsonConvert.DeserializeObject<ProductConnectorRootLocalData>(commentsContent);

			var productConnectorsDto = new List<ProductConnectorDataObject>();

			foreach (var connector in co.Connectors)
			{
				var productConnectorDto = new ProductConnectorDataObject();
				productConnectorDto.ConnectorId = connector.ConnectorId;
				productConnectorDto.ProductNumber = connector.ProductNumber;
				productConnectorDto.Text = connector.Text ?? string.Empty;
				productConnectorDto.EnTextValue = connector.L10N?.En?.Text ?? string.Empty;
				productConnectorDto.DeTextValue = connector.L10N?.De?.Text ?? string.Empty;
				productConnectorDto.ItTextValue = connector.L10N?.It?.Text ?? string.Empty;
				productConnectorDto.FrTextValue = connector.L10N?.Fr?.Text ?? string.Empty;
				productConnectorDto.NlTextValue = connector.L10N?.Nl?.Text ?? string.Empty;

				productConnectorsDto.Add(productConnectorDto);
			}

			_productConnectorRepository.DeleteAll();
			_productConnectorRepository.SaveList(productConnectorsDto);
		}

		public void PopulateConnectors(List<ProductConnectorContract> connectors)
		{
			var productConnectorsDto = new List<ProductConnectorDataObject>();

			foreach (var connector in connectors)
			{
				var productConnectorDto = new ProductConnectorDataObject();
				productConnectorDto.ConnectorId = connector.ConnectorId;
				productConnectorDto.ProductNumber = connector.ProductNumber;
				productConnectorDto.Text = connector.Text ?? string.Empty;
				productConnectorDto.EnTextValue = connector.L10N?.En?.Text ?? string.Empty;
				productConnectorDto.DeTextValue = connector.L10N?.De?.Text ?? string.Empty;
				productConnectorDto.ItTextValue = connector.L10N?.It?.Text ?? string.Empty;
				productConnectorDto.FrTextValue = connector.L10N?.Fr?.Text ?? string.Empty;
				productConnectorDto.NlTextValue = connector.L10N?.Nl?.Text ?? string.Empty;

				productConnectorsDto.Add(productConnectorDto);
			}

			_productConnectorRepository.DeleteAll();
			_productConnectorRepository.SaveList(productConnectorsDto);
		}
	}
}
