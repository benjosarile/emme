﻿using EmmeApp.Entities;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Repositories.Abstractions;

namespace EmmeApp.Managers
{
    public class ProductCategoryManager : ManagerBase, IProductCategoryManager
	{
		private readonly IProductCategoryRepository _productCategoryRepository;

		public ProductCategoryManager(IServiceEntityMapper mapper, IProductCategoryRepository productCategoryRepository) : base(mapper)
		{
			this._productCategoryRepository = productCategoryRepository;
		}

		public ProductCategoryEntity GetCategory(long categoryId)
		{
			var productCategoryDto = this._productCategoryRepository.FirstOrDefault(x => x.CategoryId == categoryId);
			return this.Mapper.Map<ProductCategoryEntity>(productCategoryDto);
		}
	}
}
