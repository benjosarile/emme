﻿using EmmeApp.Entities;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Repositories.Abstractions;
using System.Collections.Generic;

namespace EmmeApp.Managers
{
    public class ProductConnectorManager : ManagerBase, IProductConnectorManager
	{
		private readonly IProductConnectorRepository _productConnectorRepository;

		public ProductConnectorManager(IServiceEntityMapper mapper, IProductConnectorRepository productConnectorRepository) : base(mapper)
		{
			this._productConnectorRepository = productConnectorRepository;
		}

		public List<ProductConnectorEntity> GetConnectors()
		{
			var productConnectorsDto = this._productConnectorRepository.GetAll();
			return this.Mapper.Map<List<ProductConnectorEntity>>(productConnectorsDto);
		}

		public ProductConnectorEntity GetConnector(string connectorId)
		{
			var ProductConnectorEntity = this._productConnectorRepository.FirstOrDefault(x => x.ConnectorId == connectorId);
			return this.Mapper.Map<ProductConnectorEntity>(ProductConnectorEntity);
		}
	}
}
