﻿using EmmeApp.Common.Constants;
using EmmeApp.Common.Exceptions;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.DataObjects;
using EmmeApp.Entities;
using EmmeApp.LocalData.Models;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Repositories.Abstractions;
using EmmeApp.WebServices.Abstractions;
using Newtonsoft.Json;
using Plugin.Connectivity.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmmeApp.Managers
{
    public class SortimentManager : ManagerBase, ISortimentManager
	{
		private readonly IAssemblyResourceManager _assemblyResourceManager;
		private readonly ISortimentRepository _sortimentRepository;
		private readonly ISortimentWebService _sortimentWebService;
		private readonly ISettingsService _settingsService;

		public SortimentManager(IConnectivity connectivity,
			IServiceEntityMapper mapper,
			IAssemblyResourceManager assemblyResourceManager,
			ISortimentRepository sortimentRepository,
			ISortimentWebService sortimentWebService,
			ISettingsService settingsService) : base(connectivity, mapper)
		{
			_assemblyResourceManager = assemblyResourceManager;
			_sortimentRepository = sortimentRepository;
			_sortimentWebService = sortimentWebService;
			_settingsService = settingsService;
		}

		public SortimentEntity GetSortimentFromAssemblyResource()
		{
			string fileContent = _assemblyResourceManager.GetResourceAsString(AssemblyResourceNames.PumpReplacementSortiment);
			var sortimentLocalData = JsonConvert.DeserializeObject<SortimentLocalData>(fileContent);
			var sortimentEntity = Mapper.Map<SortimentEntity>(sortimentLocalData);
			return sortimentEntity;
		}

		public int GetSortimentVersionFromAssemblyResource()
		{
			string fileContent = _assemblyResourceManager.GetResourceAsString(AssemblyResourceNames.PumpReplacementSortiment);
			var sortiment = JsonConvert.DeserializeObject<SortimentLocalData>(fileContent);
			return sortiment.Version;
		}

		public int GetSortimentVersionFromDB()
		{
			var sortimentDto = _sortimentRepository.FirstOrDefault();
			return sortimentDto != null ? sortimentDto.Version : -1;
		}

		public void SaveSortimentsToDB()
		{
			string fileContent = _assemblyResourceManager.GetResourceAsString(AssemblyResourceNames.PumpReplacementSortiment);
			var sortiment = JsonConvert.DeserializeObject<SortimentLocalData>(fileContent);

			var markets = Mapper.Map<List<SortimentMarketEntity>>(sortiment.Markets);
			string marketContent = JsonConvert.SerializeObject(markets);

			var sortimentDto = new SortimentDataObject();
			sortimentDto.Version = sortiment.Version;
			sortimentDto.MarketContent = marketContent;

			_sortimentRepository.DeleteAll();
			_sortimentRepository.Save(sortimentDto);
		}

		
		public SortimentEntity GetSortimentsFromDB()
		{
			var sortimentDto = _sortimentRepository.FirstOrDefault();

			if (sortimentDto != null)
			{
				var sortimentEntity = new SortimentEntity();
				sortimentEntity.Version = sortimentDto.Version;
				sortimentEntity.Markets = JsonConvert.DeserializeObject<List<SortimentMarketEntity>>(sortimentDto.MarketContent);

				return sortimentEntity;
			}

			return null;
		}

		public SortimentMarketEntity GetSortimentMarketsFromDB(string marketKey)
		{
			var sortiment = GetSortimentsFromDB();
			if (sortiment != null)
			{
				return sortiment.Markets.FirstOrDefault(x => x.Key == marketKey);
			}

			return null;
		}

		public DateTimeOffset? GetPumpReplacementLastModified()
		{
			string settingLastModified = _settingsService.GetValue(SettingsConstant.PumpReplacementLastModified);
			DateTimeOffset result;
			if (DateTimeOffset.TryParse(settingLastModified, out result))
			{
				return result;
			}

			return null;
		}

		public void SetPumpReplacementLastModified(DateTimeOffset lastModified)
		{
			_settingsService.SetValue(SettingsConstant.PumpReplacementLastModified, lastModified.ToString());
		}

	
		public async Task<bool> HasUpdate(DateTimeOffset? lastModified)
		{
			if (!Connectivity.IsConnected)
			{
				throw new NoInternetConnectivityException("No Internet Connectivity");
			}

			var sortimentContract = await _sortimentWebService.GetSortiment();

			if (sortimentContract != null)
			{
				if (lastModified.HasValue)
				{
					if (sortimentContract.LastModified > lastModified)
					{
						return true;
					}
				}
				else
				{
					return true;
				}
			}

			return false;
		}
	}
}
