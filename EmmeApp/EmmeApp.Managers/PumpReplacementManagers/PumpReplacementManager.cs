﻿using EmmeApp.Entities;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Repositories.Abstractions;
using System.Collections.Generic;

namespace EmmeApp.Managers
{
    public class PumpReplacementManager : ManagerBase, IPumpReplacementManager
	{
		private readonly IPumpReplacementRepository _pumpReplacementRepository;
		private readonly IMarketManager _marketManager; 
		public PumpReplacementManager(IServiceEntityMapper mapper,
			IPumpReplacementRepository pumpReplacementRepository,
			IMarketManager marketManager)
			: base(mapper)
		{
			this._pumpReplacementRepository = pumpReplacementRepository;
			this._marketManager = marketManager;
		}

		public PumpReplacementEntity GetOldPumpDetails(string pumpType)
		{
			var marketKey = _marketManager.GetMarketLanguage().Item1;

			var pumpReplacementDto = this._pumpReplacementRepository.FirstOrDefault(x => x.MarketKey == marketKey && x.Type == pumpType);

			var pumpReplacementEntity = this.Mapper.Map<PumpReplacementEntity>(pumpReplacementDto);

			return pumpReplacementEntity;
		}

		public List<PumpReplacementEntity> GetOldPumps()
        {
			var marketKey = _marketManager.GetMarketLanguage().Item1;

			var pumpReplacementDto = this._pumpReplacementRepository.Where(x => x.MarketKey == marketKey);
			var pumpReplacements = this.Mapper.Map<List<PumpReplacementEntity>>(pumpReplacementDto);
			return pumpReplacements;
		}
	}
}
