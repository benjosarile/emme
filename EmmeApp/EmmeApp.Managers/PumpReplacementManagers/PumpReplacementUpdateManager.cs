﻿using EmmeApp.Common.Constants;
using EmmeApp.DataContracts.Response;
using EmmeApp.Managers.Abstractions;
using EmmeApp.WebServices.Abstractions;
using Plugin.Connectivity.Abstractions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace EmmeApp.Managers
{
    public class PumpReplacementUpdateManager : ManagerBase, IPumpReplacementUpdateManager
	{
		private readonly ISortimentWebService _sortimentWebService;
		private readonly IPumpReplacementWebService _pumpReplacementWebService;
		private readonly IMasterPumpReplacementManager _masterPumpReplacementManager;

		public PumpReplacementUpdateManager(IConnectivity connectivity, IServiceEntityMapper mapper, ISortimentWebService sortimentWebService, IPumpReplacementWebService pumpReplacementWebService, IMasterPumpReplacementManager masterPumpReplacementManager) : base(connectivity, mapper)
		{
			_sortimentWebService = sortimentWebService;
			_pumpReplacementWebService = pumpReplacementWebService;
			_masterPumpReplacementManager = masterPumpReplacementManager;
		}

		public async Task<bool> Update()
		{
			bool hasError = false;

			var sortimentContract = await _sortimentWebService.GetSortiment();
			var exchanges = new List<Tuple<string, PumpReplacementRootContract>>();
			if (sortimentContract != null && sortimentContract.Markets != null)
			{
				foreach (var market in sortimentContract.Markets)
				{
					market.Key = market.Key.ToUpper();

					Debug.WriteLine($"Market Key:{market.Key}");
					PumpReplacementRootContract exchangeContract = null;

					if (Connectivity.IsConnected)
					{
						exchangeContract = await _pumpReplacementWebService.GetExchange(market.Key);
					}

					if (exchangeContract != null)
					{
						exchanges.Add(new Tuple<string, PumpReplacementRootContract>(market.Key, exchangeContract));
					}
					else
					{
						hasError = true;
						break;
					}
				}

				if (!hasError)
				{
					_masterPumpReplacementManager.DeleteAllPumpReplacement();

					foreach (var exchange in exchanges)
					{
						_masterPumpReplacementManager.PopulatePumpReplacement(exchange.Item1, exchange.Item2);
					}

					PopulateCategories(exchanges);
					PopulateComments(exchanges);
					PopulateConnectors(exchanges);

					return true;
				}
			}

			return false;
		}

		private void PopulateCategories(List<Tuple<string, PumpReplacementRootContract>> exchanges)
		{
			try
			{
				var categories = new List<ProductCategoryContract>();
				foreach (var exchange in exchanges)
				{
					foreach (var category in exchange.Item2.Categories)
					{
						var currentCategory = categories.FirstOrDefault(x => x.CategoryId == category.CategoryId);

						if (currentCategory == null)
						{
							categories.Add(category);
						}
						else
						{
							if (currentCategory.L10N == null)
							{
								currentCategory.L10N = new LocalizationContract();
							}

							if (category.L10N != null)
							{
								currentCategory.L10N.En = category.L10N.En ?? currentCategory.L10N.En;
								currentCategory.L10N.De = category.L10N.De ?? currentCategory.L10N.De;
								currentCategory.L10N.Fr = category.L10N.Fr ?? currentCategory.L10N.Fr;
								currentCategory.L10N.It = category.L10N.It ?? currentCategory.L10N.It;
								currentCategory.L10N.Nl = category.L10N.Nl ?? currentCategory.L10N.Nl;
							}
							else
							{
								switch (exchange.Item1)
								{
									case AppConstants.MarketDE: currentCategory.L10N.De = new LocalizationDetailContract() { Name = category.Name }; break;
									case AppConstants.MarketCH: currentCategory.L10N.De = new LocalizationDetailContract() { Name = category.Name }; break;
									case AppConstants.MarketAT: currentCategory.L10N.De = new LocalizationDetailContract() { Name = category.Name }; break;
									case AppConstants.MarketEU: currentCategory.L10N.En = new LocalizationDetailContract() { Name = category.Name }; break;
									case AppConstants.MarketIT: currentCategory.L10N.It = new LocalizationDetailContract() { Name = category.Name }; break;
									case AppConstants.MarketNL: currentCategory.L10N.Nl = new LocalizationDetailContract() { Name = category.Name }; break;
								}
							}
						}
					}
				}

				_masterPumpReplacementManager.PopulateCategories(categories);
			}
			catch(Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}
		}

		private void PopulateComments(List<Tuple<string, PumpReplacementRootContract>> exchanges)
		{
			try
			{
				var comments = new List<ProductCommentContract>();
				foreach (var exchange in exchanges)
				{
					foreach (var comment in exchange.Item2.Comments)
					{
						var currentComment = comments.FirstOrDefault(x => x.CommentId == comment.CommentId);

						if (currentComment == null)
						{
							comments.Add(comment);
						}
						else
						{
							if (currentComment.L10N == null)
							{
								currentComment.L10N = new LocalizationContract();
							}

							if (comment.L10N != null)
							{
								currentComment.L10N.En = comment.L10N.En ?? currentComment.L10N.En;
								currentComment.L10N.De = comment.L10N.De ?? currentComment.L10N.De;
								currentComment.L10N.Fr = comment.L10N.Fr ?? currentComment.L10N.Fr;
								currentComment.L10N.It = comment.L10N.It ?? currentComment.L10N.It;
								currentComment.L10N.Nl = comment.L10N.Nl ?? currentComment.L10N.Nl;
							}
							else
							{
								switch (exchange.Item1)
								{
									case AppConstants.MarketDE: currentComment.L10N.De = new LocalizationDetailContract() { Text = comment.Text }; break;
									case AppConstants.MarketCH: currentComment.L10N.De = new LocalizationDetailContract() { Text = comment.Text }; break;
									case AppConstants.MarketAT: currentComment.L10N.De = new LocalizationDetailContract() { Text = comment.Text }; break;
									case AppConstants.MarketEU: currentComment.L10N.En = new LocalizationDetailContract() { Text = comment.Text }; break;
									case AppConstants.MarketIT: currentComment.L10N.It = new LocalizationDetailContract() { Text = comment.Text }; break;
									case AppConstants.MarketNL: currentComment.L10N.Nl = new LocalizationDetailContract() { Text = comment.Text }; break;
								}
							}
						}
					}
				}

				_masterPumpReplacementManager.PopulateComments(comments);
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}
		}

		private void PopulateConnectors(List<Tuple<string, PumpReplacementRootContract>> exchanges)
		{
			try
			{
				var connectors = new List<ProductConnectorContract>();
				foreach (var exchange in exchanges)
				{
					foreach (var connector in exchange.Item2.Connectors)
					{
						var currentConnectors = connectors.FirstOrDefault(x => x.ConnectorId == connector.ConnectorId);

						if (currentConnectors == null)
						{
							connectors.Add(connector);
						}
						else
						{
							if (currentConnectors.L10N == null)
							{
								currentConnectors.L10N = new LocalizationContract();
							}

							if (connector.L10N != null)
							{
								currentConnectors.L10N.En = connector.L10N.En ?? currentConnectors.L10N.En;
								currentConnectors.L10N.De = connector.L10N.De ?? currentConnectors.L10N.De;
								currentConnectors.L10N.Fr = connector.L10N.Fr ?? currentConnectors.L10N.Fr;
								currentConnectors.L10N.It = connector.L10N.It ?? currentConnectors.L10N.It;
								currentConnectors.L10N.Nl = connector.L10N.Nl ?? currentConnectors.L10N.Nl;
							}
							else
							{
								switch (exchange.Item1)
								{
									case AppConstants.MarketDE: currentConnectors.L10N.De = new LocalizationDetailContract() { Text = connector.Text }; break;
									case AppConstants.MarketCH: currentConnectors.L10N.De = new LocalizationDetailContract() { Text = connector.Text }; break;
									case AppConstants.MarketAT: currentConnectors.L10N.De = new LocalizationDetailContract() { Text = connector.Text }; break;
									case AppConstants.MarketEU: currentConnectors.L10N.En = new LocalizationDetailContract() { Text = connector.Text }; break;
									case AppConstants.MarketIT: currentConnectors.L10N.It = new LocalizationDetailContract() { Text = connector.Text }; break;
									case AppConstants.MarketNL: currentConnectors.L10N.Nl = new LocalizationDetailContract() { Text = connector.Text }; break;
								}
							}
						}
					}
				}

				_masterPumpReplacementManager.PopulateConnectors(connectors);
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}
		}
	}
}
