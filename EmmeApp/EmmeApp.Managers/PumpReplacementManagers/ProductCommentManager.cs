﻿using EmmeApp.Entities;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Repositories.Abstractions;
using System.Collections.Generic;

namespace EmmeApp.Managers
{
    public class ProductCommentManager : ManagerBase, IProductCommentManager
	{
		private readonly IProductCommentRepository _productCommentRepository;

		public ProductCommentManager(IServiceEntityMapper mapper, IProductCommentRepository productCommentRepository) : base(mapper)
		{
			this._productCommentRepository = productCommentRepository;
		}

		public List<ProductCommentEntity> GetComments()
		{
			var productCommentsDto = this._productCommentRepository.GetAll();
			return this.Mapper.Map<List<ProductCommentEntity>>(productCommentsDto);
		}

		public ProductCommentEntity GetComment(long commentId)
		{
			var productCommentDto = this._productCommentRepository.FirstOrDefault(x => x.CommentId == commentId);
			return this.Mapper.Map<ProductCommentEntity>(productCommentDto);
		}
	}
}
