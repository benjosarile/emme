﻿using EmmeApp.Common.Constants;
using EmmeApp.Entities;
using EmmeApp.LocalData.Models;
using EmmeApp.Managers.Abstractions;
using EmmeApp.Repositories.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EmmeApp.Managers
{
    public class BiralProductManager : ManagerBase, IBiralProductManager
	{
		private readonly IProductCategoryManager _productCategoryManager;
		private readonly IProductCommentManager _productCommentManager;
		private readonly IProductConnectorManager _productConnectorManager;
		private readonly IBiralProductRepository _biralProductRepository;
		private readonly IMarketManager _marketManager;

		public BiralProductManager(IServiceEntityMapper mapper,
			IProductCategoryManager productCategoryManager, 
			IProductCommentManager productCommentManager, 
			IProductConnectorManager productConnectorManager, 
			IBiralProductRepository biralProductRepository,
			IMarketManager marketManager) : base(mapper)
		{
			_productCategoryManager = productCategoryManager;
			_productCommentManager = productCommentManager;
			_productConnectorManager = productConnectorManager;
			_biralProductRepository = biralProductRepository;
			_marketManager = marketManager;
		}

		public BiralProductEntity GetBiralProductDetail(PumpReplacementProductEntity pumpReplacementProductEntity)
		{
			var language = _marketManager.GetMarketLanguage().Item2;

			var biralProductDto = _biralProductRepository.FirstOrDefault(x => x.MarketKey == pumpReplacementProductEntity.MarketKey && x.ProductId == pumpReplacementProductEntity.ProductId);


			if (biralProductDto != null)
			{
				var biralProductEntity = Mapper.Map<BiralProductEntity>(biralProductDto);

				var category = _productCategoryManager.GetCategory(biralProductEntity.Category);

				if(category != null)
				{
					switch (language)
					{
						case AppConstants.LanguageCodeEN: biralProductEntity.CategoryValue = category.EnTextValue; break;
						case AppConstants.LanguageCodeFR: biralProductEntity.CategoryValue = category.FrTextValue; break;
						case AppConstants.LanguageCodeDE: biralProductEntity.CategoryValue = category.DeTextValue; break;
						case AppConstants.LanguageCodeNL: biralProductEntity.CategoryValue = category.NlTextValue; break;
						case AppConstants.LanguageCodeIT: biralProductEntity.CategoryValue = category.ItTextValue; break;
					}
				}

				biralProductEntity.CategoryValue = string.IsNullOrEmpty(biralProductEntity.CategoryValue) ? category.Text : biralProductEntity.CategoryValue;
				biralProductEntity.Remarks = GetProductComments(pumpReplacementProductEntity.CommentCodes);

				var adapterInfos = GetAdapterInfos(pumpReplacementProductEntity.ConnectorInformation);
				biralProductEntity.AdapterInfos = adapterInfos;

				return biralProductEntity;
			}

			return null;
		}

		private string GetProductComments(string commentCodes)
		{
			var language = _marketManager.GetMarketLanguage().Item2;

			List<string> commentCodesValue = new List<string>();
			commentCodes = commentCodes ?? string.Empty;


			if (commentCodes.Any())
			{
				List<ProductCommentEntity> productComments = _productCommentManager.GetComments();
				List<long> commentCodeList = commentCodes.Split(',').Select(x => Convert.ToInt64(x)).ToList();

				foreach (long commentCode in commentCodeList)
				{
					var productComment = productComments.FirstOrDefault(x => x.CommentId == commentCode);
					if (productComment != null)
					{
						string value = string.Empty;
						switch (language)
						{
							case AppConstants.LanguageCodeEN: value = productComment.EnTextValue; break;
							case AppConstants.LanguageCodeFR: value = productComment.FrTextValue; break;
							case AppConstants.LanguageCodeDE: value = productComment.DeTextValue; break;
							case AppConstants.LanguageCodeNL: value = productComment.NlTextValue; break;
							case AppConstants.LanguageCodeIT: value = productComment.ItTextValue; break;
						}

						if (string.IsNullOrEmpty(value))
						{
							value = productComment.Text;
						}

						commentCodesValue.Add(value);
					}
				}
			}

			return string.Join($",{Environment.NewLine}", (commentCodesValue as IEnumerable<string>).Reverse());
		}

		private List<AdapterInfo> GetAdapterInfos(string connectionInformation)
		{
			var language = _marketManager.GetMarketLanguage().Item2;

			var list = new List<AdapterInfo>();
			var productConnectors = JsonConvert.DeserializeObject<List<PumpReplacementConnectorInformationLocalData>>(connectionInformation);

			if (productConnectors.Any())
			{
				var connectorList = _productConnectorManager.GetConnectors();

				foreach (var productConnector in productConnectors)
				{
					var connector = connectorList.FirstOrDefault(x => x.ConnectorId == productConnector.ConnectorCode);
					if (connector != null)
					{
						string connectorCode = string.Empty;
						switch (language)
						{
							case AppConstants.LanguageCodeEN: connectorCode = connector.EnTextValue; break;
							case AppConstants.LanguageCodeFR: connectorCode = connector.FrTextValue; break;
							case AppConstants.LanguageCodeDE: connectorCode = connector.DeTextValue; break;
							case AppConstants.LanguageCodeNL: connectorCode = connector.NlTextValue; break;
							case AppConstants.LanguageCodeIT: connectorCode = connector.ItTextValue; break;
						}

						if (string.IsNullOrEmpty(connectorCode))
						{
							connectorCode = connector.Text;
						}

						list.Add(
							new AdapterInfo
							{
								Code = connectorCode,
								ItemNumber = connector.ProductNumber,
								NumberOfPieces = productConnector.NumberOfPieces
							});
					}
				}
			}

			return list;
		}
	}
}
