﻿using EmmeApp.Common.Attributes;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.Configuration;
using EmmeApp.Common.PageModels.External;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Entities.External;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Humanizer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace EmmeApp.Managers
{
    public class CockpitPumpsModulAManager : ManagerBase, ICockpitPumpsModulAManager
	{
		private readonly IUnixTimestampService _unixTimestampService;

		public CockpitPumpsModulAManager(IServiceEntityMapper mapper, IUnixTimestampService unixTimestampService) : base(mapper)
		{
			_unixTimestampService = unixTimestampService;
		}

		public StringBuilder GenerateEmailContent(CockpitPumpsModulARootModel rootModel, string newLine = "\r\n")
		{
			string dateFormat = "yyyyMMdd HH:mm";

			string additionalModuleText = rootModel.AdditionalModule.Humanize();
			var additionalModuleVal = Convert.ToInt32(rootModel.AdditionalModule);
			if (!Enum.IsDefined(typeof(PumpAdditionalModuleValue), additionalModuleVal))
			{
				additionalModuleText = AppResources.Pump_AdditionalModule_UnknownModule;
			}

			var sb = new StringBuilder();

			sb.Append($"{AppResources.CockpitPumps_Section_Title_Info}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_PumpType}: {rootModel.PumpType}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_PumpVersionType}: {rootModel.PumpVersionType}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_SWVersion}: {rootModel.SwVersion}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_SerialNumber}: {rootModel.SerialNumber}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_ManufacturingDate}: {rootModel.ManufactureDate}{newLine}");
			var time = _unixTimestampService.ConvertToLocalTime(rootModel.Time);
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_Time}: {time.ToString(dateFormat, CultureInfo.InvariantCulture)}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitPumps_Section_Title_Status}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_FlowRate}: {rootModel.FlowRate} {AppResources.CockpitPumps_UnitOfMeasure_FlowRate}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_FlowHeight}: {rootModel.FlowHeight} {AppResources.CockpitPumps_UnitOfMeasure_Height}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_MediaTemperature}: {rootModel.MediaTemperature} {AppResources.Label_UnitOfMeasurement_Celsius}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_PowerOutput}: {rootModel.PowerOutput} {AppResources.CockpitPumps_UnitOfMeasure_PowerOutput}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_Speed}: {rootModel.Speed} {AppResources.CockpitPumps_UnitOfMeasure_Speed}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_OperatingHours}: {rootModel.OperatingHours} {AppResources.CockpitPumps_UnitOfMeasure_Hours}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_ElectricalEnergy}: {rootModel.ElectricalEnergy} {AppResources.CockpitPumps_UnitOfMeasure_Energy}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_AdditionalModule}: {additionalModuleText}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_SignalOrigin}: {rootModel.SignalOrigin.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_PowerLimit}: {rootModel.PowerLimit.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_RelayFunction}: {rootModel.RelayFunction.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_DigitalIn1011}: {rootModel.DigIn1011.Humanize()}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitPumps_Section_Title_Settings}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_TypeOfOperation}: {rootModel.TypeOfOperation.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_ControlType}: {rootModel.ControlType.Humanize()} {newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_NominalValue}: {rootModel.NominalValue} {AppResources.CockpitPumps_UnitOfMeasure_NominalValue}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_KeyLock}: {rootModel.KeyLock.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_PumpNumber}: {rootModel.PumpNumber}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_Switch1}: {rootModel.Switch1.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_Switch2}: {rootModel.Switch2.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_Switch3}: {rootModel.Switch3.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_PowerLimitMax}: {rootModel.PowerLimitMax} {AppResources.CockpitPumps_UnitOfMeasure_FlowRate}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitPumps_Section_Title_Alarms}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_CurrentAlarm}: {rootModel.CurrentAlarm.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_CurrentWarning}: {rootModel.CurrentWarning.Humanize()}{newLine}");
			return sb;

		}

		public Tuple<CockpitPumpsModulARootModel, List<ParameterPumpModulACharacteristicExternalEntity>> MapParametersToModel(CockpitPumpsModulARootModel rootModel, List<ParameterTExternalEntity> parameters)
		{
			var parameterCharacteristicList = new List<ParameterPumpModulACharacteristicExternalEntity>();

			parameters = parameters ?? new List<ParameterTExternalEntity>();

			foreach (var parameter in parameters)
			{
				var objtype = rootModel.GetType();

				if (parameter.IdCase == ExternalIdOneofCaseValue.ModulA && parameter.Value != null)
				{
					if (parameter.Scaling != null && parameter.Scaling.Step >= 0)
					{
						var characteristic = parameterCharacteristicList.FirstOrDefault(x => x.ModulA == parameter.ModulA);

						if (characteristic == null)
						{
							parameterCharacteristicList.Add(new ParameterPumpModulACharacteristicExternalEntity()
							{
								ModulA = parameter.ModulA,
								Scaling = parameter.Scaling,
								Unit = parameter.Unit
							});
						}
					}

					var property = objtype.GetProperties()
						.FirstOrDefault(p =>
											p.CustomAttributes
											.Any(catt =>
											catt.AttributeType == typeof(PumpModulAParamAttribute) &&
											(ExternalPumpModulAParameterIdValue)catt.ConstructorArguments?[0].Value == parameter.ModulA));

					if (property != null && parameter != null)
					{
						var catt = (PumpModulAParamAttribute)property.GetCustomAttributes(typeof(PumpModulAParamAttribute), true).FirstOrDefault();
						if (parameter.ModulA == catt.ModulAParameterId)
						{
							var parameterValue = Mapper.Map<ValueTExternalModel>(parameter.Value);
							var parameterScaling = Mapper.Map<ScalingTExternalModel>(parameter.Scaling);

							var value = catt.GetValue(parameter.IdCase, parameterValue, parameterScaling);
							if (value != null)
							{
								property.SetValue(rootModel, value);
							}
						}
					}
				}
			}


			return new Tuple<CockpitPumpsModulARootModel, List<ParameterPumpModulACharacteristicExternalEntity>>(rootModel, parameterCharacteristicList);
		}

		public PumpConfigurationModulAWizardModel MapToConfigurationModel(CockpitPumpsModulARootModel rootModel)
		{
			return Mapper.Map<PumpConfigurationModulAWizardModel>(rootModel);
		}
	}
}
