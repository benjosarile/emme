﻿using EmmeApp.Common.Attributes;
using EmmeApp.Common.Enum;
using EmmeApp.Common.PageModels.Cockpit;
using EmmeApp.Common.PageModels.Configuration;
using EmmeApp.Common.PageModels.External;
using EmmeApp.Common.Utilities.Abstractions;
using EmmeApp.Entities.External;
using EmmeApp.Localization.Resources;
using EmmeApp.Managers.Abstractions;
using Humanizer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

namespace EmmeApp.Managers
{
    public class CockpitPumpsVariAEManager : ManagerBase, ICockpitPumpsVariAEManager
	{
		private readonly IUnixTimestampService _unixTimestampService;

		public CockpitPumpsVariAEManager(IServiceEntityMapper mapper, IUnixTimestampService unixTimestampService) : base(mapper)
		{
			_unixTimestampService = unixTimestampService;
		}

		public StringBuilder GenerateEmailContent(CockpitPumpsVariAERootModel rootModel, string newLine = "\r\n")
		{
			string dateFormat = "yyyyMMdd HH:mm";

			string additionalModuleText = rootModel.AdditionalModule.Humanize();
			var additionalModuleVal = Convert.ToInt32(rootModel.AdditionalModule);
			if (!Enum.IsDefined(typeof(PumpAdditionalModuleValue), additionalModuleVal))
			{
				additionalModuleText = AppResources.Pump_AdditionalModule_UnknownModule;
			}

			var sb = new StringBuilder();

			sb.Append($"{AppResources.CockpitPumps_Section_Title_Info}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_PumpType}: {rootModel.PumpType}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_SWVersion}: {rootModel.SwVersion}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_SerialNumber}: {rootModel.SerialNumber}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_ManufacturingDate}: {rootModel.ManufactureDate}{newLine}");
			var time = _unixTimestampService.ConvertToLocalTime(rootModel.Time);
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_Time}: {time.ToString(dateFormat, CultureInfo.InvariantCulture)}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitPumps_Section_Title_Status}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_FlowRate}: {rootModel.FlowRate} {AppResources.CockpitPumps_UnitOfMeasure_FlowRate}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_FlowHeight}: {rootModel.FlowHeight} {AppResources.CockpitPumps_UnitOfMeasure_Height}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_PowerOutput}: {rootModel.PowerOutput} {AppResources.CockpitPumps_UnitOfMeasure_PowerOutput}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_Speed}: {rootModel.Speed} {AppResources.CockpitPumps_UnitOfMeasure_Speed}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_OperatingHours}: {rootModel.OperatingHours} {AppResources.CockpitPumps_UnitOfMeasure_Hours}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_ElectricalEnergy}: {rootModel.ElectricalEnergy} {AppResources.CockpitPumps_UnitOfMeasure_Energy}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_AdditionalModule}: {additionalModuleText}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_SignalOrigin}: {rootModel.SignalOrigin.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpVariAEStatusPage_Label_Parameter_Relay1}: {rootModel.Relay1.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpVariAEStatusPage_Label_Parameter_Relay2}: {rootModel.Relay2.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_DigitalIn1011}: {rootModel.DigIn1011.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpVariAEStatusPage_Label_Parameter_DigitalIn1015}: {rootModel.DigIn1015.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpVariAEStatusPage_Label_Parameter_DigitalIn1016}: {rootModel.DigIn1016.Humanize()}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitPumps_Section_Title_Settings}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_TypeOfOperation}: {rootModel.TypeOfOperation.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_ControlType}: {rootModel.ControlType.Humanize()} {newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_NominalValue}: {rootModel.NominalValue} {AppResources.CockpitPumps_UnitOfMeasure_NominalValue}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_KeyLock}: {rootModel.KeyLock.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_PumpNumber}: {rootModel.PumpNumber}{newLine}");
			
			//TODO 2257
			sb.Append($"{AppResources.CockpitPumpVariAEStatusPage_Label_Parameter_Switch1}: {rootModel.Switch1.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpVariAEStatusPage_Label_Parameter_Switch2}: {rootModel.Switch2.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpVariAEStatusPage_Label_Parameter_Switch3}: {rootModel.Switch3.Humanize()}{newLine}");
			
			sb.Append($"{AppResources.CockpitPumpVariAEStatusPage_Label_Parameter_Switch4}: {rootModel.Switch4.Humanize()}{newLine}");
			sb.Append(newLine);
			sb.Append($"{AppResources.CockpitPumps_Section_Title_Alarms}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_CurrentAlarm}: {rootModel.CurrentAlarm.Humanize()}{newLine}");
			sb.Append($"{AppResources.CockpitPumpModulAStatusPage_Label_Parameter_CurrentWarning}: {rootModel.CurrentWarning.Humanize()}{newLine}");

			return sb;
		}

		public Tuple<CockpitPumpsVariAERootModel, List<ParameterPumpVariACharacteristicExternalEntity>> MapParametersToModel(CockpitPumpsVariAERootModel rootModel, List<ParameterTExternalEntity> parameters)
		{
			var parameterCharacteristicList = new List<ParameterPumpVariACharacteristicExternalEntity>();

			parameters = parameters ?? new List<ParameterTExternalEntity>();
			
			foreach (var parameter in parameters)
			{
				var objtype = rootModel.GetType();

				if (parameter.IdCase == ExternalIdOneofCaseValue.VariA && parameter.Value != null)
				{
					if (parameter.Scaling != null && parameter.Scaling.Step >= 0)
					{
						var characteristic = parameterCharacteristicList.FirstOrDefault(x => x.VariA == parameter.VariA);

						if (characteristic == null)
						{
							Debug.WriteLine($"{parameter.VariA}: {parameter.Scaling.Min} {parameter.Scaling.Max} {parameter.Scaling.Step}");
							parameterCharacteristicList.Add(new ParameterPumpVariACharacteristicExternalEntity()
							{
								VariA = parameter.VariA,
								Scaling = parameter.Scaling,
								Unit = parameter.Unit
							});
						}
					}

					var property = objtype.GetProperties()
						.FirstOrDefault(p =>
											p.CustomAttributes
											.Any(catt =>
											catt.AttributeType == typeof(PumpVariAParamAttribute) &&
											(ExternalPumpVariAParameterIdValue)catt.ConstructorArguments?[0].Value == parameter.VariA));

					if (property != null && parameter != null)
					{
						var catt = (PumpVariAParamAttribute)property.GetCustomAttributes(typeof(PumpVariAParamAttribute), true).FirstOrDefault();
						if (parameter.VariA == catt.VariaAParameterId)
						{
							var parameterValue = Mapper.Map<ValueTExternalModel>(parameter.Value);
							var parameterScaling = Mapper.Map<ScalingTExternalModel>(parameter.Scaling);

							var value = catt.GetValue(parameter.IdCase, parameterValue, parameterScaling);
							if (value != null)
							{
								property.SetValue(rootModel, value);
							}
						}
					}
				}
			}

			return new Tuple<CockpitPumpsVariAERootModel, List<ParameterPumpVariACharacteristicExternalEntity>>(rootModel, parameterCharacteristicList);
		}

		public PumpConfigurationVariAWizardModel MapToConfigurationModel(CockpitPumpsVariAERootModel rootModel)
		{
			return Mapper.Map<PumpConfigurationVariAWizardModel>(rootModel);
		}
	}
}
