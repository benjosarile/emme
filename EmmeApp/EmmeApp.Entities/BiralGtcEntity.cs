﻿namespace EmmeApp.Entities
{
    public class BiralGtcEntity
	{
		public string Country { get; set; }
		public string CountryCode { get; set; }
		public string Language { get; set; }
		public string Link { get; set; }
	}
}
