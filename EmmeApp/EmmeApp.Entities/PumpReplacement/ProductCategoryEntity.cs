﻿namespace EmmeApp.Entities
{
    public class ProductCategoryEntity
	{
		public long CategoryId { get; set; }

		public string Text { get; set; }

		public string FrTextValue { get; set; }
		public string ItTextValue { get; set; }
		public string NlTextValue { get; set; }
		public string DeTextValue { get; set; }
		public string EnTextValue { get; set; }
	}
}
