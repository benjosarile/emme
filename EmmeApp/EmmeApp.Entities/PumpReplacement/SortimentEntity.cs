﻿using System.Collections.Generic;

namespace EmmeApp.Entities
{
    public class SortimentEntity
	{
		public int Version { get; set; }
		public List<SortimentMarketEntity> Markets { get; set; }
	}
}
