﻿using System;

namespace EmmeApp.Entities
{
    public class SortimentMarketEntity
	{
		public string Key { get; set; }
		public string Name { get; set; }
		public MarketLinkEntity MarketLink { get; set; }
	}

	public class MarketLinkEntity
	{
		public Uri De { get; set; }
		public Uri Fr { get; set; }
		public Uri It { get; set; }
		public Uri Nl { get; set; }
		public Uri En { get; set; }
	}
}
