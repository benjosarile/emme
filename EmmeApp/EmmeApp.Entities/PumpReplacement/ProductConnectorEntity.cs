﻿namespace EmmeApp.Entities
{
    public class ProductConnectorEntity
	{
		public string ConnectorId { get; set; }
		public uint ProductNumber { get; set; }
		public string Text { get; set; }

		public string FrTextValue { get; set; }
		public string ItTextValue { get; set; }
		public string NlTextValue { get; set; }
		public string DeTextValue { get; set; }
		public string EnTextValue { get; set; }
	}
}
