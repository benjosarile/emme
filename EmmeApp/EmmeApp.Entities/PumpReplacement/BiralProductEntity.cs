﻿using System.Collections.Generic;

namespace EmmeApp.Entities
{
    public class BiralProductEntity
	{
		public string MarketKey { get; set; }

		public long ProductId { get; set; }
		public string Type { get; set; }
		public string Diameter { get; set; }
		public string Pressure { get; set; }
		public string Length { get; set; }
		public string Engine { get; set; }
		public long Category { get; set; }
		public string CategoryValue { get; set; }

		public IEnumerable<AdapterInfo> AdapterInfos { get; set; }
		public string Remarks { get; set; }
		public string DataSheetLink { get; set; } = string.Empty;
		public string OperatingInstructionsLink { get; set; } = string.Empty;
		public string OtherInformationLink { get; set; } = string.Empty;

	}
}
