﻿namespace EmmeApp.Entities
{
    public class PumpReplacementProductEntity
	{
		public string MarketKey { get; set; }
		public string Type { get; set; }

		public long ProductId { get; set; }
		public string ConnectorInformation { get; set; }
		public string CommentCodes { get; set; }
	}
}
