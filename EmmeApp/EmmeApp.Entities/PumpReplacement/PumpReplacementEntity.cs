﻿namespace EmmeApp.Entities
{
    public class PumpReplacementEntity
	{
		public string MarketKey { get; set; }

		public string Type { get; set; }
		public string Diameter { get; set; }
		public string Length { get; set; }
		public string Pressure { get; set; }
		public string Engine { get; set; }
	}

	
}
