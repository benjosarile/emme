﻿namespace EmmeApp.Entities
{
    public class AdapterInfo
   {
      public string Code { get; set; }
      public uint ItemNumber { get; set; }
      public int NumberOfPieces { get; set; }
   }
}
