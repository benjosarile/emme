﻿using System.Collections.Generic;

namespace EmmeApp.Entities
{
    public class OcrRegionEntity
    {
        public string BoundingBox { get; set; }
        public List<OcrLineEntity> Lines { get; set; } = new List<OcrLineEntity>();
    }
}
