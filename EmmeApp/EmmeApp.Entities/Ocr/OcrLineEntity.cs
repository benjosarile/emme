﻿using System.Collections.Generic;

namespace EmmeApp.Entities
{
    public class OcrLineEntity
    {
        public string BoundingBox { get; set; }
        public List<OcrWordEntity> Words { get; set; } = new List<OcrWordEntity>();
    }
}
