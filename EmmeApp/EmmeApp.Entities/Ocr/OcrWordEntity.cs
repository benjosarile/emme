﻿namespace EmmeApp.Entities
{
    public class OcrWordEntity
    {
        public string BoundingBox { get; set; }
        public string Text { get; set; }
    }
}
