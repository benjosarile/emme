﻿using System.Collections.Generic;

namespace EmmeApp.Entities
{
    public class OcrObjectRootEntity
	{
        public string Language { get; set; }
        public double TextAngle { get; set; }
        public string Orientation { get; set; }
        public List<OcrRegionEntity> Regions { get; set; } = new List<OcrRegionEntity>();
    }
}
