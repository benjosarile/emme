﻿namespace EmmeApp.Entities
{
    public class SearchResultItemEntity
	{
		public string Key { get; set; }
		public string Name { get; set; }
		public string DocumentLink { get; set; }
		public string PreviewImage { get; set; }
		public long PageNumber { get; set; }
	}
}
