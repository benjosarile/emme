﻿using System;
using System.Collections.Generic;

namespace EmmeApp.Entities
{
    public class SearchContentByTypeRootEntity
	{
		public bool Success { get; set; }
		public bool Error { get; set; }
		public List<SearchCurrentFilterEntity> CurrentFilters { get; set; }
		public List<SearchFilterEntity> Filters { get; set; }
		public List<SearchFilterBoxEntity> FilterBox { get; set; }
		public string OutputType { get; set; }
		public bool LimitHit { get; set; }
		public List<SearchGroupEntity> Groups { get; set; }
	}

	public class SearchGroupEntity
	{
		public string AnchorLink { get; set; }
		public string AnchorLinkName { get; set; }
		public int AnchorCount { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
		public List<SearchCatalogEntity> Catalogs { get; set; }
	}

	public class SearchPagesByCatalogRootEntity
	{
		public bool Success { get; set; }
		public bool Error { get; set; }
		public List<SearchCurrentFilterEntity> CurrentFilters { get; set; }
		public List<SearchFilterEntity> Filters { get; set; }
		public List<SearchFilterBoxEntity> FilterBox { get; set; }
		public string OutputType { get; set; }
		public bool LimitHit { get; set; }
		public List<SearchCatalogEntity> Groups { get; set; }
	}

	public class SearchCurrentFilterEntity
	{
		public string Id { get; set; }
		public string Name { get; set; }
	}

	public class SearchFilterBoxEntity
	{
		public string Text { get; set; }
		public List<SearchFilterEntity> Children { get; set; }
	}

	public class SearchFilterEntity
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Group { get; set; }
		public string Img { get; set; }
		public string Icon { get; set; }
		public string DisplayText { get => this.Name; }
	}

	public class SearchCatalogEntity
	{
		public string Id { get; set; }
		public object Code { get; set; }
		public string Name { get; set; }
		public string BrandName { get; set; }
		public string BrandId { get; set; }
		public string BrandIcon { get; set; }
		public string SupplierNumber { get; set; }
		public string SupplierNumbers { get; set; }
		public List<object> Categories { get; set; }
		public List<object> Tags { get; set; }
		public bool Outdated { get; set; }
		public string Type { get; set; }
		public string Lang { get; set; }
		public DateTimeOffset PublishDate { get; set; }
		public string TypeName { get; set; }
		public string Query { get; set; }
		public long NumPages { get; set; }
		public bool Printable { get; set; }
		public string PreviewUrl { get; set; }
		public string MediumUrl { get; set; }
		public string ArchiveLabel { get; set; }
		public bool PdfAvailable { get; set; }
		public string DownloadUrl { get; set; }
		public List<SearchPageEntity> Pages { get; set; }
	}

	public class SearchPageEntity
	{
		public string Id { get; set; }
		public string PageName { get; set; }
		public string Name { get; set; }
		public string InternalName { get; set; }
		public long PagePage { get; set; }
		public string UsePageNum { get; set; }
		public string Brand { get; set; }
		public string Query { get; set; }
		public string BrandIcon { get; set; }
		public string PreviewUrl { get; set; }
		public string MediumUrl { get; set; }
		public bool Outdated { get; set; }
		public bool PdfAvailable { get; set; }
	}

	public class CatalogDetailsEntity
	{
		public bool Success { get; set; }
		public bool Error { get; set; }
		public string Name { get; set; }
		public string Catalog { get; set; }
		public bool Outdated { get; set; }
		public string ArchiveLabel { get; set; }
		public bool PdfAvailable { get; set; }
		public bool InfoplayEnabled { get; set; }
		public bool MobileEnabled { get; set; }
		public string BrandName { get; set; }
		public string BrandId { get; set; }
		public string SupplierNumber { get; set; }
		public List<string> SupplierNumbers { get; set; }
		public long NumPages { get; set; }
		public string WebviewerXod { get; set; }
		public string PreviewUrl { get; set; }
		public string BaseFileUrl { get; set; }
		public bool IsForwardingEnabled { get; set; }
		public long PageOffset { get; set; }
		public List<object> Toc { get; set; }
		public List<object> PageMapping { get; set; }
		public object Surcharge { get; set; }
		public bool ShowHintForCatalogsWithoutForwarding { get; set; }
		public bool ShowHintForCatalogNotInPartnerPortal { get; set; }
		public bool IsVisibleInPartnerPortal { get; set; }
		public bool ExtendedSecurity { get; set; }
		public bool ExtendedSecurityWithWarning { get; set; }
		public bool PaperclipActive { get; set; }
		public bool DeeplinksActive { get; set; }
		public bool RequestByMailActive { get; set; }
		public bool RequestToSupplierActive { get; set; }
		public string CatalogDownloadUrl { get; set; }
	}

}
