﻿using System.Collections.Generic;

namespace EmmeApp.Entities.External
{
	public class LogEntriesTExternalEntity : BaseExternalEntity
	{
		public List<LogEntryTExternalEntity> LogEntries { get; set; } = new List<LogEntryTExternalEntity>();
	}
}
