﻿using EmmeApp.Common.Enum;

namespace EmmeApp.Entities.External
{
	public class InitCommunicationTExternalEntity : BaseExternalEntity
	{
		public ExternalComProfileValue ProfileId { get; set; }
		public string FwVersion { get; set; }
		public ExternalCommunicationVersionValue CommunicationVersion { get; set; }
	}
}
