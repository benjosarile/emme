﻿using EmmeApp.Common.Enum;

namespace EmmeApp.Entities.External
{
	public class ParameterPumpVariACharacteristicExternalEntity
    {
        public ExternalPumpVariAParameterIdValue VariA { get; set; }
        public ScalingTExternalEntity Scaling { get; set; }
        public ExternalUnitTValue Unit { get; set; }
    }
}
