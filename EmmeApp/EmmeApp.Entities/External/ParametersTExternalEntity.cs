﻿using System.Collections.Generic;

namespace EmmeApp.Entities.External
{
	public class ParametersTExternalEntity : BaseExternalEntity
	{
		public List<ParameterTExternalEntity> Parameters { get; set; } = new List<ParameterTExternalEntity>();
	}
}
