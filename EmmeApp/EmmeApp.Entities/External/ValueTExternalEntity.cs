﻿using EmmeApp.Common.Enum;

namespace EmmeApp.Entities.External
{
	public class ValueTExternalEntity : BaseExternalEntity
	{
		public string Text { get; set; }
		public long Number { get; set; }
		public ExternalValueOneofCaseValue ValueCase { get; set; }
	}
}
