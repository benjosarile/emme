﻿using EmmeApp.Common.Enum;

namespace EmmeApp.Entities.External
{
	public class ParameterTExternalEntity : BaseExternalEntity
	{
		public ExternalControlUnitParameterIdValue ControlUnit { get; set; }
		public ExternalPumpModulAParameterIdValue ModulA { get; set; }
		public ExternalPumpVariAParameterIdValue VariA { get; set; }
		public ValueTExternalEntity Value { get; set; }
		public ScalingTExternalEntity Scaling { get; set; }
		public ExternalUnitTValue Unit { get; set; }
		public ExternalAccessLevelValue Read { get; set; }
		public ExternalAccessLevelValue Write { get; set; }
		public ExternalIdOneofCaseValue IdCase { get; set; }
	}
}
