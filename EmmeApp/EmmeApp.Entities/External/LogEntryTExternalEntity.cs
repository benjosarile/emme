﻿using EmmeApp.Common.Enum;

namespace EmmeApp.Entities.External
{
	public class LogEntryTExternalEntity : BaseExternalEntity
	{
		public int LogId { get; set; }
		public uint TimeAppear { get; set; }
		public uint TimeDisappear { get; set; }
		public ExternalLogSourceValue Source { get; set; }
		public ExternalLogTypeValue Type { get; set; }
	}
}
