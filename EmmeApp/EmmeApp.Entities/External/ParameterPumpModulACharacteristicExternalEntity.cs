﻿using EmmeApp.Common.Enum;

namespace EmmeApp.Entities.External
{
	public class ParameterPumpModulACharacteristicExternalEntity
    {
        public ExternalPumpModulAParameterIdValue ModulA { get; set; }
        public ScalingTExternalEntity Scaling { get; set; }
        public ExternalUnitTValue Unit { get; set; }
    }
}
