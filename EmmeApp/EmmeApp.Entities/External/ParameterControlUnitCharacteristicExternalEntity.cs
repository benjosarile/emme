﻿using EmmeApp.Common.Enum;

namespace EmmeApp.Entities.External
{
	public class ParameterControlUnitCharacteristicExternalEntity
	{
		public ExternalControlUnitParameterIdValue ControlUnit { get; set; }
		public ScalingTExternalEntity Scaling { get; set; }
		public ExternalUnitTValue Unit { get; set; }
	}
}
