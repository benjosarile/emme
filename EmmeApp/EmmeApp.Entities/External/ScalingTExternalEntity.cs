﻿namespace EmmeApp.Entities.External
{
	public class ScalingTExternalEntity : BaseExternalEntity
	{
		public long Max { get; set; }
		public long Min { get; set; }
		public double Step { get; set; }
	}
}
