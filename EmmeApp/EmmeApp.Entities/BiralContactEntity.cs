﻿namespace EmmeApp.Entities
{
    public class BiralContactEntity
	{
		public string Country { get; set; }
		public string CountryCode { get; set; }
		public string LanguageCode { get; set; }
		public string Market { get; set; }
		public string Name { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string ContactNumber { get; set; }
		public string EmailAddress { get; set; }
		public string Website { get; set; }

	}
}
