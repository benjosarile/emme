﻿using System.Collections.Generic;

namespace EmmeApp.Entities
{
    public class BleServiceEntity
	{
		public string Name { get; set; }
		public string Uuid { get; set; }

		public List<BleServiceCharacteristicEntity> Characteristics { get; set; }
	}

	public class BleServiceCharacteristicEntity
	{
		public string Name { get; set; }
		public string Uuid { get; set; }
		public string Role { get; set; }
	}
}
